﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Infinity.FCMB.EntityFramework.Extensions;

using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Services.Implementation;
using Microsoft.Extensions.DependencyInjection;
using Infinity.FCMB.TargetSavings.Data;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System.Security.Cryptography;
using System.IO;

namespace Infinity.FCMB.TargetSavings.DailyInterestRunProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            Run();
        }

        static void Run()
        {
            try
            {
                DateTime scheduledDate;
                string scheduledConfig = ConfigurationManager.AppSettings["DailyInterestRunScheduleDate"];
                Console.WriteLine($"Fetching ScheduleDate from config file: {scheduledConfig}");
                if (!DateTime.TryParse(scheduledConfig, out scheduledDate))
                {
                    scheduledDate = DateTime.Today.Date;
                }
                AppSettings.ConnectionString = ConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;
                Console.WriteLine($"Fetching ConnectionString from config file: ");
                var serviceProvider = new ServiceCollection()
                .AddScoped(provider =>
                {
                    return new TargetSavingsDBContext(AppSettings.ConnectionString);
                })
                .AddScoped<IUnitOfWork, TargetSavingsDBContext>()
                .AddScoped<ICustomerService, CustomerService>()
                .AddScoped<IApplicationLogService, ApplicationLogService>()
                .AddScoped<ITransactionService, TransactionService>()
                .AddScoped<IUtilityService, UtilityService>()
                .BuildServiceProvider();

                var transactionService = serviceProvider.GetService<ITransactionService>();
                var customerService = serviceProvider.GetService<ICustomerService>();
                var logger = serviceProvider.GetService<IApplicationLogService>();

                ActionReturn result = transactionService.ProcessDailyTargetSavingsInterest(scheduledDate);
                //ActionReturn result = transactionService.ProcessLiquidation();
                //logger.SendPendingEmails();
                if (!result.Flag)
                {
                    //send email
                    //log
                    logger.Add(result);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }
        }
    }
}
