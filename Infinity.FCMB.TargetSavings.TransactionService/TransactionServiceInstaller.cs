﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.TransactionService
{
    [RunInstaller(true)]
    public partial class TransactionServiceInstaller : System.Configuration.Install.Installer
    {
        public TransactionServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
