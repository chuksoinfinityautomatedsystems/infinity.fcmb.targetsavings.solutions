﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Data;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.TargetSavings.Services.Implementation;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Configuration;
using System.ServiceProcess;
using System.Threading.Tasks;
using System.Timers;

namespace Infinity.FCMB.TargetSavings.TransactionService
{
    public partial class TransactionService : ServiceBase
    {
        private Timer timer;
        private ITransactionService transactionServiceProcessor;
        private IApplicationLogService applicationLogProcessor;
        private double timerWakeUpFrequence;
        string FileLogPath = "";
        private bool busyProcessing;
        public TransactionService()
        {
            InitializeComponent();
            try
            {
                busyProcessing = false;
                RegisterService();

               
                string Timer_WakeUpFrequence_freq = ConfigurationManager.AppSettings["Timer_WakeUpFrequence"];
                FileLogPath = ConfigurationManager.AppSettings["FileLogPath"];
                if (!Double.TryParse(Timer_WakeUpFrequence_freq, out timerWakeUpFrequence)) timerWakeUpFrequence = 10000;
                timerWakeUpFrequence *= 1000;
                //-------------------------
            }
            catch (Exception ex)
            {
            }


            timer = new Timer(timerWakeUpFrequence);
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            applicationLogProcessor.AddToFile("TransactionService Initialized", "TSATransactionLiquidationService", FileLogPath);
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            applicationLogProcessor.AddToFile("TransactionService timer_Elapsed", "TSATransactionLiquidationService", FileLogPath);
            if (busyProcessing == false)
            {
                busyProcessing = true;
                RegisterService();
                applicationLogProcessor.AddToFile("TransactionService started running", "TSATransactionLiquidationService", FileLogPath);
                //ActionReturn result= transactionServiceProcessor.ProcessLiquidationRequests();
                ActionReturn result = transactionServiceProcessor.ProcessLiquidation();
                if (result.Flag)
                {
                    if (result.HasEmail)
                    {
                        Dictionary<int, object> emailLogDictionary = (Dictionary<int, object>)result.EmailLog;
                        Dictionary<int, object> emailValuesDictionary = (Dictionary<int, object>)result.EmailValues;
                        for (int i = 1; i <= emailLogDictionary.Count(); i++)
                        {
                            EmailLog emailLog = (EmailLog)emailLogDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            Dictionary<string, string> emailValue = (Dictionary<string, string>)emailValuesDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            applicationLogProcessor.AddEmailToLog(emailLog, emailValue);
                        }

                    }
                }
                //log
                applicationLogProcessor.Add(result);
                //System.Threading.Thread.Sleep(15000);
                busyProcessing = false;
                applicationLogProcessor.AddToFile("TransactionService stopped running, Busy Flag:" + busyProcessing, "TSATransactionLiquidationService", FileLogPath);
            }
            else
            {
                applicationLogProcessor.AddToFile("TransactionService still busy running, Busy Flag:" + busyProcessing, "TSATransactionLiquidationService", FileLogPath);
            }
        }
        private void RegisterService()
        {
            AppSettings.ConnectionString = ConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;
            var serviceProvider = new ServiceCollection()
            .AddScoped(provider =>
            {
                return new TargetSavingsDBContext(AppSettings.ConnectionString);
            })
            .AddScoped<IUnitOfWork, TargetSavingsDBContext>()
            .AddScoped<ICustomerService, CustomerService>()
            .AddScoped<IApplicationLogService, ApplicationLogService>()
            .AddScoped<ITransactionService, Services.Implementation.TransactionService>()
            .AddScoped<IUtilityService, UtilityService>()
            .BuildServiceProvider();

            applicationLogProcessor = serviceProvider.GetService<IApplicationLogService>();
            transactionServiceProcessor = serviceProvider.GetService<ITransactionService>();
        }

        protected override void OnStart(string[] args)
        {
            timer.Start();
            applicationLogProcessor.AddToFile("TransactionService Started", "TSATransactionLiquidationService", FileLogPath);

        }

        protected override void OnStop()
        {
            timer.Stop();
            applicationLogProcessor = null;
            transactionServiceProcessor = null;
        }
    }
}
