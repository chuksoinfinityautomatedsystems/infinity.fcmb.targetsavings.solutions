﻿using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class EmailLogConfiguration : EntityTypeConfiguration<EmailLog>
    {
        public EmailLogConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EmailType).HasColumnName("EmailType");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.EmailTo).HasColumnName("EmailTo");
            this.Property(t => t.EmailCopy).HasColumnName("EmailCopy");
            this.Property(t => t.EmailBlindCopy).HasColumnName("EmailBlindCopy");
            this.Property(t => t.EmailBody).HasColumnName("EmailBody");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.DateTimeCreated).HasColumnName("DateTimeCreated");
            this.Property(t => t.LastModifiedDateTime).HasColumnName("LastModifiedDate");
            this.Property(t => t.LastModifiedDateTime).HasColumnName("LastModifiedDateTime");
            this.Property(t => t.Trial).HasColumnName("Trial");
            this.Property(t => t.Response).HasColumnName("Response");


            this.ToTable("EmailLog", "dbo");

        }
    }
}
