﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class LiquidationTransactionConfiguration : EntityTypeConfiguration<LiquidationTransaction>
    {
        public LiquidationTransactionConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.CreatedDateTime).HasColumnName("CreatedDateTime");
            this.Property(t => t.HashValue).HasColumnName("HashValue");
            this.Property(t => t.LastUpdatedDate).HasColumnName("LastUpdatedDate");
            this.Property(t => t.LastUpdatedDateTime).HasColumnName("LastUpdatedDateTime");
            this.Property(t => t.Narration1).HasColumnName("Narration1");
            this.Property(t => t.Narration2).HasColumnName("Narration2");
            this.Property(t => t.ProcessingStatus).HasColumnName("ProcessingStatus");
            this.Property(t => t.ProcessingStatusDescription).HasColumnName("ProcessingStatusDescription");
            this.Property(t => t.RequestId).HasColumnName("RequestId");
            this.Property(t => t.Response).HasColumnName("Response");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.StatusDescription).HasColumnName("StatusDescription");
            this.Property(t => t.TransactionType).HasColumnName("TransactionType");
            this.Property(t => t.TransactionTypeDescription).HasColumnName("TransactionTypeDescription");
            this.Property(t => t.Trial).HasColumnName("Trial");

            this.ToTable("LiquidationTransaction", "dbo");
        }
    }
}
