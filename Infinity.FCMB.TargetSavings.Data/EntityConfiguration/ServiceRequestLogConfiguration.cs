﻿using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class ServiceRequestLogConfiguration : EntityTypeConfiguration<ServiceRequestLog>
    {
        public ServiceRequestLogConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ModuleId).HasColumnName("ModuleId");
            this.Property(t => t.ChannelId).HasColumnName("ChannelId");
            this.Property(t => t.ProfileId).HasColumnName("ProfileId");
            this.Property(t => t.RequestHost).HasColumnName("RequestHost");
            this.Property(t => t.UserAgent).HasColumnName("UserAgent");
            this.Property(t => t.Referer).HasColumnName("Referer");
            this.Property(t => t.Origin).HasColumnName("Origin");
            this.Property(t => t.Request).HasColumnName("Request");
            this.Property(t => t.Response).HasColumnName("Response");
            this.Property(t => t.InternalResponses).HasColumnName("InternalResponses");
            this.Property(t => t.RequestDate).HasColumnName("RequestDate");
            this.Property(t => t.RequestDateTime).HasColumnName("RequestDateTime");
            this.Property(t => t.ResponseDate).HasColumnName("ResponseDate");
            this.Property(t => t.ServiceType).HasColumnName("ServiceType");


            this.ToTable("ServiceRequestLog", "dbo");

        }
    }
}
