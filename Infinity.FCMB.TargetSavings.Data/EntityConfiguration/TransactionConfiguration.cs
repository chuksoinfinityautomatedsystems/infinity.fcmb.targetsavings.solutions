﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class TransactionConfiguration : EntityTypeConfiguration<Transaction>
    {
        public TransactionConfiguration()
        {
            base.HasKey<decimal>((Transaction t) => t.TransactionId);
            base.Property((Transaction t) => t.TransactionId).HasDatabaseGeneratedOption(new DatabaseGeneratedOption?(DatabaseGeneratedOption.Identity));
            base.Property((Transaction t) => t.TransactionId).HasColumnName("TransactionId");
            base.Property((Transaction t) => t.Amount).HasColumnName("Amount");
            base.Property((Transaction t) => t.ApprovedBy).HasColumnName("ApprovedBy").HasMaxLength(new int?(100));
            base.Property((Transaction t) => t.ApprovedDate).HasColumnName("ApprovedDate");
            base.Property((Transaction t) => t.Narration).HasColumnName("Narration").HasMaxLength(new int?(100));
            base.Property((Transaction t) => t.PostedBy).HasColumnName("PostedBy").HasMaxLength(new int?(100));
            base.Property((Transaction t) => t.PostedDate).HasColumnName("PostedDate");
            base.Property((Transaction t) => t.TransactionDate).HasColumnName("TransactionDate");
            base.Property((Transaction t) => t.TransactionType).HasColumnName("TransactionType").HasMaxLength(new int?(50));
            base.Property((Transaction t) => t.TargetGoalsId).HasColumnName("TargetGoalId");
            this.Property(t=>t.HashValue).HasColumnName("HashValue");

            this.HasRequired(o => o.TargetGoal).WithMany(o => o.Transactions).HasForeignKey(o => o.TargetGoalsId);
            base.ToTable("Transactions", "dbo");
        }
    }
}
