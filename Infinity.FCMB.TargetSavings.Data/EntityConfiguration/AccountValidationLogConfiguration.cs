﻿using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System.Data.Entity.ModelConfiguration;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class AccountValidationLogConfiguration : EntityTypeConfiguration<AccountValidationLog>
    {
        public AccountValidationLogConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ReferenceId).HasColumnName("ReferenceId");
            this.Property(t => t.TargetGoalId).HasColumnName("TargetGoalId");
            this.Property(t => t.CustomerId).HasColumnName("CustomerId");
            this.Property(t => t.HashValue).HasColumnName("HashValue");
            this.Property(t => t.CreatedDate).HasColumnName("CreatedDate");
            this.Property(t => t.LastModifiedDate).HasColumnName("LastModifiedDate");


            this.ToTable("AccountValidationLog", "dbo");

        }
    }
}
