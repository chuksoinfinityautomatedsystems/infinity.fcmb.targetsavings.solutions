﻿using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class EmailTemplateConfiguration : EntityTypeConfiguration<EmailTemplate>
    {
        public EmailTemplateConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.EmailType).HasColumnName("EmailType");
            this.Property(t => t.Subject).HasColumnName("Subject");
            this.Property(t => t.Template).HasColumnName("Template");
            this.Property(t => t.IsActive).HasColumnName("IsActive");


            this.ToTable("EmailTemplate", "dbo");

        }
    }
}
