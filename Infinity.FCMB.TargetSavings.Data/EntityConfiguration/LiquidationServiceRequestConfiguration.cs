﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class LiquidationServiceRequestConfiguration : EntityTypeConfiguration<LiquidationServiceRequest>
    {
        public LiquidationServiceRequestConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.RequestType).HasColumnName("RequestType");
            this.Property(t => t.Amount).HasColumnName("Amount");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.HashValue).HasColumnName("HashValue");
            this.Property(t => t.TargetId).HasColumnName("TargetId");
            this.Property(t => t.ProcessingResponse).HasColumnName("ProcessingResponse");
            this.Property(t => t.ProcessingStatus).HasColumnName("ProcessingStatus");
            this.Property(t => t.RequestDate).HasColumnName("RequestDate");
            this.Property(t => t.RequestDateTime).HasColumnName("RequestDateTime");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.TreatedDate).HasColumnName("TreatedDate");
            this.Property(t => t.TreatedDateTime).HasColumnName("TreatedDateTime");
            this.Property(t => t.Trial).HasColumnName("Trial");

            this.ToTable("LiquidationServiceRequest", "dbo");
        }
    
    }
}
