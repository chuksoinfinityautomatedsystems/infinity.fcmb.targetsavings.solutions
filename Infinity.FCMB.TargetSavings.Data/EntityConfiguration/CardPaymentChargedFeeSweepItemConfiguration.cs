﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    class CardPaymentChargedFeeSweepItemConfiguration : EntityTypeConfiguration<CardPaymentChargedFeeSweepItem>
    {
        public CardPaymentChargedFeeSweepItemConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DateSwept).HasColumnName("DateSwept");
            this.Property(t => t.DateTimeSwept).HasColumnName("DateTimeSwept");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.CollectionAccount).HasColumnName("CollectionAccount");
            this.Property(t => t.ExpenseAccount).HasColumnName("ExpenseAccount");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.SweepDate).HasColumnName("SweepDate");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");

            this.ToTable("CardPaymentChargedFeeSweepItem", "dbo");
        }
    
    }
}
