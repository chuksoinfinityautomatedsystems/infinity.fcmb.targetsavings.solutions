﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    class BankConfiguration : EntityTypeConfiguration<Bank>
    {
        public BankConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.BankName).HasColumnName("Bank");
            this.Property(t => t.Code).HasColumnName("Code");
            this.Property(t => t.IsActive).HasColumnName("IsActive");


            this.ToTable("Bank", "dbo");

        }
    }
}
