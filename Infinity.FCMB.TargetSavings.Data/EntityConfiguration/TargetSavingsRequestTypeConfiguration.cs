﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class TargetSavingsRequestTypeConfiguration : EntityTypeConfiguration<TargetSavingsRequestType>
    {
        public TargetSavingsRequestTypeConfiguration()
        {
            this.HasKey(t => t.RequestTypeId);
            this.Property(t => t.RequestTypeId).HasColumnName("RequestTypeId");
            this.Property(t => t.RequestType).HasColumnName("RequestType");
            this.Property(t => t.IsActive).HasColumnName("IsActive");
            this.Property(t => t.IsLiquidation).HasColumnName("IsLiquidation");
            this.Property(t => t.IsInterestLiquidation).HasColumnName("IsInterestLiquidation"); 

            this.ToTable("TargetSavingsRequestType", "dbo");

        }
    }
}
