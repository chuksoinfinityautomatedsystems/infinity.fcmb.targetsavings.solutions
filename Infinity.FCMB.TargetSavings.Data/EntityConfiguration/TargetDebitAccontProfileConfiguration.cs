﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class TargetDebitAccountProfileConfiguration : EntityTypeConfiguration<TargetDebitAccountProfile>
    {
        public TargetDebitAccountProfileConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.HashValue).HasColumnName("HashValue");
            this.Property(t => t.LastModifiedDate).HasColumnName("LastModifiedDate");
            this.Property(t => t.MandateId).HasColumnName("MandateId");
            this.Property(t => t.RequestId).HasColumnName("RequestId");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.TargetGoalId).HasColumnName("TargetGoalId");
            this.Property(t => t.IsActive).HasColumnName("IsActive");


            this.ToTable("TargetDebitAccountProfile", "dbo");

        }
    }
}
