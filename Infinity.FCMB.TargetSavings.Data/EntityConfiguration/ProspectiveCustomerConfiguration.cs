﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class ProspectiveCustomerConfiguration : EntityTypeConfiguration<ProspectiveCustomer>
    {
        public ProspectiveCustomerConfiguration()
        {
            base.HasKey<long>((ProspectiveCustomer t) => t.ProspectId);
            base.Property<long>((ProspectiveCustomer t) => t.ProspectId).HasDatabaseGeneratedOption(new DatabaseGeneratedOption?(DatabaseGeneratedOption.Identity));
            base.Property<long>((ProspectiveCustomer t) => t.ProspectId).HasColumnName("ProspectId");
            base.Property((ProspectiveCustomer t) => t.BiometricVerificationNo).HasColumnName("BiometricVerificationNo").HasMaxLength(new int?(50));
            base.Property((ProspectiveCustomer t) => t.EmailAddress).HasColumnName("EmailAddress").HasMaxLength(new int?(100));
            base.Property((ProspectiveCustomer t) => t.FirstName).HasColumnName("FirstName").HasMaxLength(new int?(200));
            base.Property<bool>((ProspectiveCustomer t) => t.IsFCMBCustomer).HasColumnName("IsFCMBCustomer");
            base.Property((ProspectiveCustomer t) => t.PhoneNumber).HasColumnName("PhoneNumber").HasMaxLength(new int?(50));
            base.Property((ProspectiveCustomer t) => t.Surname).HasColumnName("Surname").HasMaxLength(new int?(200));
            base.Property((ProspectiveCustomer t) => t.Status).HasColumnName("Status").HasMaxLength(new int?(50));
            base.Property((ProspectiveCustomer t) => t.ReferalCode).HasColumnName("ReferalCode").HasMaxLength(new int?(50));
            base.ToTable("ProspectiveCustomers", "dbo");
        }
    }
}
