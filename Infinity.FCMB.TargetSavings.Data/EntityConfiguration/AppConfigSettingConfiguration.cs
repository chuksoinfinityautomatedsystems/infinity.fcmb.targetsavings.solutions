﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class AppConfigSettingConfiguration : EntityTypeConfiguration<AppConfigSetting>
    {
        public AppConfigSettingConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Key).HasColumnName("SettingKey");
            this.Property(t => t.Value).HasColumnName("SettingValue");
            this.Property(t => t.IsActive).HasColumnName("IsActive");


            this.ToTable("AppConfigSetting", "dbo");

        }
    }
}
