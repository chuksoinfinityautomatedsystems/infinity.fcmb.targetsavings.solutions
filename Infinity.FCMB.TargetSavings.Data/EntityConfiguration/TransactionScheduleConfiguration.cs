﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System.Data.Entity.ModelConfiguration;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class TransactionScheduleConfiguration : EntityTypeConfiguration<TransactionSchedule>
    {
        public TransactionScheduleConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TargetGoalId).HasColumnName("TargetGoalId");
            this.Property(t => t.DebitType).HasColumnName("DebitType");
            this.Property(t => t.RunDate).HasColumnName("RunDate");
            this.Property(t => t.NextRunDate).HasColumnName("NextRunDate");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.DateCreated).HasColumnName("DateCreated");
            this.Property(t => t.DatetimeCreated).HasColumnName("DateTimeCreated");
            this.Property(t => t.DateProcessed).HasColumnName("DateProcessed");
            this.Property(t => t.DatetimeProcessed).HasColumnName("DateTimeProcessed");
            this.Property(t => t.ProcessingStatus).HasColumnName("ProcessingStatus");
            this.Property(t => t.ProcessingResponse).HasColumnName("ProcessingResponse");
            this.Property(t => t.Trials).HasColumnName("Trials");

            this.ToTable("TransactionSchedule", "dbo");

        }
    }
}

