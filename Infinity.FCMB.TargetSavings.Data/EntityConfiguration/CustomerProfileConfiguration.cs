﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.CompilerServices;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class CustomerProfileConfiguration : EntityTypeConfiguration<CustomerProfile>
    {
        public CustomerProfileConfiguration()
        {
            base.HasKey<long>((CustomerProfile t) => t.CustomerProfileId);
            base.Property<long>((CustomerProfile t) => t.CustomerProfileId).HasDatabaseGeneratedOption(new DatabaseGeneratedOption?(DatabaseGeneratedOption.Identity));
            base.Property<long>((CustomerProfile t) => t.CustomerProfileId).HasColumnName("CustomerProfileId");
            base.Property((CustomerProfile t) => t.BiometricVerificationNo).HasColumnName("BiometricVerificationNo").HasMaxLength(new int?(50));
            base.Property((CustomerProfile t) => t.Address).HasColumnName("Address").HasMaxLength(new int?(500));
            base.Property((CustomerProfile t) => t.ComputerDetails).HasColumnName("ComputerDetails").HasMaxLength(new int?(250));
            base.Property((CustomerProfile t) => t.DateCreated).HasColumnName("DateCreated");
            base.Property((CustomerProfile t) => t.EmailAddress).HasColumnName("EmailAddress").HasMaxLength(new int?(100));
            base.Property((CustomerProfile t) => t.FirstName).HasColumnName("FirstName").HasMaxLength(new int?(200));
            base.Property<bool>((CustomerProfile t) => t.IsFCMBCustomer).HasColumnName("IsFCMBCustomer");
            base.Property((CustomerProfile t) => t.LastLoginDate).HasColumnName("LastLoginDate");
            base.Property((CustomerProfile t) => t.LastPasswordChanged).HasColumnName("LastPasswordChanged");
            base.Property((CustomerProfile t) => t.NextOfKinEmail).HasColumnName("NextOfKinEmail").HasMaxLength(new int?(100));
            base.Property((CustomerProfile t) => t.NextOfKinFullName).HasColumnName("NextOfKinFullName").HasMaxLength(new int?(200));
            base.Property((CustomerProfile t) => t.NextOfKinPhone).HasColumnName("NextOfKinPhone").HasMaxLength(new int?(50));
            base.Property(t => t.Password).HasColumnName("Password");
            base.Property((CustomerProfile t) => t.PhoneNumber).HasColumnName("PhoneNumber").HasMaxLength(new int?(50));
            base.Property(t=> t.ProfilePicture).HasColumnName("ProfilePicture");
            base.Property((CustomerProfile t) => t.ProfileStatus).HasColumnName("ProfileStatus").HasMaxLength(new int?(100));
            base.Property<long>((CustomerProfile t) => t.ProspectId).HasColumnName("ProspectId");
            base.Property((CustomerProfile t) => t.Surname).HasColumnName("Surname").HasMaxLength(new int?(200));
            base.Property((CustomerProfile t) => t.UserName).HasColumnName("UserName").HasMaxLength(new int?(50));
            this.Property(t => t.HashValue).HasColumnName("HashValue");

            base.ToTable("CustomerProfiles", "dbo");
        }
    }
}
