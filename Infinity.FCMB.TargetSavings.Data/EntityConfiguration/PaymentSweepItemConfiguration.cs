﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class PaymentSweepItemConfiguration : EntityTypeConfiguration<PaymentSweepItem>
    {
        public PaymentSweepItemConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.DateSwept).HasColumnName("DateSwept");
            this.Property(t => t.DateTimeSwept).HasColumnName("DateTimeSwept");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.FixedAccount).HasColumnName("FixedAccount");
            this.Property(t => t.FixedAmount).HasColumnName("FixedAmount");
            this.Property(t => t.FlexibleAccount).HasColumnName("FlexibleAccount");
            this.Property(t => t.FlexibleAmount).HasColumnName("FlexibleAmount");
            this.Property(t => t.Status).HasColumnName("Status");
            this.Property(t => t.SweepAccount).HasColumnName("SweepAccount");
            this.Property(t => t.SweepDate).HasColumnName("SweepDate");
            this.Property(t => t.TotalAmount).HasColumnName("TotalAmount");
            this.Property(t => t.SweepType).HasColumnName("SweepType");

            this.ToTable("PaymentSweepItem", "dbo");
        }
    }
}
