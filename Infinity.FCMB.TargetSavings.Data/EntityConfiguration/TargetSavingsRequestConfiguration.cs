﻿using System;
using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.CompilerServices;


namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class TargetSavingsRequestConfiguration : EntityTypeConfiguration<TargetSavingsRequest>
    {
        public TargetSavingsRequestConfiguration()
        {
            base.HasKey<long>((TargetSavingsRequest t) => t.RequestId);
            base.Property<long>((TargetSavingsRequest t) => t.RequestId).HasDatabaseGeneratedOption(new DatabaseGeneratedOption?(DatabaseGeneratedOption.Identity));
            base.Property<long>((TargetSavingsRequest t) => t.RequestId).HasColumnName("RequestId");
            base.Property((TargetSavingsRequest t) => t.DateTreated).HasColumnName("DateTreated");
            base.Property((TargetSavingsRequest t) => t.RequestDate).HasColumnName("RequestDate");
            base.Property((TargetSavingsRequest t) => t.RequestDescription).HasColumnName("RequestDescription").HasMaxLength(new int?(200));
            base.Property((TargetSavingsRequest t) => t.RequestTypeId).HasColumnName("RequestTypeId");
            base.Property((TargetSavingsRequest t) => t.Status).HasColumnName("Status").HasMaxLength(new int?(50));
            base.Property((TargetSavingsRequest t) => t.ApprovalStatus).HasColumnName("ApprovalStatus").HasMaxLength(new int?(50));
            base.Property<long>((TargetSavingsRequest t) => t.TargetGoalId).HasColumnName("TargetGoalId");
            base.Property((TargetSavingsRequest t) => t.TreatedBy).HasColumnName("TreatedBy").HasMaxLength(new int?(100));
            base.Property(t => t.LiquidationAmount).HasColumnName("LiquidationAmount");
            this.Property(t => t.HashValue).HasColumnName("HashValue");
            this.Property(t => t.ProcessingResponse).HasColumnName("ProcessingResponse");
            this.Property(t => t.Trial).HasColumnName("Trial");
            this.HasRequired(o => o.TargetGoal).WithMany(o => o.TargetSavingsRequests).HasForeignKey(o => o.TargetGoalId);

            this.HasRequired(o => o.RequestType).WithMany(o => o.TargetSavingsRequest).HasForeignKey(o => o.RequestTypeId);

            base.ToTable("TargetSavingsRequests", "dbo");
        }
    }
}
