﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class TargetWebPaymentProfileConfiguration : EntityTypeConfiguration<TargetWebPaymentProfile>
    {
        public TargetWebPaymentProfileConfiguration()
        {
            base.HasKey(t => t.Id);
            base.Property(t => t.Id).HasDatabaseGeneratedOption(new DatabaseGeneratedOption?(DatabaseGeneratedOption.Identity));
            base.Property(t => t.Id).HasColumnName("Id");
            base.Property(t => t.InternalValue).HasColumnName("InternalValue");
            base.Property(t => t.ExternalValue).HasColumnName("ExternalValue");

            base.ToTable("TargetWebPaymentProfile", "dbo");
        }
    }
}
