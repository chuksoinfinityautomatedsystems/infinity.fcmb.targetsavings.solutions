﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Runtime.CompilerServices;
namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class TargetGoalConfiguration : EntityTypeConfiguration<TargetGoal>
    {
        public TargetGoalConfiguration()
        {
            base.HasKey<long>((TargetGoal t) => t.TargetGoalsId);
            base.Property<long>((TargetGoal t) => t.TargetGoalsId).HasDatabaseGeneratedOption(new DatabaseGeneratedOption?(DatabaseGeneratedOption.Identity));
            base.Property<long>((TargetGoal t) => t.TargetGoalsId).HasColumnName("TargetGoalsId");
            base.Property<long>((TargetGoal t) => t.CustomerProfileId).HasColumnName("CustomerProfileId");
            base.Property((TargetGoal t) => t.GoalBalance).HasColumnName("GoalBalance");
            base.Property((TargetGoal t) => t.GoalCardTokenDetails).HasColumnName("GoalCardTokenDetails");
            base.Property((TargetGoal t) => t.GoalDebitAccountNo).HasColumnName("GoalDebitAccountNo").HasMaxLength(new int?(50));
            base.Property((TargetGoal t) => t.GoalDebitBank).HasColumnName("GoalDebitBank").HasMaxLength(new int?(50));
            base.Property((TargetGoal t) => t.GoalDebitType).HasColumnName("GoalDebitType").HasMaxLength(new int?(50));
            base.Property((TargetGoal t) => t.GoalDescription).HasColumnName("GoalDescription").HasMaxLength(new int?(100));
            base.Property((TargetGoal t) => t.GoalEndDate).HasColumnName("GoalEndDate");
            base.Property((TargetGoal t) => t.GoalName).HasColumnName("GoalName").HasMaxLength(new int?(100));
            base.Property((TargetGoal t) => t.GoalnterestEarned).HasColumnName("GoalnterestEarned");
            base.Property((TargetGoal t) => t.GoalStartAmount).HasColumnName("GoalStartAmount");
            base.Property((TargetGoal t) => t.GoalStartDate).HasColumnName("GoalStartDate");
            base.Property((TargetGoal t) => t.GoalStatus).HasColumnName("GoalStatus").HasMaxLength(new int?(50));
            base.Property((TargetGoal t) => t.LastRunDate).HasColumnName("LastRunDate");
            base.Property((TargetGoal t) => t.NextRunDate).HasColumnName("NextRunDate");
            base.Property((TargetGoal t) => t.SetUpDate).HasColumnName("SetUpDate");
            base.Property((TargetGoal t) => t.TargetAmount).HasColumnName("TargetAmount");
            base.Property<int>((TargetGoal t) => t.TargetFrequency).HasColumnName("TargetFrequency");
            base.Property((TargetGoal t) => t.TargetFrequencyAmount).HasColumnName("TargetFrequencyAmount");
            base.Property(t=> t.TargetGoalPics).HasColumnName("TargetGoalPics");
            base.Property(t => t.LiquidationMode).HasColumnName("LiquidationMode");
            this.Property(t => t.HashValue).HasColumnName("HashValue");
            this.Property(t => t.IsLiquidated).HasColumnName("IsLiquidated");
            this.Property(t => t.ExtendedGoalStatus).HasColumnName("ExtendedGoalStatus");
            //base.Property(t=>t.GoalDebitAccountId).HasColumnName("GoalDebitAccountId").HasMaxLength(new int?(50));

            this.HasRequired(o => o.CustomerProfile).WithMany(o => o.TargetGoals).HasForeignKey(o => o.CustomerProfileId);
            this.HasRequired(o => o.TargetSavingsFrequency).WithMany(o => o.TargetGoal).HasForeignKey(o => o.TargetFrequency);

            base.ToTable("TargetGoals", "dbo");
        }
    }
}
