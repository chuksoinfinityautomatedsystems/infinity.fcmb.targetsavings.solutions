﻿using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class ApplicationLogConfiguration : EntityTypeConfiguration<ApplicationLog>
    {
        public ApplicationLogConfiguration()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ApplicationName).HasColumnName("ApplicationName");
            this.Property(t => t.ClassName).HasColumnName("ClassName");
            this.Property(t => t.MethodName).HasColumnName("MethodName");
            this.Property(t => t.Flag).HasColumnName("Flag");
            this.Property(t => t.Message).HasColumnName("Message");
            this.Property(t => t.FlagDescription).HasColumnName("FlagDescription");
            this.Property(t => t.ReturnedObject).HasColumnName("ReturnedObject");
            this.Property(t => t.DatecCeated).HasColumnName("DateCreated");
            this.Property(t => t.DateTimeCreated).HasColumnName("DateTimeCreated");


            this.ToTable("ApplicationLog", "dbo");

        }
    }
}
