﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            base.HasKey(t => t.Id);
            base.Property(t => t.Id).HasDatabaseGeneratedOption(new DatabaseGeneratedOption?(DatabaseGeneratedOption.Identity));
            base.Property(t => t.Id).HasColumnName("Id");
            base.Property(t => t.Name).HasColumnName("Name").HasMaxLength(new int?(50));
            base.Property(t=>t.Image).HasColumnName("Image");
            base.Property(t => t.IsActive).HasColumnName("IsActive");

            base.ToTable("Category", "dbo");
        }
    }
}
