﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Data.EntityConfiguration
{
    public class TransactionInterestConfiguration : EntityTypeConfiguration<TransactionInterest>
    {
        public TransactionInterestConfiguration()
        {
            base.HasKey<decimal>((TransactionInterest t) => t.TransactionId);
            base.Property((TransactionInterest t) => t.TransactionId).HasDatabaseGeneratedOption(new DatabaseGeneratedOption?(DatabaseGeneratedOption.Identity));
            base.Property((TransactionInterest t) => t.TransactionId).HasColumnName("TransactionId");
            base.Property((TransactionInterest t) => t.Amount).HasColumnName("Amount");
            base.Property((TransactionInterest t) => t.Narration).HasColumnName("Narration").HasMaxLength(new int?(100));
            base.Property((TransactionInterest t) => t.PostedDate).HasColumnName("PostedDate");
            base.Property((TransactionInterest t) => t.TransactionDate).HasColumnName("TransactionDate");
            base.Property((TransactionInterest t) => t.GoalBalanceAmount).HasColumnName("GoalBalanceAmount");
            base.Property((TransactionInterest t) => t.TargetGoalsId).HasColumnName("TargetGoalId");
            this.Property(t => t.HashValue).HasColumnName("HashValue");
            this.Property(t => t.Status).HasColumnName("Status");

            base.ToTable("TransactionInterest", "dbo");
        }
    }
}
