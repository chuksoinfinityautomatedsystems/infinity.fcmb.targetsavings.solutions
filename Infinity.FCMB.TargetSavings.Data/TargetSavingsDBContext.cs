﻿using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Data.EntityConfiguration;
using Infinity.FCMB.TargetSavings.Domain.Models;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System;
using System.Data.Entity;

namespace Infinity.FCMB.TargetSavings.Data
{
    public class TargetSavingsDBContext : DbContext, IUnitOfWork
    {
        public DbSet<ProspectiveCustomer> ProspectiveCustomers
        {
            get;
            set;
        }

        public DbSet<CustomerProfile> CustomerProfiles
        {
            get;
            set;
        }

        public DbSet<TargetGoal> TargetGoals
        {
            get;
            set;
        }

        public DbSet<TargetSavingsRequest> TargetSavingsRequests
        {
            get;
            set;
        }

        public DbSet<Transaction> Transactions
        {
            get;
            set;
        }
        public DbSet<Category> Categories
        {
            get;
            set;
        }
        public DbSet<TargetResultNote> TargetResultNotes { get; set; }
        public DbSet<TargetSavingsFrequency> TargetSavingsFrequencys { get; set; }
        public DbSet<TargetSavingsRequestType> Targetsavingsrequesttypes { get; set; }
        public DbSet<TargetDebitAccountProfile> TargetDebitAccountProfiles { get; set; }
        public DbSet<TargetWebPaymentProfile> TargetWebPaymentProfiles { get; set; }
        public DbSet<AppConfigSetting> AppConfigSettings { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<TransactionSchedule> TransactionSchedules { get; set; }
        public DbSet<ApplicationLog> ApplicationLogs { get; set; }
        public DbSet<EmailLog> EmailLogs { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }
        public DbSet<ServiceRequestLog> ServiceRequestLogs { get; set; }
        public DbSet<AccountValidationLog> AccountValidationLogs { get; set; }
        public DbSet<TransactionInterest> TransactionInterests { get; set; }
        public DbSet<PaymentSweepItem> PaymentSweepItems { get; set; }
        public DbSet<CardPaymentChargedFeeSweepItem> CardPaymentChargedFeeSweepItems { get; set; }
        public DbSet<LiquidationTransaction> LiquidationTransactions { get; set; }
        public DbSet<LiquidationServiceRequest> LiquidationServiceRequests { get; set; }

        //Database Database => throw new NotImplementedException();

        public TargetSavingsDBContext() : base(AppSettings.ConnectionString)
        {
            base.Configuration.ProxyCreationEnabled = false;
            base.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<TargetSavingsDBContext>(null);
        }

        public TargetSavingsDBContext(string connectionString) : base(AppSettings.ConnectionString)
        {
            base.Configuration.ProxyCreationEnabled = false;
            base.Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer<TargetSavingsDBContext>(null);
        }

        public int ExecuteSqlCommand(string sql, params object[] parameters)
        {
            if (parameters != null)
            {
                int arg_05_0 = parameters.Length;
            }
            return base.Database.ExecuteSqlCommand(sql, Array.Empty<object>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add<ProspectiveCustomer>(new ProspectiveCustomerConfiguration());
            modelBuilder.Configurations.Add<CustomerProfile>(new CustomerProfileConfiguration());
            modelBuilder.Configurations.Add<TargetGoal>(new TargetGoalConfiguration());
            modelBuilder.Configurations.Add<TargetSavingsRequest>(new TargetSavingsRequestConfiguration());
            modelBuilder.Configurations.Add<Transaction>(new TransactionConfiguration());
            modelBuilder.Configurations.Add<Category>(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new TargetResultNoteConfiguration());
            modelBuilder.Configurations.Add(new TargetSavingsFrequencyConfiguration());
            modelBuilder.Configurations.Add(new TargetSavingsRequestTypeConfiguration());
            modelBuilder.Configurations.Add(new TargetDebitAccountProfileConfiguration());
            modelBuilder.Configurations.Add(new TargetWebPaymentProfileConfiguration());
            modelBuilder.Configurations.Add(new AppConfigSettingConfiguration());
            modelBuilder.Configurations.Add(new BankConfiguration());
            modelBuilder.Configurations.Add(new TransactionScheduleConfiguration());
            modelBuilder.Configurations.Add(new ApplicationLogConfiguration());
            modelBuilder.Configurations.Add(new EmailLogConfiguration());
            modelBuilder.Configurations.Add(new EmailTemplateConfiguration());
            modelBuilder.Configurations.Add(new ServiceRequestLogConfiguration());
            modelBuilder.Configurations.Add(new AccountValidationLogConfiguration());
            modelBuilder.Configurations.Add(new TransactionInterestConfiguration());
            modelBuilder.Configurations.Add(new PaymentSweepItemConfiguration());
            modelBuilder.Configurations.Add(new CardPaymentChargedFeeSweepItemConfiguration());
            modelBuilder.Configurations.Add(new LiquidationTransactionConfiguration());
            modelBuilder.Configurations.Add(new LiquidationServiceRequestConfiguration());
        }

    }
}
