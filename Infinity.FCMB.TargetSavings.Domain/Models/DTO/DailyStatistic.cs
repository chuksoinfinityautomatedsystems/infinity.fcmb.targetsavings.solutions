﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.DTO
{
    public class DailyStatistic
    {
        public string Name { get; set; }
        public int TotalCount { get; set; }
        public int DailyCount { get; set; }
        public decimal DailyCountPercentage { get; set; }
        public DateTime StatisticsDateTime { get; set; }
    }
}
