﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.DTO
{
    public class CustomerRegistrationStatusDTO
    {
        public bool HasProspectId { get; set; }
        public bool HasCustomerProfileId { get; set; }
        public bool HasGoals { get; set; }
        public CustomerProfile Profile { get; set; }
        public ProspectiveCustomer Prospect { get; set; }
        public List<TargetGoal> Targets { get; set; }
    }
}
