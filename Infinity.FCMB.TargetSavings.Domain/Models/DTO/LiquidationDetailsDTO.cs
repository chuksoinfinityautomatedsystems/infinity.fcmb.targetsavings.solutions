﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.DTO
{
    public class LiquidationDetailsDTO
    {
        public string AccountNumber { get; set; }
        public string Bank { get; set; }
        public string BankCode { get; set; }
        public long TargetGoalId { get; set; }
    }
}
