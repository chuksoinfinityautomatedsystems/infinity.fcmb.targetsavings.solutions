﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Paystack
{
    public class ChargeCardRequest
    {
        public string authorization_code { get; set; }
        public string email { get; set; }
        public long amount { get; set; }
    }

    public class ChargeCardResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public Data data { get; set; }
        
        public Customer customer { get; set; }
    }

    public class Data
    {
        public string status { get; set; }
        public string reference { get; set; }
        public string gateway_response { get; set; }
        public Authorization authorization { get; set; }
    }

    public class Authorization
    {
        public string authorization_code { get; set; }
        public string card_type { get; set; }
        public string last4 { get; set; }
        public string exp_month { get; set; }
        public string exp_year { get; set; }
        public string bank { get; set; }
        public bool reusable { get; set; }
        public string signature { get; set; }
        public string country_code { get; set; }
    }
    public class Customer
    {
        public long id { get; set; }
        public string customer_code { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
    }
}
