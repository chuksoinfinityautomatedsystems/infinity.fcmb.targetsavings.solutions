﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.FCMB
{
    public class NameEnquiryRequest
    {
        public string AccountNumber { get; set; }
        public string BankCode { get; set; }
    }

    public class GetTransactionStatusRequest
    {
        public string TransactionId { get; set; }

    }

    public class InterBankNameEquiryRequest
    {
        public string AccountNumber { get; set; }
        public string ChannelCode { get; set; }
        public string DestinationInstitutionCode { get; set; }

    }

    public class TokenRequest
    {
        public string AccountNumber { get; set; }
        public string TokenType { get; set; }

    }

    public class BvnValidationRequest
    {
        public string Bvn { get; set; }
        public bool ShowImageIfAvailable { get; set; }

    }

    public class DirectCreditRequest
    {
        public string SourceAccountNumber { get; set; }
        public string DestinationAccountNumber { get; set; }
        public string Amount { get; set; }
        public string Narration { get; set; }
        public string Currency { get; set; }
        public string BranchCode { get; set; }
        public string Narration2 { get; set; }
        public string InstrumentNo { get; set; }
        public string TransactionDate { get; set; }
        public string DestinationBank { get; set; }
        public string DestinationBankCode { get; set; }
        public string TransactionId { get; set; }
        public string BatchId { get; set; }
        public string Token { get; set; }
        public string OriginatorName { get; set; }
    }

    public class InterBankTransferRequest
    {
        public string NameEnquiryRef { get; set; }
        public string DestinationInstitutionCode { get; set; }
        public string ChannelCode { get; set; }
        public string BeneficiaryAccountNumber { get; set; }
        public string BeneficiaryAccountName { get; set; }
        public string BeneficiaryBankVerificationNumber { get; set; }
        public string BeneficiaryKYCLevel { get; set; }
        public string OriginatorAccountName { get; set; }
        public string OriginatorAccountNumber { get; set; }
        public string TransactionLocation { get; set; }
        public string Narration { get; set; }
        public string PaymentReference { get; set; }
        public decimal Amount { get; set; }
        public string Token { get; set; }
        public string TransactionDate { get; set; }
        public string Currency { get; set; }
    }
}
