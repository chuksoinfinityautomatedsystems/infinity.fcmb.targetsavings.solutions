﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.FCMB
{
    public class FCMBResponse
    {
        public string StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public object ResponseData { get; set; }
        public long Id { get; set; }
        public string TranId { get; set; }
        public string CallerName { get; set; }
        public string BaseUrl { get; set; }
    }

    public class NameEnquiryResponse
    {
        public string AccountNumber { get; set; }
        public string PhoneOrEmail { get; set; }
        public string PhonenOrLocalCode { get; set; }
        public string EmailAddress { get; set; }
        public string SurName { get; set; }
        public string FullName { get; set; }
        public string Bvn { get; set; }
    }

    public class InterBankNameEquiryResponse
    {
        public string ReferenceNumber { get; set; }
        public string AccountName { get; set; }
        public string ResponseCode { get; set; }
        public string BankVerificationNumber { get; set; }
        public string KYCLevel { get; set; }
    }

    public class GetTransactionStatusResponse
    {
        public string Status { get; set; }
        public string FinacleTranID { get; set; }
        public string Stan { get; set; }
        public string TransactionId { get; set; }
    }

   

    public class TokenResponse
    {
        public string Message { get; set; }
    }

    public class BvnValidationResponse
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public BVNDataResponse Data { get; set; }
    }

    public class BVNDataResponse
    {
        public string BVNImage { get; set; }
        public BVNDetailResponse BVN { get; set; }
    }

    public class BVNDetailResponse
    {
        public string ResponseCode { get; set; }
        public string BVN { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string RegistrationDate { get; set; }
        public string EnrollmentBank { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string LevelOfAccount { get; set; }
        public string LgaOfOrigin { get; set; }
        public string LgaOfResidence { get; set; }
        public string MaritalStatus { get; set; }
        public string NIN { get; set; }
        public string NameOnCard { get; set; }
        public string Nationality { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string ResidentialAddress { get; set; }
        public string StateOfOrigin { get; set; }
        public string StateOfResidence { get; set; }
        public string WatchListed { get; set; }
        public string ImageHashValue { get; set; }
    }

    public class DirectCreditResponse
    {
        public string Status { get; set; }
        public string FinacleTranID { get; set; }
        public string Stan { get; set; }
        public string TransactionId { get; set; }
    }

    public class BankListResponse
    {
        public string BankCode { get; set; }
        public string BankIdentifyCode { get; set; }
        public string BankName { get; set; }
        public string ActiveFlag { get; set; }
        public string Category { get; set; }
        public string BankCode2 { get; set; }
        public string BankShortName { get; set; }

    }

    public class CustomerAccountInformation
    {
        public string Name { get; set; }
        public string Balance { get; set; }
        public string Currency { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
    }

    public class AccountValidationResponse
    {
        public string ReferenceId { get; set; }
        public string Details { get; set; }
    }
}
