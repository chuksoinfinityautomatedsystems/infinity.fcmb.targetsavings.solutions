﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.FCMB
{
    public class BvnValidationResponseObject
    {
        public string BVN { get; set; }
        public bool FirstNameMatch { get; set; }
        public bool LastNameMatch { get; set; }
        public bool PhoneNumberMatch { get; set; }

        public BvnValidationResponse Details { get; set; }
    }
}
