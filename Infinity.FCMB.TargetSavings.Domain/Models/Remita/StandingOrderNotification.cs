﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Remita
{
    public class StandingOrderNotification
    {
        public StandingOrderNotification()
        {
            lineItems = new List<LineItem>();
        }
        public string notificationType { get; set; }
        public IList<LineItem> lineItems { get; set; }
    }

    public class LineItem
    {
        public string mandateId { get; set; }
        public string debitDate { get; set; }
        public string requestId { get; set; }
        public string amount { get; set; }
    }
}
