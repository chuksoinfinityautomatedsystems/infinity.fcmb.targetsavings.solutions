﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Remita
{
    public class Mandate
    {
        public string merchantId { get; set; }
        public string serviceTypeId { get; set; }
        public string requestId { get; set; }
        public string hash { get; set; }
        public string payerName { get; set; }
        public string payerEmail { get; set; }
        public string payerPhone { get; set; }
        public string payerBankCode { get; set; }
        public string payerAccount { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string amount { get; set; }
        public string mandateType { get; set; }
        public string maxNoOfDebits { get; set; }
        public string frequency { get; set; }
    }

    public class MandateResponse
    {
        public string statuscode { get; set; }
        public string mandateId { get; set; }
        public string status { get; set; }
        public string requestId { get; set; }
    }

    public class ValidateMandateRequest
    {
        public string mandateId { get; set; }
        public string requestId { get; set; }
    }

    public class ValidateMandateResponse
    {
        public ValidateMandateResponse()
        {
            authParams = new List<ValidateMandateParam>();
        }
        public string remitaTransRef { get; set; }
        public string statuscode { get; set; }
        public string requestId { get; set; }
        public string mandateId { get; set; }
        public string status { get; set; }
        public List<ValidateMandateParam> authParams { get; set; }
        public long TargetGoalId { get; set; }
    }

    public class ValidateMandateParam
    {
        public string param1 { get; set; }
        public string description1 { get; set; }
    }

    public class ValidateMandateRequestOTP
    {
        public ValidateMandateRequestOTP()
        {
            authParams = new List<object>();
        }
        public string remitaTransRef { get; set; }
        public List<object> authParams { get; set; }
    }

    public class ValidateMandateRequestOTPParam1
    {
        public string param1 { get; set; }
        public string value { get; set; }
    }
    public class ValidateMandateRequestOTPParam2
    {
        public string param2 { get; set; }
        public string value { get; set; }
    }
}
