﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Remita
{
    public class DesbitStatusRequest
    {
        public string merchantId { get; set; }
        public string mandateId { get; set; }
        public string hash { get; set; }
        public string requestId { get; set; }
    }
}
