﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Customer
{
    public class TargetSavingsRequest
    {
        public long RequestId
        {
            get;
            set;
        }

        public long TargetGoalId
        {
            get;
            set;
        }

        public int RequestTypeId
        {
            get;
            set;
        }

        public DateTime RequestDate
        {
            get;
            set;
        }

        public string RequestDescription
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }

        public string ApprovalStatus
        {
            get;
            set;
        }

        public string TreatedBy
        {
            get;
            set;
        }

        public DateTime? DateTreated
        {
            get;
            set;
        }

        public int Trial { get; set; }
        public string ProcessingResponse { get; set; }
        [IgnoreDataMember]
        public string HashValue { get; set; }
        public decimal LiquidationAmount { get; set; }
        public TargetGoal TargetGoal { get; set; }
        public Common.TargetSavingsRequestType RequestType { get; set; }
    }

    public class RecentTargetSavingsRequest
    {
        public long RequestId { get; set; }
        public string CustomerName { get; set; }
        public string RequestType { get; set; }
        public DateTime RequestDate { get; set; }
    }

    public class LiquidationTransaction
    {
        public long Id { get; set; }
        public long RequestId { get; set; }        
        public decimal Amount { get; set; }        
        public string Narration1 { get; set; }
        public string Narration2 { get; set; }
        public string TransactionType { get; set; }
        public string TransactionTypeDescription { get; set; }
        public string Status { get; set; }
        public string StatusDescription { get; set; }
        public string ProcessingStatus { get; set; }
        public string ProcessingStatusDescription { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime CreatedDateTime { get; set; }
        public DateTime LastUpdatedDate { get; set; }
        public DateTime LastUpdatedDateTime { get; set; }
        public int Trial { get; set; }
        public string Response { get; set; }
        public string HashValue { get; set; }
    }
}
