﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Customer
{
    public class LiquidationServiceRequest
    {
        public long Id { get; set; }        
        public long TargetId { get; set; }
        public decimal Amount { get; set; }
        public string RequestType { get; set; }
        public DateTime RequestDate { get; set; }
        public DateTime RequestDateTime { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public DateTime? TreatedDate { get; set; }
        public DateTime? TreatedDateTime { get; set; }
        public string ProcessingStatus { get; set; }
        [IgnoreDataMember]
        public string ProcessingResponse { get; set; }
        [IgnoreDataMember]
        public string HashValue { get; set; }
        public int Trial { get; set; }
    }

    public class LiquidationServiceRequestStatistics
    {
        public long TargetId { get; set; }
        public int Count { get; set; }
        public decimal TotalSum { get; set; }
    }
}
