﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Customer
{
    public class TargetDebitAccountProfile
    {
        public long Id { get; set; }
        public long TargetGoalId { get; set; }
        public string MandateId { get; set; }
        public string RequestId { get; set; }
        [IgnoreDataMember]
        public string HashValue { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Status { get; set; }
    }
}
