﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace Infinity.FCMB.TargetSavings.Domain.Models.Customer
{
    public class CustomerProfile
    {
        public CustomerProfile()
        {
            TargetGoals = new List<TargetGoal>();
            
            
        }
        public long CustomerProfileId
        {
            get;
            set;
        }

        public string BiometricVerificationNo
        {
            get;
            set;
        }

        public long ProspectId
        {
            get;
            set;
        }

        public string Surname
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string EmailAddress
        {
            get;
            set;
        }

        public string PhoneNumber
        {
            get;
            set;
        }

        public bool IsFCMBCustomer
        {
            get;
            set;
        }

        public string UserName
        {
            get;
            set;
        }

        public byte[] Password
        {
            get;
            set;
        }

        public string ProfileStatus
        {
            get;
            set;
        }

        public DateTime? DateCreated
        {
            get;
            set;
        }

        public string Address
        {
            get;
            set;
        }

        public string NextOfKinPhone
        {
            get;
            set;
        }

        public string NextOfKinEmail
        {
            get;
            set;
        }

        public string NextOfKinFullName
        {
            get;
            set;
        }

        public DateTime? LastLoginDate
        {
            get;
            set;
        }

        public DateTime? LastPasswordChanged
        {
            get;
            set;
        }

        public string ComputerDetails
        {
            get;
            set;
        }

        public byte[] ProfilePicture
        {
            get;
            set;
        }
        [IgnoreDataMember]
        public string HashValue { get; set; }


        public IList<TargetGoal> TargetGoals { get; set; }
        
    }
}
