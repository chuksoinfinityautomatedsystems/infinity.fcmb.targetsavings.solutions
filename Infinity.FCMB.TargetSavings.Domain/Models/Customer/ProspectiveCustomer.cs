﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Customer
{
    public class ProspectiveCustomer
    {
        public long ProspectId
        {
            get;
            set;
        }

        public string BiometricVerificationNo
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Surname is required")]
        [MaxLength(200,ErrorMessage = "Surname length must not be more than 200 characters")]
        [MinLength(3, ErrorMessage = "Surname length must not be less than 3 characters")]
        public string Surname
        {
            get;
            set;
        }
        [Required(ErrorMessage = "First name is required")]
        [MaxLength(200, ErrorMessage = "First name length must not be more than 200 characters")]
        [MinLength(3, ErrorMessage = "First name length must not be less than 3 characters")]
        public string FirstName
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Email is required")]
        [MaxLength(100, ErrorMessage = "Email length must not be more than 100 characters")]
        [MinLength(11, ErrorMessage = "Email length must not be less than 11 characters")]
        public string EmailAddress
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Phone number is required")]
        [MaxLength(13, ErrorMessage = "Phone  number length must not be more than 13 characters")]
        [MinLength(11, ErrorMessage = "Phone number length must not be less than 11 characters")]
        public string PhoneNumber
        {
            get;
            set;
        }

        public string ReferalCode { get; set; }

        public bool IsFCMBCustomer
        {
            get;
            set;
        }

        public DateTime? DateCreated
        {
            get;
            set;
        }
        public string Status { get; set; }
    }
}
