﻿using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Customer
{
    public class TargetGoal
    {
        public TargetGoal()
        {
            TargetSavingsRequests = new List<TargetSavingsRequest>();
            Transactions = new List<Transaction>();
        }
        public long TargetGoalsId
        {
            get;
            set;
        }

        public long CustomerProfileId
        {
            get;
            set;
        }

        public string GoalName
        {
            get;
            set;
        }

        public string GoalDescription
        {
            get;
            set;
        }

        public DateTime SetUpDate
        {
            get;
            set;
        }

        public decimal TargetAmount
        {
            get;
            set;
        }

        public int TargetFrequency
        {
            get;
            set;
        }

        public decimal TargetFrequencyAmount
        {
            get;
            set;
        }

        public DateTime? LastRunDate
        {
            get;
            set;
        }

        public DateTime? NextRunDate
        {
            get;
            set;
        }

        public decimal GoalBalance
        {
            get;
            set;
        }

        public byte[] TargetGoalPics
        {
            get;
            set;
        }

        public DateTime? GoalStartDate
        {
            get;
            set;
        }

        public DateTime? GoalEndDate
        {
            get;
            set;
        }

        public decimal GoalStartAmount
        {
            get;
            set;
        }

        public string GoalDebitType
        {
            get;
            set;
        }

        public string GoalDebitBank
        {
            get;
            set;
        }

        public string GoalDebitAccountNo
        {
            get;
            set;
        }

        public string GoalStatus
        {
            get;
            set;
        }

        public decimal GoalnterestEarned
        {
            get;
            set;
        }

        public string GoalCardTokenDetails
        {
            get;
            set;
        }
        public string ExtendedGoalStatus { get; set; }
        [IgnoreDataMember]
        public string HashValue { get; set; }
        public string LiquidationAccount { get; set; }
        public string LiquidationAccountBankCode { get; set; }
        public int LiquidationMode { get; set; }
        public bool IsLiquidated { get; set; }
        //public Bank DebitBank { get; set; }
        public CustomerProfile CustomerProfile { get; set; }
        public IList<TargetSavingsRequest> TargetSavingsRequests { get; set; }
        public IList<Transaction> Transactions { get; set; }
        public TargetSavingsFrequency TargetSavingsFrequency { get; set; }

    }
}
