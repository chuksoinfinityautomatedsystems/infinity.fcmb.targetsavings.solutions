﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class Bank
    {
        public Bank()
        {
            //Goals = new List<TargetGoal>();
        }
        public int Id { get; set; }
        public string BankName { get; set; }
        public string Code { get; set; }
        public bool IsActive { get; set; }
        //public List<TargetGoal> Goals { get; set; }
    }
}
