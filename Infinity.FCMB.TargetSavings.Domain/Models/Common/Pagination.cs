﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class Pagination
    {
        public int pageSize { get; set; }
        public long lastIdFetched { get; set; }
        public int pageNumber { get; set; }
        public bool FilterByDateRange { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public object SearchData { get; set; }
    }
}
