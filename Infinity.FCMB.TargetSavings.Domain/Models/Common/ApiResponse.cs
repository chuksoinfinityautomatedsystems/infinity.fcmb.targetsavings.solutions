﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class ApiResponse
    {
        public string ResponseCode
        {
            get;
            set;
        }

        public string ResponseDescription
        {
            get;
            set;
        }

        public string FriendlyMessage
        {
            get;
            set;
        }
    }
}
