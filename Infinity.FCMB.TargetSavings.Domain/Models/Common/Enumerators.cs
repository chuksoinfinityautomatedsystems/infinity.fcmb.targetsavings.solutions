﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class Enumerators
    {
        public enum STATUS
        {
            NEW=0,PENDING=1,PROCESSING=2,PROCESSED=3,SUCCESS=4,FAILED=5,EXCEPTION=6, DELETED=7, COMPLETED=8, TAMPERED_WITH=9
        }
        public enum EXTENDED_STATUS
        {
            NONE,STOP,SUSPEND
        }
        public enum APPROVAL_STATUS
        {
            NEW=0,APPROVED=1,TREATED=2,REJECTED=3
        }
        public enum FLAG_DESCRIPTION
        {
            SUCCESS,FAILED,EXCEPTION, API_EXCEPTION
        }
        public enum PAYMENT_GATEWAY
        {
            PAYSTACK,REMITA,FCMB
        }

        public enum TRANSACTION_TYPE
        {
            CREDIT,DEBIT
        }
        public enum LIQUIDATION_MODE
        {
            FLEXIBLE=2, FIXED=1
        }

        public enum LIQUIDATION_TRANSACTION_MODE
        {
            FLEXIBLE = 1, FIXED = 2,PENAL_CHARGE=3,PENAL_CHARGE_BALANCE=4,FIXED_GOAL_BALANCE=5,
            FULL_FLEXIBLE_CONTRIBUTED=6, FULL_FIXED_CONTRIBUTED = 7, FULL_FLEXIBLE_INTEREST = 8, FULL_FIXED_INTEREST = 9
        }
        public enum LIQUIDATION_REQUEST_TYPE
        {
            FULL_COMPLETED_PRINCIPAL_AND_INTEREST,FULL_COMPLETED_INTEREST,
            //PARTIAL_COMPLETED_PRINCIPAL_AND_INTEREST,PARTIAL_COMPLETED_INTEREST,
            PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST, FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST, FAILED_INTEREST_PAYMENT
        }

        public enum SWEEP_STATUS
        {
            FULL_SWEEP=1, PARTIAL_SWEEP_FIXED_FAILED=2, PARTIAL_SWEEP_FLEXIBLE_FAILED, SWEEP_FAILED
        }
        
    }

    public static class CONSTANT
    {
        public const string EMPTY = "";
        public const object NULL = null;
        public const bool HAS_HEADER = true;
        public const bool NO_HEADER = false;
        public const string POST = "POST";
        public const string PUT = "PUT";
        public const string GET = "GET";
        public const string BANK_DEBIT_TYPE = "2";
        public const string CARD_DEBIT_TYPE = "1";

        public const string SUCCESS_RESPONSE = "00";
        public const string FAILED_RESPONSE = "01";
        public const string VALIDATION_ERROR_RESPONSE = "02";
        public const string EXCEPTION_RESPONSE = "99";
        public const string API_EXCEPTION_RESPONSE = "98";


        public const string CUSTOMER_PROFILE = "CUSTOMER_PROFILE";
        public const string DEBIT_ACCOUNT_PROFILE = "DEBIT_ACCOUNT_PROFILE";
        public const string TARGET_GOAL = "TARGET_GOAL";
        public const string REQUEST = "REQUEST";
        public const string TRANSACTION = "TRANSACTION";
        public const string TRANSACTION_INTEREST = "TRANSACTION_INTEREST";
        public const string LIQUIDATION_TRANSACTION = "LIQUIDATION_TRANSACTION";
        public const string LIQUIDATION_REQUEST = "LIQUIDATION_REQUEST";

        public const string CONTENT_TYPE_XML = "text/xml;charset=\"utf-8\"";
        public const string CONTENT_TYPE_JSON = "application/json";
    }
}
