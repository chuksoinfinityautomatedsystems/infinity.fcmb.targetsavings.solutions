﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class TargetSavingsFrequency
    {
        public TargetSavingsFrequency()
        {
            TargetGoal = new List<TargetGoal>();
        }
        public int Id { get; set; }
        public string Frequency { get; set; }
        public int ValueInDays { get; set; }
        public bool IsActive { get; set; }

        public IList<TargetGoal> TargetGoal { get; set; }
    }
}
