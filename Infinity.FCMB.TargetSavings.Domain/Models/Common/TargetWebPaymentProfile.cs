﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class TargetWebPaymentProfile
    {
        public int Id { get; set; }
        public string InternalValue { get; set; }
        public string ExternalValue { get; set; }
    }
}
