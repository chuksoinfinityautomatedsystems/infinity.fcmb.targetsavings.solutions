﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class TargetSavingsRequestType
    {
        public TargetSavingsRequestType()
        {
            TargetSavingsRequest = new List<TargetSavingsRequest>();
        }
        public int RequestTypeId {get; set;}
        public string RequestType {get; set;}
        public bool IsActive {get; set;}
        public bool IsLiquidation { get; set; }
        public bool IsInterestLiquidation { get; set; }
        public IList<TargetSavingsRequest> TargetSavingsRequest { get; set; }
    }
}
