﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class TargetResultNote
    {
        public int Id { get; set; }
        public string Note { get; set; }
        public bool IsActive { get; set; }
    }
}
