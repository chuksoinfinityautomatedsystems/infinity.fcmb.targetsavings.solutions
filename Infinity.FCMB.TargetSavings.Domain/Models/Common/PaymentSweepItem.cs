﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class PaymentSweepItem
    {
        public long Id { get; set; }
        public string SweepAccount { get; set; }
        public string FlexibleAccount { get; set; }
        public string FixedAccount { get; set; }
        public decimal FlexibleAmount { get; set; }
        public decimal FixedAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime SweepDate { get; set; }
        public DateTime DateSwept { get; set; }
        public DateTime DateTimeSwept { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string SweepType { get; set; }

    }

    public class CardPaymentChargedFeeSweepItem
    {
        public long Id { get; set; }
        public string ExpenseAccount { get; set; }
        public string CollectionAccount { get; set; }     
        public decimal TotalAmount { get; set; }
        public DateTime SweepDate { get; set; }
        public DateTime DateSwept { get; set; }
        public DateTime DateTimeSwept { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }

    }
}
