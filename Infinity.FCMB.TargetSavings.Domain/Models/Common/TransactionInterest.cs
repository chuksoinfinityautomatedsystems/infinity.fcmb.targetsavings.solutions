﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class TransactionInterest
    {
        public long TransactionId
        {
            get;
            set;
        }

        public DateTime? TransactionDate
        {
            get;
            set;
        }

        public decimal GoalBalanceAmount
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public long TargetGoalsId
        {
            get;
            set;
        }

        public string Narration
        {
            get;
            set;
        } 
        public string Status { get; set; }
        public DateTime PostedDate { get; set; }
        [IgnoreDataMember]
        public string HashValue { get; set; }
       
    }

    public class TransactionInterestSummary
    {
        public long TargetGoalsId { get; set; }
        public decimal Amount { get; set; }
    }
}
