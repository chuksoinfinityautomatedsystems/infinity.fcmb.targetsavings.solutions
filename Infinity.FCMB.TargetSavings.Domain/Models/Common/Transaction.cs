﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class Transaction
    {
        public long TransactionId
        {
            get;
            set;
        }

        public DateTime? TransactionDate
        {
            get;
            set;
        }

        public string TransactionType
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public long TargetGoalsId
        {
            get;
            set;
        }

        public string Narration
        {
            get;
            set;
        }

        public string PostedBy
        {
            get;
            set;
        }

        public DateTime? PostedDate
        {
            get;
            set;
        }

        public string ApprovedBy
        {
            get;
            set;
        }

        public DateTime? ApprovedDate
        {
            get;
            set;
        }
        [IgnoreDataMember]
        public string HashValue { get; set; }
        public decimal ChargedFee { get; set; }
        public TargetGoal TargetGoal { get; set; }
    }

    public class RecentTransaction
    {
        public long TransactionId { get; set; }
        public string CustomerName { get; set; }
        public string TargetGoal { get; set; }
        public decimal Amount { get; set; }
        public string TransactionType { get; set; }
    }
}
