﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class AppSettings
    {
        public static string ConnectionString
        {
            get;
            set;
        }

        public static string AuthenticationMode
        {
            get;
            set;
        }
    }
}
