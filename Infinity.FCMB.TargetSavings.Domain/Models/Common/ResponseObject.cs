﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class ResponseObject
    {
        public ApiResponse ApiResponse
        {
            get;
            set;
        }

        public object ResponseItem
        {
            get;
            set;
        }

        public ResponseObject()
        {
            this.ApiResponse = new ApiResponse();
        }
    }
}
