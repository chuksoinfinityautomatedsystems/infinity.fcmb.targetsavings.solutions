﻿using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    public class TransactionSchedule
    {
        public long Id { get; set; }
        public long TargetGoalId { get; set; }
        public string DebitType { get; set; }
        public DateTime RunDate {get; set;}
        public DateTime NextRunDate {get; set;}
        public string Status { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated {get; set;}
        public DateTime DatetimeCreated {get; set;}
        public DateTime? DateProcessed {get; set;}
        public DateTime? DatetimeProcessed {get; set;}
        public string ProcessingStatus { get; set; }
        public string ProcessingResponse { get; set; }
        public int Trials { get; set; }
    }

    public class TransactionScheduleReport
    {
        public long Id { get; set; }
        public long TargetGoalId { get; set; }
        public string DebitType { get; set; }
        public DateTime RunDate { get; set; }
        public DateTime NextRunDate { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DatetimeCreated { get; set; }
        public DateTime? DateProcessed { get; set; }
        public DateTime? DatetimeProcessed { get; set; }
        public string ProcessingStatus { get; set; }
        public string ProcessingResponse { get; set; }
        public int Trials { get; set; }
        public TargetGoal Goal { get; set; }
    }
}
