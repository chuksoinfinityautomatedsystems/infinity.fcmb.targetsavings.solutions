﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Domain.Models.Common
{
    //public class GetNRequestsByCustomerProfileIdApiRequest
    //{
    //    public long CustomerId { get; set; }
    //    public Pagination Pagination { get; set; }

    //}
    public class GetNObjectByIdApiRequest
    {
        public long Id { get; set; }
        public Pagination Pagination { get; set; }       
        

    }


    public class GetDailyProspectsApiRequest
    {
        public DateTime CreatedDate { get; set; }
        public string Status { get; set; }
    }

    public class GetDailyCustomerProfilesApiRequest
    {
        public DateTime CreatedDate { get; set; }
        public string Status { get; set; }
    }

    public class GetDailyTargetsApiRequest
    {
        public DateTime CreatedDate { get; set; }
        public string Status { get; set; }
    }

    public class CustomerProfileApiRequest
    {
        [Required(ErrorMessage = "BVN is required")]
        //[StringLength(10, ErrorMessage = "BVN length must not be more or less than 10 characters")]
        public string BiometricVerificationNo { get; set; }
        [Required(ErrorMessage = "Prospect Id is required")]
        public long ProspectId { get; set; }
        [Required(ErrorMessage = "Surname is required")]
        [MaxLength(200, ErrorMessage = "Surname length must not be more than 200 characters")]
        [MinLength(3, ErrorMessage = "Surname length must not be less than 3 characters")]
        public string Surname { get; set; }
        [Required(ErrorMessage = "First name is required")]
        [MaxLength(200, ErrorMessage = "First name length must not be more than 200 characters")]
        [MinLength(3, ErrorMessage = "First name length must not be less than 3 characters")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Email is required")]
        [MaxLength(100, ErrorMessage = "Email length must not be more than 100 characters")]
        [MinLength(5, ErrorMessage = "Email length must not be less than 5 characters")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Phone number is required")]
        [MaxLength(11, ErrorMessage = "Phone number length must not be more than 11 characters")]
        [MinLength(11, ErrorMessage = "Phone number length must not be less than 11 characters")]
        public string PhoneNumber { get; set; }
        public bool IsFCMBCustomer { get; set; }
        [Required(ErrorMessage = "Username is required")]
        [MaxLength(50, ErrorMessage = "Username length must not be more than 50 characters")]
        [MinLength(10, ErrorMessage = "Username length must not be less than 10 characters")]
        public string UserName
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string ProfileStatus
        {
            get;
            set;
        }


        [Required(ErrorMessage = "Address is required")]
        [MaxLength(500, ErrorMessage = "Address length must not be more than 500 characters")]
        [MinLength(10, ErrorMessage = "Address length must not be less than 10 characters")]
        public string Address
        {
            get;
            set;
        }
        //[Required(ErrorMessage = "Next of kin Phone number is required")]
        [MaxLength(11, ErrorMessage = "Next of kin Phone number length must not be more than 11 characters")]
        //[MinLength(11, ErrorMessage = "Next of kin phone number length must not be less than 11 characters")]
        public string NextOfKinPhone
        {
            get;
            set;
        }
        
        //[Required(ErrorMessage = "Next of kin email is required")]
        [MaxLength(100, ErrorMessage = "Next of kin email length must not be more than 100 characters")]
        //[MinLength(5, ErrorMessage = "Next of kin email length must not be less than 5 characters")]
        public string NextOfKinEmail
        {
            get;
            set;
        }
        //[Required(ErrorMessage = "Next of kin name is required")]
        [MaxLength(200, ErrorMessage = "Next of kin name length must not be more than 200 characters")]
        //[MinLength(3, ErrorMessage = "Next of kin name length must not be less than 3 characters")]
        public string NextOfKinFullName
        {
            get;
            set;
        }
        //[Required(ErrorMessage = "Computer detail is required")]
        [MaxLength(250, ErrorMessage = "Computer detail length must not be more than 250 characters")]
        //[MinLength(3, ErrorMessage = "Computer detail length must not be less than 3 characters")]
        public string ComputerDetails
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Profile picture is required")]
        public string ProfilePicture {
            get;
            set;
        }
    }

    public class CustomerRegistrationStatusApiRequest
    {
        [Required(ErrorMessage = "Email is required")]
        [MaxLength(100, ErrorMessage = "Email length must not be more than 100 characters")]
        [MinLength(5, ErrorMessage = "Email length must not be less than 5 characters")]
        public string EmailAddress { get; set; }
        [Required(ErrorMessage = "Phone number is required")]
        [MaxLength(11, ErrorMessage = "Phone number length must not be more than 11 characters")]
        [MinLength(11, ErrorMessage = "Phone number length must not be less than 11 characters")]
        public string PhoneNumber { get; set; }
    }

    public class TargetGoalApiRequest
    {
        [Required(ErrorMessage = "Customer profile Id is required")]        
        public long CustomerProfileId
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Goal Name is required")]
        [MaxLength(100, ErrorMessage = "Goal name length must not be more than 100 characters")]
        [MinLength(3, ErrorMessage = "Goal name length must not be less than 3 characters")]
        public string GoalName
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Goal description is required")]
        [MaxLength(100, ErrorMessage = "Goal description length must not be more than 100 characters")]
        [MinLength(3, ErrorMessage = "Goal descritpion length must not be less than 3 characters")]
        public string GoalDescription
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Target amount is required")]        
        public decimal TargetAmount 
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Frequency is required")]      
        public int TargetFrequency
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Frequency amount is required")]
        public decimal TargetFrequencyAmount
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Goal balance is required, default value is 0")]        
        public decimal GoalBalance
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Target picture is required")]
        public string TargetGoalPics
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Goal start date is required")]
        public string GoalStartDate
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Goal end date is required")]
        public string GoalEndDate
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Goal start amount is required, default is 0")]
        public decimal GoalStartAmount
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Goal debit type is required")]
        public string GoalDebitType
        {
            get;
            set;
        }

        public string GoalDebitBank
        {
            get;
            set;
        }

        public string GoalDebitAccountNo
        {
            get;
            set;
        }

        public string GoalStatus
        {
            get;
            set;
        }


        public string GoalCardTokenDetails
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Liquidation Account is required")]
        public string LiquidationAccount { get; set; }
        [Required(ErrorMessage = "Liquidation Account Bank Code is required")]
        public string LiquidationAccountBankCode { get; set; }
        [Required(ErrorMessage = "Liquidation Mode Code is required")]
        public int LiquidationMode { get; set; }

    }

    public class TargetGoalPictureApiRequest
    {
        [Required(ErrorMessage = "Customer target Id is required")]
        public long TargetId
        {
            get;
            set;
        }
        
        [Required(ErrorMessage = "Target picture is required")]
        public string TargetGoalPics
        {
            get;
            set;
        }
        

    }

    public class TargetSavingsRequestApiRequest
    {
        [Required(ErrorMessage = "Target goal Id is required")]
        public long TargetGoalId
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Request type is required")]        
        //[MaxLength(100, ErrorMessage = "Request type length must not be more than 100 characters")]
        //[MinLength(3, ErrorMessage = "Request typse length must not be less than 3 characters")]
        public int RequestTypeId
        {
            get;
            set;
        }


        [Required(ErrorMessage = "Request description is required")]
        [MaxLength(200, ErrorMessage = "Request description length must not be more than 200 characters")]
        [MinLength(3, ErrorMessage = "Request descritpion length must not be less than 3 characters")]
        public string RequestDescription
        {
            get;
            set;
        }

        public string Status
        {
            get;
            set;
        }
        public decimal LiquidationAmount { get; set; }
        public bool IsLiquidation { get; set; }

    }

    public class LiquidationAccountDetailApiRequest {
        [Required(ErrorMessage = "Liquidation account is required")]
        public string LiquidationAccount { get; set; }
        [Required(ErrorMessage = "Bank code is required")]
        public string BankCode { get; set; }
        [Required(ErrorMessage = "Goal Id is required")]
        public long TargetGoalId { get; set; }
    }


    public class TreatTargetSavingsRequestApiRequest
    {
        [Required(ErrorMessage = "Request Id is required")]
        public long RequestId
        {
            get;
            set;
        }
        
        [Required(ErrorMessage = "Approval Status is required, must either be APPROVED or REJECTED")]
        public string ApprovalStatus
        {
            get;
            set;
        }
        [Required(ErrorMessage = "Treated By is required")]
        public string TreatedBy
        {
            get;
            set;
        }

    }

    /// <summary>My super duper data</summary>
    public class LiquidationRequestApiRequest
    {
        [Required(ErrorMessage = "Target Goal Id is required")]
        public long TargetGoalId
        {
            get;
            set;
        }
        
        [Required(ErrorMessage = "Request Type is required")]
        public string RequestType
        {
            get;
            set;
        }        
        public decimal Amount
        {
            get;
            set;
        }

    }

    public class TransactionApiRequest
    {
        public string TransactionType
        {
            get;
            set;
        }

        public decimal Amount
        {
            get;
            set;
        }

        public long TargetGoalId
        {
            get;
            set;
        }

        public string Narration
        {
            get;
            set;
        }

        public string PostedBy
        {
            get;
            set;
        }
        public DateTime PostedDate { get; set; }



        public string ApprovedBy
        {
            get;
            set;
        }

        public DateTime ApprovedDate
        {
            get;
            set;
        }
    }

    public class CategoryApiRequest
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Image is required")]
        public string Image { get; set; }
    }

    public class AuthenticateApiRequest
    {
        [Required(ErrorMessage = "Username is required")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string Password { get; set; }
    }

    public class ChangePasswordApiRequest
    {
        [Required(ErrorMessage = "Customer profile id is required")]
        public long CustomerProfileId { get; set; }
        [Required(ErrorMessage = "Old Password is required")]
        public string OldPassword { get; set; }
        [Required(ErrorMessage = "Password is required")]
        public string NewPassword { get; set; }
    }

    public class TargetSavingsRequestTypeApiRequest
    {
        public int RequestTypeId { get; set; }
        [Required(ErrorMessage = "Request Type is required")]
        public string RequestType { get; set; }
        public bool IsActive { get; set; }
        public bool IsLiquidation { get; set; }
    }

    public class TargetSavingsFrequencyApiRequest
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Frequency is required")]
        public string Frequency { get; set; }
        [Required(ErrorMessage = "Value In Days is required")]
        public int ValueInDays { get; set; }
        public bool IsActive { get; set; }
    }

    public class ValidateMandateOTPApiRequest
    {
        [Required(ErrorMessage = "Target Goal Id is required")]
        public long TargetGoalId { get; set; }
        [Required(ErrorMessage = "Remita Trans Ref  is required")]
        public string RemitaTransRef { get; set; }
        [Required(ErrorMessage = "OTP is required")]
        public string Otp { get; set; }
        public string CardLast4Digit { get; set; }
    }

    public class ValidateAccountOTPApiRequest
    {
        [Required(ErrorMessage = "Account Number is required")]
        public string AccountNumber { get; set; }
        [Required(ErrorMessage = "Reference Id  is required")]
        public string ReferenceId { get; set; }
        [Required(ErrorMessage = "OTP is required")]
        public string Otp { get; set; }
        [Required(ErrorMessage = "Customer Profile Id is required")]
        public long CustomerProfileId { get; set; }
    }

    public class TopupApiRequest
    {
        [Required(ErrorMessage = "Goal Id is required")]
        public long TargetGoalId { get; set; }
        [Required(ErrorMessage = "Is Card Transaction  is required")]
        public bool IsCardTransaction { get; set; }
        [Required(ErrorMessage = "Amount is required")]
        public decimal Amount { get; set; }
        public string CardToken { get; set; }
    }

    public class NameEnquiryApiRequest
    {
        [Required(ErrorMessage = "Account number is required")]
        public string AccountNumber { get; set; }
        [Required(ErrorMessage = "Bank code is required")]
        public string BankCode { get; set; }
        
    }

    public class ValidateBVNApiRequest
    {
        [Required(ErrorMessage = "BVN is required")]
        public string Bvn { get; set; }
        [Required(ErrorMessage = "First name is required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Phone number is required")]
        [MaxLength(11, ErrorMessage = "Phone number length must not be more than 11 characters")]
        [MinLength(11, ErrorMessage = "Phone number length must not be less than 11 characters")]
        public string PhoneNumber { get; set; }

    }
}
