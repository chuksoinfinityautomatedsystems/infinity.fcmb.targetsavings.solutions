﻿using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Data;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Services.Implementation;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Configuration;
using System.ServiceProcess;
using System.Timers;

namespace Infinity.FCMB.TargetSavings.MessagingService
{
    public partial class TSAMessagingService : ServiceBase
    {
        private Timer timer;
        private IApplicationLogService Processor;
        private double timerWakeUpFrequence;
        string FileLogPath = "";
        private bool busyProcessing;
        public TSAMessagingService()
        {
            InitializeComponent();
            try
            {
                busyProcessing = false;


                RegisterService();
                //PND Notification service
                string Timer_WakeUpFrequence_freq = ConfigurationManager.AppSettings["Timer_WakeUpFrequence"];
                FileLogPath = ConfigurationManager.AppSettings["FileLogPath"];
                if (!Double.TryParse(Timer_WakeUpFrequence_freq, out timerWakeUpFrequence)) timerWakeUpFrequence = 10000;
                timerWakeUpFrequence *= 1000;
                //-------------------------
            }
            catch (Exception ex)
            {
            }


            timer = new Timer(timerWakeUpFrequence);
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);

            Processor.AddToFile("TSAMessagingService Initialized", "TSAMessagingService", FileLogPath);
        }
        private void RegisterService()
        {
            AppSettings.ConnectionString = ConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;
            var serviceProvider = new ServiceCollection()
            .AddScoped(provider =>
            {
                return new TargetSavingsDBContext(AppSettings.ConnectionString);
            })
            .AddScoped<IUnitOfWork, TargetSavingsDBContext>()
            .AddScoped<ICustomerService, CustomerService>()
            .AddScoped<IApplicationLogService, ApplicationLogService>()
            .AddScoped<ITransactionService, TransactionService>()
            .AddScoped<IUtilityService, UtilityService>()
            .BuildServiceProvider();

            Processor = serviceProvider.GetService<IApplicationLogService>();
        }

        private void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Processor.AddToFile("TSAMessagingService timer_Elapsed", "TSAMessagingService", FileLogPath);
            if (busyProcessing == false)
            {
                RegisterService();
                //Processor = serviceProvider.GetService<IApplicationLogService>();
                busyProcessing = true;
                Processor.AddToFile("TSAMessagingService started running", "TSAMessagingService", FileLogPath);
                bool flag=Processor.SendPendingEmails();
                //System.Threading.Thread.Sleep(15000);
                if(flag) busyProcessing = false;
                
                Processor.AddToFile("TSAMessagingService stopped running, Busy Flag:"+ busyProcessing, "TSAMessagingService", FileLogPath);
            }
            else
            {
                Processor.AddToFile("TSAMessagingService still busy running, Busy Flag:" + busyProcessing, "TSAMessagingService", FileLogPath);
            }
        }

        protected override void OnStart(string[] args)
        {
            timer.Start();

            
            Processor.AddToFile("TSAMessagingService Started", "TSAMessagingService", FileLogPath);
        }

        protected override void OnStop()
        {
            timer.Stop();
            Processor = null;
        }
    }
}
