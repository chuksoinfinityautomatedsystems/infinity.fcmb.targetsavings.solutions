// import { Component } from "@angular/core";
// import { Http } from "@angular/http";
// import { FormBuilder, FormGroup, Validators } from "@angular/forms";
// import { User } from "../models/User";
// import { EmailValidator } from "../validators/EmailValidator";
// import { PhoneNumberValidator } from "../validators/PhoneNumberValidator";
// import { RequiredNonZeroIntegerValidator } from "../validators/RequiredNonZeroIntegerValidator";
// import { UsermanagementService } from "../services/usermanagement.service"
// import { IJsonResponse } from "../interfaces/IJsonResponse"
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// @Component({
//     selector: "ng-app",
//     templateUrl: "Assets/html/manage-user.html",
//     providers: [UsermanagementService]
// })
// export class AppComponent {
//     users: Array<User>;
//     newUser: User;
//     showNewUserForm: boolean;
//     showLoading: boolean;
//     newUserFormGroup: FormGroup;
//     private userService: UsermanagementService
//     constructor(formBuilder: FormBuilder, userService: UsermanagementService) {
//         this.users = new Array<User>();
//         this.showNewUserForm = false;
//         this.showLoading = false;
//         this.userService = userService;
//         this.newUserFormGroup = formBuilder.group({
//             firstname: ['', Validators.compose([
//                 Validators.required, Validators.maxLength(20)
//             ])],
//             lastname: ['', Validators.compose([
//                 Validators.required, Validators.maxLength(20)
//             ])],
//             email: ['', Validators.compose([
//                 Validators.required, Validators.maxLength(20), EmailValidator.isValid
//             ])],
//             password: ['', Validators.required],
//             primary_phone: ['', Validators.compose([
//                 Validators.maxLength(20), PhoneNumberValidator.isValid
//             ])],
//             alternate_phone: ['', Validators.compose([
//                 Validators.maxLength(20), PhoneNumberValidator.isValid
//             ])],
//             address1: ['', Validators.compose([
//                 Validators.required, Validators.maxLength(25)
//             ])],
//             address2: ['', Validators.maxLength(25)],
//             city: ['', Validators.compose([
//                 Validators.required, Validators.maxLength(20)
//             ])],
//             state_id: ['', Validators.compose([
//                 Validators.required, RequiredNonZeroIntegerValidator.isValid
//             ])],
//             zip_code: ['', Validators.compose([
//                 Validators.required, Validators.maxLength(10)
//             ])],
//             date_of_birth: ['', Validators.compose([
//                 Validators.required, Validators.maxLength(20)
//             ])]
//         });
//     }
//     AddNewUser(): void {
//         this.showNewUserForm = true;
//         this.newUser = new User();
//         this.newUser.state_id = 0;
//     }
//     SaveUser() {
//         if (this.newUserFormGroup.valid) {
//             this.showLoading = true;
//             this.userService.postUser(this.newUserFormGroup.value)
//                 .subscribe(
//                 (response: IJsonResponse<User, string>) => {
//                     if (response.Success) {
//                         this.users.push(<User>response.Message);
//                     }
//                     else {
//                         if (response.MessageList.length > 1) {
//                             console.log(response.MessageList);
//                         }
//                         else {
//                             console.log(response.Message);
//                         }
//                     }
//                     this.showLoading = false;
//                 },
//                 (error: any) => {
//                     console.log(`Error thrown ${error}`);
//                     this.showLoading = false;
//                 },
//                 () => {
//                     console.log(`Ajax complete.`);
//                     this.showLoading = false;
//                 }
//                 )
//         }
//         return false;
//     }
//     HideNewUserForm(): void {
//         this.showNewUserForm = false;
//     }
// }
const core_1 = require("@angular/core");
const authentication_service_1 = require("../commons/services/authentication.service");
const router_1 = require("@angular/router");
let AppComponent = class AppComponent {
    constructor(authService, _router) {
        this.authService = authService;
        this._router = _router;
        if (this.authService.isLoggedIn) {
            this.showLogoutLink = true;
        }
    }
    Logout() {
        this.authService.logout();
        this.showLogoutLink = false;
        this._router.navigate(['login']);
        return false;
    }
    ngOnInit() {
        this.authService.isLoggedInStatusEvent.
            subscribe(value => {
            this.showLogoutLink = value;
        });
    }
};
AppComponent = __decorate([
    core_1.Component({
        selector: "ng-app",
        templateUrl: "Assets/html/app.html",
    }), 
    __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, router_1.Router])
], AppComponent);
exports.AppComponent = AppComponent;
var _a;
//# sourceMappingURL=app.component.js.map