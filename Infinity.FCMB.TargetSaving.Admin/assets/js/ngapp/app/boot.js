"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const platform_browser_1 = require('@angular/platform-browser');
const forms_1 = require('@angular/forms');
const http_1 = require('@angular/http');
const router_1 = require('@angular/router');
const usermanagement_module_1 = require("./UserManagement/usermanagement.module");
const static_module_1 = require("./static/static.module");
const app_component_1 = require('./app.component');
const usermanagement_routing_1 = require('./UserManagement/usermanagement.routing');
const static_routing_1 = require('./static/static.routing');
const app_routing_1 = require('./app.routing');
let AppModule = class AppModule {
};
AppModule = __decorate([
    core_1.NgModule({
        imports: [platform_browser_1.BrowserModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule, usermanagement_module_1.UserManagementModule,
            static_module_1.StaticPagesModule, usermanagement_routing_1.usermanagementRoute, static_routing_1.staticPageRoutes, app_routing_1.globalRouteAsax],
        declarations: [app_component_1.AppComponent],
        providers: [],
        bootstrap: [app_component_1.AppComponent]
    }), 
    __metadata('design:paramtypes', [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=boot.js.map