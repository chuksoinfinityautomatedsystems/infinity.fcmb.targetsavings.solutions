"use strict";
class PhoneNumberValidator {
    static isValid(formControl) {
        let phoneRegex = /^[0-9+][ ]*[0-9][0-9\s+]+$/i;
        if (formControl.value) {
            let isValid = phoneRegex.test(formControl.value);
            if (isValid === false) {
                return { phoneNumberInvalid: true };
            }
        }
        return null;
    }
}
exports.PhoneNumberValidator = PhoneNumberValidator;
//# sourceMappingURL=PhoneNumberValidator.js.map