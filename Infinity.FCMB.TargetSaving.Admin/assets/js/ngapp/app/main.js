"use strict";
const platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
const core_1 = require('@angular/core');
const environment_1 = require('../environments/environment');
const boot_1 = require('./boot');
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(boot_1.AppModule);
//# sourceMappingURL=main.js.map