"use strict";
const router_1 = require("@angular/router");
const notfound_component_1 = require("./static/components/notfound.component");
exports.globalRouteAsax = router_1.RouterModule.forRoot([
    { path: "**", component: notfound_component_1.NotFoundComponent }
]);
//# sourceMappingURL=app.routing.js.map