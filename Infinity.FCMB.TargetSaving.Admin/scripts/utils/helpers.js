﻿/// <reference path="../vendor/jquery-2.1.1.min.js" />
/// <reference path="../vendor/underscore.js" />
/// <reference path="../vendor/backbone-min.js" />
/// <reference path="../vendor/Backbone.ModelBinder.min.js" />


window.stanbicIBTCExplorer = window.stanbicIBTCExplorer || {};

stanbicIBTCExplorer.helpers = stanbicIBTCExplorer.helpers || {};

(function ($, Backbone, _, toastr, mod) {

    mod.view = mod.view || {};

    $.extend(mod.view, {


        getJson: function (url, requestPayload, done, cancel, options) {
            var self = this;
            if (typeof url === "string") {
                if (url && url.length > 0) {

                    $.getJSON(url, requestPayload, function (data) {
                        if (data != null) {
                            if (typeof done === "function") {
                                if (options != undefined) {
                                    done(options, data);
                                } else {
                                    done(data);
                                }
                            }
                        }
                    }).fail(function (jqXHR, textStatus, errorThrown) {

                        if (jqXHR.getAllResponseHeaders()) {
                            if (self.ajaxUnauthorize(jqXHR.status, jqXHR) === false) { // check if unauthorized access to resource.

                                if (options != undefined) {
                                    cancel(options);
                                } else {
                                    cancel();
                                }
                            }
                        }
                        else { // incase of deliberate request abort.

                            if (options != undefined) {
                                cancel(options);
                            } else {
                                cancel();
                            }
                        }
                    });
                }
            }
            else {
                throw TypeError("Url must be a string");
            }

        },

        postJson: function (url, requestPayload, done, cancel, options) {
            var self = this;
            if (typeof url == "string") {
                if (url && url.length > 0) {

                    $.post(url, requestPayload, function (data) {
                        if (data != null) {
                            if (typeof done === "function") {
                                if (options != undefined) {
                                    done(options, data);
                                } else {
                                    done(data);
                                }
                            }
                        }
                    }, 'json')

                    .fail(function (jqXHR, textStatus, errorThrown) {

                        if (jqXHR.getAllResponseHeaders()) {
                            if (self.ajaxUnauthorize(jqXHR.status, jqXHR) === false) {

                                if (options != undefined) {
                                    cancel(options);
                                } else {
                                    cancel();
                                }
                            }
                        }
                        else {
                            if (options != undefined) {
                                cancel(options);
                            } else {
                                cancel();
                            }
                        }
                    });
                }
            }
            else {
                throw TypeError("Url must be a string");
            }

        },

        ajaxUnauthorize: function (status, xhr) {

            if (status === 200 && xhr != undefined) {
                var authHeader = xhr.getResponseHeader("X-Responded-JSON");

                if (authHeader != undefined) {
                    authHeader = JSON.parse(authHeader);

                    var statusCode = authHeader.status;
                    var location = authHeader.headers.location;

                    if (statusCode === 401 && location !== undefined && location !== null && location.length > 0 && location.toLowerCase().indexOf("returnurl") !== -1) {
                        var returnUrl = window.location.pathname;
                        if (returnUrl.substr(0, 1) === "/") {
                            returnUrl = returnUrl.substr(1);
                        }

                        toastr.info("Session timeout.");
                        window.location.href = "/signin?ReturnUrl=" + returnUrl;
                        return true;
                    }
                }
            }

            return false;
        },

        validateEmail: function (email) {
            var emailRegex = /^[ ]*\w+([-+.']\w+)*@\w+([-.]\\w+)*\.\w+([-.]\w+)*[ ]*$/i;
            return emailRegex.test(email);
        },

        validatePhone: function (phone) {
            var phoneRegex = /^[0-9+][ ]*[0-9][0-9\s+]+$/i;
            phone = $.trim(phone).replace(/\s+/g, '');
            return phoneRegex.test(phone);
        },

        validateDigit: function (digit) {
            var digitRegex = /^\d+$/;
            return digitRegex.test(digit);
        },

        validateRequired: function (value) {
            if (typeof value == "undefined" || value == null || $.trim(value).length < 1) {
                return false;
            }
            return true;
        },

        validateMaxLength: function (value, maximumLength) {

            if (typeof value == 'string' && typeof maximumLength == 'number') {
                return value.length <= maximumLength;
            }
            return false;
        },

        validatePasswordMatch: function (password, confirmPassword) {
            return confirmPassword === password;
        },

        toastErrolist: function (errorList) {
            if (errorList.length > 0) {
                _.forEach(errorList, function (error) {
                    toastr.error(error);
                });
            }
        },

        showValidationIcon: function (parentElement, nameAttributeValue) {
            var control = $(parentElement).find('input[name="' + nameAttributeValue + '"]');

            if (control.length < 1) {
                control = $(parentElement).find('textarea[name="' + nameAttributeValue + '"]');
            }

            if (control.length > 0) {
                $(control).siblings(".error-icon").show();
            }

        },

        hideValidationIcon: function (parentElement, nameAttributeValue) {
            var control = $(parentElement).find('input[name="' + nameAttributeValue + '"]');

            if (control.length < 1) {
                control = $(parentElement).find('textarea[name="' + nameAttributeValue + '"]');
            }

            if (control.length > 0) {
                $(control).siblings(".error-icon").hide();
            }
        },

        hideAllValidationIcon: function (parentElement) {
            $(parentElement).find(".error-icon").hide();
        },

        addValidationIcon: function (nameAttributeValue, parentContainer, hasImmediateParent) {

            var validatIconDiv = $('<div class="validate-icon"><i class="fa fa-times-circle-o error-icon-fill"></i></div>');

            if (hasImmediateParent === true) {
                var inputControl = $(parentContainer).find('input[name="' + nameAttributeValue + '"]');

                if (inputControl != undefined && inputControl !== '') {
                    var immediateParent = $(inputControl).parent();
                    $(validatIconDiv).insertAfter($(immediateParent));
                }
            }
        },

        removeValidationIcon: function () {
            $('.validate-icon').remove();
        },

        removeSingleValidationIconLogin: function (nameAttributeValue, parentContainer, hasImmediateParent) {

            if (hasImmediateParent === true) {
                var inputControl = $(parentContainer).find('input[name="' + nameAttributeValue + '"]');

                if (inputControl != undefined && inputControl !== '') {
                    var immediateParent = $(inputControl).parent();
                    $(immediateParent).siblings(".validate-icon").remove();
                }
            }
        },

        addValidationIconSignup: function (nameAttributeValue, parentContainer, hasImmediateParent) {

            var validatIconDiv = $('<div class="validate-icon-signup"><i class="fa fa-times-circle-o error-icon-fill"></i></div>');

            if (nameAttributeValue === 'email' || nameAttributeValue === 'password' || nameAttributeValue === 'confirmpassword' || nameAttributeValue === 'company_name' || nameAttributeValue === 'company_phone') {
                validatIconDiv = $('<div class="validate-icon"><i class="fa fa-times-circle-o error-icon-fill"></i></div>');
            }


            if (hasImmediateParent === true) {
                var inputControl = $(parentContainer).find('input[name="' + nameAttributeValue + '"]');

                if (inputControl != undefined && inputControl !== '') {
                    var immediateParent = $(inputControl).parent();
                    $(validatIconDiv).insertAfter($(immediateParent));
                }
            }
        },

        addFirstnameValidationIconSignup: function (parentContainer) {

            this.removeFirstnameValidationIconSignup(parentContainer);
            var validateIcon = $('<i class="fa fa-times-circle-o error-icon-fill signup-firstname-icon-error"></i>');
            var inputControl = $(parentContainer).find('input[name="firstname"]');
            $(validateIcon).insertAfter($(inputControl));
        },

        addLastnameValidationIconSignup: function (parentContainer) {
            this.removeLastnameValidationIconSignup(parentContainer);
            var validateIcon = $('<i class="fa fa-times-circle-o error-icon-fill signup-lastname-icon-error"></i>');
            var inputControl = $(parentContainer).find('input[name="lastname"]');
            $(validateIcon).insertAfter($(inputControl));
        },

        removeFirstnameValidationIconSignup: function (parentContainer) {
            var inputControl = $(parentContainer).find('input[name="firstname"]');
            $(inputControl).siblings(".signup-firstname-icon-error").remove();
        },

        removeLastnameValidationIconSignup: function (parentContainer) {
            var inputControl = $(parentContainer).find('input[name="lastname"]');
            $(inputControl).siblings(".signup-lastname-icon-error").remove();
        },

        removeSingleValidationIconSignup: function (nameAttributeValue, parentContainer, hasImmediateParent) {

            if (hasImmediateParent === true) {
                var inputControl = $(parentContainer).find('input[name="' + nameAttributeValue + '"]');

                if (inputControl != undefined && inputControl !== '') {
                    var immediateParent = $(inputControl).parent();

                    if (nameAttributeValue === 'email' || nameAttributeValue === 'password' || nameAttributeValue === 'confirmpassword' || nameAttributeValue === 'company_name' || nameAttributeValue === 'company_phone') {
                        $(immediateParent).siblings(".validate-icon").remove();
                    } else {
                        $(immediateParent).siblings(".validate-icon-signup").remove();
                    }
                }
            }
        },

        removeValidationIconSignup: function () {
            $('.validate-icon-signup').remove();
            $('.validate-icon').remove();
            $('.signup-firstname-icon-error').remove();
            $('.signup-lastname-icon-error').remove();
        },

        validateModelOnView: function (model, el) {

            if (el === undefined || el === null) {
                throw TypeError('el reference error.');
            }

            if ($(el).length < 1) {
                throw TypeError('el not found in DOM.');
            }


            if (typeof model != "undefined" && model instanceof Backbone.Model) {


                var changedAttrs = model.changed;

                //there will only be one change in the model attributes at a time
                var singleChangedAttr = "";

                // this will end up with a single loop towards the changed attribute.
                for (var attr in changedAttrs) {
                    singleChangedAttr = attr;
                }

                //prevent error messages from showing when validating
                model.toastModelError = false;

                if (model.isValid() === false) {

                    if (model.invalidAttributeList.indexOf(singleChangedAttr) !== -1) {

                        this.showValidationIcon(el, singleChangedAttr);
                        toastr.error(model.errorDictionary[singleChangedAttr]);
                    }
                    else {
                        this.hideValidationIcon(el, singleChangedAttr);
                    }
                }
                else {
                    this.hideAllValidationIcon(el);
                }

                model.toastModelError = true;

            }
            else {
                throw TypeError("model must be a backbone model.");
            }
        },



        placeholderIsSupportedOnInput: function () {
            var tempInputTag = document.createElement('input');
            return ('placeholder' in tempInputTag);

            // tempInputTag will contains properties like formAction, placeholder, formMethod if supported by the browser
            // you can use the snippet below in a browser console to have a view of the properties

            //for (var prop in tempInputTag) {
            //    console.log(prop)
            //}
        },

        fixInputPlaceholder: function () {

            if (this.placeholderIsSupportedOnInput() === false) {

                $("input[placeholder], textarea[placeholder]").each(function (i, e) {
                    if ($(e).val() === "") {
                        $(e).val($(e).attr("placeholder"));
                    }
                    $(e).blur(function () {
                        if ($(this).val() === "")
                            $(this).val($(e).attr("placeholder"));
                    }).focus(function () {
                        if ($(this).val() === $(e).attr("placeholder"))
                            $(this).val("");
                    });
                });
            }
        },

        minimize: function () {
            $('nav#side-menu-ct').removeClass('maximize-side-nav');
            $('div#page-wrapper').removeClass('maximize-page-wrapper');
            $('nav#side-menu-ct').addClass('minimize-side-nav');
            $('div#page-wrapper').addClass('minimize-page-wrapper');

            $('i.side-navs-glyph').addClass('nav-label-icon');
            $('p.nav-label-minimize').show();



        },

        maximize: function () {
            $('body').addClass('body-temp');
            $('nav#side-menu-ct').addClass('maximize-side-nav');
            $('div#page-wrapper').addClass('maximize-page-wrapper');
            $('nav#side-menu-ct').removeClass('minimize-side-nav');
            $('div#page-wrapper').removeClass('minimize-page-wrapper');
            $('body').removeClass('body-temp');


            $('i.side-navs-glyph').removeClass('nav-label-icon');
            $('p.nav-label-minimize').hide();
        },

        minimizeNavbar: function () {
            $('.navbar-minimalize').click();
        },

        initSettingsSubLinksMouseOverOutHandler: function () {

            $("ul#settings-link-ul li").mouseover(function () {
                $(this).find("i.fa").css("color", "#45B5DE");
                $(this).find("a").css("color", "#45B5DE");
            });

            $("ul#settings-link-ul li").mouseout(function () {

                var aTag = $(this).find("a");

                if ($(aTag).hasClass("setting-active-link") === false) {
                    //$(this).find("i.fa").css("color", "#676A6C");
                    //$(this).find("a").css("color", "#676A6C"); // 

                    $(this).find("i.fa").css("color", "#16233a");
                    $(this).find("a").css("color", "#16233a");
                }
            });
        },

        fixPageContainerDimensions: function () {

            var bodyHeight = $('body').height();
            var wrapperHeight = $("#wrapper").height();

            if (bodyHeight < wrapperHeight) {
                $('body').css("height", wrapperHeight + "px");
            } else {
                wrapperHeight = bodyHeight;
                $('#wrapper').css("height", wrapperHeight + "px");
            }

            $('#page-wrapper').css("height", wrapperHeight + "px");
            $('#side-menu-ct').css("height", (wrapperHeight + 9) + "px");

            var windowWidth = $(window).width();

            if (windowWidth <= 1280) {
                windowWidth = 1280;
            }

            $('html').css("width", windowWidth + "px");
            $('body').css("width", windowWidth + "px");
            $('#wrapper').css("width", windowWidth + "px");

            if ($('body').width() <= 1280) {
                this.allowPageMaximization(false);
            } else {
                this.allowPageMaximization(true);
            }
        },

        allowPageMaximization: function (disable) {

            if (disable === false) {
                // if page is in full screen before disabling, minimize page
                if ($('body').hasClass('mini-navbar') === false) {
                    this.minimizeNavbar();
                }

                $('.navbar-minimalize').hide();

                $('.nav-top-header-ct .header-text').addClass('disable-navtop-header-text');

            } else {
                $('.navbar-minimalize').show();
                $('.nav-top-header-ct .header-text').addClass('enable-navtop-header-text');
            }
        },

        setTablePercentageMaxHeight: function (height) {

            var self = this;
            if (height > 1 || height < 0) {
                height = 1;
            }

            var maxHeight = $(window).height() * height;

            $('table').slimscroll({
                height: maxHeight + 'px',
                alwaysVisible: true,
                size: '10px',
                railVisible: false

            });


            $(window).bind("resize", function () {
                self.fixPageContainerDimensions();

                var maxHeight = $(window).height() * height;

                $('table').slimscroll({
                    height: maxHeight + 'px',
                    alwaysVisible: true,
                    size: '10px',
                    railVisible: false
                });
            });
        },

        setSlimScrollPercentageMaxHeight: function (element, height) {

            if (element === undefined || element === null) {
                throw TypeError('element reference error.');
            }

            if ($(element).length < 1) {
                throw TypeError('element not found in DOM.');
            }


            var self = this;

            if (height > 1 || height < 0) {
                height = 1;
            }

            var maxHeight = $(window).height() * height;

            $(element).slimscroll({
                height: maxHeight + 'px',
                alwaysVisible: true,
                size: '10px',
                railVisible: false

            });


            $(window).bind("resize", function () {
                self.fixPageContainerDimensions();

                var maxHeight = $(window).height() * height;

                $(element).slimscroll({
                    height: maxHeight + 'px',
                    alwaysVisible: true,
                    size: '10px',
                    railVisible: false

                });
            });

        },

        removeSlimScroll: function (element) {

            if (element === undefined || element === null) {
                throw TypeError('element reference error.');
            }

            if ($(element).length < 1) {
                throw TypeError('element not found in DOM.');
            }

            $(element).parent().replaceWith($(element));

        },


        adjustSlimScrollBar: function () {

            $('div.slimScrollRail').mouseover();  // if this is not there, the scrollBar will not adjust to the correct position

            var slimScrollRailHeight = $('div.slimScrollRail').height();
            var currentScrollBarHeight = $('div.slimScrollBar').height();

            var newScrollBarAdjustment = slimScrollRailHeight - currentScrollBarHeight - 20; // the scroll bar is 20px less the scrollRail


            $('div.slimScrollBar').css({
                'top': (newScrollBarAdjustment) + 'px'
            });
        },

        setModalTablePercentageMaxHeight: function (el, height) {

            if (height > 1 || height < 0) {
                height = 1;
            }

            var maxHeight = $(el).find('.modal-body').height() * height;

            $('.modal-body table').slimscroll({
                height: maxHeight + 'px',
                alwaysVisible: true,
                size: '10px',
                opacity: .5,
                railVisible: false
            });


            $('.modal-body').bind("load resize scroll", function () {

                var maxHeight = $(el).find('.modal-body').height() * height;

                $('.modal-body table').slimscroll({
                    height: maxHeight + 'px',
                    alwaysVisible: true,
                    size: '10px',
                    opacity: .5,
                    railVisible: false
                });
            });



        },

        adjustModalSlimScrollBar: function () {

            $('.modal-body div.slimScrollRail').mouseover();  // if this is not there, the scrollBar will not adjust to the correct position

            var slimScrollRailHeight = $('.modal-body div.slimScrollRail').height();
            var currentScrollBarHeight = $('.modal-body div.slimScrollBar').height();

            var newScrollBarAdjustment = slimScrollRailHeight - currentScrollBarHeight - 20; // the scroll bar is 20px less the scrollRail


            $('.modal-body div.slimScrollBar').css({
                'top': (newScrollBarAdjustment) + 'px'
            });
        },


        hideLoadMore: function (selectorId) {
            if (selectorId != undefined && selectorId.length > 0) {
                $("#" + selectorId).hide();
            }
            else {
                $('#load-more-btn').hide();
            }
        },

        showLoadMore: function (selectorId) {
            if (selectorId != undefined && selectorId.length > 0) {
                $("#" + selectorId).show();
            }
            else {
                $('#load-more-btn').show();
            }
        },

        hidePageLoading: function (selectorId) {
            if (selectorId != undefined && selectorId.length > 0) {
                $("#" + selectorId).hide();
            }
            else {
                $('#page-loading').hide();
            }
        },

        showPageLoading: function (selectorId) {
            if (selectorId != undefined && selectorId.length > 0) {
                $("#" + selectorId).show();
            }
            else {
                $('#page-loading').show();
            }
        },

        hideModalFormSpinner: function (selectorId) {
            if (selectorId != undefined && selectorId.length > 0) {
                $("#" + selectorId).hide();
            }
            else {
                $('.modal-lightbox .modal-spinner').hide();
            }
        },

        showModalFormSpinner: function (selectorId) {
            if (selectorId != undefined && selectorId.length > 0) {
                $("#" + selectorId).show();
            }
            else {
                $('.modal-lightbox .modal-spinner').show();
            }
        },

        hideViewDataSpinner: function (parentElement, spinner) {
            var _spinner = spinner;

            if (_spinner === undefined || _spinner === null) {

                if (parentElement === undefined || parentElement === null) {
                    throw TypeError('data spinner parentElement reference error.');
                    return;
                }

                _spinner = $(parentElement).find('.view-data-spinner');

                if (_spinner.length < 1) {
                    throw TypeError('data spinner not found.')
                    return;
                }
            }

            $(_spinner).hide();
        },

        showViewDataSpinner: function (parentElement, spinner) {

            var _spinner = spinner;

            if (_spinner === undefined || _spinner === null) {

                if (parentElement === undefined || parentElement === null) {
                    throw TypeError('data spinner parentElement reference error.');
                    return;
                }

                _spinner = $(parentElement).find('.view-data-spinner');

                if (_spinner.length < 1) {
                    throw TypeError('data spinner not found.')
                    return;
                }
            }

            $(_spinner).show();


            return _spinner
        },

        hideModalDataSpinner: function (parentElement, spinner) {
            var _spinner = spinner;

            if (_spinner === undefined || _spinner === null) {

                if (parentElement === undefined || parentElement === null) {
                    throw TypeError('data spinner parentElement reference error.');
                    return;
                }

                _spinner = $(parentElement).find('.modal-data-spinner');

                if (_spinner.length < 1) {
                    throw TypeError('data spinner not found.')
                    return;
                }
            }

            $(_spinner).hide();
        },

        showModalDataSpinner: function (parentElement, spinner) {

            var _spinner = spinner;

            if (_spinner === undefined || _spinner === null) {

                if (parentElement === undefined || parentElement === null) {
                    throw TypeError('data spinner parentElement reference error.');
                    return;
                }

                _spinner = $(parentElement).find('.modal-data-spinner');

                if (_spinner.length < 1) {
                    throw TypeError('data spinner not found.')
                    return;
                }
            }

            $(_spinner).show();
            return _spinner
        },

        showData404: function (selectorId) {
            if (selectorId != undefined && selectorId.length > 0) {
                $("#" + selectorId).show();
            }
            else {
                $('#data-404').show();
            }
        },

        hideData404: function (selectorId) {
            if (selectorId != undefined && selectorId.length > 0) {
                $("#" + selectorId).hide();
            }
            else {
                $('#data-404').hide();
            }
        },

        copyModelAttrs: function (src, dest) {
            if (typeof src != "undefined" && src instanceof Backbone.Model) {

                if (typeof dest != "undefined" && dest instanceof Backbone.Model) {

                    for (attr in src.attributes) {
                        if (src.attributes.hasOwnProperty(attr)) {
                            dest.set(attr, src.get(attr));
                        }
                    }
                }
                else {
                    throw TypeError("dest must be a backbone model.");
                }
            }
            else {
                throw TypeError("src must be a backbone model.");
            }
        },

        copyObjectToModel: function (srcObj, dest) {

            if (typeof srcObj === 'object') {

                if (typeof dest != "undefined" && dest instanceof Backbone.Model) {

                    for (attr in srcObj) {

                        if (attr !== 'cid' && attr !== 'id') {

                            if (dest.attributes.hasOwnProperty(attr) === false) {
                                throw TypeError("object to model attribute mismatch ( " + attr + " is not an attribute of the model)");
                            }
                        }

                        dest.set(attr, srcObj[attr]);
                    }

                    //console.log(dest.get('id'));
                    //console.log(dest.get('cid'));
                }
                else {
                    throw TypeError("dest must be a backbone model.");
                }
            }
            else {
                throw TypeError("srcObj must be an object literal.");
            }
        },

        stickyHeader: function (stickHeaders, targetParent, keyOffsetTopHash, storeOriginalOffsetTop) {

            (function () {

                var $stickies;

                if (typeof stickHeaders === "object" && stickHeaders instanceof jQuery && stickHeaders.length > 0) {

                    $stickies = stickHeaders.each(function () {

                        var $thisStickyHeader = $(this);

                        $thisStickyHeader
                                .data('originalPositionOnPageLoad', $thisStickyHeader.offset().top - 240)
                                .data('originalHeightOnPageLoad', $thisStickyHeader.outerHeight());

                        if (storeOriginalOffsetTop === true) {
                            keyOffsetTopHash[$thisStickyHeader.data('key')] = $thisStickyHeader.offset().top - 240;
                        }

                        console.log(keyOffsetTopHash);
                    });

                    targetParent.off("scroll.stickies").on("scroll.stickies", function (event) {
                        _whenScrolling(event);
                    });
                }


                var _whenScrolling = function (event) {

                    var $scrollTop = $(event.currentTarget).scrollTop();

                    $stickies.each(function (i) {

                        var $thisStickyHeader = $(this),
                            $originalStickyPosition = $thisStickyHeader.data('originalPositionOnPageLoad'),
                            $newScrollPosition,
                            $newStickyHeaderTopPosition = 0,
                            $nextStickyHeader;


                        if ($originalStickyPosition <= $scrollTop) {

                            $newScrollPosition = Math.max(0, $scrollTop - $originalStickyPosition);
                            $nextStickyHeader = $stickies.eq(i + 1);

                            if ($nextStickyHeader.length > 0) {

                                $newStickyHeaderTopPosition = Math.min($newScrollPosition, ($nextStickyHeader.data('originalPositionOnPageLoad') - $originalStickyPosition) - $thisStickyHeader.data('originalHeightOnPageLoad'));

                            } else {
                                if ($newScrollPosition > 0) {
                                    $newStickyHeaderTopPosition = $newScrollPosition;
                                }
                            }

                        } else {

                            $newStickyHeaderTopPosition = 0;
                        }

                        $thisStickyHeader.css('transform', 'translateY(' + $newStickyHeaderTopPosition + 'px)');
                    });
                };


            })();

        },

        removeStickyHeader: function (targetParent) {

            (function () {

                var $stickies;

                if (typeof targetParent === "object" && targetParent instanceof jQuery && targetParent.length > 0) {
                    targetParent.off("scroll.stickies");
                }
            })();
        },

        stickyHeader2: function (stickHeaders, targetParent, keyOffsetTopHash) {

            (function () {

                var $stickies;

                if (typeof stickHeaders === "object" && stickHeaders instanceof jQuery && stickHeaders.length > 0) {

                    $stickies = stickHeaders.each(function () {

                        var $thisStickyHeader = $(this);

                        $thisStickyHeader
                                .data('originalPositionOnPageLoad', keyOffsetTopHash[$thisStickyHeader.data('key')])
                                .data('originalHeightOnPageLoad', $thisStickyHeader.outerHeight());

                    });

                    targetParent.off("scroll.stickies").on("scroll.stickies", function (event) {
                        _whenScrolling(event);
                    });
                }


                var _whenScrolling = function (event) {

                    var $scrollTop = $(event.currentTarget).scrollTop();

                    $stickies.each(function (i) {

                        var $thisStickyHeader = $(this),
                            $originalStickyPosition = $thisStickyHeader.data('originalPositionOnPageLoad'),
                            $newScrollPosition,
                            $newStickyHeaderTopPosition = 0,
                            $nextStickyHeader;


                        if ($originalStickyPosition <= $scrollTop) {

                            $newScrollPosition = Math.max(0, $scrollTop - $originalStickyPosition);
                            $nextStickyHeader = $stickies.eq(i + 1);

                            if ($nextStickyHeader.length > 0) {

                                $newStickyHeaderTopPosition = Math.min($newScrollPosition, ($nextStickyHeader.data('originalPositionOnPageLoad') - $originalStickyPosition) - $thisStickyHeader.data('originalHeightOnPageLoad'));

                            } else {
                                if ($newScrollPosition > 0) {
                                    $newStickyHeaderTopPosition = $newScrollPosition;
                                }
                            }

                        } else {

                            $newStickyHeaderTopPosition = 0;
                        }

                        $thisStickyHeader.css('transform', 'translateY(' + $newStickyHeaderTopPosition + 'px)');
                    });
                };


            })();

        },


    });


    mod.Constants = mod.Constants || {};
    mod.Constants.url = mod.Constants.url || {};

    $.extend(mod.Constants.url, {
         


    });

    mod.Constants.view = mod.Constants.view || {};

    $.extend(mod.Constants.view, {
        currentView: undefined,
        currentModalView: undefined,
        currentGridSubView: undefined,
        currentModel: undefined,
        currentCollection: undefined,
        currentSelectModalView: undefined,
        currentViewAjaxFetch: undefined,
        currentViewCollectionFetch: undefined,
        dataViewMode: {
            Grid: 1,
            List: 2
        }
    });

})($, Backbone, _, toastr, stanbicIBTCExplorer.helpers);