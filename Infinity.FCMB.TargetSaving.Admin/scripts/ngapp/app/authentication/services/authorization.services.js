"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var authentication_service_1 = require("../../authentication/services/authentication.service");
require("rxjs/add/observable/of");
var AuthorizeService = /** @class */ (function () {
    function AuthorizeService(_router, authService) {
        this._router = _router;
        this.authService = authService;
    }
    AuthorizeService.prototype.canActivate = function (route, state) {
        if (!authentication_service_1.AuthenticationService.authUserObj) {
            this._router.navigate(['Login']);
            return false;
        }
        if (localStorage.getItem('X-UserOauthToken')) {
            this.authService.isLoggedIn = true;
            return true;
        }
        else {
            this._router.navigate(['Login'], { queryParams: { returnUrl: state.url } });
            return false;
        }
        // this._router.navigate(['login'], { queryParams: { returnUrl: state.url } });
        // if (this.authService.isLoggedIn) {
        //     console.log(111);
        //     return true;
        // }
        // else {
        //     this._router.navigate(['Login'], { queryParams: { returnUrl: state.url } });
        // }
    };
    ;
    AuthorizeService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router, authentication_service_1.AuthenticationService])
    ], AuthorizeService);
    return AuthorizeService;
}());
exports.AuthorizeService = AuthorizeService;
//# sourceMappingURL=authorization.services.js.map