"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/Rx");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/of");
var header_service_1 = require("../../commons/services/header.service");
var utils_services_1 = require("../../commons/services/utils.services");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var user_model_1 = require("../models/user.model");
var Rx_1 = require("rxjs/Rx");
function _window() {
    // return the global native browser window object
    return window;
}
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(_headerService, http) {
        this._headerService = _headerService;
        this.http = http;
        this._loggedIn = new Rx_1.Subject();
        this.loggedIn$ = this._loggedIn.asObservable();
        if (localStorage.getItem("X-UserOauthToken")) {
            this.isLoggedIn = true;
        }
        else {
            this.isLoggedIn = false;
        }
    }
    AuthenticationService_1 = AuthenticationService;
    AuthenticationService.prototype.loggedIn = function (flag) {
        this._loggedIn.next(flag);
    };
    AuthenticationService.prototype.setAccessToken = function (accessToken) {
        localStorage.setItem("X-UserOauthToken", accessToken);
        this.isLoggedIn = true;
    };
    AuthenticationService.prototype.GetAccessToken = function () {
        if (localStorage.getItem("X-UserOauthToken")) {
            return JSON.parse(localStorage.getItem("X-UserOauthToken"));
            //return JSON.parse(localStorage.getItem("X-InternetBankingLoggedInUserOauthToken")).access_token;
        }
        return "";
    };
    AuthenticationService.prototype.logout = function () {
        this.isLoggedIn = false;
        AuthenticationService_1.authUserObj = undefined;
        localStorage.removeItem("X-UserOauthToken");
        return this.isLoggedIn;
    };
    AuthenticationService.prototype.authenticateUserID = function (user) {
        this.logout();
        if (utils_services_1.UtilService.StringIsNullOrEmpty(user.UserId) || utils_services_1.UtilService.StringIsNullOrEmpty(user.Password)) {
            throw new Error("UserId or password empty.");
        }
        var _user = new user_model_1.User();
        _user.UserId = user.UserId;
        _user.Password = btoa(user.Password);
        this.isLoggedIn = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, _user, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                // console.log("Authentication Login: " + _response)
                // console.log("Authentication Login JSON: " + _response.json())
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                responseDescription: "",
                ResponseFriendlyMessage: "Username or password is incorrect.",
                AccessToken: "FGNGFG"
            });
        }
    };
    // getSecurityQuestions() {
    //     if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
    //         return this.http.post(Appsettings.GET_SECURITY_QUESTIONS_URL, null,
    //             this._headerService.getRequestJsonHeaders()
    //         ).map((_response: Response) => {
    //             return _response.json();
    //         });
    //     }
    //     else {
    //         return Observable.of({
    //             ResponseCode: ResponseCodes.SUCCESS,
    //             SessionId: "",
    //             ResponseFriendlyMessage: "",
    //             questionDetailList: [
    //                 {
    //                     "questionId": "1",
    //                     "questionDescription": "What's your dream job.",
    //                     "answer": ""
    //                 },
    //                 {
    //                     "questionId": "2",
    //                     "questionDescription": "What's your spouse name",
    //                     "answer": ""
    //                 },
    //                 {
    //                     "questionId": "3",
    //                     "questionDescription": "What's your first boss name.",
    //                     "answer": ""
    //                 },
    //                 {
    //                     "questionId": "4",
    //                     "questionDescription": "What your favorite soccer club.",
    //                     "answer": ""
    //                 }
    //             ]
    //         });
    //     }
    // }
    AuthenticationService.prototype.stopAutoLogout = function () {
        if ($('body').off) {
            $('body').off("mousemove");
            $('body').off("mousedown");
            $('body').off("keypress");
            $('body').off("DOMMouseScroll");
            $('body').off("mousewheel");
            $('body').off("touchmove");
            $('body').off("MSPointerMove");
            $('body').off("pointermove");
        }
        else if ($('body').unbind) {
            $('body').unbind("mousemove");
            $('body').unbind("mousedown");
            $('body').unbind("keypress");
            $('body').unbind("DOMMouseScroll");
            $('body').unbind("mousewheel");
            $('body').unbind("touchmove");
            $('body').unbind("MSPointerMove");
            $('body').unbind("pointermove");
        }
        if (this.timeoutID) {
            _window().clearTimeout(this.timeoutID);
        }
    };
    AuthenticationService.prototype.setupAutoLogout = function () {
        var self = this;
        if ($('body').off) {
            $('body').off("mousemove");
            $('body').off("mousedown");
            $('body').off("keypress");
            $('body').off("DOMMouseScroll");
            $('body').off("mousewheel");
            $('body').off("touchmove");
            $('body').off("MSPointerMove");
            $('body').off("pointermove");
            $('body').off("keyup");
            $('body').off("keydown");
        }
        else if ($('body').unbind) {
            $('body').unbind("mousemove");
            $('body').unbind("mousedown");
            $('body').unbind("keypress");
            $('body').unbind("DOMMouseScroll");
            $('body').unbind("mousewheel");
            $('body').unbind("touchmove");
            $('body').unbind("MSPointerMove");
            $('body').unbind("pointermove");
            $('body').unbind("keyup");
            $('body').unbind("keydown");
        }
        if (self.timeoutID) {
            _window().clearTimeout(self.timeoutID);
        }
        if ($.on) {
            $('body').on("mousemove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("mousedown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("keypress", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("keydown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("keyup", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("DOMMouseScroll", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("mousewheel", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("touchmove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("MSPointerMove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("pointermove", function () {
                self.resetAutoLogoutTimer(self);
            });
        }
        else if ($.bind) {
            $('body').bind("mousemove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("mousedown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("keypress", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("DOMMouseScroll", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("mousewheel", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("touchmove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("MSPointerMove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("pointermove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("keydown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("keyup", function () {
                self.resetAutoLogoutTimer(self);
            });
        }
        self.startAutoLogoutTimer();
    };
    AuthenticationService.prototype.startAutoLogoutTimer = function () {
        var self = this;
        self.timeoutID = _window().setTimeout(function () {
            // EventHandlerService.emitSessionTimeoutEvent();
            //self.logout();
            self.stopAutoLogout();
        }, appsettings_constant_1.Appsettings.autoLogoutTimeInMicrosec);
    };
    AuthenticationService.prototype.resetAutoLogoutTimer = function (self) {
        _window().clearTimeout(self.timeoutID);
        self.startAutoLogoutTimer();
    };
    AuthenticationService = AuthenticationService_1 = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [header_service_1.HeaderService, http_1.Http])
    ], AuthenticationService);
    return AuthenticationService;
    var AuthenticationService_1;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map