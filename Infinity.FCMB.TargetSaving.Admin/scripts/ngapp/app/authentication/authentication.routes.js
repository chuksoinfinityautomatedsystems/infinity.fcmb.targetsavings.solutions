"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var login_component_1 = require("./components/login.component");
exports.authenticationRoutes = router_1.RouterModule.forChild([
    {
        path: "Login",
        component: login_component_1.LoginComponent,
        pathMatch: 'full'
    },
]);
//# sourceMappingURL=authentication.routes.js.map