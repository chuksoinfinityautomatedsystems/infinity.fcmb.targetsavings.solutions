"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var authentication_service_1 = require("../services/authentication.service");
var utils_services_1 = require("../../commons/services/utils.services");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var user_model_1 = require("../models/user.model");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
function _window() {
    return window;
}
var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, activatedRoute, renerer, elementRef, router, authService, cdRef) {
        this.formBuilder = formBuilder;
        this.activatedRoute = activatedRoute;
        this.renerer = renerer;
        this.elementRef = elementRef;
        this.router = router;
        this.authService = authService;
        this.cdRef = cdRef;
        document.body.style.backgroundColor = '#23bde3';
        //this.renerer.setElementStyle(this.elementRef.nativeElement, 'background-color', '#23bde3');
        this.loginFormGroup = formBuilder.group({
            username: ['', forms_1.Validators.required],
            password: ['', forms_1.Validators.required]
        });
        this.formSubmitted = false;
        this.requestOngoing = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.logout();
        this.returnUrlSub = this.activatedRoute.queryParams.subscribe(function (queryParams) {
            _this.returnUrl = queryParams['returnUrl'];
        });
    };
    LoginComponent.prototype.clearFormsubmittedOnFocus = function () {
        this.formSubmitted = false;
    };
    LoginComponent.prototype.loginUser = function () {
        var self = this;
        this.formSubmitted = true;
        this.requestOngoing = true;
        if (this.loginFormGroup.valid) {
            var username = this.loginFormGroup.controls['username'].value;
            var pass = this.loginFormGroup.controls['password'].value;
            if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                var loadingModalInterval_1 = setInterval(function () {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                    self._authenticateUser();
                    _window().clearInterval(loadingModalInterval_1);
                }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
            }
            else {
                self._authenticateUser();
            }
        }
        else {
            this.requestOngoing = false;
        }
    };
    LoginComponent.prototype._authenticateUser = function () {
        var _this = this;
        var self = this;
        self.user = new user_model_1.User();
        self.user.UserId = utils_services_1.UtilService.trim(self.loginFormGroup.controls['username'].value);
        self.user.Password = utils_services_1.UtilService.trim(self.loginFormGroup.controls['password'].value);
        self.loginSub = self.authService.authenticateUserID(self.user)
            .subscribe(function (response) {
            self.requestOngoing = false;
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.user.AccessToken = response.AccessToken;
                self.authService.setAccessToken(self.user.AccessToken);
                authentication_service_1.AuthenticationService.authUserObj = self.user;
                _this.authService.loggedIn(true);
                if (self.returnUrl && self.returnUrl.length > 0) {
                    self.router.navigate([self.returnUrl]);
                }
                else {
                    self.router.navigate([appsettings_constant_1.Appsettings.DASHBOARD_ROUTER_URL]);
                }
            }
            else {
                utils_services_1.UtilService.toastError(response.ResponseFriendlyMessage);
            }
            self.cdRef.detectChanges();
        }, function (error) {
            utils_services_1.UtilService.toastError(appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE);
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        }, function () {
            // self.requestOngoing = false;
            // self.cdRef.detectChanges();
        });
    };
    LoginComponent.prototype.ngOnDestroy = function () {
        if (this.loginSub) {
            this.loginSub.unsubscribe();
        }
        if (this.returnUrlSub) {
            this.returnUrlSub.unsubscribe();
        }
    };
    LoginComponent = __decorate([
        core_1.Component({
            templateUrl: "html/authentication/login.html",
            styles: ['body {  background-color: #23bde3}']
        }),
        __metadata("design:paramtypes", [forms_1.FormBuilder, router_1.ActivatedRoute, core_1.Renderer,
            core_1.ElementRef,
            router_1.Router, authentication_service_1.AuthenticationService, core_1.ChangeDetectorRef])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map