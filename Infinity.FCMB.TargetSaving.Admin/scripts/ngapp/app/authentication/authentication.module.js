"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var login_component_1 = require("./components/login.component");
var header_service_1 = require("../commons/services/header.service");
var authentication_service_1 = require("./services/authentication.service");
var authorization_services_1 = require("./services/authorization.services");
var AuthenticationPagesModule = /** @class */ (function () {
    function AuthenticationPagesModule() {
    }
    AuthenticationPagesModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule],
            declarations: [login_component_1.LoginComponent],
            providers: [header_service_1.HeaderService, authentication_service_1.AuthenticationService, authorization_services_1.AuthorizeService]
        })
    ], AuthenticationPagesModule);
    return AuthenticationPagesModule;
}());
exports.AuthenticationPagesModule = AuthenticationPagesModule;
//# sourceMappingURL=authentication.module.js.map