"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/Rx");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/of");
var header_service_1 = require("../../commons/services/header.service");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var authentication_service_1 = require("../../authentication/services/authentication.service");
function _window() {
    // return the global native browser window object
    return window;
}
var DashboardService = /** @class */ (function () {
    function DashboardService(authService, _headerService, http) {
        this.authService = authService;
        this._headerService = _headerService;
        this.http = http;
    }
    DashboardService.prototype.getProspectCount = function () {
        return this.http.post(appsettings_constant_1.Appsettings.GET_PROSPECT_COUNT_URL, this._headerService.getRequestJsonHeaders()).map(function (_response) {
            return _response.json();
        });
    };
    DashboardService.prototype.getCustomerProfileCount = function () {
        return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_PROFILE_COUNT_URL, this._headerService.getRequestJsonHeaders()).map(function (_response) {
            return _response.json();
        });
    };
    DashboardService.prototype.GetTargetsCount = function () {
        return this.http.post(appsettings_constant_1.Appsettings.GET_TARGET_GOAL_COUNT_URL, this._headerService.getRequestJsonHeaders()).map(function (_response) {
            return _response.json();
        });
    };
    DashboardService.prototype.GetTargetSavingsRequestCountByStatus = function (status) {
        return this.http.post(appsettings_constant_1.Appsettings.GET_TARGET_GOAL_COUNT_URL, JSON.stringify(status), this._headerService.getRequestJsonHeaders()).map(function (_response) {
            return _response.json();
        });
    };
    DashboardService.prototype.GetTopNPendingTransactions = function (pagination) {
        return this.http.post(appsettings_constant_1.Appsettings.GET_RECENT_TRANSACTION_URL, JSON.stringify(pagination), this._headerService.getRequestJsonHeaders()).map(function (_response) {
            return _response.json();
        });
    };
    DashboardService.prototype.GetTopNPendingRequests = function (pagination) {
        return this.http.post(appsettings_constant_1.Appsettings.GET_RECENT_REQUESTS_URL, JSON.stringify(pagination), this._headerService.getRequestJsonHeaders()).map(function (_response) {
            return _response.json();
        });
    };
    DashboardService.prototype.getDashboardSummary = function () {
        // if (UtilService.StringIsNullOrEmpty(accountNo) || UtilService.StringIsNullOrEmpty(startDate) || UtilService.StringIsNullOrEmpty(endDate)) {
        //     throw new Error("Invalid parameters.");
        // }
        // if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
        //     return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
        //         { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
        //         this._headerService.getRequestJsonHeaders()
        //     ).map((_response: Response) => {
        //         return _response.json();
        //     });
        // }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, 
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Summaries: [
                    {
                        num: 1000,
                        type: 'prospect'
                    },
                    {
                        num: 2100,
                        type: 'customer'
                    },
                    {
                        num: 200,
                        type: 'request'
                    },
                    {
                        num: 1500,
                        type: 'goal'
                    }
                ]
            });
        }
    };
    DashboardService.prototype.getDailyIncrementStat = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, 
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Summaries: [
                    {
                        num: 20,
                        type: 'prospect'
                    },
                    {
                        num: 52,
                        type: 'customer'
                    },
                    {
                        num: 21,
                        type: 'goal'
                    }
                ]
            });
        }
    };
    DashboardService.prototype.getRecentTransactions = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, 
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Transactions: [
                    {
                        customerName: "Chuks Popoola",
                        targetGoal: 'Ford Explorer Limited Edition',
                        amount: 700768
                    },
                    {
                        customerName: "Andy Rubin",
                        targetGoal: 'Said Business School',
                        amount: 675029
                    },
                    {
                        customerName: "Sage Rodriguez",
                        targetGoal: 'Wifey Business',
                        amount: 540292
                    },
                    {
                        customerName: "Philip Chaney",
                        targetGoal: 'Europe Tour',
                        amount: 9083927
                    },
                    {
                        customerName: "Alade Bekky",
                        targetGoal: 'Property Purchase',
                        amount: 8927811
                    }
                ]
            });
        }
    };
    DashboardService.prototype.getRecentRequests = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, 
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Requests: [
                    {
                        customerName: "Warren Buffet",
                        requestType: 'Liquidation',
                    },
                    {
                        customerName: "Akande Adebola",
                        requestType: 'Password Reset'
                    },
                    {
                        customerName: "Uju BRed",
                        requestType: 'Wallet Complaint'
                    },
                    {
                        customerName: "Abe Shinzo",
                        requestType: 'Liquidation'
                    },
                    {
                        customerName: "Yinka Coker",
                        requestType: 'Password Reset'
                    }
                ]
            });
        }
    };
    DashboardService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
            header_service_1.HeaderService, http_1.Http])
    ], DashboardService);
    return DashboardService;
}());
exports.DashboardService = DashboardService;
//# sourceMappingURL=dashboard.services.js.map