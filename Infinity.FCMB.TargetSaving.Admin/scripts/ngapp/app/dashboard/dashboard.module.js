"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var common_2 = require("@angular/common");
var dashboard_component_1 = require("./components/dashboard.component");
var header_service_1 = require("../commons/services/header.service");
var utils_services_1 = require("../commons/services/utils.services");
var dashboard_services_1 = require("./services/dashboard.services");
var authentication_service_1 = require("../authentication/services/authentication.service");
var DashboardModule = /** @class */ (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule],
            declarations: [dashboard_component_1.DashboardComponent],
            providers: [authentication_service_1.AuthenticationService, header_service_1.HeaderService, utils_services_1.UtilService, dashboard_services_1.DashboardService, common_2.DecimalPipe]
        })
    ], DashboardModule);
    return DashboardModule;
}());
exports.DashboardModule = DashboardModule;
//# sourceMappingURL=dashboard.module.js.map