"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var dashboard_component_1 = require("./components/dashboard.component");
exports.dashboardRoutes = router_1.RouterModule.forChild([
    { path: "Dashboard", component: dashboard_component_1.DashboardComponent } //, canActivate: [AuthorizeService] }
]);
//# sourceMappingURL=dashboard.routes.js.map