"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var account_details_model_1 = require("../models/account-details.model");
var AccountDetailsModalComponent = (function () {
    function AccountDetailsModalComponent() {
        this.visible = false;
        this.visibleAnimate = false;
        this.obj = new account_details_model_1.AccountDetails();
    }
    AccountDetailsModalComponent.prototype.show = function () {
        var _this = this;
        this.obj = new account_details_model_1.AccountDetails();
        this.visible = true;
        setTimeout(function () { return _this.visibleAnimate = true; }, 100);
    };
    AccountDetailsModalComponent.prototype.hideModal = function () {
        var _this = this;
        this.obj = new account_details_model_1.AccountDetails();
        this.visibleAnimate = false;
        setTimeout(function () { return _this.visible = false; }, 300);
    };
    AccountDetailsModalComponent = __decorate([
        core_1.Component({
            selector: 'ng-account-details-modal',
            templateUrl: "html/account-details.modal.html"
        }), 
        __metadata('design:paramtypes', [])
    ], AccountDetailsModalComponent);
    return AccountDetailsModalComponent;
}());
exports.AccountDetailsModalComponent = AccountDetailsModalComponent;
//# sourceMappingURL=account-details-modal.component.js.map