"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var EmailValidator_1 = require("../../commons/validators/EmailValidator");
var PhoneNumberValidator_1 = require("../../commons/validators/PhoneNumberValidator");
var RequiredNonZeroIntegerValidator_1 = require("../../commons/validators/RequiredNonZeroIntegerValidator");
var request_services_1 = require("../../request/services/request.services");
var utils_services_1 = require("../../commons/services/utils.services");
var request_log_model_1 = require("../../request/models/request-log.model");
var NewLogModalComponent = (function () {
    function NewLogModalComponent(router, requestService, formBuilder) {
        this.router = router;
        this.requestService = requestService;
        this.visible = false;
        this.visibleAnimate = false;
        this.newObj = new request_log_model_1.RequestLog();
        this.showIsSavingSpinner = false;
        this.disableSubmit = false;
        this._formBuilder = formBuilder;
        this.disableDatePicker = true;
        this.currentEmail = this.currentAccountNo = this.currentPhoneNo = "";
        this.newObjFormGroup = formBuilder.group({
            custAccNo: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])],
            isCustomer: [true],
            customerPhoneNo: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(50), PhoneNumberValidator_1.PhoneNumberValidator.isValid
                ])],
            customerEmail: ['', forms_1.Validators.compose([
                    forms_1.Validators.maxLength(200), EmailValidator_1.EmailValidator.isValid
                ])],
            issueType: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            businessOwner: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            requestDescription: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(500)
                ])],
            requestProblemTypeId: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            requestCategoryId: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            // requestMediumId: [0, Validators.compose([
            //     Validators.required, RequiredNonZeroIntegerValidator.isValid
            // ])],
            requestMoodId: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            requestPriorityId: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            expectedResolutionDate: [moment(new Date()).format("DD/MM/YYYY"), forms_1.Validators.compose([
                    forms_1.Validators.required
                ])]
        });
    }
    NewLogModalComponent.prototype.reInitModal = function () {
        this.newObj = new request_log_model_1.RequestLog();
        this.showIsSavingSpinner = false;
        this.disableSubmit = false;
        this.newObjFormGroup = this._formBuilder.group({
            custAccNo: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])],
            isCustomer: [true],
            customerPhoneNo: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(50), PhoneNumberValidator_1.PhoneNumberValidator.isValid
                ])],
            customerEmail: ['', forms_1.Validators.compose([
                    forms_1.Validators.maxLength(200), EmailValidator_1.EmailValidator.isValid
                ])],
            issueType: [1, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            businessOwner: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            requestDescription: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(500)
                ])],
            requestProblemTypeId: [1, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            requestCategoryId: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            // requestMediumId: [0, Validators.compose([
            //     Validators.required, RequiredNonZeroIntegerValidator.isValid
            // ])],
            requestMoodId: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            requestPriorityId: [0, forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            expectedResolutionDate: [moment(new Date()).format("DD/MM/YYYY"), forms_1.Validators.compose([
                    forms_1.Validators.required
                ])]
        });
        this.newObjFormGroup.controls['requestPriorityId'].setValue(0);
        this.newObjFormGroup.controls['requestMoodId'].setValue(0);
        // this.newObjFormGroup.controls['requestMediumId'].setValue(0);
        this.newObjFormGroup.controls['requestCategoryId'].setValue(0);
        this.newObjFormGroup.controls['requestProblemTypeId'].setValue(0);
        this.newObjFormGroup.controls['businessOwner'].setValue(0);
        this.newObjFormGroup.controls['issueType'].setValue(0);
    };
    NewLogModalComponent.prototype.show = function () {
        var _this = this;
        this.visible = true;
        setTimeout(function () { return _this.visibleAnimate = true; }, 100);
        $('#expectedDatePicker').datetimepicker({
            widgetPositioning: {
                horizontal: 'auto',
                vertical: 'bottom'
            },
            showTodayButton: true,
            keepOpen: false,
            format: 'l',
            locale: moment.locale('en-gb'),
            useCurrent: true
        });
        if (this.currentAccountNo != undefined && this.currentAccountNo != "" && utils_services_1.UtilService.trim(this.currentAccountNo).length > 0) {
            this.newObjFormGroup.controls['custAccNo'].setValue(this.currentAccountNo);
            this.newObj.custAccNo = this.currentAccountNo;
        }
        if (this.currentPhoneNo != undefined && this.currentPhoneNo != "" && utils_services_1.UtilService.trim(this.currentPhoneNo).length > 0) {
            this.newObjFormGroup.controls['customerPhoneNo'].setValue(this.currentPhoneNo);
            this.newObj.customerPhoneNo = this.currentPhoneNo;
        }
        if (this.currentEmail != undefined && this.currentEmail != "" && utils_services_1.UtilService.trim(this.currentEmail).length > 0) {
            this.newObjFormGroup.controls['customerEmail'].setValue(this.currentEmail);
            this.newObj.customerEmail = this.currentEmail;
        }
        this.requestService.getCategoryDDL()
            .subscribe(function (response) {
            if (response.success == true) {
                if (!_this.categories) {
                    _this.categories = new Array();
                }
                _this.categories = response.results;
            }
        }, function (error) {
            if (error.status === 400) {
                utils_services_1.UtilService.toastError(error._body);
            }
            else if (error.status === 401) {
                //UtilService.toastInfo(UtilService.authErrorMessage);
                _this.router.navigate(['login'], { queryParams: { returnUrl: "dashboard" } });
            }
            else if (error.status === 500) {
                utils_services_1.UtilService.toastError(error._body);
            }
        });
        this.requestService.getProblemTypesDDL()
            .subscribe(function (response) {
            if (response.success == true) {
                if (!_this.problemTypes) {
                    _this.problemTypes = new Array();
                }
                _this.problemTypes = response.results;
            }
        }, function (error) {
            if (error.status === 400) {
                utils_services_1.UtilService.toastError(error._body);
            }
            else if (error.status === 401) {
                // UtilService.toastInfo(UtilService.authErrorMessage);
                _this.router.navigate(['login'], { queryParams: { returnUrl: "dashboard" } });
            }
            else if (error.status === 500) {
                utils_services_1.UtilService.toastError(error._body);
            }
        });
        this.requestService.getMoodsDDL()
            .subscribe(function (response) {
            if (response.success == true) {
                if (!_this.moods) {
                    _this.moods = new Array();
                }
                _this.moods = response.results;
            }
        }, function (error) {
            if (error.status === 400) {
                utils_services_1.UtilService.toastError(error._body);
            }
            else if (error.status === 401) {
                //UtilService.toastInfo(UtilService.authErrorMessage);
                _this.router.navigate(['login'], { queryParams: { returnUrl: "dashboard" } });
            }
            else if (error.status === 500) {
                utils_services_1.UtilService.toastError(error._body);
            }
        });
        this.requestService.getPrioritiesDDL()
            .subscribe(function (response) {
            if (response.success == true) {
                if (!_this.priorities) {
                    _this.priorities = new Array();
                }
                _this.priorities = response.results;
            }
        }, function (error) {
            if (error.status === 400) {
                utils_services_1.UtilService.toastError(error._body);
            }
            else if (error.status === 401) {
                //UtilService.toastInfo(UtilService.authErrorMessage);
                _this.router.navigate(['login'], { queryParams: { returnUrl: "dashboard" } });
            }
            else if (error.status === 500) {
                utils_services_1.UtilService.toastError(error._body);
            }
        });
    };
    NewLogModalComponent.prototype.hideModal = function () {
        var _this = this;
        this.visibleAnimate = false;
        setTimeout(function () { return _this.visible = false; }, 300);
    };
    NewLogModalComponent.prototype.SaveObject = function () {
        var _this = this;
        if (this.newObjFormGroup.valid) {
            this.showIsSavingSpinner = true;
            this.disableSubmit = true;
            var isCustomer = this.newObjFormGroup.controls['isCustomer'].value;
            this.newObj.isCustomer = 0;
            if (isCustomer == true) {
                this.newObj.isCustomer = 1;
            }
            this.newObj.expectedResolutionDateString = $('#dateme').val().toString();
            this.requestService.postLog(this.newObj)
                .subscribe(function (response) {
                if (response.success == true) {
                    utils_services_1.UtilService.toastSuccess("This log has been added successfully.");
                    _this.reInitModal();
                    _this.hideModal();
                }
                else {
                    if (response.results) {
                        for (var _i = 0, _a = response.results; _i < _a.length; _i++) {
                            var mess = _a[_i];
                            utils_services_1.UtilService.toastError(mess);
                        }
                    }
                }
            }, function (error) {
                if (error.status === 400) {
                    utils_services_1.UtilService.toastError('bad request.');
                }
                else if (error.status === 401) {
                    //UtilService.toastInfo(UtilService.authErrorMessage);
                    _this.router.navigate(['login'], { queryParams: { returnUrl: "dashboard" } });
                }
                else if (error.status === 500) {
                    utils_services_1.UtilService.toastError(error._body);
                }
                _this.showIsSavingSpinner = false;
                _this.disableSubmit = false;
            }, function () {
                _this.showIsSavingSpinner = false;
                _this.disableSubmit = false;
            });
        }
        return false;
    };
    NewLogModalComponent = __decorate([
        core_1.Component({
            selector: 'new-log-modal',
            templateUrl: "html/add-log.modal.html"
        }), 
        __metadata('design:paramtypes', [router_1.Router, (typeof (_a = typeof request_services_1.RequestService !== 'undefined' && request_services_1.RequestService) === 'function' && _a) || Object, forms_1.FormBuilder])
    ], NewLogModalComponent);
    return NewLogModalComponent;
    var _a;
}());
exports.NewLogModalComponent = NewLogModalComponent;
//# sourceMappingURL=add-request-log.component.js.map