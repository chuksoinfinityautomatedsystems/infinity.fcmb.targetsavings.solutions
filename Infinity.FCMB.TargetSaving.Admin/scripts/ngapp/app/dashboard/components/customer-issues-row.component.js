"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var issues_models_1 = require("../models/issues-models");
var CustomerIssuesRowComponent = (function () {
    function CustomerIssuesRowComponent() {
        this.selected = false;
    }
    CustomerIssuesRowComponent.prototype.isSelected = function () {
        this.selected = !this.selected;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', issues_models_1.Issue)
    ], CustomerIssuesRowComponent.prototype, "obj", void 0);
    CustomerIssuesRowComponent = __decorate([
        core_1.Component({
            selector: "ng-account-issue-row",
            templateUrl: "html/customer-issues-row.html"
        }), 
        __metadata('design:paramtypes', [])
    ], CustomerIssuesRowComponent);
    return CustomerIssuesRowComponent;
}());
exports.CustomerIssuesRowComponent = CustomerIssuesRowComponent;
//# sourceMappingURL=customer-issues-row.component.js.map