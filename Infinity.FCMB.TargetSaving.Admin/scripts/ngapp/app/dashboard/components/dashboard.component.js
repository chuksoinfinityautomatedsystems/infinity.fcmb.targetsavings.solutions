"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
// import { EventHandlerService } from "../../commons/services/event_handlers.service";
// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 
var authentication_service_1 = require("../../authentication/services/authentication.service");
var base_component_1 = require("../../commons/components/base.component");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var dashboard_services_1 = require("../services/dashboard.services");
function _window() {
    // return the global native browser window object
    return window;
}
var DashboardComponent = /** @class */ (function (_super) {
    __extends(DashboardComponent, _super);
    function DashboardComponent(authService, dashService, decimalPipe, router, formBuilder, cdRef) {
        var _this = _super.call(this, authService, router, formBuilder, cdRef) || this;
        _this.authService = authService;
        _this.dashService = dashService;
        _this.decimalPipe = decimalPipe;
        _this.router = router;
        _this.formBuilder = formBuilder;
        _this.cdRef = cdRef;
        _this.isSettings = false;
        _this.requestOngoing = false;
        _this.summaryFetched = _this.incrementStatFetched = _this.recentTransactionFetched = _this.recentRequestFetched = false;
        document.title = "Dashboard";
        _this.recentTransactions = new Array();
        _this.recentRequests = new Array();
        document.body.style.backgroundColor = '#eee';
        return _this;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var self = this;
        self.requestOngoing = true;
        self.isSettings = false;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                // self.requestOngoing = false;
                self._getDashboardSummary();
                self.getRecentRequests();
                self.getRecentTransactions();
                self.getDailyStat();
                self.getProspectCount();
                self.getCustomerProfileCount();
                self.GetTargetsCount();
                self.GetTargetSavingsRequestCount();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getDashboardSummary();
            self.getRecentRequests();
            self.getRecentTransactions();
            self.getDailyStat();
            self.getProspectCount();
            self.getCustomerProfileCount();
            self.GetTargetsCount();
            self.GetTargetSavingsRequestCount();
        }
        var allDashboardDataLoadDoneInterval = setInterval(function () {
            if (self.incrementStatFetched && self.recentRequestFetched && self.recentTransactionFetched
                && self.summaryFetched) {
                self.requestOngoing = false;
            }
            _window().clearInterval(allDashboardDataLoadDoneInterval);
        }, 500);
        //  self.SetupTimeout();
    };
    DashboardComponent.prototype.closeSettings = function (event) {
        event.stopPropagation();
        var self = this;
        self.isSettings = false;
        return false;
    };
    DashboardComponent.prototype.openSettings = function (event) {
        event.stopPropagation();
        var self = this;
        self.isSettings = true;
        return false;
    };
    DashboardComponent.prototype.getProspectCount = function () {
        var _this = this;
        var self = this;
        console.log('getProspectCount');
        self.getRecentRequestSub = self.dashService.getProspectCount()
            .subscribe(function (response) {
            console.log('getProspectCount', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                _this.totalNumOfProspects = response.responseItem;
                _this.prospectCount = _this.decimalPipe.transform(response.responseItem);
                _this.hasProspectCount = true;
            }
            else {
                _this.hasProspectCount = false;
            }
            self.recentRequestFetched = true;
            self.cdRef.detectChanges();
        }, function (error) {
            self.recentRequestFetched = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    DashboardComponent.prototype.getCustomerProfileCount = function () {
        var _this = this;
        var self = this;
        console.log('getCustomerProfileCount');
        self.getRecentRequestSub = self.dashService.getCustomerProfileCount()
            .subscribe(function (response) {
            console.log('getCustomerProfileCount', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                _this.totalNumOfCustomers = response.responseItem;
                _this.customerProfileCount = _this.decimalPipe.transform(response.responseItem);
                _this.hasCustomerProfileCount = true;
            }
            else {
                _this.hasCustomerProfileCount = false;
            }
            self.recentRequestFetched = true;
            self.cdRef.detectChanges();
        }, function (error) {
            self.recentRequestFetched = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    DashboardComponent.prototype.GetTargetsCount = function () {
        var _this = this;
        var self = this;
        console.log('GetTargetsCount');
        self.getRecentRequestSub = self.dashService.GetTargetsCount()
            .subscribe(function (response) {
            console.log('GetTargetsCount', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                _this.totalNumOfGoals = response.responseItem;
                _this.targetGoalCount = _this.decimalPipe.transform(response.responseItem);
                _this.hasTargetGoalCount = true;
            }
            else {
                _this.hasTargetGoalCount = false;
            }
            self.recentRequestFetched = true;
            self.cdRef.detectChanges();
        }, function (error) {
            self.recentRequestFetched = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    DashboardComponent.prototype.GetTargetSavingsRequestCount = function () {
        var _this = this;
        var self = this;
        console.log('GetTargetSavingsRequestCount');
        var status = 'NEW';
        self.getRecentRequestSub = self.dashService.GetTargetSavingsRequestCountByStatus(status)
            .subscribe(function (response) {
            console.log('GetTargetSavingsRequestCount', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                _this.totalNumOfRequests = response.responseItem;
                _this.requestCount = _this.decimalPipe.transform(response.responseItem);
                _this.hasRequestCount = true;
            }
            else {
                _this.hasRequestCount = false;
            }
            self.recentRequestFetched = true;
            self.cdRef.detectChanges();
        }, function (error) {
            self.recentRequestFetched = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    DashboardComponent.prototype.goToTargetImages = function (event) {
        console.log('goToTargetImages');
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.SETTINGS_TARGET_IMAGES_ROUTER_URL]);
        return false;
    };
    DashboardComponent.prototype._getDashboardSummary = function () {
        var self = this;
        self.getSummarySub = self.dashService.getDashboardSummary()
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                var summaries = response.Summaries;
                if (summaries && summaries.length > 0) {
                    var prospects = summaries.filter(function (x) { return x.type.toLowerCase() == 'prospect'; });
                    var customers = summaries.filter(function (x) { return x.type.toLowerCase() == 'customer'; });
                    var requests = summaries.filter(function (x) { return x.type.toLowerCase() == 'request'; });
                    var goals = summaries.filter(function (x) { return x.type.toLowerCase() == 'goal'; });
                    if (prospects && prospects.length > 0) {
                        self.totalNumOfProspects = prospects[0].num;
                    }
                    else {
                        self.totalNumOfProspects = 0;
                    }
                    if (customers && customers.length > 0) {
                        self.totalNumOfCustomers = customers[0].num;
                    }
                    else {
                        self.totalNumOfCustomers = 0;
                    }
                    if (requests && requests.length > 0) {
                        self.totalNumOfRequests = requests[0].num;
                    }
                    else {
                        self.totalNumOfRequests = 0;
                    }
                    if (goals && goals.length > 0) {
                        self.totalNumOfGoals = goals[0].num;
                    }
                    else {
                        self.totalNumOfGoals = 0;
                    }
                }
                else {
                    self.totalNumOfGoals = self.totalNumOfRequests = self.totalNumOfProspects = self.totalNumOfCustomers = 0;
                }
            }
            self.summaryFetched = true;
            self.cdRef.detectChanges();
        }, function (error) {
            self.summaryFetched = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    DashboardComponent.prototype.getDailyStat = function () {
        var self = this;
        self.getDailyStatSub = self.dashService.getDailyIncrementStat()
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                var summaries = response.Summaries;
                if (summaries && summaries.length > 0) {
                    var prospects = summaries.filter(function (x) { return x.type.toLowerCase() == 'prospect'; });
                    var customers = summaries.filter(function (x) { return x.type.toLowerCase() == 'customer'; });
                    var goals = summaries.filter(function (x) { return x.type.toLowerCase() == 'goal'; });
                    if (prospects && prospects.length > 0) {
                        self.prospectsDailyIncrementStat = prospects[0].num;
                    }
                    else {
                        self.prospectsDailyIncrementStat = 0;
                    }
                    if (customers && customers.length > 0) {
                        self.customersDailyIncrementStat = customers[0].num;
                    }
                    else {
                        self.customersDailyIncrementStat = 0;
                    }
                    if (goals && goals.length > 0) {
                        self.goalsDailyIncrementStat = goals[0].num;
                    }
                    else {
                        self.goalsDailyIncrementStat = 0;
                    }
                }
                else {
                    self.goalsDailyIncrementStat = self.customersDailyIncrementStat = self.prospectsDailyIncrementStat = 0;
                }
            }
            self.incrementStatFetched = true;
            self.cdRef.detectChanges();
        }, function (error) {
            self.incrementStatFetched = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    DashboardComponent.prototype.getRecentTransactions = function () {
        var self = this;
        console.log('getRecentTransactions');
        var pagination = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 };
        self.getDailyStatSub = self.dashService.GetTopNPendingTransactions(pagination)
            .subscribe(function (response) {
            console.log('getRecentTransactions', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                // if (!self.recentTransactions) {
                //     self.recentTransactions = new Array<RecentTransaction>();
                // }
                self.recentTransactions = response.responseItem.items;
            }
            self.recentTransactionFetched = true;
            self.cdRef.detectChanges();
            self.requestOngoing = false;
        }, function (error) {
            self.recentTransactionFetched = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    DashboardComponent.prototype.getRecentRequests = function () {
        var self = this;
        console.log('getRecentRequests');
        var pagination = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 };
        self.getRecentRequestSub = self.dashService.GetTopNPendingRequests(pagination)
            .subscribe(function (response) {
            console.log('getRecentRequests', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                // if (!self.recentTransactions) {
                //     self.recentTransactions = new Array<RecentTransaction>();
                // }
                self.recentRequests = response.responseItem.items;
            }
            self.recentTransactionFetched = true;
            self.cdRef.detectChanges();
            self.requestOngoing = false;
        }, function (error) {
            self.recentRequestFetched = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    DashboardComponent.prototype.goToProspects = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    };
    DashboardComponent.prototype.goToCustomers = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL]);
        return false;
    };
    DashboardComponent.prototype.goToCustomerRequests = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL]);
        return false;
    };
    DashboardComponent.prototype.goToCustomerGoals = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    };
    DashboardComponent.prototype.goToCustomerTransactions = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    };
    DashboardComponent.prototype.ngOnDestroy = function () {
        if (this.getRecentRequestSub) {
            this.getRecentRequestSub.unsubscribe();
        }
        if (this.getDailyStatSub) {
            this.getDailyStatSub.unsubscribe();
        }
        if (this.getDailyStatSub) {
            this.getDailyStatSub.unsubscribe();
        }
        if (this.getSummarySub) {
            this.getSummarySub.unsubscribe();
        }
    };
    DashboardComponent = __decorate([
        core_1.Component({
            templateUrl: "html/dashboard.html"
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, dashboard_services_1.DashboardService,
            common_1.DecimalPipe,
            router_1.Router, forms_1.FormBuilder, core_1.ChangeDetectorRef])
    ], DashboardComponent);
    return DashboardComponent;
}(base_component_1.BaseComponent));
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map