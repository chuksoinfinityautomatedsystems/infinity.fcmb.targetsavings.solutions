"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var event_handlers_service_1 = require("../../commons/services/event_handlers.service");
var component_event_publisher_enum_1 = require("../../commons/enums/component_event_publisher.enum");
var customer_account_models_1 = require("../models/customer_account.models");
var CustomerAccountRowComponent = (function () {
    function CustomerAccountRowComponent() {
        this.selected = false;
    }
    CustomerAccountRowComponent.prototype.isSelected = function () {
        this.selected = !this.selected;
    };
    CustomerAccountRowComponent.prototype.GetAccountDetails = function () {
        event_handlers_service_1.EventHandlerService.emitAccountNoSelected(component_event_publisher_enum_1.ComponentEventPublisherEnum.AccountDetails, this.obj.accountNo);
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', customer_account_models_1.CustomerAccount)
    ], CustomerAccountRowComponent.prototype, "obj", void 0);
    CustomerAccountRowComponent = __decorate([
        core_1.Component({
            selector: "ng-account-row",
            templateUrl: "html/customer-accounts-row.html"
        }), 
        __metadata('design:paramtypes', [])
    ], CustomerAccountRowComponent);
    return CustomerAccountRowComponent;
}());
exports.CustomerAccountRowComponent = CustomerAccountRowComponent;
//# sourceMappingURL=customer-account-row.component.js.map