"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
// import { EventHandlerService } from "../../commons/services/event_handlers.service";
// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 
var authentication_service_1 = require("../../authentication/services/authentication.service");
var base_component_1 = require("../../commons/components/base.component");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var customer_services_1 = require("../services/customer.services");
function _window() {
    // return the global native browser window object
    return window;
}
var ProspectsComponent = (function (_super) {
    __extends(ProspectsComponent, _super);
    function ProspectsComponent(authService, customerService, router, formBuilder, cdRef) {
        _super.call(this, authService, router, formBuilder, cdRef);
        this.authService = authService;
        this.customerService = customerService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.cdRef = cdRef;
        this.requestOngoing = false;
        document.title = "Prospects";
        this.prospects = new Array();
    }
    ProspectsComponent.prototype.ngOnInit = function () {
        var self = this;
        self.requestOngoing = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self._getProspects();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getProspects();
        }
        //  self.SetupTimeout();
    };
    ProspectsComponent.prototype._getProspects = function () {
        var self = this;
        self.getProspectsSub = self.customerService.getProspects()
            .subscribe(function (response) {
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS &&
                response.responseItem) {
                self.prospects = response.responseItem;
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    ProspectsComponent.prototype.goToProspects = function (event) {
        event.stopPropagation();
        return false;
    };
    ProspectsComponent.prototype.goHome = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    };
    ProspectsComponent.prototype.ngOnDestroy = function () {
        if (this.getProspectsSub) {
            this.getProspectsSub.unsubscribe();
        }
    };
    ProspectsComponent = __decorate([
        core_1.Component({
            templateUrl: "html/customer/prospects.html"
        }), 
        __metadata('design:paramtypes', [authentication_service_1.AuthenticationService, customer_services_1.CustomerService, router_1.Router, forms_1.FormBuilder, core_1.ChangeDetectorRef])
    ], ProspectsComponent);
    return ProspectsComponent;
}(base_component_1.BaseComponent));
exports.ProspectsComponent = ProspectsComponent;
//# sourceMappingURL=prospects.component.1.js.map