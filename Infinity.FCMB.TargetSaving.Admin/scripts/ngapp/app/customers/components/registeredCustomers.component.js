"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
// import { EventHandlerService } from "../../commons/services/event_handlers.service";
// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 
var authentication_service_1 = require("../../authentication/services/authentication.service");
var base_component_1 = require("../../commons/components/base.component");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var customer_services_1 = require("../services/customer.services");
var customer_model_1 = require("../models/customer.model");
function _window() {
    // return the global native browser window object
    return window;
}
var RegisteredCustomersComponent = /** @class */ (function (_super) {
    __extends(RegisteredCustomersComponent, _super);
    function RegisteredCustomersComponent(authService, customerService, router, formBuilder, cdRef) {
        var _this = _super.call(this, authService, router, formBuilder, cdRef) || this;
        _this.authService = authService;
        _this.customerService = customerService;
        _this.router = router;
        _this.formBuilder = formBuilder;
        _this.cdRef = cdRef;
        _this.requestOngoing = false;
        document.title = "Registered Customers";
        _this.customers = new Array();
        _this.showDetailViews = false;
        _this.customer = new customer_model_1.Customer();
        document.body.style.backgroundColor = '#eee';
        return _this;
    }
    RegisteredCustomersComponent.prototype.ngOnInit = function () {
        var self = this;
        self.requestOngoing = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self._getCustomers();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getCustomers();
        }
        //  self.SetupTimeout();
    };
    RegisteredCustomersComponent.prototype._getCustomers = function () {
        var self = this;
        console.log('_getCustomers');
        var pagination = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 };
        self.getCustomersSub = self.customerService.getNRegisteredCustomers(pagination)
            .subscribe(function (response) {
            console.log('_getCustomers', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS &&
                response.responseItem) {
                self.customers = response.responseItem.items;
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    RegisteredCustomersComponent.prototype.goToCustomerDetails = function (customerId) {
        if (this.customers && this.customers.length > 0) {
            var customerSelected = this.customers.filter(function (x) { return x.customerProfileId == customerId; })[0];
            if (customerSelected) {
                this.customer = customerSelected;
                this.showDetailViews = true;
                this.cdRef.detectChanges();
            }
        }
    };
    RegisteredCustomersComponent.prototype.goToProspects = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    };
    RegisteredCustomersComponent.prototype.goToCustomers = function (event) {
        event.stopPropagation();
        return false;
    };
    RegisteredCustomersComponent.prototype.goToCustomerGoals = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    };
    RegisteredCustomersComponent.prototype.goToCustomerRequests = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL]);
        return false;
    };
    RegisteredCustomersComponent.prototype.goToCustomerTransactions = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    };
    RegisteredCustomersComponent.prototype.goBackToList = function () {
        this.customer = new customer_model_1.Customer();
        this.showDetailViews = false;
        this.cdRef.detectChanges();
    };
    RegisteredCustomersComponent.prototype.goHome = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    };
    RegisteredCustomersComponent.prototype.ngOnDestroy = function () {
        if (this.getCustomersSub) {
            this.getCustomersSub.unsubscribe();
        }
    };
    RegisteredCustomersComponent = __decorate([
        core_1.Component({
            templateUrl: "html/customer/registered-customers.html"
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, customer_services_1.CustomerService,
            router_1.Router, forms_1.FormBuilder, core_1.ChangeDetectorRef])
    ], RegisteredCustomersComponent);
    return RegisteredCustomersComponent;
}(base_component_1.BaseComponent));
exports.RegisteredCustomersComponent = RegisteredCustomersComponent;
//# sourceMappingURL=registeredCustomers.component.js.map