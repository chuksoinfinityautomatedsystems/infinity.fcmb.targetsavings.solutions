"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
// import { EventHandlerService } from "../../commons/services/event_handlers.service";
// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 
var authentication_service_1 = require("../../authentication/services/authentication.service");
var utils_services_1 = require("../../commons/services/utils.services");
var base_component_1 = require("../../commons/components/base.component");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var customer_services_1 = require("../services/customer.services");
var request_model_1 = require("../models/request.model");
function _window() {
    // return the global native browser window object
    return window;
}
var CustomerRequestComponent = /** @class */ (function (_super) {
    __extends(CustomerRequestComponent, _super);
    function CustomerRequestComponent(authService, customerService, router, formBuilder, cdRef) {
        var _this = _super.call(this, authService, router, formBuilder, cdRef) || this;
        _this.authService = authService;
        _this.customerService = customerService;
        _this.router = router;
        _this.formBuilder = formBuilder;
        _this.cdRef = cdRef;
        _this.requestOngoing = false;
        document.title = "Customer Requests";
        _this.customerRequests = new Array();
        _this.showDetailViews = false;
        _this.request = new request_model_1.Request();
        _this.tabIndex = 1;
        document.body.style.backgroundColor = '#eee';
        return _this;
    }
    CustomerRequestComponent.prototype.ngOnInit = function () {
        var self = this;
        self.requestOngoing = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self._getPendingCustomerRequests();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getPendingCustomerRequests();
        }
        //  self.SetupTimeout();
    };
    CustomerRequestComponent.prototype._getPendingCustomerRequests = function () {
        var self = this;
        var pagination = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 };
        self.getRequestsSub = self.customerService.getCustomerPendingRequests(pagination)
            .subscribe(function (response) {
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS &&
                response.responseItem) {
                var requests = response.responseItem.items;
                console.log('requests.length', requests.length);
                if (requests.length > 0) {
                    self.customerRequests = requests.filter(function (o) { return o.status === 'NEW'; });
                }
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    CustomerRequestComponent.prototype._getTreatedCustomerRequests = function () {
        var self = this;
        var pagination = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 };
        self.getRequestsSub = self.customerService.getCustomerPendingRequests(pagination)
            .subscribe(function (response) {
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS &&
                response.responseItem) {
                var requests = response.responseItem.items;
                if (requests.length > 0) {
                    self.customerRequests = requests.filter(function (o) { return o.status === 'TREATED'; });
                }
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    CustomerRequestComponent.prototype.GetPendingRequest = function (event) {
        event.stopPropagation();
        var self = this;
        self.requestOngoing = true;
        self.tabIndex = 1;
        self.customerRequests.length = 0;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_2 = setInterval(function () {
                self._getPendingCustomerRequests();
                _window().clearInterval(loadingModalInterval_2);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getPendingCustomerRequests();
        }
    };
    CustomerRequestComponent.prototype.GetTreatedRequest = function (event) {
        event.stopPropagation();
        var self = this;
        self.requestOngoing = true;
        self.tabIndex = 2;
        self.customerRequests.length = 0;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_3 = setInterval(function () {
                self._getTreatedCustomerRequests();
                _window().clearInterval(loadingModalInterval_3);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getTreatedCustomerRequests();
        }
    };
    CustomerRequestComponent.prototype.goToGoalDetails = function (requestId) {
        if (this.customerRequests && this.customerRequests.length > 0) {
            var requestSelected = this.customerRequests.filter(function (x) { return x.requestId == requestId; })[0];
            if (requestSelected) {
                this.request = requestSelected;
                this.showDetailViews = true;
                this.cdRef.detectChanges();
            }
        }
    };
    CustomerRequestComponent.prototype.goBackToList = function () {
        this.request = new request_model_1.Request();
        this.showDetailViews = false;
        this.cdRef.detectChanges();
    };
    CustomerRequestComponent.prototype.treatRequest = function (event) {
        event.stopPropagation();
        var self = this;
        self.requestOngoing = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_4 = setInterval(function () {
                utils_services_1.UtilService.toastSuccess("Request treated successfully.");
                self.showDetailViews = false;
                self.requestOngoing = false;
                _window().clearInterval(loadingModalInterval_4);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            utils_services_1.UtilService.toastSuccess("Requested Treated Successfully.");
            self.showDetailViews = false;
        }
    };
    CustomerRequestComponent.prototype.goHome = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    };
    CustomerRequestComponent.prototype.goToProspects = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    };
    CustomerRequestComponent.prototype.goToCustomers = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL]);
        return false;
    };
    CustomerRequestComponent.prototype.goToCustomerTransactions = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    };
    CustomerRequestComponent.prototype.goToCustomerGoals = function (event) {
        event.stopPropagation();
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    };
    CustomerRequestComponent.prototype.goToCustomerRequests = function (event) {
        event.stopPropagation();
        return false;
    };
    CustomerRequestComponent.prototype.ngOnDestroy = function () {
        if (this.getRequestsSub) {
            this.getRequestsSub.unsubscribe();
        }
    };
    CustomerRequestComponent = __decorate([
        core_1.Component({
            templateUrl: "html/customer/requests.html"
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, customer_services_1.CustomerService,
            router_1.Router, forms_1.FormBuilder, core_1.ChangeDetectorRef])
    ], CustomerRequestComponent);
    return CustomerRequestComponent;
}(base_component_1.BaseComponent));
exports.CustomerRequestComponent = CustomerRequestComponent;
//# sourceMappingURL=request.component.js.map