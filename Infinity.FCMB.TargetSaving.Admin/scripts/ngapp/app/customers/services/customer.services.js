"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/Rx");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/of");
var header_service_1 = require("../../commons/services/header.service");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var authentication_service_1 = require("../../authentication/services/authentication.service");
function _window() {
    // return the global native browser window object
    return window;
}
var CustomerService = /** @class */ (function () {
    function CustomerService(authService, _headerService, http) {
        this.authService = authService;
        this._headerService = _headerService;
        this.http = http;
    }
    CustomerService.prototype.getProspects = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "offline") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_PROSPECTIVE_CUSTOMERS_URL, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                apiResponse: {
                    responseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                    responseDescription: "string",
                    friendlyMessage: "string"
                },
                responseItem: [
                    {
                        prospectId: 1,
                        biometricVerificationNo: '37388337473',
                        surname: "Putin",
                        firstName: 'Donald',
                        emailAddress: 'donald@trump.com',
                        phoneNumber: "234058493984",
                        isFCMBCustomer: 'y',
                        dateCreated: '04-04-2018'
                    },
                    {
                        prospectId: 2,
                        biometricVerificationNo: '54572883',
                        surname: "Messi",
                        firstName: 'Leo',
                        emailAddress: 'leo@messi.com',
                        phoneNumber: "2345783937884",
                        isFCMBCustomer: 'n',
                        dateCreated: '04-04-2018'
                    }
                ]
            });
        }
    };
    CustomerService.prototype.getNRegisteredCustomers = function (pagination) {
        return this.http.post(appsettings_constant_1.Appsettings.GET_REGISTERED_CUSTOMERS_URL, JSON.stringify(pagination), this._headerService.getRequestJsonHeaders()).map(function (_response) {
            return _response.json();
        });
    };
    CustomerService.prototype.getRegisteredCustomers = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "offline") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_REGISTERED_CUSTOMERS_URL, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                apiResponse: {
                    responseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                    responseDescription: "string",
                    friendlyMessage: "string"
                },
                responseItem: [
                    {
                        customerProfileId: 1,
                        biometricVerificationNo: '37388337473',
                        surname: "Putin",
                        firstName: 'Donald',
                        emailAddress: 'donald@trump.com',
                        phoneNumber: "234058493984",
                        isFCMBCustomer: 'y',
                        dateCreated: '04-04-2018',
                        userName: "Putin234",
                        profileStatus: 'Active',
                        address: 'Trump towers, NYC',
                        nextOfKinPhone: "2345478386474",
                        nextOfKinEmail: 'ron@trump.com',
                        nextOfKinFullName: 'Ron Trump',
                        lastLoginDate: '01-04-2018',
                        lastPasswordChanged: "13-02-2018",
                        computerDetails: '23.24.140.2',
                        profilePicture: 'n/a'
                    },
                    {
                        prospectId: 2,
                        biometricVerificationNo: '54572883',
                        surname: "Messi",
                        firstName: 'Leo',
                        emailAddress: 'leo@messi.com',
                        phoneNumber: "2345783937884",
                        isFCMBCustomer: 'n',
                        dateCreated: '04-04-2018',
                        userName: "Putin234",
                        profileStatus: 'Active',
                        address: 'Barcelona, spain',
                        nextOfKinPhone: "2345478386474",
                        nextOfKinEmail: 'ron@messi.com',
                        nextOfKinFullName: 'Ron Messi',
                        lastLoginDate: '21-03-2018',
                        lastPasswordChanged: "13-03-2018",
                        computerDetails: '134.15.17.200',
                        profilePicture: 'n/a'
                    }
                ]
            });
        }
    };
    CustomerService.prototype.getDailyIncrementStat = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, 
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Summaries: [
                    {
                        num: 20,
                        type: 'prospect'
                    },
                    {
                        num: 52,
                        type: 'customer'
                    },
                    {
                        num: 21,
                        type: 'goal'
                    }
                ]
            });
        }
    };
    CustomerService.prototype.getRecentTransactions = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, 
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Transactions: [
                    {
                        customerName: "Chuks Popoola",
                        targetGoal: 'Ford Explorer Limited Edition',
                        amount: 700768
                    },
                    {
                        customerName: "Andy Rubin",
                        targetGoal: 'Said Business School',
                        amount: 675029
                    },
                    {
                        customerName: "Sage Rodriguez",
                        targetGoal: 'Wifey Business',
                        amount: 540292
                    },
                    {
                        customerName: "Philip Chaney",
                        targetGoal: 'Europe Tour',
                        amount: 9083927
                    },
                    {
                        customerName: "Alade Bekky",
                        targetGoal: 'Property Purchase',
                        amount: 8927811
                    }
                ]
            });
        }
    };
    CustomerService.prototype.getRecentRequests = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, 
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Requests: [
                    {
                        customerName: "Warren Buffet",
                        requestType: 'Liquidation',
                    },
                    {
                        customerName: "Akande Adebola",
                        requestType: 'Password Reset'
                    },
                    {
                        customerName: "Uju BRed",
                        requestType: 'Wallet Complaint'
                    },
                    {
                        customerName: "Abe Shinzo",
                        requestType: 'Liquidation'
                    },
                    {
                        customerName: "Yinka Coker",
                        requestType: 'Password Reset'
                    }
                ]
            });
        }
    };
    CustomerService.prototype.getCustomerGoals = function (pagination) {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "offline") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_TARGET_GOAL_URL, JSON.stringify(pagination), this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                apiResponse: {
                    responseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                    responseDescription: "string",
                    friendlyMessage: "string"
                },
                responseItem: [
                    {
                        targetGoalsId: 1,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        goalName: 'Said Business School',
                        goalDescription: "MBA Savings",
                        setupDate: '01-04-2018',
                        targetAmount: 20000000,
                        targetFrequency: 'Daily',
                        targetFrequencyAmount: 3000000,
                        lastRunDate: '05-04-2018',
                        nextRunDate: '09-04-2018',
                        goalBalance: 17000000,
                        targetGoalPics: 'donald@trump.com',
                        goalStartDate: "03-04-2018",
                        goalEndDate: '03-04-2019',
                        goalStartAmount: 3000000,
                        goalDebitType: "Card",
                        goalDebitBank: 'FCMB',
                        goalDebitAccountNo: '4058691276',
                        goalStatus: "Active",
                        goalnterestEarned: 300000,
                        goalCardTokenDetails: '55245263737252'
                    },
                    {
                        targetGoalsId: 2,
                        customerProfileId: 1,
                        customerName: 'Nick Bilton',
                        goalName: 'Silk Road Book',
                        goalDescription: "Get book published by 2019",
                        setupDate: '01-04-2018',
                        targetAmount: 20000000,
                        targetFrequency: 'Monthly',
                        targetFrequencyAmount: 3000000,
                        lastRunDate: '05-04-2018',
                        nextRunDate: '05-05-2018',
                        goalBalance: 17000000,
                        targetGoalPics: 'donald@trump.com',
                        goalStartDate: "03-04-2018",
                        goalEndDate: '03-04-2019',
                        goalStartAmount: 3000000,
                        goalDebitType: "Card",
                        goalDebitBank: 'Bank of New York Mellon',
                        goalDebitAccountNo: '4058691276',
                        goalStatus: "Active",
                        goalnterestEarned: 300000,
                        goalCardTokenDetails: '55245263737252'
                    },
                    {
                        targetGoalsId: 4,
                        customerProfileId: 4,
                        customerName: 'Tinubu Buhari',
                        goalName: '2019 Election',
                        goalDescription: "Savings for 2019 election",
                        setupDate: '01-04-2018',
                        targetAmount: 20000000,
                        targetFrequency: 'Daily',
                        targetFrequencyAmount: 3000000,
                        lastRunDate: '05-04-2018',
                        nextRunDate: '09-04-2018',
                        goalBalance: 17000000,
                        targetGoalPics: 'donald@trump.com',
                        goalStartDate: "03-04-2018",
                        goalEndDate: '03-04-2019',
                        goalStartAmount: 3000000,
                        goalDebitType: "Card",
                        goalDebitBank: 'FCMB',
                        goalDebitAccountNo: '4058691276',
                        goalStatus: "Active",
                        goalnterestEarned: 300000,
                        goalCardTokenDetails: '55245263737252'
                    },
                    {
                        targetGoalsId: 3,
                        customerProfileId: 2,
                        customerName: 'Jack Dorsey',
                        goalName: 'New Fintech Company',
                        goalDescription: "Savings for a new company by 2020",
                        setupDate: '01-04-2018',
                        targetAmount: 20000000,
                        targetFrequency: 'Yearly',
                        targetFrequencyAmount: 3000000,
                        lastRunDate: '05-04-2018',
                        nextRunDate: '05-04-2019',
                        goalBalance: 17000000,
                        targetGoalPics: 'donald@trump.com',
                        goalStartDate: "03-04-2018",
                        goalEndDate: '03-04-2019',
                        goalStartAmount: 3000000,
                        goalDebitType: "Card",
                        goalDebitBank: 'Bank of New York Mellon',
                        goalDebitAccountNo: '4058691276',
                        goalStatus: "Active",
                        goalnterestEarned: 300000,
                        goalCardTokenDetails: '55245263737252'
                    }
                ]
            });
        }
    };
    CustomerService.prototype.getCustomerPendingRequests = function (pagination) {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "offline") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_REQUEST_LIST_URL, JSON.stringify(pagination), this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                apiResponse: {
                    responseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                    responseDescription: "string",
                    friendlyMessage: "string"
                },
                responseItem: [
                    {
                        targetGoalsId: 1,
                        goalName: 'Said Business School',
                        requestId: 1,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        requestType: 'Liquidation',
                        requestDate: '01-04-2018',
                        requestDescription: "Liquidate my target savings",
                        status: "Pending",
                        treatedBy: "B382390 -- Conte Ibrahim",
                        dateTreated: '01-04-2018'
                    },
                    {
                        targetGoalsId: 1,
                        goalName: 'Property Purchase',
                        requestId: 2,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        requestType: 'Liquidation',
                        requestDate: '01-04-2018',
                        requestDescription: "Liquidate my target savings",
                        status: "Pending",
                        treatedBy: "B382390 -- Alan Idowu",
                        dateTreated: '01-04-2018'
                    },
                    {
                        targetGoalsId: 1,
                        goalName: 'New Business Venture',
                        requestId: 3,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        requestType: 'Liquidation',
                        requestDate: '01-04-2018',
                        requestDescription: "Liquidate my target savings",
                        status: "Pending",
                        treatedBy: "B382390 -- Abisoye Coker",
                        dateTreated: '01-04-2018'
                    }
                ]
            });
        }
    };
    CustomerService.prototype.getCustomerTreatedRequests = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_REGISTERED_CUSTOMERS_URL, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                apiResponse: {
                    responseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                    responseDescription: "string",
                    friendlyMessage: "string"
                },
                responseItem: [
                    {
                        targetGoalsId: 1,
                        goalName: 'Said Business School',
                        requestId: 1,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        requestType: 'Liquidation',
                        requestDate: '01-04-2018',
                        requestDescription: "Liquidate my target savings",
                        status: "Treated",
                        treatedBy: "B382390 -- Conte Ibrahim",
                        dateTreated: '01-04-2018'
                    },
                    {
                        targetGoalsId: 1,
                        goalName: 'Property Purchase',
                        requestId: 2,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        requestType: 'Liquidation',
                        requestDate: '01-04-2018',
                        requestDescription: "Liquidate my target savings",
                        status: "Treated",
                        treatedBy: "B382390 -- Alan Idowu",
                        dateTreated: '01-04-2018'
                    },
                    {
                        targetGoalsId: 1,
                        goalName: 'New Business Venture',
                        requestId: 2,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        requestType: 'Liquidation',
                        requestDate: '01-04-2018',
                        requestDescription: "Liquidate my target savings",
                        status: "Treated",
                        treatedBy: "B382390 -- Abisoye Coker",
                        dateTreated: '01-04-2018'
                    }
                ]
            });
        }
    };
    CustomerService.prototype.getPendingCustomerTransactions = function (pagination) {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "offline") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_TRANSACTIONS_URL, JSON.stringify(pagination), this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                apiResponse: {
                    responseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                    responseDescription: "string",
                    friendlyMessage: "string"
                },
                responseItem: [
                    {
                        transactionId: 1,
                        targetGoalsId: 1,
                        goalName: 'Said Business School',
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        transactionType: 'Credit',
                        transactionDate: '01-04-2018',
                        amount: 500000,
                        narration: "Liquidate my target savings",
                        postedBy: "B372883, Kunle Abiodun",
                        postedDate: '01-04-2018',
                        status: 'Pending',
                        approvedBy: "B382390 -- Conte Ibrahim",
                        approvedDate: '05-04-2018'
                    },
                    {
                        transactionId: 2,
                        targetGoalsId: 1,
                        goalName: 'House warming',
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        transactionType: 'Credit',
                        transactionDate: '01-04-2018',
                        amount: 500000,
                        narration: "Liquidate my target savings",
                        postedBy: "B372883, Kunle Abiodun",
                        postedDate: '01-04-2018',
                        status: 'Pending',
                        approvedBy: "B382390 -- Conte Ibrahim",
                        approvedDate: '05-04-2018'
                    }
                ]
            });
        }
    };
    CustomerService.prototype.getApprovedCustomerTransactions = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_REGISTERED_CUSTOMERS_URL, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                apiResponse: {
                    responseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                    responseDescription: "string",
                    friendlyMessage: "string"
                },
                responseItem: [
                    {
                        transactionId: 1,
                        targetGoalsId: 1,
                        goalName: 'Said Business School',
                        requestId: 1,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        transactionType: 'Credit',
                        transactionDate: '01-04-2018',
                        amount: 500000,
                        narration: "Liquidate my target savings",
                        postedBy: "B372883, Kunle Abiodun",
                        postedDate: '01-04-2018',
                        status: 'approved',
                        approvedBy: "B382390 -- Conte Ibrahim",
                        approvedDate: '05-04-2018'
                    },
                    {
                        transactionId: 2,
                        targetGoalsId: 1,
                        goalName: '2019 Election',
                        requestId: 1,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        transactionType: 'Credit',
                        transactionDate: '01-04-2018',
                        amount: 500000,
                        narration: "Liquidate my target savings",
                        postedBy: "System",
                        treatedBy: "System",
                        postedDate: '01-04-2018',
                        approvedBy: "System",
                        approvedDate: '01-04-2018',
                        status: 'Approved'
                    },
                    {
                        transactionId: 3,
                        targetGoalsId: 1,
                        goalName: 'New Venture',
                        requestId: 1,
                        customerProfileId: 1,
                        customerName: 'Donald Trump',
                        transactionType: 'Credit',
                        transactionDate: '01-04-2018',
                        amount: 500000,
                        narration: "Liquidate my target savings",
                        postedBy: "B372883, Kunle Abiodun",
                        postedDate: '01-04-2018',
                        status: 'Approved',
                        approvedBy: "B382390 -- Mo Abudu",
                        approvedDate: '05-04-2018'
                    }
                ]
            });
        }
    };
    CustomerService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService,
            header_service_1.HeaderService, http_1.Http])
    ], CustomerService);
    return CustomerService;
}());
exports.CustomerService = CustomerService;
//# sourceMappingURL=customer.services.js.map