"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var prospects_component_1 = require("./components/prospects.component");
var header_service_1 = require("../commons/services/header.service");
var utils_services_1 = require("../commons/services/utils.services");
var customer_services_1 = require("./services/customer.services");
var authentication_service_1 = require("../authentication/services/authentication.service");
var registeredCustomers_component_1 = require("./components/registeredCustomers.component");
var goal_component_1 = require("./components/goal.component");
var request_component_1 = require("./components/request.component");
var transactions_component_1 = require("./components/transactions.component");
var CustomerModule = /** @class */ (function () {
    function CustomerModule() {
    }
    CustomerModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule],
            declarations: [prospects_component_1.ProspectsComponent, registeredCustomers_component_1.RegisteredCustomersComponent,
                goal_component_1.TargetGoalsComponent, request_component_1.CustomerRequestComponent, transactions_component_1.CustomerTransactionComponent],
            providers: [authentication_service_1.AuthenticationService, header_service_1.HeaderService, utils_services_1.UtilService, customer_services_1.CustomerService]
        })
    ], CustomerModule);
    return CustomerModule;
}());
exports.CustomerModule = CustomerModule;
//# sourceMappingURL=customer.module.js.map