"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var appsettings_constant_1 = require("../commons/constants/appsettings.constant");
var prospects_component_1 = require("./components/prospects.component");
var registeredCustomers_component_1 = require("./components/registeredCustomers.component");
var goal_component_1 = require("./components/goal.component");
var request_component_1 = require("./components/request.component");
var transactions_component_1 = require("./components/transactions.component");
exports.customerRoutes = router_1.RouterModule.forChild([
    {
        path: appsettings_constant_1.Appsettings.PROSPECTS_ROUTER_URL,
        component: prospects_component_1.ProspectsComponent,
        pathMatch: 'full'
    } //, canActivate: [AuthorizeService] }
    ,
    {
        path: appsettings_constant_1.Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL,
        component: registeredCustomers_component_1.RegisteredCustomersComponent,
        pathMatch: 'full'
    },
    {
        path: appsettings_constant_1.Appsettings.CUSTOMERS_GOALS_ROUTER_URL,
        component: goal_component_1.TargetGoalsComponent,
        pathMatch: 'full'
    },
    {
        path: appsettings_constant_1.Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL,
        component: request_component_1.CustomerRequestComponent,
        pathMatch: 'full'
    },
    {
        path: appsettings_constant_1.Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL,
        component: transactions_component_1.CustomerTransactionComponent,
        pathMatch: 'full'
    }
]);
//# sourceMappingURL=customer.routes.js.map