"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var notfound_component_1 = require("./static/components/notfound.component");
exports.rootRoutes = router_1.RouterModule.forRoot([
    { path: "**", component: notfound_component_1.NotFoundComponent }
]);
//# sourceMappingURL=app.routing.js.map