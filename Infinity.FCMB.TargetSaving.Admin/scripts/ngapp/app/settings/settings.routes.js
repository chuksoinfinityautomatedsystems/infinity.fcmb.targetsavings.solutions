"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var appsettings_constant_1 = require("../commons/constants/appsettings.constant");
var target_images_component_1 = require("./components/target-images.component");
var target_frequency_component_1 = require("./components/target-frequency.component");
exports.settingsRoutes = router_1.RouterModule.forChild([
    {
        path: appsettings_constant_1.Appsettings.SETTINGS_TARGET_IMAGES_ROUTER_URL,
        component: target_images_component_1.TargetImagesComponent,
        pathMatch: 'full'
    } //, canActivate: [AuthorizeService] }
    ,
    {
        path: appsettings_constant_1.Appsettings.SETTINGS_TARGET_FREQUENCY_ROUTER_URL,
        component: target_frequency_component_1.TargetFrequencyComponent,
        pathMatch: 'full'
    }
    //,
    // {
    //     path: Appsettings.CUSTOMERS_GOALS_ROUTER_URL,
    //     //component: TargetGoalsComponent,
    //     pathMatch: 'full'
    // },
    // {
    //     path: Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL,
    //     //component: CustomerRequestComponent,
    //     pathMatch: 'full'
    // },
    // {
    //     path: Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL,
    //     //component: CustomerTransactionComponent,
    //     pathMatch: 'full'
    // }
]);
//# sourceMappingURL=settings.routes.js.map