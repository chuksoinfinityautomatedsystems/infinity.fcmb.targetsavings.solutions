"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var authentication_service_1 = require("../../authentication/services/authentication.service");
var base_component_1 = require("../../commons/components/base.component");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var settings_service_1 = require("../services/settings.service");
var platform_browser_1 = require("@angular/platform-browser");
var target_frequency_model_1 = require("../models/target-frequency.model");
function _window() {
    // return the global native browser window object
    return window;
}
var TargetFrequencyComponent = /** @class */ (function (_super) {
    __extends(TargetFrequencyComponent, _super);
    function TargetFrequencyComponent(authService, settingsService, router, formBuilder, cdRef, sanitizer) {
        var _this = _super.call(this, authService, router, formBuilder, cdRef) || this;
        _this.authService = authService;
        _this.settingsService = settingsService;
        _this.router = router;
        _this.formBuilder = formBuilder;
        _this.cdRef = cdRef;
        _this.sanitizer = sanitizer;
        _this.requestOngoing = false;
        document.title = "Target Frequency";
        _this.targetFrequencies = new Array();
        _this.targetFrequency = new target_frequency_model_1.TargetFrequency();
        _this.newTargetFrequency = new target_frequency_model_1.TargetFrequency();
        _this.createForm();
        document.body.style.backgroundColor = '#eee';
        return _this;
    }
    TargetFrequencyComponent.prototype.ngOnInit = function () {
        this.loadFrequencies();
    };
    TargetFrequencyComponent.prototype.ngOnDestroy = function () {
        this.getTargetFrequencySub.unsubscribe();
    };
    TargetFrequencyComponent.prototype.loadFrequencies = function () {
        var _this = this;
        var self = this;
        self.getTargetFrequencySub = self.settingsService.getTargetFrequency()
            .subscribe(function (response) {
            console.log('getTargetFrequency', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS &&
                response.responseItem) {
                self.targetFrequencies = response.responseItem;
                _this.showDetailViews = false;
                _this.targetFrequency = new target_frequency_model_1.TargetFrequency();
                _this.newTargetFrequency = new target_frequency_model_1.TargetFrequency();
                _this.isAddNew = false;
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    TargetFrequencyComponent.prototype.createForm = function () {
        this.imageForm = this.formBuilder.group({
            frequency: ['', forms_1.Validators.required],
            valueInDays: ['', forms_1.Validators.required]
        });
    };
    TargetFrequencyComponent.prototype.goToDetails = function (targetFrequency) {
        var self = this;
        self.targetFrequency = targetFrequency;
        self.showDetailViews = true;
        self.isAddNew = false;
        self.cdRef.detectChanges();
    };
    TargetFrequencyComponent.prototype.addNew = function (event) {
        event.stopPropagation();
        var self = this;
        self.targetFrequency = new target_frequency_model_1.TargetFrequency();
        self.showDetailViews = true;
        self.isAddNew = true;
        self.cdRef.detectChanges();
    };
    TargetFrequencyComponent.prototype.goBackToList = function () {
        this.targetFrequency = new target_frequency_model_1.TargetFrequency();
        this.showDetailViews = false;
        this.isAddNew = false;
        this.cdRef.detectChanges();
    };
    TargetFrequencyComponent.prototype.save = function () {
        var _this = this;
        var formModel = this.imageForm.value;
        //this.newTargetFrequency.image = formModel.image.value;
        console.log('', JSON.stringify(formModel));
        console.log('saveImage', JSON.stringify(this.newTargetFrequency));
        var self = this;
        self.getTargetFrequencySub = self.settingsService.createTargetFrequency(this.newTargetFrequency)
            .subscribe(function (response) {
            console.log('newTargetFrequency', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                //self.targetImages = response.responseItem;
                _this.loadFrequencies();
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            console.log(error);
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    TargetFrequencyComponent.prototype.update = function () {
        var _this = this;
        var self = this;
        console.log(' update', this.targetFrequency);
        self.getTargetFrequencySub = self.settingsService.updateTargetFrequency(this.targetFrequency)
            .subscribe(function (response) {
            console.log('update target freq', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                //self.targetImages = response.responseItem;
                _this.loadFrequencies();
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            console.log(error);
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    TargetFrequencyComponent.prototype.delete = function () {
        var _this = this;
        var self = this;
        self.getTargetFrequencySub = self.settingsService.deleteTargetFrequency(this.targetFrequency)
            .subscribe(function (response) {
            console.log('delete', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                //self.targetImages = response.responseItem;
                _this.loadFrequencies();
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            console.log(error);
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    TargetFrequencyComponent = __decorate([
        core_1.Component({
            templateUrl: "html/settings/target-frequency.html"
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, settings_service_1.SettingsService,
            router_1.Router, forms_1.FormBuilder, core_1.ChangeDetectorRef,
            platform_browser_1.DomSanitizer])
    ], TargetFrequencyComponent);
    return TargetFrequencyComponent;
}(base_component_1.BaseComponent));
exports.TargetFrequencyComponent = TargetFrequencyComponent;
//# sourceMappingURL=target-frequency.component.js.map