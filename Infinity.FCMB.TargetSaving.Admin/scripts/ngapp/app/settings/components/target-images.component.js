"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var authentication_service_1 = require("../../authentication/services/authentication.service");
var base_component_1 = require("../../commons/components/base.component");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var settings_service_1 = require("../services/settings.service");
var target_images_model_1 = require("../models/target-images.model");
var platform_browser_1 = require("@angular/platform-browser");
function _window() {
    // return the global native browser window object
    return window;
}
var TargetImagesComponent = /** @class */ (function (_super) {
    __extends(TargetImagesComponent, _super);
    function TargetImagesComponent(authService, settingsService, router, formBuilder, cdRef, sanitizer) {
        var _this = _super.call(this, authService, router, formBuilder, cdRef) || this;
        _this.authService = authService;
        _this.settingsService = settingsService;
        _this.router = router;
        _this.formBuilder = formBuilder;
        _this.cdRef = cdRef;
        _this.sanitizer = sanitizer;
        _this.isSettings = false;
        _this.isAddNewTarget = false;
        _this.requestOngoing = false;
        document.title = "Target Images";
        _this.targetImages = new Array();
        _this.targetImage = new target_images_model_1.TargetImage();
        _this.newTargetImage = new target_images_model_1.TargetImage();
        _this.createForm();
        document.body.style.backgroundColor = '#eee';
        return _this;
    }
    TargetImagesComponent.prototype.ngOnInit = function () {
        this.loadTargetImage();
    };
    TargetImagesComponent.prototype.loadTargetImage = function () {
        // let self = this;
        // self.requestOngoing = true;
        // self.isSettings = true;
        // this.showDetailViews = false;
        // self.isAddNewTarget = false;
        // console.log('Appsettings.APP_MODE.toLowerCase()', Appsettings.APP_MODE.toLowerCase());
        // if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
        //     let loadingModalInterval = setInterval(function () {
        //         self.getTargetImages();
        //         _window().clearInterval(loadingModalInterval);
        //     }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        // }
        // else {
        //     self.getTargetImages();
        // }
        var self = this;
        self.requestOngoing = true;
        self.isSettings = true;
        this.showDetailViews = false;
        self.isAddNewTarget = false;
        var loadingModalInterval = setInterval(function () {
            self.getTargetImages();
            _window().clearInterval(loadingModalInterval);
        }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
    };
    TargetImagesComponent.prototype.ngOnDestroy = function () {
    };
    TargetImagesComponent.prototype.createForm = function () {
        this.imageForm = this.formBuilder.group({
            name: ['', forms_1.Validators.required],
            image: null
        });
    };
    TargetImagesComponent.prototype.getTargetImages = function () {
        var self = this;
        self.getTargetImagesSub = self.settingsService.getTargetImages()
            .subscribe(function (response) {
            console.log('getTargetImages', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS &&
                response.responseItem) {
                self.targetImages = response.responseItem;
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    TargetImagesComponent.prototype.goToDetails = function (targetImage) {
        var self = this;
        self.targetImage = targetImage;
        self.showDetailViews = true;
        self.isAddNewTarget = false;
        self.cdRef.detectChanges();
    };
    TargetImagesComponent.prototype.addNewTargetImage = function (event) {
        event.stopPropagation();
        var self = this;
        self.targetImage = new target_images_model_1.TargetImage();
        self.showDetailViews = true;
        self.isAddNewTarget = true;
        self.cdRef.detectChanges();
    };
    TargetImagesComponent.prototype.goBackToList = function () {
        this.targetImage = new target_images_model_1.TargetImage();
        this.showDetailViews = false;
        this.isAddNewTarget = false;
        this.cdRef.detectChanges();
    };
    TargetImagesComponent.prototype.onFileChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            var file_1 = event.target.files[0];
            reader.readAsDataURL(file_1);
            reader.onload = function () {
                _this.imageForm.get('image').setValue({
                    filename: file_1.name,
                    filetype: file_1.type,
                    value: reader.result.split(',')[1]
                });
            };
        }
    };
    TargetImagesComponent.prototype.onFileUpdateChange = function (event) {
        var _this = this;
        var reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            var file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = function () {
                _this.targetImage.image = reader.result.split(',')[1];
            };
        }
    };
    TargetImagesComponent.prototype.saveImage = function () {
        var _this = this;
        var formModel = this.imageForm.value;
        this.newTargetImage.image = formModel.image.value;
        console.log('', JSON.stringify(formModel));
        console.log('saveImage', JSON.stringify(this.newTargetImage));
        var self = this;
        self.getTargetImagesSub = self.settingsService.createTargetImage(this.newTargetImage)
            .subscribe(function (response) {
            console.log('getTargetImages', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                //self.targetImages = response.responseItem;
                _this.loadTargetImage();
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            console.log(error);
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    TargetImagesComponent.prototype.updateImage = function () {
        var _this = this;
        var self = this;
        self.getTargetImagesSub = self.settingsService.updateTargetImage(this.targetImage)
            .subscribe(function (response) {
            console.log('getTargetImages', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                //self.targetImages = response.responseItem;
                _this.loadTargetImage();
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            console.log(error);
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    TargetImagesComponent.prototype.deleteImage = function () {
        var _this = this;
        var self = this;
        self.getTargetImagesSub = self.settingsService.deleteTargetImage(this.targetImage)
            .subscribe(function (response) {
            console.log('getTargetImages', response);
            if (response.apiResponse.responseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                //self.targetImages = response.responseItem;
                _this.loadTargetImage();
            }
        }, function (error) {
            self.requestOngoing = false;
            //show error toast.
            console.log(error);
            self.cdRef.detectChanges();
        }, function () {
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    __decorate([
        core_1.ViewChild('fileInput'),
        __metadata("design:type", core_1.ElementRef)
    ], TargetImagesComponent.prototype, "fileInput", void 0);
    TargetImagesComponent = __decorate([
        core_1.Component({
            templateUrl: "html/settings/target-images.html"
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, settings_service_1.SettingsService,
            router_1.Router, forms_1.FormBuilder, core_1.ChangeDetectorRef,
            platform_browser_1.DomSanitizer])
    ], TargetImagesComponent);
    return TargetImagesComponent;
}(base_component_1.BaseComponent));
exports.TargetImagesComponent = TargetImagesComponent;
//# sourceMappingURL=target-images.component.js.map