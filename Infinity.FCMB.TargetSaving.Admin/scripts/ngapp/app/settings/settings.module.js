"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
//import { ProspectsComponent } from './components/prospects.component';
var header_service_1 = require("../commons/services/header.service");
var utils_services_1 = require("../commons/services/utils.services");
//import { CustomerService } from "./services/customer.services";
var authentication_service_1 = require("../authentication/services/authentication.service");
var settings_service_1 = require("./services/settings.service");
var target_images_component_1 = require("./components/target-images.component");
var target_frequency_component_1 = require("./components/target-frequency.component");
//import { RegisteredCustomersComponent } from './components/registeredCustomers.component';
//import { TargetGoalsComponent } from './components/goal.component';
//import { CustomerRequestComponent } from './components/request.component';
//import { CustomerTransactionComponent } from './components/transactions.component';
var SettingsModule = /** @class */ (function () {
    function SettingsModule() {
    }
    SettingsModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule],
            declarations: [target_images_component_1.TargetImagesComponent, target_frequency_component_1.TargetFrequencyComponent],
            providers: [authentication_service_1.AuthenticationService, header_service_1.HeaderService, utils_services_1.UtilService, settings_service_1.SettingsService]
        })
    ], SettingsModule);
    return SettingsModule;
}());
exports.SettingsModule = SettingsModule;
//# sourceMappingURL=settings.module.js.map