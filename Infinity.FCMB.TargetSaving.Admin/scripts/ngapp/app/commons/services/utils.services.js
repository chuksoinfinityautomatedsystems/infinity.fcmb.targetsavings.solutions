"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
function _window() {
    return window;
}
var UtilService = /** @class */ (function () {
    function UtilService() {
    }
    UtilService_1 = UtilService;
    UtilService.clone = function (srcObj) {
        return Object.assign({}, srcObj);
    };
    UtilService.copy = function (srcObj, destObj) {
        return Object.assign(destObj, srcObj);
    };
    UtilService.trim = function (txt) {
        txt = txt.toString();
        if (String.prototype.trim) {
            return txt.trim();
        }
        else {
            return txt.replace(/^\s+|\s+$/g, '');
        }
    };
    UtilService.convertToDate = function (date) {
        var parts = date.split('/');
        return new Date(parseInt(parts[2]), parseInt(parts[1]) - 1, parseInt(parts[0])); // months start from 0;
    };
    UtilService.getScreenHeight = function () {
        return _window().screen.height;
    };
    UtilService.setElementWidth = function () {
        return _window().document.getElementsByTagName('body')[0].offsetWidth - 17;
    };
    UtilService.getElementWidth = function () {
        return document.getElementsByTagName('body')[0].offsetWidth;
    };
    // static groupArray(list: Array<any>) {
    //     let result = list.reduce(function (r, a) {
    //     r[a.make] = r[a.make] || [];
    //     r[a.make].push(a);
    //     return r;
    // }, Object.create(null));
    // }
    UtilService.StringIsNullOrEmpty = function (text) {
        if (!text) {
            return true;
        }
        if (UtilService_1.trim(text).length < 1) {
            return true;
        }
        return false;
    };
    UtilService.ObjectKeysLength = function (obj) {
        if (!Object.keys) {
            // Object.keys = function (obj) {
            Object.keys = function (obj) {
                var keys = [], k;
                for (k in obj) {
                    if (Object.prototype.hasOwnProperty.call(obj, k)) {
                        keys.push(k);
                    }
                }
                return keys.length;
            };
        }
        else {
            return Object.keys(obj).length;
        }
    };
    UtilService.GetUniqueArray = function (array) {
        var arr = [];
        for (var i = 0; i < array.length; i++) {
            if (arr.indexOf(array[i]) == -1) {
                arr.push(array[i]);
            }
        }
        return arr;
    };
    UtilService.StringContainsDigit = function (value) {
        if (value) {
            return /\d/.test(value);
        }
        return false;
    };
    UtilService.StringContainsUpperCaseCharacters = function (value) {
        if (value) {
            return value.toLowerCase() != value;
        }
        return false;
    };
    UtilService.StringContainsLowerCaseCharacters = function (value) {
        if (value) {
            return value.toUpperCase() != value;
        }
        return false;
    };
    UtilService.getBrowserObj = function () {
        var userAgent = _window().navigator.userAgent;
        var tempArray;
        var M = userAgent.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tempArray = /\brv[ :]+(\d+)/g.exec(userAgent) || [];
            if (tempArray != null) {
                return { name: 'IE', version: (tempArray[1] || '') };
            }
        }
        if (M[1] === 'Chrome') {
            tempArray = userAgent.match(/\b(OPR|Edge)\/(\d+)/);
            if (tempArray != null) {
                return { name: tempArray[1].replace('OPR', 'Opera'), version: tempArray[2] };
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tempArray = userAgent.match(/version\/(\d+)/i)) != null)
            M.splice(1, 1, tempArray[1]);
        return { name: M[0], version: M[1] };
        // {name: "MSIE", version: "8"}
        // {name: "Chrome", version: "39"}
        // {name: "Firefox", version: "36"}
        // {name: "Opera", version: "26"}
        // {name: "Edge", version: "14"}
        // {name: "Safari", version: "9"}
    };
    UtilService.toastError = function (message) {
        var type = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];
        $.notify({
            // icon: "notifications",
            message: message
        }, {
            type: 'danger',
            timer: 200000,
            placement: {
                from: 'top',
                align: 'center'
            }
        });
    };
    UtilService.toastSuccess = function (message) {
        var type = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];
        $.notify({
            // icon: "notifications",
            message: message
        }, {
            type: 'success',
            timer: 200000,
            placement: {
                from: 'top',
                align: 'center'
            }
        });
    };
    UtilService.authErrorMessage = "You are not authorized to perform this operation. Please signin to continue.";
    UtilService = UtilService_1 = __decorate([
        core_1.Injectable()
    ], UtilService);
    return UtilService;
    var UtilService_1;
}());
exports.UtilService = UtilService;
//# sourceMappingURL=utils.services.js.map