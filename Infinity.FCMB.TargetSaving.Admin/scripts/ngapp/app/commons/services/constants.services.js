"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
function _window() {
    // return the global native browser window object
    return window;
}
var ConstantService = (function () {
    function ConstantService() {
        var fullUrl = _window().location.href;
        var pathArray = fullUrl.split("/");
        var apiDomain = pathArray[0] + "//" + pathArray[2] + "/api/"; // "http://localhost:12462/api/";       
        var accountCheckDomain = "http://localhost:2746/api/";
        this.TokenUrl = apiDomain + "token";
        this.RequestCategoryUrl = apiDomain + "setup/categories";
        this.RequestMediumUrl = apiDomain + "setup/media";
        this.ProblemTypeUrl = apiDomain + "setup/problem-types";
        this.PriorityUrl = apiDomain + "setup/priorities";
        this.MoodUrl = apiDomain + "setup/moods";
        this.StateTypeUrl = apiDomain + "setup/request-states";
        this.RequestLogUrl = apiDomain + "requests";
        this.RequestCategoryDDLUrl = apiDomain + "requests/categories";
        this.RequestMediumDDLUrl = apiDomain + "requests/media";
        this.ProblemTypeDDLUrl = apiDomain + "requests/problem-types";
        this.PriorityDDLUrl = apiDomain + "requests/priorities";
        this.MoodDDLUrl = apiDomain + "requests/moods";
        this.StateTypeDDLUrl = apiDomain + "requests/states";
        this.RequestLogFilterUrl = apiDomain + "requests/filter";
        this.UpdateRequestStatusURL = apiDomain + "requests/update-status";
        this.ResolveRequestURL = apiDomain + "requests/resolve";
        this.logSummaryUrl = apiDomain + "requests/log-summary";
        this.logTypeSummaryUrl = apiDomain + "requests/log-type-summary";
        this.logReportUrl = apiDomain + "requests/reports";
        this.reportLogFilterUrl = apiDomain + "requests/filter-reports";
        this.CheckAccountURL = accountCheckDomain + "customer/account-exist";
        this.CustomerDetailsURL = accountCheckDomain + "customer/details";
        this.CustomerAccountsURL = accountCheckDomain + "customer/accounts";
        this.AccountDetailsURL = accountCheckDomain + "customer/account-details";
        this.AccountTransactionsURL = accountCheckDomain + "customer/transactions";
        this.CustomerIssuesURL = apiDomain + "customer/issues";
        this.PermissionURL = apiDomain + "permission/exist";
        this.LogAccountSearchURL = apiDomain + "customer/log-account-search";
        this.LogAccountDetailsSearchURL = apiDomain + "customer/log-account-details-search";
        this.ReportPermissionName = "CUST_SERVICE";
        this.RequestPermissionName = "CUST_SERVICE";
        this.SetupPermissionName = "CUST_SERVICE";
        this.PullAccountPermissionName = "CUST_SERVICE";
    }
    ConstantService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], ConstantService);
    return ConstantService;
}());
exports.ConstantService = ConstantService;
//# sourceMappingURL=constants.services.js.map