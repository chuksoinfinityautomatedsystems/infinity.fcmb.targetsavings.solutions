"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var EventHandlerService = /** @class */ (function () {
    function EventHandlerService() {
    }
    //@Output() static accountDetailsEventPublisher = new EventEmitter<{ forComponent: ComponentEventPublisherEnum, accountNo: string }>();
    EventHandlerService.editObjectEventPublisher = new core_1.EventEmitter();
    EventHandlerService.isSeletectedEventPublisher = new core_1.EventEmitter();
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], EventHandlerService, "editObjectEventPublisher", void 0);
    __decorate([
        core_1.Output(),
        __metadata("design:type", Object)
    ], EventHandlerService, "isSeletectedEventPublisher", void 0);
    EventHandlerService = __decorate([
        core_1.Injectable()
    ], EventHandlerService);
    return EventHandlerService;
}());
exports.EventHandlerService = EventHandlerService;
//# sourceMappingURL=event_handlers.service.js.map