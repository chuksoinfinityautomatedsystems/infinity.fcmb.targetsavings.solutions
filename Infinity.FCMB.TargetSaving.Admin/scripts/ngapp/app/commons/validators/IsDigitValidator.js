"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IsDigitValidator = /** @class */ (function () {
    function IsDigitValidator() {
    }
    IsDigitValidator.isValid = function (formControl) {
        if (formControl.value) {
            var val = formControl.value;
            if (val && val.length > 0) {
                val = val.replace(/\s/g, '');
                if (/^[0-9]+$/.test(val) === false) {
                    return { isDigitInvalid: true };
                }
            }
        }
        return null;
    };
    return IsDigitValidator;
}());
exports.IsDigitValidator = IsDigitValidator;
//# sourceMappingURL=IsDigitValidator.js.map