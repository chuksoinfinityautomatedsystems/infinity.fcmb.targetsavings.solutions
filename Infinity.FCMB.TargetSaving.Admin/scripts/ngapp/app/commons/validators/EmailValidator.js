"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EmailValidator = /** @class */ (function () {
    function EmailValidator() {
    }
    EmailValidator.isValid = function (formControl) {
        var emailRegex = /^[ ]*\w+([-+.']\w+)*@\w+([-.]\\w+)*\.\w+([-.]\w+)*[ ]*$/i;
        if (formControl.value) {
            var isValid = emailRegex.test(formControl.value);
            if (isValid === false) {
                return { emailInvalid: true };
            }
        }
        return null;
    };
    return EmailValidator;
}());
exports.EmailValidator = EmailValidator;
//# sourceMappingURL=EmailValidator.js.map