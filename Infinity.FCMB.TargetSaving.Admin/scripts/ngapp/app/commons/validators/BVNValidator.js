"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BVNValidator = /** @class */ (function () {
    function BVNValidator() {
    }
    BVNValidator.isValid = function (formControl) {
        if (formControl.value && formControl.value.length > 0) {
            if (formControl.value.length > 11) {
                return { bvnInvalid: true };
            }
        }
        return null;
    };
    return BVNValidator;
}());
exports.BVNValidator = BVNValidator;
//# sourceMappingURL=BVNValidator.js.map