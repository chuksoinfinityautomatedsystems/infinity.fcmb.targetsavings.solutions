"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ResponseCodes = /** @class */ (function () {
    function ResponseCodes() {
    }
    ResponseCodes.SUCCESS = "00";
    ResponseCodes.TechnicalError = "99";
    ResponseCodes.NoError = "XX";
    return ResponseCodes;
}());
exports.ResponseCodes = ResponseCodes;
//# sourceMappingURL=response_codes.constant.js.map