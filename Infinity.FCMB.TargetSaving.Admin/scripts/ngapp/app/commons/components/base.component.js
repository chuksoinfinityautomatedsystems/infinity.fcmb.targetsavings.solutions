"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
function _window() {
    return window;
}
var BaseComponent = /** @class */ (function () {
    function BaseComponent(_authService, _router, _formBuilder, _cdRef) {
        this.requestOngoing = false;
        this._authService = _authService;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this._cdRef = _cdRef;
    }
    BaseComponent.prototype.logout = function () {
        this._authService.logout();
        this._router.navigate([appsettings_constant_1.Appsettings.LOGIN_ROUTER_URL]);
    };
    return BaseComponent;
}());
exports.BaseComponent = BaseComponent;
//# sourceMappingURL=base.component.js.map