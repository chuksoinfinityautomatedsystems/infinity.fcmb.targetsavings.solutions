"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var authentication_service_1 = require("./authentication/services/authentication.service");
var utils_services_1 = require("./commons/services/utils.services");
var appsettings_constant_1 = require("./commons/constants/appsettings.constant");
var AppComponent = /** @class */ (function () {
    function AppComponent(authService, router) {
        var _this = this;
        this.authService = authService;
        this.router = router;
        this.currentPage = 'Dashboard';
        this.isSettings = false;
        //let isLoggedInChecker: boolean = false;
        authService.loggedIn$.subscribe(function (flag) { return _this.loggedIn = flag; });
        if (!this.loggedIn) {
            //this.requiresAuth = true;
            this.router.navigate(['Login']);
        }
        else {
            document.body.style.backgroundColor = '#eee';
            //Handle Page refresh
            var currentUrl = window.location.pathname;
            currentUrl = utils_services_1.UtilService.trim(currentUrl);
            //this.requiresAuth = false;
            if (currentUrl.length > 0 && currentUrl != "" && currentUrl != "/") {
                if (currentUrl.substr(currentUrl.length - 1) == "/") {
                    router.navigate([currentUrl.substr(1, currentUrl.length - 2)]);
                }
                else {
                    router.navigate([currentUrl.substr(1, currentUrl.length - 1)]);
                }
            }
            else {
                this.currentPage = 'Dashboard';
                this.router.navigate([appsettings_constant_1.Appsettings.DASHBOARD_ROUTER_URL]);
            }
        }
    }
    AppComponent.prototype.closeSettings = function (event) {
        event.stopPropagation();
        var self = this;
        self.isSettings = false;
        return false;
    };
    AppComponent.prototype.openSettings = function (event) {
        event.stopPropagation();
        var self = this;
        self.isSettings = true;
        return false;
    };
    AppComponent.prototype.gotoDashboard = function (event) {
        event.stopPropagation();
        this.currentPage = 'Dashboard';
        this.router.navigate([appsettings_constant_1.Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    };
    AppComponent.prototype.goToProspects = function (event) {
        event.stopPropagation();
        this.currentPage = 'Prospects';
        this.router.navigate([appsettings_constant_1.Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    };
    AppComponent.prototype.goToCustomers = function (event) {
        event.stopPropagation();
        this.currentPage = 'Customers';
        this.router.navigate([appsettings_constant_1.Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL]);
        return false;
    };
    AppComponent.prototype.goToCustomerRequests = function (event) {
        event.stopPropagation();
        this.currentPage = 'Requests';
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL]);
        return false;
    };
    AppComponent.prototype.goToCustomerGoals = function (event) {
        event.stopPropagation();
        this.currentPage = 'Goals';
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    };
    AppComponent.prototype.goToCustomerTransactions = function (event) {
        event.stopPropagation();
        this.currentPage = 'Transactions';
        this.router.navigate([appsettings_constant_1.Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    };
    AppComponent.prototype.goToTargetImages = function (event) {
        console.log('goToTargetImages');
        event.stopPropagation();
        this.currentPage = 'Target Images';
        this.router.navigate([appsettings_constant_1.Appsettings.SETTINGS_TARGET_IMAGES_ROUTER_URL]);
        return false;
    };
    AppComponent.prototype.goToTargetFrequency = function (event) {
        console.log('goToTargetFrequency');
        event.stopPropagation();
        this.currentPage = 'Target Frequency';
        this.router.navigate([appsettings_constant_1.Appsettings.SETTINGS_TARGET_FREQUENCY_ROUTER_URL]);
        return false;
    };
    AppComponent.prototype.logout = function (event) {
        event.stopPropagation();
        this.authService.loggedIn(false);
        this.router.navigate(['Login']);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: "ng-app",
            templateUrl: "html/app.html",
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map