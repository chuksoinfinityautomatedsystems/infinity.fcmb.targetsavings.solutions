"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var router_1 = require('@angular/router');
var authorization_services_1 = require("../authentication/services/authorization.services");
var receipt_component_1 = require("./components/receipt.component");
var service_request_component_1 = require('./components/service-request.component');
var profile_settings_component_1 = require('./components/profile_settings.component');
var schedule_payments_component_1 = require('./components/schedule_payments.component');
var ibranch_online_component_1 = require('./components/ibranch_online.component');
var collection_component_1 = require('./components/collection.component');
var paycode_request_component_1 = require('./components/paycode-request.component');
var header_service_1 = require("../commons/services/header.service");
var constants_services_1 = require("../commons/services/constants.services");
var utils_services_1 = require("../commons/services/utils.services");
var menu_services_1 = require("./services/menu.services");
var authentication_service_1 = require("../authentication/services/authentication.service");
var MenuModule = (function () {
    function MenuModule() {
    }
    MenuModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule],
            declarations: [receipt_component_1.ManageReceiptComponent, service_request_component_1.ManageServiceRequestComponent,
                profile_settings_component_1.ProfileSettingsComponent, schedule_payments_component_1.ManageScheduleReceiptComponent, collection_component_1.CollectionsComponent, ibranch_online_component_1.IBranchCollectionComponent,
                paycode_request_component_1.PaycodeRequestComponent],
            providers: [header_service_1.HeaderService, constants_services_1.ConstantService, utils_services_1.UtilService, menu_services_1.MenuService, authentication_service_1.AuthenticationService, authorization_services_1.AuthorizeService]
        }), 
        __metadata('design:paramtypes', [])
    ], MenuModule);
    return MenuModule;
}());
exports.MenuModule = MenuModule;
//# sourceMappingURL=menu.module.js.map