"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/Rx");
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/of');
var utils_services_1 = require("../../commons/services/utils.services");
var header_service_1 = require("../../commons/services/header.service");
var constants_services_1 = require("../../commons/services/constants.services");
var authentication_service_1 = require("../../authentication/services/authentication.service");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var MenuService = (function () {
    function MenuService(authService, _headerService, constantService, http) {
        this.authService = authService;
        this._headerService = _headerService;
        this.constantService = constantService;
        this.http = http;
    }
    MenuService.prototype.getBeneficiaryTransferReceipt = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters.");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_TRANSACTION_DETAILS, { userId: userId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Receipts: [
                    {
                        date: "2018-03-12T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 1967882.49,
                        transactionType: "Beneficiary payment",
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        isSuccessful: true,
                        beneficiaryName: "Kayode Coker"
                    },
                    {
                        date: "2018-03-12T12:14:30.6534304+01:00",
                        description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
                        amount: 18000000000,
                        transactionType: "Beneficiary payment",
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        isSuccessful: true,
                        beneficiaryName: "Daniel Craig"
                    },
                    {
                        date: "2018-03-11T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 647383929.0,
                        transactionType: "Beneficiary payment",
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        isSuccessful: true,
                        beneficiaryName: "Gabby"
                    },
                    {
                        date: "2018-03-11T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 1039499223.0,
                        transactionType: "Beneficiary payment",
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        isSuccessful: true,
                        beneficiaryName: "Bunmi Atanda"
                    }, {
                        date: "2018-03-10T12:14:30.6534304+01:00",
                        description: "FASTPAY_CHARGES",
                        amount: 4488438848.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: false,
                        beneficiaryName: "Yetunde Alan"
                    },
                    {
                        date: "2018-03-10T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 7827781392.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Robin Crusoe"
                    },
                    {
                        date: "2018-03-12T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 35788582.21,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Trevor Noah"
                    },
                    {
                        date: "2018-03-09T12:14:30.6534304+01:00",
                        description: "FASTPAY_CHARGES",
                        amount: 123748839.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "DSTV Payment"
                    },
                    {
                        date: "2018-03-08T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 1112737733.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: false,
                        beneficiaryName: "Brian Donald"
                    }, {
                        date: "2018-03-08T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 384938823.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Jorgen Klopp"
                    },
                    {
                        date: "2018-03-07T12:14:30.6534304+01:00",
                        description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
                        amount: 56373883.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Kolapo Sola"
                    }, {
                        date: "2018-03-07T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 312494993.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Trellio Bond"
                    },
                    {
                        date: "2018-03-06T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 321849493.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: false,
                        beneficiaryName: "Wesley Snipes"
                    },
                    {
                        date: "2018-03-08T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 56737883.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Bolu Kayode"
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 11903002.67,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Bello Grunt"
                    },
                    {
                        date: "2018-02-14T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 33434534.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Adebayo Bukky"
                    }, {
                        date: "2018-01-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 342335233.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Seun Nelson"
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
                        amount: 89383892.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Freedom Park"
                    }, {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 24643233.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Landon Donovan"
                    },
                    {
                        date: "2018-05-14T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 33445432.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Music Fest"
                    }, {
                        date: "2018-01-14T12:14:30.6534304+01:00",
                        description: "FASTPAY_CHARGES",
                        amount: 333234.0,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "Google Ad transfer"
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 32334234.40,
                        fromAccountName: "Kleiner Perkins",
                        currency: "NGN",
                        transactionId: "2893922389392TR",
                        transactionType: "Beneficiary payment",
                        isSuccessful: true,
                        beneficiaryName: "MS Computing fee."
                    },
                ]
            });
        }
    };
    MenuService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof header_service_1.HeaderService !== 'undefined' && header_service_1.HeaderService) === 'function' && _b) || Object, (typeof (_c = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _c) || Object, http_1.Http])
    ], MenuService);
    return MenuService;
    var _a, _b, _c;
}());
exports.MenuService = MenuService;
//# sourceMappingURL=menu.services.js.map