"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var menu_services_1 = require("../services/menu.services");
var utils_services_1 = require("../../commons/services/utils.services");
var constants_services_1 = require("../../commons/services/constants.services");
var authentication_service_1 = require("../../authentication/services/authentication.service");
function _window() {
    // return the global native browser window object
    return window;
}
var CollectionsComponent = (function () {
    function CollectionsComponent(authService, utiilService, router, formBuilder, menuService, constantService, cdRef) {
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.menuService = menuService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.burgerMenuIsActive = false;
    }
    CollectionsComponent.prototype.toggleMenu = function () {
        this.burgerMenuIsActive = !this.burgerMenuIsActive;
    };
    CollectionsComponent.prototype.gotoTxnReceipt = function () {
        this.router.navigate(['manage-receipts']);
        return false;
    };
    CollectionsComponent.prototype.gotoManageBeneficiaries = function () {
    };
    CollectionsComponent.prototype.gotoServiceRequest = function () {
        this.router.navigate(['service-request']);
        return false;
    };
    CollectionsComponent.prototype.goToProfilesAndSettings = function () {
        this.burgerMenuIsActive = !this.burgerMenuIsActive;
        return false;
    };
    CollectionsComponent.prototype.goToScheduteTxn = function () {
        this.router.navigate(['manage-scheduled-receipts']);
        return false;
    };
    CollectionsComponent.prototype.goToSubcategories = function () {
        this.router.navigate(['ibranch-collections/category/details']);
        return false;
    };
    CollectionsComponent.prototype.gotoPayBeneficiary = function () {
        this.router.navigate(['beneficiaries']);
        return false;
    };
    CollectionsComponent.prototype.gotoBuy = function () {
        this.router.navigate(['buy-airtime']);
        return false;
    };
    CollectionsComponent.prototype.gotoRedeem = function () {
        this.router.navigate(['redeem-western-union']);
        return false;
    };
    CollectionsComponent.prototype.goHome = function () {
        this.router.navigate(['dashboard']);
        return false;
    };
    CollectionsComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    CollectionsComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    CollectionsComponent = __decorate([
        core_1.Component({
            templateUrl: "html/menu/collections.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof menu_services_1.MenuService !== 'undefined' && menu_services_1.MenuService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], CollectionsComponent);
    return CollectionsComponent;
    var _a, _b, _c, _d;
}());
exports.CollectionsComponent = CollectionsComponent;
//# sourceMappingURL=collection.component.js.map