"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var menu_services_1 = require("../services/menu.services");
var utils_services_1 = require("../../commons/services/utils.services");
var constants_services_1 = require("../../commons/services/constants.services");
var authentication_service_1 = require("../../authentication/services/authentication.service");
function _window() {
    // return the global native browser window object
    return window;
}
var ManageServiceRequestComponent = (function () {
    function ManageServiceRequestComponent(authService, utiilService, router, formBuilder, menuService, constantService, cdRef) {
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.menuService = menuService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.cancelModalOpened = false;
        this.pageTitle = "Service Requests";
        this.pageSubtitle = "";
        this.pageIndex = 1;
        this.subPageIndex = 1;
        this.requestChequeFormGroup = formBuilder.group({});
        this.stopChequeFormGroup = formBuilder.group({});
        this.modifyAccountDetailsFormGroup = formBuilder.group({});
    }
    ManageServiceRequestComponent.prototype.hideCancelTransferModal = function () {
        this.cancelModalOpened = false;
    };
    ManageServiceRequestComponent.prototype.CloseTranfer = function () {
        this.cancelModalOpened = true;
    };
    ManageServiceRequestComponent.prototype.goBack = function () {
        this.pageTitle = "Service Requests";
        this.pageSubtitle = "";
        this.pageIndex = this.pageIndex - 1;
        // switch (this.pageIndex) {
        //     case 1:
        //         this.pageTitle = "Service Requests";
        //         break;
        //     case 2:
        //         this.pageTitle = "New Service Request";
        //         break;    
        // }
    };
    ManageServiceRequestComponent.prototype.goToManageReceipts = function () {
        this.router.navigate(['manage-receipts']);
        return false;
    };
    ManageServiceRequestComponent.prototype.listServiceRequest = function () {
        this.pageTitle = "New Service Request";
        this.pageIndex = 2;
    };
    ManageServiceRequestComponent.prototype.showRequestChequeForm = function () {
        this.pageIndex = 2;
        this.subPageIndex = 1;
        this.pageTitle = "Request Cheque Book";
        this.pageSubtitle = "Details";
    };
    ManageServiceRequestComponent.prototype.showStopChequeForm = function () {
        this.pageIndex = 2;
        this.subPageIndex = 2;
        this.pageTitle = "Stop cheque";
        this.pageSubtitle = "Details";
    };
    ManageServiceRequestComponent.prototype.showModifyAccountForm = function () {
        this.pageIndex = 2;
        this.subPageIndex = 3;
        this.pageTitle = "Modify Account Details";
        this.pageSubtitle = "Details";
    };
    ManageServiceRequestComponent.prototype.goHome = function () {
        this.router.navigate(['dashboard']);
        return false;
    };
    ManageServiceRequestComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    ManageServiceRequestComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    ManageServiceRequestComponent = __decorate([
        core_1.Component({
            templateUrl: "html/menu/service-request.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof menu_services_1.MenuService !== 'undefined' && menu_services_1.MenuService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], ManageServiceRequestComponent);
    return ManageServiceRequestComponent;
    var _a, _b, _c, _d;
}());
exports.ManageServiceRequestComponent = ManageServiceRequestComponent;
//# sourceMappingURL=service-request.component.js.map