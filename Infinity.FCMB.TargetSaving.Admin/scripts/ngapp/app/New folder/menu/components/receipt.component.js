"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var menu_services_1 = require("../services/menu.services");
var utils_services_1 = require("../../commons/services/utils.services");
var constants_services_1 = require("../../commons/services/constants.services");
var authentication_service_1 = require("../../authentication/services/authentication.service");
var base_component_1 = require("../../commons/components/base.component");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
function _window() {
    // return the global native browser window object
    return window;
}
var ManageReceiptComponent = (function (_super) {
    __extends(ManageReceiptComponent, _super);
    function ManageReceiptComponent(authService, utiilService, router, menuService, constantService, cdRef) {
        _super.call(this, authService, router);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.menuService = menuService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.selectedMenuTab = 1;
        this.burgerMenuIsActive = false;
        this.filterActive = false;
    }
    ManageReceiptComponent.prototype.ngOnInit = function () {
        this.selectedMenuTab = 1;
        this.getTransactionReceipts();
    };
    ManageReceiptComponent.prototype.getTransactionReceipts = function () {
        var self = this;
        self.showBusyLoader = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                switch (self.selectedMenuTab) {
                    case 1:
                        self._getBeneficiaryReceipts();
                        break;
                    case 2:
                        self._getOneOffReceipts();
                        break;
                    case 3:
                        self._getSelfAccountTransferTransactionReceipts();
                        break;
                    case 4:
                        self._getAirtimeTopupTransactionReceipts();
                        break;
                    case 5:
                        self._getBillerTransactionReceipts();
                        break;
                    case 6:
                        self._getRemitaTransactionReceipts();
                        break;
                }
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            switch (self.selectedMenuTab) {
                case 1:
                    self._getBeneficiaryReceipts();
                    break;
                case 2:
                    self._getOneOffReceipts();
                    break;
                case 3:
                    self._getSelfAccountTransferTransactionReceipts();
                    break;
                case 4:
                    self._getAirtimeTopupTransactionReceipts();
                    break;
                case 5:
                    self._getBillerTransactionReceipts();
                    break;
                case 6:
                    self._getRemitaTransactionReceipts();
                    break;
            }
        }
    };
    ManageReceiptComponent.prototype._getBeneficiaryReceipts = function () {
        var self = this;
        // console.log(AuthenticationService.authUserID);
        // console.log(AuthenticationService);
        self.getTransactionReceiptsSub = self.menuService.getBeneficiaryTransferReceipt(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                // let transactions = response.Receipts; 
                var receipts = response.Receipts;
                if (receipts && receipts.length > 0) {
                    //group transactions by date. this will produce an object literal.
                    var dateGroupedReceipts = receipts.reduce(function (r, a) {
                        r[a.date] = r[a.date] || [];
                        r[a.date].push(a);
                        return r;
                    }, Object.create(null));
                    //convert the object literal into a date sorted array of transaction objects
                    var dateSortedReceiptArray = Object.values(dateGroupedReceipts).sort(function (a, b) {
                        if (new Date(a[0].date) < new Date(b[0].date))
                            return 1;
                        if (new Date(a[0].date) > new Date(b[0].date))
                            return -1;
                        return 0;
                    });
                    //fill up existing array.
                    self.dateGroupedReceipts = [];
                    self.dateGroupedReceipts.push.apply(self.dateGroupedReceipts, dateSortedReceiptArray);
                    console.log('Receipts');
                    console.log(self.dateGroupedReceipts);
                }
                else {
                }
                self.showBusyLoader = false;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            // self.showBusyLoader = false;
            //   self.showTransactionLoading = false; 
            self.cdRef.detectChanges();
        }, function () {
            // self.showBusyLoader = false; 
            // self.showTransactionLoading = false;  
            //  self.cdRef.detectChanges();
        });
    };
    ManageReceiptComponent.prototype._getOneOffReceipts = function () {
        var self = this;
        self.getTransactionReceiptsSub = self.menuService.getBeneficiaryTransferReceipt(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                // let transactions = response.Receipts; 
                var receipts = response.Receipts;
                if (receipts && receipts.length > 0) {
                    //group transactions by date. this will produce an object literal.
                    var dateGroupedReceipts = receipts.reduce(function (r, a) {
                        r[a.date] = r[a.date] || [];
                        r[a.date].push(a);
                        return r;
                    }, Object.create(null));
                    //convert the object literal into a date sorted array of transaction objects
                    var dateSortedReceiptArray = Object.values(dateGroupedReceipts).sort(function (a, b) {
                        if (new Date(a[0].date) < new Date(b[0].date))
                            return 1;
                        if (new Date(a[0].date) > new Date(b[0].date))
                            return -1;
                        return 0;
                    });
                    //fill up existing array.
                    self.dateGroupedReceipts = [];
                    self.dateGroupedReceipts.push.apply(self.dateGroupedReceipts, dateSortedReceiptArray);
                    console.log('Receipts');
                    console.log(self.dateGroupedReceipts);
                }
                else {
                }
                self.showBusyLoader = false;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    ManageReceiptComponent.prototype._getScheduleTransactionReceipts = function () {
        var self = this;
        self.getTransactionReceiptsSub = self.menuService.getBeneficiaryTransferReceipt(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                // let transactions = response.Receipts; 
                var receipts = response.Receipts;
                if (receipts && receipts.length > 0) {
                    //group transactions by date. this will produce an object literal.
                    var dateGroupedReceipts = receipts.reduce(function (r, a) {
                        r[a.date] = r[a.date] || [];
                        r[a.date].push(a);
                        return r;
                    }, Object.create(null));
                    //convert the object literal into a date sorted array of transaction objects
                    var dateSortedReceiptArray = Object.values(dateGroupedReceipts).sort(function (a, b) {
                        if (new Date(a[0].date) < new Date(b[0].date))
                            return 1;
                        if (new Date(a[0].date) > new Date(b[0].date))
                            return -1;
                        return 0;
                    });
                    //fill up existing array.
                    self.dateGroupedReceipts = [];
                    self.dateGroupedReceipts.push.apply(self.dateGroupedReceipts, dateSortedReceiptArray);
                }
                else {
                }
                self.showBusyLoader = false;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    ManageReceiptComponent.prototype._getBillerTransactionReceipts = function () {
        var self = this;
        self.getTransactionReceiptsSub = self.menuService.getBeneficiaryTransferReceipt(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                // let transactions = response.Receipts; 
                var receipts = response.Receipts;
                if (receipts && receipts.length > 0) {
                    //group transactions by date. this will produce an object literal.
                    var dateGroupedReceipts = receipts.reduce(function (r, a) {
                        r[a.date] = r[a.date] || [];
                        r[a.date].push(a);
                        return r;
                    }, Object.create(null));
                    //convert the object literal into a date sorted array of transaction objects
                    var dateSortedReceiptArray = Object.values(dateGroupedReceipts).sort(function (a, b) {
                        if (new Date(a[0].date) < new Date(b[0].date))
                            return 1;
                        if (new Date(a[0].date) > new Date(b[0].date))
                            return -1;
                        return 0;
                    });
                    //fill up existing array.
                    self.dateGroupedReceipts = [];
                    self.dateGroupedReceipts.push.apply(self.dateGroupedReceipts, dateSortedReceiptArray);
                }
                else {
                }
                self.showBusyLoader = false;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    ManageReceiptComponent.prototype._getRemitaTransactionReceipts = function () {
        var self = this;
        self.getTransactionReceiptsSub = self.menuService.getBeneficiaryTransferReceipt(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                // let transactions = response.Receipts; 
                var receipts = response.Receipts;
                if (receipts && receipts.length > 0) {
                    //group transactions by date. this will produce an object literal.
                    var dateGroupedReceipts = receipts.reduce(function (r, a) {
                        r[a.date] = r[a.date] || [];
                        r[a.date].push(a);
                        return r;
                    }, Object.create(null));
                    //convert the object literal into a date sorted array of transaction objects
                    var dateSortedReceiptArray = Object.values(dateGroupedReceipts).sort(function (a, b) {
                        if (new Date(a[0].date) < new Date(b[0].date))
                            return 1;
                        if (new Date(a[0].date) > new Date(b[0].date))
                            return -1;
                        return 0;
                    });
                    //fill up existing array.
                    self.dateGroupedReceipts = [];
                    self.dateGroupedReceipts.push.apply(self.dateGroupedReceipts, dateSortedReceiptArray);
                }
                else {
                }
                self.showBusyLoader = false;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    ManageReceiptComponent.prototype._getAirtimeTopupTransactionReceipts = function () {
        var self = this;
        self.getTransactionReceiptsSub = self.menuService.getBeneficiaryTransferReceipt(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                // let transactions = response.Receipts; 
                var receipts = response.Receipts;
                if (receipts && receipts.length > 0) {
                    //group transactions by date. this will produce an object literal.
                    var dateGroupedReceipts = receipts.reduce(function (r, a) {
                        r[a.date] = r[a.date] || [];
                        r[a.date].push(a);
                        return r;
                    }, Object.create(null));
                    //convert the object literal into a date sorted array of transaction objects
                    var dateSortedReceiptArray = Object.values(dateGroupedReceipts).sort(function (a, b) {
                        if (new Date(a[0].date) < new Date(b[0].date))
                            return 1;
                        if (new Date(a[0].date) > new Date(b[0].date))
                            return -1;
                        return 0;
                    });
                    //fill up existing array.
                    self.dateGroupedReceipts = [];
                    self.dateGroupedReceipts.push.apply(self.dateGroupedReceipts, dateSortedReceiptArray);
                }
                else {
                }
                self.showBusyLoader = false;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    ManageReceiptComponent.prototype._getSelfAccountTransferTransactionReceipts = function () {
        var self = this;
        self.getTransactionReceiptsSub = self.menuService.getBeneficiaryTransferReceipt(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                // let transactions = response.Receipts; 
                var receipts = response.Receipts;
                if (receipts && receipts.length > 0) {
                    //group transactions by date. this will produce an object literal.
                    var dateGroupedReceipts = receipts.reduce(function (r, a) {
                        r[a.date] = r[a.date] || [];
                        r[a.date].push(a);
                        return r;
                    }, Object.create(null));
                    //convert the object literal into a date sorted array of transaction objects
                    var dateSortedReceiptArray = Object.values(dateGroupedReceipts).sort(function (a, b) {
                        if (new Date(a[0].date) < new Date(b[0].date))
                            return 1;
                        if (new Date(a[0].date) > new Date(b[0].date))
                            return -1;
                        return 0;
                    });
                    //fill up existing array.
                    self.dateGroupedReceipts = [];
                    self.dateGroupedReceipts.push.apply(self.dateGroupedReceipts, dateSortedReceiptArray);
                }
                else {
                }
                self.showBusyLoader = false;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    ManageReceiptComponent.prototype.toggleMenu = function () {
        this.burgerMenuIsActive = !this.burgerMenuIsActive;
    };
    ManageReceiptComponent.prototype.gotoTxnReceipt = function () {
        this.burgerMenuIsActive = !this.burgerMenuIsActive;
        return false;
    };
    ManageReceiptComponent.prototype.gotoServiceRequest = function () {
        this.router.navigate(['service-request']);
        return false;
    };
    ManageReceiptComponent.prototype.goToProfilesAndSettings = function () {
        this.router.navigate(['profile-and-settings']);
        return false;
    };
    ManageReceiptComponent.prototype.goToScheduteTxn = function () {
        this.router.navigate(['manage-scheduled-receipts']);
        return false;
    };
    ManageReceiptComponent.prototype.gotoManageBeneficiaries = function () {
    };
    ManageReceiptComponent.prototype.gotoPayBeneficiary = function () {
        this.router.navigate(['beneficiaries']);
        return false;
    };
    ManageReceiptComponent.prototype.gotoBuy = function () {
        this.router.navigate(['buy-airtime']);
        return false;
    };
    ManageReceiptComponent.prototype.gotoRedeem = function () {
        this.router.navigate(['redeem-western-union']);
        return false;
    };
    ManageReceiptComponent.prototype.goHome = function () {
        this.router.navigate(['dashboard']);
        return false;
    };
    ManageReceiptComponent.prototype.getTransactions = function (tranType) {
        if (tranType > 0 && tranType < 7) {
            this.selectedMenuTab = tranType;
            this.getTransactionReceipts();
        }
    };
    ManageReceiptComponent.prototype.clearAllFilters = function (event) {
        event.stopPropagation();
        this.filterActive = false;
        return false;
    };
    ManageReceiptComponent.prototype.toggleFilter = function () {
        this.filterActive = !this.filterActive;
        return false;
    };
    ManageReceiptComponent.prototype.backHome = function () {
        this.router.navigate(['dashboard']);
    };
    ManageReceiptComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    ManageReceiptComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    ManageReceiptComponent = __decorate([
        core_1.Component({
            templateUrl: "html/menu/manage-receipts.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, (typeof (_c = typeof menu_services_1.MenuService !== 'undefined' && menu_services_1.MenuService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], ManageReceiptComponent);
    return ManageReceiptComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.ManageReceiptComponent = ManageReceiptComponent;
//# sourceMappingURL=receipt.component.js.map