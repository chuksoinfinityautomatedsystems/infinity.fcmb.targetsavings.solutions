"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var menu_services_1 = require("../services/menu.services");
var utils_services_1 = require("../../commons/services/utils.services");
var constants_services_1 = require("../../commons/services/constants.services");
var authentication_service_1 = require("../../authentication/services/authentication.service");
var base_component_1 = require("../../commons/components/base.component");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
function _window() {
    // return the global native browser window object
    return window;
}
var ProfileSettingsComponent = (function (_super) {
    __extends(ProfileSettingsComponent, _super);
    function ProfileSettingsComponent(authService, utiilService, router, formBuilder, menuService, constantService, cdRef) {
        _super.call(this, authService, router);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.menuService = menuService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.textInputActive = this.errorMessage = "";
        this.selectedMenuTab = this.flowIndex = 1;
        this.burgerMenuIsActive = false;
        this.changePasswordFormGroup = formBuilder.group({
            password: ['', forms_1.Validators.required],
            confirmPassword: ['', forms_1.Validators.required],
            currentPassword: ['', forms_1.Validators.required]
        });
        //OTP INIT
        this.otpFormGroup = formBuilder.group({
            otpModel1: ['', forms_1.Validators.required],
            otpModel2: ['', forms_1.Validators.required],
            otpModel3: ['', forms_1.Validators.required],
            otpModel4: ['', forms_1.Validators.required],
            otpModel5: ['', forms_1.Validators.required]
        });
        this.otpValues = ["", "", "", "", ""];
        this.minimumPasswordLengthValidated = this.passwordHasUpperCaseCharacters =
            this.passwordHasLowerCaseCharacters = this.passwordHasOneOrMoreNumber = this.passwordHasValidSymbols = false;
        this.passwordValidationPassedCount = 0;
        this.showSessionTimeoutPopup = false;
        this.authService.setupAutoLogout();
    }
    ProfileSettingsComponent.prototype.ngOnInit = function () {
        var self = this;
        self.SetupTimeout();
    };
    // CHANGE PASSWORD SNIPPET
    ProfileSettingsComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.formHasErrorOnSubmit = false;
        //  this.errorMessage = "";
        if (formControlName == "password") {
            this.showPasswordValidationWrap = true;
        }
        this.cdRef.detectChanges();
    };
    ProfileSettingsComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.changePasswordFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
        }
        if (formControlName == "password" && this.passwordValidationPassedCount == 5) {
            this.showPasswordValidationWrap = false;
        }
        if (formControlName == "password" && !this.changePasswordFormGroup.controls['password'].value) {
            this.showPasswordValidationWrap = false;
        }
        this.cdRef.detectChanges();
    };
    ProfileSettingsComponent.prototype.passwordInputKeyUp = function (value) {
        var currentPassword = this.changePasswordFormGroup.controls['password'].value;
        this.AutoPasswordValidation(currentPassword);
    };
    ProfileSettingsComponent.prototype.AutoPasswordValidation = function (currentPassword) {
        this.passwordValidationPassedCount = 0;
        if (currentPassword.length > 7) {
            this.minimumPasswordLengthValidated = true;
            this.passwordValidationPassedCount = 1;
        }
        else {
            this.minimumPasswordLengthValidated = false;
        }
        if (utils_services_1.UtilService.StringContainsUpperCaseCharacters(currentPassword)) {
            this.passwordHasUpperCaseCharacters = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasUpperCaseCharacters = false;
        }
        if (utils_services_1.UtilService.StringContainsLowerCaseCharacters(currentPassword)) {
            this.passwordHasLowerCaseCharacters = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasLowerCaseCharacters = false;
        }
        if (utils_services_1.UtilService.StringContainsDigit(currentPassword)) {
            this.passwordHasOneOrMoreNumber = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasOneOrMoreNumber = false;
        }
        if (this.PasswordHasValidSymbol(currentPassword)) {
            this.passwordHasValidSymbols = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasValidSymbols = false;
        }
    };
    ProfileSettingsComponent.prototype.PasswordHasValidSymbol = function (password) {
        if (password.indexOf("@") >= 0 ||
            password.indexOf("#") >= 0 ||
            password.indexOf("$") >= 0 ||
            password.indexOf("%") >= 0 ||
            password.indexOf("^") >= 0 ||
            password.indexOf("\\") >= 0) {
            return true;
        }
        return false;
    };
    ProfileSettingsComponent.prototype.SendPasswordOTP = function () {
        event.stopPropagation();
        this.formHasErrorOnSubmit = false;
        this.errorMessage = "";
        var self = this;
        if (this.changePasswordFormGroup.valid) {
            if (this.passwordValidationPassedCount < 5) {
                this.errorMessage = "Passwords does not meet the criteria.";
                this.formHasErrorOnSubmit = true;
            }
            else if (this.changePasswordFormGroup.controls['password'].value != this.changePasswordFormGroup.controls['confirmPassword'].value) {
                this.errorMessage = "There is a password mismatch.";
                this.formHasErrorOnSubmit = true;
            }
            else {
                // Call change password and go to login
                self.showBusyLoader = true;
                if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                    var loadingModalInterval_1 = setInterval(function () {
                        self.showBusyLoader = false;
                        self.cdRef.detectChanges();
                        self._sendOTP();
                        _window().clearInterval(loadingModalInterval_1);
                    }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
                }
                else {
                    self._sendOTP();
                }
            }
        }
        else {
            this.errorMessage = "Please enter the new password.";
            this.formHasErrorOnSubmit = true;
        }
    };
    ProfileSettingsComponent.prototype._sendOTP = function () {
        var self = this;
        if (this.initiateOTPSub) {
            this.initiateOTPSub.unsubscribe();
        }
        self.initiateOTPSub = self.authService.initiateOTP(authentication_service_1.AuthenticationService.authUserObj.UserId, authentication_service_1.AuthenticationService.authUserObj.CifID, appsettings_constant_1.Appsettings.OTP_REASON_CODE_CHANGE_PASSWORD)
            .subscribe(function (initiateOtpResponse) {
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.showBusyLoader = false;
                // show the next page
                self.otpReference = initiateOtpResponse.ResponseDescription;
                self.flowIndex = 2;
            }
            else {
                self.errorMessage = initiateOtpResponse.ResponseFriendlyMessage;
                self.formHasErrorOnSubmit = true;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.errorMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.formHasErrorOnSubmit = true;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    ProfileSettingsComponent.prototype.preventArrow = function (event) {
        var key = event.charCode || event.keyCode || 0;
        if (key > 36 && key < 41) {
            event.preventDefault();
        }
    };
    ProfileSettingsComponent.prototype.goNext = function (event, index) {
        var key = event.charCode || event.keyCode || 0;
        // Shift Key : do nada
        if (event.shiftKey) {
            return false;
        }
        else if (key == 8 || key == 46 || key == 45) {
            this.clearOTPValues();
            this.showOtpError = true;
        }
        else if (key == 9) {
            if (!this.otpValues[index - 1]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else if (key > 36 && key < 41) {
            if (index == 5 && !this.otpValues[4]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else {
            //save value and replace with * on txtbox view (if not empty); move to next tab
            if (index == 1) {
                if (this.otpFormGroup.controls["otpModel1"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel1"].value)) {
                    this.otpValues[0] = this.otpFormGroup.controls["otpModel1"].value;
                    this.otpFormGroup.controls["otpModel1"].setValue("*");
                }
                else {
                    this.otpValues[0] = "";
                }
                $("#otpModel2").focus();
            }
            else if (index == 2) {
                if (this.otpFormGroup.controls["otpModel2"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel2"].value)) {
                    this.otpValues[1] = this.otpFormGroup.controls["otpModel2"].value;
                    this.otpFormGroup.controls["otpModel2"].setValue("*");
                }
                else {
                    this.otpValues[1] = "";
                }
                $("#otpModel3").focus();
            }
            else if (index == 3) {
                if (this.otpFormGroup.controls["otpModel3"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel3"].value)) {
                    this.otpValues[2] = this.otpFormGroup.controls["otpModel3"].value;
                    this.otpFormGroup.controls["otpModel3"].setValue("*");
                }
                else {
                    this.otpValues[2] = "";
                }
                $("#otpModel4").focus();
            }
            else if (index == 4) {
                if (this.otpFormGroup.controls["otpModel4"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel4"].value)) {
                    this.otpValues[3] = this.otpFormGroup.controls["otpModel4"].value;
                    this.otpFormGroup.controls["otpModel4"].setValue("*");
                }
                else {
                    this.otpValues[3] = "";
                }
                $("#otpModel5").focus();
            }
            else if (index == 5) {
                if (this.otpFormGroup.controls["otpModel5"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel5"].value)) {
                    this.otpValues[4] = this.otpFormGroup.controls["otpModel5"].value;
                    this.otpFormGroup.controls["otpModel5"].setValue("*");
                }
                else {
                    this.otpValues[4] = "";
                }
            }
            //invalidate otp form if last otp xter is empty
            if (this.otpValues[4]) {
                this.showOtpError = false;
            }
            else {
                this.showOtpError = true;
            }
        }
    };
    ProfileSettingsComponent.prototype.clearOTPValues = function () {
        this.otpFormGroup.controls["otpModel1"].setValue("");
        this.otpFormGroup.controls["otpModel2"].setValue("");
        this.otpFormGroup.controls["otpModel3"].setValue("");
        this.otpFormGroup.controls["otpModel4"].setValue("");
        this.otpFormGroup.controls["otpModel5"].setValue("");
        $("#otpModel1_password").focus();
        this.otpValues[0] = this.otpValues[1] = this.otpValues[2] = this.otpValues[3] = this.otpValues[4] = "";
        this.showOtpError = false;
        this.cdRef.detectChanges();
    };
    ProfileSettingsComponent.prototype.verifyOTPAndChangePassword = function (event) {
        var self = this;
        if (!self.otpValues[4] || (self.otpValues[4] && utils_services_1.UtilService.StringIsNullOrEmpty(self.otpValues[4]))) {
            self.showOtpError = true;
            self.cdRef.detectChanges();
        }
        else {
            self.showBusyLoader = true;
            self.cdRef.detectChanges();
            var otp_1 = self.otpValues.join("");
            if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                var loadingModalInterval_2 = setInterval(function () {
                    self.showBusyLoader = false;
                    self.cdRef.detectChanges();
                    self._changePassword(otp_1);
                    _window().clearInterval(loadingModalInterval_2);
                }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
            }
            else {
                self._changePassword(otp_1);
            }
        }
    };
    ProfileSettingsComponent.prototype._changePassword = function (otp) {
        var self = this;
        self.changePasswordSub = self.authService.changePassword({
            OldPassword: self.changePasswordFormGroup.controls['currentPassword'].value,
            NewPassword: self.changePasswordFormGroup.controls['password'].value,
            OTP: otp,
            UserId: authentication_service_1.AuthenticationService.authUserObj.UserId,
            SourceRefId: self.otpReference
        }).subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.showBusyLoader = false;
                self.cdRef.detectChanges();
                self.router.navigate(['dashboard']);
            }
        }, function (error) {
            self.showOtpError = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            ;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.showOtpError = true;
            self.cdRef.detectChanges();
        });
    };
    ProfileSettingsComponent.prototype.keepBanking = function (event) {
        event.stopPropagation();
        var self = this;
        this.showSessionTimeoutPopup = false;
        if (self.sessionTimeoutInterval) {
            _window().clearInterval(self.sessionTimeoutInterval);
        }
        this.cdRef.detectChanges();
        this.authService.setupAutoLogout();
    };
    // MENU SNIPPETS    
    ProfileSettingsComponent.prototype.toggleMenu = function () {
        this.burgerMenuIsActive = !this.burgerMenuIsActive;
    };
    ProfileSettingsComponent.prototype.gotoTxnReceipt = function () {
        this.router.navigate(['manage-receipts']);
        return false;
    };
    ProfileSettingsComponent.prototype.gotoManageBeneficiaries = function () {
    };
    ProfileSettingsComponent.prototype.gotoServiceRequest = function () {
        this.router.navigate(['service-request']);
        return false;
    };
    ProfileSettingsComponent.prototype.goToProfilesAndSettings = function () {
        this.burgerMenuIsActive = !this.burgerMenuIsActive;
        return false;
    };
    ProfileSettingsComponent.prototype.goToScheduteTxn = function () {
        this.router.navigate(['manage-scheduled-receipts']);
        return false;
    };
    ProfileSettingsComponent.prototype.gotoPayBeneficiary = function () {
        this.router.navigate(['beneficiaries']);
        return false;
    };
    ProfileSettingsComponent.prototype.gotoBuy = function () {
        this.router.navigate(['buy-airtime']);
        return false;
    };
    ProfileSettingsComponent.prototype.gotoRedeem = function () {
        this.router.navigate(['redeem-western-union']);
        return false;
    };
    ProfileSettingsComponent.prototype.goHome = function () {
        var self = this;
        if (self.sessionTimeoutInterval) {
            _window().clearInterval(self.sessionTimeoutInterval);
        }
        if (this.sessionTimeoutSub) {
            this.sessionTimeoutSub.unsubscribe();
        }
        this.router.navigate(['dashboard']);
        return false;
    };
    ProfileSettingsComponent.prototype.getTransactions = function (tranType) {
        if (tranType > 0 && tranType < 2) {
            this.selectedMenuTab = tranType;
        }
    };
    ProfileSettingsComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    ProfileSettingsComponent.prototype.logout = function () {
        var self = this;
        if (self.sessionTimeoutInterval) {
            _window().clearInterval(self.sessionTimeoutInterval);
        }
        if (this.sessionTimeoutSub) {
            this.sessionTimeoutSub.unsubscribe();
        }
        authentication_service_1.AuthenticationService.authLogoutMessage = appsettings_constant_1.Appsettings.LOGOUT_MESSAGE;
        this.authService.logout();
        this.router.navigate(['login']);
    };
    ProfileSettingsComponent = __decorate([
        core_1.Component({
            templateUrl: "html/menu/profile_settings.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof menu_services_1.MenuService !== 'undefined' && menu_services_1.MenuService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], ProfileSettingsComponent);
    return ProfileSettingsComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.ProfileSettingsComponent = ProfileSettingsComponent;
//# sourceMappingURL=profile_settings.component.js.map