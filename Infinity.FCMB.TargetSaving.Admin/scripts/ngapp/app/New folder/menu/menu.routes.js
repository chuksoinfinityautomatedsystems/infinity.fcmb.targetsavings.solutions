"use strict";
var router_1 = require("@angular/router");
var authorization_services_1 = require("../authentication/services/authorization.services");
var receipt_component_1 = require('./components/receipt.component');
var service_request_component_1 = require('./components/service-request.component');
var profile_settings_component_1 = require('./components/profile_settings.component');
var schedule_payments_component_1 = require('./components/schedule_payments.component');
var ibranch_online_component_1 = require('./components/ibranch_online.component');
var collection_component_1 = require('./components/collection.component');
var paycode_request_component_1 = require('./components/paycode-request.component');
exports.menuRoutes = router_1.RouterModule.forChild([
    { path: "manage-receipts", component: receipt_component_1.ManageReceiptComponent, canActivate: [authorization_services_1.AuthorizeService] },
    { path: "service-request", component: service_request_component_1.ManageServiceRequestComponent, canActivate: [authorization_services_1.AuthorizeService] },
    { path: "profile-and-settings", component: profile_settings_component_1.ProfileSettingsComponent, canActivate: [authorization_services_1.AuthorizeService] },
    { path: "manage-scheduled-receipts", component: schedule_payments_component_1.ManageScheduleReceiptComponent, canActivate: [authorization_services_1.AuthorizeService] },
    { path: "ibranch-collections/category", component: collection_component_1.CollectionsComponent, pathMatch: 'full', canActivate: [authorization_services_1.AuthorizeService] },
    {
        path: "ibranch-collections/category/details", component: ibranch_online_component_1.IBranchCollectionComponent, pathMatch: 'full',
        canActivate: [authorization_services_1.AuthorizeService]
    },
    {
        path: "generate-paycode", component: paycode_request_component_1.PaycodeRequestComponent,
        canActivate: [authorization_services_1.AuthorizeService]
    },
]);
//# sourceMappingURL=menu.routes.js.map