"use strict";
var router_1 = require("@angular/router");
var login_component_1 = require('./components/login.component');
var forgot_password_component_1 = require('./components/forgot-password.component');
var registration_component_1 = require('./components/registration.component');
var new_account_component_1 = require('./components/new-account.component');
var login_security_question_component_1 = require('./components/login-security-question.component');
var change_password_component_1 = require('./components/change-password.component');
var registration_validate_default_password_component_1 = require('./components/registration-validate-default-password.component');
exports.authenticationRoutes = router_1.RouterModule.forChild([
    {
        path: "Login",
        component: login_component_1.LoginComponent,
        pathMatch: 'full'
    },
    {
        path: "login",
        component: login_component_1.LoginComponent,
        pathMatch: 'full'
    },
    {
        path: "ForgotPassword/EnterUserCredentials",
        component: forgot_password_component_1.ResetPasswordComponent,
        pathMatch: 'full'
    },
    {
        path: "Registration/CreateUserId",
        component: registration_component_1.RegisterComponent,
        pathMatch: 'full'
    },
    {
        path: "Registration/NewAccount",
        component: new_account_component_1.NewAccountComponent,
        pathMatch: 'full'
    },
    {
        path: "Login/CreateSecurityQuestion",
        component: login_security_question_component_1.LoginSecurityQuestionComponent,
        pathMatch: 'full'
    },
    {
        path: "Login/CreateNewPassword",
        component: change_password_component_1.ChangePasswordComponent,
        pathMatch: 'full'
    },
    {
        path: "Registration/VerifyDefaultPassword",
        component: registration_validate_default_password_component_1.ValidateDefaultPasswordComponent,
        pathMatch: 'full'
    }
]);
//# sourceMappingURL=authentication.routes.js.map