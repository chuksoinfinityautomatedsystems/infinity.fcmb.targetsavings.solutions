"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var base_component_1 = require("../../commons/components/base.component");
var authentication_service_1 = require('../services/authentication.service');
var utils_services_1 = require("../../commons/services/utils.services");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
function _window() {
    return window;
}
var ChangePasswordComponent = (function (_super) {
    __extends(ChangePasswordComponent, _super);
    function ChangePasswordComponent(formBuilder, activatedRoute, router, authService, cdRef) {
        _super.call(this, authService, router);
        this.formBuilder = formBuilder;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.authService = authService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK PLC";
        this.textInputActive = this.errorMessage = "";
        this.changePasswordFormGroup = formBuilder.group({
            password: ['', forms_1.Validators.required],
            confirmPassword: ['', forms_1.Validators.required]
        });
        this.minimumPasswordLengthValidated = this.passwordHasUpperCaseCharacters =
            this.passwordHasLowerCaseCharacters = this.passwordHasOneOrMoreNumber = this.passwordHasValidSymbols = this.cancelModalOpened = false;
        this.passwordValidationPassedCount = 0;
        this.hideOTPToast = true;
        this.isErrorToast = false;
        this.authUserId = authentication_service_1.AuthenticationService.authUserID;
    }
    ChangePasswordComponent.prototype.ngOnInit = function () {
        if (!authentication_service_1.AuthenticationService.authUserID || !authentication_service_1.AuthenticationService.fromPage || !authentication_service_1.AuthenticationService.authOTP) {
            this.router.navigate(['Login']);
        }
        if (authentication_service_1.AuthenticationService.fromPage != appsettings_constant_1.Appsettings.LOGIN_FROM_PAGE && authentication_service_1.AuthenticationService.fromPage != appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE) {
            this.router.navigate(['Login']);
        }
        if (authentication_service_1.AuthenticationService.fromPage == appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE) {
            this.showSubmenuBullets = true;
        }
    };
    ChangePasswordComponent.prototype.ngOnDestroy = function () {
        if (this.queryParamSub) {
            this.queryParamSub.unsubscribe();
        }
    };
    ChangePasswordComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.formHasErrorOnSubmit = false;
        this.errorMessage = "";
        if (formControlName == "password") {
            this.showPasswordValidationWrap = true;
        }
        this.cdRef.detectChanges();
    };
    ChangePasswordComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.changePasswordFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
        }
        if (formControlName == "password" && this.passwordValidationPassedCount == 5) {
            this.showPasswordValidationWrap = false;
        }
        if (formControlName == "password" && !this.changePasswordFormGroup.controls['password'].value) {
            this.showPasswordValidationWrap = false;
        }
        this.cdRef.detectChanges();
    };
    ChangePasswordComponent.prototype.passwordInputKeyUp = function (value) {
        var currentPassword = this.changePasswordFormGroup.controls['password'].value;
        this.AutoPasswordValidation(currentPassword);
    };
    ChangePasswordComponent.prototype.AutoPasswordValidation = function (currentPassword) {
        this.passwordValidationPassedCount = 0;
        if (currentPassword.length > 7) {
            this.minimumPasswordLengthValidated = true;
            this.passwordValidationPassedCount = 1;
        }
        else {
            this.minimumPasswordLengthValidated = false;
        }
        if (utils_services_1.UtilService.StringContainsUpperCaseCharacters(currentPassword)) {
            this.passwordHasUpperCaseCharacters = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasUpperCaseCharacters = false;
        }
        if (utils_services_1.UtilService.StringContainsLowerCaseCharacters(currentPassword)) {
            this.passwordHasLowerCaseCharacters = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasLowerCaseCharacters = false;
        }
        if (utils_services_1.UtilService.StringContainsDigit(currentPassword)) {
            this.passwordHasOneOrMoreNumber = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasOneOrMoreNumber = false;
        }
        if (this.PasswordHasValidSymbol(currentPassword)) {
            this.passwordHasValidSymbols = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasValidSymbols = false;
        }
    };
    ChangePasswordComponent.prototype.SavePassword = function (event) {
        event.stopPropagation();
        this.formHasErrorOnSubmit = false;
        this.errorMessage = "";
        var self = this;
        if (this.changePasswordFormGroup.valid) {
            if (this.passwordValidationPassedCount < 5) {
                this.errorMessage = "Passwords does not meet the criteria.";
                this.formHasErrorOnSubmit = true;
            }
            else if (this.changePasswordFormGroup.controls['password'].value != this.changePasswordFormGroup.controls['confirmPassword'].value) {
                this.errorMessage = "There is a password mismatch.";
                this.formHasErrorOnSubmit = true;
            }
            else {
                // Call change password and go to login
                self.showBusyLoader = true;
                if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                    var loadingModalInterval_1 = setInterval(function () {
                        self.showBusyLoader = false;
                        self.cdRef.detectChanges();
                        self._changePassword({
                            oldPassword: authentication_service_1.AuthenticationService.authPass,
                            newPassword: self.changePasswordFormGroup.controls['password'].value,
                            otp: authentication_service_1.AuthenticationService.authOTP,
                            userId: authentication_service_1.AuthenticationService.authUserID
                        });
                        _window().clearInterval(loadingModalInterval_1);
                    }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
                }
                else {
                    self._changePassword({
                        oldPassword: authentication_service_1.AuthenticationService.authPass,
                        newPassword: self.changePasswordFormGroup.controls['password'].value,
                        otp: authentication_service_1.AuthenticationService.authOTP,
                        userId: authentication_service_1.AuthenticationService.authUserID
                    });
                }
            }
        }
        else {
            this.errorMessage = "Please enter the new password.";
            this.formHasErrorOnSubmit = true;
        }
    };
    ChangePasswordComponent.prototype._changePassword = function (newPasswordInformation) {
        var _this = this;
        var self = this;
        self.changePasswordSub = self.authService.changePassword(newPasswordInformation)
            .subscribe(function (response) {
            self.showBusyLoader = false;
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                _this.ngOnDestroy();
                _this.router.navigate(['Login']);
            }
            else {
                self.hideOTPToast = false;
                self.isErrorToast = true;
                self.toastMessage = response.ResponseFriendlyMessage;
                self.InitToastr();
                self.cdRef.detectChanges();
            }
        }, function (error) {
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    ChangePasswordComponent.prototype.PasswordHasValidSymbol = function (password) {
        if (password.indexOf("@") >= 0 ||
            password.indexOf("#") >= 0 ||
            password.indexOf("$") >= 0 ||
            password.indexOf("%") >= 0 ||
            password.indexOf("^") >= 0 ||
            password.indexOf("\\") >= 0) {
            return true;
        }
        return false;
    };
    ChangePasswordComponent.prototype.cancelChangePasswordModal = function () {
        this.cancelModalOpened = true;
    };
    ChangePasswordComponent.prototype.hideCancelModal = function () {
        this.cancelModalOpened = false;
    };
    ChangePasswordComponent.prototype.backToLogin = function () {
        this.ngOnDestroy();
        this.router.navigate(['Login']);
    };
    ChangePasswordComponent = __decorate([
        core_1.Component({
            templateUrl: "html/authentication/change-password.html"
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.ActivatedRoute, router_1.Router, (typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, core_1.ChangeDetectorRef])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
    var _a;
}(base_component_1.BaseComponent));
exports.ChangePasswordComponent = ChangePasswordComponent;
//# sourceMappingURL=change-password.component.js.map