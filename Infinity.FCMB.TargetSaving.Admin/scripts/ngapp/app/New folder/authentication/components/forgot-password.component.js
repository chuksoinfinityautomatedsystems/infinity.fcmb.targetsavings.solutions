"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var authentication_service_1 = require('../services/authentication.service');
var utils_services_1 = require("../../commons/services/utils.services");
var base_component_1 = require("../../commons/components/base.component");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
function _window() {
    return window;
}
var ResetPasswordComponent = (function (_super) {
    __extends(ResetPasswordComponent, _super);
    function ResetPasswordComponent(formBuilder, activatedRoute, router, authService, cdRef) {
        _super.call(this, authService, router);
        this.formBuilder = formBuilder;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.authService = authService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK PLC";
        this.textInputActive = "";
        this.resetPasswordFormGroup = formBuilder.group({
            accountNo: ['', forms_1.Validators.required],
            userID: ['', forms_1.Validators.required]
        });
        this.securityQuestionFormGroup = formBuilder.group({
            questionA: ['', forms_1.Validators.required],
            questionB: ['', forms_1.Validators.required],
            questionC: ['', forms_1.Validators.required]
        });
        this.changePasswordFormGroup = formBuilder.group({
            password: ['', forms_1.Validators.required],
            confirmPassword: ['', forms_1.Validators.required]
        });
        this.formSubmitted = false;
        this.pageIndex = 1;
        this.pageSubtitle = "Identify yourself";
        this.minimumPasswordLengthValidated = this.passwordHasUpperCaseCharacters =
            this.passwordHasLowerCaseCharacters = this.passwordHasOneOrMoreNumber = this.passwordHasValidSymbols = false;
        this.passwordValidationPassedCount = 0;
        this.hideOTPToast = true;
        this.isErrorToast = false;
        //OTP INIT
        this.otpFormGroup = formBuilder.group({
            otpModel1: ['', forms_1.Validators.required],
            otpModel2: ['', forms_1.Validators.required],
            otpModel3: ['', forms_1.Validators.required],
            otpModel4: ['', forms_1.Validators.required],
            otpModel5: ['', forms_1.Validators.required]
        });
        this.otpValues = ["", "", "", "", ""];
        this.showOtpError = this.otpFormSubmitted = this.showOtpView = false;
    }
    ResetPasswordComponent.prototype.nextToSecurityView = function (event) {
        event.stopPropagation();
        this.formSubmitted = true;
        if (this.resetPasswordFormGroup.valid) {
            this.pageIndex = 2;
            this.pageSubtitle = "Answer security questions";
            this.formSubmitted = false;
            this.authID = this.resetPasswordFormGroup.controls["userID"].value;
        }
    };
    ResetPasswordComponent.prototype.nextToChangePasswordView = function (event) {
        event.stopPropagation();
        this.formSubmitted = true;
        var self = this;
        if (this.securityQuestionFormGroup.valid) {
            if (utils_services_1.UtilService.StringIsNullOrEmpty(self.securityQuestionFormGroup.controls["questionA"].value) ||
                utils_services_1.UtilService.StringIsNullOrEmpty(self.securityQuestionFormGroup.controls["questionB"].value) ||
                utils_services_1.UtilService.StringIsNullOrEmpty(self.securityQuestionFormGroup.controls["questionC"].value)) {
                self.hideOTPToast = false;
                self.isErrorToast = true;
                self.toastMessage = "Please enter all the question answers";
                self.InitToastr();
                self.cdRef.detectChanges();
            }
            else {
                this.pageIndex = 3;
                this.pageSubtitle = "Change password";
                this.formSubmitted = false;
            }
        }
    };
    ResetPasswordComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        if (this.pageIndex == 3) {
            this.formHasErrorOnSubmit = false;
            this.errorMessage = "";
            if (formControlName == "password") {
                this.showPasswordValidationWrap = true;
            }
        }
        this.cdRef.detectChanges();
    };
    ResetPasswordComponent.prototype.textInputBlur = function (formControlName) {
        if (this.pageIndex == 1 && !this.resetPasswordFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
        }
        else if (this.pageIndex == 2 && !this.securityQuestionFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
        }
        else if (this.pageIndex == 3 && !this.changePasswordFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
        }
        if (this.pageIndex == 3 && formControlName == "password" && this.passwordValidationPassedCount == 5) {
            this.showPasswordValidationWrap = false;
        }
        if (this.pageIndex == 3 && formControlName == "password" && !this.changePasswordFormGroup.controls['password'].value) {
            this.showPasswordValidationWrap = false;
        }
        this.cdRef.detectChanges();
    };
    ResetPasswordComponent.prototype.passwordInputKeyUp = function (value) {
        var currentPassword = this.changePasswordFormGroup.controls['password'].value;
        this.AutoPasswordValidation(currentPassword);
    };
    ResetPasswordComponent.prototype.AutoPasswordValidation = function (currentPassword) {
        this.passwordValidationPassedCount = 0;
        if (currentPassword.length > 7) {
            this.minimumPasswordLengthValidated = true;
            this.passwordValidationPassedCount = 1;
        }
        else {
            this.minimumPasswordLengthValidated = false;
        }
        if (utils_services_1.UtilService.StringContainsUpperCaseCharacters(currentPassword)) {
            this.passwordHasUpperCaseCharacters = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasUpperCaseCharacters = false;
        }
        if (utils_services_1.UtilService.StringContainsLowerCaseCharacters(currentPassword)) {
            this.passwordHasLowerCaseCharacters = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasLowerCaseCharacters = false;
        }
        if (utils_services_1.UtilService.StringContainsDigit(currentPassword)) {
            this.passwordHasOneOrMoreNumber = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasOneOrMoreNumber = false;
        }
        if (this.PasswordHasValidSymbol(currentPassword)) {
            this.passwordHasValidSymbols = true;
            this.passwordValidationPassedCount += 1;
        }
        else {
            this.passwordHasValidSymbols = false;
        }
    };
    ResetPasswordComponent.prototype.PasswordHasValidSymbol = function (password) {
        if (password.indexOf("@") >= 0 ||
            password.indexOf("#") >= 0 ||
            password.indexOf("$") >= 0 ||
            password.indexOf("%") >= 0 ||
            password.indexOf("^") >= 0 ||
            password.indexOf("\\") >= 0) {
            return true;
        }
        return false;
    };
    ResetPasswordComponent.prototype.enterUserCredentialsToSecurityQuestions = function () {
        this.formSubmitted = true;
        // if (this.resetPasswordFormGroup.valid) {
        //     this.exceptionMessage = "You have entered invalid details, please enter the correct details.";
        //     this.showExceptionPage = true;
        //     //this.requestOngoing = true;
        // }
        return false;
    };
    ResetPasswordComponent.prototype.accountNoKeyDown = function (e) {
        var key = e.charCode || e.keyCode || 0;
        if (e.shiftKey) {
            return false;
        }
        return (
        // numbers
        (key >= 48 && key <= 57) ||
            // Numeric keypad
            (key >= 96 && key <= 105) ||
            // Backspace and Tab and Enter
            key == 8 || key == 9 || key == 13 ||
            // Home and End
            key == 35 || key == 36 ||
            // left and right arrows
            key == 37 || key == 39 ||
            // up and down arrows
            key == 38 || key == 40 ||
            // Del and Ins
            key == 46 || key == 45);
    };
    ResetPasswordComponent.prototype.ResetPassword = function (event) {
        event.stopPropagation();
        this.formHasErrorOnSubmit = false;
        this.errorMessage = "";
        var self = this;
        if (this.changePasswordFormGroup.valid) {
            if (this.passwordValidationPassedCount < 5) {
                this.errorMessage = "Passwords does not meet the criteria.";
                this.formHasErrorOnSubmit = true;
            }
            else if (this.changePasswordFormGroup.controls['password'].value != this.changePasswordFormGroup.controls['confirmPassword'].value) {
                this.errorMessage = "There is a password mismatch.";
                this.formHasErrorOnSubmit = true;
            }
            else {
                // Call change password and go to login
                self.showBusyLoader = true;
                if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                    var loadingModalInterval_1 = setInterval(function () {
                        self.showBusyLoader = false;
                        self.cdRef.detectChanges();
                        // self._ResetPassword({
                        //     oldPassword: AuthenticationService.authPass,
                        //     newPassword: self.changePasswordFormGroup.controls['password'].value,
                        //     otp: AuthenticationService.authOTP,
                        //     userId: AuthenticationService.authUserID
                        // });
                        self._resendOTP();
                        _window().clearInterval(loadingModalInterval_1);
                    }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
                }
                else {
                    // self._ResetPassword({
                    //     oldPassword: AuthenticationService.authPass,
                    //     newPassword: self.changePasswordFormGroup.controls['password'].value,
                    //     otp: AuthenticationService.authOTP,
                    //     userId: AuthenticationService.authUserID
                    // });
                    self._resendOTP();
                }
            }
        }
        else {
            this.errorMessage = "Please enter the new password.";
            this.formHasErrorOnSubmit = true;
        }
    };
    ResetPasswordComponent.prototype._ResetPassword = function (newPasswordInformation) {
        var _this = this;
        var self = this;
        self.resetPasswordSub = self.authService.changePassword(newPasswordInformation)
            .subscribe(function (response) {
            self.showBusyLoader = false;
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                _this.ngOnDestroy();
                _this.router.navigate(['Login']);
            }
            else {
                self.hideOTPToast = false;
                self.isErrorToast = true;
                self.toastMessage = response.ResponseFriendlyMessage;
                self.InitToastr();
                self.cdRef.detectChanges();
            }
        }, function (error) {
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    // OTP SNIPPETS.
    ResetPasswordComponent.prototype.closeOtpView = function () {
        this.clearOTPValues();
        this.showOtpView = false;
        this.otpFormSubmitted = false;
        this.hideOTPToast = true;
        this.cdRef.detectChanges();
    };
    ResetPasswordComponent.prototype.clearOTPValues = function () {
        this.otpFormGroup.controls["otpModel1"].setValue("");
        this.otpFormGroup.controls["otpModel2"].setValue("");
        this.otpFormGroup.controls["otpModel3"].setValue("");
        this.otpFormGroup.controls["otpModel4"].setValue("");
        this.otpFormGroup.controls["otpModel5"].setValue("");
        $("#otpModel1").focus();
        this.otpValues[0] = this.otpValues[1] = this.otpValues[2] = this.otpValues[3] = this.otpValues[4] = "";
        this.showOtpError = false;
        this.cdRef.detectChanges();
    };
    ResetPasswordComponent.prototype.preventArrow = function (event) {
        var key = event.charCode || event.keyCode || 0;
        if (key > 36 && key < 41) {
            event.preventDefault();
        }
    };
    ResetPasswordComponent.prototype.goNext = function (event, index) {
        var key = event.charCode || event.keyCode || 0;
        // Shift Key : do nada
        if (event.shiftKey) {
            return false;
        }
        else if (key == 8 || key == 46 || key == 45) {
            this.clearOTPValues();
            this.showOtpError = true;
        }
        else if (key == 9) {
            if (!this.otpValues[index - 1]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else if (key > 36 && key < 41) {
            if (index == 5 && !this.otpValues[4]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else {
            //save value and replace with * on txtbox view (if not empty); move to next tab
            if (index == 1) {
                if (this.otpFormGroup.controls["otpModel1"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel1"].value)) {
                    this.otpValues[0] = this.otpFormGroup.controls["otpModel1"].value;
                    this.otpFormGroup.controls["otpModel1"].setValue("*");
                }
                else {
                    this.otpValues[0] = "";
                }
                $("#otpModel2").focus();
            }
            else if (index == 2) {
                if (this.otpFormGroup.controls["otpModel2"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel2"].value)) {
                    this.otpValues[1] = this.otpFormGroup.controls["otpModel2"].value;
                    this.otpFormGroup.controls["otpModel2"].setValue("*");
                }
                else {
                    this.otpValues[1] = "";
                }
                $("#otpModel3").focus();
            }
            else if (index == 3) {
                if (this.otpFormGroup.controls["otpModel3"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel3"].value)) {
                    this.otpValues[2] = this.otpFormGroup.controls["otpModel3"].value;
                    this.otpFormGroup.controls["otpModel3"].setValue("*");
                }
                else {
                    this.otpValues[2] = "";
                }
                $("#otpModel4").focus();
            }
            else if (index == 4) {
                if (this.otpFormGroup.controls["otpModel4"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel4"].value)) {
                    this.otpValues[3] = this.otpFormGroup.controls["otpModel4"].value;
                    this.otpFormGroup.controls["otpModel4"].setValue("*");
                }
                else {
                    this.otpValues[3] = "";
                }
                $("#otpModel5").focus();
            }
            else if (index == 5) {
                if (this.otpFormGroup.controls["otpModel5"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel5"].value)) {
                    this.otpValues[4] = this.otpFormGroup.controls["otpModel5"].value;
                    this.otpFormGroup.controls["otpModel5"].setValue("*");
                }
                else {
                    this.otpValues[4] = "";
                }
            }
            //invalidate otp form if last otp xter is empty
            if (this.otpValues[4]) {
                this.showOtpError = false;
            }
            else {
                this.showOtpError = true;
            }
        }
    };
    ResetPasswordComponent.prototype._resendOTP = function () {
        var self = this;
        self.isErrorToast = true;
        self.toastMessage = "";
        self.cdRef.detectChanges();
        if (this.initiateOTPSub) {
            this.initiateOTPSub.unsubscribe();
        }
        self.initiateOTPSub = self.authService.initiateOTP(self.authID, "self.user.CifID", appsettings_constant_1.Appsettings.OTP_REASON_CODE_RESET_PASSWORD)
            .subscribe(function (initiateOtpResponse) {
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.showBusyLoader = false;
                self.hideOTPToast = false;
                self.isErrorToast = false;
                self.toastMessage = appsettings_constant_1.Appsettings.OTP_SENT_MESSAGE;
                self.showOtpView = true;
            }
            else {
                self.hideOTPToast = false;
                self.isErrorToast = true;
                self.toastMessage = initiateOtpResponse.ResponseFriendlyMessage;
            }
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function (error) {
            self.showBusyLoader = false;
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    ResetPasswordComponent.prototype.reSendOtp = function () {
        var self = this;
        //re-init toast container
        self.hideOTPToast = true;
        self.isErrorToast = true;
        self.cdRef.detectChanges();
        // $("#toast-container").css("pointer-events: auto;"); 
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            self.showBusyLoader = true;
            self.cdRef.detectChanges();
            var loadingModalInterval_2 = setInterval(function () {
                self.showBusyLoader = false;
                self.cdRef.detectChanges();
                self._resendOTP();
                _window().clearInterval(loadingModalInterval_2);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._resendOTP();
        }
    };
    ResetPasswordComponent.prototype.resetPassword = function (event) {
        event.stopPropagation();
        var self = this;
        self.otpFormSubmitted = true;
        this.isErrorToast = true;
        //re-init toast container
        self.hideOTPToast = true;
        $("#toast-container").css("pointer-events: auto;");
        self.cdRef.detectChanges();
        if (!self.otpValues[4] || (self.otpValues[4] && utils_services_1.UtilService.StringIsNullOrEmpty(self.otpValues[4]))) {
            self.showOtpError = true;
            self.cdRef.detectChanges();
        }
        else {
            self.showBusyLoader = true;
            self.cdRef.detectChanges();
            var otp = self.otpValues.join("");
            if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                var loadingModalInterval_3 = setInterval(function () {
                    self.showBusyLoader = false;
                    self.cdRef.detectChanges();
                    // self.authenticateUserToken(otp); 
                    _window().clearInterval(loadingModalInterval_3);
                }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
            }
            else {
            }
        }
    };
    ResetPasswordComponent.prototype.backHome = function () {
        this.router.navigate(['login']);
        return false;
    };
    ResetPasswordComponent.prototype.arrowClick = function () {
        this.router.navigate(['login']);
        // navigate to registration page
        return false;
    };
    ResetPasswordComponent.prototype.continue = function () {
        return false;
    };
    ResetPasswordComponent.prototype.ngOnInit = function () {
        this.authService.logout();
        if (authentication_service_1.AuthenticationService.passwordChanged) {
        }
    };
    ResetPasswordComponent.prototype.ngOnDestroy = function () {
        //this.subscription.unsubscribe();
    };
    ResetPasswordComponent = __decorate([
        core_1.Component({
            templateUrl: "html/authentication/reset-password.html"
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.ActivatedRoute, router_1.Router, (typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, core_1.ChangeDetectorRef])
    ], ResetPasswordComponent);
    return ResetPasswordComponent;
    var _a;
}(base_component_1.BaseComponent));
exports.ResetPasswordComponent = ResetPasswordComponent;
//# sourceMappingURL=forgot-password.component.js.map