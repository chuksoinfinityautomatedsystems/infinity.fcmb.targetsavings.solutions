"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var authentication_service_1 = require('../services/authentication.service');
var utils_services_1 = require("../../commons/services/utils.services");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var base_component_1 = require("../../commons/components/base.component");
function _window() {
    return window;
}
var RegisterComponent = (function (_super) {
    __extends(RegisterComponent, _super);
    function RegisterComponent(formBuilder, activatedRoute, router, authService, cdRef) {
        _super.call(this, authService, router);
        this.formBuilder = formBuilder;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.authService = authService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK PLC";
        this.registerFormGroup = formBuilder.group({
            accountNo: ['', forms_1.Validators.required],
            termsConditions: [false]
        });
        this.showExceptionPage = this.formSubmitted = this.termsAndConditionUnChecked = this.accounNumIsWhitespace = false;
        this.hideOTPToast = true;
        this.isErrorToast = false;
        this.textInputActive = "";
    }
    RegisterComponent.prototype.backHome = function () {
        this.router.navigate(['login']);
        return false;
    };
    RegisterComponent.prototype.closeModal = function () {
    };
    RegisterComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.cdRef.detectChanges();
    };
    RegisterComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.registerFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
    };
    RegisterComponent.prototype.nextToSecurityView = function (event) {
        event.stopPropagation();
        var self = this;
        self.formSubmitted = true;
        self.termsAndConditionUnChecked = self.accounNumIsWhitespace = false;
        self.cdRef.detectChanges();
        if (self.registerFormGroup.valid) {
            if (self.registerFormGroup.controls["accountNo"].value &&
                utils_services_1.UtilService.trim(self.registerFormGroup.controls["accountNo"].value).length < 1) {
                self.accounNumIsWhitespace = true;
                return false;
            }
            else if (self.registerFormGroup.controls["termsConditions"].value == false) {
                self.termsAndConditionUnChecked = true;
                return false;
            }
            else {
                self.showBusyLoader = true;
                self.cdRef.detectChanges();
                if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                    var loadingModalInterval_1 = setInterval(function () {
                        self.showBusyLoader = false;
                        self.cdRef.detectChanges();
                        self._confirmUserAccount(self.registerFormGroup.controls["accountNo"].value);
                        _window().clearInterval(loadingModalInterval_1);
                    }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
                }
                else {
                    self._confirmUserAccount(self.registerFormGroup.controls["accountNo"].value);
                }
            }
        }
    };
    RegisterComponent.prototype._confirmUserAccount = function (accountNo) {
        var self = this;
        self.validateAccountSub = self.authService.confirmIfUserHasIBProfile(accountNo)
            .subscribe(function (response) {
            self.showBusyLoader = false;
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                authentication_service_1.AuthenticationService.fromPage = appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE;
                authentication_service_1.AuthenticationService.authUserID = self.registerFormGroup.controls["accountNo"].value;
                self.cdRef.detectChanges();
                self.ngOnDestroy();
                self.router.navigate(['Login/CreateSecurityQuestion']);
            }
            else {
                self.hideOTPToast = false;
                self.isErrorToast = true;
                self.toastMessage = response.ResponseFriendlyMessage;
                self.cdRef.detectChanges();
                self.InitToastr();
            }
        }, function (error) {
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
        // self.validateAccountSub = self.authService.getSecurityQuestions()
        //     .subscribe(response => {
        //         self.showBusyLoader = false;
        //         if (response.ResponseCode == ResponseCodes.SUCCESS) {
        //             AuthenticationService.fromPage = Appsettings.REGISTER_USER_FROM_PAGE;
        //             AuthenticationService.authUserID = self.registerFormGroup.controls["accountNo"].value;
        //             self.cdRef.detectChanges();
        //             self.router.navigate(['Login/CreateSecurityQuestion']);
        //         }
        //         else {
        //             self.hideOTPToast = false; 
        //             self.isErrorToast = true;    
        //             self.toastMessage = response.ResponseFriendlyMessage;
        //             self.cdRef.detectChanges();
        //             self.InitToastr();
        //         }
        //     },
        //     (error: any) => {
        //         self.hideOTPToast = false;
        //         self.isErrorToast = true;
        //         self.toastMessage = Appsettings.TECHNICAL_ERROR_MESSAGE; 
        //         self.showBusyLoader = false;
        //         self.cdRef.detectChanges();
        //         self.InitToastr();
        //     },
        //     () => {
        //         self.showBusyLoader = false;
        //         self.cdRef.detectChanges();
        //     });
    };
    RegisterComponent.prototype.ngOnDestroy = function () {
        if (this.validateAccountSub) {
            this.validateAccountSub.unsubscribe();
        }
    };
    RegisterComponent = __decorate([
        core_1.Component({
            templateUrl: "html/authentication/registration.html"
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.ActivatedRoute, router_1.Router, (typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, core_1.ChangeDetectorRef])
    ], RegisterComponent);
    return RegisterComponent;
    var _a;
}(base_component_1.BaseComponent));
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=registration.component.js.map