"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var authentication_service_1 = require('../services/authentication.service');
var utils_services_1 = require("../../commons/services/utils.services");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var base_component_1 = require("../../commons/components/base.component");
var user_model_1 = require("../models/user.model");
function _window() {
    return window;
}
var LoginComponent = (function (_super) {
    __extends(LoginComponent, _super);
    function LoginComponent(formBuilder, activatedRoute, router, authService, cdRef) {
        _super.call(this, authService, router);
        this.formBuilder = formBuilder;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.authService = authService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK PLC";
        // LOGIN FORM INIT
        this.loginFormGroup = formBuilder.group({
            userID: ['', forms_1.Validators.required],
            password: ['', forms_1.Validators.required]
        });
        this.shakeLoginBoxOnError = false;
        this.loginSubmitClicked = undefined;
        this.textInputActive = this.loginErrorMessage = "";
        this.showLoginError = false;
        //OTP INIT
        this.otpFormGroup = formBuilder.group({
            otpModel1: ['', forms_1.Validators.required],
            otpModel2: ['', forms_1.Validators.required],
            otpModel3: ['', forms_1.Validators.required],
            otpModel4: ['', forms_1.Validators.required],
            otpModel5: ['', forms_1.Validators.required]
        });
        this.otpValues = ["", "", "", "", ""];
        this.showOtpError = this.otpFormSubmitted = this.showOtpView = false;
        this.hideOTPToast = true;
        this.isErrorToast = false;
        // HELP MODALS INIT 
        this.showDisclaimer = this.showHelp = this.showPrivacy = this.showTerms = false;
        this.animatePageModalPopupVisibility = false;
        this.loginPageModalPopupTitle = "";
        this.isTimeoutLogout = this.requestOngoing = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.initialNoErrorMessage = authentication_service_1.AuthenticationService.authLogoutMessage;
    };
    // MODAL FUNCTIONS
    LoginComponent.prototype.hideModalContent = function () {
        this.showDisclaimer = this.showHelp = this.showPrivacy = this.showTerms = false;
        this.cdRef.detectChanges();
    };
    LoginComponent.prototype.closeModal = function () {
        if ($('.tc-container') && $('.tc-container').length > 0) {
            $('.tc-container')[0].scrollTop = 0;
        }
        this.animatePageModalPopupVisibility = false;
        this.hideModalContent();
        this.loginPageModalPopupTitle = "";
        this.cdRef.detectChanges();
    };
    LoginComponent.prototype.openDisclaimerDialog = function () {
        this.hideModalContent();
        this.animatePageModalPopupVisibility = true;
        this.showDisclaimer = true;
        this.loginPageModalPopupTitle = "Disclaimer";
        this.cdRef.detectChanges();
    };
    LoginComponent.prototype.openPrivacyDialog = function () {
        this.hideModalContent();
        this.animatePageModalPopupVisibility = true;
        this.showPrivacy = true;
        this.loginPageModalPopupTitle = "Privacy & Security";
        this.cdRef.detectChanges();
    };
    LoginComponent.prototype.openTermsDialog = function () {
        this.hideModalContent();
        this.animatePageModalPopupVisibility = true;
        this.showTerms = true;
        this.loginPageModalPopupTitle = "Terms & Conditions";
        this.cdRef.detectChanges();
    };
    LoginComponent.prototype.openHelpDialog = function () {
        this.hideModalContent();
        this.animatePageModalPopupVisibility = true;
        this.showHelp = true;
        this.loginPageModalPopupTitle = "Need Help";
        this.cdRef.detectChanges();
    };
    // LOGIN SNIPPET    
    LoginComponent.prototype.clearLoginValues = function () {
        this.loginFormGroup.controls['userID'].setValue("");
        this.loginFormGroup.controls['password'].setValue("");
        this.cdRef.detectChanges();
    };
    LoginComponent.prototype.textInputFocus = function (formControlName) {
        this.showLoginError = false;
        this.textInputActive = formControlName;
        this.initialNoErrorMessage = authentication_service_1.AuthenticationService.authLogoutMessage = "";
        this.cdRef.detectChanges();
    };
    LoginComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.loginFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
    };
    LoginComponent.prototype.LoginUser = function () {
        var self = this;
        self.loginSubmitClicked = true;
        self.shakeLoginBoxOnError = false;
        self.showLoginError = self.requestError = false;
        self.loginErrorMessage = "";
        self.cdRef.detectChanges();
        if (!self.loginFormGroup.valid ||
            ((self.loginFormGroup.valid) && (utils_services_1.UtilService.trim(self.loginFormGroup.controls['userID'].value).length < 1 ||
                utils_services_1.UtilService.trim(self.loginFormGroup.controls['password'].value).length < 1))) {
            self.shakeLoginBoxOnError = true;
            self.cdRef.detectChanges();
            var tranxModalInterval_1 = setInterval(function () {
                self.shakeLoginBoxOnError = false;
                self.cdRef.detectChanges();
                _window().clearInterval(tranxModalInterval_1);
            }, appsettings_constant_1.Appsettings.LOGIN_ERROR_SHAKE_INTERVAL);
        }
        else {
            self.showBusyLoader = true;
            self.requestOngoing = true;
            self.cdRef.detectChanges();
            if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                var loadingModalInterval_1 = setInterval(function () {
                    self.requestOngoing = false;
                    self.showBusyLoader = false;
                    self.cdRef.detectChanges();
                    self.authenticateUser();
                    _window().clearInterval(loadingModalInterval_1);
                }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
            }
            else {
                self.authenticateUser();
            }
        }
        return false;
    };
    LoginComponent.prototype.authenticateUser = function () {
        var self = this;
        self.user = new user_model_1.User();
        self.user.UserId = utils_services_1.UtilService.trim(self.loginFormGroup.controls['userID'].value);
        self.user.Password = utils_services_1.UtilService.trim(self.loginFormGroup.controls['password'].value);
        self.loginSub = self.authService.authenticateUserID(self.user)
            .subscribe(function (response) {
            self.user.CifID = response.CifId;
            self.user.LastLoginIn = response.lastLoginIn;
            self.user.SessionId = response.SessionId;
            self.user.Reference = response.OTPResponseDescription; // encrypted
            self.user.LastFourPhoneDigit = response.last_four_digit_of_customer_phone_no;
            self.user.LastLoginIn = response.LastLoginIn;
            var sendOTP = false;
            self.showBusyLoader = false;
            self.requestOngoing = false;
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS || response.ResponseCode == response_codes_constant_1.ResponseCodes.LoginSuccessWithLimitedAccess) {
                self.authService.setPartialLoginToken();
                self.user.LoginStatus = response_codes_constant_1.ResponseCodes.SUCCESS;
                if (response.ResponseCode == response_codes_constant_1.ResponseCodes.LoginSuccessWithLimitedAccess) {
                    authentication_service_1.AuthenticationService.limitedAccess = true;
                }
                self.showOtpView = true;
            }
            else if (response.ResponseCode == response_codes_constant_1.ResponseCodes.PasswordChangeRequired) {
                self.user.LoginStatus = response_codes_constant_1.ResponseCodes.PasswordChangeRequired;
                self.showOtpView = true;
            }
            else if (response.ResponseCode == response_codes_constant_1.ResponseCodes.QuestionAndPasswordRegistrationRequired) {
                self.user.LoginStatus = response_codes_constant_1.ResponseCodes.QuestionAndPasswordRegistrationRequired;
                self.showOtpView = true;
            }
            else {
                self.showLoginError = self.requestError = true;
                self.loginErrorMessage = response.ResponseFriendlyMessage;
                self.shakeLoginBoxOnError = true;
                self.clearLoginValues();
                self.showBusyLoader = false;
                var tranxModalInterval_2 = setInterval(function () {
                    self.shakeLoginBoxOnError = false;
                    self.cdRef.detectChanges();
                    _window().clearInterval(tranxModalInterval_2);
                }, 500);
            }
            self.cdRef.detectChanges();
            //  if (sendOTP) {
            //      self.initiateOTPSub = self.authService.initiateOTP(self.user.UserId)
            //          .subscribe(initiateOtpResponse => {
            //              if (initiateOtpResponse.ResponseCode == ResponseCodes.SUCCESS) {
            //                  self.showOtpView = true;
            //                  sendOTP = false;
            //              }                      
            //          },
            //          (error: any) => {
            //              self.showLoginError = true;
            //              self.loginErrorMessage = Appsettings.TECHNICAL_ERROR_MESSAGE;
            //              self.requestOngoing = false;
            //              self.showBusyLoader = false;
            //              self.cdRef.detectChanges();
            //          },
            //          () => {
            //              self.showBusyLoader = false;
            //              self.requestOngoing = false;
            //              self.cdRef.detectChanges();
            //          });
            //  }
        }, function (error) {
            self.showLoginError = true;
            self.loginErrorMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.requestOngoing = false;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    //LINK TO OTHER PAGES
    LoginComponent.prototype.openForgotPasswordWindow = function (event) {
        event.stopPropagation();
        this.router.navigate(['ForgotPassword/EnterUserCredentials']);
        return false;
    };
    LoginComponent.prototype.openRegistrationWindow = function (event) {
        event.stopPropagation();
        this.router.navigate(['Registration/CreateUserId']);
        return false;
    };
    LoginComponent.prototype.openNewAccountWindow = function (event) {
        event.stopPropagation();
        this.router.navigate(['Registration/NewAccount']);
        return false;
    };
    // OTP SNIPPETS.
    LoginComponent.prototype.closeOtpView = function () {
        this.clearOTPValues();
        this.showOtpView = false;
        this.otpFormSubmitted = false;
        this.hideOTPToast = true;
        this.cdRef.detectChanges();
    };
    LoginComponent.prototype.clearOTPValues = function () {
        this.otpFormGroup.controls["otpModel1"].setValue("");
        this.otpFormGroup.controls["otpModel2"].setValue("");
        this.otpFormGroup.controls["otpModel3"].setValue("");
        this.otpFormGroup.controls["otpModel4"].setValue("");
        this.otpFormGroup.controls["otpModel5"].setValue("");
        $("#otpModel1").focus();
        this.otpValues[0] = this.otpValues[1] = this.otpValues[2] = this.otpValues[3] = this.otpValues[4] = "";
        this.showOtpError = false;
        this.cdRef.detectChanges();
    };
    LoginComponent.prototype.preventArrow = function (event) {
        var key = event.charCode || event.keyCode || 0;
        if (key > 36 && key < 41) {
            event.preventDefault();
        }
    };
    LoginComponent.prototype.goNext = function (event, index) {
        var key = event.charCode || event.keyCode || 0;
        // Shift Key : do nada
        if (event.shiftKey) {
            return false;
        }
        else if (key == 8 || key == 46 || key == 45) {
            this.clearOTPValues();
            this.showOtpError = true;
        }
        else if (key == 9) {
            if (!this.otpValues[index - 1]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else if (key > 36 && key < 41) {
            if (index == 5 && !this.otpValues[4]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else {
            //save value and replace with * on txtbox view (if not empty); move to next tab
            if (index == 1) {
                if (this.otpFormGroup.controls["otpModel1"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel1"].value)) {
                    this.otpValues[0] = this.otpFormGroup.controls["otpModel1"].value;
                    this.otpFormGroup.controls["otpModel1"].setValue("*");
                }
                else {
                    this.otpValues[0] = "";
                }
                $("#otpModel2").focus();
            }
            else if (index == 2) {
                if (this.otpFormGroup.controls["otpModel2"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel2"].value)) {
                    this.otpValues[1] = this.otpFormGroup.controls["otpModel2"].value;
                    this.otpFormGroup.controls["otpModel2"].setValue("*");
                }
                else {
                    this.otpValues[1] = "";
                }
                $("#otpModel3").focus();
            }
            else if (index == 3) {
                if (this.otpFormGroup.controls["otpModel3"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel3"].value)) {
                    this.otpValues[2] = this.otpFormGroup.controls["otpModel3"].value;
                    this.otpFormGroup.controls["otpModel3"].setValue("*");
                }
                else {
                    this.otpValues[2] = "";
                }
                $("#otpModel4").focus();
            }
            else if (index == 4) {
                if (this.otpFormGroup.controls["otpModel4"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel4"].value)) {
                    this.otpValues[3] = this.otpFormGroup.controls["otpModel4"].value;
                    this.otpFormGroup.controls["otpModel4"].setValue("*");
                }
                else {
                    this.otpValues[3] = "";
                }
                $("#otpModel5").focus();
            }
            else if (index == 5) {
                if (this.otpFormGroup.controls["otpModel5"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel5"].value)) {
                    this.otpValues[4] = this.otpFormGroup.controls["otpModel5"].value;
                    this.otpFormGroup.controls["otpModel5"].setValue("*");
                }
                else {
                    this.otpValues[4] = "";
                }
            }
            //invalidate otp form if last otp xter is empty
            if (this.otpValues[4]) {
                this.showOtpError = false;
            }
            else {
                this.showOtpError = true;
            }
        }
    };
    LoginComponent.prototype.authenticateUserToken = function (otp) {
        var self = this;
        self.user.Otp = otp;
        self.loginOTPSub = self.authService.validateLoginOTP(self.user)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                authentication_service_1.AuthenticationService.authOTP = self.user.Otp;
                self.ngOnDestroy();
                self.showOtpView = false;
                if (self.user.LoginStatus == response_codes_constant_1.ResponseCodes.SUCCESS) {
                    self.authService.setAccessToken(self.user.SessionId);
                    authentication_service_1.AuthenticationService.authUserObj = self.user;
                    //          self.showBusyLoader = false;
                    // self.requestOngoing = false;
                    // self.cdRef.detectChanges();
                    self.router.navigate(['dashboard']);
                }
                else if (self.user.LoginStatus == response_codes_constant_1.ResponseCodes.QuestionAndPasswordRegistrationRequired) {
                    authentication_service_1.AuthenticationService.authUserID = utils_services_1.UtilService.trim(self.loginFormGroup.controls['userID'].value);
                    authentication_service_1.AuthenticationService.authPass = utils_services_1.UtilService.trim(self.loginFormGroup.controls['password'].value);
                    authentication_service_1.AuthenticationService.fromPage = appsettings_constant_1.Appsettings.LOGIN_FROM_PAGE;
                    self.router.navigate(['Login/CreateSecurityQuestion']);
                }
                else if (self.user.LoginStatus == response_codes_constant_1.ResponseCodes.PasswordChangeRequired) {
                    authentication_service_1.AuthenticationService.authUserID = utils_services_1.UtilService.trim(self.loginFormGroup.controls['userID'].value);
                    authentication_service_1.AuthenticationService.authPass = utils_services_1.UtilService.trim(self.loginFormGroup.controls['password'].value);
                    authentication_service_1.AuthenticationService.fromPage = appsettings_constant_1.Appsettings.LOGIN_FROM_PAGE;
                    // self.router.navigate(['Login/CreateNewPassword']);       
                    self.router.navigate(['Login/CreateSecurityQuestion']);
                }
            }
            else {
                //show error toast
                self.hideOTPToast = false;
                self.isErrorToast = true;
                self.toastMessage = response.ResponseFriendlyMessage;
                self.cdRef.detectChanges();
                self.InitToastr();
            }
        }, function (error) {
            self.showLoginError = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.requestOngoing = false;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.requestOngoing = false;
            self.cdRef.detectChanges();
        });
    };
    LoginComponent.prototype.validateLoginOTP = function (event) {
        event.stopPropagation();
        var self = this;
        self.loginErrorMessage = "";
        self.otpFormSubmitted = true;
        this.isErrorToast = true;
        //re-init toast container
        self.hideOTPToast = true;
        $("#toast-container").css("pointer-events: auto;");
        self.cdRef.detectChanges();
        if (!self.otpValues[4] || (self.otpValues[4] && utils_services_1.UtilService.StringIsNullOrEmpty(self.otpValues[4]))) {
            self.showOtpError = true;
            self.cdRef.detectChanges();
        }
        else {
            self.showBusyLoader = true;
            self.requestOngoing = true;
            self.cdRef.detectChanges();
            var otp_1 = self.otpValues.join("");
            if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                var loadingModalInterval_2 = setInterval(function () {
                    self.requestOngoing = false;
                    self.showBusyLoader = false;
                    self.cdRef.detectChanges();
                    self.authenticateUserToken(otp_1);
                    _window().clearInterval(loadingModalInterval_2);
                }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
            }
            else {
                self.authenticateUserToken(otp_1);
            }
        }
    };
    LoginComponent.prototype._resendOTP = function () {
        var self = this;
        self.isErrorToast = true;
        self.toastMessage = self.loginErrorMessage = "";
        self.cdRef.detectChanges();
        // self.resendOTPSub = self.authService.resendLoginOTP(
        //     UtilService.trim(self.loginFormGroup.controls['userID'].value),
        //     UtilService.trim(self.loginFormGroup.controls['password'].value))
        //     .subscribe(response => {
        //        self.hideOTPToast = false;
        //        if (response.status_code == "000") {
        //            self.isErrorToast = false;
        //            self.loginErrorMessage = Appsettings.OTP_SENT_MESSAGE;
        //        }
        //        else {                 
        //            self.loginErrorMessage = response.short_status_description;      
        //        }
        //        self.cdRef.detectChanges(); 
        //        let toastInterval = setInterval(function () {
        //            $("#toast-container").fadeOut(Appsettings.TOAST_FADE_OUT_IN_MICROSECOND, function () {
        //                self.hideOTPToast = true;
        //                self.cdRef.detectChanges();
        //                $("#toast-container").removeAttr("style");
        //            });
        //            _window().clearInterval(toastInterval);
        //        }, Appsettings.TOAST_INTERVAL_IN_MICROSECOND);                 
        //    },
        //    (error: any) => { 
        //        self.loginErrorMessage = Appsettings.TECHNICAL_ERROR_MESSAGE;
        //        self.requestOngoing = false;
        //        self.showBusyLoader = false;
        //        self.cdRef.detectChanges();
        //        let toastInterval = setInterval(function () {
        //            $("#toast-container").fadeOut(Appsettings.TOAST_FADE_OUT_IN_MICROSECOND, function () {
        //                self.hideOTPToast = true;
        //                self.cdRef.detectChanges();
        //                $("#toast-container").removeAttr("style");
        //            });
        //            _window().clearInterval(toastInterval);
        //        }, Appsettings.TOAST_INTERVAL_IN_MICROSECOND);
        //     },
        //    () => {
        //        self.showBusyLoader = false;
        //        self.requestOngoing = false;
        //        self.cdRef.detectChanges();
        //    });
        if (this.initiateOTPSub) {
            this.initiateOTPSub.unsubscribe();
        }
        self.initiateOTPSub = self.authService.initiateOTP(self.user.UserId, self.user.CifID, appsettings_constant_1.Appsettings.OTP_REASON_CODE_LOGIN)
            .subscribe(function (initiateOtpResponse) {
            self.showBusyLoader = false;
            self.hideOTPToast = false;
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.isErrorToast = false;
                self.toastMessage = appsettings_constant_1.Appsettings.OTP_SENT_MESSAGE;
            }
            else {
                self.isErrorToast = true;
                self.toastMessage = initiateOtpResponse.ResponseFriendlyMessage;
            }
            self.InitToastr();
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
        //  (error: any) => {
        //      self.showLoginError = true;
        //      self.loginErrorMessage = Appsettings.TECHNICAL_ERROR_MESSAGE;
        //      self.requestOngoing = false;
        //      self.showBusyLoader = false;
        //      self.cdRef.detectChanges();
        //  },
        //  () => {
        //      self.showBusyLoader = false;
        //      self.requestOngoing = false;
        //      self.cdRef.detectChanges();
        //  });
    };
    LoginComponent.prototype.reSendOtp = function () {
        var self = this;
        //re-init toast container
        self.hideOTPToast = true;
        self.isErrorToast = true;
        self.showBusyLoader = true;
        self.requestOngoing = true;
        self.cdRef.detectChanges();
        // $("#toast-container").css("pointer-events: auto;"); 
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_3 = setInterval(function () {
                self.requestOngoing = false;
                self.showBusyLoader = false;
                self.cdRef.detectChanges();
                self._resendOTP();
                _window().clearInterval(loadingModalInterval_3);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._resendOTP();
        }
    };
    LoginComponent.prototype.ngOnDestroy = function () {
        if (this.loginSub) {
            this.loginSub.unsubscribe();
        }
        if (this.loginOTPSub) {
            this.loginOTPSub.unsubscribe();
        }
        if (this.initiateOTPSub) {
            this.initiateOTPSub.unsubscribe();
        }
        if (this.resendOTPSub) {
            this.resendOTPSub.unsubscribe();
        }
    };
    LoginComponent = __decorate([
        core_1.Component({
            templateUrl: "html/authentication/login.html"
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.ActivatedRoute, router_1.Router, (typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, core_1.ChangeDetectorRef])
    ], LoginComponent);
    return LoginComponent;
    var _a;
}(base_component_1.BaseComponent));
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map