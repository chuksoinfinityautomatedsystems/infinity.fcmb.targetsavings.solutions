"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var event_handlers_service_1 = require("../../commons/services/event_handlers.service");
var authentication_service_1 = require('../services/authentication.service');
var utils_services_1 = require("../../commons/services/utils.services");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var base_component_1 = require("../../commons/components/base.component");
var security_question_model_1 = require("../models/security-question.model");
// import { DropdownComponent } from "../../commons/components/dropdown.component";
function _window() {
    return window;
}
var LoginSecurityQuestionComponent = (function (_super) {
    __extends(LoginSecurityQuestionComponent, _super);
    function LoginSecurityQuestionComponent(formBuilder, activatedRoute, router, authService, cdRef) {
        _super.call(this, authService, router);
        this.formBuilder = formBuilder;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.authService = authService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK PLC";
        this.securityFormGroup = formBuilder.group({
            answer1: ['', forms_1.Validators.required],
            answer2: ['', forms_1.Validators.required],
            answer3: ['', forms_1.Validators.required]
        });
        this.hideOTPToast = true;
        this.isErrorToast = false;
        this.textInputActive = "";
        this.selectedSecurityQuestionsMap = {};
        this.enableQuestion2 = this.enableQuestion3 = this.cancelModalOpened = this.showSubmenuBullets = false;
    }
    LoginSecurityQuestionComponent.prototype.ngOnInit = function () {
        if (!authentication_service_1.AuthenticationService.authUserID || !authentication_service_1.AuthenticationService.fromPage) {
            this.router.navigate(['Login']);
        }
        if (authentication_service_1.AuthenticationService.fromPage != appsettings_constant_1.Appsettings.LOGIN_FROM_PAGE && authentication_service_1.AuthenticationService.fromPage != appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE) {
            this.router.navigate(['Login']);
        }
        if (authentication_service_1.AuthenticationService.fromPage == appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE) {
            this.showSubmenuBullets = true;
        }
        var self = this;
        self.showBusyLoader = true;
        self.getQuestionsSub = self.authService.getSecurityQuestions()
            .subscribe(function (response) {
            self.showBusyLoader = false;
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.securityQuestions = response.questionDetailList;
            }
            else {
                self.hideOTPToast = false;
                self.isErrorToast = true;
                self.toastMessage = response.ResponseFriendlyMessage;
                self.cdRef.detectChanges();
                self.InitToastr();
            }
        }, function (error) {
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
        this.selectedOptionSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            var dropdownIndex = object.index;
            var optionKey = object.optionKey;
            console.log("optionKey: " + optionKey);
            console.log("drop index: " + dropdownIndex);
            self.selectedSecurityQuestionsMap[dropdownIndex] = optionKey;
            // Enable Second or Third Questions if need be.
            if (!self.enableQuestion2 && dropdownIndex == 1 && self.securityFormGroup.controls["answer1"].value &&
                utils_services_1.UtilService.trim(self.securityFormGroup.controls["answer1"].value).length > 0) {
                self.enableQuestion2 = true;
            }
            else if (!self.enableQuestion3 && dropdownIndex == 2 && self.securityFormGroup.controls["answer2"].value &&
                utils_services_1.UtilService.trim(self.securityFormGroup.controls["answer2"].value).length > 0) {
                self.enableQuestion3 = true;
            }
        });
    };
    LoginSecurityQuestionComponent.prototype.ngOnDestroy = function () {
        if (this.selectedOptionSub) {
            this.selectedOptionSub.unsubscribe();
        }
        if (this.getQuestionsSub) {
            this.getQuestionsSub.unsubscribe();
        }
        if (this.saveQuestionsSub) {
            this.saveQuestionsSub.unsubscribe();
        }
    };
    LoginSecurityQuestionComponent.prototype.GotoChangePassword = function (event) {
        this.ReInitToast();
        this.cdRef.detectChanges();
        var self = this;
        if (!this.securityFormGroup.valid) {
            this.toastMessage = "Security questions and answers are required.";
            this.hideOTPToast = false;
            this.isErrorToast = true;
            this.InitToastr();
            this.cdRef.detectChanges();
        }
        else {
            var keyLength = utils_services_1.UtilService.ObjectKeysLength(this.selectedSecurityQuestionsMap);
            if (keyLength < 3) {
                this.hideOTPToast = false;
                this.isErrorToast = true;
                this.toastMessage = "Provide all the 3 unique Security questions";
                this.InitToastr();
                this.cdRef.detectChanges();
            }
            else if (utils_services_1.UtilService.trim(this.securityFormGroup.controls["answer1"].value).length < 1 ||
                utils_services_1.UtilService.trim(this.securityFormGroup.controls["answer2"].value).length < 1 ||
                utils_services_1.UtilService.trim(this.securityFormGroup.controls["answer3"].value).length < 1) {
                this.hideOTPToast = false;
                this.isErrorToast = true;
                this.toastMessage = "Provide answers to all the 3 unique Security questions";
                this.InitToastr();
                this.cdRef.detectChanges();
            }
            else {
                event.stopPropagation();
                var questionKeyArray_1 = [];
                questionKeyArray_1.push(this.selectedSecurityQuestionsMap[1]);
                questionKeyArray_1.push(this.selectedSecurityQuestionsMap[2]);
                questionKeyArray_1.push(this.selectedSecurityQuestionsMap[3]);
                questionKeyArray_1 = utils_services_1.UtilService.GetUniqueArray(questionKeyArray_1);
                if (questionKeyArray_1.length < 3) {
                    this.hideOTPToast = false;
                    this.isErrorToast = true;
                    this.toastMessage = "Please select unique security questions";
                    this.InitToastr();
                    this.cdRef.detectChanges();
                }
                else {
                    var userQuestions_1 = new security_question_model_1.UserSecurityQuestion();
                    userQuestions_1.userId = authentication_service_1.AuthenticationService.authUserID;
                    userQuestions_1.questions = this.securityQuestions.filter(function (x) { return x.questionId == questionKeyArray_1[0] ||
                        x.questionId == questionKeyArray_1[1] || x.questionId == questionKeyArray_1[2]; });
                    self.showBusyLoader = true;
                    if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                        var loadingModalInterval_1 = setInterval(function () {
                            self.showBusyLoader = false;
                            self.cdRef.detectChanges();
                            //  self._saveSecurityQuestions(userQuestions);
                            if (authentication_service_1.AuthenticationService.fromPage == appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE) {
                                self._saveSecurityQuestionsAndGenerateDefaultPasword(userQuestions_1);
                            }
                            else if (authentication_service_1.AuthenticationService.fromPage == appsettings_constant_1.Appsettings.LOGIN_FROM_PAGE) {
                                self._saveSecurityQuestions(userQuestions_1);
                            }
                            _window().clearInterval(loadingModalInterval_1);
                        }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
                    }
                    else {
                        self._saveSecurityQuestions(userQuestions_1);
                    }
                }
            }
        }
    };
    LoginSecurityQuestionComponent.prototype._saveSecurityQuestions = function (userQuestions) {
        var _this = this;
        var self = this;
        self.saveQuestionsSub = self.authService.saveSecurityQuestions(userQuestions)
            .subscribe(function (response) {
            self.showBusyLoader = false;
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                _this.selectedSecurityQuestionsMap = {};
                _this.ngOnDestroy();
                _this.router.navigate(['Login/CreateNewPassword']);
            }
            else {
                self.hideOTPToast = false;
                self.isErrorToast = true;
                self.toastMessage = response.ResponseFriendlyMessage;
                self.InitToastr();
                self.cdRef.detectChanges();
            }
        }, function (error) {
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    LoginSecurityQuestionComponent.prototype._saveSecurityQuestionsAndGenerateDefaultPasword = function (userQuestions) {
        var _this = this;
        var self = this;
        self.saveQuestionsSub = self.authService.saveSecurityQuestionsAndGenerateDefaultPassword(userQuestions)
            .subscribe(function (response) {
            self.showBusyLoader = false;
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                _this.selectedSecurityQuestionsMap = {};
                _this.ngOnDestroy();
                _this.router.navigate(['Registration/VerifyDefaultPassword']);
            }
            else {
                self.hideOTPToast = false;
                self.isErrorToast = true;
                self.toastMessage = response.ResponseFriendlyMessage;
                self.InitToastr();
                self.cdRef.detectChanges();
            }
        }, function (error) {
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    LoginSecurityQuestionComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.cdRef.detectChanges();
    };
    LoginSecurityQuestionComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.securityFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
        else {
            if (formControlName == "answer1" && this.selectedSecurityQuestionsMap[1] != undefined) {
                this.enableQuestion2 = true;
            }
            else if (formControlName == "answer2" && this.selectedSecurityQuestionsMap[2] != undefined) {
                this.enableQuestion3 = true;
            }
            this.cdRef.detectChanges();
        }
    };
    LoginSecurityQuestionComponent.prototype.cancelRegisterSecurityQuestion = function () {
        this.cancelModalOpened = true;
    };
    LoginSecurityQuestionComponent.prototype.hideCancelModal = function () {
        this.cancelModalOpened = false;
    };
    LoginSecurityQuestionComponent.prototype.backToLogin = function () {
        this.ngOnDestroy();
        if (authentication_service_1.AuthenticationService.fromPage == appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE) {
            this.router.navigate(['Registration/CreateUserId']);
        }
        else if (authentication_service_1.AuthenticationService.fromPage == appsettings_constant_1.Appsettings.LOGIN_FROM_PAGE) {
            this.router.navigate(['Login']);
        }
    };
    LoginSecurityQuestionComponent.prototype.backToLoginOnButtonClick = function (event) {
        event.stopPropagation();
        this.ngOnDestroy();
        if (authentication_service_1.AuthenticationService.fromPage == appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE) {
            this.router.navigate(['Registration/CreateUserId']);
        }
        else if (authentication_service_1.AuthenticationService.fromPage == appsettings_constant_1.Appsettings.LOGIN_FROM_PAGE) {
            this.router.navigate(['Login']);
        }
    };
    LoginSecurityQuestionComponent = __decorate([
        core_1.Component({
            templateUrl: "html/authentication/login-security-question.html"
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.ActivatedRoute, router_1.Router, (typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, core_1.ChangeDetectorRef])
    ], LoginSecurityQuestionComponent);
    return LoginSecurityQuestionComponent;
    var _a;
}(base_component_1.BaseComponent));
exports.LoginSecurityQuestionComponent = LoginSecurityQuestionComponent;
//# sourceMappingURL=login-security-question.component.js.map