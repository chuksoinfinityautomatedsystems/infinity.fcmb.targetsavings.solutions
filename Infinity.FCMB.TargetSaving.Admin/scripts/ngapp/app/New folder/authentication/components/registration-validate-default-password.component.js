"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
require("rxjs/Rx");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var authentication_service_1 = require('../services/authentication.service');
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var base_component_1 = require("../../commons/components/base.component");
var user_model_1 = require("../models/user.model");
function _window() {
    return window;
}
var ValidateDefaultPasswordComponent = (function (_super) {
    __extends(ValidateDefaultPasswordComponent, _super);
    function ValidateDefaultPasswordComponent(formBuilder, activatedRoute, router, authService, cdRef) {
        _super.call(this, authService, router);
        this.formBuilder = formBuilder;
        this.activatedRoute = activatedRoute;
        this.router = router;
        this.authService = authService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK PLC";
        this.OTPFormGroup = formBuilder.group({
            otp: ['', forms_1.Validators.required]
        });
        this.showExceptionPage = this.formSubmitted = false;
        this.hideOTPToast = true;
        this.isErrorToast = false;
    }
    ValidateDefaultPasswordComponent.prototype.ngOnInit = function () {
        if (!authentication_service_1.AuthenticationService.authUserID || !authentication_service_1.AuthenticationService.fromPage) {
            this.router.navigate(['Login']);
        }
        if (authentication_service_1.AuthenticationService.fromPage != appsettings_constant_1.Appsettings.LOGIN_FROM_PAGE && authentication_service_1.AuthenticationService.fromPage != appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE) {
            this.router.navigate(['Login']);
        }
        console.log("console.log(AuthenticationService.authUserID)222;   " + authentication_service_1.AuthenticationService.authUserID);
    };
    ValidateDefaultPasswordComponent.prototype.backHome = function () {
        this.router.navigate(['login']);
        return false;
    };
    ValidateDefaultPasswordComponent.prototype.closeModal = function () {
    };
    ValidateDefaultPasswordComponent.prototype.verifyDefaultPassword = function (event) {
        event.stopPropagation();
        var self = this;
        self.showBusyLoader = true;
        self.cdRef.detectChanges();
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                self.cdRef.detectChanges();
                self.authenticateUser();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self.authenticateUser();
        }
    };
    ValidateDefaultPasswordComponent.prototype.authenticateUser = function () {
        var _this = this;
        var self = this;
        var user = new user_model_1.User();
        user.UserId = authentication_service_1.AuthenticationService.authUserID;
        user.Password = self.OTPFormGroup.controls['otp'].value;
        self.loginSub = self.authService.authenticateUserIDWithoutLogout(user)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.PasswordChangeRequired || response.ResponseCode == response_codes_constant_1.ResponseCodes.QuestionAndPasswordRegistrationRequired) {
                authentication_service_1.AuthenticationService.authPass = user.Password;
                authentication_service_1.AuthenticationService.authOTP = user.Password;
                authentication_service_1.AuthenticationService.fromPage = appsettings_constant_1.Appsettings.REGISTER_USER_FROM_PAGE;
                _this.ngOnDestroy();
                _this.router.navigate(['Login/CreateNewPassword']);
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    ValidateDefaultPasswordComponent.prototype.ngOnDestroy = function () {
        if (this.loginSub) {
            this.loginSub.unsubscribe();
        }
    };
    ValidateDefaultPasswordComponent = __decorate([
        core_1.Component({
            templateUrl: "html/authentication/registration-default-password.html"
        }), 
        __metadata('design:paramtypes', [forms_1.FormBuilder, router_1.ActivatedRoute, router_1.Router, (typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, core_1.ChangeDetectorRef])
    ], ValidateDefaultPasswordComponent);
    return ValidateDefaultPasswordComponent;
    var _a;
}(base_component_1.BaseComponent));
exports.ValidateDefaultPasswordComponent = ValidateDefaultPasswordComponent;
//# sourceMappingURL=registration-validate-default-password.component.js.map