"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/Rx");
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/of');
var event_handlers_service_1 = require("../../commons/services/event_handlers.service");
var header_service_1 = require("../../commons/services/header.service");
var constants_services_1 = require("../../commons/services/constants.services");
var utils_services_1 = require("../../commons/services/utils.services");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var user_model_1 = require("../models/user.model");
function _window() {
    // return the global native browser window object
    return window;
}
var AuthenticationService = (function () {
    function AuthenticationService(_headerService, constantService, http) {
        this._headerService = _headerService;
        this.constantService = constantService;
        this.http = http;
        if (localStorage.getItem("X-InternetBankingLoggedInUserOauthToken")) {
            this.isLoggedIn = true;
        }
        else {
            this.isLoggedIn = false;
        }
        this.partiallyLoggedIn = false;
    }
    AuthenticationService.prototype.setPartialLoginToken = function () {
        //localStorage.setItem("X-InternetBankingPartialLoginSessionID", sessionId); 
        this.partiallyLoggedIn = true; // logged in without OTP validation
    };
    AuthenticationService.prototype.GetPartialLoginToken = function () {
        return this.partiallyLoggedIn;
    };
    AuthenticationService.prototype.setAccessToken = function (accessToken) {
        console.log("Access token: " + accessToken);
        localStorage.setItem("X-InternetBankingLoggedInUserOauthToken", accessToken);
        this.isLoggedIn = true;
    };
    AuthenticationService.prototype.GetAccessToken = function () {
        if (localStorage.getItem("X-InternetBankingLoggedInUserOauthToken")) {
            return JSON.parse(localStorage.getItem("X-InternetBankingLoggedInUserOauthToken"));
        }
        return "";
    };
    AuthenticationService.prototype.login = function (username, password) {
        var _this = this;
        this.logout();
        if (username.length < 1 || password.length < 1) {
            throw new Error("username or password empty.");
        }
        //Post to token endpoint to verify username and password and to get a auth token if credential is valid.
        return this.http.post(this.constantService.TokenUrl, "username=" + username + "&password=" + password + "&grant_type=password", this._headerService.getFormRequestJsonHeaders()).map(function (response) {
            var newUser = response.json();
            if (newUser && newUser.access_token) {
                localStorage.setItem("X-InternetBankingLoggedInUserOauthToken", JSON.stringify(newUser));
                _this.isLoggedIn = true;
            }
        });
    };
    AuthenticationService.prototype.logout = function () {
        this.isLoggedIn = this.partiallyLoggedIn = false;
        AuthenticationService.authUserID = AuthenticationService.authPass = AuthenticationService.fromPage = "";
        AuthenticationService.authUserObj = undefined;
        localStorage.removeItem("X-InternetBankingLoggedInUserOauthToken");
        //localStorage.removeItem("X-InternetBankingPartialLoginSessionID");
        return this.isLoggedIn;
    };
    //  return {
    //             operation_status: true,
    //             last_four_digit: "1234",
    //             session_id: "qweret6377==",
    //             is_locked: false,
    //             error_message: ""
    //         }
    AuthenticationService.prototype.authenticateUserID = function (user) {
        this.logout();
        if (utils_services_1.UtilService.StringIsNullOrEmpty(user.UserId) || utils_services_1.UtilService.StringIsNullOrEmpty(user.Password)) {
            throw new Error("UserId or password empty.");
        }
        var _user = new user_model_1.User();
        _user.UserId = user.UserId;
        _user.Password = btoa(user.Password);
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, _user, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                console.log("Authentication Login: " + _response);
                console.log("Authentication Login JSON: " + _response.json());
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                last_four_digit_of_customer_phone_no: "4567",
                lastLoginIn: "",
                SessionId: "87654321",
                CifId: "1234636",
                OTPResponseDescription: "trwy26171==",
                responseDescription: "",
                ResponseFriendlyMessage: ""
            });
        }
    };
    AuthenticationService.prototype.authenticateUserIDWithoutLogout = function (user) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(user.UserId) || utils_services_1.UtilService.StringIsNullOrEmpty(user.Password)) {
            throw new Error("UserId or password empty.");
        }
        var _user = new user_model_1.User();
        _user.UserId = user.UserId;
        _user.Password = btoa(user.Password);
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.AUTHENTICATE_USER_ID_URL, _user, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                console.log("Authentication Login: " + _response);
                console.log("Authentication Login JSON: " + _response.json());
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.QuestionAndPasswordRegistrationRequired,
                last_four_digit_of_customer_phone_no: "4567",
                lastLoginIn: "",
                SessionId: "87654321",
                CifId: "1213232",
                OTPResponseDescription: "312yt36ui39",
                responseDescription: "",
                ResponseFriendlyMessage: ""
            });
        }
    };
    AuthenticationService.prototype.saveSecurityQuestions = function (userQuestions) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userQuestions.userId) || !userQuestions.questions || (userQuestions.questions && userQuestions.questions.length < 1)) {
            throw new Error("Invalid parameters");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.SAVE_SECURITY_QUESTIONS_URL, userQuestions, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                SessionId: "87654321",
                ResponseFriendlyMessage: "Invalid Save Security Request."
            });
        }
    };
    AuthenticationService.prototype.saveSecurityQuestionsAndGenerateDefaultPassword = function (userQuestions) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userQuestions.userId) || !userQuestions.questions || (userQuestions.questions && userQuestions.questions.length < 1)) {
            throw new Error("Invalid parameters");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.SAVE_SECURITY_QUESTIONS_AND_GENERATE_DEFAULT_PASSWORD_URL, userQuestions, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                SessionId: "87654321",
                ResponseFriendlyMessage: "Invalid Save Security Request."
            });
        }
    };
    AuthenticationService.prototype.initiateOTP = function (userId, cifId, reasonCode) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId) || utils_services_1.UtilService.StringIsNullOrEmpty(cifId) || utils_services_1.UtilService.StringIsNullOrEmpty(reasonCode)) {
            throw new Error("Invalid parameters.");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.INITIATE_OTP_URL, { UserId: userId, CifId: cifId, ReasonCode: reasonCode }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                ResponseFriendlyMessage: "Error sending OTP.",
                ResponseDescription: ""
            });
        }
    };
    AuthenticationService.prototype.confirmIfUserHasIBProfile = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters: UserId");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.CONFIRM_USER_EXIST_URL, {
                UserId: userId,
                AccountNo: userId
            }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                ResponseFriendlyMessage: "UserId does not exist"
            });
        }
    };
    AuthenticationService.prototype.getSecurityQuestions = function () {
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_SECURITY_QUESTIONS_URL, null, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                SessionId: "",
                ResponseFriendlyMessage: "",
                questionDetailList: [
                    {
                        "questionId": "1",
                        "questionDescription": "What's your dream job.",
                        "answer": ""
                    },
                    {
                        "questionId": "2",
                        "questionDescription": "What's your spouse name",
                        "answer": ""
                    },
                    {
                        "questionId": "3",
                        "questionDescription": "What's your first boss name.",
                        "answer": ""
                    },
                    {
                        "questionId": "4",
                        "questionDescription": "What your favorite soccer club.",
                        "answer": ""
                    }
                ]
            });
        }
    };
    AuthenticationService.prototype.validateLoginOTP = function (user) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(user.UserId) || utils_services_1.UtilService.StringIsNullOrEmpty(user.Password) ||
            utils_services_1.UtilService.StringIsNullOrEmpty(user.Otp) || utils_services_1.UtilService.StringIsNullOrEmpty(user.Reference)) {
            throw new Error("UserId, password, otpRefernce, or otp empty.");
        }
        return Observable_1.Observable.of({
            ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
            SessionId: "",
            responseDescription: "",
            ResponseFriendlyMessage: "OTP has been locked out. Please contact the Customer Contact Centre."
        });
        // if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
        //     user.ReasonCode = "02";            
        //     return this.http.post(Appsettings.VALIDATE_OTP_URL, user,
        //         this._headerService.getRequestJsonHeaders()
        //     ).map((_response: Response) => {
        //             return _response.json();
        //     });
        // }
        // else {
        //      return Observable.of({
        //         ResponseCode: ResponseCodes.SUCCESS,
        //         SessionId: "",
        //         responseDescription: "",
        //         ResponseFriendlyMessage: "OTP has been locked out. Please contact the Customer Contact Centre."
        //     }); 
        // }
    };
    AuthenticationService.prototype.validateAccountNo = function (userAccountNo) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userAccountNo)) {
            throw new Error("UserId, password or otp empty.");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.CONFIRM_USER_EXIST_URL, {
                "UserId": userAccountNo
            }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                SessionId: "",
                responseDescription: "",
                ResponseFriendlyMessage: ""
            });
        }
    };
    AuthenticationService.prototype.resendLoginOTP = function (userId, password) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId) || utils_services_1.UtilService.StringIsNullOrEmpty(password)) {
            throw new Error("UserId, password empty.");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.INITIATE_OTP_URL, "user_id=" + userId + "&password=" + password, this._headerService.getFormRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                status_code: "000",
                short_status_description: ""
            });
        }
    };
    AuthenticationService.prototype.changePassword = function (userPasswordInformation) {
        console.log(userPasswordInformation);
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userPasswordInformation.UserId) ||
            utils_services_1.UtilService.StringIsNullOrEmpty(userPasswordInformation.OldPassword) ||
            utils_services_1.UtilService.StringIsNullOrEmpty(userPasswordInformation.NewPassword) ||
            utils_services_1.UtilService.StringIsNullOrEmpty(userPasswordInformation.OTP)) {
            throw new Error("Invalid parameters");
        }
        var _userPasswordInformation = utils_services_1.UtilService.clone(userPasswordInformation);
        _userPasswordInformation.OldPassword = btoa(_userPasswordInformation.OldPassword);
        _userPasswordInformation.NewPassword = btoa(_userPasswordInformation.NewPassword);
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.CHANGE_PASSWORD_URL, _userPasswordInformation, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                SessionId: "87654321",
                ResponseFriendlyMessage: "Invalid Change Password Request."
            });
        }
    };
    AuthenticationService.prototype.stopAutoLogout = function () {
        if ($('body').off) {
            $('body').off("mousemove");
            $('body').off("mousedown");
            $('body').off("keypress");
            $('body').off("DOMMouseScroll");
            $('body').off("mousewheel");
            $('body').off("touchmove");
            $('body').off("MSPointerMove");
            $('body').off("pointermove");
        }
        else if ($('body').unbind) {
            $('body').unbind("mousemove");
            $('body').unbind("mousedown");
            $('body').unbind("keypress");
            $('body').unbind("DOMMouseScroll");
            $('body').unbind("mousewheel");
            $('body').unbind("touchmove");
            $('body').unbind("MSPointerMove");
            $('body').unbind("pointermove");
        }
        if (this.timeoutID) {
            _window().clearTimeout(this.timeoutID);
        }
    };
    AuthenticationService.prototype.setupAutoLogout = function () {
        var self = this;
        if ($('body').off) {
            $('body').off("mousemove");
            $('body').off("mousedown");
            $('body').off("keypress");
            $('body').off("DOMMouseScroll");
            $('body').off("mousewheel");
            $('body').off("touchmove");
            $('body').off("MSPointerMove");
            $('body').off("pointermove");
            $('body').off("keyup");
            $('body').off("keydown");
        }
        else if ($('body').unbind) {
            $('body').unbind("mousemove");
            $('body').unbind("mousedown");
            $('body').unbind("keypress");
            $('body').unbind("DOMMouseScroll");
            $('body').unbind("mousewheel");
            $('body').unbind("touchmove");
            $('body').unbind("MSPointerMove");
            $('body').unbind("pointermove");
            $('body').unbind("keyup");
            $('body').unbind("keydown");
        }
        if (self.timeoutID) {
            _window().clearTimeout(self.timeoutID);
        }
        if ($.on) {
            $('body').on("mousemove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("mousedown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("keypress", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("keydown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("keyup", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("DOMMouseScroll", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("mousewheel", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("touchmove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("MSPointerMove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("pointermove", function () {
                self.resetAutoLogoutTimer(self);
            });
        }
        else if ($.bind) {
            $('body').bind("mousemove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("mousedown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("keypress", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("DOMMouseScroll", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("mousewheel", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("touchmove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("MSPointerMove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("pointermove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("keydown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("keyup", function () {
                self.resetAutoLogoutTimer(self);
            });
        }
        self.startAutoLogoutTimer();
    };
    AuthenticationService.prototype.startAutoLogoutTimer = function () {
        var self = this;
        self.timeoutID = _window().setTimeout(function () {
            event_handlers_service_1.EventHandlerService.emitSessionTimeoutEvent();
            //self.logout();
            self.stopAutoLogout();
        }, appsettings_constant_1.Appsettings.autoLogoutTimeInMicrosec);
    };
    AuthenticationService.prototype.resetAutoLogoutTimer = function (self) {
        _window().clearTimeout(self.timeoutID);
        self.startAutoLogoutTimer();
    };
    AuthenticationService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof header_service_1.HeaderService !== 'undefined' && header_service_1.HeaderService) === 'function' && _a) || Object, (typeof (_b = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _b) || Object, http_1.Http])
    ], AuthenticationService);
    return AuthenticationService;
    var _a, _b;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map