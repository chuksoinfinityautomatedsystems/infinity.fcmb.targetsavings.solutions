"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var router_1 = require('@angular/router');
var common_module_1 = require('../commons/common.module');
var login_component_1 = require('./components/login.component');
var forgot_password_component_1 = require('./components/forgot-password.component');
var registration_component_1 = require('./components/registration.component');
var new_account_component_1 = require('./components/new-account.component');
var login_security_question_component_1 = require('./components/login-security-question.component');
var change_password_component_1 = require('./components/change-password.component');
var registration_validate_default_password_component_1 = require('./components/registration-validate-default-password.component');
// import {DropdownComponent} from '../commons/components/dropdown.component'; 
//import {LoginSecurityQuestionComponent} from './components/login-security-question.component'; 
var header_service_1 = require("../commons/services/header.service");
var authentication_service_1 = require('./services/authentication.service');
var authorization_services_1 = require('./services/authorization.services');
//import { LoadingModalComponent2 } from "../commons/components/loading-dialog.component";  
var AuthenticationPagesModule = (function () {
    function AuthenticationPagesModule() {
    }
    AuthenticationPagesModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule, common_module_1.SharedModule],
            declarations: [login_component_1.LoginComponent, forgot_password_component_1.ResetPasswordComponent, registration_component_1.RegisterComponent, new_account_component_1.NewAccountComponent,
                login_security_question_component_1.LoginSecurityQuestionComponent, change_password_component_1.ChangePasswordComponent, registration_validate_default_password_component_1.ValidateDefaultPasswordComponent],
            providers: [header_service_1.HeaderService, authentication_service_1.AuthenticationService, authorization_services_1.AuthorizeService]
        }), 
        __metadata('design:paramtypes', [])
    ], AuthenticationPagesModule);
    return AuthenticationPagesModule;
}());
exports.AuthenticationPagesModule = AuthenticationPagesModule;
//# sourceMappingURL=authentication.module.js.map