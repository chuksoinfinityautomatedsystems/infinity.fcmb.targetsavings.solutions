"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var forms_1 = require('@angular/forms');
require("rxjs/Rx");
var Observable_1 = require('rxjs/Observable');
require('rxjs/add/observable/of');
var header_service_1 = require("../../commons/services/header.service");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var authentication_service_1 = require("../../authentication/services/authentication.service");
var utils_services_1 = require("../../commons/services/utils.services");
var beneficiaries_model_1 = require("../models/beneficiaries.model");
var countryPhoneCode_model_1 = require("../models/countryPhoneCode.model");
var bank_model_1 = require("../models/bank.model");
var account_model_1 = require("../models/account.model");
function _window() {
    // return the global native browser window object
    return window;
}
var DashboardService = (function () {
    function DashboardService(authService, _headerService, http) {
        this.authService = authService;
        this._headerService = _headerService;
        this.http = http;
    }
    DashboardService.prototype.getCustomerAccountList = function (cifId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(cifId)) {
            throw new Error("Invalid parameters: cifId");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_ACCOUNT_LIST, { CifId: cifId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Accounts: [
                    {
                        accountNumber: "0021236296",
                        accountName: "Abraham Dyke",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Current account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 450000,
                        availableBalance: 450000,
                        currency: "NGN"
                    },
                    {
                        accountNumber: "0021236297",
                        accountName: "Elon Musk",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Current account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 780000,
                        availableBalance: 780000,
                        currency: "NGN"
                    },
                    {
                        accountNumber: "0021236298",
                        accountName: "Andy Groove",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Current account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 1500000,
                        availableBalance: 1500000,
                        currency: "NGN"
                    },
                    {
                        accountNumber: "0021236212",
                        accountName: "Andy Groove Euro",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Domiciliary account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 12000,
                        availableBalance: 12000,
                        currency: "Euro"
                    },
                    {
                        accountNumber: "002123345672",
                        accountName: "Andy Groove Euro2",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Domiciliary account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 145000,
                        availableBalance: 145000,
                        currency: "Euro"
                    },
                    {
                        accountNumber: "00212331111",
                        accountName: "Andy Groove Euro3",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Domiciliary account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 121000,
                        availableBalance: 1215000,
                        currency: "Euro"
                    },
                    {
                        accountNumber: "0045236092",
                        accountName: "Andy Groove USD",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Domiciliary account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 30000,
                        availableBalance: 30000,
                        currency: "USD"
                    },
                    {
                        accountNumber: "004523101010",
                        accountName: "Andy Groove USD2",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Domiciliary account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 29000,
                        availableBalance: 29000,
                        currency: "USD"
                    },
                    {
                        accountNumber: "004523202020",
                        accountName: "Andy Groove USD3",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Domiciliary account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 48000,
                        availableBalance: 48000,
                        currency: "USD"
                    },
                    {
                        accountNumber: "0045232762",
                        accountName: "Andy Groove GBP",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Domiciliary account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 70000,
                        availableBalance: 70000,
                        currency: "GBP"
                    },
                    {
                        accountNumber: "004523585858",
                        accountName: "Andy Groove GBP2",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Domiciliary account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 278000,
                        availableBalance: 278000,
                        currency: "GBP"
                    },
                    {
                        accountNumber: "00452320909093",
                        accountName: "Andy Groove GBP3",
                        debitAllowed: true,
                        creditAllowed: true,
                        accountType: "Domiciliary account",
                        accountCategory: "sample string 6",
                        effectiveBalance: 74700,
                        availableBalance: 74700,
                        currency: "GBP"
                    }
                ]
            });
        }
    };
    DashboardService.prototype.getCustomerTransactionDetails = function (accountNo, transactionCount) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(accountNo)) {
            throw new Error("Invalid parameters: accountNo");
        }
        if (transactionCount < 1) {
            throw new Error("Invalid parameters: transactionCount");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_TRANSACTION_DETAILS, { AccountNumber: accountNo, TransactionCount: transactionCount, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Transactions: [
                    {
                        date: "2018-02-14T12:14:30.6534304+01:00",
                        description: "sample string 1",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "C"
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "sample string 2",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-04-14T12:14:30.6534304+01:00",
                        description: "sample string 3",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-05-14T12:14:30.6534304+01:00",
                        description: "sample string 4",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "C"
                    }, {
                        date: "2018-02-14T12:14:30.6534304+01:00",
                        description: "sample string 3",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "D"
                    },
                ]
            });
        }
    };
    DashboardService.prototype.getCustomerDatedTransaction = function (accountNo, startDate, endDate) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(accountNo) || utils_services_1.UtilService.StringIsNullOrEmpty(startDate) || utils_services_1.UtilService.StringIsNullOrEmpty(endDate)) {
            throw new Error("Invalid parameters.");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_TRANSACTION_DETAILS, { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Transactions: [
                    {
                        date: "2018-03-12T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 1967882.49,
                        balance: 529191.00,
                        transactionType: "C"
                    },
                    {
                        date: "2018-03-12T12:14:30.6534304+01:00",
                        description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
                        amount: 18000000000000000,
                        balance: 2497073.49,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-11T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 647383929.0,
                        balance: 6738892.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-11T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 1039499223.0,
                        balance: 6342552939.0,
                        transactionType: "C"
                    }, {
                        date: "2018-03-10T12:14:30.6534304+01:00",
                        description: "FASTPAY_CHARGES",
                        amount: 4488438848.0,
                        balance: 2738897332.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-10T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 7827781392.0,
                        balance: 4892023773.0,
                        transactionType: "C"
                    },
                    {
                        date: "2018-03-12T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 35788582.21,
                        balance: 3674833.49,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-09T12:14:30.6534304+01:00",
                        description: "FASTPAY_CHARGES",
                        amount: 123748839.0,
                        balance: 9048377282.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-08T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 1112737733.0,
                        balance: 490483822.0,
                        transactionType: "C"
                    }, {
                        date: "2018-03-08T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 384938823.0,
                        balance: 400483724.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-07T12:14:30.6534304+01:00",
                        description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
                        amount: 56373883.0,
                        balance: 4773883.30,
                        transactionType: "C"
                    }, {
                        date: "2018-03-07T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 312494993.0,
                        balance: 444004982.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-06T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 321849493.0,
                        balance: 430488833.0,
                        transactionType: "C"
                    },
                    {
                        date: "2018-03-08T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 56737883.0,
                        balance: 400483724.0,
                        transactionType: "C"
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 11903002.67,
                        balance: 4242233.0,
                        transactionType: "C"
                    },
                    {
                        date: "2018-02-14T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 33434534.0,
                        balance: 42254543.0,
                        transactionType: "C"
                    }, {
                        date: "2018-01-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 342335233.0,
                        balance: 4353243233.0,
                        transactionType: "C"
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
                        amount: 89383892.0,
                        balance: 9594003.98,
                        transactionType: "C"
                    }, {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 24643233.0,
                        balance: 4242233.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-05-14T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 33445432.0,
                        balance: 343543533.0,
                        transactionType: "C"
                    }, {
                        date: "2018-01-14T12:14:30.6534304+01:00",
                        description: "FASTPAY_CHARGES",
                        amount: 333234.0,
                        balance: 445343223.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 32334234.40,
                        balance: 563533224.04,
                        transactionType: "C"
                    },
                ]
            });
        }
    };
    DashboardService.prototype.getBeneficiaryTransferReceipt = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters.");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_TRANSACTION_DETAILS, { userId: userId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Transactions: [
                    {
                        date: "2018-03-12T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 1967882.49,
                        balance: 529191.00,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-12T12:14:30.6534304+01:00",
                        description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
                        amount: 18000000000,
                        balance: 2497073.49,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-11T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 647383929.0,
                        balance: 6738892.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-11T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 1039499223.0,
                        balance: 6342552939.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267418",
                        isSuccessful: true
                    }, {
                        date: "2018-03-10T12:14:30.6534304+01:00",
                        description: "FASTPAY_CHARGES",
                        amount: 4488438848.0,
                        balance: 2738897332.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267193",
                        isSuccessful: false
                    },
                    {
                        date: "2018-03-10T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 7827781392.0,
                        balance: 4892023773.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-12T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 35788582.21,
                        balance: 3674833.49,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-09T12:14:30.6534304+01:00",
                        description: "FASTPAY_CHARGES",
                        amount: 123748839.0,
                        balance: 9048377282.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-08T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 1112737733.0,
                        balance: 490483822.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: false
                    }, {
                        date: "2018-03-08T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 384938823.0,
                        balance: 400483724.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-07T12:14:30.6534304+01:00",
                        description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
                        amount: 56373883.0,
                        balance: 4773883.30,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    }, {
                        date: "2018-03-07T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 312494993.0,
                        balance: 444004982.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267412",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-06T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 321849493.0,
                        balance: 430488833.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: false
                    },
                    {
                        date: "2018-03-08T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 56737883.0,
                        balance: 400483724.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 11903002.67,
                        balance: 4242233.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-02-14T12:14:30.6534304+01:00",
                        description: "Tud Nuru Zinc",
                        amount: 33434534.0,
                        balance: 42254543.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    }, {
                        date: "2018-01-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 342335233.0,
                        balance: 4353243233.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
                        amount: 89383892.0,
                        balance: 9594003.98,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267444",
                        isSuccessful: true
                    }, {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 24643233.0,
                        balance: 4242233.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-05-14T12:14:30.6534304+01:00",
                        description: "BO KNIGHT METAL MANUFACTURING CO LTD",
                        amount: 33445432.0,
                        balance: 343543533.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    }, {
                        date: "2018-01-14T12:14:30.6534304+01:00",
                        description: "FASTPAY_CHARGES",
                        amount: 333234.0,
                        balance: 445343223.0,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                    {
                        date: "2018-03-14T12:14:30.6534304+01:00",
                        description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
                        amount: 32334234.40,
                        balance: 563533224.04,
                        transactionType: "Beneficiary payment",
                        fromAccount: "001267483",
                        isSuccessful: true
                    },
                ]
            });
        }
    };
    DashboardService.prototype.getCustomerDatedTransaction2 = function (accountNo, startDate, endDate) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(accountNo) || utils_services_1.UtilService.StringIsNullOrEmpty(startDate) || utils_services_1.UtilService.StringIsNullOrEmpty(endDate)) {
            throw new Error("Invalid parameters.");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_TRANSACTION_DETAILS, { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Transactions: [
                    {
                        date: "2018-03-04T12:14:30.6534304+01:00",
                        description: "sample string 1",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "C"
                    },
                    {
                        date: "2018-03-04T12:14:30.6534304+01:00",
                        description: "sample string 2",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-04T12:14:30.6534304+01:00",
                        description: "sample string 3",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-03T12:14:30.6534304+01:00",
                        description: "sample string 4",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "C"
                    }, {
                        date: "2018-03-03T12:14:30.6534304+01:00",
                        description: "sample string 3",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-02T12:14:30.6534304+01:00",
                        description: "sample string 4",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "C"
                    }, {
                        date: "2018-03-02T12:14:30.6534304+01:00",
                        description: "sample string 3",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-02T12:14:30.6534304+01:00",
                        description: "sample string 4",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "C"
                    }, {
                        date: "2018-03-02T12:14:30.6534304+01:00",
                        description: "sample string 3",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-02T12:14:30.6534304+01:00",
                        description: "sample string 4",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "C"
                    }, {
                        date: "2018-03-02T12:14:30.6534304+01:00",
                        description: "sample string 3",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "D"
                    },
                    {
                        date: "2018-03-01T12:14:30.6534304+01:00",
                        description: "sample string 4",
                        amount: 3.0,
                        balance: 4.0,
                        transactionType: "C"
                    },
                ]
            });
        }
    };
    // getBillers(): Array<Biller> {
    //     let billers = new Array<Biller>();
    //     let _biller = new Biller();
    //     _biller.billerName = "9mobile Data Bundles";
    //     _biller.billerNickname = '9Mobile DB';
    //     _biller.myReference = '9Mobile Reference';
    //     _biller.billerCollectionValue = '09085799082'; 
    //     billers.push(_biller);
    //     _biller = new Biller();
    //     _biller.billerName = "Above Only MFB";
    //     _biller.billerNickname = 'AboveMFB';
    //     _biller.myReference = 'AMFB Reference';
    //     _biller.billerCollectionValue = '032920022';
    //     billers.push(_biller);         
    //     return billers;
    // }
    DashboardService.prototype.getBeneficiaryList = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters: userId");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_BENEFICIARIES, { UserId: userId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Beneficiaries: [
                    {
                        beneficiaryAlias: "Khadijat Jubreel Olabode",
                        beneficiaryName: "Khadijat Jubreel",
                        beneficiaryAccountNumber: "0011289291",
                        beneficiaryAccountName: "Khadi Jay Current",
                        beneficiaryBank: "Stanbic IBTC BANK",
                        beneficiaryBankCode: "221",
                        beneficiaryEmailAddress: "khadij@stanbicibtc.com",
                        beneficiaryReference: "Khady Jay",
                        customerReference: "SEUN POPOOLA.",
                        beneficiaryId: 10,
                        beneficiaryMobileNo: '07035799078'
                    },
                    {
                        beneficiaryAlias: "KAYODE CURRENT",
                        beneficiaryName: "KAYODE POPOOLA",
                        beneficiaryAccountNumber: "0011289296",
                        beneficiaryAccountName: "Khayode Burgundy",
                        beneficiaryBank: "Diamond BANK",
                        beneficiaryBankCode: "061",
                        beneficiaryEmailAddress: "kaybrian@gmail.com",
                        beneficiaryReference: "Kay Pumping",
                        customerReference: "SEUN POPOOLA.",
                        beneficiaryId: 11,
                        beneficiaryMobileNo: '07035799083'
                    },
                    {
                        beneficiaryAlias: "GABBY",
                        beneficiaryName: "GABRIEL OLUBOLA",
                        beneficiaryAccountNumber: "0011289297",
                        beneficiaryAccountName: "Gabby Cisco",
                        beneficiaryBank: "GUARANTY TRUST BANK",
                        beneficiaryBankCode: "058",
                        beneficiaryEmailAddress: "gabby@cr7.com",
                        beneficiaryReference: "GABBY",
                        customerReference: "SEUN POPOOLA.",
                        beneficiaryId: 12
                    },
                    {
                        beneficiaryAlias: "SEUN NELSON",
                        beneficiaryName: "OLUWASEUN NELSON IGE",
                        beneficiaryAccountNumber: "0045289222",
                        beneficiaryAccountName: "Gabby Cisco",
                        beneficiaryBank: "GUARANTY TRUST BANK",
                        beneficiaryBankCode: "058",
                        beneficiaryEmailAddress: "gabby@cr7.com",
                        beneficiaryReference: "SEUN NELSON REF",
                        customerReference: "SEUN POPOOLA.",
                        beneficiaryId: 13
                    }
                ]
            });
        }
    };
    DashboardService.prototype.getBillerBeneficiaryList = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters: userId");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_CUSTOMER_BILLER_BENEFICIARIES, { UserId: userId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Billers: [
                    {
                        billerId: 12,
                        billerName: "DSTV",
                        billerType: "SN",
                        payeeFormat: "DSTV Smart Card Number",
                        billerCurrency: "NGN",
                        categoryId: "2",
                        categoryName: "Cable TV Bills",
                        subscriptionIds: "2511336",
                        billerNickName: "DSTV-MUM",
                        myReference: "MY DSTV Ref",
                        consumerCode: '00110022'
                    },
                    {
                        billerId: 13,
                        billerName: "MTN TOPUP",
                        billerType: "SN",
                        payeeFormat: "Phone Number",
                        billerCurrency: "NGN",
                        categoryId: "1",
                        categoryName: "Airtime Bills",
                        subscriptionIds: "2511337",
                        billerNickName: "MTN airtime",
                        myReference: "MY MTN Topup REF",
                        consumerCode: '00110023'
                    },
                    {
                        billerId: 14,
                        billerName: "Above MFB",
                        billerType: "SN",
                        payeeFormat: "Account Number",
                        billerCurrency: "NGN",
                        categoryId: "3",
                        categoryName: "MFB Bills",
                        subscriptionIds: "2511336",
                        billerNickName: "MY Above MFM Savings",
                        myReference: "MY Above MFB Ref",
                        consumerCode: '00110024'
                    },
                    {
                        billerId: 15,
                        billerName: "OAU",
                        billerType: "SN",
                        payeeFormat: "Matric Number",
                        billerCurrency: "NGN",
                        categoryId: "4",
                        categoryName: "School Bills",
                        subscriptionIds: "3511310",
                        billerNickName: "OAU Levy Fee",
                        myReference: "MY OAU Ref",
                        consumerCode: '00110025'
                    },
                    {
                        billerId: 16,
                        billerName: "9Mobile VTU",
                        billerType: "SN",
                        payeeFormat: "Phone Number",
                        billerCurrency: "NGN",
                        categoryId: "1",
                        categoryName: "Airtime Bills",
                        subscriptionIds: "65738922",
                        billerNickName: "Biweekly Topup",
                        myReference: "MY 9Mobile Ref",
                        consumerCode: '903983727'
                    },
                ]
            });
        }
    };
    DashboardService.prototype.getBillers = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters: userId");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_BILLERS, { UserId: userId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Billers: [
                    {
                        billerId: 12,
                        billerName: "9mobile Data Bundles",
                        billerType: "SN",
                        payeeFormat: "Mobile Number",
                        billerCurrency: "NGN",
                        categoryId: "1",
                        categoryName: "Internet Services"
                    },
                    {
                        billerId: 13,
                        billerName: "9mobile Dealer Payments",
                        billerType: "SN",
                        payeeFormat: "Account Number",
                        billerCurrency: "NGN",
                        categoryId: "2",
                        categoryName: "Dealer Payments"
                    },
                    {
                        billerId: 14,
                        billerName: "9mobile Postpaid Payments",
                        billerType: "SN",
                        payeeFormat: "Payment Number",
                        billerCurrency: "NGN",
                        categoryId: "3",
                        categoryName: "Phone Bills"
                    },
                    {
                        billerId: 15,
                        billerName: "Aero Mobile Book-On-Hold",
                        billerType: "SN",
                        payeeFormat: "Ticket Number",
                        billerCurrency: "NGN",
                        categoryId: "4",
                        categoryName: "Airlines And Hotels Payments"
                    },
                    {
                        billerId: 16,
                        billerName: "Airtime Non Mobile Services",
                        billerType: "SN",
                        payeeFormat: "Mobile Number",
                        billerCurrency: "NGN",
                        categoryId: "2",
                        categoryName: "Dealer Payments"
                    },
                    {
                        billerId: 17,
                        billerName: "Mobitel Payments",
                        billerType: "SN",
                        payeeFormat: "Device No.",
                        billerCurrency: "NGN",
                        categoryId: "1",
                        categoryName: "Internet Services"
                    },
                    {
                        billerId: 18,
                        billerName: "Wakanow Mobile",
                        billerType: "SN",
                        payeeFormat: "Mobile Number",
                        billerCurrency: "NGN",
                        categoryId: "5",
                        categoryName: "International Airtime"
                    }
                ]
            });
        }
    };
    DashboardService.prototype.getServiceProviders = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters: userId");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_BILLERS, { UserId: userId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Operators: [
                    {
                        operatorId: 12,
                        operatorName: "9mobile Recharge (E-Top Up)"
                    },
                    {
                        operatorId: 13,
                        operatorName: "Airtel Mobile Top-up (Prepaid)"
                    },
                    {
                        operatorId: 14,
                        operatorName: "Airtel Top-up (Postpaid)"
                    },
                    {
                        operatorId: 15,
                        operatorName: "GLO QuickCharge (Top-up)"
                    },
                    {
                        operatorId: 16,
                        operatorName: "MTN Direct Top-up(Prepaid)"
                    },
                    {
                        operatorId: 17,
                        operatorName: "Smile Bundle"
                    },
                    {
                        operatorId: 18,
                        operatorName: "Topup (prepaid)"
                    }
                ]
            });
        }
    };
    DashboardService.prototype.getOperatorProductList = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters: userId");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_BILLER_PRODUCTS, { UserId: userId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Products: [
                    {
                        operatorId: 12,
                        productId: 17,
                        productCode: "51617",
                        productName: "MTN VTU 50",
                        isAmountFixed: 'Y',
                        amount: 450001
                    },
                    {
                        operatorId: 12,
                        productId: 18,
                        productCode: "51617",
                        productName: "MTN VTU 100",
                        paymentCurrency: "NGN",
                        isAmountFixed: 'Y',
                        amount: 5000
                    },
                    {
                        operatorId: 12,
                        productId: 19,
                        productCode: "51617",
                        productName: "MTN VTU 500",
                        paymentCurrency: "NGN",
                        isAmountFixed: 'N',
                        amount: 0
                    },
                    {
                        operatorId: 12,
                        productId: 20,
                        productCode: "51617",
                        productName: "1GB Data (Smile Lite <2Mbps)",
                        paymentCurrency: "NGN",
                        isAmountFixed: 'Y',
                        amount: 6000
                    },
                    {
                        operatorId: 12,
                        productId: 21,
                        productCode: "51617",
                        productName: "3GB Data Bundle",
                        paymentCurrency: "NGN",
                        isAmountFixed: 'N',
                        amount: 0
                    }
                ]
            });
        }
    };
    DashboardService.prototype.getBillerCategories = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters: userId");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_BILLER_CATEGORIES, { UserId: userId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                BillerCategories: [
                    {
                        categoryId: 1,
                        categoryName: "Internet Services"
                    },
                    {
                        categoryId: 2,
                        categoryName: "Dealer Payments"
                    },
                    {
                        categoryId: 3,
                        categoryName: "Phone Bills"
                    },
                    {
                        categoryId: 4,
                        categoryName: "Airlines And Hotels Payments"
                    },
                    {
                        categoryId: 5,
                        categoryName: "International Airtime"
                    }
                ]
            });
        }
    };
    DashboardService.prototype.getBillerProductList = function (userId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId)) {
            throw new Error("Invalid parameters: userId");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_BILLER_PRODUCTS, { UserId: userId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Products: [
                    {
                        billerId: 12,
                        productId: 17,
                        productCode: "51617",
                        productName: "Insurance 2011",
                        paymentCurrency: "NGN",
                        isAmountFixed: 'Y',
                        amount: 450001
                    },
                    {
                        billerId: 12,
                        productId: 18,
                        productCode: "51617",
                        productName: "Insurance 2012",
                        paymentCurrency: "NGN",
                        isAmountFixed: 'Y',
                        amount: 5000
                    },
                    {
                        billerId: 12,
                        productId: 19,
                        productCode: "51617",
                        productName: "Insurance 2013",
                        paymentCurrency: "NGN",
                        isAmountFixed: 'N',
                        amount: 0
                    },
                    {
                        billerId: 12,
                        productId: 20,
                        productCode: "51617",
                        productName: "Insurance 2014",
                        paymentCurrency: "NGN",
                        isAmountFixed: 'Y',
                        amount: 6000
                    },
                    {
                        billerId: 12,
                        productId: 21,
                        productCode: "51617",
                        productName: "Insurance 2015",
                        paymentCurrency: "NGN",
                        isAmountFixed: 'N',
                        amount: 0
                    },
                ]
            });
        }
    };
    DashboardService.prototype.getBeneficiaryRecentPayment = function (userId, beneficiaryId) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId) || beneficiaryId < 1) {
            throw new Error("Invalid parameters.");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.GET_RECENT_BENEFICIARY_TRANSFER, { UserId: userId, BeneficiaryId: beneficiaryId }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                Transactions: [
                    {
                        currency: "NGN",
                        amount: 5000000,
                        transactionDate: "2018-02-14T12:14:30.6534304+01:00"
                    },
                    {
                        currency: "NGN",
                        amount: 45000000,
                        transactionDate: "2018-03-19T12:14:30.6534304+01:00"
                    },
                    {
                        currency: "USD",
                        amount: 32000,
                        transactionDate: "2018-05-19T12:14:30.6534304+01:00"
                    },
                    {
                        currency: "EUR",
                        amount: 50000,
                        transactionDate: "2018-04-19T12:14:30.6534304+01:00"
                    },
                    {
                        currency: "NGN",
                        amount: 34569903,
                        transactionDate: "2018-03-23T12:14:30.6534304+01:00"
                    }
                ]
            });
        }
    };
    DashboardService.prototype.doNameEnquiry = function (userId, destinationAccountNumber, destinationBankCode) {
        if (utils_services_1.UtilService.StringIsNullOrEmpty(userId) || utils_services_1.UtilService.StringIsNullOrEmpty(destinationAccountNumber) || utils_services_1.UtilService.StringIsNullOrEmpty(destinationBankCode)) {
            throw new Error("Invalid parameters");
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(appsettings_constant_1.Appsettings.NAME_ENQUIRY_URL, {
                UserId: userId,
                DestinationAccountNo: destinationAccountNumber,
                DestinationBankCode: destinationBankCode
            }, this._headerService.getRequestJsonHeaders()).map(function (_response) {
                return _response.json();
            });
        }
        else {
            return Observable_1.Observable.of({
                ResponseCode: response_codes_constant_1.ResponseCodes.SUCCESS,
                ResponseFriendlyMessage: "Name Enquiry Technical Error."
            });
        }
    };
    DashboardService.prototype.getBeneficiaries = function () {
        var beneficiaries = new Array();
        var _beneficiary = new beneficiaries_model_1.Beneficiary();
        //  _beneficiary.beneficiaryAbbreviation = "G";
        _beneficiary.beneficiaryName = 'GABBY';
        _beneficiary.beneficiaryBank = 'GUARANTY TRUST BANK';
        // _beneficiary.myReference = 'SEUN POPOOLA.';
        _beneficiary.beneficiaryReference = 'GABBY';
        // _beneficiary.beneficiaryGroup = 'NONE';
        //  _beneficiary.beneficiaryAccountNo = "0011289297";
        _beneficiary.beneficiaryAmountFormRef = "0011289297_amount";
        _beneficiary.beneficiaryMyReferenceFormRef = "0011289297_my_ref";
        _beneficiary.beneficiaryTheirReferenceFormRef = "0011289297_their_ref";
        // _beneficiary.beneficiaryAccountBankLocal = false;
        //  _beneficiary.beneficiaryAccountCurrenyLocal = true;
        beneficiaries.push(_beneficiary);
        _beneficiary = new beneficiaries_model_1.Beneficiary();
        // _beneficiary.beneficiaryAbbreviation = "K";
        _beneficiary.beneficiaryName = 'KAYODE';
        _beneficiary.beneficiaryBank = 'GUARANTY TRUST BANK';
        // _beneficiary.myReference = 'SEUN POPE.';
        //_beneficiary.beneficiaryGroup = 'NONE';
        // _beneficiary.beneficiaryAccountNo = "0011289296";
        _beneficiary.beneficiaryReference = 'KAYODE POPE';
        _beneficiary.beneficiaryAmountFormRef = "0011289296_amount";
        _beneficiary.beneficiaryMyReferenceFormRef = "0011289296_my_ref";
        _beneficiary.beneficiaryTheirReferenceFormRef = "0011289296_their_ref";
        //_beneficiary.beneficiaryAccountBankLocal = false;
        //  _beneficiary.beneficiaryAccountCurrenyLocal = true;
        beneficiaries.push(_beneficiary);
        // _beneficiary = new Beneficiary();
        // _beneficiary.beneficiaryAbbreviation = "MG";
        // _beneficiary.beneficiaryName = 'MY GTB';
        // _beneficiary.beneficiaryBank = 'GUARANTY TRUST BANK';
        // _beneficiary.myReference = 'MY GTB.';
        // _beneficiary.beneficiaryReference = 'MY GTB';
        // _beneficiary.beneficiaryGroup = 'NONE';
        // _beneficiary.beneficiaryAccountNo = "0011289295";
        // _beneficiary.beneficiaryAmountFormRef = "0011289295_amount";
        // _beneficiary.beneficiaryMyReferenceFormRef = "0011289295_my_ref";
        // _beneficiary.beneficiaryTheirReferenceFormRef = "0011289295_their_ref";
        // _beneficiary.beneficiaryAccountBankLocal = false;
        // _beneficiary.beneficiaryAccountCurrenyLocal = true;
        // beneficiaries.push(_beneficiary);
        // _beneficiary = new Beneficiary();
        // _beneficiary.beneficiaryAbbreviation = "PD";
        // _beneficiary.beneficiaryName = 'POPOOLA DAYO';
        // _beneficiary.beneficiaryBank = 'GUARANTY TRUST BANK';
        // _beneficiary.myReference = 'POPOOLA SADE.';
        // _beneficiary.beneficiaryReference = 'POPOOLA SADE';
        // _beneficiary.beneficiaryGroup = 'NONE';
        // _beneficiary.beneficiaryAccountNo = "0011289294";
        // _beneficiary.beneficiaryAmountFormRef = "0011289294_amount";
        // _beneficiary.beneficiaryMyReferenceFormRef = "0011289294_my_ref";
        // _beneficiary.beneficiaryTheirReferenceFormRef = "0011289294_their_ref";
        // _beneficiary.beneficiaryAccountBankLocal = false;
        // _beneficiary.beneficiaryAccountCurrenyLocal = true;
        // beneficiaries.push(_beneficiary);
        // _beneficiary = new Beneficiary();
        // _beneficiary.beneficiaryAbbreviation = "ps";
        // _beneficiary.beneficiaryName = 'POPOOLA TOSIN Stanbic';
        // _beneficiary.beneficiaryBank = 'Stanbic IBTC BANK';
        // _beneficiary.myReference = 'SEUN POPE.';
        // _beneficiary.beneficiaryReference = 'TOSIN POPE';
        // _beneficiary.beneficiaryGroup = 'NONE';
        // _beneficiary.beneficiaryAccountNo = "0011289291";
        // _beneficiary.beneficiaryAmountFormRef = "0011289291_amount";
        // _beneficiary.beneficiaryMyReferenceFormRef = "0011289291_my_ref";
        // _beneficiary.beneficiaryTheirReferenceFormRef = "0011289291_their_ref";
        // _beneficiary.beneficiaryAccountBankLocal = false;
        // _beneficiary.beneficiaryAccountCurrenyLocal = true;
        // beneficiaries.push(_beneficiary);
        // _beneficiary = new Beneficiary();
        // _beneficiary.beneficiaryAbbreviation = "SN";
        // _beneficiary.beneficiaryName = 'SEUN NELSON';
        // _beneficiary.beneficiaryBank = 'Diamond BANK';
        // _beneficiary.myReference = 'SEUN POPE.';
        // _beneficiary.beneficiaryReference = 'PASTOR SEUN NELSON IGE';
        // _beneficiary.beneficiaryGroup = 'NONE';
        // _beneficiary.beneficiaryAccountNo = "0011289292";
        // _beneficiary.beneficiaryAmountFormRef = "0011289292_amount";
        // _beneficiary.beneficiaryMyReferenceFormRef = "0011289292_my_ref";
        // _beneficiary.beneficiaryTheirReferenceFormRef = "0011289292_their_ref";
        // _beneficiary.beneficiaryAccountBankLocal = false;
        // _beneficiary.beneficiaryAccountCurrenyLocal = true;
        // beneficiaries.push(_beneficiary);
        return beneficiaries;
    };
    DashboardService.prototype.getBeneficiaries2 = function () {
        var beneficiaries = new Array();
        var _beneficiary = new beneficiaries_model_1.Beneficiary();
        //_beneficiary.beneficiaryAbbreviation = "G";
        _beneficiary.beneficiaryName = 'GABBY';
        _beneficiary.beneficiaryBank = 'GUARANTY TRUST BANK';
        //   _beneficiary.myReference = 'SEUN POPOOLA.';
        //  _beneficiary.beneficiaryGroup = 'NONE';
        // _beneficiary.beneficiaryAccountNo = "0493932002";
        _beneficiary.beneficiaryAmountFormRef = "0493932002_amount";
        _beneficiary.beneficiaryMyReferenceFormRef = "0493932002_my_ref";
        _beneficiary.beneficiaryTheirReferenceFormRef = "0493932002_their_ref";
        //     _beneficiary.beneficiaryAccountBankLocal = true;
        //    _beneficiary.beneficiaryAccountCurrenyLocal = true;
        beneficiaries.push(_beneficiary);
        return beneficiaries;
    };
    // beneficiaryToFormGroup(beneficiaries: Array<Beneficiary>) {
    //     let group: any = {};
    //     beneficiaries.forEach(beneficiary => {
    //         group[beneficiary.beneficiaryAmountFormRef] = new FormControl('', Validators.required);
    //         group[beneficiary.beneficiaryMyReferenceFormRef] = new FormControl('');
    //         group[beneficiary.beneficiaryTheirReferenceFormRef] = new FormControl('');
    //         group[beneficiary.beneficiaryAmountFormRef + "_beneficiaryEmail"] = new FormControl('');
    //         group[beneficiary.beneficiaryAmountFormRef + "_beneficiaryPhoneNo"] = new FormControl('');
    //         group[beneficiary.beneficiaryAmountFormRef + "_beneficiaryCountryCode"] = new FormControl('');
    //     });
    //     return new FormGroup(group);
    // }
    DashboardService.prototype.beneficiaryToFormGroup = function (beneficiaries) {
        var group = {};
        beneficiaries.forEach(function (beneficiary) {
            group[beneficiary.beneficiaryId + "_beneficiaryAmount"] = new forms_1.FormControl('', forms_1.Validators.required);
            group[beneficiary.beneficiaryId + "_beneficiaryCustomerRef"] = new forms_1.FormControl('');
            group[beneficiary.beneficiaryId + "_beneficiaryRef"] = new forms_1.FormControl('');
            group[beneficiary.beneficiaryId + "_beneficiaryEmail"] = new forms_1.FormControl('');
            group[beneficiary.beneficiaryId + "_beneficiaryPhoneNo"] = new forms_1.FormControl('');
            group[beneficiary.beneficiaryId + "_beneficiaryCountryCodeSelected"] = new forms_1.FormControl('');
            group[beneficiary.beneficiaryId + "_beneficiaryCountryCode"] = new forms_1.FormControl('');
            group[beneficiary.beneficiaryId + "_errorOnView"] = new forms_1.FormControl('');
        });
        return new forms_1.FormGroup(group);
    };
    DashboardService.prototype.getCountryCodes = function () {
        var countryCodes = new Array();
        var _countryCode = new countryPhoneCode_model_1.CountryPhoneCode();
        _countryCode.countryName = "Abkhazia";
        _countryCode.code = "7";
        countryCodes.push(_countryCode);
        _countryCode = new countryPhoneCode_model_1.CountryPhoneCode();
        _countryCode.countryName = "Afghanistan";
        _countryCode.code = "93";
        countryCodes.push(_countryCode);
        _countryCode = new countryPhoneCode_model_1.CountryPhoneCode();
        _countryCode.countryName = "Albania";
        _countryCode.code = "355";
        countryCodes.push(_countryCode);
        _countryCode = new countryPhoneCode_model_1.CountryPhoneCode();
        _countryCode.countryName = "Algeria";
        _countryCode.code = "213";
        countryCodes.push(_countryCode);
        return countryCodes;
    };
    DashboardService.prototype.getBanks = function () {
        var banks = new Array();
        var bank = new bank_model_1.Bank();
        bank.name = "Stanbic IBTC Bank";
        bank.code = "221";
        banks.push(bank);
        bank = new bank_model_1.Bank();
        bank.name = "Guaranty Trust Bank";
        bank.code = "058";
        banks.push(bank);
        bank = new bank_model_1.Bank();
        bank.name = "Standard chartered bank";
        bank.code = "332";
        banks.push(bank);
        bank = new bank_model_1.Bank();
        bank.name = "Paga";
        bank.code = "337";
        banks.push(bank);
        bank = new bank_model_1.Bank();
        bank.name = "First Bank of Nigeria";
        bank.code = "987";
        banks.push(bank);
        bank = new bank_model_1.Bank();
        bank.name = "Fideliy Bank";
        bank.code = "100";
        banks.push(bank);
        bank = new bank_model_1.Bank();
        bank.name = "ACCESS BANK";
        bank.code = "338";
        banks.push(bank);
        bank = new bank_model_1.Bank();
        bank.name = "First City Monument Bank";
        bank.code = "442";
        banks.push(bank);
        bank = new bank_model_1.Bank();
        bank.name = "Diamond Bank";
        bank.code = "061";
        banks.push(bank);
        bank = new bank_model_1.Bank();
        bank.name = "Wema Bank";
        bank.code = "908";
        banks.push(bank);
        return banks;
    };
    DashboardService.prototype.getMyAccounts = function () {
        var accounts = new Array();
        var account = new account_model_1.Account();
        account.name = "Atanda Shola";
        account.number = "0027738382";
        account.availableBalance = 450000;
        accounts.push(account);
        account = new account_model_1.Account();
        account.name = "Chesky Alan";
        account.number = "006372992";
        account.availableBalance = 780000;
        accounts.push(account);
        account = new account_model_1.Account();
        account.name = "Akala Bola";
        account.number = "006745362";
        account.availableBalance = 1500000;
        accounts.push(account);
        account = new account_model_1.Account();
        account.name = "Chukwudi Agnes";
        account.number = "0073627282";
        account.availableBalance = 3000000;
        accounts.push(account);
        return accounts;
    };
    DashboardService.prototype.generateStatement = function (headerLogo, watermarkImage, disclaimerImage, accountNo, generationDate, accountName, fromDate, toDate, openingBalance, closingBalance, statementCurrency, headerColumns, rowValues) {
        var doc = new jsPDF("p", "pt");
        // margin is 40millimeter both left and right
        // Draw First Page
        // Add Stanbic Logo
        doc.setFillColor(240, 243, 248);
        doc.rect(0, 0, 600, 210, "F");
        // Set Provisional Statement and Account Number Header
        doc.setFontSize(12);
        doc.setTextColor(0, 0, 0);
        doc.setFontStyle("bold");
        doc.text("Provisional Statement", 40, 110);
        doc.text("Account Number: " + accountNo, 555, 110, "right");
        // Set details as at
        doc.setFontStyle("normal");
        doc.setFontSize(10);
        doc.setTextColor(102, 102, 102);
        doc.text("Details as on: ", 40, 125);
        doc.setTextColor(51, 51, 51);
        // doc.text('2:46:46 PM, 20/03/2018', 102, 125);
        doc.text(generationDate, 102, 125);
        // Set account name
        doc.setFontSize(10);
        doc.setTextColor(51, 51, 51);
        doc.text(accountName, 555, 125, "right");
        doc.setFontSize(10);
        doc.setTextColor(102, 102, 102);
        // Set From Date Headers
        doc.text("From date", 40, 160);
        // Set To Date Headers
        doc.text("To date", 170, 160);
        // Set Opening Balance Headers
        doc.text("Opening balance", 415, 160, "right");
        // Set Closing Balance Headers
        doc.text("Closing balance", 555, 160, "right");
        doc.setFontSize(12);
        doc.setTextColor(51, 51, 51);
        doc.setFontStyle("bold");
        // Set From Date Value
        // doc.text("27/09/2017", 40, 175);
        doc.text(fromDate, 40, 175);
        // Set To Date Value      
        doc.text(toDate, 170, 175);
        // Set Opening Balance Value
        doc.text(statementCurrency.toUpperCase() + " " + openingBalance, 415, 175, "right");
        // Set Closing Balance Value
        doc.text(statementCurrency.toUpperCase() + " " + closingBalance, 555, 175, "right");
        // Generate Table
        doc.autoTable(headerColumns, rowValues, {
            startY: 210,
            styles: { fontSize: 8, lineHeight: 30, valign: "middle", halign: 'left', tableWidth: 'auto',
                columnWidth: 'auto' },
            headerStyles: {
                fillColor: [34, 68, 132],
                fontStyle: 'bold',
                textColor: [255, 255, 255]
            },
            drawHeaderRow: function (row, data) {
                row.height = 35;
                row.width = 600;
            },
            drawRow: function (row, data) {
                row.height = 30;
                row.width = 600;
            },
            margin: {
                right: 0,
                left: 0,
                top: 100,
                bottom: 40
            }
        });
        // Place Disclaimer Image. Height of Disclaimer: 120  
        var lastPageHeightLessFooter = doc.internal.pageSize.height - 30;
        // doc.autoTable.previous.finalY -- gives the y-coordinate of the bottom of the table on the current page.
        // Add 150 to the value to prevent disclaimer from falling on the footer
        if (doc.autoTable.previous.finalY + 150 < lastPageHeightLessFooter) {
            doc.addImage(disclaimerImage, "PNG", 50, doc.autoTable.previous.finalY + 20, 493, 120);
        }
        else {
            doc.addPage();
            doc.addImage(disclaimerImage, "PNG", 50, 100, 493, 120);
        }
        //Add footer, watermark, and stanbic logo to each page
        var numberOfPages = doc.internal.getNumberOfPages();
        doc.setFontStyle("normal");
        doc.setFontSize(13);
        doc.setTextColor(0, 0, 0);
        for (var O = 1; O <= numberOfPages; O++) {
            var P = "Page " + O + " of " + numberOfPages;
            var Q = doc.internal.pageSize.height - 30;
            doc.setPage(O);
            doc.addImage(watermarkImage, "PNG", 220, 330, 160, 190);
            doc.addImage(headerLogo, "PNG", 40, 40, 183, 34);
            doc.text(P, 40, Q);
        }
        return doc;
        // doc.autoPrint();
        // var blobUrl = doc.output('bloburl'); 
        // window.frames["printOutput"].focus();
        // window.frames["printOutput"].print();
        // doc.save('output.pdf');  
    };
    DashboardService.prototype.msieversion = function () {
        var ua = _window().navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        if (msie != -1 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
            return true;
        }
        else {
            return false;
        }
        //return false; 
    };
    DashboardService.prototype.JSONToCSVConvertor = function (JSONData, ShowLabel, fileName) {
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
        var CSV = '';
        if (ShowLabel) {
            var row = "";
            for (var index in arrData[0]) {
                row += index + ',';
            }
            row = row.slice(0, -1);
            CSV += row + '\r\n';
        }
        for (var i = 0; i < arrData.length; i++) {
            var row = "";
            //  for (var index in arrData[i]) {
            // 		var arrValue = arrData[i][index] == null ? "" : '="' + arrData[i][index] + '"';
            // 		row += arrValue + ',';
            //  }
            for (var index in arrData[i]) {
                //var arrValue = arrData[i][index] == null ? "" : '="' + arrData[i][index] + '"';
                var arrValue = arrData[i][index];
                row += arrValue + ',';
            }
            //row.slice(0, row.length - 1);
            CSV += row + '\r\n';
        }
        if (CSV == '') {
            //  growl.error("Invalid data");
            console.error("Invalid data");
            return;
        }
        //var fileName = "Result";
        if (this.msieversion()) {
            var IEwindow = _window().open();
            IEwindow.document.write('sep=,\r\n' + CSV);
            IEwindow.document.close();
            IEwindow.document.execCommand('SaveAs', true, fileName + ".csv");
            IEwindow.close();
        }
        else {
            var uri = 'data:application/csv;charset=utf-8,' + _window().escape(CSV);
            var link = _window().document.createElement("a");
            link.href = uri;
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";
            _window().document.body.appendChild(link);
            link.click();
            _window().document.body.removeChild(link);
        }
    };
    DashboardService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof header_service_1.HeaderService !== 'undefined' && header_service_1.HeaderService) === 'function' && _b) || Object, http_1.Http])
    ], DashboardService);
    return DashboardService;
    var _a, _b;
}());
exports.DashboardService = DashboardService;
//# sourceMappingURL=dashboard.services.js.map