"use strict";
var BeneficiaryPaymentFormBase = (function () {
    function BeneficiaryPaymentFormBase(options) {
        if (options === void 0) { options = {}; }
        this.value = options.value;
        this.key = options.key || '';
        this.required = !!options.required;
        this.controlType = options.controlType || '';
    }
    return BeneficiaryPaymentFormBase;
}());
exports.BeneficiaryPaymentFormBase = BeneficiaryPaymentFormBase;
//# sourceMappingURL=beneficiary_payment_dynamic_form.model.js.map