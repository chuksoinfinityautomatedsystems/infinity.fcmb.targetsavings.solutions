// export class Biller{ 
//     billerName: string;
//     billerNickname: string;
//     myReference: string; 
//     billerCollectionValue: string; 
// }
"use strict";
var BillerBeneficiary = (function () {
    function BillerBeneficiary() {
    }
    return BillerBeneficiary;
}());
exports.BillerBeneficiary = BillerBeneficiary;
var Biller = (function () {
    function Biller() {
    }
    return Biller;
}());
exports.Biller = Biller;
var BillerCategory = (function () {
    function BillerCategory() {
    }
    return BillerCategory;
}());
exports.BillerCategory = BillerCategory;
var BillerProduct = (function () {
    function BillerProduct() {
    }
    return BillerProduct;
}());
exports.BillerProduct = BillerProduct;
var ServiceOperator = (function () {
    function ServiceOperator() {
    }
    return ServiceOperator;
}());
exports.ServiceOperator = ServiceOperator;
var ServiceOperatorProduct = (function () {
    function ServiceOperatorProduct() {
    }
    return ServiceOperatorProduct;
}());
exports.ServiceOperatorProduct = ServiceOperatorProduct;
//# sourceMappingURL=biller.model.js.map