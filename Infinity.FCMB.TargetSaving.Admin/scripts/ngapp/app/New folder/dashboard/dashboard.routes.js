"use strict";
var router_1 = require("@angular/router");
var authorization_services_1 = require("../authentication/services/authorization.services");
var dashboard_component_1 = require('./components/dashboard.component');
var beneficiaries_component_1 = require('./components/Transfer/beneficiary/beneficiaries.component');
var beneficiary_payment_component_1 = require('./components/Transfer/beneficiary/beneficiary-payment.component');
var one_off_payment_component_1 = require('./components/Transfer/one-off-payment.component');
var add_beneficiary_component_1 = require('./components/Transfer/beneficiary/add-beneficiary.component');
var billers_component_1 = require('./components/Transfer/biller/billers.component');
var pay_biller_details_component_1 = require('./components/Transfer/biller/pay-biller-details.component');
var add_biller_component_1 = require('./components/Transfer/biller/add-biller.component');
var buyAirtime_component_1 = require('./components/Buy/buyAirtime.component');
var redeem_component_1 = require('./components/Western_Union/redeem.component');
var remita_component_1 = require('./components/Transfer/remita.component');
var self_transfer_component_1 = require('./components/Transfer/self-transfer.component');
var statement_component_1 = require('./components/statement.component');
var beneficiary_details_component_1 = require('./components/Transfer/beneficiary/beneficiary-details.component');
var edit_beneficiary_component_1 = require('./components/Transfer/beneficiary/edit-beneficiary.component');
var biller_details_component_1 = require('./components/Transfer/biller/biller-details.component');
var edit_biller_component_1 = require('./components/Transfer/biller/edit-biller.component');
var international_payment_component_1 = require('./components/Transfer/international-payment.component');
exports.dashboardRoutes = router_1.RouterModule.forChild([
    { path: "dashboard", component: dashboard_component_1.DashboardComponent, canActivate: [authorization_services_1.AuthorizeService] },
    {
        path: "MainView/PayTransfer/Beneficiary/List",
        component: beneficiaries_component_1.BeneficiariesComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/Beneficiary/Details",
        component: beneficiary_payment_component_1.PayBeneficiariesComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/Beneficiary/Add",
        component: add_beneficiary_component_1.AddBeneficiaryComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/OneOffPayment",
        component: one_off_payment_component_1.OneOffPaymentComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/Recipient-details",
        component: beneficiary_details_component_1.BeneficiaryDetailsComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/Edit-Beneficiary",
        component: edit_beneficiary_component_1.EditBeneficiaryComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/Accounts/AccountTransactions",
        component: statement_component_1.AccountStatementComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/Biller/List",
        component: billers_component_1.BillersComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/Biller/PayBillerDetails",
        component: pay_biller_details_component_1.PayBillerDetailsComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/Recipients/Biller/AddBiller",
        component: add_biller_component_1.AddBillerComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/Recipients/Biller/Details",
        component: biller_details_component_1.BillerDetailsComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/Edit-Biller",
        component: edit_biller_component_1.EditBillerComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/InternationalPayments",
        component: international_payment_component_1.InternationalPaymentComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    {
        path: "MainView/PayTransfer/Self-transfer",
        component: self_transfer_component_1.SelfTransferComponent,
        canActivate: [authorization_services_1.AuthorizeService],
        pathMatch: 'full'
    },
    // { path: "OnceOffPayment", component: OneOffPaymentComponent, canActivate: [AuthorizeService] },
    { path: "add-beneficiary", component: add_beneficiary_component_1.AddBeneficiaryComponent, canActivate: [authorization_services_1.AuthorizeService] },
    { path: "remita-payment", component: remita_component_1.RemitaComponent, canActivate: [authorization_services_1.AuthorizeService] },
    // { path: "self-transfer", component: SelfTransferComponent, canActivate: [AuthorizeService] },
    { path: "buy-airtime", component: buyAirtime_component_1.BuyAirtimeComponent, canActivate: [authorization_services_1.AuthorizeService] },
    { path: "redeem-western-union", component: redeem_component_1.RedeemWesternUnionComponent, canActivate: [authorization_services_1.AuthorizeService] },
    { path: "", component: dashboard_component_1.DashboardComponent, canActivate: [authorization_services_1.AuthorizeService] }
]);
//# sourceMappingURL=dashboard.routes.js.map