"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var dashboard_services_1 = require("../services/dashboard.services");
var utils_services_1 = require("../../commons/services/utils.services");
var constants_services_1 = require("../../commons/services/constants.services");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var authentication_service_1 = require("../../authentication/services/authentication.service");
var base_component_1 = require("../../commons/components/base.component");
var account_model_1 = require("../models/account.model");
function _window() {
    // return the global native browser window object
    return window;
}
var DashboardComponent = (function (_super) {
    __extends(DashboardComponent, _super);
    function DashboardComponent(authService, utiilService, router, dashService, constantService, cdRef) {
        _super.call(this, authService, router);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.burgerMenuIsActive = this.showExceptionPage = false;
        this.HeroAccountInformation = new account_model_1.CustomerAccount();
        this.CustomerAccounts = new Array();
        this.CustomerAccountTransactions = new Array();
        this.showSessionTimeoutPopup = false;
        this.authService.setupAutoLogout();
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var self = this;
        self.showBusyLoader = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                self._getAccountList();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getAccountList();
        }
        this.lastLogin = authentication_service_1.AuthenticationService.authUserObj.LastLoginIn;
        self.SetupTimeout();
    };
    DashboardComponent.prototype.keepBanking = function (event) {
        event.stopPropagation();
        var self = this;
        this.showSessionTimeoutPopup = false;
        if (self.sessionTimeoutInterval) {
            _window().clearInterval(self.sessionTimeoutInterval);
        }
        this.cdRef.detectChanges();
        this.authService.setupAutoLogout();
    };
    DashboardComponent.prototype._getAccountTransactionDetails = function (accountNo) {
        var self = this;
        self.getCustomerAccountTransactionsSub = self.dashService.getCustomerTransactionDetails(accountNo, 5)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.CustomerAccountTransactions = response.Transactions;
                self.transactionLoadingStatus = "";
            }
            else {
                self.transactionLoadingStatus = "Transaction not available";
            }
            self.cdRef.detectChanges();
        }, function (error) {
            // self.showBusyLoader = false;
            //   self.showTransactionLoading = false; 
            self.cdRef.detectChanges();
        }, function () {
            // self.showBusyLoader = false; 
            // self.showTransactionLoading = false;  
            self.cdRef.detectChanges();
        });
    };
    DashboardComponent.prototype._getAccountList = function () {
        var self = this;
        self.getCustomerAccountsSub = self.dashService.getCustomerAccountList(authentication_service_1.AuthenticationService.authUserObj.CifID)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                if (response.Accounts && response.Accounts.length > 0) {
                    if (self.HeroAccountInformation == undefined || self.HeroAccountInformation == null) {
                        self.HeroAccountInformation = new account_model_1.CustomerAccount();
                    }
                    var accounts = dashboard_services_1.DashboardService.CrossPageCustomerAccountList = response.Accounts;
                    self.HeroAccountInformation = accounts[0];
                    if (self.CustomerAccounts == undefined || self.CustomerAccounts == null) {
                        self.CustomerAccounts = new Array();
                    }
                    self.CustomerAccounts = accounts;
                    self.CustomerAccounts[0].isHero = true;
                    self.transactionLoadingStatus = "Loading...";
                    self._getAccountTransactionDetails(self.HeroAccountInformation.accountNumber);
                }
            }
        }, function (error) {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    // TOP LINKS
    DashboardComponent.prototype.gotoPayBeneficiary = function () {
        // this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
        return false;
    };
    DashboardComponent.prototype.gotoBuy = function () {
        // this.ngOnDestroy();
        this.router.navigate(['buy-airtime']);
        return false;
    };
    DashboardComponent.prototype.gotoRedeem = function () {
        //  this.ngOnDestroy();
        this.router.navigate(['redeem-western-union']);
        return false;
    };
    // MENU LINKS
    DashboardComponent.prototype.toggleMenu = function () {
        this.burgerMenuIsActive = !this.burgerMenuIsActive;
    };
    DashboardComponent.prototype.openMenu = function () {
        this.burgerMenuIsActive = !this.burgerMenuIsActive;
    };
    DashboardComponent.prototype.gotoTxnReceipt = function () {
        //this.ngOnDestroy();
        this.router.navigate(['manage-receipts']);
        return false;
    };
    DashboardComponent.prototype.gotoServiceRequest = function () {
        // this.ngOnDestroy();
        this.router.navigate(['service-request']);
        return false;
    };
    DashboardComponent.prototype.goToProfilesAndSettings = function () {
        var self = this;
        if (self.sessionTimeoutInterval) {
            _window().clearInterval(self.sessionTimeoutInterval);
        }
        if (this.sessionTimeoutSub) {
            this.sessionTimeoutSub.unsubscribe();
        }
        //  this.ngOnDestroy();
        this.router.navigate(['profile-and-settings']);
        return false;
    };
    DashboardComponent.prototype.goToScheduteTxn = function () {
        // this.ngOnDestroy();
        this.router.navigate(['manage-scheduled-receipts']);
        return false;
    };
    DashboardComponent.prototype.goToIBranchOnline = function () {
        //this.ngOnDestroy();
        this.router.navigate(['ibranch-collections/category']);
        return false;
    };
    DashboardComponent.prototype.goToPaycodePage = function () {
        //this.ngOnDestroy();
        this.router.navigate(['generate-paycode']);
        return false;
    };
    // ACCOUNT STATEMENT PAGE  
    DashboardComponent.prototype.goToAccountStatement = function (accountNo) {
        console.log(dashboard_services_1.DashboardService.CrossPageCustomerAccountList);
        console.log(accountNo);
        console.log(dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.accountNumber.toLowerCase() == accountNo.toLowerCase(); }));
        dashboard_services_1.DashboardService.CrossStatementAccount = dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.accountNumber.toLowerCase() == accountNo.toLowerCase(); })[0];
        console.log(dashboard_services_1.DashboardService.CrossStatementAccount);
        this.router.navigate(['MainView/Accounts/AccountTransactions']);
        return false;
    };
    // Others    
    DashboardComponent.prototype.gotoManageBeneficiaries = function () {
    };
    DashboardComponent.prototype.accountsToDashboard = function () {
    };
    //  goToAccounts(accountNo: string, customerName: string, heroTile: boolean) {
    //    this.router.navigate(['redeem-western-union']); 
    //      return false;
    //  } 
    // goToAccounts() {
    //    this.router.navigate(['redeem-western-union']); 
    //      return false;
    //  }
    DashboardComponent.prototype.ngOnDestroy = function () {
        //alert(111);
        if (this.getCustomerAccountsSub) {
            this.getCustomerAccountsSub.unsubscribe();
        }
        if (this.getCustomerAccountTransactionsSub) {
            this.getCustomerAccountTransactionsSub.unsubscribe();
        }
    };
    DashboardComponent.prototype.logout = function () {
        var self = this;
        if (self.sessionTimeoutInterval) {
            _window().clearInterval(self.sessionTimeoutInterval);
        }
        if (this.sessionTimeoutSub) {
            this.sessionTimeoutSub.unsubscribe();
        }
        authentication_service_1.AuthenticationService.authLogoutMessage = appsettings_constant_1.Appsettings.LOGOUT_MESSAGE;
        // this.ngOnDestroy();
        this.authService.logout();
        this.router.navigate(['login']);
    };
    DashboardComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/dashboard.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], DashboardComponent);
    return DashboardComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map