"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../services/dashboard.services");
var utils_services_1 = require("../../../commons/services/utils.services");
var constants_services_1 = require("../../../commons/services/constants.services");
var authentication_service_1 = require("../../../authentication/services/authentication.service");
function _window() {
    // return the global native browser window object
    return window;
}
var RedeemWesternUnionComponent = (function () {
    function RedeemWesternUnionComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef) {
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.redeemFormGroup = formBuilder.group({});
        this.cancelModalOpened = this.toolExpectedAmountHelperShown = this.toolMtcnHelperShown = this.toolCountryCodeHelperShown
            = this.toolTestQuestionHelperShown = false;
    }
    RedeemWesternUnionComponent.prototype.toggletoolExpectedAmountHelper = function () {
        this.toolExpectedAmountHelperShown = !this.toolExpectedAmountHelperShown;
    };
    RedeemWesternUnionComponent.prototype.toggletoolMtcnHelper = function () {
        this.toolMtcnHelperShown = !this.toolMtcnHelperShown;
    };
    RedeemWesternUnionComponent.prototype.toggletoolCountryCodeHelper = function () {
        this.toolCountryCodeHelperShown = !this.toolCountryCodeHelperShown;
    };
    RedeemWesternUnionComponent.prototype.toggletoolTestQuestionHelper = function () {
        this.toolTestQuestionHelperShown = !this.toolTestQuestionHelperShown;
    };
    RedeemWesternUnionComponent.prototype.hideCancelTransferModal = function () {
        this.cancelModalOpened = false;
    };
    RedeemWesternUnionComponent.prototype.CloseTranfer = function () {
        this.cancelModalOpened = true;
    };
    RedeemWesternUnionComponent.prototype.backHome = function () {
        this.router.navigate(['dashboard']);
    };
    RedeemWesternUnionComponent.prototype.nextToReview = function (event) {
        event.stopPropagation();
        this.pageIndex = 2;
        this.pageSubtitle = "Review";
        return false;
    };
    RedeemWesternUnionComponent.prototype.BuyAgain = function (event) {
        // clear all form fields
        event.stopPropagation();
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        return false;
    };
    RedeemWesternUnionComponent.prototype.buyAirtime = function (event) {
        event.stopPropagation();
        this.pageIndex = 3;
        this.pageSubtitle = "Receipt";
        return false;
    };
    RedeemWesternUnionComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    RedeemWesternUnionComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    RedeemWesternUnionComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/western_union/redeem.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], RedeemWesternUnionComponent);
    return RedeemWesternUnionComponent;
    var _a, _b, _c, _d;
}());
exports.RedeemWesternUnionComponent = RedeemWesternUnionComponent;
//# sourceMappingURL=redeem.component.js.map