"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require('@angular/common');
var dashboard_services_1 = require("../services/dashboard.services");
var utils_services_1 = require("../../commons/services/utils.services");
var constants_services_1 = require("../../commons/services/constants.services");
var authentication_service_1 = require("../../authentication/services/authentication.service");
var base_component_1 = require("../../commons/components/base.component");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var appsettings_base64_constant_1 = require("../../commons/constants/appsettings.base64.constant");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var event_handlers_service_1 = require("../../commons/services/event_handlers.service");
function _window() {
    // return the global native browser window object
    return window;
}
var AccountStatementComponent = (function (_super) {
    __extends(AccountStatementComponent, _super);
    function AccountStatementComponent(authService, utiilService, router, dashService, constantService, cdRef, decimalPipe) {
        _super.call(this, authService, router);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        this.decimalPipe = decimalPipe;
        document.title = "STANBIC IBTC BANK";
        this.burgerMenuIsActive = this.filterActive = this.showDetailOverlay = this.showServiceOverlay = false;
        this.hangFilterModal = this.animateModalUp = false;
        this.dateGroupedTransactions = new Array();
        this.currentNoOfMonthsInStatement = 0;
        this.hideViewMore = false;
        this.statementFilterPlaceholder = '';
        this.downloadTypes = appsettings_constant_1.Appsettings.DOWNLOAD_TYPES;
    }
    AccountStatementComponent.prototype.ngOnInit = function () {
        if (!dashboard_services_1.DashboardService.CrossStatementAccount) {
            this.router.navigate(['dashboard']);
        }
        this.statementAccount = dashboard_services_1.DashboardService.CrossStatementAccount;
        var self = this;
        self.showBusyLoader = true;
        this.transactionEndDate = new Date();
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                self._getAccountTransactionDetails(dashboard_services_1.DashboardService.CrossStatementAccount.accountNumber);
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getAccountTransactionDetails(dashboard_services_1.DashboardService.CrossStatementAccount.accountNumber);
        }
        //      $('#fromDateCal').datetimepicker({
        //             widgetPositioning: {
        //                 horizontal: 'auto',
        //                 vertical: 'bottom'
        //          },
        //     //     startDate: '01-03-2018',
        //             showTodayButton: false,
        //             useCurrent: false,
        //             keepOpen: true,
        //             format: 'l',
        //             locale: moment.locale('en-gb')
        // });
        //From Date:
        // start from a month ago to today.
        // end date should be today.
        // On date select, change --TO Date-- start date to selected.
        //End Date:
        // end Date -- today
        // start date -- controlled by From Date.
        // clear date when new From Date is selected.
        var initToCalendarInterval = setInterval(function () {
            if ($('#toDateCal').length > 0) {
                $('#toDateCal').datepicker({
                    orientation: "bottom left",
                    autoclose: true,
                    format: 'dd M yy',
                    endDate: '0d'
                }).on('changeDate', function (e) {
                    self.toCalValue = moment(e.date).format('DD-MM-YYYY');
                    self.textInputActive = "";
                    self.cdRef.detectChanges();
                });
                _window().clearInterval(initToCalendarInterval);
            }
        }, 300);
        var initFromCalendarInterval = setInterval(function () {
            if ($('#fromDateCal').length > 0) {
                $('#fromDateCal').datepicker({
                    orientation: "bottom left",
                    autoclose: true,
                    format: 'dd M yy',
                    startDate: '-1m',
                    endDate: '0d'
                })
                    .on('changeDate', function (e) {
                    self.fromCalValue = moment(e.date).format('DD-MM-YYYY');
                    self.toCalValue = "";
                    self.textInputActive = "";
                    self.cdRef.detectChanges();
                    $("#toDateCal").datepicker("destroy");
                    $('#toDateCal').val(""); //.datepicker("update");    
                    $('#toDateCal').datepicker({
                        orientation: "bottom left",
                        autoclose: true,
                        format: 'dd M yy',
                        endDate: '0d',
                        startDate: moment(e.date).format('DD-MM-YYYY')
                    })
                        .off('changeDate')
                        .on('changeDate', function (e) {
                        self.toCalValue = moment(e.date).format('DD-MM-YYYY');
                        self.textInputActive = "";
                        self.cdRef.detectChanges();
                    });
                });
                _window().clearInterval(initFromCalendarInterval);
            }
        }, 300);
        this.selectedDropdownOptionSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            // optionKey is accounNumber on dropdown since its unique per account 
            var optionKey = object.optionKey;
            var index = object.index;
            if (index == 1) {
                var downloadTypeSelected = self.downloadTypes.filter(function (x) { return x.Id == optionKey; });
                if (downloadTypeSelected && downloadTypeSelected.length > 0) {
                    switch (downloadTypeSelected[0].Name.toLowerCase()) {
                        case 'pdf':
                            self.downloadPDFStatement();
                            break;
                        case 'csv':
                            self.downloadCSVStatement();
                            break;
                    }
                }
            }
        });
    };
    AccountStatementComponent.prototype.getMoreTransactions = function () {
        var self = this;
        self.showBusyLoader = true;
        self.cdRef.detectChanges();
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_2 = setInterval(function () {
                self.showBusyLoader = false;
                self.cdRef.detectChanges();
                self._getAccountTransactionDetails(dashboard_services_1.DashboardService.CrossStatementAccount.accountNumber);
                _window().clearInterval(loadingModalInterval_2);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getAccountTransactionDetails(dashboard_services_1.DashboardService.CrossStatementAccount.accountNumber);
        }
    };
    AccountStatementComponent.prototype.getStatements = function (_currentNoOfMonthsInStatement) {
        var self = this;
        this.currentNoOfMonthsInStatement = _currentNoOfMonthsInStatement;
        self.showBusyLoader = true;
        // self.dateGroupedTransactions = [];
        self.hideViewMore = false;
        self.cdRef.detectChanges();
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_3 = setInterval(function () {
                self.showBusyLoader = false;
                self.cdRef.detectChanges();
                self._getAccountTransactionDetails(dashboard_services_1.DashboardService.CrossStatementAccount.accountNumber);
                _window().clearInterval(loadingModalInterval_3);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getAccountTransactionDetails(dashboard_services_1.DashboardService.CrossStatementAccount.accountNumber);
        }
    };
    AccountStatementComponent.prototype._getAccountTransactionDetails = function (accountNo) {
        var _this = this;
        var self = this;
        // this.transactionEndDate =  new Date(self.transactionStartDate.toString());
        this.transactionStartDate = new Date(self.transactionEndDate.toString());
        if (this.currentNoOfMonthsInStatement == 3) {
            this.transactionStartDate.setMonth(self.transactionEndDate.getMonth() - this.currentNoOfMonthsInStatement - 3);
        }
        else {
            this.transactionStartDate.setMonth(self.transactionEndDate.getMonth() - this.currentNoOfMonthsInStatement - 1);
        }
        console.log('start date.');
        console.log(self.transactionStartDate.toDateString());
        console.log('end date.');
        console.log(self.transactionEndDate.toDateString());
        self.getCustomerAccountTransactionsSub = self.dashService.getCustomerDatedTransaction(accountNo, moment(self.transactionStartDate).format('YYYY-MM-DD'), moment(this.transactionEndDate).format('YYYY-MM-DD'))
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                var transactions = response.Transactions;
                if (transactions && transactions.length > 0) {
                    //group transactions by date. this will produce an object literal.
                    var dateGroupedTransactions = transactions.reduce(function (r, a) {
                        r[a.date] = r[a.date] || [];
                        r[a.date].push(a);
                        return r;
                    }, Object.create(null));
                    //convert the object literal into a date sorted array of transaction objects
                    var dateSortedTransactionArray = Object.values(dateGroupedTransactions).sort(function (a, b) {
                        if (new Date(a[0].date) < new Date(b[0].date))
                            return 1;
                        if (new Date(a[0].date) > new Date(b[0].date))
                            return -1;
                        return 0;
                    });
                    //fill up existing array.
                    self.dateGroupedTransactions = [];
                    self.dateGroupedTransactions.push.apply(self.dateGroupedTransactions, dateSortedTransactionArray);
                    console.log('Transactions');
                    console.log(self.dateGroupedTransactions);
                    self.currentNoOfMonthsInStatement += 1;
                    if (self.currentNoOfMonthsInStatement > 3) {
                        self.hideViewMore = true;
                    }
                    switch (self.currentNoOfMonthsInStatement) {
                        case 1:
                            self.statementFilterPlaceholder = 'Search in last 30 days transactions';
                            break;
                        case 2:
                            self.statementFilterPlaceholder = 'Search in last 60 days transactions';
                            break;
                        case 3:
                            self.statementFilterPlaceholder = 'Search in last 90 days transactions';
                            break;
                        case 4:
                            self.statementFilterPlaceholder = 'Search in last 180 days transactions';
                            break;
                    }
                }
                else {
                    _this.hideViewMore = true;
                }
                self.showBusyLoader = false;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            // self.showBusyLoader = false;
            //   self.showTransactionLoading = false; 
            self.cdRef.detectChanges();
        }, function () {
            // self.showBusyLoader = false; 
            // self.showTransactionLoading = false;  
            //  self.cdRef.detectChanges();
        });
    };
    AccountStatementComponent.prototype.toggleAccountDetails = function () {
        this.showDetailOverlay = !this.showDetailOverlay;
        if (this.showServiceOverlay) {
            this.showServiceOverlay = false;
        }
    };
    AccountStatementComponent.prototype.toggleServiceDetails = function () {
        this.showServiceOverlay = !this.showServiceOverlay;
        if (this.showDetailOverlay) {
            this.showDetailOverlay = false;
        }
    };
    AccountStatementComponent.prototype.toggleFilteringOption2 = function () {
        this.filterActive = !this.filterActive;
    };
    AccountStatementComponent.prototype.hangModalFilter = function () {
        this.hangFilterModal = this.animateModalUp = this.filterActive = true;
    };
    AccountStatementComponent.prototype.closeFilter = function () {
        this.filterActive = this.hangFilterModal = this.animateModalUp = false;
    };
    AccountStatementComponent.prototype.toggleMenu = function () {
        this.burgerMenuIsActive = !this.burgerMenuIsActive;
    };
    AccountStatementComponent.prototype.generatePrintData = function () {
        var dataRows = [];
        var self = this;
        var counter = 1;
        for (var cursor = 0; cursor < self.dateGroupedTransactions.length; cursor++) {
            for (var _cursor = 0; _cursor < self.dateGroupedTransactions[cursor].length; _cursor++) {
                var transToBePrinted = [];
                //insert s/n
                transToBePrinted.push(counter);
                //insert date               
                try {
                    if (self.dateGroupedTransactions[cursor][_cursor].date) {
                        transToBePrinted.push(moment(self.dateGroupedTransactions[cursor][_cursor].date).format('DD/MM/YYYY'));
                    }
                }
                catch () {
                    transToBePrinted.push("");
                }
                //insert narration
                transToBePrinted.push(self.dateGroupedTransactions[cursor][_cursor].description);
                // insert amount if tran type is debit
                if (self.dateGroupedTransactions[cursor][_cursor].transactionType.toLowerCase() == 'd') {
                    try {
                        if (self.dateGroupedTransactions[cursor][_cursor].amount) {
                            transToBePrinted.push((self.decimalPipe.transform(self.dateGroupedTransactions[cursor][_cursor].amount, '.2')).toString());
                        }
                    }
                    catch () {
                        transToBePrinted.push("");
                    }
                }
                else {
                    transToBePrinted.push("");
                }
                // insert amount if tran type is credit
                if (self.dateGroupedTransactions[cursor][_cursor].transactionType.toLowerCase() == 'c') {
                    try {
                        if (self.dateGroupedTransactions[cursor][_cursor].amount) {
                            transToBePrinted.push((self.decimalPipe.transform(self.dateGroupedTransactions[cursor][_cursor].amount, '.2')).toString());
                        }
                    }
                    catch () {
                        transToBePrinted.push("");
                    }
                }
                else {
                    transToBePrinted.push("");
                }
                // insert balance
                try {
                    if (self.dateGroupedTransactions[cursor][_cursor].balance) {
                        transToBePrinted.push((self.decimalPipe.transform(self.dateGroupedTransactions[cursor][_cursor].balance, '.2')).toString());
                    }
                }
                catch () {
                    transToBePrinted.push("");
                }
                dataRows.push(transToBePrinted);
                counter += 1;
            }
        }
        return dataRows;
    };
    AccountStatementComponent.prototype.downloadPDFStatement = function () {
        var rows = this.generatePrintData();
        try {
            var pdfDoc = this.dashService.generateStatement(appsettings_base64_constant_1.AppsettingsBase64.STANBIC_PDF_HEADER_LOGO, appsettings_base64_constant_1.AppsettingsBase64.STANBIC_PDF_WATERMARK, appsettings_base64_constant_1.AppsettingsBase64.STANBIC_PDF_DISCLAIMER, dashboard_services_1.DashboardService.CrossStatementAccount.accountNumber, moment(new Date()).format('hh:mm:ss A, DD/MM/YYYY'), dashboard_services_1.DashboardService.CrossStatementAccount.accountName, moment(this.transactionStartDate).format('DD/MM/YYYY'), moment(this.transactionEndDate).format('DD/MM/YYYY'), (this.decimalPipe.transform(dashboard_services_1.DashboardService.CrossStatementAccount.availableBalance, '.2')).toString(), (this.decimalPipe.transform(dashboard_services_1.DashboardService.CrossStatementAccount.availableBalance, '.2')).toString(), dashboard_services_1.DashboardService.CrossStatementAccount.currency, ["", "Date", "Description", "Debit", "Credit", "Balance"], rows);
            var browserInfo = utils_services_1.UtilService.getBrowserObj();
            pdfDoc.save('account_statement.pdf');
        }
        catch (Exce) {
        }
    };
    AccountStatementComponent.prototype.downloadCSVStatement = function () {
        if (this.dateGroupedTransactions.length > 0) {
            var dataRows = [];
            var self_1 = this;
            var counter = 1;
            for (var cursor = 0; cursor < self_1.dateGroupedTransactions.length; cursor++) {
                for (var _cursor = 0; _cursor < self_1.dateGroupedTransactions[cursor].length; _cursor++) {
                    var tempRow = {
                        "S/N": counter
                    };
                    //insert date               
                    try {
                        if (self_1.dateGroupedTransactions[cursor][_cursor].date) {
                            tempRow['Date'] = moment(self_1.dateGroupedTransactions[cursor][_cursor].date).format('DD/MM/YYYY');
                        }
                    }
                    catch () {
                        tempRow['Date'] = "";
                    }
                    //insert narration
                    tempRow['Description'] = self_1.dateGroupedTransactions[cursor][_cursor].description;
                    // insert amount if tran type is debit
                    if (self_1.dateGroupedTransactions[cursor][_cursor].transactionType.toLowerCase() == 'd') {
                        tempRow['Debit'] = self_1.dateGroupedTransactions[cursor][_cursor].amount;
                    }
                    else {
                        tempRow['Debit'] = "";
                    }
                    // insert amount if tran type is credit
                    if (self_1.dateGroupedTransactions[cursor][_cursor].transactionType.toLowerCase() == 'c') {
                        tempRow['Credit'] = self_1.dateGroupedTransactions[cursor][_cursor].amount;
                    }
                    else {
                        tempRow['Credit'] = "";
                    }
                    // insert balance
                    try {
                        if (self_1.dateGroupedTransactions[cursor][_cursor].balance) {
                            tempRow['Balance'] = self_1.dateGroupedTransactions[cursor][_cursor].balance;
                        }
                    }
                    catch () {
                        tempRow['Balance'] = "";
                    }
                    dataRows.push(tempRow);
                    counter += 1;
                }
            }
            var myJsonString = JSON.stringify(dataRows);
            this.dashService.JSONToCSVConvertor(myJsonString, true, "account_statement");
        }
    };
    AccountStatementComponent.prototype.generatePrint = function (event) {
        event.stopPropagation();
        var rows = this.generatePrintData();
        try {
            var pdfDoc = this.dashService.generateStatement(appsettings_base64_constant_1.AppsettingsBase64.STANBIC_PDF_HEADER_LOGO, appsettings_base64_constant_1.AppsettingsBase64.STANBIC_PDF_WATERMARK, appsettings_base64_constant_1.AppsettingsBase64.STANBIC_PDF_DISCLAIMER, dashboard_services_1.DashboardService.CrossStatementAccount.accountNumber, moment(new Date()).format('hh:mm:ss A, DD/MM/YYYY'), dashboard_services_1.DashboardService.CrossStatementAccount.accountName, moment(this.transactionStartDate).format('DD/MM/YYYY'), moment(this.transactionEndDate).format('DD/MM/YYYY'), (this.decimalPipe.transform(dashboard_services_1.DashboardService.CrossStatementAccount.availableBalance, '.2')).toString(), (this.decimalPipe.transform(dashboard_services_1.DashboardService.CrossStatementAccount.availableBalance, '.2')).toString(), dashboard_services_1.DashboardService.CrossStatementAccount.currency, ["", "Date", "Description", "Debit", "Credit", "Balance"], rows);
            pdfDoc.autoPrint();
            var browserInfo = utils_services_1.UtilService.getBrowserObj();
            //IE, Object { name: "Firefox", version: "45" }  {name: "Chrome", version: "65"} {name: "Edge", version: "14"}
            if (browserInfo && browserInfo.name && browserInfo.name.toLowerCase() == 'ie') {
                pdfDoc.save('account_statement.pdf');
            }
            else if (browserInfo && browserInfo.name && browserInfo.name.toLowerCase() != 'ie') {
                var blobUrl = pdfDoc.output('bloburl');
                $('#printOutput').attr('src', blobUrl);
                _window().frames["printOutput"].focus();
            }
            else {
                pdfDoc.save('account_statement.pdf');
            }
        }
        catch (Exce) {
            console.log("Exception generating staement.");
            console.log(Exce);
        }
        return false;
    };
    AccountStatementComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.cdRef.detectChanges();
    };
    AccountStatementComponent.prototype.textInputBlur = function (formControlName) {
        // if (formControlName.toLowerCase() == 'fromCalValue' && !this.fromCalValue) {
        //     this.textInputActive = ""; 
        // }
        // else if (formControlName.toLowerCase() == 'toCalValue' && !this.toCalValue) {
        //     this.textInputActive = "";
        // }
        this.textInputActive = "";
        this.cdRef.detectChanges();
    };
    AccountStatementComponent.prototype.gotoTxnReceipt = function () {
        this.router.navigate(['manage-receipts']);
        return false;
    };
    AccountStatementComponent.prototype.gotoServiceRequest = function () {
        this.router.navigate(['service-request']);
        return false;
    };
    AccountStatementComponent.prototype.goToProfilesAndSettings = function () {
        this.router.navigate(['profile-and-settings']);
        return false;
    };
    AccountStatementComponent.prototype.goToScheduteTxn = function () {
        this.router.navigate(['manage-scheduled-receipts']);
        return false;
    };
    AccountStatementComponent.prototype.gotoPayBeneficiary = function () {
        this.router.navigate(['beneficiaries']);
        return false;
    };
    AccountStatementComponent.prototype.gotoBuy = function () {
        this.router.navigate(['buy-airtime']);
        return false;
    };
    AccountStatementComponent.prototype.gotoRedeem = function () {
        this.router.navigate(['redeem-western-union']);
        return false;
    };
    AccountStatementComponent.prototype.goHome = function () {
        this.router.navigate(['dashboard']);
        return false;
    };
    AccountStatementComponent.prototype.ngOnDestroy = function () {
        if (this.selectedDropdownOptionSub) {
            this.selectedDropdownOptionSub.unsubscribe();
        }
    };
    AccountStatementComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    AccountStatementComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    AccountStatementComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/statement.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef, common_1.DecimalPipe])
    ], AccountStatementComponent);
    return AccountStatementComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.AccountStatementComponent = AccountStatementComponent;
// LOGIN.
// SET TEMPORARY TOKEN
//  
//# sourceMappingURL=statement.component.js.map