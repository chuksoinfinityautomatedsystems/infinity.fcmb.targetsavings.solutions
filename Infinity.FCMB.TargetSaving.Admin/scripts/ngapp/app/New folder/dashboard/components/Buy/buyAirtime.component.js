"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../services/dashboard.services");
var utils_services_1 = require("../../../commons/services/utils.services");
var constants_services_1 = require("../../../commons/services/constants.services");
var authentication_service_1 = require("../../../authentication/services/authentication.service");
var appsettings_constant_1 = require("../../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../../commons/constants/response_codes.constant");
var base_component_1 = require("../../../commons/components/base.component");
var common_1 = require('@angular/common');
var event_handlers_service_1 = require("../../../commons/services/event_handlers.service");
function _window() {
    // return the global native browser window object
    return window;
}
var BuyAirtimeComponent = (function (_super) {
    __extends(BuyAirtimeComponent, _super);
    function BuyAirtimeComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef, decimalPipe) {
        _super.call(this, authService, router, formBuilder, cdRef, appsettings_constant_1.Appsettings.OTP_REASON_CODE_PURCHASE_AIRTIME);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        this.decimalPipe = decimalPipe;
        document.title = "STANBIC IBTC BANK";
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.cancelModalOpened = false;
        this.buyAirtimeFormGroup = formBuilder.group({
            mobileNo: ['', forms_1.Validators.required],
            amount: [''],
            operatorCode: ['', forms_1.Validators.required],
            operatorName: ['', forms_1.Validators.required],
            productType: ['', forms_1.Validators.required]
        });
        this.cancelModalOpened = this.formSubmitted = false;
        this.textInputActive = '';
        this.textInputActive = '';
        this.withinDailyLimit = true;
        this.totalAmountOnView = 0;
        this.amountErrorOnView = false;
        this.insufficientFund = false;
        this.amountUtilizedFromLimit = 0;
        this.availableLimit = 1000000;
    }
    BuyAirtimeComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        if (!dashboard_services_1.DashboardService.CrossPageCustomerAccountList ||
            (dashboard_services_1.DashboardService.CrossPageCustomerAccountList && dashboard_services_1.DashboardService.CrossPageCustomerAccountList.length < 1)) {
            this.router.navigate(['Dashboard']);
            return;
        }
        this.debitableNGNAccounts = dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.debitAllowed == true && x.currency.toLowerCase() == 'ngn'; });
        self.showBusyLoader = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                self._getServiceProviderAndProductList();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getServiceProviderAndProductList();
        }
        this.selectedDropdownOptionSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            // optionKey is accounNumber on dropdown since its unique per account 
            var optionKey = object.optionKey;
            var index = object.index;
            if (index == 1) {
                var selectedAccount = self.debitableNGNAccounts.filter(function (a) { return a.accountNumber == optionKey; })[0];
                self.availableBalance = selectedAccount.availableBalance;
                self.selectedAccountNo = selectedAccount.accountNumber;
                self.selectedAccountName = selectedAccount.accountName;
                // self.payBillerFormGroup.controls['amount'].value;
                self.validateAmountAgainstAvailableBalance(self.buyAirtimeFormGroup.controls['amount'].value);
                if (_this.buyAirtimeFormGroup.controls["amount"]) {
                    var amountEntered = '';
                    amountEntered = (_this.buyAirtimeFormGroup.controls["amount"].value).toString();
                    //remove .00 if exist
                    if (amountEntered.indexOf('.00') != -1) {
                        amountEntered = amountEntered.replace('.00', '');
                    }
                    if (amountEntered && /\D/g.test(amountEntered)) {
                        // Filter non-digits from input value.
                        amountEntered = amountEntered.replace(/\D/g, '');
                    }
                    var amount = parseFloat(amountEntered);
                    _this.validateAmountAgainstAvailableBalance(amount);
                }
            }
            else if (index == 2) {
                self.buyAirtimeFormGroup.controls['operatorCode'].setValue(undefined);
                self.buyAirtimeFormGroup.controls['operatorName'].setValue(undefined);
                var selectedOperator = self.operators.filter(function (c) { return c.operatorId == parseInt(optionKey); });
                if (selectedOperator && selectedOperator[0]) {
                    self.buyAirtimeFormGroup.controls['operatorCode'].setValue(selectedOperator[0].operatorId);
                    self.buyAirtimeFormGroup.controls['operatorName'].setValue(selectedOperator[0].operatorName);
                }
            }
            else if (index == 3) {
                self.selectedProductType = undefined;
                self.buyAirtimeFormGroup.controls['amount'].setValue(undefined);
                self.buyAirtimeFormGroup.controls['productType'].setValue(undefined);
                var selectedProductType = self.operatorProducts.filter(function (c) { return c.productId == parseInt(optionKey); });
                if (selectedProductType && selectedProductType.length > 0) {
                    self.selectedProductType = selectedProductType[0];
                    self.invalidProductType = false;
                    self.buyAirtimeFormGroup.controls['productType'].setValue(self.selectedProductType.productName);
                }
                if (self.selectedProductType && self.selectedProductType.isAmountFixed.toLowerCase() == 'y') {
                    self.buyAirtimeFormGroup.controls['amount'].setValue(self.selectedProductType.amount.toString());
                    self.validateAmountAgainstAvailableBalance(self.selectedProductType.amount);
                }
                else {
                    self.validateAmountAgainstAvailableBalance(0);
                }
            }
        });
    };
    BuyAirtimeComponent.prototype._getServiceProviderAndProductList = function () {
        var self = this;
        self.getServiceProvidersSub = self.dashService.getServiceProviders(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.operators = response.Operators;
                if (!dashboard_services_1.DashboardService.CrossPageOperatorProductList) {
                    self._getOperatorProducts();
                }
                else {
                    self.operatorProducts = dashboard_services_1.DashboardService.CrossPageOperatorProductList;
                }
            }
        }, function (error) {
            self.showBusyLoader = false;
            self.showExceptionPage = true;
            self.cdRef.detectChanges();
        }, function () {
            // self.showBusyLoader = false;   
            // self.cdRef.detectChanges();
        });
    };
    BuyAirtimeComponent.prototype._getOperatorProducts = function () {
        var self = this;
        self.getServiceProviderProductsSub = self.dashService.getOperatorProductList(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                dashboard_services_1.DashboardService.CrossPageOperatorProductList = self.operatorProducts = new Array();
                dashboard_services_1.DashboardService.CrossPageOperatorProductList = self.operatorProducts = response.Products;
            }
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.showExceptionPage = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    BuyAirtimeComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.formSubmitted = false;
        if (formControlName == "amount") {
            //this.amountRequiredError = false;
            this.showTransactionLimit = true;
            this.withinDailyLimit = true;
            this.insufficientFund = false;
            //   this.firstKeyUpPassedAfterAmountFocus = false;
            if (this.buyAirtimeFormGroup.controls["amount"]) {
                var amountEntered = this.buyAirtimeFormGroup.controls["amount"].value;
                if (amountEntered) {
                    //remove .00 if exist
                    if (amountEntered.indexOf('.00') != -1) {
                        amountEntered = amountEntered.replace('.00', '');
                    }
                    // Filter non-digits from input value.
                    if (/\D/g.test(amountEntered)) {
                        amountEntered = amountEntered.replace(/\D/g, '');
                    }
                }
                this.buyAirtimeFormGroup.controls["amount"].setValue(amountEntered);
            }
        }
        this.cdRef.detectChanges();
    };
    BuyAirtimeComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.buyAirtimeFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
        if (formControlName == "amount") {
            this.showTransactionLimit = false;
            var transferCharges = 0;
            var amount = 0;
            if (this.buyAirtimeFormGroup.controls['' + formControlName]) {
                amount = this.buyAirtimeFormGroup.controls['' + formControlName].value;
            }
            if (amount > 0) {
                this.buyAirtimeFormGroup.controls['' + formControlName].setValue(this.decimalPipe.transform(amount, '.2'));
            }
            this.cdRef.detectChanges();
        }
    };
    BuyAirtimeComponent.prototype.amountInputKeyup = function () {
        var amount = 0.0;
        // Only allow digits on amount alone.
        if (this.buyAirtimeFormGroup.controls["amount"]) {
            var amountEntered = this.buyAirtimeFormGroup.controls["amount"].value;
            if (amountEntered && /\D/g.test(amountEntered)) {
                // Filter non-digits from input value.
                amountEntered = amountEntered.replace(/\D/g, '');
            }
            this.buyAirtimeFormGroup.controls["amount"].setValue(amountEntered);
            amount = parseFloat(amountEntered);
        }
        this.validateAmountAgainstAvailableBalance(amount);
    };
    BuyAirtimeComponent.prototype.mobileNoInputKeyup = function () {
        if (this.buyAirtimeFormGroup.controls["mobileNo"]) {
            var numberEntered = this.buyAirtimeFormGroup.controls["mobileNo"].value;
            if (numberEntered && /\D/g.test(numberEntered)) {
                // Filter non-digits from input value.
                numberEntered = numberEntered.replace(/\D/g, '');
            }
            this.buyAirtimeFormGroup.controls["mobileNo"].setValue(numberEntered);
        }
    };
    BuyAirtimeComponent.prototype.validateAmountAgainstAvailableBalance = function (amount) {
        this.totalAmountOnView = amount;
        if (amount > this.availableBalance) {
            this.insufficientFund = true;
            this.amountErrorOnView = true;
        }
        else {
            this.insufficientFund = false;
            this.amountErrorOnView = false;
        }
        if (amount > (+this.availableLimit - +this.amountUtilizedFromLimit)) {
            this.withinDailyLimit = false;
            if (!this.amountErrorOnView) {
                this.amountErrorOnView = true;
            }
        }
        else {
            this.withinDailyLimit = true;
        }
    };
    BuyAirtimeComponent.prototype.backDetailsForm = function (event) {
        event.stopPropagation();
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.cdRef.detectChanges();
        return false;
    };
    BuyAirtimeComponent.prototype.validateOTP = function (event) {
        event.stopPropagation();
        // validate otp and make payment
        this.showOtpView = false;
        this.pageIndex = 3;
        this.closeOtpView();
        this.pageSubtitle = "Receipt";
        this.cdRef.detectChanges();
        return false;
    };
    BuyAirtimeComponent.prototype.nextToReview = function (event) {
        event.stopPropagation();
        var self = this;
        this.formSubmitted = true;
        this.invalidProductType = false;
        if (this.buyAirtimeFormGroup.valid) {
            if (!this.selectedProductType) {
                this.invalidProductType = true;
            }
            if (!this.invalidProductType && !this.insufficientFund && this.withinDailyLimit && this.totalAmountOnView > 0) {
                this.pageIndex = 2;
                this.pageSubtitle = "Review";
            }
        }
        self.cdRef.detectChanges();
        return false;
    };
    BuyAirtimeComponent.prototype.BuyAgain = function (event) {
        // clear all form fields
        event.stopPropagation();
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.formSubmitted = false;
        this.textInputActive = '';
        this.buyAirtimeFormGroup.controls['mobileNo'].setValue(undefined);
        this.buyAirtimeFormGroup.controls['amount'].setValue(undefined);
        this.buyAirtimeFormGroup.controls['operatorCode'].setValue(undefined);
        this.buyAirtimeFormGroup.controls['operatorName'].setValue(undefined);
        this.buyAirtimeFormGroup.controls['productType'].setValue(undefined);
        event_handlers_service_1.EventHandlerService.emitUnselectDropdownEvent(2); // 2== index for operator dropdown
        event_handlers_service_1.EventHandlerService.emitUnselectDropdownEvent(3); // 3== index for product dropdown
        this.cdRef.detectChanges();
        this.validateAmountAgainstAvailableBalance(0);
        return false;
    };
    BuyAirtimeComponent.prototype.buyAirtime = function (event) {
        event.stopPropagation();
        this.pageIndex = 3;
        this.pageSubtitle = "Receipt";
        return false;
    };
    BuyAirtimeComponent.prototype.hideCancelTransferModal = function () {
        this.cancelModalOpened = false;
    };
    BuyAirtimeComponent.prototype.CloseTranfer = function () {
        this.cancelModalOpened = true;
    };
    BuyAirtimeComponent.prototype.backHome = function () {
        this.router.navigate(['dashboard']);
    };
    BuyAirtimeComponent.prototype.ngOnDestroy = function () {
        if (this.getServiceProviderProductsSub) {
            this.getServiceProviderProductsSub.unsubscribe();
        }
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
        if (this.getServiceProvidersSub) {
            this.getServiceProvidersSub.unsubscribe();
        }
    };
    BuyAirtimeComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    BuyAirtimeComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    BuyAirtimeComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/buy/airtime.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef, common_1.DecimalPipe])
    ], BuyAirtimeComponent);
    return BuyAirtimeComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.BuyAirtimeComponent = BuyAirtimeComponent;
//# sourceMappingURL=buyAirtime.component.js.map