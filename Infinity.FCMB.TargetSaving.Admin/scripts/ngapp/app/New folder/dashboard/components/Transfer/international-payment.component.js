"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../services/dashboard.services");
var utils_services_1 = require("../../../commons/services/utils.services");
var constants_services_1 = require("../../../commons/services/constants.services");
var authentication_service_1 = require("../../../authentication/services/authentication.service");
var common_1 = require('@angular/common');
var event_handlers_service_1 = require("../../../commons/services/event_handlers.service");
var appsettings_constant_1 = require("../../../commons/constants/appsettings.constant");
var schedule_payment_base_component_1 = require("../../../commons/components/schedule_payment.base.component");
// interface IBeneficiaryAmountHash{
//     beneficiaryAccountNo: string;
//     amount: number;
//     charges: number;
// }
function _window() {
    // return the global native browser window object
    return window;
}
var InternationalPaymentComponent = (function (_super) {
    __extends(InternationalPaymentComponent, _super);
    function InternationalPaymentComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef, decimalPipe) {
        _super.call(this, authService, router, cdRef);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        this.decimalPipe = decimalPipe;
        document.title = "STANBIC IBTC BANK";
        this._formGroup = formBuilder.group({
            recipientName: ['', forms_1.Validators.required],
            recipientAddress: ['', forms_1.Validators.required],
            recipientBankAddress: ['', forms_1.Validators.required],
            recipientAccountNumber: ['', forms_1.Validators.required],
            recipientBankName: ['', forms_1.Validators.required],
            amount: ['', forms_1.Validators.required],
            additionalChargesPayer: ['0'],
            recipientBankSortCode: ['', forms_1.Validators.required],
            recipientIban: [''],
            recipientSwiftCode: [''],
            useIntermediary: [false],
            intermediaryBankName: [''],
            intermediaryBankAccountNumber: [''],
            intermediarySwiftCode: [''],
            intermediaryBankAddress: [''],
            myRef: ['', forms_1.Validators.required],
            paymentPupose: ['', forms_1.Validators.required],
            email: ['', forms_1.Validators.required],
            mobileNumber: [''],
            tranDate: [''],
            numberofScheduleFrequency: [''],
            paymentDate: ['']
        });
        this.showToolTipOne = this.showToolTipTwo = this.showToolTipThree = this.showIntermediarySection = this.amountFieldOnFocus = false;
        this.availableBalance = 100000;
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.cancelModalOpened = false;
    }
    InternationalPaymentComponent.prototype.ngOnInit = function () {
        var self = this;
        // Today, 28 Mar 2018
        this._formGroup.controls["tranDate"].setValue("Today, " + moment(new Date()).format('DD MMM YYYY'));
        //  this.filteredCountryCodes = this.dashService.getCountryCodes();  
        //  this.filteredBanks = this.dashService.getBanks();
        // this.filteredCountryCodes = this.sortedCountryCodes();
        //  this.filteredBanks = this.sortedBanks();
        // if (DashboardService.CrossBeneficiaryDetailsObject) {
        //     if (DashboardService.CrossBeneficiaryDetailsObject.beneficiaryAlias) {
        //         this._formGroup.controls["beneficiaryName"].setValue(DashboardService.CrossBeneficiaryDetailsObject.beneficiaryAlias);                 
        //     }
        //     if (DashboardService.CrossBeneficiaryDetailsObject.beneficiaryAccountNumber) {
        //         this._formGroup.controls["accountNo"].setValue(DashboardService.CrossBeneficiaryDetailsObject.beneficiaryAccountNumber);                 
        //     }
        //     if (DashboardService.CrossBeneficiaryDetailsObject.customerReference) {
        //         this._formGroup.controls["myRef"].setValue(DashboardService.CrossBeneficiaryDetailsObject.customerReference);                 
        //     }
        //     if (DashboardService.CrossBeneficiaryDetailsObject.beneficiaryReference) {
        //         this._formGroup.controls["theirRef"].setValue(DashboardService.CrossBeneficiaryDetailsObject.beneficiaryReference);                 
        //     }
        // } 
        this.myDebitableForeignAccounts = dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.debitAllowed == true &&
            x.currency.toLowerCase() != appsettings_constant_1.Appsettings.LOCAL_CURRENCY.toLowerCase(); });
        this.selectedAccountOptionSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            // optionKey is accounNumber on dropdown since its unique per account 
            var optionKey = object.optionKey;
            var index = object.index;
            if (index == 1) {
                var selectedAccount = self.myDebitableForeignAccounts.filter(function (a) { return a.accountNumber == optionKey; })[0];
                self.availableBalance = selectedAccount.availableBalance;
                self.transactionCurrency = selectedAccount.currency.toUpperCase();
                self.selectedAccountNo = selectedAccount.accountNumber;
                self.selectedAccountName = selectedAccount.accountName;
                self.validateAmountAgainstAvailableBalance(self._formGroup.controls['amount'].value);
            }
        });
    };
    InternationalPaymentComponent.prototype.validateAmountAgainstAvailableBalance = function (amount) {
        if (amount) {
            this.totalAmountLessChargeForScheduleView = amount;
        }
        if (amount > this.availableBalance) {
            this.insufficientFund = true;
        }
        else {
            this.insufficientFund = false;
        }
    };
    InternationalPaymentComponent.prototype.toggleToolTip1 = function () {
        this.showToolTipOne = !this.showToolTipOne;
        this.cdRef.detectChanges();
    };
    InternationalPaymentComponent.prototype.toggleToolTip2 = function () {
        this.showToolTipTwo = !this.showToolTipTwo;
        this.cdRef.detectChanges();
    };
    InternationalPaymentComponent.prototype.toggleToolTip3 = function () {
        this.showToolTipThree = !this.showToolTipThree;
        this.cdRef.detectChanges();
    };
    InternationalPaymentComponent.prototype.toggleIntermediaryView = function (value) {
        this.showIntermediarySection = value;
        if (!this.showIntermediarySection) {
            this._formGroup.controls["intermediaryBankName"].setValue(undefined);
            this._formGroup.controls["intermediaryBankAccountNumber"].setValue(undefined);
            this._formGroup.controls["intermediarySwiftCode"].setValue(undefined);
            this._formGroup.controls["intermediaryBankAddress"].setValue(undefined);
            this.textInputActive = "";
        }
        this.cdRef.detectChanges();
    };
    InternationalPaymentComponent.prototype.nextToReview = function (event) {
        event.stopPropagation();
        var self = this;
        this.formSubmitted = true;
        if (self._formGroup.valid) {
            if (!this.insufficientFund) {
                this.pageSubtitle = "Review";
                this.pageIndex = 2;
            }
            self.cdRef.detectChanges();
        }
        return false;
    };
    InternationalPaymentComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.formSubmitted = false;
        if (formControlName == "amount") {
            this.amountFieldOnFocus = true;
            this.insufficientFund = false;
            if (this._formGroup.controls["amount"]) {
                var amountEntered = this._formGroup.controls["amount"].value;
                if (amountEntered) {
                    //remove .00 if exist
                    if (amountEntered.indexOf('.00') != -1) {
                        amountEntered = amountEntered.replace('.00', '');
                    }
                    // Filter non-digits from input value.
                    if (/\D/g.test(amountEntered)) {
                        amountEntered = amountEntered.replace(/\D/g, '');
                    }
                }
                this._formGroup.controls["amount"].setValue(amountEntered);
            }
        }
        this.cdRef.detectChanges();
    };
    InternationalPaymentComponent.prototype.textInputBlur = function (formControlName) {
        if (!this._formGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
        this.amountFieldOnFocus = false;
        if (formControlName == "amount") {
            var amount = 0;
            if (this._formGroup.controls['' + formControlName]) {
                amount = this._formGroup.controls['' + formControlName].value;
            }
            this.cdRef.detectChanges();
            if (amount > 0) {
                this._formGroup.controls['' + formControlName].setValue(this.decimalPipe.transform(amount, '.2'));
            }
        }
    };
    InternationalPaymentComponent.prototype.amountInputKeyup = function () {
        var amount = 0.0;
        // Only allow digits on amount alone.        
        if (this._formGroup.controls["amount"]) {
            var amountEntered = this._formGroup.controls["amount"].value;
            if (amountEntered && /\D/g.test(amountEntered)) {
                // Filter non-digits from input value.
                amountEntered = amountEntered.replace(/\D/g, '');
            }
            this._formGroup.controls["amount"].setValue(amountEntered);
            amount = parseFloat(amountEntered);
        }
        if (amount > this.availableBalance) {
            this.insufficientFund = true;
        }
        else {
            this.insufficientFund = false;
        }
    };
    InternationalPaymentComponent.prototype.preventNonDigitOnAccountNo = function () {
        // Only allow digits on amount alone.        
        if (this._formGroup.controls["recipientAccountNumber"]) {
            var accountNoEntered = this._formGroup.controls["recipientAccountNumber"].value;
            if (accountNoEntered && /\D/g.test(accountNoEntered)) {
                // Filter non-digits from input value.
                accountNoEntered = accountNoEntered.replace(/\D/g, '');
            }
            this._formGroup.controls["recipientAccountNumber"].setValue(accountNoEntered);
        }
    };
    InternationalPaymentComponent.prototype.preventNonDigitOnIntermediaryAccountNo = function () {
        // Only allow digits on amount alone.        
        if (this._formGroup.controls["intermediaryBankAccountNumber"]) {
            var accountNoEntered = this._formGroup.controls["intermediaryBankAccountNumber"].value;
            if (accountNoEntered && /\D/g.test(accountNoEntered)) {
                // Filter non-digits from input value.
                accountNoEntered = accountNoEntered.replace(/\D/g, '');
            }
            this._formGroup.controls["intermediaryBankAccountNumber"].setValue(accountNoEntered);
        }
    };
    InternationalPaymentComponent.prototype.preventNonDigitOnMobileNo = function () {
        // Only allow digits on amount alone.        
        if (this._formGroup.controls["mobileNumber"]) {
            var mobileNoEntered = this._formGroup.controls["mobileNumber"].value;
            if (mobileNoEntered && /\D/g.test(mobileNoEntered)) {
                // Filter non-digits from input value.
                mobileNoEntered = mobileNoEntered.replace(/\D/g, '');
            }
            this._formGroup.controls["mobileNumber"].setValue(mobileNoEntered);
        }
    };
    InternationalPaymentComponent.prototype.backToDetails = function (event) {
        event.stopPropagation();
        this.pageSubtitle = "Details";
        this.pageIndex = 1;
        return false;
    };
    // FLOW NAVIGATION
    InternationalPaymentComponent.prototype.pay = function (event) {
        event.stopPropagation();
        this.pageSubtitle = "Receipt";
        this.pageIndex = 3;
        return false;
    };
    InternationalPaymentComponent.prototype.hideCancelTransferModal = function () {
        this.cancelModalOpened = false;
    };
    InternationalPaymentComponent.prototype.CloseTranfer = function () {
        this.cancelModalOpened = true;
    };
    InternationalPaymentComponent.prototype.backHome = function () {
        //this.ngOnDestroy();
        this.router.navigate(['dashboard']);
    };
    InternationalPaymentComponent.prototype.backToBeneficiaryList = function () {
        //this.ngOnDestroy();
        if (dashboard_services_1.DashboardService.BeneficiaryOrBillerDetailsToPaymentPage == true) {
            this.router.navigate(['MainView/PayTransfer/Recipient-details']);
        }
        else {
            this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
        }
        return false;
    };
    InternationalPaymentComponent.prototype.closeExceptionModal = function () {
        this.exceptionMessage = '';
        this.showExceptionPage = false;
    };
    InternationalPaymentComponent.prototype.ngOnDestroy = function () {
        if (this.selectedAccountOptionSub) {
            this.selectedAccountOptionSub.unsubscribe();
        }
    };
    InternationalPaymentComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    InternationalPaymentComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/international-payment.modal.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef, common_1.DecimalPipe])
    ], InternationalPaymentComponent);
    return InternationalPaymentComponent;
    var _a, _b, _c, _d;
}(schedule_payment_base_component_1.SchedulePaymentBaseComponent));
exports.InternationalPaymentComponent = InternationalPaymentComponent;
//# sourceMappingURL=international-payment.component.js.map