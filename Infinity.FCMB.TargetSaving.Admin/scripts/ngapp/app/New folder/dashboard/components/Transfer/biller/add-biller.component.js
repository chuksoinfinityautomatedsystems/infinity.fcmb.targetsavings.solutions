"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../../services/dashboard.services");
var utils_services_1 = require("../../../../commons/services/utils.services");
var constants_services_1 = require("../../../../commons/services/constants.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var base_component_1 = require("../../../../commons/components/base.component");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../../../commons/constants/response_codes.constant");
var event_handlers_service_1 = require("../../../../commons/services/event_handlers.service");
function _window() {
    // return the global native browser window object
    return window;
}
var AddBillerComponent = (function (_super) {
    __extends(AddBillerComponent, _super);
    function AddBillerComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef) {
        _super.call(this, authService, router, formBuilder, cdRef, appsettings_constant_1.Appsettings.OTP_REASON_CODE_ADD_BENEFICIARY);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.pageIndex = 1;
        this.pageSubtitle = "Choose Biller";
        this.newBillerFormGroup = formBuilder.group({
            myReference: ['', forms_1.Validators.required],
            consumerCode: ['', forms_1.Validators.required],
            billerNickname: ['', forms_1.Validators.required]
        });
        this.cancelModalOpened = this.formSubmitted = false;
        this.textInputActive = this.billerFilterQ = '';
    }
    AddBillerComponent.prototype.ngOnInit = function () {
        var self = this;
        self.showBusyLoader = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                self._getBillerAndCategoryList();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getBillerAndCategoryList();
        }
        this.selectedAccountOptionSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            // optionKey is accounNumber on dropdown since its unique per account 
            var optionKey = object.optionKey;
            var index = object.index;
            if (index == 1) {
                if (self.billerCategories.filter(function (a) { return a.categoryId == optionKey; })[0]) {
                    var selectedCategoryId = self.selectedCategoryId = self.billerCategories.filter(function (a) { return a.categoryId == optionKey; })[0].categoryId;
                    if (selectedCategoryId > 0) {
                        self.filteredBillers = self.billers.filter(function (c) { return c.categoryId == selectedCategoryId; });
                    }
                    else {
                        self.filteredBillers = self.billers;
                    }
                }
                else {
                    self.selectedCategoryId = optionKey;
                    self.filteredBillers = self.billers;
                }
            }
        });
    };
    AddBillerComponent.prototype.sortedBillers = function () {
        if (this.filteredBillers) {
            return this.filteredBillers.sort(function (a, b) {
                if (a.billerName < b.billerName)
                    return -1;
                if (a.billerName > b.billerName)
                    return 1;
                return 0;
            });
        }
        return [];
    };
    AddBillerComponent.prototype._getBillerAndCategoryList = function () {
        var self = this;
        self.getBillersSub = self.dashService.getBillers(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.billers = self.filteredBillers = response.Billers;
                if (!dashboard_services_1.DashboardService.CrossPageBillerCategories) {
                    self._getBillerCategories();
                }
                else {
                    self.billerCategories = dashboard_services_1.DashboardService.CrossPageBillerCategories;
                }
            }
        }, function (error) {
            self.showBusyLoader = false;
            self.showExceptionPage = true;
            self.cdRef.detectChanges();
        }, function () {
            // self.showBusyLoader = false;   
            // self.cdRef.detectChanges();
        });
    };
    AddBillerComponent.prototype._getBillerCategories = function () {
        var self = this;
        self.getBillerProductsSub = self.dashService.getBillerCategories(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                dashboard_services_1.DashboardService.CrossPageBillerCategories = self.billerCategories = new Array();
                dashboard_services_1.DashboardService.CrossPageBillerCategories = self.billerCategories = response.BillerCategories;
            }
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.showExceptionPage = true;
            self.cdRef.detectChanges();
        }, function () {
        });
    };
    AddBillerComponent.prototype.backToAddFormView = function (event) {
        event.stopPropagation();
        this.pageIndex = 2;
        this.pageSubtitle = "Details";
        this.cdRef.detectChanges();
    };
    AddBillerComponent.prototype.nextToAddFormView = function (billerId) {
        this.formSubmitted = false;
        this.textInputActive = '';
        this.newBillerFormGroup.controls['consumerCode'].setValue(undefined);
        this.newBillerFormGroup.controls['billerNickname'].setValue(undefined);
        this.newBillerFormGroup.controls['myReference'].setValue(undefined);
        if (this.billers) {
            this.selectedBiller = this.billers.filter(function (c) { return c.billerId == billerId; })[0];
        }
        if (this.selectedBiller) {
            //     if (this.selectedBiller.) {
            //      self.payBillerFormGroup.controls['consumerCode'].setValue(this.thisBiller.consumerCode);                          
            // }
            // if (this.thisBiller.myReference) {
            //      self.payBillerFormGroup.controls['myReference'].setValue(this.thisBiller.myReference);                          
            // } 
            this.pageIndex = 2;
            this.pageSubtitle = "Details";
        }
        this.cdRef.detectChanges();
    };
    AddBillerComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.formSubmitted = false;
        this.cdRef.detectChanges();
    };
    AddBillerComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.newBillerFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
    };
    AddBillerComponent.prototype.nextToReview = function (event) {
        event.stopPropagation();
        var self = this;
        this.formSubmitted = true;
        if (this.newBillerFormGroup.valid) {
            this.pageIndex = 3;
            this.pageSubtitle = "Review";
        }
        // biller nickname, biller name, customer code ref 3
        self.cdRef.detectChanges();
        return false;
    };
    AddBillerComponent.prototype.validateOTP = function (event) {
        event.stopPropagation();
        // validate otp and make payment
        this.router.navigate(['MainView/PayTransfer/Biller/List']);
        return false;
    };
    AddBillerComponent.prototype.backToBillerSelection = function (event) {
        event.stopPropagation();
        this.pageIndex = 1;
        this.pageSubtitle = "Choose Biller";
        return false;
    };
    // backToBillerList(event) {
    //     event.stopPropagation();
    //     this.router.navigate(['billers']);
    //     return false;
    // }
    AddBillerComponent.prototype.filterBillersOnKeyup = function () {
        var self = this;
        if (self.selectedCategoryId > 0) {
            if (self.billerFilterQ) {
                self.filteredBillers = self.billers.filter(function (x) { return x.categoryId == self.selectedCategoryId && x.billerName.toLowerCase().indexOf(self.billerFilterQ.toLowerCase()) != -1; });
            }
            else {
                self.filteredBillers = self.billers.filter(function (c) { return c.categoryId == self.selectedCategoryId; });
            }
        }
        else {
            if (self.billerFilterQ) {
                self.filteredBillers = self.billers.filter(function (x) { return x.billerName.toLowerCase().indexOf(self.billerFilterQ.toLowerCase()) != -1; });
            }
            else {
                self.filteredBillers = self.billers;
            }
        }
    };
    AddBillerComponent.prototype.backHome = function () {
        this.router.navigate(['dashboard']);
    };
    AddBillerComponent.prototype.closeExceptionModal = function () {
        this.showExceptionPage = false;
        this.cdRef.detectChanges();
        this.router.navigate(['dashboard']);
    };
    AddBillerComponent.prototype.hideCancelProcessModal = function () {
        this.cancelModalOpened = false;
    };
    AddBillerComponent.prototype.CloseModalProcess = function () {
        // only show warning modal on other pages except choose biller page.
        if (this.pageIndex > 1) {
            this.cancelModalOpened = true;
        }
        else {
            this.backHome();
        }
    };
    AddBillerComponent.prototype.ngOnDestroy = function () {
        if (this.getBillersSub) {
            this.getBillersSub.unsubscribe();
        }
        if (this.getBillerProductsSub) {
            this.getBillerProductsSub.unsubscribe();
        }
        if (this.selectedAccountOptionSub) {
            this.selectedAccountOptionSub.unsubscribe();
        }
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
    };
    AddBillerComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    AddBillerComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    AddBillerComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/biller/add-biller.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], AddBillerComponent);
    return AddBillerComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.AddBillerComponent = AddBillerComponent;
//# sourceMappingURL=add-biller.component.js.map