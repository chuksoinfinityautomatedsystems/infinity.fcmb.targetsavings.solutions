"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../../services/dashboard.services");
var utils_services_1 = require("../../../../commons/services/utils.services");
var constants_services_1 = require("../../../../commons/services/constants.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../../../commons/constants/response_codes.constant");
var base_component_1 = require("../../../../commons/components/base.component");
function _window() {
    // return the global native browser window object
    return window;
}
var BillersComponent = (function (_super) {
    __extends(BillersComponent, _super);
    function BillersComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef) {
        _super.call(this, authService, router);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.burgerMenuIsActive = false;
        this.screenHeight = utils_services_1.UtilService.getScreenHeight();
        this.hasForeignAccounts = false;
        this.hideMobilesearch = true;
        this.navigateGroups = false;
        this.billerIdSelected = undefined;
        this.payButtonText = this.billerNameFilter = "";
        this.billers = this.filteredBillers = Array();
    }
    BillersComponent.prototype.ngOnInit = function () {
        dashboard_services_1.DashboardService.CrossBillerBeneficiaryObject = undefined;
        var self = this;
        self.showBusyLoader = true;
        var foreignAccounts = dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.debitAllowed == true &&
            x.currency.toLowerCase() != appsettings_constant_1.Appsettings.LOCAL_CURRENCY.toLowerCase(); });
        if (foreignAccounts && foreignAccounts.length > 0) {
            self.hasForeignAccounts = true;
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                self._getBillerAndProductList();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getBillerAndProductList();
        }
    };
    BillersComponent.prototype._getBillerAndProductList = function () {
        var self = this;
        self.getBillersSub = self.dashService.getBillerBeneficiaryList(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.billers = self.filteredBillers = response.Billers;
                self._getBillerProductList();
            }
            // self.cdRef.detectChanges(); 
        }, function (error) {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            // self.showBusyLoader = false;   
            // self.cdRef.detectChanges();
        });
    };
    BillersComponent.prototype._getBillerProductList = function () {
        var self = this;
        self.getBillerProductsSub = self.dashService.getBillerProductList(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                dashboard_services_1.DashboardService.CrossPageBillerProductList = new Array();
                dashboard_services_1.DashboardService.CrossPageBillerProductList = response.Products;
            }
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            // self.showBusyLoader = false;   
            // self.cdRef.detectChanges();
        });
    };
    BillersComponent.prototype.filterBillers = function () {
        var _this = this;
        this.billerIdSelected = undefined;
        $("[name=listRadio]").prop('checked', false);
        if (this.billerNameFilter) {
            this.filteredBillers = this.billers.filter(function (x) { return x.billerNickName.toLowerCase().indexOf(_this.billerNameFilter.toLowerCase()) != -1; });
        }
        else {
            this.filteredBillers = this.billers;
        }
    };
    BillersComponent.prototype.sortedBillers = function () {
        return this.filteredBillers.sort(function (a, b) {
            if (a.billerNickName < b.billerNickName)
                return -1;
            if (a.billerNickName > b.billerNickName)
                return 1;
            return 0;
        });
    };
    BillersComponent.prototype.selectBiller = function (idx) {
        this.billerIdSelected = idx;
        this.payButtonText = "Pay " + this.filteredBillers[idx].billerNickName.toUpperCase();
    };
    BillersComponent.prototype.viewBillerDetails = function (billerId) {
        var biller = this.filteredBillers.filter(function (c) { return c.billerId == billerId; })[0];
        if (biller) {
            dashboard_services_1.DashboardService.CrossBillerBeneficiaryObject = biller;
            this.router.navigate(['MainView/Recipients/Biller/Details']);
        }
    };
    BillersComponent.prototype.navigateToPayBillerDetails = function (event) {
        event.stopPropagation();
        var self = this;
        dashboard_services_1.DashboardService.CrossBillerBeneficiaryObject = this.filteredBillers[self.billerIdSelected];
        this.router.navigate(['MainView/PayTransfer/Biller/PayBillerDetails']);
        return false;
    };
    BillersComponent.prototype.gotoBeneficiariesView = function () {
        this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
    };
    BillersComponent.prototype.gotoBillersView = function () {
        this.router.navigate(['MainView/PayTransfer/Biller/List']);
    };
    BillersComponent.prototype.addBiller = function () {
        this.router.navigate(['MainView/Recipients/Biller/AddBiller']);
    };
    BillersComponent.prototype.goHome = function () {
        this.router.navigate(['dashboard']);
    };
    BillersComponent.prototype.navigateToSelfTransferView = function () {
    };
    BillersComponent.prototype.navigateToInternationalPaymentView = function () {
    };
    BillersComponent.prototype.navigateToRemitaPaymentsView = function () {
    };
    BillersComponent.prototype.navigateToOneOffWalletPayment = function () {
    };
    BillersComponent.prototype.navigateToMobileWalletView = function () {
    };
    BillersComponent.prototype.focusOnInput = function () {
        $('.input-type-textbox').focus();
        this.hideMobilesearch = false;
    };
    BillersComponent.prototype.groupFunction = function () {
    };
    BillersComponent.prototype.ngOnDestroy = function () {
        if (this.getBillersSub) {
            this.getBillersSub.unsubscribe();
        }
        if (this.getBillerProductsSub) {
            this.getBillerProductsSub.unsubscribe();
        }
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
    };
    BillersComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    BillersComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    BillersComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/biller/billers.modal.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], BillersComponent);
    return BillersComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.BillersComponent = BillersComponent;
//# sourceMappingURL=billers.component.js.map