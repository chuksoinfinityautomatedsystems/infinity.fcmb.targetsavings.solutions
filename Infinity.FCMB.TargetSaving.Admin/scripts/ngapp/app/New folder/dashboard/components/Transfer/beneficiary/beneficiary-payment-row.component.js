"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var common_1 = require('@angular/common');
var utils_services_1 = require("../../../../commons/services/utils.services");
var event_handlers_service_1 = require("../../../../commons/services/event_handlers.service");
var component_event_publisher_enum_1 = require("../../../../commons/enums/component_event_publisher.enum");
var beneficiaries_model_1 = require("../../../models/beneficiaries.model");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
function _window() {
    // return the global native browser window object
    return window;
}
var BeneficiaryPaymentRowComponent = (function () {
    function BeneficiaryPaymentRowComponent(cdRef, utilService, decimalPipe) {
        this.cdRef = cdRef;
        this.utilService = utilService;
        this.decimalPipe = decimalPipe;
        this.isFormOpen = true;
        this.showTransactionLimit = this.notifyBeneficiaryAboutPayment = false;
        this.lastAmountOnBlur = this.lastTransferChargesOnBlur = 0;
        this.withinDailyLimit = true;
        this.selectIsEmpty = true;
        this.selectIsOpen = false;
        this.countryCodeSelected = '';
        this.countryCodeHoveredIndex = 0;
        this.filteredCountryCodes = this.countryCodes;
        this.LOCAL_BANK_CODE = appsettings_constant_1.Appsettings.LOCAL_BANK_CODE;
    }
    BeneficiaryPaymentRowComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this.filteredCountryCodes = this.countryCodes;
        // Cancel Insufficient error  if event is thrown        
        this.cancelInsufficientFundErrorSub = event_handlers_service_1.EventHandlerService.ClearInsufficientFundErrorEventPublisher
            .subscribe(function (object) {
            var componentType = object.forComponent;
            if (componentType == component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer) {
                _this.insufficientFund = false;
            }
        });
        // Cancel Insufficient error  if event is thrown        
        this.requiredAmountErrorSub = event_handlers_service_1.EventHandlerService.RequiredAmountErrorEventPublisher
            .subscribe(function (object) {
            var componentType = object.forComponent;
            var accountNo = object.accountNo;
            var isError = object.isError;
            if (componentType == component_event_publisher_enum_1.ComponentEventPublisherEnum.BeneficiaryTransfer && accountNo.toLowerCase() == self.obj.beneficiaryAccountNumber.toLowerCase()) {
                console.log('Major blocker...');
                console.log(isError);
                _this.amountRequiredError = isError;
            }
        });
        this.cancelAmountNotWithinAvailableLimitErrorSub = event_handlers_service_1.EventHandlerService.ClearAvailableLimitErrorEventPublisher
            .subscribe(function (object) {
            var componentType = object.forComponent;
            if (componentType == component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer) {
                _this.withinDailyLimit = true;
            }
        });
        // If focus is on other beneficiary sub panel. Remove all error from this sub panel.
        this.cancelOtherInsufficientFundErrorSub = event_handlers_service_1.EventHandlerService.ClearOtherInsufficientFundErrorEventPublisher
            .subscribe(function (object) {
            var componentType = object.forComponent;
            var emmitterRef = object.emmitterRef;
            if (componentType == component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer &&
                (emmitterRef && emmitterRef.toLowerCase() != _this.obj.beneficiaryAccountNumber.toLowerCase())) {
                _this.insufficientFund = false;
            }
        });
        this.cancelOtherAmountNotWithinAvailableLimitErrorSub = event_handlers_service_1.EventHandlerService.ClearOtherAvailableLimitErrorEventPublisher
            .subscribe(function (object) {
            var componentType = object.forComponent;
            var emmitterRef = object.emmitterRef;
            if (componentType == component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer &&
                (emmitterRef && emmitterRef.toLowerCase() != _this.obj.beneficiaryAccountNumber.toLowerCase())) {
                _this.withinDailyLimit = true;
            }
        });
        this.cancelInsufficientFundErrorSub = event_handlers_service_1.EventHandlerService.ClearInsufficientFundErrorEventPublisher
            .subscribe(function (object) {
            var componentType = object.forComponent;
            if (componentType == component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer) {
                _this.insufficientFund = false;
            }
        });
        // If focus is on other beneficiary sub panel. Remove all error from this sub panel.
        this.cancelOtherInsufficientFundErrorSub = event_handlers_service_1.EventHandlerService.LastAmountAndChargesEnteredEventPublisher
            .subscribe(function (object) {
            var componentType = object.forComponent;
            var accountNo = object.accountNo;
            if (componentType == component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer &&
                self.obj.beneficiaryAccountNumber.toLowerCase() == accountNo.toLowerCase()) {
                self.lastAmountOnBlur = object.lastAmount;
                self.lastTransferChargesOnBlur = object.lastCharges;
            }
        });
        //  group[beneficiary.beneficiaryId + "_beneficiaryCustomerRef"] = new FormControl('');
        //     group[beneficiary.beneficiaryId + "_beneficiaryRef"] = new FormControl('');
        if (this.obj && this.obj.customerReference) {
            this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryCustomerRef"].setValue(this.obj.customerReference.toUpperCase());
        }
        if (this.obj && this.obj.beneficiaryReference) {
            this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryRef"].setValue(this.obj.beneficiaryReference.toUpperCase());
        }
        // if (this.obj && this.obj.beneficiaryReference) {
        //     this.theirReference = this.obj.beneficiaryReference.toUpperCase();
        //     this.theirReferenceInputActive = true;
        // }       
    };
    // An Amount field, show transaction limit, init first key up.
    // Remove formatting from if field is amount.
    BeneficiaryPaymentRowComponent.prototype.textInputFocus = function (formControlName) {
        try {
            this.textInputActive = formControlName;
            if (formControlName == this.obj.beneficiaryId + "_beneficiaryAmount") {
                this.amountRequiredError = false;
                this.showTransactionLimit = true;
                this.firstKeyUpPassedAfterAmountFocus = false;
                if (this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"]) {
                    var amountEntered = this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"].value;
                    if (amountEntered) {
                        //remove .00 if exist
                        if (amountEntered.indexOf('.00') != -1) {
                            amountEntered = amountEntered.replace('.00', '');
                        }
                        // Filter non-digits from input value.
                        if (/\D/g.test(amountEntered)) {
                            amountEntered = amountEntered.replace(/\D/g, '');
                        }
                    }
                    this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"].setValue(amountEntered);
                }
            }
            this.cdRef.detectChanges();
        }
        catch (ex) {
            console.log(ex);
        }
    };
    // Keep track of the last transfer charges and amount entered on this amount field.
    // This must be removed from the total amount and charges for the next update on this amount field.
    // Format text with number: '.2' if amount field
    BeneficiaryPaymentRowComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.subPaymentForm.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
        if (formControlName == this.obj.beneficiaryId + "_beneficiaryAmount") {
            this.showTransactionLimit = false;
            var transferCharges = 0;
            var amount = 0;
            if (this.subPaymentForm.controls['' + formControlName]) {
                amount = this.subPaymentForm.controls['' + formControlName].value;
            }
            if (this.obj.beneficiaryBankCode.toLowerCase() != appsettings_constant_1.Appsettings.LOCAL_BANK_CODE &&
                this.applyCharges && amount > 0) {
                transferCharges = this.otherBankCharges;
            }
            console.log("transferCharges in blur: " + transferCharges);
            this.lastAmountOnBlur = amount;
            this.lastTransferChargesOnBlur = transferCharges;
            this.cdRef.detectChanges();
            // Update amount entered and charges for beneficiary. This would have effect on the total charges and amount for all beneficiary on the view.
            event_handlers_service_1.EventHandlerService.emitTransferAmountDetails(this.lastTransferChargesOnBlur, this.lastAmountOnBlur, component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer, this.obj.beneficiaryAccountNumber, this.obj.beneficiaryBankCode);
            if (amount > 0) {
                this.subPaymentForm.controls['' + formControlName].setValue(this.decimalPipe.transform(amount, '.2'));
            }
        }
    };
    // Remove the last amount and charges saved at the last blur for this field for every new amount entered.
    // Clear all error messages on all other amount fields.
    // Update the current total amount accross all the view.
    // Update Total Amount on view via an event Publisher
    BeneficiaryPaymentRowComponent.prototype.amountInputKeyup = function () {
        var errorOnAmountField = false;
        this.subPaymentForm.controls[this.obj.beneficiaryId + "_errorOnView"].setValue(false);
        // Only allow digits on amount alone.        
        if (this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"]) {
            var amountEntered = this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"].value;
            if (amountEntered && /\D/g.test(amountEntered)) {
                // Filter non-digits from input value.
                amountEntered = amountEntered.replace(/\D/g, '');
            }
            this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"].setValue(amountEntered);
        }
        // On first keyup, clear all errors on the other beneficiary amount fields.        
        if (!this.firstKeyUpPassedAfterAmountFocus) {
            this.firstKeyUpPassedAfterAmountFocus = true;
            event_handlers_service_1.EventHandlerService.emitClearOtherInsufficientFundErrorEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer, this.obj.beneficiaryAccountNumber);
            event_handlers_service_1.EventHandlerService.emitClearOtherAvailableLimitErrorEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer, this.obj.beneficiaryAccountNumber);
        }
        console.log("totalAmountEnteredSoFar after event was thrown: " + this.totalAmountEnteredSoFar);
        console.log("totalChargesSoFar after event was thrown: " + this.totalChargesSoFar);
        console.log("lastAmountOnBlur after event was thrown: " + this.lastAmountOnBlur);
        console.log("lastTransferChargesOnBlur after event was thrown: " + this.lastTransferChargesOnBlur);
        var amount = 0;
        if (this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"]) {
            amount = this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"].value;
        }
        // Remove the last entered amount and charges from the total amount so far on page before adding the new value entered.
        var currentTotalAmount = +this.totalAmountEnteredSoFar + +this.totalChargesSoFar + +amount - +this.lastAmountOnBlur - +this.lastTransferChargesOnBlur;
        var currentTotalAmountWithoutCharges = +this.totalAmountEnteredSoFar + +amount - +this.lastAmountOnBlur;
        console.log("Current amount: " + amount);
        console.log("Current total amount: " + currentTotalAmount);
        // Compute and publish the new total amount and charges.
        //Also show the neccessary error message.
        if (this.obj.beneficiaryBankCode.toLowerCase() != appsettings_constant_1.Appsettings.LOCAL_BANK_CODE && this.applyCharges && amount > 0) {
            var totalAmount = +currentTotalAmount + +this.otherBankCharges;
            event_handlers_service_1.EventHandlerService.emitTotalAmountEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer, totalAmount, this.otherBankCharges);
            if (totalAmount > this.availableBalance) {
                this.insufficientFund = true;
                errorOnAmountField = true;
            }
            else {
                this.insufficientFund = false;
                errorOnAmountField = false;
            }
            console.log("Current total amount with charges: " + totalAmount);
        }
        else {
            event_handlers_service_1.EventHandlerService.emitTotalAmountEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer, currentTotalAmount, 0);
            if (currentTotalAmount > this.availableBalance) {
                this.insufficientFund = true;
                errorOnAmountField = true;
            }
            else {
                this.insufficientFund = false;
                errorOnAmountField = false;
            }
        }
        if (currentTotalAmountWithoutCharges > (+this.availableLimit - +this.amountUtilizedFromLimit)) {
            this.withinDailyLimit = false;
            if (errorOnAmountField == false) {
                errorOnAmountField = true;
            }
        }
        else {
            this.withinDailyLimit = true;
        }
        this.subPaymentForm.controls[this.obj.beneficiaryId + "_errorOnView"].setValue(errorOnAmountField);
        this.cdRef.detectChanges();
    };
    BeneficiaryPaymentRowComponent.prototype.enableCustomerNotification = function (notificationType) {
        this.notifyBeneficiaryAboutPayment = true;
        this.textInputActive = '';
        if (notificationType == appsettings_constant_1.Appsettings.TRANSFER_NOTIFICATION_VIA_SMS) {
            this.showEmailNotificationField = false; // this.beneficiaryEmailInputActive = false; // show sms field
            this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryEmail"].setValue(undefined);
        }
        else {
            this.showEmailNotificationField = true;
            this.selectIsEmpty = true;
            this.selectIsOpen = false;
            this.countryCodeSelected = '';
            this.countryCodeHoveredIndex = 0;
            this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryCountryCodeSelected"].setValue(undefined);
            this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryPhoneNo"].setValue(undefined);
        }
    };
    BeneficiaryPaymentRowComponent.prototype.disableCustomerNotification = function () {
        this.notifyBeneficiaryAboutPayment = false;
        this.showEmailNotificationField = false;
        this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryEmail"].setValue(undefined);
        this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryPhoneNo"].setValue(undefined);
        this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryCountryCodeSelected"].setValue(undefined);
        this.selectIsEmpty = true;
        this.selectIsOpen = false;
        this.countryCodeSelected = '';
        this.countryCodeHoveredIndex = 0;
        this.textInputActive = '';
    };
    Object.defineProperty(BeneficiaryPaymentRowComponent.prototype, "subFormInvalid", {
        get: function () {
            var amount = 0;
            if (this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"]) {
                amount = this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryAmount"].value;
            }
            return !amount || amount <= 0;
        },
        enumerable: true,
        configurable: true
    });
    // control the visibility of other fields on the payment pane.
    BeneficiaryPaymentRowComponent.prototype.openOrClosePaymentPane = function () {
        this.isFormOpen = !this.isFormOpen;
    };
    BeneficiaryPaymentRowComponent.prototype.filterCountryPhoneCode = function () {
        var _this = this;
        if (this.countryCodeFilter) {
            this.filteredCountryCodes = this.countryCodes.filter(function (x) { return x.countryName.toLowerCase().indexOf(_this.countryCodeFilter.toLowerCase()) != -1; });
        }
        else {
            this.filteredCountryCodes = this.countryCodes;
        }
    };
    BeneficiaryPaymentRowComponent.prototype.sortedCountryCodes = function () {
        return this.filteredCountryCodes.sort(function (a, b) {
            if (a.countryName < b.countryName)
                return -1;
            if (a.countryName > b.countryName)
                return 1;
            return 0;
        });
    };
    BeneficiaryPaymentRowComponent.prototype.selectCountryCode = function (idx, event) {
        event.stopPropagation();
        this.countryCodeSelected = this.filteredCountryCodes[idx].code;
        this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryCountryCodeSelected"].setValue(this.countryCodeSelected);
        this.selectIsEmpty = false;
        this.selectIsOpen = false;
        var countryCode = this.filteredCountryCodes[idx];
        this.countryCodeFilter = "";
        this.filteredCountryCodes = this.countryCodes;
        this.countryCodeHoveredIndex = this.countryCodes.indexOf(countryCode);
        return false;
    };
    BeneficiaryPaymentRowComponent.prototype.setActiveCountry = function (idx) {
        this.countryCodeHoveredIndex = idx;
    };
    BeneficiaryPaymentRowComponent.prototype.countryHovered = function (idx) {
        return idx == this.countryCodeHoveredIndex;
    };
    BeneficiaryPaymentRowComponent.prototype.telephoneInputKeyUp = function () {
        if (this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryPhoneNo"]) {
            var phoneEntered = this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryPhoneNo"].value;
            if (phoneEntered && /\D/g.test(phoneEntered)) {
                // Filter non-digits from input value.
                phoneEntered = phoneEntered.replace(/\D/g, '');
            }
            this.subPaymentForm.controls[this.obj.beneficiaryId + "_beneficiaryPhoneNo"].setValue(phoneEntered);
        }
    };
    BeneficiaryPaymentRowComponent.prototype.toggleSelectClick = function (event) {
        event.stopPropagation();
        this.selectIsOpen = !this.selectIsOpen;
        return false;
    };
    BeneficiaryPaymentRowComponent.prototype.ngOnDestroy = function () {
        if (this.cancelInsufficientFundErrorSub) {
            this.cancelInsufficientFundErrorSub.unsubscribe();
        }
        if (this.requiredAmountErrorSub) {
            this.requiredAmountErrorSub.unsubscribe();
        }
        if (this.cancelAmountNotWithinAvailableLimitErrorSub) {
            this.cancelAmountNotWithinAvailableLimitErrorSub.unsubscribe();
        }
        if (this.cancelOtherInsufficientFundErrorSub) {
            this.cancelOtherInsufficientFundErrorSub.unsubscribe();
        }
        if (this.cancelOtherAmountNotWithinAvailableLimitErrorSub) {
            this.cancelOtherAmountNotWithinAvailableLimitErrorSub.unsubscribe();
        }
        if (this.cancelInsufficientFundErrorSub) {
            this.cancelInsufficientFundErrorSub.unsubscribe();
        }
        if (this.cancelOtherInsufficientFundErrorSub) {
            this.cancelOtherInsufficientFundErrorSub.unsubscribe();
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', (typeof (_a = typeof beneficiaries_model_1.Beneficiary !== 'undefined' && beneficiaries_model_1.Beneficiary) === 'function' && _a) || Object)
    ], BeneficiaryPaymentRowComponent.prototype, "obj", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', forms_1.FormGroup)
    ], BeneficiaryPaymentRowComponent.prototype, "subPaymentForm", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], BeneficiaryPaymentRowComponent.prototype, "availableLimit", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], BeneficiaryPaymentRowComponent.prototype, "availableBalance", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], BeneficiaryPaymentRowComponent.prototype, "otherBankCharges", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], BeneficiaryPaymentRowComponent.prototype, "applyCharges", void 0);
    __decorate([
        // Only apply transfer charges on local (NGN) transfer to non-local banks
        core_1.Input(), 
        __metadata('design:type', Number)
    ], BeneficiaryPaymentRowComponent.prototype, "amountUtilizedFromLimit", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], BeneficiaryPaymentRowComponent.prototype, "totalAmountEnteredSoFar", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], BeneficiaryPaymentRowComponent.prototype, "totalChargesSoFar", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], BeneficiaryPaymentRowComponent.prototype, "transactionCurrency", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], BeneficiaryPaymentRowComponent.prototype, "countryCodes", void 0);
    BeneficiaryPaymentRowComponent = __decorate([
        core_1.Component({
            selector: "ng-beneficiary-payment-row",
            templateUrl: "html/home/transfer/beneficiary/beneficiary-payment-row.html"
        }), 
        __metadata('design:paramtypes', [core_1.ChangeDetectorRef, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, common_1.DecimalPipe])
    ], BeneficiaryPaymentRowComponent);
    return BeneficiaryPaymentRowComponent;
    var _a, _b;
}());
exports.BeneficiaryPaymentRowComponent = BeneficiaryPaymentRowComponent;
//# sourceMappingURL=beneficiary-payment-row.component.js.map