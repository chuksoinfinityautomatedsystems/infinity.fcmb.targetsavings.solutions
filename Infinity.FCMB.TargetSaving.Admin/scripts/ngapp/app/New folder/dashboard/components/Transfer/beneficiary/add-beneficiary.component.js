"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../../services/dashboard.services");
var utils_services_1 = require("../../../../commons/services/utils.services");
var constants_services_1 = require("../../../../commons/services/constants.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var base_component_1 = require("../../../../commons/components/base.component");
var event_handlers_service_1 = require("../../../../commons/services/event_handlers.service");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../../../commons/constants/response_codes.constant");
function _window() {
    // return the global native browser window object
    return window;
}
var AddBeneficiaryComponent = (function (_super) {
    __extends(AddBeneficiaryComponent, _super);
    function AddBeneficiaryComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef) {
        _super.call(this, authService, router);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        //  this.newBeneficiaryFormGroup = formBuilder.group({}); 
        // LOGIN FORM INIT
        this.newBeneficiaryFormGroup = formBuilder.group({
            userID: [''],
            accountNo: ['', forms_1.Validators.required],
            bankName: ['', forms_1.Validators.required],
            bankCode: ['', forms_1.Validators.required],
            beneficiaryName: ['', forms_1.Validators.required],
            emailAddress: [''],
            countryCode: [''],
            mobileNo: [''],
            myReference: [''],
            beneficiaryReference: [''],
            beneficiaryAccountName: [''],
            group: ['']
        });
        this.newBeneficiaryVerificationViewFormGroup = formBuilder.group({
            termsConditions: [false]
        });
        this.formSubmitted = this.countryHasError = this.invalidAccountNumber = false;
        this.showExceptionPage = false;
        this.exceptionMessage = '';
        //OTP INIT
        this.otpFormGroup = formBuilder.group({
            otpModel1: ['', forms_1.Validators.required],
            otpModel2: ['', forms_1.Validators.required],
            otpModel3: ['', forms_1.Validators.required],
            otpModel4: ['', forms_1.Validators.required],
            otpModel5: ['', forms_1.Validators.required]
        });
        this.otpValues = ["", "", "", "", ""];
        this.showOtpError = this.otpFormSubmitted = this.showOtpView = false;
        this.termsAndConditionUnChecked = false;
        this.hideOTPToast = true;
        this.isErrorToast = false;
    }
    AddBeneficiaryComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this.filteredCountryCodes = this.dashService.getCountryCodes();
        this.filteredBanks = this.dashService.getBanks();
        this.filteredCountryCodes = this.sortedCountryCodes();
        this.filteredBanks = this.sortedBanks();
        this.selectedBankCodeSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            var optionKey = object.optionKey;
            var dropdownIndex = object.index;
            console.log('Dropdown key selected: ' + optionKey);
            console.log('Dropdown Index: ' + optionKey);
            switch (dropdownIndex) {
                case 1:
                    var selectedBank = self.filteredBanks.filter(function (x) { return x.code.toLowerCase() == optionKey.toLowerCase(); })[0];
                    console.log('Selected Bank: ' + selectedBank);
                    if (selectedBank) {
                        self.newBeneficiaryFormGroup.controls['bankCode'].setValue(selectedBank.code);
                        self.newBeneficiaryFormGroup.controls['bankName'].setValue(selectedBank.name);
                    }
                    else {
                        self.newBeneficiaryFormGroup.controls['bankCode'].setValue(undefined);
                        self.newBeneficiaryFormGroup.controls['bankName'].setValue(undefined);
                    }
                    break;
                case 2:
                    var selectedCountry = self.filteredCountryCodes.filter(function (x) { return x.code.toLowerCase() == optionKey.toLowerCase(); })[0];
                    console.log('Selected Bank: ' + selectedCountry);
                    if (selectedCountry) {
                        self.selectedCountryCode = selectedCountry.code;
                        if (_this.countryHasError) {
                            _this.countryHasError = false;
                        }
                    }
                    else {
                        self.selectedCountryCode = undefined;
                    }
                    break;
            }
        });
    };
    AddBeneficiaryComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.countryHasError = this.invalidAccountNumber = false;
        this.cdRef.detectChanges();
    };
    AddBeneficiaryComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.newBeneficiaryFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
    };
    AddBeneficiaryComponent.prototype.sortedCountryCodes = function () {
        return this.filteredCountryCodes.sort(function (a, b) {
            if (a.countryName < b.countryName)
                return -1;
            if (a.countryName > b.countryName)
                return 1;
            return 0;
        });
    };
    AddBeneficiaryComponent.prototype.sortedBanks = function () {
        return this.filteredBanks.sort(function (a, b) {
            if (a.name < b.name)
                return -1;
            if (a.name > b.name)
                return 1;
            return 0;
        });
    };
    AddBeneficiaryComponent.prototype.backHome = function () {
        // this.ngOnDestroy();
        this.router.navigate(['dashboard']);
    };
    AddBeneficiaryComponent.prototype.nextToReview = function (event) {
        event.stopPropagation();
        var self = this;
        this.formSubmitted = true;
        var otherViewErrorCount = 0;
        if (self.newBeneficiaryFormGroup.valid) {
            var accountNo = self.newBeneficiaryFormGroup.controls['accountNo'].value;
            var mobileNo = self.newBeneficiaryFormGroup.controls['mobileNo'].value;
            //   let countryCode = self.newBeneficiaryFormGroup.controls['countryCode'].value;
            if (!accountNo || (accountNo && utils_services_1.UtilService.trim(accountNo).length > appsettings_constant_1.Appsettings.MINIMUM_ACCOUNT_NO_LENGTH)) {
                self.invalidAccountNumber = true;
                otherViewErrorCount += 1;
            }
            else {
                self.invalidAccountNumber = false;
            }
            if (mobileNo && utils_services_1.UtilService.trim(mobileNo).length > 0 && !self.selectedCountryCode) {
                self.countryHasError = true;
                otherViewErrorCount += 1;
            }
            else {
                self.countryHasError = false;
            }
            self.cdRef.detectChanges();
            if (otherViewErrorCount > 0) {
            }
            else {
                // Do Name  Enquiry.
                if (self.selectedCountryCode) {
                    self.newBeneficiaryFormGroup.controls['countryCode'].setValue(self.selectedCountryCode);
                }
                else {
                    self.newBeneficiaryFormGroup.controls['countryCode'].setValue(undefined);
                }
                console.log(self.newBeneficiaryFormGroup.controls);
                self.showBusyLoader = true;
                self.cdRef.detectChanges();
                if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                    var loadingModalInterval_1 = setInterval(function () {
                        self.showBusyLoader = false;
                        self.cdRef.detectChanges();
                        self._doNameEnquiry();
                        _window().clearInterval(loadingModalInterval_1);
                    }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
                }
                else {
                    self._doNameEnquiry();
                }
            }
        }
        return false;
    };
    AddBeneficiaryComponent.prototype._doNameEnquiry = function () {
        var _this = this;
        var self = this;
        self.doNameEnquirySub = self.dashService.doNameEnquiry(authentication_service_1.AuthenticationService.authUserObj.UserId, self.newBeneficiaryFormGroup.controls['accountNo'].value, self.newBeneficiaryFormGroup.controls['bankCode'].value)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.newBeneficiaryFormGroup.controls['beneficiaryAccountName'].setValue(response.ResponseFriendlyMessage);
                _this.pageIndex = 2;
                _this.pageSubtitle = "Review";
                self.cdRef.detectChanges();
            }
            else {
                self.newBeneficiaryFormGroup.controls['beneficiaryAccountName'].setValue(undefined);
                _this.exceptionMessage = response.ResponseFriendlyMessage;
                _this.showExceptionPage = true;
                self.cdRef.detectChanges();
            }
        }, function (error) {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    AddBeneficiaryComponent.prototype.mobileNoInputKeyup = function () {
        var mobileNoEntered = this.newBeneficiaryFormGroup.controls["mobileNo"].value;
        if (mobileNoEntered && /\D/g.test(mobileNoEntered)) {
            // Filter non-digits from input value.
            mobileNoEntered = mobileNoEntered.replace(/\D/g, '');
        }
        this.newBeneficiaryFormGroup.controls["mobileNo"].setValue(mobileNoEntered);
    };
    AddBeneficiaryComponent.prototype.closeExceptionModal = function () {
        this.exceptionMessage = '';
        this.showExceptionPage = false;
    };
    AddBeneficiaryComponent.prototype.finishBeneficiaryAction = function (event) {
        event.stopPropagation();
        var self = this;
        self.termsAndConditionUnChecked = false;
        //initiate otp
        if (self.newBeneficiaryVerificationViewFormGroup.controls["termsConditions"].value == false) {
            self.termsAndConditionUnChecked = true;
            return false;
        }
        else {
            self.showBusyLoader = true;
            self.cdRef.detectChanges();
            if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                var loadingModalInterval_2 = setInterval(function () {
                    self._initiateOTPRequest();
                    _window().clearInterval(loadingModalInterval_2);
                }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
            }
            else {
                self._initiateOTPRequest();
            }
        }
    };
    AddBeneficiaryComponent.prototype._initiateOTPRequest = function () {
        var _this = this;
        var self = this;
        self.isErrorToast = true;
        self.toastMessage = "";
        self.cdRef.detectChanges();
        self.initiateOtpSub = self.authService.initiateOTP(authentication_service_1.AuthenticationService.authUserObj.UserId, authentication_service_1.AuthenticationService.authUserObj.CifID, appsettings_constant_1.Appsettings.OTP_REASON_CODE_ADD_BENEFICIARY)
            .subscribe(function (initiateOtpResponse) {
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.showOtpView = true;
                self.showBusyLoader = false;
            }
            else {
                _this.exceptionMessage = initiateOtpResponse.ResponseFriendlyMessage;
                _this.showExceptionPage = true;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            _this.exceptionMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            _this.showExceptionPage = true;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    AddBeneficiaryComponent.prototype.reSendOtp = function () {
        var self = this;
        //re-init toast container
        self.hideOTPToast = true;
        self.toastMessage = "";
        self.isErrorToast = true;
        self.showBusyLoader = true;
        self.cdRef.detectChanges();
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_3 = setInterval(function () {
                self.showBusyLoader = false;
                self.cdRef.detectChanges();
                self._resendOTP();
                _window().clearInterval(loadingModalInterval_3);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._resendOTP();
        }
    };
    AddBeneficiaryComponent.prototype._resendOTP = function () {
        var self = this;
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
        self.initiateOtpSub = self.authService.initiateOTP(authentication_service_1.AuthenticationService.authUserObj.UserId, authentication_service_1.AuthenticationService.authUserObj.CifID, appsettings_constant_1.Appsettings.OTP_REASON_CODE_ADD_BENEFICIARY)
            .subscribe(function (initiateOtpResponse) {
            self.showBusyLoader = false;
            self.hideOTPToast = false;
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.isErrorToast = false;
                self.toastMessage = appsettings_constant_1.Appsettings.OTP_SENT_MESSAGE;
            }
            else {
                self.isErrorToast = true;
                self.toastMessage = initiateOtpResponse.ResponseFriendlyMessage;
            }
            self.InitToastr();
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    // OTP SNIPPETS.
    AddBeneficiaryComponent.prototype.closeOtpView = function () {
        this.clearOTPValues();
        this.showOtpView = false;
        this.otpFormSubmitted = false;
        this.hideOTPToast = true;
        this.cdRef.detectChanges();
    };
    AddBeneficiaryComponent.prototype.clearOTPValues = function () {
        this.otpFormGroup.controls["otpModel1"].setValue("");
        this.otpFormGroup.controls["otpModel2"].setValue("");
        this.otpFormGroup.controls["otpModel3"].setValue("");
        this.otpFormGroup.controls["otpModel4"].setValue("");
        this.otpFormGroup.controls["otpModel5"].setValue("");
        $("#otpModel1").focus();
        this.otpValues[0] = this.otpValues[1] = this.otpValues[2] = this.otpValues[3] = this.otpValues[4] = "";
        this.showOtpError = false;
        this.cdRef.detectChanges();
    };
    AddBeneficiaryComponent.prototype.preventArrow = function (event) {
        var key = event.charCode || event.keyCode || 0;
        if (key > 36 && key < 41) {
            event.preventDefault();
        }
    };
    AddBeneficiaryComponent.prototype.goNext = function (event, index) {
        var key = event.charCode || event.keyCode || 0;
        // Shift Key : do nada
        if (event.shiftKey) {
            return false;
        }
        else if (key == 8 || key == 46 || key == 45) {
            this.clearOTPValues();
            this.showOtpError = true;
        }
        else if (key == 9) {
            if (!this.otpValues[index - 1]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else if (key > 36 && key < 41) {
            if (index == 5 && !this.otpValues[4]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else {
            //save value and replace with * on txtbox view (if not empty); move to next tab
            if (index == 1) {
                if (this.otpFormGroup.controls["otpModel1"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel1"].value)) {
                    this.otpValues[0] = this.otpFormGroup.controls["otpModel1"].value;
                    this.otpFormGroup.controls["otpModel1"].setValue("*");
                }
                else {
                    this.otpValues[0] = "";
                }
                $("#otpModel2").focus();
            }
            else if (index == 2) {
                if (this.otpFormGroup.controls["otpModel2"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel2"].value)) {
                    this.otpValues[1] = this.otpFormGroup.controls["otpModel2"].value;
                    this.otpFormGroup.controls["otpModel2"].setValue("*");
                }
                else {
                    this.otpValues[1] = "";
                }
                $("#otpModel3").focus();
            }
            else if (index == 3) {
                if (this.otpFormGroup.controls["otpModel3"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel3"].value)) {
                    this.otpValues[2] = this.otpFormGroup.controls["otpModel3"].value;
                    this.otpFormGroup.controls["otpModel3"].setValue("*");
                }
                else {
                    this.otpValues[2] = "";
                }
                $("#otpModel4").focus();
            }
            else if (index == 4) {
                if (this.otpFormGroup.controls["otpModel4"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel4"].value)) {
                    this.otpValues[3] = this.otpFormGroup.controls["otpModel4"].value;
                    this.otpFormGroup.controls["otpModel4"].setValue("*");
                }
                else {
                    this.otpValues[3] = "";
                }
                $("#otpModel5").focus();
            }
            else if (index == 5) {
                if (this.otpFormGroup.controls["otpModel5"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel5"].value)) {
                    this.otpValues[4] = this.otpFormGroup.controls["otpModel5"].value;
                    this.otpFormGroup.controls["otpModel5"].setValue("*");
                }
                else {
                    this.otpValues[4] = "";
                }
            }
            //invalidate otp form if last otp xter is empty
            if (this.otpValues[4]) {
                this.showOtpError = false;
            }
            else {
                this.showOtpError = true;
            }
        }
    };
    AddBeneficiaryComponent.prototype.backToAddForm = function (event) {
        event.stopPropagation();
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        return false;
    };
    AddBeneficiaryComponent.prototype.backToBeneficiaryList = function (event) {
        event.stopPropagation();
        //   this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
        return false;
    };
    AddBeneficiaryComponent.prototype.Goto = function (url) {
        //  this.ngOnDestroy();
        this.router.navigate([url]);
        return false;
    };
    AddBeneficiaryComponent.prototype.logout = function () {
        //   this.ngOnDestroy();
        this.authService.logout();
        this.router.navigate(['login']);
    };
    AddBeneficiaryComponent.prototype.ngOnDestroy = function () {
        if (this.selectedBankCodeSub) {
            this.selectedBankCodeSub.unsubscribe();
        }
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
    };
    AddBeneficiaryComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/beneficiary/add-beneficiary.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], AddBeneficiaryComponent);
    return AddBeneficiaryComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.AddBeneficiaryComponent = AddBeneficiaryComponent;
//# sourceMappingURL=add-beneficiary.component.js.map