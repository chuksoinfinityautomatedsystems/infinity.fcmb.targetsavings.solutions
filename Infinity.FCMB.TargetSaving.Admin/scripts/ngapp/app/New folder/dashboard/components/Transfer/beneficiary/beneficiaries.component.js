"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var utils_services_1 = require("../../../../commons/services/utils.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../../../commons/constants/response_codes.constant");
var dashboard_services_1 = require("../../../services/dashboard.services");
var base_component_1 = require("../../../../commons/components/base.component");
function _window() {
    // return the global native browser window object
    return window;
}
var BeneficiariesComponent = (function (_super) {
    __extends(BeneficiariesComponent, _super);
    function BeneficiariesComponent(authService, utiilService, router, formBuilder, dashService, cdRef) {
        _super.call(this, authService, router);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.screenHeight = utils_services_1.UtilService.getScreenHeight();
        //Miscellaneous
        this.hideMobilesearch = true;
        this.navigateGroups = false;
        this.hasForeignAccounts = false;
        // Beneficiaries
        this.beneficiaryIdList = new Array();
        this.beneficiaries = this.filteredBeneficiaries = Array();
        this.payButtonText = this.beneficiaryNameFilter = "";
    }
    BeneficiariesComponent.prototype.ngOnInit = function () {
        dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject = undefined;
        var self = this;
        self.showBusyLoader = true;
        var foreignAccounts = dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.debitAllowed == true &&
            x.currency.toLowerCase() != appsettings_constant_1.Appsettings.LOCAL_CURRENCY.toLowerCase(); });
        if (foreignAccounts && foreignAccounts.length > 0) {
            self.hasForeignAccounts = true;
        }
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                self._getBeneficiaryList();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getBeneficiaryList();
        }
        // this.lastLogin = AuthenticationService.authUserObj.LastLoginIn;
        // self.SetupTimeout();  
    };
    BeneficiariesComponent.prototype._getBeneficiaryList = function () {
        var self = this;
        self.getBeneficiariesSub = self.dashService.getBeneficiaryList(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.beneficiaries = self.filteredBeneficiaries = response.Beneficiaries;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    BeneficiariesComponent.prototype.ngOnDestroy = function () {
        if (this.getBeneficiariesSub) {
            this.getBeneficiariesSub.unsubscribe();
        }
    };
    BeneficiariesComponent.prototype.toggleBeneficiarySelection = function (idx) {
        if (!this.beneficiaryIdList) {
            this.beneficiaryIdList = new Array();
        }
        if (this.beneficiaryIdList.indexOf(idx) != -1) {
            this.beneficiaryIdList.splice(this.beneficiaryIdList.indexOf(idx), 1);
        }
        else {
            this.beneficiaryIdList.push(idx);
        }
        // Update Payment Button Name
        if (this.beneficiaryIdList.length > 1) {
            this.payButtonText = "Pay " + this.beneficiaryIdList.length + " Selected";
        }
        else if (this.beneficiaryIdList && this.beneficiaryIdList.length == 1) {
            var beneficiaryName = this.filteredBeneficiaries[this.beneficiaryIdList[0]].beneficiaryName.toUpperCase();
            this.payButtonText = "Pay " + beneficiaryName;
        }
    };
    BeneficiariesComponent.prototype.filterBeneficiary = function () {
        var _this = this;
        //Unselect all selected beneficiary.
        this.beneficiaryIdList = undefined;
        $('.beneficiary-checkbox').prop("checked", false);
        if (this.beneficiaryNameFilter) {
            this.filteredBeneficiaries = this.beneficiaries.filter(function (x) { return x.beneficiaryName.toLowerCase().indexOf(_this.beneficiaryNameFilter.toLowerCase()) != -1; });
        }
        else {
            this.filteredBeneficiaries = this.beneficiaries;
        }
    };
    BeneficiariesComponent.prototype.sortedBeneficiaries = function () {
        return this.filteredBeneficiaries.sort(function (a, b) {
            if (a.beneficiaryName < b.beneficiaryName)
                return -1;
            if (a.beneficiaryName > b.beneficiaryName)
                return 1;
            return 0;
        });
    };
    BeneficiariesComponent.prototype.beneficiaryIdSelected = function (idx) {
        if (this.beneficiaryIdList.indexOf(idx) != -1) {
            return true;
        }
        return false;
    };
    BeneficiariesComponent.prototype.addBeneficiaryFunction = function () {
        //this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Beneficiary/Add']);
    };
    BeneficiariesComponent.prototype.navigateToThisBeneficiaryDetails = function (event) {
        event.stopPropagation();
        // 0003675134
        if (this.beneficiaryIdList && this.beneficiaryIdList.length > 0) {
            dashboard_services_1.DashboardService.CrossPageBeneficiaryList = new Array();
            for (var _i = 0, _a = this.beneficiaryIdList; _i < _a.length; _i++) {
                var index = _a[_i];
                dashboard_services_1.DashboardService.CrossPageBeneficiaryList.push(this.filteredBeneficiaries[index]);
            }
            if (dashboard_services_1.DashboardService.CrossPageBeneficiaryList.length > 0) {
                //   this.ngOnDestroy();
                this.router.navigate(['MainView/PayTransfer/Beneficiary/Details']);
            }
        }
        return false;
    };
    BeneficiariesComponent.prototype.gotoBeneficiariesView = function () {
        // this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
    };
    BeneficiariesComponent.prototype.goHome = function () {
        //this.ngOnDestroy();
        this.router.navigate(['dashboard']);
    };
    BeneficiariesComponent.prototype.gotoBillersView = function () {
        this.router.navigate(['MainView/PayTransfer/Biller/List']);
    };
    BeneficiariesComponent.prototype.navigateToSelfTransferView = function () {
        // this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Self-transfer']);
    };
    BeneficiariesComponent.prototype.navigateToInternationalPaymentView = function () {
        this.router.navigate(['MainView/PayTransfer/InternationalPayments']);
    };
    BeneficiariesComponent.prototype.navigateToRemitaPaymentsView = function () {
        //this.ngOnDestroy();
        this.router.navigate(['remita-payment']);
    };
    BeneficiariesComponent.prototype.navigateToOnceOffBeneficiaryDetails = function (event) {
        event.stopPropagation();
        //this.ngOnDestroy();  
        this.router.navigate(['MainView/PayTransfer/OneOffPayment']);
        return false;
    };
    BeneficiariesComponent.prototype.navigateToOneOffBeneficiaryDetails = function () {
        // this.ngOnDestroy();
        this.router.navigate(['benefeciary-details']);
        return false;
    };
    BeneficiariesComponent.prototype.viewBeneficiaryDetails = function (beneficiaryAccountNo) {
        var beneficiary = this.beneficiaries.filter(function (c) { return c.beneficiaryAccountNumber.toLowerCase() == beneficiaryAccountNo.toLowerCase(); })[0];
        dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject = beneficiary;
        //this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Recipient-details']);
    };
    BeneficiariesComponent.prototype.navigateToOneOffWalletPayment = function () {
    };
    BeneficiariesComponent.prototype.navigateToMobileWalletView = function () {
    };
    BeneficiariesComponent.prototype.focusOnInput = function () {
        $('.input-type-textbox').focus();
        this.hideMobilesearch = false;
    };
    BeneficiariesComponent.prototype.groupFunction = function () {
    };
    BeneficiariesComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    BeneficiariesComponent.prototype.logout = function () {
        //this.ngOnDestroy();
        this.authService.logout();
        this.router.navigate(['login']);
    };
    BeneficiariesComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/beneficiary/beneficiaries.modal.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, core_1.ChangeDetectorRef])
    ], BeneficiariesComponent);
    return BeneficiariesComponent;
    var _a, _b, _c;
}(base_component_1.BaseComponent));
exports.BeneficiariesComponent = BeneficiariesComponent;
//# sourceMappingURL=beneficiaries.component.js.map