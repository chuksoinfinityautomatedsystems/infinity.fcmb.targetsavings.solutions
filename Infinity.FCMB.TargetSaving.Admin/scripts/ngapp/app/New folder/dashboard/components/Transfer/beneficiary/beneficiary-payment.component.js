"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var utils_services_1 = require("../../../../commons/services/utils.services");
var constants_services_1 = require("../../../../commons/services/constants.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var event_handlers_service_1 = require("../../../../commons/services/event_handlers.service");
var component_event_publisher_enum_1 = require("../../../../commons/enums/component_event_publisher.enum");
var dashboard_services_1 = require("../../../services/dashboard.services");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
var schedule_payment_base_component_1 = require("../../../../commons/components/schedule_payment.base.component");
function _window() {
    // return the global native browser window object
    return window;
}
function add(a, b) {
    // return the global native browser window object
    return +a + +b;
}
var PayBeneficiariesComponent = (function (_super) {
    __extends(PayBeneficiariesComponent, _super);
    function PayBeneficiariesComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef) {
        _super.call(this, authService, router, cdRef);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        //  this.beneficiarySelectedIdList = new Array<number>();
        this._formGroup = formBuilder.group({});
        //this.payBeneFormGroup = formBuilder.group({});
        this.availableLimit = 1000000;
        // this.availableBalance = 30808.57;
        this.availableBalance = 100000;
        this.otherBankCharges = 52.50;
        this.amountUtilizedFromLimit = 20000.0;
        this.totalAmountEnteredSoFar = this.totalChargesSoFar = 0;
        this.amountBeneficiaryHash = new Array();
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.fromAccountSelectIsOpen = false;
        this.fromAccountSelectIsEmpty = true;
        this.fromAccountSelected = "";
        this.myAccountHoveredIndex = 0;
        this.cancelModalOpened = false;
        this.totalAmountEnteredSoFar = this.totalChargesSoFar = 0;
        //SCHEDULE PAYMENT
        this.frequencyTypes = appsettings_constant_1.Appsettings.SCHEDUE_PAYMENT_FREQUENCY_TYPES;
        this.numberOfPayments = 0;
        this.todaySelectedOnJustOnceSchedule = false;
    }
    // openOrCloseBeneficiaryDiv(idx: number) { 
    //     if (this.beneficiarySelectedIdList.indexOf(idx) != -1) {
    //         this.beneficiarySelectedIdList.splice(this.beneficiarySelectedIdList.indexOf(idx), 1);
    //     }
    //     else {
    //         this.beneficiarySelectedIdList.push(idx);
    //     }
    // }
    PayBeneficiariesComponent.prototype.navigateToPaymentsConfirmation = function () {
    };
    PayBeneficiariesComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        if (!dashboard_services_1.DashboardService.CrossPageBeneficiaryList) {
            this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
            return;
        }
        this.beneficiaries = dashboard_services_1.DashboardService.CrossPageBeneficiaryList;
        // this.dashService.getBeneficiaries();
        this.myDebitableAccounts = dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.debitAllowed == true; });
        //  this.myAccounts = this.dashService.getMyAccounts();
        //Create dynamic form controls for each beneficiary child component.
        this._formGroup = this.dashService.beneficiaryToFormGroup(this.beneficiaries);
        this._formGroup.controls['numberofScheduleFrequency'] = new forms_1.FormControl('');
        this._formGroup.controls['paymentDate'] = new forms_1.FormControl('');
        // get country codes
        this.countryCodes = this.dashService.getCountryCodes();
        this.selectedAccountOptionSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            // optionKey is accounNumber on dropdown since its unique per account 
            var optionKey = object.optionKey;
            var dropdownIndex = object.index;
            // Account dropdown
            if (dropdownIndex == 1) {
                var selectedAccount = self.myDebitableAccounts.filter(function (a) { return a.accountNumber == optionKey; })[0];
                //this.fromAccountSelected = selectedAccount.accountName + "-" + selectedAccount.accountNumber;
                self.availableBalance = selectedAccount.availableBalance;
                self.transactionCurrency = selectedAccount.currency.toUpperCase();
                self.selectedAccountNo = selectedAccount.accountNumber;
                self.selectedAccountName = selectedAccount.accountName;
                if (selectedAccount.currency.toLowerCase() == "ngn") {
                    self.applyCharges = true;
                }
                else {
                    self.applyCharges = false;
                }
                if (!self.applyCharges) {
                    for (var _i = 0, _a = _this.amountBeneficiaryHash; _i < _a.length; _i++) {
                        var account = _a[_i];
                        account.charges = 0;
                        // update last charges on each beneficiary sub view.
                        event_handlers_service_1.EventHandlerService.emitLastAmountAndChargesOnBeneficiaryEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer, account.amount, account.charges, account.beneficiaryAccountNo);
                    }
                }
                else {
                    for (var _b = 0, _c = _this.amountBeneficiaryHash; _b < _c.length; _b++) {
                        var account = _c[_b];
                        if (account.bankCode.toLowerCase() != appsettings_constant_1.Appsettings.LOCAL_BANK_CODE.toLowerCase()) {
                            account.charges = _this.otherBankCharges;
                            event_handlers_service_1.EventHandlerService.emitLastAmountAndChargesOnBeneficiaryEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer, account.amount, account.charges, account.beneficiaryAccountNo);
                        }
                    }
                }
                self.totalAmountEnteredSoFar = self.amountBeneficiaryHash.map(function (x) { return x.amount; }).reduce(add, 0);
                self.totalChargesSoFar = self.amountBeneficiaryHash.map(function (x) { return x.charges; }).reduce(add, 0);
                self.totalAmountOnView = self.totalAmountEnteredSoFar + self.totalChargesSoFar;
                self.totalAmountLessChargeForScheduleView = self.totalAmountEnteredSoFar;
                if (_this.totalAmountOnView > _this.availableBalance) {
                    _this.amountErrorOnView = true;
                }
                else {
                    _this.amountErrorOnView = false;
                }
                if (!_this.totalAmountOnView) {
                    if (_this.totalAmountOnView > (_this.availableLimit - _this.amountUtilizedFromLimit)) {
                        _this.amountErrorOnView = true;
                    }
                    else {
                        _this.amountErrorOnView = false;
                    }
                }
                console.log("Current amount charges map after change....");
                console.log(_this.amountBeneficiaryHash);
            }
            else if (dropdownIndex == 2) {
                var selectedFrequency = self.frequencyTypes.filter(function (x) { return x.Id.toString() == optionKey; })[0];
                if (selectedFrequency) {
                    self.selectedScheduleFrequency = selectedFrequency.Id;
                    self.resetNumberOfScheduleFrequency(self.selectedScheduleFrequency);
                    // if(self.selectedScheduleFrequency == 1){
                    //     self.payBeneFormGroup.controls['numberofScheduleFrequency'] = undefined;
                    //     self.textInputActive = "";
                    // }  
                    self.computeSchedulePaymentBreakdown();
                }
            }
        });
        this.totalAmountOnViewSUb = event_handlers_service_1.EventHandlerService.TotalAmountOnViewEventPublisher
            .subscribe(function (object) {
            var componentType = object.forComponent;
            var amount = object.amount;
            var charges = object.charges;
            if (componentType == component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer) {
                //  alert(amount);
                self.totalAmountOnView = amount;
                self.totalAmountLessChargeForScheduleView = amount - charges;
                if (_this.totalAmountOnView > _this.availableBalance) {
                    _this.amountErrorOnView = true;
                }
                else {
                    _this.amountErrorOnView = false;
                }
                if (!_this.totalAmountOnView) {
                    if (_this.totalAmountOnView > (_this.availableLimit - _this.amountUtilizedFromLimit)) {
                        _this.amountErrorOnView = true;
                    }
                    else {
                        _this.amountErrorOnView = false;
                    }
                }
            }
        });
        this.transferSub = event_handlers_service_1.EventHandlerService.BeneficiaryPaymentAmountEventPublisher
            .subscribe(function (object) {
            var componentType = object.forComponent;
            if (componentType == component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer) {
                var amountEntered = object.amountEntered;
                var chargesOnAmount = object.chargesOnAmount;
                var beneficiaryAccountNo_1 = object.beneficiaryAccountNo;
                var beneficiaryAmount = _this.amountBeneficiaryHash.filter(function (x) { return x.beneficiaryAccountNo.toLowerCase() == beneficiaryAccountNo_1.toLowerCase(); })[0];
                if (beneficiaryAmount != undefined && beneficiaryAmount != null) {
                    beneficiaryAmount.amount = amountEntered;
                    beneficiaryAmount.charges = chargesOnAmount;
                    beneficiaryAmount.bankCode = object.beneficiaryBankCode;
                }
                else {
                    _this.amountBeneficiaryHash.push({
                        amount: amountEntered,
                        beneficiaryAccountNo: beneficiaryAccountNo_1,
                        charges: chargesOnAmount,
                        bankCode: object.beneficiaryBankCode
                    });
                }
                console.log(_this.amountBeneficiaryHash);
                _this.totalAmountEnteredSoFar = _this.amountBeneficiaryHash.map(function (x) { return x.amount; }).reduce(add, 0);
                _this.totalChargesSoFar = _this.amountBeneficiaryHash.map(function (x) { return x.charges; }).reduce(add, 0);
                console.log("Total Amount entered so far: " + _this.totalAmountEnteredSoFar);
                console.log("Total charges so far: " + _this.totalChargesSoFar);
                if ((+_this.totalAmountEnteredSoFar + +_this.totalChargesSoFar) <= _this.availableBalance) {
                    console.log("Clear insufficient balance");
                    event_handlers_service_1.EventHandlerService.emitClearInsufficientFundErrorEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer);
                }
                if ((+_this.totalAmountEnteredSoFar) <= _this.availableLimit) {
                    event_handlers_service_1.EventHandlerService.emitClearAvailableLimitErrorEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.AmountTransfer);
                }
            }
        });
    };
    PayBeneficiariesComponent.prototype.selectMyAccount = function (idx, event) {
        event.stopPropagation();
        this.fromAccountSelected = this.myDebitableAccounts[idx].accountName + "-" + this.myDebitableAccounts[idx].accountNumber;
        this.availableBalance = this.myDebitableAccounts[idx].availableBalance;
        this.fromAccountSelectIsEmpty = false;
        this.fromAccountSelectIsOpen = false;
        return false;
    };
    PayBeneficiariesComponent.prototype.setActiveAccount = function (idx) {
        this.myAccountHoveredIndex = idx;
    };
    PayBeneficiariesComponent.prototype.toggleMyAccountSelectClick = function (event) {
        event.stopPropagation();
        this.fromAccountSelectIsOpen = !this.fromAccountSelectIsOpen;
        return false;
    };
    PayBeneficiariesComponent.prototype.backToBeneficiaryList = function () {
        // this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
        return false;
    };
    PayBeneficiariesComponent.prototype.CloseTranfer = function () {
        this.cancelModalOpened = true;
    };
    PayBeneficiariesComponent.prototype.hideCancelTransferModal = function () {
        this.cancelModalOpened = false;
    };
    PayBeneficiariesComponent.prototype.GotoReview = function (event) {
        event.stopPropagation();
        // this.pageSubtitle = "Review";
        //this.pageIndex = 2;
        //console.log(this.amountBeneficiaryHash);
        //console.log(this.payBeneFormGroup);
        var errors = [];
        var _loop_1 = function(obj) {
            var hasError = false;
            if (this_1._formGroup.controls[obj.beneficiaryId + "_errorOnView"].value == true) {
                hasError = true;
            }
            var amountEntry = this_1.amountBeneficiaryHash.filter(function (c) { return c.beneficiaryAccountNo.toLowerCase() == obj.beneficiaryAccountNumber.toLowerCase(); })[0];
            if (!hasError && !amountEntry) {
                event_handlers_service_1.EventHandlerService.emitTrasferAmountErrorEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.BeneficiaryTransfer, obj.beneficiaryAccountNumber, true);
                hasError = true;
            }
            else if (!hasError && amountEntry && (!amountEntry.amount || (amountEntry.amount && amountEntry.amount < 1))) {
                hasError = true;
                event_handlers_service_1.EventHandlerService.emitTrasferAmountErrorEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.BeneficiaryTransfer, obj.beneficiaryAccountNumber, true);
            }
            else {
                event_handlers_service_1.EventHandlerService.emitTrasferAmountErrorEvent(component_event_publisher_enum_1.ComponentEventPublisherEnum.BeneficiaryTransfer, obj.beneficiaryAccountNumber, false);
            }
            if (!hasError && obj.beneficiaryBankCode != appsettings_constant_1.Appsettings.LOCAL_BANK_CODE && !this_1.applyCharges) {
                hasError = true;
            }
            if (hasError) {
                errors.push(true);
            }
        };
        var this_1 = this;
        for (var _i = 0, _a = this.beneficiaries; _i < _a.length; _i++) {
            var obj = _a[_i];
            _loop_1(obj);
        }
        if (errors.length < 1) {
            var _loop_2 = function(obj) {
                var _ben = this_2.beneficiaries.filter(function (a) { return a.beneficiaryAccountNumber == obj.beneficiaryAccountNo; })[0];
                obj.beneficiaryRef = this_2._formGroup.controls[_ben.beneficiaryId + "_beneficiaryRef"].value;
                obj.beneficiaryCountryCode = this_2._formGroup.controls[_ben.beneficiaryId + "_beneficiaryCountryCodeSelected"].value;
                obj.beneficiaryEmail = this_2._formGroup.controls[_ben.beneficiaryId + "_beneficiaryEmail"].value;
                obj.beneficiaryId = _ben.beneficiaryId;
                obj.beneficiaryPhoneNo = this_2._formGroup.controls[_ben.beneficiaryId + "_beneficiaryPhoneNo"].value;
                obj.myReference = this_2._formGroup.controls[_ben.beneficiaryId + "_beneficiaryCustomerRef"].value;
                obj.beneficiaryAlias = _ben.beneficiaryAlias;
                obj.beneficiaryBankName = _ben.beneficiaryBank;
                obj.beneficiaryName = _ben.beneficiaryName;
                obj.beneficiaryAccountName = _ben.beneficiaryAccountName;
            };
            var this_2 = this;
            for (var _b = 0, _c = this.amountBeneficiaryHash; _b < _c.length; _b++) {
                var obj = _c[_b];
                _loop_2(obj);
            }
            this.pageSubtitle = "Review";
            this.pageIndex = 2;
        }
        return false;
    };
    PayBeneficiariesComponent.prototype.backToDetails = function (event) {
        event.stopPropagation();
        this.pageSubtitle = "Details";
        this.pageIndex = 1;
        return false;
    };
    PayBeneficiariesComponent.prototype.pay = function (event) {
        event.stopPropagation();
        this.pageSubtitle = "Receipt";
        this.pageIndex = 3;
        return false;
    };
    PayBeneficiariesComponent.prototype.backHome = function () {
        // this.ngOnDestroy();
        this.router.navigate(['dashboard']);
    };
    PayBeneficiariesComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    PayBeneficiariesComponent.prototype.logout = function () {
        //this.ngOnDestroy();
        this.authService.logout();
        this.router.navigate(['login']);
    };
    PayBeneficiariesComponent.prototype.ngOnDestroy = function () {
        if (this.selectedAccountOptionSub) {
            this.selectedAccountOptionSub.unsubscribe();
        }
        if (this.totalAmountOnViewSUb) {
            this.totalAmountOnViewSUb.unsubscribe();
        }
        if (this.transferSub) {
            this.transferSub.unsubscribe();
        }
    };
    PayBeneficiariesComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.cdRef.detectChanges();
    };
    PayBeneficiariesComponent.prototype.textInputBlur = function (formControlName) {
        if (!this._formGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
    };
    PayBeneficiariesComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/beneficiary/pay-beneficiary.modal.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], PayBeneficiariesComponent);
    return PayBeneficiariesComponent;
    var _a, _b, _c, _d;
}(schedule_payment_base_component_1.SchedulePaymentBaseComponent));
exports.PayBeneficiariesComponent = PayBeneficiariesComponent;
//# sourceMappingURL=beneficiary-payment.component.js.map