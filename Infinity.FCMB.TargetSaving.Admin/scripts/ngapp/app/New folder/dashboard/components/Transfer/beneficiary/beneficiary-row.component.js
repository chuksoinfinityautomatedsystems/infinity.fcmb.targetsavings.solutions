"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var utils_services_1 = require("../../../../commons/services/utils.services");
var beneficiaries_model_1 = require("../../../models/beneficiaries.model");
function _window() {
    // return the global native browser window object
    return window;
}
var BeneficiaryRowComponent = (function () {
    function BeneficiaryRowComponent(cdRef, utilService) {
        this.cdRef = cdRef;
        this.utilService = utilService;
        this.selected = false;
    }
    BeneficiaryRowComponent.prototype.isSelected = function () {
        this.selected = !this.selected;
        this.cdRef.detectChanges();
    };
    BeneficiaryRowComponent.prototype.viewBeneficiaryDetails = function () {
        // event.stopPropagation();
        //  EventHandlerService.emitObjForDetails(this.obj, ComponentEventPublisherEnum.Log);
        return false;
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', (typeof (_a = typeof beneficiaries_model_1.Beneficiary !== 'undefined' && beneficiaries_model_1.Beneficiary) === 'function' && _a) || Object)
    ], BeneficiaryRowComponent.prototype, "obj", void 0);
    BeneficiaryRowComponent = __decorate([
        core_1.Component({
            selector: "ng-beneficiary-row",
            templateUrl: "html/home/transfer/beneficiary/beneficiary-row.html"
        }), 
        __metadata('design:paramtypes', [core_1.ChangeDetectorRef, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object])
    ], BeneficiaryRowComponent);
    return BeneficiaryRowComponent;
    var _a, _b;
}());
exports.BeneficiaryRowComponent = BeneficiaryRowComponent;
//# sourceMappingURL=beneficiary-row.component.js.map