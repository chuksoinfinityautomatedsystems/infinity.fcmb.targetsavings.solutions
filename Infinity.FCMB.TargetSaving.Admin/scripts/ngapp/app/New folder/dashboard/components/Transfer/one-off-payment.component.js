"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../services/dashboard.services");
var utils_services_1 = require("../../../commons/services/utils.services");
var constants_services_1 = require("../../../commons/services/constants.services");
var authentication_service_1 = require("../../../authentication/services/authentication.service");
var common_1 = require('@angular/common');
var event_handlers_service_1 = require("../../../commons/services/event_handlers.service");
var appsettings_constant_1 = require("../../../commons/constants/appsettings.constant");
var schedule_payment_base_component_1 = require("../../../commons/components/schedule_payment.base.component");
var response_codes_constant_1 = require("../../../commons/constants/response_codes.constant");
function _window() {
    // return the global native browser window object
    return window;
}
var OneOffPaymentComponent = (function (_super) {
    __extends(OneOffPaymentComponent, _super);
    function OneOffPaymentComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef, decimalPipe) {
        _super.call(this, authService, router, cdRef);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        this.decimalPipe = decimalPipe;
        document.title = "STANBIC IBTC BANK";
        this._formGroup = formBuilder.group({
            amount: ['', forms_1.Validators.required],
            beneficiaryName: ['', forms_1.Validators.required],
            accountNo: ['', forms_1.Validators.required],
            myRef: [''],
            theirRef: [''],
            beneficiaryEmail: [''],
            notificationPhoneNo: [''],
            countryCode: [''],
            bankCode: ['', forms_1.Validators.required],
            beneficiaryAccountName: [''],
            chargeBeneficiary: [false],
            numberofScheduleFrequency: [''],
            paymentDate: ['']
        });
        this.availableLimit = 1000000;
        this.availableBalance = 100000;
        this.otherBankCharges = 52.50;
        this.amountUtilizedFromLimit = 20000;
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.notifyBeneficiaryAboutPayment = false;
        this.currenyLocal = true;
        this.showTransactionLimit = false;
        this.withinDailyLimit = true;
        this.totalAmountOnView = this.totalAmountLessChargeForScheduleView = 0;
        this.amountErrorOnView = false;
        this.cancelModalOpened = false;
        this.selectBankOptionOnLoad = false;
        this.selectBankOptionIndex = 0;
        //SCHEDULE PAYMENT
        this.frequencyTypes = appsettings_constant_1.Appsettings.SCHEDUE_PAYMENT_FREQUENCY_TYPES;
        this.numberOfPayments = 0;
        this.todaySelectedOnJustOnceSchedule = false;
    }
    OneOffPaymentComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this.filteredCountryCodes = this.dashService.getCountryCodes();
        this.filteredBanks = this.dashService.getBanks();
        this.filteredCountryCodes = this.sortedCountryCodes();
        this.filteredBanks = this.sortedBanks();
        if (dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject) {
            if (dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.beneficiaryAlias) {
                this._formGroup.controls["beneficiaryName"].setValue(dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.beneficiaryAlias);
            }
            if (dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.beneficiaryAccountNumber) {
                this._formGroup.controls["accountNo"].setValue(dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.beneficiaryAccountNumber);
            }
            if (dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.customerReference) {
                this._formGroup.controls["myRef"].setValue(dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.customerReference);
            }
            if (dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.beneficiaryReference) {
                this._formGroup.controls["theirRef"].setValue(dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.beneficiaryReference);
            }
            if (dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.beneficiaryBankCode) {
                this._formGroup.controls["bankCode"].setValue(dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.beneficiaryBankCode);
                var selectedBankOnLoad = this.filteredBanks.filter(function (c) { return c.code.toLowerCase() == dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject.beneficiaryBankCode.toLowerCase(); })[0];
                this.selectBankOptionIndex = this.filteredBanks.indexOf(selectedBankOnLoad);
                this.selectBankOptionOnLoad = true;
            }
        }
        this.myDebitableAccounts = dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.debitAllowed == true; });
        this.selectedAccountOptionSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            // optionKey is accounNumber on dropdown since its unique per account 
            var optionKey = object.optionKey;
            var index = object.index;
            if (index == 1) {
                self.otherBankNonLocalChargesError = false;
                var selectedAccount = self.myDebitableAccounts.filter(function (a) { return a.accountNumber == optionKey; })[0];
                //this.fromAccountSelected = selectedAccount.accountName + "-" + selectedAccount.accountNumber;
                self.availableBalance = selectedAccount.availableBalance;
                self.transactionCurrency = selectedAccount.currency.toUpperCase();
                self.selectedAccountNo = selectedAccount.accountNumber;
                self.selectedAccountName = selectedAccount.accountName;
                if (selectedAccount.currency.toLowerCase() == "ngn") {
                    self.applyCharges = true;
                }
                else {
                    self.applyCharges = false;
                }
            }
            else if (index == 2) {
                // var selectedBank = self.filteredBanks.filter(a => a.code == optionKey)[0]; 
                if (optionKey != appsettings_constant_1.Appsettings.LOCAL_BANK_CODE) {
                    // add charges
                    if (!self.lastBankCodeSelected || self.lastBankCodeSelected == appsettings_constant_1.Appsettings.LOCAL_BANK_CODE) {
                        _this.reCalculateAmountOnBankChange('1');
                    }
                    self.showCharges = true;
                    _this._formGroup.controls["chargeBeneficiary"].setValue(true);
                }
                else {
                    // remove charges#
                    //  this.reCalculateAmountOnBankChange('2');
                    if (self.lastBankCodeSelected != optionKey) {
                        _this.reCalculateAmountOnBankChange('2');
                    }
                    self.showCharges = false;
                    _this._formGroup.controls["chargeBeneficiary"].setValue(false);
                }
                _this._formGroup.controls["bankCode"].setValue(optionKey);
                self.bankNameSelected = self.filteredBanks.filter(function (c) { return c.code == optionKey; })[0].name;
                self.lastBankCodeSelected = optionKey;
            }
            else if (index == 3) {
                _this._formGroup.controls["countryCode"].setValue(optionKey);
                self.countryHasError = false;
            }
            else if (index == 4) {
                var selectedFrequency = self.frequencyTypes.filter(function (x) { return x.Id.toString() == optionKey; })[0];
                if (selectedFrequency) {
                    self.selectedScheduleFrequency = selectedFrequency.Id;
                    self.resetNumberOfScheduleFrequency(self.selectedScheduleFrequency);
                    self.computeSchedulePaymentBreakdown();
                }
            }
        });
    };
    // CONTROL CUSTOMER NOTIFICATION VISIBILITY    
    OneOffPaymentComponent.prototype.enableCustomerNotification = function (mode) {
        this.notifyBeneficiaryAboutPayment = true;
        if (mode == 1) {
            this.showEmailNotificationField = false; // show sms field
            this._formGroup.controls["beneficiaryEmail"].setValue(undefined);
        }
        else {
            this.showEmailNotificationField = true;
            this._formGroup.controls["notificationPhoneNo"].setValue(undefined);
            this._formGroup.controls["countryCode"].setValue(undefined);
            event_handlers_service_1.EventHandlerService.emitClearCountryDropdownEvent();
        }
        this.textInputActive = '';
        this.cdRef.detectChanges();
    };
    OneOffPaymentComponent.prototype.disableCustomerNotification = function () {
        this.notifyBeneficiaryAboutPayment = false;
        this.showEmailNotificationField = false;
        this._formGroup.controls["beneficiaryEmail"].setValue(undefined);
        this._formGroup.controls["notificationPhoneNo"].setValue(undefined);
        this._formGroup.controls["countryCode"].setValue(undefined);
        this.textInputActive = '';
        event_handlers_service_1.EventHandlerService.emitClearCountryDropdownEvent();
        this.cdRef.detectChanges();
        // this.beneficiaryEmail = this.notificationPhoneNo = undefined;
        // this.countrySelectIsEmpty = true;
        // this.countrySelectIsOpen = false;
        // this.countryCodeSelected = '';
        // this.countryCodeHoveredIndex = 0;
    };
    OneOffPaymentComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.countryHasError = this.invalidAccountNumber = this.otherBankNonLocalChargesError = false;
        this.formSubmitted = false;
        if (formControlName == "amount") {
            //this.amountRequiredError = false;
            this.showTransactionLimit = true;
            this.withinDailyLimit = true;
            this.insufficientFund = false;
            //   this.firstKeyUpPassedAfterAmountFocus = false;
            if (this._formGroup.controls["amount"]) {
                var amountEntered = this._formGroup.controls["amount"].value;
                if (amountEntered) {
                    //remove .00 if exist
                    if (amountEntered.indexOf('.00') != -1) {
                        amountEntered = amountEntered.replace('.00', '');
                    }
                    // Filter non-digits from input value.
                    if (/\D/g.test(amountEntered)) {
                        amountEntered = amountEntered.replace(/\D/g, '');
                    }
                }
                this._formGroup.controls["amount"].setValue(amountEntered);
            }
        }
        this.cdRef.detectChanges();
    };
    OneOffPaymentComponent.prototype.textInputBlur = function (formControlName) {
        if (!this._formGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
        if (formControlName == "amount") {
            this.showTransactionLimit = false;
            var transferCharges = 0;
            var amount = 0;
            if (this._formGroup.controls['' + formControlName]) {
                amount = this._formGroup.controls['' + formControlName].value;
            }
            // if (this.obj.beneficiaryBankCode.toLowerCase() != Appsettings.LOCAL_BANK_CODE &&
            //     this.applyCharges && amount > 0) {
            //     transferCharges = this.otherBankCharges;
            // }
            //  console.log("transferCharges in blur: " + transferCharges);
            // this.lastAmountOnBlur = amount;
            // this.lastTransferChargesOnBlur = transferCharges;
            this.cdRef.detectChanges();
            // Update amount entered and charges for beneficiary. This would have effect on the total charges and amount for all beneficiary on the view.
            // EventHandlerService.emitTransferAmountDetails(this.lastTransferChargesOnBlur, this.lastAmountOnBlur,
            //     ComponentEventPublisherEnum.AmountTransfer, this.obj.beneficiaryAccountNumber, this.obj.beneficiaryBankCode);
            if (amount > 0) {
                this._formGroup.controls['' + formControlName].setValue(this.decimalPipe.transform(amount, '.2'));
            }
        }
    };
    OneOffPaymentComponent.prototype.mobileNoInputKeyup = function () {
        var mobileNoEntered = this._formGroup.controls["notificationPhoneNo"].value;
        if (mobileNoEntered && /\D/g.test(mobileNoEntered)) {
            // Filter non-digits from input value.
            mobileNoEntered = mobileNoEntered.replace(/\D/g, '');
        }
        this._formGroup.controls["notificationPhoneNo"].setValue(mobileNoEntered);
    };
    //SELECT
    OneOffPaymentComponent.prototype.sortedBanks = function () {
        return this.filteredBanks.sort(function (a, b) {
            if (a.name < b.name)
                return -1;
            if (a.name > b.name)
                return 1;
            return 0;
        });
    };
    OneOffPaymentComponent.prototype.sortedCountryCodes = function () {
        return this.filteredCountryCodes.sort(function (a, b) {
            if (a.countryName < b.countryName)
                return -1;
            if (a.countryName > b.countryName)
                return 1;
            return 0;
        });
    };
    OneOffPaymentComponent.prototype.navigateToPaymentsConfirmation = function () {
    };
    OneOffPaymentComponent.prototype.amountInputKeyup = function () {
        var amount = 0.0;
        //   let errorOnAmountField = false;      
        // this.payOneOffFormGroup.controls["amount"].setValue(false);
        // Only allow digits on amount alone.        
        if (this._formGroup.controls["amount"]) {
            var amountEntered = this._formGroup.controls["amount"].value;
            if (amountEntered && /\D/g.test(amountEntered)) {
                // Filter non-digits from input value.
                amountEntered = amountEntered.replace(/\D/g, '');
            }
            this._formGroup.controls["amount"].setValue(amountEntered);
            amount = parseFloat(amountEntered);
        }
        if (this.showCharges && amount > 0) {
            var totalAmount = this.totalAmountOnView = this.totalAmountLessChargeForScheduleView = +amount; // + +this.otherBankCharges; 
            if (this._formGroup.controls["chargeBeneficiary"] && this._formGroup.controls["chargeBeneficiary"].value == true) {
                totalAmount = this.totalAmountOnView = +totalAmount + +this.otherBankCharges;
            }
            if (totalAmount > this.availableBalance) {
                this.insufficientFund = true;
                this.amountErrorOnView = true;
            }
            else {
                this.insufficientFund = false;
                this.amountErrorOnView = false;
            }
        }
        else {
            this.totalAmountOnView = amount;
            this.totalAmountLessChargeForScheduleView = amount;
            if (amount > this.availableBalance) {
                this.insufficientFund = true;
                this.amountErrorOnView = true;
            }
            else {
                this.insufficientFund = false;
                this.amountErrorOnView = false;
            }
        }
        if (amount > (+this.availableLimit - +this.amountUtilizedFromLimit)) {
            this.withinDailyLimit = false;
            if (!this.amountErrorOnView) {
                this.amountErrorOnView = true;
            }
        }
        else {
            this.withinDailyLimit = true;
        }
    };
    OneOffPaymentComponent.prototype.reCalculateAmountOnBankChange = function (addOrRemoveCharges) {
        //   let errorOnAmountField = false;      
        // this.payOneOffFormGroup.controls["amount"].setValue(false);
        // Only allow digits on amount alone.     
        var amountEntered = this.totalAmountOnView;
        var _amountEntered = '';
        if (amountEntered > 0) {
            if (addOrRemoveCharges == '1') {
                amountEntered = amountEntered + this.otherBankCharges;
            }
            else {
                amountEntered = amountEntered - this.otherBankCharges;
            }
            this.totalAmountOnView = amountEntered;
        }
    };
    // FLOW NAVIGATION
    OneOffPaymentComponent.prototype.pay = function (event) {
        event.stopPropagation();
        this.pageSubtitle = "Receipt";
        this.pageIndex = 3;
        return false;
    };
    OneOffPaymentComponent.prototype.hideCancelTransferModal = function () {
        this.cancelModalOpened = false;
    };
    OneOffPaymentComponent.prototype.CloseTranfer = function () {
        this.cancelModalOpened = true;
    };
    OneOffPaymentComponent.prototype.backHome = function () {
        //this.ngOnDestroy();
        this.router.navigate(['dashboard']);
    };
    OneOffPaymentComponent.prototype.backToBeneficiaryList = function () {
        //this.ngOnDestroy();
        if (dashboard_services_1.DashboardService.BeneficiaryOrBillerDetailsToPaymentPage == true) {
            this.router.navigate(['MainView/PayTransfer/Recipient-details']);
        }
        else {
            this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
        }
        return false;
    };
    OneOffPaymentComponent.prototype.GotoReview = function (event) {
        event.stopPropagation();
        var self = this;
        this.formSubmitted = true;
        if (self._formGroup.valid) {
            var accountNo = self._formGroup.controls['accountNo'].value;
            var mobileNo = self._formGroup.controls['notificationPhoneNo'].value;
            var countryCode = self._formGroup.controls['countryCode'].value;
            var bankCode = self._formGroup.controls['bankCode'].value;
            var otherViewErrorCount = 0;
            if (accountNo && utils_services_1.UtilService.trim(accountNo).length > appsettings_constant_1.Appsettings.MINIMUM_ACCOUNT_NO_LENGTH) {
                self.invalidAccountNumber = true;
                otherViewErrorCount += 1;
            }
            else {
                self.invalidAccountNumber = false;
            }
            if (mobileNo && utils_services_1.UtilService.trim(mobileNo).length > 0 && !countryCode) {
                self.countryHasError = true;
                otherViewErrorCount += 1;
            }
            else {
                self.countryHasError = false;
            }
            // charges only applies if the debitable account is local.
            if (!self.applyCharges && bankCode != appsettings_constant_1.Appsettings.LOCAL_BANK_CODE) {
                self.otherBankNonLocalChargesError = true;
                otherViewErrorCount += 1;
            }
            else {
                self.otherBankNonLocalChargesError = false;
            }
            self.cdRef.detectChanges();
            if (otherViewErrorCount > 0) {
            }
            else {
                // Do Name  Enquiry.
                self.showBusyLoader = true;
                self.cdRef.detectChanges();
                if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                    var loadingModalInterval_1 = setInterval(function () {
                        self.showBusyLoader = false;
                        self.cdRef.detectChanges();
                        self._doNameEnquiry();
                        _window().clearInterval(loadingModalInterval_1);
                    }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
                }
                else {
                    self._doNameEnquiry();
                }
            }
        }
        else {
            console.log(self._formGroup);
        }
        return false;
    };
    OneOffPaymentComponent.prototype._doNameEnquiry = function () {
        var _this = this;
        var self = this;
        self.doNameEnquirySub = self.dashService.doNameEnquiry(authentication_service_1.AuthenticationService.authUserObj.UserId, self._formGroup.controls['accountNo'].value, self._formGroup.controls['bankCode'].value)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                alert('Name enquiry success');
                self._formGroup.controls['beneficiaryAccountName'].setValue(response.ResponseFriendlyMessage);
                _this.pageIndex = 2;
                _this.pageSubtitle = "Review";
                self.cdRef.detectChanges();
            }
            else {
                alert('Name enquiry error');
                self._formGroup.controls['beneficiaryAccountName'].setValue(undefined);
                _this.exceptionMessage = response.ResponseFriendlyMessage;
                _this.showExceptionPage = true;
                self.cdRef.detectChanges();
            }
        }, function (error) {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    OneOffPaymentComponent.prototype.closeExceptionModal = function () {
        this.exceptionMessage = '';
        this.showExceptionPage = false;
    };
    OneOffPaymentComponent.prototype.backToDetails = function (event) {
        event.stopPropagation();
        this.pageSubtitle = "Details";
        this.pageIndex = 1;
        return false;
    };
    OneOffPaymentComponent.prototype.ngOnDestroy = function () {
        if (this.selectedAccountOptionSub) {
            this.selectedAccountOptionSub.unsubscribe();
        }
        if (this.doNameEnquirySub) {
            this.doNameEnquirySub.unsubscribe();
        }
    };
    OneOffPaymentComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    OneOffPaymentComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/oneoff-payment.modal.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef, common_1.DecimalPipe])
    ], OneOffPaymentComponent);
    return OneOffPaymentComponent;
    var _a, _b, _c, _d;
}(schedule_payment_base_component_1.SchedulePaymentBaseComponent));
exports.OneOffPaymentComponent = OneOffPaymentComponent;
//# sourceMappingURL=one-off-payment.component.js.map