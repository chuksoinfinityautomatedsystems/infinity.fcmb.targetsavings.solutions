"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../services/dashboard.services");
var utils_services_1 = require("../../../commons/services/utils.services");
var constants_services_1 = require("../../../commons/services/constants.services");
var common_1 = require('@angular/common');
var event_handlers_service_1 = require("../../../commons/services/event_handlers.service");
var authentication_service_1 = require("../../../authentication/services/authentication.service");
var appsettings_constant_1 = require("../../../commons/constants/appsettings.constant");
var schedule_payment_base_component_1 = require("../../../commons/components/schedule_payment.base.component");
function _window() {
    // return the global native browser window object
    return window;
}
var SelfTransferComponent = (function (_super) {
    __extends(SelfTransferComponent, _super);
    function SelfTransferComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef, decimalPipe) {
        _super.call(this, authService, router, cdRef, formBuilder, appsettings_constant_1.Appsettings.OTP_REASON_CODE_PAY_BILLER);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        this.decimalPipe = decimalPipe;
        document.title = "STANBIC IBTC BANK";
        this._formGroup = formBuilder.group({
            amount: ['', forms_1.Validators.required],
            reference: ['', forms_1.Validators.required],
            numberofScheduleFrequency: [''],
            paymentDate: ['']
        });
        //this.availableBalance = 100000; 
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.currenyLocal = true;
        // this.showTransactionLimit = false; 
        this.totalAmountLessChargeForScheduleView = 0;
        this.cancelModalOpened = false;
        //SCHEDULE PAYMENT
        this.frequencyTypes = appsettings_constant_1.Appsettings.SCHEDUE_PAYMENT_FREQUENCY_TYPES;
        this.numberOfPayments = 0;
        this.todaySelectedOnJustOnceSchedule = false;
    }
    SelfTransferComponent.prototype.ngOnInit = function () {
        var self = this;
        self.todayOnRecipt = moment(new Date()).format('DD MMM YYYY'); // 30 March 2018
        this.myDebitableSourceAccounts = dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.debitAllowed == true; });
        this.myCreditableDestinationAccounts = new Array();
        this.selectedAccountOptionSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            // optionKey is accounNumber on dropdown since its unique per account 
            var optionKey = object.optionKey;
            var index = object.index;
            if (index == 1) {
                var selectedAccount = self.myDebitableSourceAccounts.filter(function (a) { return a.accountNumber == optionKey; })[0];
                if (selectedAccount) {
                    self.selectedSourceAccount = selectedAccount;
                    //self.availableBalance = selectedAccount.availableBalance; 
                    //   self.transactionCurrency = selectedAccount.currency.toUpperCase();
                    // self.selectedAccountNo = selectedAccount.accountNumber;
                    //  self.selectedAccountName = selectedAccount.accountName;
                    self.myCreditableDestinationAccounts = self.myDebitableSourceAccounts.filter(function (a) {
                        return a.creditAllowed == true &&
                            a.accountNumber.toLowerCase() != selectedAccount.accountNumber.toLowerCase() &&
                            (a.currency.toLowerCase() == appsettings_constant_1.Appsettings.LOCAL_CURRENCY.toLowerCase() ||
                                a.currency.toLowerCase() == selectedAccount.currency.toLowerCase());
                    });
                    //alert('clear to account dropdown.');
                    event_handlers_service_1.EventHandlerService.emitUnselectAccountDropdownEvent(2); // for destination account dropdown
                }
                else {
                    self.selectedSourceAccount = undefined;
                }
            }
            else if (index == 2) {
                var selectedAccount = self.myCreditableDestinationAccounts.filter(function (a) { return a.accountNumber == optionKey; })[0];
                if (selectedAccount) {
                    self.selectedDestinationAccount = selectedAccount;
                }
                else {
                    self.selectedDestinationAccount = undefined;
                }
            }
            else if (index == 3) {
                var selectedFrequency = self.frequencyTypes.filter(function (x) { return x.Id.toString() == optionKey; })[0];
                if (selectedFrequency) {
                    self.selectedScheduleFrequency = selectedFrequency.Id;
                    self.resetNumberOfScheduleFrequency(self.selectedScheduleFrequency);
                    self.computeSchedulePaymentBreakdown();
                }
            }
        });
    };
    SelfTransferComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        // this.countryHasError = this.invalidAccountNumber = this.otherBankNonLocalChargesError = false;
        this.formSubmitted = false;
        if (formControlName == "amount") {
            //this.amountRequiredError = false;
            //  this.showTransactionLimit = true;
            // this.withinDailyLimit = true;
            this.insufficientFund = false;
            //   this.firstKeyUpPassedAfterAmountFocus = false;
            if (this._formGroup.controls["amount"]) {
                var amountEntered = this._formGroup.controls["amount"].value;
                if (amountEntered) {
                    //remove .00 if exist
                    if (amountEntered.indexOf('.00') != -1) {
                        amountEntered = amountEntered.replace('.00', '');
                    }
                    // Filter non-digits from input value.
                    if (/\D/g.test(amountEntered)) {
                        amountEntered = amountEntered.replace(/\D/g, '');
                    }
                }
                this._formGroup.controls["amount"].setValue(amountEntered);
            }
        }
        this.cdRef.detectChanges();
    };
    SelfTransferComponent.prototype.textInputBlur = function (formControlName) {
        if (!this._formGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
        if (formControlName == "amount") {
            //  this.showTransactionLimit = false; 
            var amount = 0;
            if (this._formGroup.controls['' + formControlName]) {
                amount = this._formGroup.controls['' + formControlName].value;
            }
            this.cdRef.detectChanges();
            this.totalAmountLessChargeForScheduleView = amount;
            if (amount > 0) {
                this._formGroup.controls['' + formControlName].setValue(this.decimalPipe.transform(amount, '.2'));
            }
        }
    };
    SelfTransferComponent.prototype.amountInputKeyup = function () {
        var amount = 0.0;
        // Only allow digits on amount alone.        
        if (this._formGroup.controls["amount"]) {
            var amountEntered = this._formGroup.controls["amount"].value;
            if (amountEntered && /\D/g.test(amountEntered)) {
                // Filter non-digits from input value.
                amountEntered = amountEntered.replace(/\D/g, '');
            }
            this._formGroup.controls["amount"].setValue(amountEntered);
            amount = parseFloat(amountEntered);
        }
        this.totalAmountLessChargeForScheduleView = +amount;
    };
    //     this.pageIndex = 2;
    //     this.pageSubtitle = "Review";
    //    return false;
    SelfTransferComponent.prototype.nextToReview = function (event) {
        event.stopPropagation();
        var self = this;
        this.formSubmitted = true;
        if (self._formGroup.valid) {
            if (self.selectedSourceAccount && self.selectedSourceAccount.accountNumber &&
                self.selectedDestinationAccount && self.selectedDestinationAccount.accountNumber) {
                alert('success');
                this.pageIndex = 2;
                this.pageSubtitle = "Review";
            }
            self.cdRef.detectChanges();
        }
        else {
            alert('failure');
        }
        return false;
    };
    SelfTransferComponent.prototype.backToDetails = function (event) {
        event.stopPropagation();
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        return false;
    };
    SelfTransferComponent.prototype.goToReceipt = function (event) {
        event.stopPropagation();
        this.pageIndex = 3;
        this.pageSubtitle = "Receipt";
        return false;
    };
    SelfTransferComponent.prototype.hideCancelTransferModal = function () {
        this.cancelModalOpened = false;
    };
    SelfTransferComponent.prototype.CloseTranfer = function () {
        this.cancelModalOpened = true;
    };
    SelfTransferComponent.prototype.backHome = function () {
        this.router.navigate(['dashboard']);
    };
    SelfTransferComponent.prototype.GoBack = function () {
        if (this.pageIndex == 1) {
            this.router.navigate(['MainView/PayTransfer/Biller/List']);
        }
    };
    SelfTransferComponent.prototype.backToBeneficiary = function () {
        event.stopPropagation();
        this.router.navigate(['MainView/PayTransfer/Biller/List']);
        return false;
    };
    SelfTransferComponent.prototype.BuyAgain = function (event) {
        // clear all form fields
        event.stopPropagation();
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        return false;
    };
    SelfTransferComponent.prototype.buyAirtime = function (event) {
        event.stopPropagation();
        this.pageIndex = 3;
        this.pageSubtitle = "Receipt";
        return false;
    };
    SelfTransferComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    SelfTransferComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    SelfTransferComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/self-transfer.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef, common_1.DecimalPipe])
    ], SelfTransferComponent);
    return SelfTransferComponent;
    var _a, _b, _c, _d;
}(schedule_payment_base_component_1.SchedulePaymentBaseComponent));
exports.SelfTransferComponent = SelfTransferComponent;
//# sourceMappingURL=self-transfer.component.js.map