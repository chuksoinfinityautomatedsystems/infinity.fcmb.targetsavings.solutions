"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var utils_services_1 = require("../../../../commons/services/utils.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../../../commons/constants/response_codes.constant");
var dashboard_services_1 = require("../../../services/dashboard.services");
var base_component_1 = require("../../../../commons/components/base.component");
function _window() {
    // return the global native browser window object
    return window;
}
var BeneficiariesComponent = (function (_super) {
    __extends(BeneficiariesComponent, _super);
    function BeneficiariesComponent(authService, utiilService, router, formBuilder, dashService, cdRef) {
        _super.call(this, authService, router);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.screenHeight = utils_services_1.UtilService.getScreenHeight();
        //Miscellaneous
        this.hideMobilesearch = true;
        this.navigateGroups = false;
        this.hasForeignAccounts = false;
        // Beneficiaries
        this.beneficiaryIdList = new Array();
        this.beneficiaries = this.filteredBeneficiaries = Array();
        this.payButtonText = this.beneficiaryNameFilter = "";
    }
    BeneficiariesComponent.prototype.ngOnInit = function () {
        var self = this;
        self.showBusyLoader = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                self._getBeneficiaryList();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getBeneficiaryList();
        }
        // this.lastLogin = AuthenticationService.authUserObj.LastLoginIn;
        // self.SetupTimeout();  
    };
    BeneficiariesComponent.prototype._getBeneficiaryList = function () {
        var self = this;
        self.getBeneficiariesSub = self.dashService.getBeneficiaryList(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.beneficiaries = self.filteredBeneficiaries = response.Beneficiaries;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            //   self.showTransactionLoading = false; 
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            // self.showTransactionLoading = false;  
            self.cdRef.detectChanges();
        });
    };
    BeneficiariesComponent.prototype.beneficiaryIdSelected = function (idx) {
        if (this.beneficiaryIdList.indexOf(idx) != -1) {
            return true;
        }
        return false;
    };
    BeneficiariesComponent.prototype.amountTxtFocus = function () {
        this.amountTxtActive = true;
    };
    BeneficiariesComponent.prototype.amountTxtBlur = function () {
        if (!this.amountMe) {
            this.amountTxtActive = false;
        }
    };
    BeneficiariesComponent.prototype.isSelected = function (idx) {
        if (!this.beneficiaryIdList) {
            this.beneficiaryIdList = new Array();
        }
        if (this.beneficiaryIdList.indexOf(idx) != -1) {
            this.beneficiaryIdList.splice(this.beneficiaryIdList.indexOf(idx), 1);
        }
        else {
            this.beneficiaryIdList.push(idx);
        }
        if (this.beneficiaryIdList.length > 1) {
            this.payButtonText = "Pay " + this.beneficiaryIdList.length + " Selected";
        }
        else if (this.beneficiaryIdList && this.beneficiaryIdList.length == 1) {
            var beneficiaryName = this.filteredBeneficiaries[this.beneficiaryIdList[0]].beneficiaryName.toUpperCase();
            this.payButtonText = "Pay " + beneficiaryName;
        }
    };
    BeneficiariesComponent.prototype.filterBeneficiary = function () {
        var _this = this;
        console.log(this.beneficiaryIdList);
        this.beneficiaryIdList = undefined;
        if (this.beneficiaryNameFilter) {
            this.filteredBeneficiaries = this.beneficiaries.filter(function (x) { return x.beneficiaryName.toLowerCase().indexOf(_this.beneficiaryNameFilter.toLowerCase()) != -1; });
        }
        else {
            this.filteredBeneficiaries = this.beneficiaries;
        }
        console.log(this.beneficiaryIdList);
    };
    BeneficiariesComponent.prototype.sortedBeneficiaries = function () {
        return this.filteredBeneficiaries.sort(function (a, b) {
            if (a.beneficiaryName < b.beneficiaryName)
                return -1;
            if (a.beneficiaryName > b.beneficiaryName)
                return 1;
            return 0;
        });
    };
    BeneficiariesComponent.prototype.gotoBeneficiariesView = function () {
        this.router.navigate(['beneficiaries']);
    };
    BeneficiariesComponent.prototype.goHome = function () {
        this.router.navigate(['dashboard']);
    };
    BeneficiariesComponent.prototype.gotoBillersView = function () {
        this.router.navigate(['billers']);
    };
    BeneficiariesComponent.prototype.navigateToSelfTransferView = function () {
        this.router.navigate(['self-transfer']);
    };
    BeneficiariesComponent.prototype.navigateToInternationalPaymentView = function () {
    };
    BeneficiariesComponent.prototype.navigateToRemitaPaymentsView = function () {
        this.router.navigate(['remita-payment']);
    };
    BeneficiariesComponent.prototype.navigateToOnceOffBeneficiaryDetails = function () {
        event.stopPropagation();
        this.router.navigate(['OnceOffPayment']);
        return false;
    };
    BeneficiariesComponent.prototype.navigateToThisBeneficiaryDetails = function (event) {
        event.stopPropagation();
        this.router.navigate(['benefeciary-details']);
        return false;
    };
    BeneficiariesComponent.prototype.navigateToOneOffBeneficiaryDetails = function () {
        this.router.navigate(['benefeciary-details']);
        return false;
    };
    BeneficiariesComponent.prototype.navigateToOneOffWalletPayment = function () {
    };
    BeneficiariesComponent.prototype.navigateToMobileWalletView = function () {
    };
    BeneficiariesComponent.prototype.focusOnInput = function () {
        $('.input-type-textbox').focus();
        this.hideMobilesearch = false;
    };
    BeneficiariesComponent.prototype.groupFunction = function () {
    };
    BeneficiariesComponent.prototype.addBeneficiaryFunction = function () {
        this.router.navigate(['add-beneficiary']);
        //Add-Beneficiary   
    };
    BeneficiariesComponent.prototype.viewBeneficiaryDetails = function () {
        this.router.navigate(['recipient-details']);
    };
    BeneficiariesComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    BeneficiariesComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    BeneficiariesComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/beneficiary/beneficiaries.modal.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, core_1.ChangeDetectorRef])
    ], BeneficiariesComponent);
    return BeneficiariesComponent;
    var _a, _b, _c;
}(base_component_1.BaseComponent));
exports.BeneficiariesComponent = BeneficiariesComponent;
//# sourceMappingURL=beneficiaries.component - Copy.js.map