"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../../services/dashboard.services");
var utils_services_1 = require("../../../../commons/services/utils.services");
var constants_services_1 = require("../../../../commons/services/constants.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../../../commons/constants/response_codes.constant");
var base_component_1 = require("../../../../commons/components/base.component");
function _window() {
    // return the global native browser window object
    return window;
}
var BillerDetailsComponent = (function (_super) {
    __extends(BillerDetailsComponent, _super);
    function BillerDetailsComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef) {
        _super.call(this, authService, router, formBuilder, cdRef, appsettings_constant_1.Appsettings.OTP_REASON_CODE_REMOVE_BENEFICIARY);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.showPopup = false;
    }
    BillerDetailsComponent.prototype.ngOnInit = function () {
        dashboard_services_1.DashboardService.BeneficiaryOrBillerDetailsToPaymentPage = false;
        if (!dashboard_services_1.DashboardService.CrossBillerBeneficiaryObject) {
            this.router.navigate(['MainView/PayTransfer/Biller/List']);
        }
        this.obj = dashboard_services_1.DashboardService.CrossBillerBeneficiaryObject;
    };
    BillerDetailsComponent.prototype.EditBiller = function (event) {
        event.stopPropagation();
        this.router.navigate(['MainView/PayTransfer/Edit-Biller']);
    };
    BillerDetailsComponent.prototype.navigateToPayBeneficiary = function (event) {
        event.stopPropagation();
        dashboard_services_1.DashboardService.BeneficiaryOrBillerDetailsToPaymentPage = true;
        this.router.navigate(['MainView/PayTransfer/Biller/PayBillerDetails']);
    };
    BillerDetailsComponent.prototype.deleteBeneficiary = function (event) {
        event.stopPropagation();
        this.showPopup = true;
    };
    BillerDetailsComponent.prototype.cancelDelete = function (event) {
        event.stopPropagation();
        this.showPopup = false;
    };
    BillerDetailsComponent.prototype._initiateOTPRequest = function () {
        var _this = this;
        var self = this;
        self.isErrorToast = true;
        self.toastMessage = "";
        self.cdRef.detectChanges();
        self.initiateOtpSub = self.authService.initiateOTP(authentication_service_1.AuthenticationService.authUserObj.UserId, authentication_service_1.AuthenticationService.authUserObj.CifID, appsettings_constant_1.Appsettings.OTP_REASON_CODE_REMOVE_BENEFICIARY)
            .subscribe(function (initiateOtpResponse) {
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.showOtpView = true;
                self.showBusyLoader = false;
                self.showPopup = false;
            }
            else {
                _this.exceptionMessage = initiateOtpResponse.ResponseFriendlyMessage;
                _this.showExceptionPage = true;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            _this.exceptionMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            _this.showExceptionPage = true;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    BillerDetailsComponent.prototype.validateOTP = function (event) {
        event.stopPropagation();
        // validate otp and make payment
        return false;
    };
    BillerDetailsComponent.prototype.closeExceptionModal = function () {
        this.exceptionMessage = '';
        this.showExceptionPage = false;
    };
    BillerDetailsComponent.prototype.ngOnDestroy = function () {
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
    };
    BillerDetailsComponent.prototype.GoHome = function () {
        //this.ngOnDestroy();
        this.router.navigate(['dashboard']);
    };
    BillerDetailsComponent.prototype.GoBack = function () {
        // this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
    };
    BillerDetailsComponent.prototype.logout = function () {
        // this.ngOnDestroy();
        this.authService.logout();
        this.router.navigate(['login']);
    };
    BillerDetailsComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/biller/biller-details.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], BillerDetailsComponent);
    return BillerDetailsComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.BillerDetailsComponent = BillerDetailsComponent;
//# sourceMappingURL=biller-details.component.js.map