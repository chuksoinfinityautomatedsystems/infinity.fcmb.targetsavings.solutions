"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../../services/dashboard.services");
var utils_services_1 = require("../../../../commons/services/utils.services");
var constants_services_1 = require("../../../../commons/services/constants.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var base_component_1 = require("../../../../commons/components/base.component");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
function _window() {
    // return the global native browser window object
    return window;
}
var EditBillerComponent = (function (_super) {
    __extends(EditBillerComponent, _super);
    function EditBillerComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef) {
        _super.call(this, authService, router, formBuilder, cdRef, appsettings_constant_1.Appsettings.OTP_REASON_CODE_ADD_BENEFICIARY);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.selectedBiller = dashboard_services_1.DashboardService.CrossBillerBeneficiaryObject;
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.newBillerFormGroup = formBuilder.group({
            myReference: ['', forms_1.Validators.required],
            consumerCode: ['', forms_1.Validators.required],
            billerNickname: ['', forms_1.Validators.required]
        });
        this.cancelModalOpened = this.formSubmitted = false;
        this.textInputActive = '';
    }
    EditBillerComponent.prototype.ngOnInit = function () {
        var self = this;
        if (this.selectedBiller && this.selectedBiller.myReference && this.selectedBiller.myReference.length > 0) {
            self.newBillerFormGroup.controls['myReference'].setValue(this.selectedBiller.myReference);
        }
        if (this.selectedBiller && this.selectedBiller.consumerCode && this.selectedBiller.consumerCode.length > 0) {
            self.newBillerFormGroup.controls['consumerCode'].setValue(this.selectedBiller.consumerCode);
        }
        if (this.selectedBiller && this.selectedBiller.billerNickName && this.selectedBiller.billerNickName.length > 0) {
            self.newBillerFormGroup.controls['billerNickname'].setValue(this.selectedBiller.billerNickName);
        }
    };
    EditBillerComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.formSubmitted = false;
        this.cdRef.detectChanges();
    };
    EditBillerComponent.prototype.textInputBlur = function (formControlName) {
        if (!this.newBillerFormGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
    };
    EditBillerComponent.prototype.nextToReview = function (event) {
        event.stopPropagation();
        var self = this;
        this.formSubmitted = true;
        if (this.newBillerFormGroup.valid) {
            this.pageIndex = 2;
            this.pageSubtitle = "Review";
        }
        // biller nickname, biller name, customer code ref 3
        self.cdRef.detectChanges();
        return false;
    };
    EditBillerComponent.prototype.backToAddFormView = function (event) {
        event.stopPropagation();
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.cdRef.detectChanges();
    };
    EditBillerComponent.prototype.validateOTP = function (event) {
        event.stopPropagation();
        // validate otp and make payment
        this.router.navigate(['MainView/PayTransfer/Biller/List']);
        return false;
    };
    EditBillerComponent.prototype.backToBillerDetails = function (event) {
        event.stopPropagation();
        this.router.navigate(['MainView/Recipients/Biller/Details']);
        return false;
    };
    EditBillerComponent.prototype.backHome = function () {
        this.router.navigate(['dashboard']);
    };
    EditBillerComponent.prototype.closeExceptionModal = function () {
        this.showExceptionPage = false;
        this.cdRef.detectChanges();
        this.router.navigate(['dashboard']);
    };
    EditBillerComponent.prototype.hideCancelProcessModal = function () {
        this.cancelModalOpened = false;
    };
    EditBillerComponent.prototype.CloseModalProcess = function () {
        // only show warning modal on other pages except choose biller page.
        if (this.pageIndex > 1) {
            this.cancelModalOpened = true;
        }
        else {
            this.backHome();
        }
    };
    EditBillerComponent.prototype.ngOnDestroy = function () {
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
    };
    EditBillerComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    EditBillerComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    EditBillerComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/biller/edit-biller.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], EditBillerComponent);
    return EditBillerComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.EditBillerComponent = EditBillerComponent;
//# sourceMappingURL=edit-biller.component.js.map