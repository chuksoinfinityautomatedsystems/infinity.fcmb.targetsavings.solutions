"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../../services/dashboard.services");
var utils_services_1 = require("../../../../commons/services/utils.services");
var constants_services_1 = require("../../../../commons/services/constants.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../../../commons/constants/response_codes.constant");
var base_component_1 = require("../../../../commons/components/base.component");
function _window() {
    // return the global native browser window object
    return window;
}
var BeneficiaryDetailsComponent = (function (_super) {
    __extends(BeneficiaryDetailsComponent, _super);
    function BeneficiaryDetailsComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef) {
        _super.call(this, authService, router);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        document.title = "STANBIC IBTC BANK";
        this.showPopup = false;
        this.remitaFormGroup = formBuilder.group({});
        //OTP INIT
        this.otpFormGroup = formBuilder.group({
            otpModel1: ['', forms_1.Validators.required],
            otpModel2: ['', forms_1.Validators.required],
            otpModel3: ['', forms_1.Validators.required],
            otpModel4: ['', forms_1.Validators.required],
            otpModel5: ['', forms_1.Validators.required]
        });
        this.otpValues = ["", "", "", "", ""];
        this.showOtpError = this.otpFormSubmitted = this.showOtpView = this.showExceptionPage = false;
        this.hideOTPToast = true;
        this.isErrorToast = false;
        this.exceptionMessage = '';
    }
    BeneficiaryDetailsComponent.prototype.ngOnInit = function () {
        dashboard_services_1.DashboardService.BeneficiaryOrBillerDetailsToPaymentPage = false;
        if (!dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject) {
            this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
            return;
        }
        this.obj = dashboard_services_1.DashboardService.CrossBeneficiaryDetailsObject;
        var self = this;
        self.showBusyLoader = true;
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self.showBusyLoader = false;
                self._getBeneficiaryRecentPayments();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getBeneficiaryRecentPayments();
        }
    };
    BeneficiaryDetailsComponent.prototype._getBeneficiaryRecentPayments = function () {
        var self = this;
        self.getBeneficairyRecentTransactionSub = self.dashService.getBeneficiaryRecentPayment(authentication_service_1.AuthenticationService.authUserObj.UserId, this.obj.beneficiaryId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                if (!self.recentPayments) {
                    self.recentPayments = new Array();
                }
                self.recentPayments = response.Transactions;
            }
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    BeneficiaryDetailsComponent.prototype.navigateToPayBeneficiary = function (event) {
        event.stopPropagation();
        dashboard_services_1.DashboardService.BeneficiaryOrBillerDetailsToPaymentPage = true;
        // this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/OneOffPayment']);
    };
    BeneficiaryDetailsComponent.prototype.EditBeneficiary = function (event) {
        event.stopPropagation();
        //  this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Edit-Beneficiary']);
    };
    BeneficiaryDetailsComponent.prototype.deleteBeneficiary = function (event) {
        event.stopPropagation();
        this.showPopup = true;
    };
    BeneficiaryDetailsComponent.prototype.cancelDelete = function (event) {
        event.stopPropagation();
        this.showPopup = false;
    };
    BeneficiaryDetailsComponent.prototype.initiateRemovalOtp = function (event) {
        var self = this;
        self.showBusyLoader = true;
        self.cdRef.detectChanges();
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_2 = setInterval(function () {
                self._initiateOTPRequest();
                _window().clearInterval(loadingModalInterval_2);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._initiateOTPRequest();
        }
    };
    BeneficiaryDetailsComponent.prototype._initiateOTPRequest = function () {
        var _this = this;
        var self = this;
        self.isErrorToast = true;
        self.toastMessage = "";
        self.cdRef.detectChanges();
        self.initiateOtpSub = self.authService.initiateOTP(authentication_service_1.AuthenticationService.authUserObj.UserId, authentication_service_1.AuthenticationService.authUserObj.CifID, appsettings_constant_1.Appsettings.OTP_REASON_CODE_REMOVE_BENEFICIARY)
            .subscribe(function (initiateOtpResponse) {
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.showOtpView = true;
                self.showBusyLoader = false;
                self.showPopup = false;
            }
            else {
                _this.exceptionMessage = initiateOtpResponse.ResponseFriendlyMessage;
                _this.showExceptionPage = true;
            }
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            _this.exceptionMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            _this.showExceptionPage = true;
            self.cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    BeneficiaryDetailsComponent.prototype.closeExceptionModal = function () {
        this.exceptionMessage = '';
        this.showExceptionPage = false;
    };
    BeneficiaryDetailsComponent.prototype.reSendOtp = function () {
        var self = this;
        //re-init toast container
        self.hideOTPToast = true;
        self.toastMessage = "";
        self.isErrorToast = true;
        self.showBusyLoader = true;
        self.cdRef.detectChanges();
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_3 = setInterval(function () {
                self.showBusyLoader = false;
                self.cdRef.detectChanges();
                self._resendOTP();
                _window().clearInterval(loadingModalInterval_3);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._resendOTP();
        }
    };
    BeneficiaryDetailsComponent.prototype._resendOTP = function () {
        var self = this;
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
        self.initiateOtpSub = self.authService.initiateOTP(authentication_service_1.AuthenticationService.authUserObj.UserId, authentication_service_1.AuthenticationService.authUserObj.CifID, appsettings_constant_1.Appsettings.OTP_REASON_CODE_ADD_BENEFICIARY)
            .subscribe(function (initiateOtpResponse) {
            self.showBusyLoader = false;
            self.hideOTPToast = false;
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.isErrorToast = false;
                self.toastMessage = appsettings_constant_1.Appsettings.OTP_SENT_MESSAGE;
            }
            else {
                self.isErrorToast = true;
                self.toastMessage = initiateOtpResponse.ResponseFriendlyMessage;
            }
            self.InitToastr();
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self.cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        });
    };
    // OTP SNIPPETS.
    BeneficiaryDetailsComponent.prototype.closeOtpView = function () {
        this.clearOTPValues();
        this.showOtpView = false;
        this.otpFormSubmitted = false;
        this.hideOTPToast = true;
        this.cdRef.detectChanges();
    };
    BeneficiaryDetailsComponent.prototype.clearOTPValues = function () {
        this.otpFormGroup.controls["otpModel1"].setValue("");
        this.otpFormGroup.controls["otpModel2"].setValue("");
        this.otpFormGroup.controls["otpModel3"].setValue("");
        this.otpFormGroup.controls["otpModel4"].setValue("");
        this.otpFormGroup.controls["otpModel5"].setValue("");
        $("#otpModel1").focus();
        this.otpValues[0] = this.otpValues[1] = this.otpValues[2] = this.otpValues[3] = this.otpValues[4] = "";
        this.showOtpError = false;
        this.cdRef.detectChanges();
    };
    BeneficiaryDetailsComponent.prototype.preventArrow = function (event) {
        var key = event.charCode || event.keyCode || 0;
        if (key > 36 && key < 41) {
            event.preventDefault();
        }
    };
    BeneficiaryDetailsComponent.prototype.goNext = function (event, index) {
        var key = event.charCode || event.keyCode || 0;
        // Shift Key : do nada
        if (event.shiftKey) {
            return false;
        }
        else if (key == 8 || key == 46 || key == 45) {
            this.clearOTPValues();
            this.showOtpError = true;
        }
        else if (key == 9) {
            if (!this.otpValues[index - 1]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else if (key > 36 && key < 41) {
            if (index == 5 && !this.otpValues[4]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else {
            //save value and replace with * on txtbox view (if not empty); move to next tab
            if (index == 1) {
                if (this.otpFormGroup.controls["otpModel1"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel1"].value)) {
                    this.otpValues[0] = this.otpFormGroup.controls["otpModel1"].value;
                    this.otpFormGroup.controls["otpModel1"].setValue("*");
                }
                else {
                    this.otpValues[0] = "";
                }
                $("#otpModel2").focus();
            }
            else if (index == 2) {
                if (this.otpFormGroup.controls["otpModel2"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel2"].value)) {
                    this.otpValues[1] = this.otpFormGroup.controls["otpModel2"].value;
                    this.otpFormGroup.controls["otpModel2"].setValue("*");
                }
                else {
                    this.otpValues[1] = "";
                }
                $("#otpModel3").focus();
            }
            else if (index == 3) {
                if (this.otpFormGroup.controls["otpModel3"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel3"].value)) {
                    this.otpValues[2] = this.otpFormGroup.controls["otpModel3"].value;
                    this.otpFormGroup.controls["otpModel3"].setValue("*");
                }
                else {
                    this.otpValues[2] = "";
                }
                $("#otpModel4").focus();
            }
            else if (index == 4) {
                if (this.otpFormGroup.controls["otpModel4"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel4"].value)) {
                    this.otpValues[3] = this.otpFormGroup.controls["otpModel4"].value;
                    this.otpFormGroup.controls["otpModel4"].setValue("*");
                }
                else {
                    this.otpValues[3] = "";
                }
                $("#otpModel5").focus();
            }
            else if (index == 5) {
                if (this.otpFormGroup.controls["otpModel5"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel5"].value)) {
                    this.otpValues[4] = this.otpFormGroup.controls["otpModel5"].value;
                    this.otpFormGroup.controls["otpModel5"].setValue("*");
                }
                else {
                    this.otpValues[4] = "";
                }
            }
            //invalidate otp form if last otp xter is empty
            if (this.otpValues[4]) {
                this.showOtpError = false;
            }
            else {
                this.showOtpError = true;
            }
        }
    };
    BeneficiaryDetailsComponent.prototype.ngOnDestroy = function () {
        if (this.getBeneficairyRecentTransactionSub) {
            this.getBeneficairyRecentTransactionSub.unsubscribe();
        }
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
    };
    BeneficiaryDetailsComponent.prototype.GoHome = function () {
        //this.ngOnDestroy();
        this.router.navigate(['dashboard']);
    };
    BeneficiaryDetailsComponent.prototype.GoBack = function () {
        // this.ngOnDestroy();
        this.router.navigate(['MainView/PayTransfer/Beneficiary/List']);
    };
    BeneficiaryDetailsComponent.prototype.logout = function () {
        // this.ngOnDestroy();
        this.authService.logout();
        this.router.navigate(['login']);
    };
    BeneficiaryDetailsComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/beneficiary/beneficiary-details.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef])
    ], BeneficiaryDetailsComponent);
    return BeneficiaryDetailsComponent;
    var _a, _b, _c, _d;
}(base_component_1.BaseComponent));
exports.BeneficiaryDetailsComponent = BeneficiaryDetailsComponent;
//# sourceMappingURL=beneficiary-details.component.js.map