"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var dashboard_services_1 = require("../../../services/dashboard.services");
var utils_services_1 = require("../../../../commons/services/utils.services");
var constants_services_1 = require("../../../../commons/services/constants.services");
var authentication_service_1 = require("../../../../authentication/services/authentication.service");
var appsettings_constant_1 = require("../../../../commons/constants/appsettings.constant");
var response_codes_constant_1 = require("../../../../commons/constants/response_codes.constant");
var event_handlers_service_1 = require("../../../../commons/services/event_handlers.service");
var common_1 = require('@angular/common');
var schedule_payment_base_component_1 = require("../../../../commons/components/schedule_payment.base.component");
function _window() {
    // return the global native browser window object
    return window;
}
var PayBillerDetailsComponent = (function (_super) {
    __extends(PayBillerDetailsComponent, _super);
    function PayBillerDetailsComponent(authService, utiilService, router, formBuilder, dashService, constantService, cdRef, decimalPipe) {
        _super.call(this, authService, router, cdRef, formBuilder, appsettings_constant_1.Appsettings.OTP_REASON_CODE_PAY_BILLER);
        this.authService = authService;
        this.utiilService = utiilService;
        this.router = router;
        this.formBuilder = formBuilder;
        this.dashService = dashService;
        this.constantService = constantService;
        this.cdRef = cdRef;
        this.decimalPipe = decimalPipe;
        document.title = "STANBIC IBTC BANK";
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.formSubmitted = false;
        this.cancelModalOpened = false;
        this._formGroup = formBuilder.group({
            myReference: [''],
            consumerCode: [''],
            amount: [''],
            productType: ['', forms_1.Validators.required],
            numberofScheduleFrequency: [''],
            paymentDate: ['']
        });
        this.textInputActive = '';
        this.withinDailyLimit = true;
        this.totalAmountOnView = this.totalAmountLessChargeForScheduleView = 0;
        this.amountErrorOnView = false;
        this.insufficientFund = false;
        this.amountUtilizedFromLimit = 0;
        this.availableLimit = 1000000;
        this.isFormOpen = true;
        //SCHEDULE PAYMENT
        this.frequencyTypes = appsettings_constant_1.Appsettings.SCHEDUE_PAYMENT_FREQUENCY_TYPES;
        this.numberOfPayments = 0;
        this.todaySelectedOnJustOnceSchedule = false;
    }
    PayBillerDetailsComponent.prototype.openOrClosePaymentPane = function () {
        this.isFormOpen = !this.isFormOpen;
    };
    PayBillerDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        if (!dashboard_services_1.DashboardService.CrossPageCustomerAccountList ||
            (dashboard_services_1.DashboardService.CrossPageCustomerAccountList && dashboard_services_1.DashboardService.CrossPageCustomerAccountList.length < 1) ||
            !dashboard_services_1.DashboardService.CrossBillerBeneficiaryObject) {
            this.router.navigate(['MainView/PayTransfer/Biller/List']);
            return;
        }
        this.thisBiller = dashboard_services_1.DashboardService.CrossBillerBeneficiaryObject;
        this.debitableNGNAccounts = dashboard_services_1.DashboardService.CrossPageCustomerAccountList.filter(function (x) { return x.debitAllowed == true && x.currency.toLowerCase() == 'ngn'; });
        if (this.thisBiller.consumerCode) {
            self._formGroup.controls['consumerCode'].setValue(this.thisBiller.consumerCode);
        }
        if (this.thisBiller.myReference) {
            self._formGroup.controls['myReference'].setValue(this.thisBiller.myReference);
        }
        if (!dashboard_services_1.DashboardService.CrossPageBillerProductList) {
            self.showBusyLoader = true;
            if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
                var loadingModalInterval_1 = setInterval(function () {
                    self.showBusyLoader = false;
                    self._getBillerProductList();
                    _window().clearInterval(loadingModalInterval_1);
                }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
            }
            else {
                self._getBillerProductList();
            }
        }
        else {
            self.productList = new Array();
            self.productList = dashboard_services_1.DashboardService.CrossPageBillerProductList; // filter by biller id
        }
        this.selectedAccountOptionSub = event_handlers_service_1.EventHandlerService.DropdownSelectedOptionEventPublisher
            .subscribe(function (object) {
            // optionKey is accounNumber on dropdown since its unique per account 
            var optionKey = object.optionKey;
            var index = object.index;
            if (index == 1) {
                var selectedAccount = self.debitableNGNAccounts.filter(function (a) { return a.accountNumber == optionKey; })[0];
                self.availableBalance = selectedAccount.availableBalance;
                self.transactionCurrency = selectedAccount.currency.toUpperCase();
                self.selectedAccountNo = selectedAccount.accountNumber;
                self.selectedAccountName = selectedAccount.accountName;
                // self.payBillerFormGroup.controls['amount'].value;
                self.validateAmountAgainstAvailableBalance(self._formGroup.controls['amount'].value);
                if (_this._formGroup.controls["amount"]) {
                    var amountEntered = '';
                    amountEntered = (_this._formGroup.controls["amount"].value).toString();
                    //remove .00 if exist
                    if (amountEntered.indexOf('.00') != -1) {
                        amountEntered = amountEntered.replace('.00', '');
                    }
                    if (amountEntered && /\D/g.test(amountEntered)) {
                        // Filter non-digits from input value.
                        amountEntered = amountEntered.replace(/\D/g, '');
                    }
                    var amount = parseFloat(amountEntered);
                    _this.validateAmountAgainstAvailableBalance(amount);
                }
            }
            else if (index == 2) {
                self.selectedProductType = undefined;
                self._formGroup.controls['amount'].setValue(undefined);
                self._formGroup.controls['productType'].setValue(undefined);
                var selectedProductType = self.productList.filter(function (c) { return c.productId == optionKey; });
                if (selectedProductType && selectedProductType.length > 0) {
                    self.selectedProductType = selectedProductType[0];
                    self.invalidProductType = false;
                    self._formGroup.controls['productType'].setValue(self.selectedProductType.productName);
                }
                if (self.selectedProductType && self.selectedProductType.isAmountFixed.toLowerCase() == 'y') {
                    self._formGroup.controls['amount'].setValue(self.selectedProductType.amount.toString());
                    self.validateAmountAgainstAvailableBalance(self.selectedProductType.amount);
                }
                else {
                    self.validateAmountAgainstAvailableBalance(0);
                }
            }
            else if (index == 3) {
                var selectedFrequency = self.frequencyTypes.filter(function (x) { return x.Id.toString() == optionKey; })[0];
                if (selectedFrequency) {
                    self.selectedScheduleFrequency = selectedFrequency.Id;
                    self.resetNumberOfScheduleFrequency(self.selectedScheduleFrequency);
                    self.computeSchedulePaymentBreakdown();
                }
            }
        });
    };
    PayBillerDetailsComponent.prototype._getBillerProductList = function () {
        var self = this;
        self.getBillerProductsSub = self.dashService.getBillerProductList(authentication_service_1.AuthenticationService.authUserObj.UserId)
            .subscribe(function (response) {
            if (response.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                dashboard_services_1.DashboardService.CrossPageBillerProductList = new Array();
                dashboard_services_1.DashboardService.CrossPageBillerProductList = response.Products;
                self.productList = new Array();
                self.productList = dashboard_services_1.DashboardService.CrossPageBillerProductList; // filter by biller id
            }
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.cdRef.detectChanges();
        }, function () {
            // self.showBusyLoader = false;   
            // self.cdRef.detectChanges();
        });
    };
    PayBillerDetailsComponent.prototype.textInputFocus = function (formControlName) {
        this.textInputActive = formControlName;
        this.formSubmitted = false;
        if (formControlName == "amount") {
            //this.amountRequiredError = false;
            this.showTransactionLimit = true;
            this.withinDailyLimit = true;
            this.insufficientFund = false;
            //   this.firstKeyUpPassedAfterAmountFocus = false;
            if (this._formGroup.controls["amount"]) {
                var amountEntered = this._formGroup.controls["amount"].value;
                if (amountEntered) {
                    //remove .00 if exist
                    if (amountEntered.indexOf('.00') != -1) {
                        amountEntered = amountEntered.replace('.00', '');
                    }
                    // Filter non-digits from input value.
                    if (/\D/g.test(amountEntered)) {
                        amountEntered = amountEntered.replace(/\D/g, '');
                    }
                }
                this._formGroup.controls["amount"].setValue(amountEntered);
            }
        }
        this.cdRef.detectChanges();
    };
    PayBillerDetailsComponent.prototype.textInputBlur = function (formControlName) {
        if (!this._formGroup.controls['' + formControlName].value) {
            this.textInputActive = "";
            this.cdRef.detectChanges();
        }
        if (formControlName == "amount") {
            this.showTransactionLimit = false;
            var transferCharges = 0;
            var amount = 0;
            if (this._formGroup.controls['' + formControlName]) {
                amount = this._formGroup.controls['' + formControlName].value;
            }
            if (amount > 0) {
                this._formGroup.controls['' + formControlName].setValue(this.decimalPipe.transform(amount, '.2'));
            }
            this.cdRef.detectChanges();
        }
    };
    PayBillerDetailsComponent.prototype.amountInputKeyup = function () {
        var amount = 0.0;
        // Only allow digits on amount alone.
        if (this._formGroup.controls["amount"]) {
            var amountEntered = this._formGroup.controls["amount"].value;
            if (amountEntered && /\D/g.test(amountEntered)) {
                // Filter non-digits from input value.
                amountEntered = amountEntered.replace(/\D/g, '');
            }
            this._formGroup.controls["amount"].setValue(amountEntered);
            amount = parseFloat(amountEntered);
        }
        this.validateAmountAgainstAvailableBalance(amount);
    };
    PayBillerDetailsComponent.prototype.validateAmountAgainstAvailableBalance = function (amount) {
        this.totalAmountOnView = amount;
        if (amount) {
            this.totalAmountLessChargeForScheduleView = amount;
        }
        if (amount > this.availableBalance) {
            this.insufficientFund = true;
            this.amountErrorOnView = true;
        }
        else {
            this.insufficientFund = false;
            this.amountErrorOnView = false;
        }
        if (amount > (+this.availableLimit - +this.amountUtilizedFromLimit)) {
            this.withinDailyLimit = false;
            if (!this.amountErrorOnView) {
                this.amountErrorOnView = true;
            }
        }
        else {
            this.withinDailyLimit = true;
        }
    };
    PayBillerDetailsComponent.prototype.nextToReview = function (event) {
        event.stopPropagation();
        var self = this;
        this.formSubmitted = true;
        this.invalidProductType = false;
        if (this._formGroup.valid) {
            if (!this.selectedProductType) {
                this.invalidProductType = true;
            }
            if (!this.invalidProductType && !this.insufficientFund && this.withinDailyLimit && this.totalAmountOnView > 0) {
                this.pageIndex = 2;
                this.pageSubtitle = "Review";
            }
        }
        self.cdRef.detectChanges();
        return false;
    };
    PayBillerDetailsComponent.prototype.validateOTP = function (event) {
        event.stopPropagation();
        // validate otp and make payment
        this.showOtpView = false;
        this.pageIndex = 3;
        this.pageSubtitle = "Receipt";
        this.cdRef.detectChanges();
        return false;
    };
    PayBillerDetailsComponent.prototype.backToBillers = function (event) {
        event.stopPropagation();
        if (dashboard_services_1.DashboardService.BeneficiaryOrBillerDetailsToPaymentPage == true) {
            this.router.navigate(['MainView/Recipients/Biller/Details']);
        }
        else {
            this.router.navigate(['MainView/PayTransfer/Biller/List']);
        }
        return false;
    };
    PayBillerDetailsComponent.prototype.backDetailsForm = function (event) {
        event.stopPropagation();
        this.pageIndex = 1;
        this.pageSubtitle = "Details";
        this.cdRef.detectChanges();
        return false;
    };
    PayBillerDetailsComponent.prototype.ngOnDestroy = function () {
        if (this.selectedAccountOptionSub) {
            this.selectedAccountOptionSub.unsubscribe();
        }
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
        if (this.getBillerProductsSub) {
            this.getBillerProductsSub.unsubscribe();
        }
    };
    PayBillerDetailsComponent.prototype.hideCancelTransferModal = function () {
        this.cancelModalOpened = false;
    };
    PayBillerDetailsComponent.prototype.CloseTranfer = function () {
        this.cancelModalOpened = true;
    };
    PayBillerDetailsComponent.prototype.backHome = function () {
        this.router.navigate(['dashboard']);
    };
    PayBillerDetailsComponent.prototype.Goto = function (url) {
        this.router.navigate([url]);
        return false;
    };
    PayBillerDetailsComponent.prototype.logout = function () {
        this.authService.logout();
        this.router.navigate(['login']);
    };
    PayBillerDetailsComponent = __decorate([
        core_1.Component({
            templateUrl: "html/home/transfer/biller/pay-biller-details.html"
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof authentication_service_1.AuthenticationService !== 'undefined' && authentication_service_1.AuthenticationService) === 'function' && _a) || Object, (typeof (_b = typeof utils_services_1.UtilService !== 'undefined' && utils_services_1.UtilService) === 'function' && _b) || Object, router_1.Router, forms_1.FormBuilder, (typeof (_c = typeof dashboard_services_1.DashboardService !== 'undefined' && dashboard_services_1.DashboardService) === 'function' && _c) || Object, (typeof (_d = typeof constants_services_1.ConstantService !== 'undefined' && constants_services_1.ConstantService) === 'function' && _d) || Object, core_1.ChangeDetectorRef, common_1.DecimalPipe])
    ], PayBillerDetailsComponent);
    return PayBillerDetailsComponent;
    var _a, _b, _c, _d;
}(schedule_payment_base_component_1.SchedulePaymentBaseComponent));
exports.PayBillerDetailsComponent = PayBillerDetailsComponent;
//# sourceMappingURL=pay-biller-details.component.js.map