"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var http_1 = require('@angular/http');
var router_1 = require('@angular/router');
var authorization_services_1 = require("../authentication/services/authorization.services");
var common_module_1 = require('../commons/common.module');
var dashboard_component_1 = require('./components/dashboard.component');
var beneficiaries_component_1 = require('./components/Transfer/beneficiary/beneficiaries.component');
var beneficiary_row_component_1 = require('./components/Transfer/beneficiary/beneficiary-row.component');
var beneficiary_payment_component_1 = require('./components/Transfer/beneficiary/beneficiary-payment.component');
var beneficiary_payment_row_component_1 = require('./components/Transfer/beneficiary/beneficiary-payment-row.component');
var one_off_payment_component_1 = require('./components/Transfer/one-off-payment.component');
var add_beneficiary_component_1 = require('./components/Transfer/beneficiary/add-beneficiary.component');
var billers_component_1 = require('./components/Transfer/biller/billers.component');
var pay_biller_details_component_1 = require('./components/Transfer/biller/pay-biller-details.component');
var add_biller_component_1 = require('./components/Transfer/biller/add-biller.component');
var buyAirtime_component_1 = require('./components/Buy/buyAirtime.component');
var redeem_component_1 = require('./components/Western_Union/redeem.component');
var remita_component_1 = require('./components/Transfer/remita.component');
var self_transfer_component_1 = require('./components/Transfer/self-transfer.component');
var statement_component_1 = require('./components/statement.component');
var beneficiary_details_component_1 = require('./components/Transfer/beneficiary/beneficiary-details.component');
var edit_beneficiary_component_1 = require('./components/Transfer/beneficiary/edit-beneficiary.component');
var international_payment_component_1 = require('./components/Transfer/international-payment.component');
var biller_details_component_1 = require('./components/Transfer/biller/biller-details.component');
var edit_biller_component_1 = require('./components/Transfer/biller/edit-biller.component');
var common_2 = require('@angular/common');
var custom_pipes_pipe_1 = require('../commons/pipes/custom_pipes.pipe');
var header_service_1 = require("../commons/services/header.service");
var constants_services_1 = require("../commons/services/constants.services");
var utils_services_1 = require("../commons/services/utils.services");
var dashboard_services_1 = require("./services/dashboard.services");
var authentication_service_1 = require("../authentication/services/authentication.service");
//import {DropdownComponent} from '../commons/components/dropdown.component'; 
//import { FormatNumberWithCommaPipe } from "../commons/pipes/custom_pipes.pipe";
var DashboardModule = (function () {
    function DashboardModule() {
    }
    DashboardModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule, common_module_1.SharedModule],
            declarations: [dashboard_component_1.DashboardComponent, beneficiaries_component_1.BeneficiariesComponent, beneficiary_row_component_1.BeneficiaryRowComponent, custom_pipes_pipe_1.BeneficiaryNameAbbreviationPipe,
                beneficiary_payment_component_1.PayBeneficiariesComponent, beneficiary_payment_row_component_1.BeneficiaryPaymentRowComponent, one_off_payment_component_1.OneOffPaymentComponent,
                add_beneficiary_component_1.AddBeneficiaryComponent, billers_component_1.BillersComponent, pay_biller_details_component_1.PayBillerDetailsComponent, self_transfer_component_1.SelfTransferComponent,
                add_biller_component_1.AddBillerComponent, buyAirtime_component_1.BuyAirtimeComponent, redeem_component_1.RedeemWesternUnionComponent, remita_component_1.RemitaComponent, beneficiary_details_component_1.BeneficiaryDetailsComponent, edit_beneficiary_component_1.EditBeneficiaryComponent,
                biller_details_component_1.BillerDetailsComponent, edit_biller_component_1.EditBillerComponent, international_payment_component_1.InternationalPaymentComponent,
                statement_component_1.AccountStatementComponent],
            providers: [common_2.DecimalPipe, header_service_1.HeaderService, constants_services_1.ConstantService, utils_services_1.UtilService, dashboard_services_1.DashboardService, authentication_service_1.AuthenticationService, authorization_services_1.AuthorizeService, custom_pipes_pipe_1.BeneficiaryNameAbbreviationPipe]
        }), 
        __metadata('design:paramtypes', [])
    ], DashboardModule);
    return DashboardModule;
}());
exports.DashboardModule = DashboardModule;
//# sourceMappingURL=dashboard.module.js.map