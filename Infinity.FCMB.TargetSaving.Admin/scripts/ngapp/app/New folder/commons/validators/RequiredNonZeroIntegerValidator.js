"use strict";
var RequiredNonZeroIntegerValidator = (function () {
    function RequiredNonZeroIntegerValidator() {
    }
    RequiredNonZeroIntegerValidator.isValid = function (formControl) {
        try {
            var value = parseInt(formControl.value);
            if (value < 1) {
                return { requiredNonZeroInteger: true };
            }
        }
        catch (Ex) {
            return { requiredNonZeroInteger: true };
        }
        return null;
    };
    return RequiredNonZeroIntegerValidator;
}());
exports.RequiredNonZeroIntegerValidator = RequiredNonZeroIntegerValidator;
//# sourceMappingURL=RequiredNonZeroIntegerValidator.js.map