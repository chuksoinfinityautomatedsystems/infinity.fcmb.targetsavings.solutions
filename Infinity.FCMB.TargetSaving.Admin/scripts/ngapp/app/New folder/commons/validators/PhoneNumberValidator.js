"use strict";
var PhoneNumberValidator = (function () {
    function PhoneNumberValidator() {
    }
    PhoneNumberValidator.isValid = function (formControl) {
        var phoneRegex = /^[0-9+][ ]*[0-9][0-9\s+]+$/i;
        if (formControl.value) {
            var isValid = phoneRegex.test(formControl.value);
            if (isValid === false) {
                return { phoneNumberInvalid: true };
            }
        }
        return null;
    };
    return PhoneNumberValidator;
}());
exports.PhoneNumberValidator = PhoneNumberValidator;
//# sourceMappingURL=PhoneNumberValidator.js.map