"use strict";
(function (ComponentEventPublisherEnum) {
    ComponentEventPublisherEnum[ComponentEventPublisherEnum["AmountTransfer"] = 0] = "AmountTransfer";
    ComponentEventPublisherEnum[ComponentEventPublisherEnum["BeneficiaryTransfer"] = 1] = "BeneficiaryTransfer";
})(exports.ComponentEventPublisherEnum || (exports.ComponentEventPublisherEnum = {}));
var ComponentEventPublisherEnum = exports.ComponentEventPublisherEnum;
//# sourceMappingURL=component_event_publisher.enum.js.map