"use strict";
var ResponseCodes = (function () {
    function ResponseCodes() {
    }
    ResponseCodes.SUCCESS = "00";
    ResponseCodes.LoginSuccessWithLimitedAccess = "02";
    ResponseCodes.QuestionAndPasswordRegistrationRequired = "04";
    ResponseCodes.PasswordChangeRequired = "05";
    ResponseCodes.TechnicalError = "99";
    ResponseCodes.NoError = "XX";
    return ResponseCodes;
}());
exports.ResponseCodes = ResponseCodes;
//# sourceMappingURL=response_codes.constant.js.map