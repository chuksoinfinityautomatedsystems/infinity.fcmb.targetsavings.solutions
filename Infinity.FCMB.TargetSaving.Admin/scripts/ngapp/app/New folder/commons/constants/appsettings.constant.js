"use strict";
var Appsettings = (function () {
    function Appsettings() {
    }
    Appsettings.APP_MODE = "offline";
    Appsettings.API_ENDPOINT = "http://10.234.200.155:53390/api/";
    Appsettings.AUTHENTICATE_USER_ID_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/AuthenticateRIBUser";
    Appsettings.GET_SECURITY_QUESTIONS_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/GetRIBRegistrationQuestions";
    Appsettings.SAVE_SECURITY_QUESTIONS_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/SaveRegistrationQuestions";
    Appsettings.SAVE_SECURITY_QUESTIONS_AND_GENERATE_DEFAULT_PASSWORD_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/SaveRegistrationQuestionsAndDefaultPassword";
    Appsettings.CHANGE_PASSWORD_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/ChangePassword";
    Appsettings.INITIATE_OTP_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/InitiateOTPRequest";
    Appsettings.VALIDATE_OTP_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/ValidateOTPRequest";
    Appsettings.GET_CUSTOMER_ACCOUNT_LIST = Appsettings.API_ENDPOINT + "AccountManagement/GetAccountsByCIFId";
    Appsettings.GET_CUSTOMER_TRANSACTION_DETAILS = Appsettings.API_ENDPOINT + "TransactionManagement/GetTransactionDetailsByAccountNumberAndCount";
    Appsettings.NAME_ENQUIRY_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/ChangePassword";
    Appsettings.GET_CUSTOMER_BENEFICIARIES = Appsettings.API_ENDPOINT + "BeneficiaryManagement/GetBeneficiaryList";
    Appsettings.GET_RECENT_BENEFICIARY_TRANSFER = Appsettings.API_ENDPOINT + "UserProfileManagement/ChangePassword";
    Appsettings.GET_CUSTOMER_BILLER_BENEFICIARIES = Appsettings.API_ENDPOINT + "BeneficiaryManagement/GetBeneficiaryList";
    Appsettings.GET_BILLERS = Appsettings.API_ENDPOINT + "BeneficiaryManagement/GetBeneficiaryList";
    Appsettings.GET_BILLER_CATEGORIES = Appsettings.API_ENDPOINT + "BeneficiaryManagement/GetBeneficiaryList";
    Appsettings.GET_BILLER_PRODUCTS = Appsettings.API_ENDPOINT + "BeneficiaryManagement/GetBeneficiaryList";
    Appsettings.TECHNICAL_ERROR_MESSAGE = "Your request cannot be completed at the moment due to a technical error";
    Appsettings.LOCKED_USER_ERROR_MESSAGE = "Your account has been blocked. Contact a branch administrator to reser your password";
    Appsettings.TIMEOUT_LOGOUT_MESSAGE = "To protect your account, we have automatically logged you out of internet banking. You can login again when you are ready.";
    Appsettings.LOGOUT_MESSAGE = "You have been successfully logged out";
    Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND = 500;
    Appsettings.TOAST_INTERVAL_IN_MICROSECOND = 1500;
    Appsettings.TOAST_FADE_OUT_IN_MICROSECOND = 1600;
    Appsettings.LOGIN_ERROR_SHAKE_INTERVAL = 500;
    Appsettings.SESSION_LOGOUT_COUNTER = 60;
    Appsettings.autoLogoutTimeInMicrosec = 300000;
    Appsettings.LOCAL_BANK_CODE = '221';
    Appsettings.LOCAL_CURRENCY = 'NGN';
    Appsettings.MINIMUM_ACCOUNT_NO_LENGTH = 10;
    //static autoLogoutTimeInMicrosec = 20000;
    Appsettings.OTP_SENT_MESSAGE = "OTP Sent.";
    Appsettings.AUTHENTICATE_LOGIN_OTP_URL = Appsettings.API_ENDPOINT + "validate-login-otp";
    Appsettings.CONFIRM_USER_EXIST_URL = Appsettings.API_ENDPOINT + "AccountManagement/CheckIfInternetBankingProfileExist";
    //DASHBOARD
    Appsettings.CUSTOMER_ACCOUNT_LIST = Appsettings.API_ENDPOINT + "account-list";
    Appsettings.LOGIN_FROM_PAGE = "LOGIN";
    Appsettings.REGISTER_USER_FROM_PAGE = "REGISTER_USER";
    Appsettings.OTP_REASON_CODE_CHANGE_PASSWORD = "04";
    Appsettings.OTP_REASON_CODE_LOGIN = "02";
    Appsettings.OTP_REASON_CODE_RESET_PASSWORD = "02";
    Appsettings.OTP_REASON_CODE_ADD_BENEFICIARY = "06";
    Appsettings.OTP_REASON_CODE_REMOVE_BENEFICIARY = "08";
    Appsettings.OTP_REASON_CODE_EDIT_BENEFICIARY = "07";
    Appsettings.OTP_REASON_CODE_PAY_BILLER = "09";
    Appsettings.OTP_REASON_CODE_PURCHASE_AIRTIME = "10";
    Appsettings.DOWNLOAD_TYPES = [{ Name: 'CSV', Id: 1 }, { Name: 'PDF', Id: 2 }];
    Appsettings.SCHEDUE_PAYMENT_FREQUENCY_TYPES = [{ Name: 'Just once', Id: 1 }, { Name: 'Daily', Id: 2 },
        { Name: 'Weekly', Id: 3 }, { Name: 'Monthly', Id: 4 }, { Name: 'Quarterly', Id: 5 }, , { Name: 'Yearly', Id: 6 }];
    // static OTP_REASON_CODE_ADD_BILLER = "08";   
    // static OTP_REASON_CODE_EDIT_BILLER = "08";
    // static OTP_REASON_CODE_REMOVE_BILLER = "08";
    // ("01","register customer"),
    // ("02","login"),
    // ("03","reset password"),
    // ("04","change password"),
    // ("05","transfer funds"),
    // ("06","add beneficiary"),
    // ("07","modify beneficiary"),
    // ("08","remove beneficiary"),
    // ("09","pay bill"),
    // ("10","purchase airtime"),
    // ("11","bind device");
    Appsettings.TRANSFER_NOTIFICATION_VIA_SMS = 1;
    Appsettings.TRANSFER_NOTIFICATION_VIA_EMAIL = 2;
    return Appsettings;
}());
exports.Appsettings = Appsettings;
//# sourceMappingURL=appsettings.constant.js.map