"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var EventHandlerService = (function () {
    function EventHandlerService() {
    }
    EventHandlerService.emitTransferAmountDetails = function (chargesOnAmount, amountEntered, forComponent, beneficiaryAccountNo, beneficiaryBankCode) {
        this.BeneficiaryPaymentAmountEventPublisher.emit({ forComponent: forComponent, amountEntered: amountEntered, chargesOnAmount: chargesOnAmount, beneficiaryAccountNo: beneficiaryAccountNo, beneficiaryBankCode: beneficiaryBankCode });
    };
    EventHandlerService.emitClearInsufficientFundErrorEvent = function (forComponent) {
        this.ClearInsufficientFundErrorEventPublisher.emit({ forComponent: forComponent });
    };
    EventHandlerService.emitClearAvailableLimitErrorEvent = function (forComponent) {
        this.ClearAvailableLimitErrorEventPublisher.emit({ forComponent: forComponent });
    };
    EventHandlerService.emitClearOtherInsufficientFundErrorEvent = function (forComponent, emmitterRef) {
        this.ClearOtherInsufficientFundErrorEventPublisher.emit({ forComponent: forComponent, emmitterRef: emmitterRef });
    };
    EventHandlerService.emitClearOtherAvailableLimitErrorEvent = function (forComponent, emmitterRef) {
        this.ClearOtherAvailableLimitErrorEventPublisher.emit({ forComponent: forComponent, emmitterRef: emmitterRef });
    };
    EventHandlerService.emitTotalAmountEvent = function (forComponent, amount, charges) {
        this.TotalAmountOnViewEventPublisher.emit({ forComponent: forComponent, amount: amount, charges: charges });
    };
    EventHandlerService.emitDropdownOptionSelectedEvent = function (optionKey, index) {
        this.DropdownSelectedOptionEventPublisher.emit({ optionKey: optionKey, index: index });
    };
    EventHandlerService.emitTrasferAmountErrorEvent = function (forComponent, accountNo, isError) {
        this.RequiredAmountErrorEventPublisher.emit({ forComponent: forComponent, accountNo: accountNo, isError: isError });
    };
    EventHandlerService.emitSessionTimeoutEvent = function () {
        this.SessionTimeoutEventPublisher.emit({});
    };
    EventHandlerService.emitClearCountryDropdownEvent = function () {
        this.ClearCountryCodeDropdownEventPublisher.emit({});
    };
    EventHandlerService.emitUnselectScheduleFrequencyDropdownEvent = function () {
        this.UnselectScheduleDropdownEventPublisher.emit({});
    };
    EventHandlerService.emitUnselectDropdownEvent = function (index) {
        this.UnselectDropdownEventPublisher.emit({ index: index });
    };
    EventHandlerService.emitUnselectAccountDropdownEvent = function (dropdownIndex) {
        this.UnselectAccountDropdownEventPublisher.emit({ dropdownIndex: dropdownIndex });
    };
    EventHandlerService.emitLastAmountAndChargesOnBeneficiaryEvent = function (forComponent, lastAmount, lastCharges, accountNo) {
        this.LastAmountAndChargesEnteredEventPublisher.emit({ forComponent: forComponent, lastAmount: lastAmount, lastCharges: lastCharges, accountNo: accountNo });
    };
    EventHandlerService.BeneficiaryPaymentAmountEventPublisher = new core_1.EventEmitter();
    EventHandlerService.RequiredAmountErrorEventPublisher = new core_1.EventEmitter();
    EventHandlerService.ClearInsufficientFundErrorEventPublisher = new core_1.EventEmitter();
    EventHandlerService.ClearAvailableLimitErrorEventPublisher = new core_1.EventEmitter();
    EventHandlerService.ClearOtherAvailableLimitErrorEventPublisher = new core_1.EventEmitter();
    EventHandlerService.ClearOtherInsufficientFundErrorEventPublisher = new core_1.EventEmitter();
    EventHandlerService.TotalAmountOnViewEventPublisher = new core_1.EventEmitter();
    EventHandlerService.DropdownSelectedOptionEventPublisher = new core_1.EventEmitter();
    EventHandlerService.SessionTimeoutEventPublisher = new core_1.EventEmitter();
    EventHandlerService.ClearCountryCodeDropdownEventPublisher = new core_1.EventEmitter();
    EventHandlerService.UnselectScheduleDropdownEventPublisher = new core_1.EventEmitter();
    EventHandlerService.UnselectAccountDropdownEventPublisher = new core_1.EventEmitter();
    EventHandlerService.UnselectDropdownEventPublisher = new core_1.EventEmitter();
    EventHandlerService.LastAmountAndChargesEnteredEventPublisher = new core_1.EventEmitter();
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "BeneficiaryPaymentAmountEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "RequiredAmountErrorEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "ClearInsufficientFundErrorEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "ClearAvailableLimitErrorEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "ClearOtherAvailableLimitErrorEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "ClearOtherInsufficientFundErrorEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "TotalAmountOnViewEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "DropdownSelectedOptionEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "SessionTimeoutEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "ClearCountryCodeDropdownEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "UnselectScheduleDropdownEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "UnselectAccountDropdownEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "UnselectDropdownEventPublisher", void 0);
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], EventHandlerService, "LastAmountAndChargesEnteredEventPublisher", void 0);
    EventHandlerService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], EventHandlerService);
    return EventHandlerService;
}());
exports.EventHandlerService = EventHandlerService;
//# sourceMappingURL=event_handlers.service.js.map