"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
function _window() {
    // return the global native browser window object
    return window;
}
var ConstantService = (function () {
    function ConstantService() {
        var fullUrl = _window().location.href;
        var pathArray = fullUrl.split("/");
        var apiDomain = pathArray[0] + "//" + pathArray[2] + "/api/"; // "http://localhost:12462/api/";       
        var accountCheckDomain = pathArray[0] + "//" + pathArray[2] + "/api/"; // "http://localhost:2746/api/";
        this.TokenUrl = apiDomain + "token";
    }
    ConstantService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [])
    ], ConstantService);
    return ConstantService;
}());
exports.ConstantService = ConstantService;
//# sourceMappingURL=constants.services.js.map