"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var event_handlers_service_1 = require("../../commons/services/event_handlers.service");
var base_component_1 = require("../../commons/components/base.component");
function _window() {
    return window;
}
var SchedulePaymentBaseComponent = (function (_super) {
    __extends(SchedulePaymentBaseComponent, _super);
    function SchedulePaymentBaseComponent(authService, router, cdRef, formBuilder, otpReasonCode) {
        _super.call(this, authService, router, formBuilder, cdRef, otpReasonCode);
        this._authService = authService;
        this._router = router;
        this._cdRef = cdRef;
    }
    SchedulePaymentBaseComponent.prototype.showSchedulePaymentView = function () {
        this.scheduleFrequencyViewOpened = true;
        this.initScheduleCalendar();
    };
    SchedulePaymentBaseComponent.prototype.closeScheduleFrequencyOption = function () {
        this.scheduleFrequencyViewOpened = false;
        $('#paymentDateCal').val('');
        if (this._formGroup.controls['paymentDate']) {
            this._formGroup.controls['paymentDate'].setValue(undefined);
        }
        this._formGroup.controls['numberofScheduleFrequency'].setValue(undefined);
        this.selectedScheduleFrequency = undefined;
        this.schedulePaymentStartDate = undefined;
        this.scheduleBreakdown = [];
        this.numberOfPayments = 0;
        this.scheduleBreakdownLabel = this.textInputActive = '';
        //unselect dropdown
        event_handlers_service_1.EventHandlerService.emitUnselectScheduleFrequencyDropdownEvent();
    };
    SchedulePaymentBaseComponent.prototype.initScheduleCalendar = function () {
        var self = this;
        // Wednesday, 28 Mar 18
        // 28 March 2018
        var initPaymentCalendarInterval = setInterval(function () {
            if ($('#paymentDateCal').length > 0) {
                $('#paymentDateCal').val('');
                $('#paymentDateCal').datepicker('destroy');
                if (self._formGroup.controls['paymentDate']) {
                    self._formGroup.controls['paymentDate'].setValue(undefined);
                }
                $('#paymentDateCal').datepicker({
                    orientation: "bottom left",
                    autoclose: true,
                    // format: 'dd M yy',
                    format: 'DD, dd M yy',
                    startDate: '0d',
                    container: '#schedulePaymentCalendarContainer'
                })
                    .off('changeDate')
                    .on('changeDate', function (e) {
                    //SET DATE VALUES
                    self._formGroup.controls['paymentDate'].setValue(moment(e.date).format('dddd, DD MMM  YY'));
                    self.schedulePaymentStartDate = moment(e.date).format('DD MMMM YYYY');
                    self.dateSelected = e.date;
                    self.textInputActive = "";
                    self.todaySelectedOnJustOnceSchedule = false;
                    if (moment(e.date).format('DD MMMM YYYY').toLowerCase() == moment(new Date()).format('DD MMMM YYYY').toLowerCase() && self.selectedScheduleFrequency == 1) {
                        self.todaySelectedOnJustOnceSchedule = true;
                    }
                    self._cdRef.detectChanges();
                    // DO BELOW IN CASE SCHEDULE FREQUENCY IS NOT JUST ONCE
                    self.computeSchedulePaymentBreakdown();
                });
                _window().clearInterval(initPaymentCalendarInterval);
            }
        }, 300);
    };
    SchedulePaymentBaseComponent.prototype.numberOfPaymentOnKeyUp = function () {
        this.computeSchedulePaymentBreakdown();
    };
    SchedulePaymentBaseComponent.prototype.computeSchedulePaymentBreakdown = function () {
        var self = this;
        var dateSelected = undefined;
        if (self.selectedScheduleFrequency > 1 && self.dateSelected && this._formGroup.controls['numberofScheduleFrequency']) {
            var numberOfSchedule = this._formGroup.controls['numberofScheduleFrequency'].value;
            var scheduleBreakdown = [];
            self.numberOfPayments = numberOfSchedule;
            dateSelected = new Date(self.dateSelected.toString());
            switch (self.selectedScheduleFrequency) {
                case 2:
                    self.scheduleBreakdownLabel = 'daily payments';
                    for (var a = 0; a < numberOfSchedule; a++) {
                        //let now = new Date(self.dateSelected.toString());
                        var nextDate = new Date(dateSelected.toString());
                        nextDate.setDate(dateSelected.getDate() + a);
                        var breakdown = {};
                        breakdown['date'] = moment(nextDate).format("DD MMM 'YY");
                        if (a == 0) {
                            breakdown['label'] = 'First';
                        }
                        else if (a == numberOfSchedule - 1) {
                            breakdown['label'] = 'Last';
                        }
                        else {
                            if (a < 10) {
                                breakdown['label'] = '0' + (a + 1);
                            }
                            else {
                                breakdown['label'] = a + 1;
                            }
                        }
                        scheduleBreakdown.push(breakdown);
                    }
                    break;
                case 3:
                    self.scheduleBreakdownLabel = 'weekly payments';
                    for (var a = 0; a < numberOfSchedule; a++) {
                        // let now = new Date(self.dateSelected.toString());
                        var nextDate = new Date(dateSelected.toString());
                        nextDate.setDate(dateSelected.getDate() + (a * 7));
                        var breakdown = {};
                        breakdown['date'] = moment(nextDate).format("DD MMM 'YY");
                        if (a == 0) {
                            breakdown['label'] = 'First';
                        }
                        else if (a == numberOfSchedule - 1) {
                            breakdown['label'] = 'Last';
                        }
                        else {
                            if (a < 10) {
                                breakdown['label'] = '0' + (a + 1);
                            }
                            else {
                                breakdown['label'] = a + 1;
                            }
                        }
                        scheduleBreakdown.push(breakdown);
                    }
                    break;
                case 4:
                    self.scheduleBreakdownLabel = 'monthly payments';
                    for (var a = 0; a < numberOfSchedule; a++) {
                        //let now = new Date(self.dateSelected.toString());
                        var nextDate = new Date(dateSelected.toString());
                        nextDate.setMonth(dateSelected.getMonth() + a);
                        var breakdown = {};
                        breakdown['date'] = moment(nextDate).format("DD MMM 'YY");
                        if (a == 0) {
                            breakdown['label'] = 'First';
                        }
                        else if (a == numberOfSchedule - 1) {
                            breakdown['label'] = 'Last';
                        }
                        else {
                            if (a < 10) {
                                breakdown['label'] = '0' + (a + 1);
                            }
                            else {
                                breakdown['label'] = a + 1;
                            }
                        }
                        scheduleBreakdown.push(breakdown);
                    }
                    break;
                case 5:
                    self.scheduleBreakdownLabel = 'quaterly payments';
                    for (var a = 0; a < numberOfSchedule; a++) {
                        //let now = new Date(self.dateSelected.toString());
                        var nextDate = new Date(dateSelected.toString());
                        nextDate.setMonth(dateSelected.getMonth() + (a * 3));
                        var breakdown = {};
                        breakdown['date'] = moment(nextDate).format("DD MMM 'YY");
                        if (a == 0) {
                            breakdown['label'] = 'First';
                        }
                        else if (a == numberOfSchedule - 1) {
                            breakdown['label'] = 'Last';
                        }
                        else {
                            if (a < 10) {
                                breakdown['label'] = '0' + (a + 1);
                            }
                            else {
                                breakdown['label'] = a + 1;
                            }
                        }
                        scheduleBreakdown.push(breakdown);
                    }
                    break;
                case 6:
                    self.scheduleBreakdownLabel = 'yearly payments';
                    for (var a = 0; a < numberOfSchedule; a++) {
                        // let now = new Date(self.dateSelected.toString());
                        var nextDate = new Date(dateSelected.toString());
                        nextDate.setMonth(dateSelected.getMonth() + (a * 12));
                        var breakdown = {};
                        breakdown['date'] = moment(nextDate).format("DD MMM 'YY");
                        if (a == 0) {
                            breakdown['label'] = 'First';
                        }
                        else if (a == numberOfSchedule - 1) {
                            breakdown['label'] = 'Last';
                        }
                        else {
                            if (a < 10) {
                                breakdown['label'] = '0' + (a + 1);
                            }
                            else {
                                breakdown['label'] = a + 1;
                            }
                        }
                        scheduleBreakdown.push(breakdown);
                    }
                    break;
            }
            self.scheduleBreakdown = scheduleBreakdown;
        }
    };
    SchedulePaymentBaseComponent.prototype.resetNumberOfScheduleFrequency = function (frequency) {
        var self = this;
        switch (frequency) {
            case 2:
                self.numberofScheduleFrequencyLabel = "Number of daily payments";
                break;
            case 3:
                self.numberofScheduleFrequencyLabel = "Number of weekly payments";
                break;
            case 4:
                self.numberofScheduleFrequencyLabel = "Number of monthly payments";
                break;
            case 5:
                self.numberofScheduleFrequencyLabel = "Number of quarterly payments";
                break;
            case 6:
                self.numberofScheduleFrequencyLabel = "Number of yearly payments";
                break;
        }
    };
    return SchedulePaymentBaseComponent;
}(base_component_1.BaseComponent));
exports.SchedulePaymentBaseComponent = SchedulePaymentBaseComponent;
//# sourceMappingURL=schedule_payment.base.component.js.map