"use strict";
var forms_1 = require("@angular/forms");
var response_codes_constant_1 = require("../../commons/constants/response_codes.constant");
var appsettings_constant_1 = require("../../commons/constants/appsettings.constant");
var event_handlers_service_1 = require("../../commons/services/event_handlers.service");
var authentication_service_1 = require("../../authentication/services/authentication.service");
var utils_services_1 = require("../../commons/services/utils.services");
function _window() {
    return window;
}
var BaseComponent = (function () {
    function BaseComponent(_authService, _router, _formBuilder, _cdRef, _otpReasonCode) {
        this.showBusyLoader = false;
        this._authService = _authService;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this._cdRef = _cdRef;
        this.otpReasonCode = _otpReasonCode;
        //OTP INIT
        if (_formBuilder) {
            this.otpFormGroup = this._formBuilder.group({
                otpModel1: ['', forms_1.Validators.required],
                otpModel2: ['', forms_1.Validators.required],
                otpModel3: ['', forms_1.Validators.required],
                otpModel4: ['', forms_1.Validators.required],
                otpModel5: ['', forms_1.Validators.required]
            });
            this.otpValues = ["", "", "", "", ""];
            this.showOtpError = this.otpFormSubmitted = this.showOtpView = false;
        }
        this.hideOTPToast = true;
        this.isErrorToast = false;
    }
    BaseComponent.prototype.closeExceptionModal = function () {
        this.showExceptionPage = false;
    };
    BaseComponent.prototype.InitToastr = function () {
        var self = this;
        var toastInterval = setInterval(function () {
            $("#toast-container").fadeOut(appsettings_constant_1.Appsettings.TOAST_FADE_OUT_IN_MICROSECOND, function () {
                self.hideOTPToast = true;
                $("#toast-container").removeAttr("style");
            });
            _window().clearInterval(toastInterval);
        }, appsettings_constant_1.Appsettings.TOAST_INTERVAL_IN_MICROSECOND);
    };
    BaseComponent.prototype.ReInitToast = function () {
        this.hideOTPToast = true;
        $("#toast-container").css("pointer-events: auto;");
    };
    BaseComponent.prototype.SetupTimeout = function () {
        var self = this;
        self.sessionTimeoutSub = event_handlers_service_1.EventHandlerService.SessionTimeoutEventPublisher
            .subscribe(function (object) {
            self.showSessionTimeoutPopup = true;
            self.timeoutSecondLabel = appsettings_constant_1.Appsettings.SESSION_LOGOUT_COUNTER;
            self.sessionTimeoutInterval = setInterval(function () {
                self.timeoutSecondLabel -= 1;
                // console.log(999);    
                if (self.timeoutSecondLabel < 1) {
                    if (self.sessionTimeoutInterval) {
                        _window().clearInterval(self.sessionTimeoutInterval);
                    }
                    self._authService.logout();
                    if (self.sessionTimeoutSub) {
                        self.sessionTimeoutSub.unsubscribe();
                    }
                    // console.log(888);
                    authentication_service_1.AuthenticationService.authLogoutMessage = appsettings_constant_1.Appsettings.TIMEOUT_LOGOUT_MESSAGE;
                    self._router.navigate(['login']);
                }
            }, 1000);
        });
    };
    // OTP SNIPPETS.
    BaseComponent.prototype.closeOtpView = function () {
        this.clearOTPValues();
        this.showOtpView = false;
        this.otpFormSubmitted = false;
        this.hideOTPToast = true;
        if (this._cdRef) {
            this._cdRef.detectChanges();
        }
    };
    BaseComponent.prototype.clearOTPValues = function () {
        this.otpFormGroup.controls["otpModel1"].setValue("");
        this.otpFormGroup.controls["otpModel2"].setValue("");
        this.otpFormGroup.controls["otpModel3"].setValue("");
        this.otpFormGroup.controls["otpModel4"].setValue("");
        this.otpFormGroup.controls["otpModel5"].setValue("");
        $("#otpModel1").focus();
        this.otpValues[0] = this.otpValues[1] = this.otpValues[2] = this.otpValues[3] = this.otpValues[4] = "";
        this.showOtpError = false;
        if (this._cdRef) {
            this._cdRef.detectChanges();
        }
    };
    BaseComponent.prototype.preventArrow = function (event) {
        var key = event.charCode || event.keyCode || 0;
        if (key > 36 && key < 41) {
            event.preventDefault();
        }
    };
    BaseComponent.prototype.goNext = function (event, index) {
        var key = event.charCode || event.keyCode || 0;
        // Shift Key : do nada
        if (event.shiftKey) {
            return false;
        }
        else if (key == 8 || key == 46 || key == 45) {
            this.clearOTPValues();
            this.showOtpError = true;
        }
        else if (key == 9) {
            if (!this.otpValues[index - 1]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else if (key > 36 && key < 41) {
            if (index == 5 && !this.otpValues[4]) {
                this.clearOTPValues();
                this.showOtpError = true;
            }
            return false;
        }
        else {
            //save value and replace with * on txtbox view (if not empty); move to next tab
            if (index == 1) {
                if (this.otpFormGroup.controls["otpModel1"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel1"].value)) {
                    this.otpValues[0] = this.otpFormGroup.controls["otpModel1"].value;
                    this.otpFormGroup.controls["otpModel1"].setValue("*");
                }
                else {
                    this.otpValues[0] = "";
                }
                $("#otpModel2").focus();
            }
            else if (index == 2) {
                if (this.otpFormGroup.controls["otpModel2"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel2"].value)) {
                    this.otpValues[1] = this.otpFormGroup.controls["otpModel2"].value;
                    this.otpFormGroup.controls["otpModel2"].setValue("*");
                }
                else {
                    this.otpValues[1] = "";
                }
                $("#otpModel3").focus();
            }
            else if (index == 3) {
                if (this.otpFormGroup.controls["otpModel3"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel3"].value)) {
                    this.otpValues[2] = this.otpFormGroup.controls["otpModel3"].value;
                    this.otpFormGroup.controls["otpModel3"].setValue("*");
                }
                else {
                    this.otpValues[2] = "";
                }
                $("#otpModel4").focus();
            }
            else if (index == 4) {
                if (this.otpFormGroup.controls["otpModel4"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel4"].value)) {
                    this.otpValues[3] = this.otpFormGroup.controls["otpModel4"].value;
                    this.otpFormGroup.controls["otpModel4"].setValue("*");
                }
                else {
                    this.otpValues[3] = "";
                }
                $("#otpModel5").focus();
            }
            else if (index == 5) {
                if (this.otpFormGroup.controls["otpModel5"].value && utils_services_1.UtilService.trim(this.otpFormGroup.controls["otpModel5"].value)) {
                    this.otpValues[4] = this.otpFormGroup.controls["otpModel5"].value;
                    this.otpFormGroup.controls["otpModel5"].setValue("*");
                }
                else {
                    this.otpValues[4] = "";
                }
            }
            //invalidate otp form if last otp xter is empty
            if (this.otpValues[4]) {
                this.showOtpError = false;
            }
            else {
                this.showOtpError = true;
            }
        }
    };
    BaseComponent.prototype._resendOTP = function () {
        var self = this;
        if (this.initiateOtpSub) {
            this.initiateOtpSub.unsubscribe();
        }
        self.initiateOtpSub = self._authService.initiateOTP(authentication_service_1.AuthenticationService.authUserObj.UserId, authentication_service_1.AuthenticationService.authUserObj.CifID, self.otpReasonCode)
            .subscribe(function (initiateOtpResponse) {
            self.showBusyLoader = false;
            self.hideOTPToast = false;
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.isErrorToast = false;
                self.toastMessage = appsettings_constant_1.Appsettings.OTP_SENT_MESSAGE;
            }
            else {
                self.isErrorToast = true;
                self.toastMessage = initiateOtpResponse.ResponseFriendlyMessage;
            }
            self.InitToastr();
            self._cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            self.hideOTPToast = false;
            self.isErrorToast = true;
            self.toastMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            self._cdRef.detectChanges();
            self.InitToastr();
        }, function () {
            self.showBusyLoader = false;
            self._cdRef.detectChanges();
        });
    };
    BaseComponent.prototype.initiateOTP = function (event) {
        event.stopPropagation();
        var self = this;
        this.showBusyLoader = true;
        this._cdRef.detectChanges();
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_1 = setInterval(function () {
                self._initiateOTPRequest();
                _window().clearInterval(loadingModalInterval_1);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._initiateOTPRequest();
        }
    };
    BaseComponent.prototype._initiateOTPRequest = function () {
        var _this = this;
        var self = this;
        self.isErrorToast = true;
        self.toastMessage = "";
        self._cdRef.detectChanges();
        self.initiateOtpSub = self._authService.initiateOTP(authentication_service_1.AuthenticationService.authUserObj.UserId, authentication_service_1.AuthenticationService.authUserObj.CifID, self.otpReasonCode)
            .subscribe(function (initiateOtpResponse) {
            if (initiateOtpResponse.ResponseCode == response_codes_constant_1.ResponseCodes.SUCCESS) {
                self.showOtpView = true;
                self.showBusyLoader = false;
            }
            else {
                _this.exceptionMessage = initiateOtpResponse.ResponseFriendlyMessage;
                _this.showExceptionPage = true;
            }
            self._cdRef.detectChanges();
        }, function (error) {
            self.showBusyLoader = false;
            _this.exceptionMessage = appsettings_constant_1.Appsettings.TECHNICAL_ERROR_MESSAGE;
            _this.showExceptionPage = true;
            self._cdRef.detectChanges();
        }, function () {
            self.showBusyLoader = false;
            self._cdRef.detectChanges();
        });
    };
    BaseComponent.prototype.reSendOtp = function () {
        var self = this;
        self.hideOTPToast = true;
        self.toastMessage = "";
        self.isErrorToast = true;
        self.showBusyLoader = true;
        self._cdRef.detectChanges();
        if (appsettings_constant_1.Appsettings.APP_MODE && appsettings_constant_1.Appsettings.APP_MODE.toLowerCase() != "online") {
            var loadingModalInterval_2 = setInterval(function () {
                self.showBusyLoader = false;
                self._cdRef.detectChanges();
                self._resendOTP();
                _window().clearInterval(loadingModalInterval_2);
            }, appsettings_constant_1.Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._resendOTP();
        }
    };
    return BaseComponent;
}());
exports.BaseComponent = BaseComponent;
//# sourceMappingURL=base.component.js.map