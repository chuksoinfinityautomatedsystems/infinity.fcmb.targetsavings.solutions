"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var LoadingModalComponent = (function () {
    function LoadingModalComponent() {
        this.visible = false;
        this.visibleAnimate = false;
    }
    LoadingModalComponent.prototype.show = function () {
        var _this = this;
        this.visible = true;
        setTimeout(function () { return _this.visibleAnimate = true; }, 100);
    };
    LoadingModalComponent.prototype.hideModal = function () {
        var _this = this;
        this.visibleAnimate = false;
        setTimeout(function () { return _this.visible = false; }, 300);
    };
    LoadingModalComponent = __decorate([
        core_1.Component({
            selector: 'ng-loading-modal',
            templateUrl: "html/loading-dialog.modal.html"
        }), 
        __metadata('design:paramtypes', [])
    ], LoadingModalComponent);
    return LoadingModalComponent;
}());
exports.LoadingModalComponent = LoadingModalComponent;
//# sourceMappingURL=loading-dialog.component.js.map