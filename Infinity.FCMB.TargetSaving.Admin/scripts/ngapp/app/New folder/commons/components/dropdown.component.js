"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var event_handlers_service_1 = require("../../commons/services/event_handlers.service");
var DropdownComponent = (function () {
    function DropdownComponent() {
        this.dropdownIsEmpty = true;
        this.dropdownIsOpen = false;
        this.optionKeySelected = '';
        this.optionValueSelected = this.optionFilterQ = '';
        this.hideSelectedLabel = false;
    }
    DropdownComponent.prototype.ngOnInit = function () {
        var self = this;
        if (this.selectOptionOnLoad && this.optionIndexToBeSelectedOnLoad >= 0) {
            this.selectOption(this.optionIndexToBeSelectedOnLoad, null);
        }
        this.unselectDropdownSub = event_handlers_service_1.EventHandlerService.UnselectDropdownEventPublisher
            .subscribe(function (object) {
            var dropdownIndex = object.index;
            if (dropdownIndex == self.index) {
                self.dropdownIsEmpty = true;
                self.dropdownIsOpen = false;
                self.optionKeySelected = '';
                self.optionHoveredIndex = 0;
            }
        });
        this.unselectAccountDropdownSub = event_handlers_service_1.EventHandlerService.UnselectAccountDropdownEventPublisher
            .subscribe(function (object) {
            var dropdownIndex = object.dropdownIndex;
            if (dropdownIndex == self.index) {
                console.log('Unselect options...');
                console.log(dropdownIndex);
                console.log(self.index);
                self.dropdownIsEmpty = true;
                self.dropdownIsOpen = false;
                self.optionKeySelected = '';
                self.optionHoveredIndex = 0;
            }
        });
    };
    DropdownComponent.prototype.selectOption = function (idx, event) {
        if (event != null) {
            event.stopPropagation();
        }
        if (idx >= 0) {
            this.optionKeySelected = this.filteredOptions[idx]["" + this.optionKeyName + ""];
            if (this.dropdownForAccountListings) {
                this.optionValueSelected = this.filteredOptions[idx].accountName.toUpperCase() + ' - ' + this.filteredOptions[idx].accountNumber;
                this.selectedAvailableBalance = this.filteredOptions[idx].availableBalance;
                this.selectedCurrency = this.filteredOptions[idx].currency;
            }
            else {
                this.optionValueSelected = this.filteredOptions[idx]["" + this.optionValueName + ""];
            }
            event_handlers_service_1.EventHandlerService.emitDropdownOptionSelectedEvent(this.optionKeySelected, this.index);
            this.dropdownIsEmpty = false;
            this.dropdownIsOpen = false;
            if (this.dropdownSearchNeeded) {
                var filteredOptionSelected = this.filteredOptions[idx];
                this.optionFilterQ = "";
                this.filteredOptions = this.options;
                this.optionHoveredIndex = this.options.indexOf(filteredOptionSelected);
            }
            else {
                this.optionHoveredIndex = idx;
            }
        }
        else {
            this.optionValueSelected = this.clearAllLabel;
            this.dropdownIsEmpty = false;
            this.dropdownIsOpen = false;
            event_handlers_service_1.EventHandlerService.emitDropdownOptionSelectedEvent(-1, this.index);
            this.optionHoveredIndex = idx;
        }
        return false;
    };
    DropdownComponent.prototype.setActiveOption = function (idx) {
        this.optionHoveredIndex = idx;
    };
    DropdownComponent.prototype.toggleDropdownClick = function (event) {
        event.stopPropagation();
        this.dropdownIsOpen = !this.dropdownIsOpen;
        return false;
    };
    DropdownComponent.prototype.filterOptionOnKeyup = function () {
        var self = this;
        if (self.optionFilterQ) {
            self.filteredOptions = self.options.filter(function (x) { return x["" + self.optionValueName + ""].toLowerCase().indexOf(self.optionFilterQ.toLowerCase()) != -1; });
        }
        else {
            self.filteredOptions = self.options;
        }
    };
    DropdownComponent.prototype.ngOnDestroy = function () {
        if (this.unselectDropdownSub) {
            this.unselectDropdownSub.unsubscribe();
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], DropdownComponent.prototype, "options", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DropdownComponent.prototype, "optionKeyName", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DropdownComponent.prototype, "optionValueName", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DropdownComponent.prototype, "dropdownLabel", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], DropdownComponent.prototype, "dropdownSearchNeeded", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], DropdownComponent.prototype, "filteredOptions", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], DropdownComponent.prototype, "index", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], DropdownComponent.prototype, "dropdownForAccountListings", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], DropdownComponent.prototype, "selectOptionOnLoad", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], DropdownComponent.prototype, "optionIndexToBeSelectedOnLoad", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], DropdownComponent.prototype, "includeClearAllOption", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], DropdownComponent.prototype, "clearAllLabel", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], DropdownComponent.prototype, "hideSelectedLabel", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], DropdownComponent.prototype, "hideAvailableBalance", void 0);
    DropdownComponent = __decorate([
        core_1.Component({
            selector: 'ng-dropdown-control',
            templateUrl: "html/common/dropdown.html"
        }), 
        __metadata('design:paramtypes', [])
    ], DropdownComponent);
    return DropdownComponent;
}());
exports.DropdownComponent = DropdownComponent;
var CountryCodeDropdownComponent = (function () {
    // selectedCurrency: string;
    // selectedAvailableBalance: number;
    function CountryCodeDropdownComponent() {
        this.dropdownIsEmpty = true;
        this.dropdownIsOpen = false;
        this.optionKeySelected = '';
        this.optionValueSelected = this.optionFilterQ = '';
    }
    CountryCodeDropdownComponent.prototype.ngOnInit = function () {
        if (this.selectOptionOnLoad && this.optionIndexToBeSelectedOnLoad >= 0) {
            this.selectOption(this.optionIndexToBeSelectedOnLoad, null);
        }
        if (this.selectOptionOnLoad && this.optionCodeToBeSelectedOnLoad) {
            this.filteredOptions.indexOf;
            this.selectOption(this.optionIndexToBeSelectedOnLoad, null);
        }
        var self = this;
        this.clearCountryCodeSelectedSub = event_handlers_service_1.EventHandlerService.ClearCountryCodeDropdownEventPublisher
            .subscribe(function (object) {
            self.dropdownIsEmpty = true;
            self.dropdownIsOpen = false;
            self.optionKeySelected = '';
            self.optionHoveredIndex = 0;
        });
    };
    CountryCodeDropdownComponent.prototype.selectOption = function (idx, event) {
        if (event != null) {
            event.stopPropagation();
        }
        this.optionKeySelected = this.filteredOptions[idx]["" + this.optionKeyName + ""];
        this.optionValueSelected = this.filteredOptions[idx]["" + this.optionValueName + ""];
        console.log("filtered options" + this.filteredOptions[idx]);
        console.log("filtered options index" + this.index);
        event_handlers_service_1.EventHandlerService.emitDropdownOptionSelectedEvent(this.optionKeySelected, this.index);
        this.dropdownIsEmpty = false;
        this.dropdownIsOpen = false;
        if (this.dropdownSearchNeeded) {
            var filteredOptionSelected = this.filteredOptions[idx];
            this.optionFilterQ = "";
            this.filteredOptions = this.options;
            this.optionHoveredIndex = this.options.indexOf(filteredOptionSelected);
        }
        else {
            this.optionHoveredIndex = idx;
        }
        return false;
    };
    CountryCodeDropdownComponent.prototype.setActiveOption = function (idx) {
        this.optionHoveredIndex = idx;
    };
    CountryCodeDropdownComponent.prototype.toggleDropdownClick = function (event) {
        event.stopPropagation();
        this.dropdownIsOpen = !this.dropdownIsOpen;
        return false;
    };
    CountryCodeDropdownComponent.prototype.filterOptionOnKeyup = function () {
        var self = this;
        if (self.optionFilterQ) {
            self.filteredOptions = self.options.filter(function (x) { return x["" + self.optionValueName + ""].toLowerCase().indexOf(self.optionFilterQ.toLowerCase()) != -1; });
        }
        else {
            self.filteredOptions = self.options;
        }
    };
    CountryCodeDropdownComponent.prototype.ngOnDestroy = function () {
        if (this.clearCountryCodeSelectedSub) {
            this.clearCountryCodeSelectedSub.unsubscribe();
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], CountryCodeDropdownComponent.prototype, "options", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CountryCodeDropdownComponent.prototype, "optionKeyName", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CountryCodeDropdownComponent.prototype, "optionValueName", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CountryCodeDropdownComponent.prototype, "dropdownLabel", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], CountryCodeDropdownComponent.prototype, "dropdownSearchNeeded", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], CountryCodeDropdownComponent.prototype, "filteredOptions", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], CountryCodeDropdownComponent.prototype, "index", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], CountryCodeDropdownComponent.prototype, "selectOptionOnLoad", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], CountryCodeDropdownComponent.prototype, "optionIndexToBeSelectedOnLoad", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], CountryCodeDropdownComponent.prototype, "optionCodeToBeSelectedOnLoad", void 0);
    CountryCodeDropdownComponent = __decorate([
        core_1.Component({
            selector: 'ng-country-dropdown-control',
            templateUrl: "html/common/dropdown.countrycode.html"
        }), 
        __metadata('design:paramtypes', [])
    ], CountryCodeDropdownComponent);
    return CountryCodeDropdownComponent;
}());
exports.CountryCodeDropdownComponent = CountryCodeDropdownComponent;
var SchedulePaymentFrequencyDropdownComponent = (function () {
    // selectedCurrency: string;
    // selectedAvailableBalance: number;
    function SchedulePaymentFrequencyDropdownComponent() {
        this.dropdownIsEmpty = true;
        this.dropdownIsOpen = false;
        this.optionKeySelected = '';
        this.optionValueSelected = this.optionFilterQ = '';
    }
    SchedulePaymentFrequencyDropdownComponent.prototype.ngOnInit = function () {
        if (this.selectOptionOnLoad && this.optionIndexToBeSelectedOnLoad >= 0) {
            this.selectOption(this.optionIndexToBeSelectedOnLoad, null);
        }
        if (this.selectOptionOnLoad && this.optionCodeToBeSelectedOnLoad) {
            this.filteredOptions.indexOf;
            this.selectOption(this.optionIndexToBeSelectedOnLoad, null);
        }
        var self = this;
        this.unselectScheduleFrequencySelectedSub = event_handlers_service_1.EventHandlerService.UnselectScheduleDropdownEventPublisher
            .subscribe(function (object) {
            self.dropdownIsEmpty = true;
            self.dropdownIsOpen = false;
            self.optionKeySelected = '';
            self.optionHoveredIndex = 0;
        });
    };
    SchedulePaymentFrequencyDropdownComponent.prototype.selectOption = function (idx, event) {
        if (event != null) {
            event.stopPropagation();
        }
        this.optionKeySelected = this.filteredOptions[idx]["" + this.optionKeyName + ""];
        this.optionValueSelected = this.filteredOptions[idx]["" + this.optionValueName + ""];
        console.log("filtered options" + this.filteredOptions[idx]);
        console.log("filtered options index" + this.index);
        event_handlers_service_1.EventHandlerService.emitDropdownOptionSelectedEvent(this.optionKeySelected, this.index);
        this.dropdownIsEmpty = false;
        this.dropdownIsOpen = false;
        if (this.dropdownSearchNeeded) {
            var filteredOptionSelected = this.filteredOptions[idx];
            this.optionFilterQ = "";
            this.filteredOptions = this.options;
            this.optionHoveredIndex = this.options.indexOf(filteredOptionSelected);
        }
        else {
            this.optionHoveredIndex = idx;
        }
        return false;
    };
    SchedulePaymentFrequencyDropdownComponent.prototype.setActiveOption = function (idx) {
        this.optionHoveredIndex = idx;
    };
    SchedulePaymentFrequencyDropdownComponent.prototype.toggleDropdownClick = function (event) {
        event.stopPropagation();
        this.dropdownIsOpen = !this.dropdownIsOpen;
        return false;
    };
    SchedulePaymentFrequencyDropdownComponent.prototype.filterOptionOnKeyup = function () {
        var self = this;
        if (self.optionFilterQ) {
            self.filteredOptions = self.options.filter(function (x) { return x["" + self.optionValueName + ""].toLowerCase().indexOf(self.optionFilterQ.toLowerCase()) != -1; });
        }
        else {
            self.filteredOptions = self.options;
        }
    };
    SchedulePaymentFrequencyDropdownComponent.prototype.ngOnDestroy = function () {
        if (this.unselectScheduleFrequencySelectedSub) {
            this.unselectScheduleFrequencySelectedSub.unsubscribe();
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "options", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "optionKeyName", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "optionValueName", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "dropdownLabel", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "dropdownSearchNeeded", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Array)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "filteredOptions", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "index", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "selectOptionOnLoad", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Number)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "optionIndexToBeSelectedOnLoad", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "optionCodeToBeSelectedOnLoad", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', Boolean)
    ], SchedulePaymentFrequencyDropdownComponent.prototype, "hideSelectedLabel", void 0);
    SchedulePaymentFrequencyDropdownComponent = __decorate([
        core_1.Component({
            selector: 'ng-schedule-dropdown-control',
            templateUrl: "html/common/dropdown.schedule.html"
        }), 
        __metadata('design:paramtypes', [])
    ], SchedulePaymentFrequencyDropdownComponent);
    return SchedulePaymentFrequencyDropdownComponent;
}());
exports.SchedulePaymentFrequencyDropdownComponent = SchedulePaymentFrequencyDropdownComponent;
//# sourceMappingURL=dropdown.component.js.map