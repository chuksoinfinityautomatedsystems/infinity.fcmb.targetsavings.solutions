"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var utils_services_1 = require("../services/utils.services");
var BeneficiaryNameAbbreviationPipe = (function () {
    function BeneficiaryNameAbbreviationPipe() {
    }
    BeneficiaryNameAbbreviationPipe.prototype.transform = function (value) {
        if (!utils_services_1.UtilService.StringIsNullOrEmpty(value)) {
            // let wordsInValue = value.split(' ');
            if (value && value.length > 0) {
                var nonEmptyWordsInArray = value.split(' ').filter(function (c) { return c != ''; });
                if (nonEmptyWordsInArray && nonEmptyWordsInArray.length > 1) {
                    return nonEmptyWordsInArray[0][0] + nonEmptyWordsInArray[nonEmptyWordsInArray.length - 1][0];
                }
                return nonEmptyWordsInArray[0][0];
            }
        }
        return '';
    };
    BeneficiaryNameAbbreviationPipe = __decorate([
        core_1.Pipe({ name: 'AbbreviateBeneficiaryName' }), 
        __metadata('design:paramtypes', [])
    ], BeneficiaryNameAbbreviationPipe);
    return BeneficiaryNameAbbreviationPipe;
}());
exports.BeneficiaryNameAbbreviationPipe = BeneficiaryNameAbbreviationPipe;
// @Pipe({ name: 'GetIssueType' })
// export class GetIssueTypePipe implements PipeTransform {
//     transform(value: number) {
//         if (value == 1) {
//             return "Request";
//         }
//         else {
//             return "Complaint";
//         }
//     }
// }
// @Pipe({ name: 'GetBusinessFunction' })
// export class GetIsCustomerPipe implements PipeTransform {
//     transform(value: number) {
//         if (value == 1) {
//             return "PBB";
//         }
//         else {
//             return "CIB";
//         }
//     }
// }
// @Pipe({ name: 'GetBusinessFunction3' })
// export class GetIsCustomerPipe3 implements PipeTransform {
//     transform(value: number) {
//         if (value == 1) {
//             return "PBB";
//         }
//         else {
//             return "CIB";
//         }
//     }
// }
// @Pipe({ name: 'YesOrNo2' })
// export class YesOrNoPipe2 implements PipeTransform {
//     transform(value: number) {
//         if (value == 1) {
//             return "Yes";
//         }
//         else {
//             return "No";
//         }
//     }
// }
// @Pipe({ name: 'YesOrNo3' })
// export class YesOrNoPipe3 implements PipeTransform {
//     transform(value: number) {
//         if (value == 1) {
//             return "Yes";
//         }
//         else {
//             return "No";
//         }
//     }
// }
// @Pipe({ name: 'GetIssueType2' })
// export class GetIssueTypePipe2 implements PipeTransform {
//     transform(value: number) {
//         if (value == 1) {
//             return "Request";
//         }
//         else {
//             return "Complaint";
//         }
//     }
// }
// @Pipe({ name: 'GetBusinessFunction2' })
// export class GetIsCustomerPipe2 implements PipeTransform {
//     transform(value: number) {
//         if (value == 1) {
//             return "PBB";
//         }
//         else {
//             return "CIB";
//         }
//     }
// }
// @Pipe({ name: 'GetIssueType3' })
// export class GetIssueTypePipe3 implements PipeTransform {
//     transform(value: number) {
//         if (value == 1) {
//             return "Request";
//         }
//         else {
//             return "Complaint";
//         }
//     }
// }
// @Pipe({ name: 'FormatNumberWithComma' })
// export class FormatNumberWithCommaPipe implements PipeTransform {
//     transform(value: number) {
//         if (value != undefined) {
//             var parts = value.toString().split(".");
//             parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
//             return parts.join(".");
//         }
//         else {
//             return value;
//         }
//     }
// }
//# sourceMappingURL=custom_pipes.pipe.js.map