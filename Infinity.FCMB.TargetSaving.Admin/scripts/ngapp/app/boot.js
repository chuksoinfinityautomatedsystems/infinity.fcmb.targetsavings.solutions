"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var authentication_module_1 = require("./authentication/authentication.module");
var authentication_routes_1 = require("./authentication/authentication.routes");
var app_component_1 = require("./app.component");
//  Routes
var app_routing_1 = require("./app.routing");
var static_module_1 = require("./static/static.module");
var static_routes_1 = require("./static/static.routes");
var dashboard_module_1 = require("./dashboard/dashboard.module");
var dashboard_routes_1 = require("./dashboard/dashboard.routes");
var customer_module_1 = require("./customers/customer.module");
var customer_routes_1 = require("./customers/customer.routes");
var event_handlers_service_1 = require("./commons/services/event_handlers.service");
var settings_routes_1 = require("./settings/settings.routes");
var settings_module_1 = require("./settings/settings.module");
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule, forms_1.FormsModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule,
                authentication_module_1.AuthenticationPagesModule, dashboard_module_1.DashboardModule, dashboard_routes_1.dashboardRoutes,
                customer_module_1.CustomerModule, customer_routes_1.customerRoutes, static_module_1.StaticPagesModule,
                authentication_routes_1.authenticationRoutes, static_routes_1.staticPageRoutes, settings_module_1.SettingsModule, settings_routes_1.settingsRoutes,
                app_routing_1.rootRoutes],
            declarations: [app_component_1.AppComponent],
            providers: [event_handlers_service_1.EventHandlerService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=boot.js.map