import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule }      from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';   

import { NotFoundComponent }  from './components/notfound.component';   

@NgModule({
  imports:      [ CommonModule, ReactiveFormsModule, HttpModule, RouterModule ],
  declarations: [  NotFoundComponent ],
  providers: []
})
export class StaticPagesModule { }
