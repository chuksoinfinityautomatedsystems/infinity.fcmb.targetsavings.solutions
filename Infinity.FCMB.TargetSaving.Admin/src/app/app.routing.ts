import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { NotFoundComponent } from "./static/components/notfound.component";

export const rootRoutes = RouterModule.forRoot([
    { path: "**", component: NotFoundComponent }
]);