import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AuthenticationComponent } from './pages/authentication/authentication.component';
import { rootRoutes } from './app.routing';
import { AuthenticationPagesModule } from './pages/authentication/authentication.module';
import { authenticationRoutes } from './pages/authentication/authentication.routes';

@NgModule({
  declarations: [
    AppComponent,
    //AuthenticationComponent
  ],
  imports: [
    BrowserModule,
    AuthenticationPagesModule,

    authenticationRoutes,
    rootRoutes
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
