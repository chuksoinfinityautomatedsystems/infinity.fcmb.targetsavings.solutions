

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthenticationComponent } from './authentication.component';

export const authenticationRoutes = RouterModule.forChild([
    {
        path: "Login",
        component: AuthenticationComponent,
        pathMatch: 'full'
    },
]);