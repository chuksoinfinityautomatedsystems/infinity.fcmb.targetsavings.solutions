﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.EntityFramework.Extensions;
using System.Text;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.TargetSavings.Customer.API.Helpers;

namespace Infinity.FCMB.TargetSavings.Customer.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Utility")]
    [ResponseCache(CacheProfileName = "PrivateCache")]
    [ServiceFilter(typeof(ValidateModelAttributeFilter))]
    public class UtilityController : Controller
    {
        private IUtilityService utilityService { get; set; }
        private IApplicationLogService applicationLogService { get; set; }

        public UtilityController(IUtilityService utilityService, IApplicationLogService applicationLogService)
        {
            this.utilityService = utilityService;
            this.applicationLogService = applicationLogService;
        }
        [HttpPost]
        [Route("CreateCategory/")]
        public ResponseObject CreateCategory([FromBody] CategoryApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Utility/CreateCategory");
                Category category = new Category
                {
                    Image = Encoding.UTF8.GetBytes(apiRequest.Image),
                    Name=apiRequest.Name
                };
                ActionReturn result = utilityService.AddCategory(category);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("UpdateCategory/")]
        public ResponseObject UpdateCategory([FromBody] Category category)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, category, "api/Utility/UpdateCategory");
                ActionReturn result = utilityService.UpdateCategory(category);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("RemoveCategory/")]
        public ResponseObject RemoveCategory([FromBody] int id)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, id, "api/Utility/RemoveCategory");
                ActionReturn result = utilityService.RemoveCategory(id);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetCategory/")]
        public async Task<ResponseObject> GetCategory()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/GetCategory");
                ActionReturn result = await utilityService.GetCategory();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        /*
        //[HttpPost]
        //[Route("CreateTargetResultNote/")]
        public ResponseObject AddTargetResultNote([FromBody] TargetResultNote targetResultNote)
        {
            ResponseObject response = new ResponseObject();
            try
            {

                ActionReturn result = utilityService.AddTargetResultNote(targetResultNote);
                if (result.Flag)
                {
                    response.ApiResponse.ResponseCode = "00";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = "Success";
                    response.ResponseItem = result.ReturnedObject;
                }
                else
                {
                    response.ApiResponse.ResponseCode = "01";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = "Failed";
                    response.ResponseItem = result.ReturnedObject;

                }

            }
            catch (Exception ex)
            {
                response.ApiResponse.ResponseCode = "02";
                response.ApiResponse.ResponseDescription = "Exception";
                response.ApiResponse.FriendlyMessage = "Failed";
                response.ResponseItem = null;
            }
            return response;
        }

        //[HttpPost]
        //[Route("UpdateTargetResultNote/")]
        public ResponseObject UpdateTargetResultNote([FromBody] TargetResultNote targetResultNote)
        {
            ResponseObject response = new ResponseObject();
            try
            {

                ActionReturn result = utilityService.UpdateTargetResultNote(targetResultNote);
                if (result.Flag)
                {
                    response.ApiResponse.ResponseCode = "00";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = "Success";
                    response.ResponseItem = result.ReturnedObject;
                }
                else
                {
                    response.ApiResponse.ResponseCode = "01";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = "Failed";
                    response.ResponseItem = result.ReturnedObject;

                }

            }
            catch (Exception ex)
            {
                response.ApiResponse.ResponseCode = "02";
                response.ApiResponse.ResponseDescription = "Exception";
                response.ApiResponse.FriendlyMessage = "Failed";
                response.ResponseItem = null;
            }
            return response;
        }

        //[HttpPost]
        //[Route("RemoveTargetResultNote/")]
        public ResponseObject RemoveTargetResultNote([FromBody] int targetResultNoteId)
        {
            ResponseObject response = new ResponseObject();
            try
            {

                ActionReturn result = utilityService.RemoveTargetResultNote(targetResultNoteId);
                if (result.Flag)
                {
                    response.ApiResponse.ResponseCode = "00";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = "Success";
                    response.ResponseItem = result.ReturnedObject;
                }
                else
                {
                    response.ApiResponse.ResponseCode = "01";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = "Failed";
                    response.ResponseItem = result.ReturnedObject;

                }

            }
            catch (Exception ex)
            {
                response.ApiResponse.ResponseCode = "02";
                response.ApiResponse.ResponseDescription = "Exception";
                response.ApiResponse.FriendlyMessage = "Failed";
                response.ResponseItem = null;
            }
            return response;
        }

        //[HttpPost]
        //[Route("GetTargetResultNote/")]
        public async Task<ResponseObject> GetTargetResultNote()
        {
            ResponseObject response = new ResponseObject();
            try
            {
                ActionReturn result = await utilityService.GetTargetResultNote();
                if (result.Flag)
                {
                    response.ApiResponse.ResponseCode = "00";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = "Success";
                    response.ResponseItem = result.ReturnedObject;
                }
                else
                {
                    response.ApiResponse.ResponseCode = "01";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = "Failed";
                    response.ResponseItem = result.ReturnedObject;

                }

            }
            catch (Exception ex)
            {
                response.ApiResponse.ResponseCode = "02";
                response.ApiResponse.ResponseDescription = "Exception";
                response.ApiResponse.FriendlyMessage = "Failed";
                response.ResponseItem = null;
            }
            return response;
        }
        */

        [HttpPost]
        [Route("CreteTargetSavingsFrequency/")]
        public ResponseObject AddTargetSavingsFrequency([FromBody] TargetSavingsFrequencyApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Utility/CreteTargetSavingsFrequency");
                TargetSavingsFrequency targetSavingsFrequency = new TargetSavingsFrequency
                {
                    Id = apiRequest.Id,
                    Frequency=apiRequest.Frequency,
                    IsActive=apiRequest.IsActive,
                    ValueInDays=apiRequest.ValueInDays
                };
                ActionReturn result = utilityService.AddTargetSavingsFrequency(targetSavingsFrequency);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("UpdateTargetSavingsFrequency/")]
        public ResponseObject UpdateTargetSavingsFrequency([FromBody] TargetSavingsFrequencyApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Utility/UpdateTargetSavingsFrequency");
                TargetSavingsFrequency targetSavingsFrequency = new TargetSavingsFrequency
                {
                    Id = apiRequest.Id,
                    Frequency = apiRequest.Frequency,
                    IsActive = apiRequest.IsActive,
                    ValueInDays = apiRequest.ValueInDays
                };
                ActionReturn result = utilityService.UpdateTargetSavingsFrequency(targetSavingsFrequency);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("RemoveTargetSavingsFrequency/")]
        public ResponseObject RemoveTargetSavingsFrequency([FromBody] int targetSavingsFrequencyId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, targetSavingsFrequencyId, "api/Utility/RemoveTargetSavingsFrequency");
                ActionReturn result = utilityService.RemoveTargetSavingsFrequency(targetSavingsFrequencyId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetTargetSavingsFrequency/")]
        public async Task<ResponseObject> GetTargetSavingsFrequency()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/GetTargetSavingsFrequency");
                ActionReturn result = await utilityService.GetTargetSavingsFrequency();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }


        [HttpPost]
        [Route("CreateTargetSavingsRequestType/")]
        public ResponseObject AddTargetSavingsRequestType([FromBody] TargetSavingsRequestTypeApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Utility/CreateTargetSavingsRequestType");
                if (ModelState.IsValid)
                {
                    TargetSavingsRequestType targetSavingsRequestType = new TargetSavingsRequestType {
                        RequestType=apiRequest.RequestType,
                        IsLiquidation=apiRequest.IsLiquidation
                    };
                    ActionReturn result = utilityService.AddTargetSavingsRequestType(targetSavingsRequestType);
                    response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);
                }
                else
                {
                    response.ApiResponse.ResponseCode = "04";
                    response.ApiResponse.ResponseDescription = "Invalid inputs";
                    response.ApiResponse.FriendlyMessage = "Failed";
                    response.ResponseItem ="Request Type is required";
                }
                

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("UpdateTargetSavingsRequestType/")]
        public ResponseObject UpdateTargetSavingsRequestType([FromBody] TargetSavingsRequestTypeApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Utility/UpdateTargetSavingsRequestType");
                if (ModelState.IsValid)
                {
                    TargetSavingsRequestType targetSavingsRequestType = new TargetSavingsRequestType
                    {
                        RequestType = apiRequest.RequestType,
                        RequestTypeId = apiRequest.RequestTypeId

                    };
                    ActionReturn result = utilityService.UpdateTargetSavingsRequestType(targetSavingsRequestType);
                    response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);
                }
                else
                {
                    response.ApiResponse.ResponseCode = "04";
                    response.ApiResponse.ResponseDescription = "Invalid inputs";
                    response.ApiResponse.FriendlyMessage = "Failed";
                    response.ResponseItem = "Request Type and Request Type Id are required";
                }
                    

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("RemoveTargetSavingsRequestType/")]
        public ResponseObject RemoveTargetSavingsRequestType([FromBody] int targetSavingsRequestTypeId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, targetSavingsRequestTypeId, "api/Utility/RemoveTargetSavingsRequestType");
                ActionReturn result = utilityService.RemoveTargetSavingsRequestType(targetSavingsRequestTypeId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetTargetSavingsRequestType/")]
        public async Task<ResponseObject> GetTargetSavingsRequestType()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/GetTargetSavingsRequestType");
                ActionReturn result = await utilityService.GetTargetSavingsRequestType();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetWebPaymentKey/")]
        public async Task<ResponseObject> GetWebPaymentKey()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/GetWebPaymentKey");
                ActionReturn result = await utilityService.GetWebPaymentKey();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetInternalWebPaymentKey/")]
        public async Task<ResponseObject> GetInternalWebPaymentKey()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/GetInternalWebPaymentKey");
                ActionReturn result = await utilityService.GetInternalWebPaymentKey();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("UpdateWebPaymentKey/")]
        public ResponseObject UpdateWebPaymentKey([FromBody] TargetWebPaymentProfile targetWebPaymentProfile)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, targetWebPaymentProfile, "api/Utility/UpdateWebPaymentKey");
                if (ModelState.IsValid)
                {
                    
                    ActionReturn result = utilityService.UpdateWebPaymentKey(targetWebPaymentProfile);
                    response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);
                }
                else
                {
                    response.ApiResponse.ResponseCode = "04";
                    response.ApiResponse.ResponseDescription = "Invalid inputs";
                    response.ApiResponse.FriendlyMessage = "Failed";
                    response.ResponseItem = "Request Type and Request Type Id are required";
                }


            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("SetupCustomerPendingMandates/")]
        public ResponseObject SetupCustomerPendingMandates()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/SetupCustomerPendingMandates");
                ActionReturn result = utilityService.SetupCustomerPendingMandates();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetBanks/")]
        public async Task<ResponseObject> GetBanks()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/GetBanks");
                ActionReturn result = await utilityService.GetBanks();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetBankList/")]
        public async Task<ResponseObject> GetBankList()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/GetBankList");
                ActionReturn result = await utilityService.GetBanksFromFCMB();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("RequestMandateValidationOTP/")]
        public ResponseObject RequestMandateValidationOTP([FromBody] long targetGoalId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/RequestMandateValidationOTP");
                ActionReturn result = utilityService.RequestMandateValidationOTP(targetGoalId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("ValidateMandateOTP/")]
        public ResponseObject ValidateMandateOTP([FromBody] ValidateMandateOTPApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Utility/ValidateMandateOTP");
                ActionReturn result = utilityService.ValidateMandateOTP(apiRequest.TargetGoalId,apiRequest.RemitaTransRef, apiRequest.Otp,apiRequest.CardLast4Digit);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetLiquidationAccountsByCustomerAsync/")]
        public async Task<ResponseObject> GetLiquidationAccountsByCustomerAsync([FromBody] long customerId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/Utility/GetLiquidationAccountsByCustomerAsync");
                ActionReturn result = await utilityService.GetLiquidationAccountsByCustomerAsync(customerId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

    }
}