﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Remita;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.TargetSavings.Customer.API.Helpers;
using Infinity.FCMB.TargetSavings.Domain.Models.FCMB;

namespace Infinity.FCMB.TargetSavings.Customer.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Transactions")]
    [ResponseCache(CacheProfileName = "PrivateCache")]
    [ServiceFilter(typeof(ValidateModelAttributeFilter))]
    public class TransactionsController : Controller
    {
        private ITransactionService transactionService { get; set; }
        private IApplicationLogService applicationLogService { get; set; }

        public TransactionsController(ITransactionService transactionService, IApplicationLogService applicationLogService)
        {
            this.transactionService = transactionService;
            this.applicationLogService = applicationLogService;
        }

        [HttpPost]
        [Route("CreateTransction/")]
        public ResponseObject CreateTransction([FromBody] TransactionApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Transactions/CreateTransction");
                Transaction transaction = new Transaction
                {
                    Amount = apiRequest.Amount,
                    Narration = apiRequest.Narration,
                    TargetGoalsId = apiRequest.TargetGoalId,
                    TransactionDate=DateTime.Now.Date,
                    TransactionType=apiRequest.TransactionType,
                    ApprovedBy=apiRequest.ApprovedBy,
                    ApprovedDate=apiRequest.ApprovedDate,
                    PostedBy=apiRequest.PostedBy,
                    PostedDate=apiRequest.PostedDate
                };
                ActionReturn result = transactionService.AddTransaction(transaction);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNTransactionByCustomerProfileId/")]
        public async Task<ResponseObject> GetNTransactionByCustomerProfileId([FromBody]GetNObjectByIdApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Transactions/GetNTransactionByCustomerProfileId");
                ActionReturn result = await transactionService.GetNTransactionByCustomerProfileId(apiRequest.Pagination, apiRequest.Id);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNTransactionByTargetId/")]
        public async Task<ResponseObject> GetNTransactionByTargetId([FromBody]GetNObjectByIdApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Transactions/GetNTransactionByTargetId");
                ActionReturn result = await transactionService.GetNTransactionByTargetId(apiRequest.Pagination, apiRequest.Id);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNTransactions/")]
        public async Task<ResponseObject> GetNTransactions([FromBody]Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/Transactions/GetNTransactions");
                ActionReturn result = await transactionService.GetNTransactions(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNPendingTransactions/")]
        public async Task<ResponseObject> GetNPendingTransactions([FromBody]Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/Transactions/GetNPendingTransactions");
                ActionReturn result = await transactionService.GetNPendingTransactions(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNFailedTransactionSchedule/")]
        public async Task<ResponseObject> GetNFailedTransactionSchedule([FromBody]Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/Transactions/GetNFailedTransactionSchedule");
                ActionReturn result = await transactionService.GetNFailedTransactionSchedule(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("PostTransactionNotification/")]
        public ResponseObject PostTransactionNotification([FromBody]StandingOrderNotification standingOrderNotification)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, standingOrderNotification, "api/Transactions/PostTransactionNotification");
                ActionReturn result = transactionService.PostTransactionNotification(standingOrderNotification);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("DirectCreditTest/")]
        public ResponseObject DirectCreditTest([FromBody] DirectCreditRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Transactions/DirectCreditTest");
                
                ActionReturn result = transactionService.DirectCreditTest(apiRequest);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }
    }
} 