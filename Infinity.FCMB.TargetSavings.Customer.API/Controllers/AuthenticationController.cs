﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.TargetSavings.Customer.API.Helpers;
using System.Text;
using Infinity.FCMB.TargetSavings.Domain.Models.Customer;

namespace Infinity.FCMB.TargetSavings.Customer.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Authentication")]
    [ResponseCache(CacheProfileName = "PrivateCache")]
    [ServiceFilter(typeof(ValidateModelAttributeFilter))]
    public class AuthenticationController : Controller
    {
        private ICustomerService customerService { get; set; }
        private IUtilityService utilityService { get; set; }
        private IApplicationLogService applicationLogService { get; set; }

        public AuthenticationController(ICustomerService customerService, IUtilityService utilityService, IApplicationLogService applicationLogService)
        {
            this.customerService = customerService;
            this.utilityService = utilityService;
            this.applicationLogService = applicationLogService;
        }

        [HttpPost]
        [Route("AuthenticateCustomer/")]
        public ResponseObject AuthenticateCustomer([FromBody] AuthenticateApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Authentication/AuthenticateCustomer");

                ActionReturn result = customerService.ValidateUser(apiRequest.UserName,apiRequest.Password);
                responseItemToLog.Add(result);
                if (result.Flag)
                {
                    CustomerProfile customerProfile = (CustomerProfile)result.ReturnedObject;
                    customerProfile.Password = null;
                    response.ApiResponse.ResponseCode = "00";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = result.Message;
                    response.ResponseItem = customerProfile;
                }
                else
                {
                    response.ApiResponse.FriendlyMessage = result.Message;
                    response = ApiUtility.GetFailedRequestResponse(response, result);
                }

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("AuthenticateAdminUser/")]
        public ResponseObject AuthenticateAdminUser([FromBody] AuthenticateApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Authentication/AuthenticateAdminUser");

                //byte[] passwordInByte = System.Convert.FromBase64String(apiRequest.Password);
                //string password = Encoding.UTF8.GetString(passwordInByte);

                ActionReturn result = utilityService.ValidateAdminUser(apiRequest.UserName, apiRequest.Password);
                responseItemToLog.Add(result);
                if (result.Flag)
                {
                    response.ApiResponse.ResponseCode = "00";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = result.Message;
                    response.ResponseItem = result.ReturnedObject;
                }
                else
                {
                    response.ApiResponse.FriendlyMessage = result.Message;
                    response = ApiUtility.GetFailedRequestResponse(response, result);

                }

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("ResetCustomerPassword/")]
        public ResponseObject ResetCustomerPassword([FromBody] string username)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, username, "api/Authentication/ResetCustomerPassword");
                ActionReturn result = customerService.ResetPassword(username);
                responseItemToLog.Add(result);
                if (result.Flag)
                {
                    if (result.HasEmail)
                    {
                        EmailLog emailLog = (EmailLog)result.EmailLog;
                        Dictionary<string, string> emailValues = (Dictionary<string, string>)result.EmailValues;
                        applicationLogService.AddEmailToLog(emailLog, emailValues);
                    }
                    response.ApiResponse.ResponseCode = "00";
                    response.ApiResponse.ResponseDescription = result.Message;
                    response.ApiResponse.FriendlyMessage = result.Message;
                    response.ResponseItem = result.ReturnedObject;
                }
                else
                {
                    response.ApiResponse.FriendlyMessage = result.Message;
                    response = ApiUtility.GetFailedRequestResponse(response, result);
                }
                
            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("ChangePassword/")]
        public ResponseObject ChangePassword([FromBody] ChangePasswordApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/Authentication/ChangePassword");
                if (ModelState.IsValid)
                {
                    ActionReturn result = customerService.ChangePassword(apiRequest.CustomerProfileId, apiRequest.OldPassword, apiRequest.NewPassword);
                    responseItemToLog.Add(result);
                    if (result.Flag)
                    {
                        response.ApiResponse.ResponseCode = "00";
                        response.ApiResponse.ResponseDescription = result.Message;
                        response.ApiResponse.FriendlyMessage = result.Message;
                        response.ResponseItem = result.ReturnedObject;
                    }
                    else
                    {
                        response.ApiResponse.FriendlyMessage = result.Message;
                        response = ApiUtility.GetFailedRequestResponse(response, result);
                    }
                }
                else
                {
                    List<string> modelErrors = new List<string>();
                    foreach (var modelState in ModelState.Values)
                    {
                        foreach (var modelError in modelState.Errors)
                        {
                            modelErrors.Add(modelError.ErrorMessage);
                        }
                    }
                    response.ApiResponse.ResponseCode = "04";
                    response.ApiResponse.ResponseDescription = "Invalid inputs";
                    response.ApiResponse.FriendlyMessage = "Failed: Invalid inputs";
                    response.ResponseItem = modelErrors;
                    responseItemToLog.Add(modelErrors);
                }
                

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

    }
}