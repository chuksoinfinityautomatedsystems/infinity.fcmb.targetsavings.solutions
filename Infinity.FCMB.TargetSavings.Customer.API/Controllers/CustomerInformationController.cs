﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Infinity.FCMB.EntityFramework.Extensions;
using System.Text;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.TargetSavings.Customer.API.Helpers;
using Microsoft.Extensions.Configuration;

namespace Infinity.FCMB.TargetSavings.Customer.API.Controllers
{
    [Produces("application/json")]
    [Route("api/CustomerInformation")]
    [ResponseCache(CacheProfileName = "PrivateCache")]
    [ServiceFilter(typeof(ValidateModelAttributeFilter))]
    public class CustomerInformationController : Controller
    {
        private ICustomerService  customerService { get; set; }
        private IApplicationLogService applicationLogService { get; set; }
        public IConfiguration Configuration { get; }

        public CustomerInformationController(ICustomerService customerService, IApplicationLogService applicationLogService, IConfiguration Configuration)
        {
            this.customerService = customerService;
            this.applicationLogService = applicationLogService;
            this.Configuration = Configuration;
        }

        [HttpPost]
        [Route("CreateProspectiveCustomer/")]
        public ResponseObject CreateProspectiveCustomer([FromBody] ProspectiveCustomer prospectiveCustomer)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, prospectiveCustomer, "api/CustomerInformation/CreateProspectiveCustomer");

                ActionReturn validationResult = customerService.ValidateProspectData(prospectiveCustomer.EmailAddress.ToLower(), prospectiveCustomer.PhoneNumber);
                responseItemToLog.Add(validationResult);
                if (validationResult.Flag && validationResult.FlagDescription == Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString())
                {
                    prospectiveCustomer.EmailAddress = prospectiveCustomer.EmailAddress.ToLower();
                    ActionReturn result = customerService.AddProspectiveCustomer(prospectiveCustomer);
                    responseItemToLog.Add(result);
                    if (result.Flag)
                    {
                        response.ApiResponse.ResponseCode = "00";
                        response.ApiResponse.ResponseDescription = result.Message;
                        response.ApiResponse.FriendlyMessage = result.Message;
                        response.ResponseItem = result.ReturnedObject;
                    }
                    else
                    {
                        response.ApiResponse.FriendlyMessage = result.Message;
                        response = ApiUtility.GetFailedRequestResponse(response, result);

                    }
                }
                else
                {

                    response.ApiResponse.FriendlyMessage = validationResult.Message;
                    response = ApiUtility.GetFailedRequestResponse(response, validationResult);
                }
                

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetPropectiveCustomers/")]
        public async Task<ResponseObject> GetPropectiveCustomers()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/CustomerInformation/GetPropectiveCustomers");
                ActionReturn result = await customerService.GetAllProspectiveCustomerAsync();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNProspectiveCustomers/")]
        public async Task<ResponseObject> GetNProspectiveCustomers([FromBody] Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/CustomerInformation/GetNProspectiveCustomers");
               
                ActionReturn result = await customerService.GetNProspectiveCustomersAsync(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        
        [HttpPost]
        [Route("GetPropectiveCustomerById/")]
        public async Task<ResponseObject> GetPropectiveCustomerById([FromBody] long prospectId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, prospectId, "api/CustomerInformation/GetPropectiveCustomerById");
                ActionReturn result = await customerService.GetProspectiveCustomerById(prospectId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetProspectsByStatus/")]
        public async Task<ResponseObject> GetProspectsByStatus([FromBody] string status)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, status, "api/CustomerInformation/GetProspectsByStatus");
                ActionReturn result = await customerService.GetProspectsByStatus(status);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetDailyProspects/")]
        public async Task<ResponseObject> GetDailyProspects([FromBody]GetDailyProspectsApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/GetDailyProspects");
                ActionReturn result = await customerService.GetDailyProspects(apiRequest.CreatedDate, apiRequest.Status);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetProspectsCount/")]
        public async Task<ResponseObject> GetProspectsCount()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/CustomerInformation/GetProspectsCount");
                ActionReturn result = await customerService.GetProspectsCount();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);
            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        

        [HttpPost]
        [Route("CreateCustomerProfile/")]
        public ResponseObject CreateCustomerProfile([FromBody] CustomerProfileApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/CreateCustomerProfile");
                CustomerProfile customerProfile = new CustomerProfile
                {
                    Address=apiRequest.Address,
                    BiometricVerificationNo=apiRequest.BiometricVerificationNo,
                    ComputerDetails=apiRequest.ComputerDetails,
                    EmailAddress=apiRequest.EmailAddress.ToLower(),
                    FirstName=apiRequest.FirstName,
                    IsFCMBCustomer=apiRequest.IsFCMBCustomer,
                    NextOfKinEmail= string.IsNullOrEmpty(apiRequest.NextOfKinEmail)?"NA": apiRequest.NextOfKinEmail,
                    NextOfKinFullName=string.IsNullOrEmpty(apiRequest.NextOfKinFullName)?"NA": apiRequest.NextOfKinFullName,
                    NextOfKinPhone=string.IsNullOrEmpty(apiRequest.NextOfKinPhone)?"NA": apiRequest.NextOfKinPhone,
                    Password= Encoding.UTF8.GetBytes(apiRequest.Password),
                    PhoneNumber=apiRequest.PhoneNumber,
                    ProfilePicture= Encoding.UTF8.GetBytes(apiRequest.ProfilePicture),
                    ProfileStatus=Enumerators.STATUS.NEW.ToString(),
                    ProspectId=apiRequest.ProspectId,
                    Surname=apiRequest.Surname,
                    UserName=apiRequest.UserName.ToLower(),
                    
                };
                ActionReturn validationResult = customerService.ValidateNewCustomerData(customerProfile.EmailAddress, customerProfile.PhoneNumber, customerProfile.UserName);
                if (validationResult.Flag && validationResult.FlagDescription == Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString())
                {
                    ActionReturn result = customerService.AddCustomerProfile(customerProfile);
                    response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);
                }
                else
                {
                    
                    response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, validationResult);
                }
                

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetCustomerProfile/")]
        public async Task<ResponseObject> GetCustomerProfile()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/CustomerInformation/GetCustomerProfile");
                ActionReturn result = await customerService.GetAllCustomerProfileAsync();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNCustomerProfile/")]
        public async Task<ResponseObject> GetNCustomerProfile([FromBody] Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/CustomerInformation/GetNCustomerProfile");
                ActionReturn result = await customerService.GetNCustomerProfileAsync(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetCustomerProfileById/")]
        public async Task<ResponseObject> GetCustomerProfileById([FromBody]long customerId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, customerId, "api/CustomerInformation/GetCustomerProfileById");
                ActionReturn result = await customerService.GetCustomerProfileById(customerId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("UpdateCustomerProfile/")]
        public ResponseObject UpdateCustomerProfile([FromBody] CustomerProfileApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/UpdateCustomerProfile");
                CustomerProfile customerProfile = new CustomerProfile
                {
                    Address = apiRequest.Address,
                    BiometricVerificationNo = apiRequest.BiometricVerificationNo,
                    ComputerDetails = apiRequest.ComputerDetails,
                    EmailAddress = apiRequest.EmailAddress.ToLower(),
                    FirstName = apiRequest.FirstName,
                    IsFCMBCustomer = apiRequest.IsFCMBCustomer,
                    NextOfKinEmail = apiRequest.NextOfKinEmail,
                    NextOfKinFullName = apiRequest.NextOfKinFullName,
                    NextOfKinPhone = apiRequest.NextOfKinPhone,
                    Password = Encoding.UTF8.GetBytes(apiRequest.Password),
                    PhoneNumber = apiRequest.PhoneNumber,
                    ProfilePicture = Encoding.UTF8.GetBytes(apiRequest.ProfilePicture),
                    ProfileStatus = apiRequest.ProfileStatus,
                    ProspectId = apiRequest.ProspectId,
                    Surname = apiRequest.Surname,
                    UserName = apiRequest.UserName.ToLower(),

                };
                ActionReturn result = customerService.UpdateCustomerProfile(customerProfile);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetDailyCustomerProfiles/")]
        public async Task<ResponseObject> GetDailyCustomerProfiles([FromBody]GetDailyCustomerProfilesApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/GetDailyCustomerProfiles");
                ActionReturn result = await customerService.GetDailyCustomerProfiles(apiRequest.CreatedDate, apiRequest.Status);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetCustomerProfilesCount/")]
        public async Task<ResponseObject> GetCustomerProfilesCount()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/CustomerInformation/GetCustomerProfilesCount");
                ActionReturn result = await customerService.GetCustomerProfilesCount();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetCustomerRegistrationStatus/")]
        public ResponseObject GetCustomerRegistrationStatus([FromBody] CustomerRegistrationStatusApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/GetCustomerRegistrationStatus");
                
                ActionReturn result = customerService.GetCustomerRegistrationStatus(apiRequest.EmailAddress,apiRequest.PhoneNumber);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("CreateTargetGoalWithCard/")]
        public ResponseObject CreateTargetGoalWithCard([FromBody]TargetGoalApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/CreateTargetGoalWithCard");
                TargetGoal targetGoal = new TargetGoal
                {
                    CustomerProfileId=apiRequest.CustomerProfileId,
                    GoalBalance=apiRequest.GoalBalance,
                    GoalCardTokenDetails=apiRequest.GoalCardTokenDetails,
                    GoalDebitAccountNo=apiRequest.GoalDebitAccountNo,
                    GoalDebitBank=apiRequest.GoalDebitBank,
                    GoalDebitType=apiRequest.GoalDebitType,
                    GoalDescription=apiRequest.GoalDescription,
                    GoalEndDate= Convert.ToDateTime(apiRequest.GoalEndDate).Date,
                    GoalName=apiRequest.GoalName,
                    GoalnterestEarned=0,
                    GoalStartAmount= Decimal.Round(apiRequest.GoalStartAmount, 2),
                    GoalStartDate=Convert.ToDateTime(apiRequest.GoalStartDate).Date,
                    GoalStatus=Enumerators.STATUS.NEW.ToString(),
                    TargetAmount= Decimal.Round(apiRequest.TargetAmount, 2),
                    TargetFrequency=apiRequest.TargetFrequency,
                    TargetFrequencyAmount= Decimal.Round(apiRequest.TargetFrequencyAmount, 2),
                    TargetGoalPics=Encoding.UTF8.GetBytes(apiRequest.TargetGoalPics)  ,
                    NextRunDate= Convert.ToDateTime(apiRequest.GoalStartDate).Date,
                    LiquidationAccount=apiRequest.LiquidationAccount,
                    LiquidationAccountBankCode=apiRequest.LiquidationAccountBankCode,
                    LiquidationMode=apiRequest.LiquidationMode
                };
                //string SMSConnectionString= Configuration["Data:DbConnection:SMSConnectionString"];
                ActionReturn result = customerService.AddTargetGoalWithCard(targetGoal);
                if (result.Flag)
                {

                    if (result.HasEmail)
                    {
                        EmailLog emailLog = (EmailLog)result.EmailLog;
                        Dictionary<string,string> emailValues=(Dictionary<string, string>)result.EmailValues;
                        applicationLogService.AddEmailToLog(emailLog, emailValues);
                    }
                }
                
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
                response.ResponseItem = ex.ToString();
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("CreateTargetGoalWithAccount/")]
        public ResponseObject CreateTargetGoalWithAccount([FromBody]TargetGoalApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/CreateTargetGoalWithAccount");
                TargetGoal targetGoal = new TargetGoal
                {
                    CustomerProfileId = apiRequest.CustomerProfileId,
                    GoalBalance = apiRequest.GoalBalance,
                    GoalCardTokenDetails = apiRequest.GoalCardTokenDetails,
                    GoalDebitAccountNo = apiRequest.GoalDebitAccountNo,
                    GoalDebitBank = apiRequest.GoalDebitBank,
                    GoalDebitType = apiRequest.GoalDebitType,
                    GoalDescription = apiRequest.GoalDescription,
                    GoalEndDate = Convert.ToDateTime(apiRequest.GoalEndDate).Date,
                    GoalName = apiRequest.GoalName,
                    GoalnterestEarned = 0,
                    GoalStartAmount = Decimal.Round(apiRequest.GoalStartAmount, 2),
                    GoalStartDate = Convert.ToDateTime(apiRequest.GoalStartDate).Date,
                    GoalStatus = Enumerators.STATUS.NEW.ToString(),
                    TargetAmount = Decimal.Round(apiRequest.TargetAmount, 2),
                    TargetFrequency = apiRequest.TargetFrequency,
                    TargetFrequencyAmount = Decimal.Round(apiRequest.TargetFrequencyAmount, 2),
                    TargetGoalPics = Encoding.UTF8.GetBytes(apiRequest.TargetGoalPics),
                    NextRunDate = Convert.ToDateTime(apiRequest.GoalStartDate).Date,
                    LiquidationAccount = apiRequest.LiquidationAccount,
                    LiquidationAccountBankCode = apiRequest.LiquidationAccountBankCode,
                    LiquidationMode = apiRequest.LiquidationMode
                };
                //string SMSConnectionString= Configuration["Data:DbConnection:SMSConnectionString"];
                ActionReturn result = customerService.AddTargetGoalWithAccount(targetGoal);
                if (result.Flag)
                {

                    if (result.HasEmail)
                    {
                        EmailLog emailLog = (EmailLog)result.EmailLog;
                        Dictionary<string, string> emailValues = (Dictionary<string, string>)result.EmailValues;
                        applicationLogService.AddEmailToLog(emailLog, emailValues);
                    }
                }

                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
                response.ResponseItem = ex.ToString();
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("ValidateAccountOTP/")]
        public ResponseObject ValidateAccountOTP([FromBody]ValidateAccountOTPApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/ValidateMandateOTP");
                ActionReturn result = customerService.ValidateOTP(apiRequest.AccountNumber, apiRequest.Otp, apiRequest.CustomerProfileId, apiRequest.ReferenceId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);
                if (result.Flag)
                {

                    if (result.HasEmail)
                    {
                        EmailLog emailLog = (EmailLog)result.EmailLog;
                        Dictionary<string, string> emailValues = (Dictionary<string, string>)result.EmailValues;
                        applicationLogService.AddEmailToLog(emailLog, emailValues);
                    }
                }
                if (result.HasMultipleLog)
                {
                    ActionReturn actionReturn = (ActionReturn)result.MultipleLog;
                    applicationLogService.Add(actionReturn);
                }
                responseItemToLog.Add(result.ServiceRequest);
                responseItemToLog.Add(result.ServiceResponse);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
                response.ResponseItem = ex.ToString();
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
                
            }
            return response;
        }

        [HttpPost]
        [Route("CreateTargetGoalWithValidatedAccount/")]
        public ResponseObject CreateTargetGoalWithValidatedAccount([FromBody]TargetGoalApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/CreateTargetGoalWithValidatedAccount");
                TargetGoal targetGoal = new TargetGoal
                {
                    CustomerProfileId = apiRequest.CustomerProfileId,
                    GoalBalance = apiRequest.GoalBalance,
                    GoalCardTokenDetails = apiRequest.GoalCardTokenDetails,
                    GoalDebitAccountNo = apiRequest.GoalDebitAccountNo,
                    GoalDebitBank = apiRequest.GoalDebitBank,
                    GoalDebitType = apiRequest.GoalDebitType,
                    GoalDescription = apiRequest.GoalDescription,
                    GoalEndDate = Convert.ToDateTime(apiRequest.GoalEndDate).Date,
                    GoalName = apiRequest.GoalName,
                    GoalnterestEarned = 0,
                    GoalStartAmount = Decimal.Round(apiRequest.GoalStartAmount, 2),
                    GoalStartDate = Convert.ToDateTime(apiRequest.GoalStartDate).Date,
                    GoalStatus = Enumerators.STATUS.NEW.ToString(),
                    TargetAmount = Decimal.Round(apiRequest.TargetAmount, 2),
                    TargetFrequency = apiRequest.TargetFrequency,
                    TargetFrequencyAmount = Decimal.Round(apiRequest.TargetFrequencyAmount, 2),
                    TargetGoalPics = Encoding.UTF8.GetBytes(apiRequest.TargetGoalPics),
                    NextRunDate = Convert.ToDateTime(apiRequest.GoalStartDate).Date,
                    LiquidationAccount = apiRequest.LiquidationAccount,
                    LiquidationAccountBankCode = apiRequest.LiquidationAccountBankCode,
                    LiquidationMode = apiRequest.LiquidationMode
                };
                //string SMSConnectionString= Configuration["Data:DbConnection:SMSConnectionString"];
                ActionReturn result = customerService.AddTargetGoalWithAccountWithoutOtp(targetGoal);
                if (result.Flag)
                {

                    if (result.HasEmail)
                    {
                        EmailLog emailLog = (EmailLog)result.EmailLog;
                        Dictionary<string, string> emailValues = (Dictionary<string, string>)result.EmailValues;
                        applicationLogService.AddEmailToLog(emailLog, emailValues);
                    }
                }
                responseItemToLog.Add(result.ServiceRequest);
                responseItemToLog.Add(result.ServiceResponse);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
                response.ResponseItem = ex.ToString();
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("SelfTopUp/")]
        public ResponseObject SelfTopUp([FromBody]TopupApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/SelfTopUp");
                
                ActionReturn result = customerService.SelfTopUp(apiRequest.TargetGoalId,apiRequest.CardToken,apiRequest.Amount,apiRequest.IsCardTransaction);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);
                if (result.Flag)
                {

                    if (result.HasEmail)
                    {
                        EmailLog emailLog = (EmailLog)result.EmailLog;
                        Dictionary<string, string> emailValues = (Dictionary<string, string>)result.EmailValues;
                        applicationLogService.AddEmailToLog(emailLog, emailValues);
                    }
                }
                if (result.HasMultipleLog)
                {
                    ActionReturn actionReturn = (ActionReturn)result.MultipleLog;
                    applicationLogService.Add(actionReturn);
                }
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
                response.ResponseItem = ex.ToString();
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);

            }
            return response;
        }

        [HttpPost]
        [Route("GetAllTargetGoal/")]
        public async Task<ResponseObject> GetAllTargetGoal()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/CustomerInformation/GetAllTargetGoal");
                ActionReturn result = await customerService.GetAllTargetGoalAsync();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNTargetGoalAsync/")]
        public async Task<ResponseObject> GetNTargetGoalAsync([FromBody]Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/CustomerInformation/GetNTargetGoalAsync");
                ActionReturn result = await customerService.GetNTargetGoalAsync(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetAllNTargetGoalAsync/")]
        public async Task<ResponseObject> GetAllNTargetGoalAsync([FromBody]Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/CustomerInformation/GetAllNTargetGoalAsync");
                ActionReturn result = await customerService.GetAllNTargetGoalAsync(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetTargetGoalById/")]
        public async Task<ResponseObject> GetTargetGoalById([FromBody]long targetId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, targetId, "api/CustomerInformation/GetTargetGoalById");
                ActionReturn result = await customerService.GetTargetGoalById(targetId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetAllTargetGoalsByCustomer/")]
        public async Task<ResponseObject> GetAllTargetGoalsByCustomer([FromBody]long customerId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, customerId, "api/CustomerInformation/GetAllTargetGoalsByCustomerAsync");
                ActionReturn result = await customerService.GetAllTargetGoalsByCustomerAsync(customerId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }
        [HttpPost]
        [Route("GetTargetGoalsByCustomer/")]
        public async Task<ResponseObject> GetTargetGoalsByCustomer([FromBody]long customerId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, customerId, "api/CustomerInformation/GetTargetGoalsByCustomer");
                ActionReturn result = await customerService.GetTargetGoalsByCustomerAsync(customerId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetCompletedTargetGoalsByCustomer/")]
        public async Task<ResponseObject> GetCompletedTargetGoalsByCustomer([FromBody]long customerId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, customerId, "api/CustomerInformation/GetCompletedTargetGoalsByCustomer");
                ActionReturn result = await customerService.GetCompletedTargetGoalsByCustomerAsync(customerId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }
        [HttpPost]
        [Route("GetDailyTargets/")]
        public async Task<ResponseObject> GetDailyTargets([FromBody]GetDailyTargetsApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/GetDailyTargets");
                ActionReturn result = await customerService.GetDailyTargets(apiRequest.CreatedDate, apiRequest.Status);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetTargetsCount/")]
        public async Task<ResponseObject> GetTargetsCount()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/CustomerInformation/GetTargetsCount");
                ActionReturn result = await customerService.GetTargetsCount();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetTotalTargetGoalAndBalanceByCustomer/")]
        public async Task<ResponseObject> GetTotalTargetGoalAndBalanceByCustomer([FromBody] long customerId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, customerId, "api/CustomerInformation/GetTotalTargetGoalAndBalanceByCustomer");
                ActionReturn result = await customerService.GetTotalTargetGoalAndBalanceByCustomer(customerId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetCompletedTotalTargetGoalAndBalanceByCustomer/")]
        public async Task<ResponseObject> GetCompletedTotalTargetGoalAndBalanceByCustomer([FromBody] long customerId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, customerId, "api/CustomerInformation/GetCompletedTotalTargetGoalAndBalanceByCustomer");
                ActionReturn result = await customerService.GetCompletedTotalTargetGoalAndBalanceByCustomer(customerId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("UpdateTargetPicture/")]
        public ResponseObject UpdateTargetPicture([FromBody]TargetGoalPictureApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/GetTotalTargetGoalAndBalanceByCustomer");
                TargetGoal targetGoal = new TargetGoal
                {
                    TargetGoalsId=apiRequest.TargetId,                 
                    TargetGoalPics = Encoding.UTF8.GetBytes(apiRequest.TargetGoalPics),
                };
                ActionReturn result = customerService.UpdateTargetPicture(targetGoal);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("UpdateLiquidationDetails/")]
        public ResponseObject UpdateLiquidationDetails([FromBody]LiquidationAccountDetailApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/UpdateLiquidationDetails");
                
                ActionReturn result = customerService.UpdateLiquidationDetails(apiRequest.TargetGoalId,apiRequest.LiquidationAccount,apiRequest.BankCode);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }



        [HttpPost]
        [Route("CreateTargetSavingsRequest/")]
        public ResponseObject CreateTargetSavingsRequest([FromBody]TargetSavingsRequestApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/CreateTargetSavingsRequest");
                TargetSavingsRequest targetSavingsRequest = new TargetSavingsRequest
                {
                    RequestDescription = apiRequest.RequestDescription,
                    RequestTypeId = apiRequest.RequestTypeId,
                    Status = Enumerators.STATUS.NEW.ToString(),
                    ApprovalStatus = Enumerators.APPROVAL_STATUS.NEW.ToString(),
                    TargetGoalId=apiRequest.TargetGoalId,
                    LiquidationAmount=0
                };
                if (apiRequest.IsLiquidation) targetSavingsRequest.LiquidationAmount = apiRequest.LiquidationAmount;
                ActionReturn result = customerService.AddTargetSavingsRequest(targetSavingsRequest);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetAllTargetSavingsRequest/")]
        public async Task<ResponseObject> GetAllTargetSavingsRequest()
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, null, "api/CustomerInformation/GetAllTargetSavingsRequest");
                ActionReturn result = await customerService.GetAllTargetSavingsRequestAsync();
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetTargetSavingsRequestById/")]
        public async Task<ResponseObject> GetTargetSavingsRequestById([FromBody]long requestId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, requestId, "api/CustomerInformation/GetTargetSavingsRequestById");
                ActionReturn result = await customerService.GetTargetSavingsRequestById(requestId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetTargetSavingsRequestByTargetGoal/")]
        public async Task<ResponseObject> GetTargetSavingsRequestByTargetGoal([FromBody]long targetId)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, targetId, "api/CustomerInformation/GetTargetSavingsRequestByTargetGoal");
                ActionReturn result = await customerService.GetTargetSavingsRequestByTargetGoalAsync(targetId);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNCustomerRequests/")]
        public async Task<ResponseObject> GetNCustomerRequests([FromBody]Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/CustomerInformation/GetNCustomerRequests");
                ActionReturn result = await customerService.GetNCustomerRequests(pagination );
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }


        [HttpPost]
        [Route("GetNRequestsByCustomerProfileId/")]
        public async Task<ResponseObject> GetNRequestsByCustomerProfileId([FromBody]GetNObjectByIdApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/GetNRequestsByCustomerProfileId");
                ActionReturn result = await customerService.GetNRequestsByCustomerProfileId(apiRequest.Pagination, apiRequest.Id);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("TreatCustomerRequests/")]
        public ResponseObject TreatCustomerRequests([FromBody]TreatTargetSavingsRequestApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/TreatCustomerRequests");
                if (ModelState.IsValid)
                {
                    string approved = Enumerators.APPROVAL_STATUS.APPROVED.ToString();
                    string rejected = Enumerators.APPROVAL_STATUS.REJECTED.ToString();
                    if (apiRequest.ApprovalStatus == approved || apiRequest.ApprovalStatus == rejected)
                    {
                        TargetSavingsRequest targetSavingsRequest = new TargetSavingsRequest
                        {
                            ApprovalStatus = apiRequest.ApprovalStatus,
                            RequestId = apiRequest.RequestId,
                            TreatedBy=apiRequest.TreatedBy,
                            DateTreated=DateTime.Now
                        };
                        ActionReturn result = customerService.TreatCustomerRequests(targetSavingsRequest);
                        response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);
                    }
                    else
                    {
                        response.ApiResponse.ResponseCode = "03";
                        response.ApiResponse.ResponseDescription = "Invalid approval status, must either be APPROVED or REJECTED";
                        response.ApiResponse.FriendlyMessage = "Failed";
                        response.ResponseItem = "Invalid approval status, must either be APPROVED or REJECTED";
                    }
                }
                else
                {
                    response.ApiResponse.ResponseCode = "04";
                    response.ApiResponse.ResponseDescription = "Invalid inputs";
                    response.ApiResponse.FriendlyMessage = "Failed";
                    response.ResponseItem = "Target id and approval status are required";
                }
                
                
                

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetTargetSavingsRequestCountByStatus/")]
        public async Task<ResponseObject> GetTargetSavingsRequestCountByStatus([FromBody]string status)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, status, "api/CustomerInformation/GetTargetSavingsRequestCountByStatus");
                ActionReturn result = await customerService.GetTargetSavingsRequestCountByStatus(status);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNRecentTargetSavingsRequest/")]
        public async Task<ResponseObject> GetNPendingTargetSavingsRequest([FromBody]Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/CustomerInformation/GetNPendingTargetSavingsRequest");
                ActionReturn result = await customerService.GetNRecentTargetSavingsRequest(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNTreatedTargetSavingsRequest/")]
        public async Task<ResponseObject> GetNTreatedTargetSavingsRequest([FromBody]Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/CustomerInformation/GetNTreatedTargetSavingsRequest");
                ActionReturn result = await customerService.GetNTreatedTargetSavingsRequest(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNNewTargetSavingsRequest/")]
        public async Task<ResponseObject> GetNNewTargetSavingsRequest([FromBody]Pagination pagination)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, pagination, "api/CustomerInformation/GetNNewTargetSavingsRequest");
                ActionReturn result = await customerService.GetNNewTargetSavingsRequest(pagination);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetDailyStatistics/")]
        public async Task<ResponseObject> GetDailyStatistics([FromBody]DateTime date)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, date, "api/CustomerInformation/GetDailyStatistics");
                ActionReturn result = await customerService.GetDailyStatistics(date.Date);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("ValidateBVN/")]
        public ResponseObject ValidateBVN([FromBody]ValidateBVNApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/ValidateBVN");
                ActionReturn result = customerService.ValidateBVN(apiRequest.Bvn, apiRequest.FirstName, apiRequest.LastName, apiRequest.PhoneNumber);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);


            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("NameEnquiry/")]
        public ResponseObject NameEnquiry([FromBody]NameEnquiryApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/NameEnquiry");
                ActionReturn result = customerService.NameEnquiry(apiRequest.AccountNumber,apiRequest.BankCode);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);


            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("CustomerInformationEnquiry/")]
        public ResponseObject CustomerInformationEnquiry([FromBody]string accountNumber)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, accountNumber, "api/CustomerInformation/CustomerInformationEnquiry");
                ActionReturn result = customerService.GetCustomerInformationByAccountNo(accountNumber);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);


            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("CreateLiquidationRequest/")]
        public ResponseObject CreateLiquidationRequest([FromBody]LiquidationRequestApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/CreateLiquidationRequest");

                ActionReturn result = new ActionReturn();
                if (apiRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_INTEREST.ToString()
                    || apiRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                {
                    result = customerService.AddCompletedLiquidationRequest(apiRequest.RequestType, apiRequest.TargetGoalId);
                }
                else if (apiRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString()
                    || apiRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                {
                    result = customerService.AddNonCompletedLiquidationRequest(apiRequest.RequestType, apiRequest.TargetGoalId,apiRequest.Amount);
                }
                else
                {
                    result.ReturnedObject = "Invalid request type. Request types are; FULL_COMPLETED_INTEREST, FULL_COMPLETED_PRINCIPAL_AND_INTEREST, FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST, PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST";
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Your liquidation request was not logged.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }                
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);
            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("GetNLiquidationRequestsByCustomerProfileId/")]
        public async Task<ResponseObject> GetNLiquidationRequestsByCustomerProfileId([FromBody]GetNObjectByIdApiRequest apiRequest)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, apiRequest, "api/CustomerInformation/GetNLiquidationRequestsByCustomerProfileId");
                ActionReturn result = await customerService.GetNLiquidationRequestsByCustomerProfileId(apiRequest.Pagination, apiRequest.Id);
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }

        [HttpPost]
        [Route("StopTargetSavings/")]
        public async Task<ResponseObject> StopTargetSavings([FromBody]long id)
        {
            ResponseObject response = new ResponseObject();
            List<object> responseItemToLog = new List<object>();
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                serviceLog = ApiUtility.SetHttpRequestLog(HttpContext, id, "api/CustomerInformation/StopTargetSavings");
                
                ActionReturn result = await customerService.UpdateTargetGoalExtentedStatus(id, Enumerators.EXTENDED_STATUS.STOP.ToString());
                response = ApiUtility.GetSuccessAndFailedResponse(response, responseItemToLog, result);

            }
            catch (Exception ex)
            {
                responseItemToLog.Add(ex.ToString());
                response = ApiUtility.GetExceptionResponse(response);
            }
            finally
            {
                //Log request and response
                serviceLog = ApiUtility.SetHttpResponseLog(serviceLog, responseItemToLog, response);
                ActionReturn logResult = applicationLogService.AddServiceLog(serviceLog);
            }
            return response;
        }
    }
}
