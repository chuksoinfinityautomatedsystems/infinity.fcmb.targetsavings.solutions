﻿using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Data;
using Infinity.FCMB.TargetSavings.Services.Implementation;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Customer.API
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, TargetSavingsDBContext>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ITransactionService, TransactionService>();
            services.AddScoped<IUtilityService, UtilityService>();
            services.AddScoped<IApplicationLogService, ApplicationLogService>();

            return services;
        }
    }
}
