﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.TargetSavings.Services.Helper;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Customer.API.Helpers
{
    public static class ApiUtility
    {
        public static ServiceRequestLog SetHttpRequestLog(HttpContext context, object apiRequest, string host)
        {
            ServiceRequestLog serviceLog = new ServiceRequestLog();
            try
            {
                string moduleId = context.Request.Headers["ModuleId"];
                string channel = context.Request.Headers["Channel"];
                string referer = context.Request.Headers["Referer"];
                string origin = context.Request.Headers["Origin"];
                string agent = context.Request.Headers["User-Agent"];

                serviceLog.ChannelId = channel;
                serviceLog.ModuleId = moduleId;
                serviceLog.Origin = origin;
                serviceLog.Referer = referer;
                serviceLog.RequestDateTime = DateTime.Now;
                serviceLog.RequestHost = host;
                serviceLog.UserAgent = agent;
                serviceLog.Request = apiRequest == null ? "" : Util.ConvertObjectToJson(apiRequest);
            }
            catch (Exception ex)
            {

            }
            return serviceLog;

        }
        public static ServiceRequestLog SetHttpResponseLog(ServiceRequestLog serviceLog, List<object> responseItemToLog, ResponseObject response)
        {
            serviceLog.InternalResponses = Util.ConvertObjectToJson(responseItemToLog);
            serviceLog.ResponseDate = DateTime.Now;
            serviceLog.Response = Util.ConvertObjectToJson(response);
            return serviceLog;
        }

        public static ResponseObject GetFailedRequestResponse(ResponseObject response, ActionReturn result)
        {

            response.ApiResponse.ResponseCode = CONSTANT.FAILED_RESPONSE;
            response.ResponseItem = result.ReturnedObject;
            if (result.FlagDescription == Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString())
            {
                response.ApiResponse.ResponseCode = CONSTANT.EXCEPTION_RESPONSE;
                response.ApiResponse.FriendlyMessage = "We could not complete your request at the moment, kindly try again later.";
                response.ResponseItem = null;
            }
            response.ApiResponse.ResponseDescription = result.Message;
            return response;
        }

        public static ResponseObject GetFailedValidationRequestResponse(ResponseObject response, ActionReturn validationResult)
        {
            response.ApiResponse.ResponseCode = CONSTANT.VALIDATION_ERROR_RESPONSE;
            response.ApiResponse.ResponseDescription = validationResult.Message;
            response.ApiResponse.FriendlyMessage = "Some or all of the input values supplied are not valid, kindly correct and retry.";
            response.ResponseItem = validationResult.ReturnedObject;
            return response;
        }

        public static ResponseObject GetExceptionResponse(ResponseObject response)
        {
            response.ApiResponse.ResponseCode = CONSTANT.API_EXCEPTION_RESPONSE;
            response.ApiResponse.ResponseDescription = Enumerators.FLAG_DESCRIPTION.API_EXCEPTION.ToString();
            response.ApiResponse.FriendlyMessage = "System is currently not available right now, kindly try again later.";
            response.ResponseItem = null;
            return response;
        }

        public static ResponseObject GetSuccessAndFailedResponse(ResponseObject response, List<object> responseItemToLog, ActionReturn result)
        {
            responseItemToLog.Add(result);
            if (result.Flag)
            {
                response.ApiResponse.ResponseCode = "00";
                response.ApiResponse.ResponseDescription = result.Message;
                response.ApiResponse.FriendlyMessage = result.Message;
                response.ResponseItem = result.ReturnedObject;
            }
            else
            {
                response.ApiResponse.FriendlyMessage = result.Message;
                response = ApiUtility.GetFailedRequestResponse(response, result);

            }

            return response;
        }
    }
}
