﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Data;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Infinity.FCMB.TargetSavings.Customer.API.Middleware;
using Swashbuckle.AspNetCore.Swagger;
using Newtonsoft.Json;

namespace Infinity.FCMB.TargetSavings.Customer.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            AppSettings.ConnectionString = Configuration["Data:DbConnection:ConnectionString"];
            services.AddScoped(provider =>
            {
                return new TargetSavingsDBContext(AppSettings.ConnectionString);
            });

            services.AddMvc().AddJsonOptions(o => {
                o.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.None;
            });
            services.AddMvc(option => {
                option.CacheProfiles.Add("PrivateCache",
                new CacheProfile()
                {
                    NoStore = true,
                    Duration = 0,
                    Location = ResponseCacheLocation.None
                });
            });
            services.AddCors(options =>
            {
                string originConfig = Configuration["TokenSettings:origins"];
                string[] origins = originConfig.Split(',');
                options.AddPolicy("AllowSpecificOrigin",
                    //builder => builder.WithOrigin(origins)
                    builder =>builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());

            });

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory("AllowSpecificOrigin"));
            });
            services.AddMvc();
            services.AddScoped<ValidateModelAttributeFilter>();
            services.RegisterServices();
            services.AddSingleton<IConfiguration>(Configuration);

            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.TryAddSingleton<IActionContextAccessor, ActionContextAccessor>();

            services.AddDistributedMemoryCache();

            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Target Goal Savings API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors("AllowSpecificOrigin");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseResponseHeaderMiddleware(new SecurityHeadersBuilder()
            .AddCustomHeader("X-Frame-Options", "DENY")
            .AddCustomHeader("X-XSS-Protection", "1; mode=block")
            .AddCustomHeader("X-Content-Type-Options", "nosniff")
            .AddCustomHeader("Strict-Transport-Security", "max-age=0")
            .RemoveHeader("X-Powered-By")
            .RemoveHeader("X-SourceFiles")
            .RemoveHeader("Server")
            .RemoveHeader("Transfer-Encoding")

            );

            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Target Goal Savings API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseMvc();
        }
    }
}
