﻿using System.Threading;
using System.Threading.Tasks;
using Infinity.FCMB.EntityFramework.Extensions.Infrastructure;

namespace Infinity.FCMB.EntityFramework.Extensions.Repository
{
    public interface IRepositoryAsync<TEntity> : IRepository<TEntity> where TEntity : class, IObjectState
    {
        Task<TEntity> FindAsync(params object[] keyValues);
        Task<TEntity> FindAsync(CancellationToken cancellationToken, params object[] keyValues);
        Task<bool> DeleteAsync(params object[] keyValues);
        Task<bool> DeleteAsync(CancellationToken cancellationToken, params object[] keyValues);
    }
}
