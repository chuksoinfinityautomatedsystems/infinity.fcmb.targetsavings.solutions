﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infinity.FCMB.EntityFramework.Extensions.Repository
{
    public interface IRepository<TEntity> : IDisposable
         where TEntity : class
    {
        void Add(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);


        TEntity Get(int id);
        TEntity Get(long id);

        IQueryable<TEntity> All { get; }
        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);
        TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate);


        IQueryable<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate);
        
        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);

        void Deactivate(TEntity entity);
        void DeactivateRange(IEnumerable<TEntity> entities);

        void Update(TEntity entity);
        void UpdateRange(IEnumerable<TEntity> entities);

        bool IsValid<T>(TEntity model, out ActionReturn<T> actionReturn);
        bool IsValid(TEntity model, out ActionReturn actionReturn); 
    }
}
