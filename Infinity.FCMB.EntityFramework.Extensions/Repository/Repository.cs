﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Infinity.FCMB.EntityFramework.Extensions.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context;

        public Repository(DbContext context)
        {
            Context = context;
        }

        public virtual IQueryable<TEntity> All => GetAllQuerable();

        private IQueryable<TEntity> GetAllQuerable()
        {
            return Context.Set<TEntity>();
              //  .Where(x => x.IsActive == 1);
        }

        public TEntity Get(int id)
        {
            return Context.Set<TEntity>().Find(id);
        }
        public TEntity Get(long id)
        {
            return Context.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {

            return All.ToList();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return All.Where(predicate).ToList();
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await All.Where(predicate).ToListAsync();
        }

        public IQueryable<TEntity> FindAll(Expression<Func<TEntity, bool>> predicate)
        {
            return All.Where(predicate);
        }
         

        public TEntity SingleOrDefault(Expression<Func<TEntity, bool>> predicate)
        {
            return All.SingleOrDefault(predicate);
        }

        public void Add(TEntity entity)
        {
            if(entity == null)
            {
                throw new ArgumentNullException("entity", "Null reference.");
            }
            //entity.Timestamp = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            //entity.IsActive = 1;
            Context.Set<TEntity>().Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
            {
                return;
            }

            //foreach(var _entity in entities)
            //{
            //    _entity.Timestamp = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            //    _entity.IsActive = 1;
            //}

            var enumerable = entities as TEntity[] ?? entities.ToArray();
            if (enumerable.Any())
            {
                Context.Set<TEntity>().AddRange(enumerable);
            }
        }


        public void Deactivate(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Null reference.");
            }

            //entity.Timestamp = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            //entity.IsActive = 0;
            Context.Entry(entity).State = EntityState.Modified; 
        }

        public void DeactivateRange(IEnumerable<TEntity> entities)
        {
            if (entities == null)
            {
                return;
            }

            foreach (var _entity in entities)
            {
                //_entity.Timestamp = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                //_entity.IsActive = 0;
                Context.Entry(_entity).State = EntityState.Modified;
            }             
        }



        public void Remove(TEntity entity)
        {
            Context.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            Context.Set<TEntity>().RemoveRange(entities);
        }


        public void Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity", "Null reference.");
            }

            //entity.Timestamp = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            Context.Entry(entity).State = EntityState.Modified;
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            if(entities == null)
            {
                return;
            }

            //foreach (var _entity in entities)
            //{
            //    _entity.Timestamp = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            //}

            var enumerable = entities as TEntity[] ?? entities.ToArray();
            if (enumerable.Any())
            {
                foreach (var _entity in enumerable)
                {
                    Context.Entry(_entity).State = EntityState.Modified;
                }
            }
        }

        public bool IsValid<T>(TEntity model, out ActionReturn<T> result)
        {
            result = new ActionReturn<T>
            {
                ValidationErrorMessages = ValidateModel(model)
            };

            if (result.ValidationErrorMessages.Count > 0)
            {
                result.UpdateReturn = UpdateReturn.ValidationError;
                return false;
            }

            return true;
        }

        public bool IsValid(TEntity model, out ActionReturn result)
        {
            result = new ActionReturn
            {
                ValidationErrorMessages = ValidateModel(model)
            };

            if (result.ValidationErrorMessages.Count > 0)
            {
                result.UpdateReturn = UpdateReturn.ValidationError;
                return false;
            }

            return true;
        }
        public bool IsValidForMany(TEntity model, out ActionReturn result)
        {
            result = new ActionReturn
            {
                ValidationErrorMessages = ValidateModel(model)
            };

            if (result.ValidationErrorMessages.Count > 0)
            {
                result.UpdateReturn = UpdateReturn.ValidationError;
                return false;
            }

            return true;
        }

        Dictionary<string, string> ValidateModel2(TEntity model)
        {
            var validateIsNull = IsModelNull(model);
            if (validateIsNull.Count > 0)
            {
                return validateIsNull;
            }

            var validationContext = new ValidationContext(model, null, null);
            var validationResults = new List<ValidationResult>();
            Validator.TryValidateObject(model, validationContext, validationResults, true);

            return validationResults.ToDictionary(validationResult => validationResult.MemberNames.First(), validationResult => validationResult.ErrorMessage);
        }

        Dictionary<string, string> IsModelNull(TEntity model)
        {
            if (model == null)
            {
                return new Dictionary<string, string>(){
                    {"", "Invalid model detected."}
                };
            }

            return new Dictionary<string, string>();
        }


        Dictionary<string, string> ValidateModel(TEntity model)
        {
            var validateIsNull = IsModelNull(model);
            if (validateIsNull.Count > 0)
            {
                return validateIsNull;
            }

            var validationResults = new Dictionary<string, string>();

            foreach (var valdRes in Context.GetValidationErrors().Where(x => x.Entry.Entity is TEntity))
            {
                foreach (var error in valdRes.ValidationErrors)
                {
                    validationResults.Add(error.PropertyName, error.ErrorMessage);                 
                }
            }

            return validationResults;
        } 


        private bool disposed;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    Context?.Dispose();
                }
            }

            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

     
    }
}
