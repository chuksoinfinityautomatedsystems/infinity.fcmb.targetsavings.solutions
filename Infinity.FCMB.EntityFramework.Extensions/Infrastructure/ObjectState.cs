﻿

namespace Infinity.FCMB.EntityFramework.Extensions.Infrastructure
{
    public enum ObjectState
    {
        Unchanged,
        Added,
        Modified,
        Deleted
    }
}
