﻿

using System.ComponentModel.DataAnnotations.Schema;

namespace Infinity.FCMB.EntityFramework.Extensions.Infrastructure
{
    public interface IObjectState
    {
        [NotMapped]
        ObjectState ObjectState { get; set; }
    }
}
