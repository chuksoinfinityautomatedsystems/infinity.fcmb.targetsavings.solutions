﻿using System.Collections.Generic;

namespace Infinity.FCMB.EntityFramework.Extensions
{
    public interface IActionReturn
    {
        UpdateReturn UpdateReturn { get; set; }
        int NumberOfRecordsAffected { get; set; }
        string AuditLogId { get; set; }
        object ReturnedObject { get; set; }
        string Message { get; set; }
        IDictionary<string, string> ValidationErrorMessages { get; set; }
    }

    public interface IActionReturn<T> : IActionReturn
    {
        T Id { get; set; }
    }

    public class ActionReturn : IActionReturn
    {
        public ActionReturn()
        {
            UpdateReturn = UpdateReturn.UnknownError;
            ValidationErrorMessages = new Dictionary<string, string>();
        }
        public UpdateReturn UpdateReturn { get; set; }
        public int NumberOfRecordsAffected { get; set; }
        public string AuditLogId { get; set; }
        public object ReturnedObject { get; set; }
        public string Message { get; set; }
        public IDictionary<string, string> ValidationErrorMessages { get; set; }
        public bool Flag { get; set; }
        public string FlagDescription { get; set; }
        public string ClassName { get; set; }
        public string MethodName { get; set; }
        public object EmailLog { get; set; }
        public bool HasEmail { get; set; }
        public object EmailValues { get; set; }
        public string ServiceRequest { get; set; }
        public string ServiceResponse { get; set; }
        public object MultipleLog { get; set; }
        public bool HasMultipleLog { get; set; }


    }

    public class ActionReturn<T> : IActionReturn<T>
    {
        public ActionReturn()
        {
            UpdateReturn = UpdateReturn.UnknownError;
            ValidationErrorMessages = new Dictionary<string, string>();
        }

        public UpdateReturn UpdateReturn { get; set; }
        public int NumberOfRecordsAffected { get; set; }
        public string AuditLogId { get; set; }
        public object ReturnedObject { get; set; }
        public string Message { get; set; }
        public IDictionary<string, string> ValidationErrorMessages { get; set; }
        public T Id { get; set; }

        //public static implicit operator ActionReturn<T>(ActionReturn d) {
        //    return new ActionReturn<T> {
        //        UpdateReturn = d.UpdateReturn,
        //        NumberOfRecordsAffected = d.NumberOfRecordsAffected,
        //        ValidationErrorMessages = d.ValidationErrorMessages
        //    };
        //}

        //  User-defined conversion from double to Digit
        public static implicit operator ActionReturn(ActionReturn<T> d)
        {
            return new ActionReturn
            {
                UpdateReturn = d.UpdateReturn,
                NumberOfRecordsAffected = d.NumberOfRecordsAffected,
                ValidationErrorMessages = d.ValidationErrorMessages
            };
        }

        public static explicit operator ActionReturn<T>(ActionReturn d)
        {
            return new ActionReturn<T>
            {
                UpdateReturn = d.UpdateReturn,
                NumberOfRecordsAffected = d.NumberOfRecordsAffected,
                ValidationErrorMessages = d.ValidationErrorMessages
            };
        }
    }
}
