﻿
namespace Infinity.FCMB.EntityFramework.Extensions
{
    public enum UpdateReturn
    {
        ItemNotExist = 0,
        NoChange = 1,
        ConcurrencyViolation = 2,
        ItemCreated = 3,
        ItemUpdated = 4,
        ItemDeleted = 5,
        PasswordReset = 6,
        PasswordChanged = 7,
        ValidationError = 8,
        PasswordValidationError = 9,
        AccessViolation = 10,
        RuntimeException = 11,
        ItemDeactivated = 12,
        UnknownError = 30,
        ItemWasNotCreated=31,
        ItemWasNotUpdated = 32,
        ItemRetrieved=33,
        Exception=34,
        ItemWasNotDeleted=35
    }
}
