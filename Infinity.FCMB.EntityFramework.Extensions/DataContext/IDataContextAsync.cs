﻿
using System.Threading;
using System.Threading.Tasks;

namespace Infinity.FCMB.EntityFramework.Extensions.DataContext
{
    interface IDataContextAsync:IDataContext
    {
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        Task<int> SaveChangesAsync();
    }
}
