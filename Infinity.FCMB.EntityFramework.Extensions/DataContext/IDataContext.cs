﻿using System;
using Infinity.FCMB.EntityFramework.Extensions.Infrastructure;

namespace Infinity.FCMB.EntityFramework.Extensions.DataContext
{
    interface IDataContext: IDisposable
    {
        int SaveChanges();
        void SyncObjectState<TEntity>(TEntity entity) where TEntity : class, IObjectState;
        void SyncObjectsStatePostCommit();
    }
}
