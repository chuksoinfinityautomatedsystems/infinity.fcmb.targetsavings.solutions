﻿using System.Threading;
using System.Threading.Tasks;
using Infinity.FCMB.EntityFramework.Extensions.Infrastructure;
using Infinity.FCMB.EntityFramework.Extensions.Repository;

namespace Infinity.FCMB.EntityFramework.Extensions.UnitOfWork
{
    public interface IUnitOfWorkAsync : IUnitOfWork
    {
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        IRepositoryAsync<TEntity> RepositoryAsync<TEntity>() where TEntity : class, IObjectState;
    }
}
