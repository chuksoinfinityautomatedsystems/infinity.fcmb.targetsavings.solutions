﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Infinity.FCMB.EntityFramework.Extensions.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();

        Task<int> SaveChangesAsync();


        int ExecuteSqlCommand(string sql, params object[] parameters);

        Database Database { get; }

    }

}
