﻿using System.Collections.Generic;

namespace Infinity.FCMB.EntityFramework.Extensions
{
    public interface IPaged
    {
        int TotalItemsCount { get; set; }
        int TotalNumberOfPages { get; set; }
        int PageSize { get; set; }
        int PageNumber { get; set; }
        int StartIndex { get; set; }
    }

    public interface IPaged<T> : IPaged where T : class
    {
        List<T> Items { get; set; }
        string Keyword { get; set; }
    }

    public class Paged<T> : IPaged<T> where T : class
    {
        public int TotalItemsCount { get; set; }
        public int TotalNumberOfPages { get; set; }
        public int PageSize { get; set; }
        public int PageNumber { get; set; }
        public int StartIndex { get; set; }
        public string Keyword { get; set; }
        public List<T> Items { get; set; }
    }
}
