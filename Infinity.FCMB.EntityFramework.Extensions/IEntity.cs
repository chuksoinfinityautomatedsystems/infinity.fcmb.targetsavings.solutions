﻿namespace Infinity.FCMB.EntityFramework.Extensions
{
    public interface IEntity
    {
        string Timestamp { get; set; }
        int IsActive { get; set; }
    }
}
