﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Data;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.TargetSavings.Services.Implementation;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.SweepProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            Run();
        }

        private static void Run()
        {
            try
            {
                DateTime scheduledDate;
                string scheduledConfig = ConfigurationManager.AppSettings["ScheduleDate"];
                Console.WriteLine($"Fetching ScheduleDate from config file: {scheduledConfig}");
                if (!DateTime.TryParse(scheduledConfig, out scheduledDate))
                {
                    scheduledDate = DateTime.Today.Date.AddDays(-1);
                }


                AppSettings.ConnectionString = ConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;
                Console.WriteLine($"Fetching ConnectionString from config file: ");
                var serviceProvider = new ServiceCollection()
                .AddScoped(provider =>
                {
                    return new TargetSavingsDBContext(AppSettings.ConnectionString);
                })
                .AddScoped<IUnitOfWork, TargetSavingsDBContext>()
                .AddScoped<ICustomerService, CustomerService>()
                .AddScoped<IApplicationLogService, ApplicationLogService>()
                .AddScoped<ITransactionService, TransactionService>()
                .AddScoped<IUtilityService, UtilityService>()
                .BuildServiceProvider();

                var transactionService = serviceProvider.GetService<ITransactionService>();
                var customerService = serviceProvider.GetService<ICustomerService>();
                var logger = serviceProvider.GetService<IApplicationLogService>();

                ActionReturn result = transactionService.ProcessCardPaymentSweep(scheduledDate,CONSTANT.CARD_DEBIT_TYPE);
                if (result.Flag)
                {
                    if (result.HasEmail)
                    {
                        Dictionary<int, object> emailLogDictionary = (Dictionary<int, object>)result.EmailLog;
                        Dictionary<int, object> emailValuesDictionary = (Dictionary<int, object>)result.EmailValues;
                        for (int i = 1; i <= emailLogDictionary.Count(); i++)
                        {
                            EmailLog emailLog = (EmailLog)emailLogDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            Dictionary<string, string> emailValue = (Dictionary<string, string>)emailValuesDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            logger.AddEmailToLog(emailLog, emailValue);
                        }

                    }
                }
                logger.Add(result);
                ActionReturn chargeFeeResult = transactionService.ProcessSweepForCardFees(scheduledDate);
                if (chargeFeeResult.Flag)
                {
                    if (chargeFeeResult.HasEmail)
                    {
                        Dictionary<int, object> emailLogDictionary = (Dictionary<int, object>)chargeFeeResult.EmailLog;
                        Dictionary<int, object> emailValuesDictionary = (Dictionary<int, object>)chargeFeeResult.EmailValues;
                        for (int i = 1; i <= emailLogDictionary.Count(); i++)
                        {
                            EmailLog emailLog = (EmailLog)emailLogDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            Dictionary<string, string> emailValue = (Dictionary<string, string>)emailValuesDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            logger.AddEmailToLog(emailLog, emailValue);
                        }

                    }
                }
                logger.Add(chargeFeeResult);
                ActionReturn liquidationSweepResult = transactionService.ProcessLiquidationInterestSweep();                
                logger.Add(liquidationSweepResult);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.ToString()); Console.ReadLine();
            }
        }
    }
}
