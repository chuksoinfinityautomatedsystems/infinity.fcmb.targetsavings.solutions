﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Infinity.FCMB.EntityFramework.Extensions;

using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Services.Implementation;
using Microsoft.Extensions.DependencyInjection;
using Infinity.FCMB.TargetSavings.Data;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System.Security.Cryptography;
using System.IO;
using Infinity.FCMB.TargetSavings.Services.Helper;

namespace Infinity.FCMB.TargetSavings.TransactionProcess
{
    class Program
    {
        static void Main(string[] args)
        {
            //string IV = Guid.NewGuid().ToString().Replace("-", "") + DateTime.Now.ToString("yyyyMMddTHHmmssffff0100");
            Run();
            //Console.ReadLine();
            //GenerateKey();
            //ActionReturn result = Util.GenerateOtp();
            //ActionReturn verifyResult = Util.ValidateOtp((string)result.ReturnedObject);
        }

        static void GenerateKey()
        {
            using (RijndaelManaged myRijndael = new RijndaelManaged())
            {

                myRijndael.GenerateKey();
                myRijndael.GenerateIV();
                string iv = Convert.ToBase64String(myRijndael.IV);
                string key = Convert.ToBase64String(myRijndael.Key);

                byte[] result = null;
                string word = "Joscool";
                byte[] wordBytes = Encoding.UTF8.GetBytes(word);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (RijndaelManaged AES = new RijndaelManaged())
                    {
                        AES.Padding = PaddingMode.PKCS7;

                        AES.KeySize = 256;
                        AES.BlockSize = 128;
                        AES.Key = Encoding.UTF8.GetBytes(key);
                        AES.IV = Encoding.UTF8.GetBytes(iv);

                        AES.Mode = System.Security.Cryptography.CipherMode.CBC;

                        using (var cs = new CryptoStream(ms, AES.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(wordBytes, 0, wordBytes.Length);
                            cs.Close();
                        }
                        byte[] encryptedBytes = ms.ToArray();
                        result = encryptedBytes;
                        string encrypted= ByteArrayToString(encryptedBytes);

                        byte[] wordBytes2 = StringToByteArray(encrypted);
                        byte[] byteBuffer = new byte[wordBytes.Length];
                        using (MemoryStream ms2 = new MemoryStream())
                        {
                            using (var cs = new CryptoStream(ms2, AES.CreateDecryptor(), CryptoStreamMode.Write))
                            {
                                cs.Write(wordBytes2, 0, wordBytes2.Length);
                                cs.Close();
                            }
                            byte[] decryptedBytes = ms2.ToArray();
                            string decrypted= System.Text.Encoding.UTF8.GetString(decryptedBytes);
                        }
                    }
                }

            }
        }
        private static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b).ToString().ToLower();
            return hex.ToString();
        }
        private static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
            .Where(x => x % 2 == 0)
            .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
            .ToArray();
        }
        private static void Run()
        {
            try
            {
                DateTime scheduledDate;
                string scheduledConfig = ConfigurationManager.AppSettings["ScheduleDate"];
                string FileLogPath = ConfigurationManager.AppSettings["FileLogPath"];
                Console.WriteLine($"Fetching ScheduleDate from config file: {scheduledConfig}");
                if (!DateTime.TryParse(scheduledConfig, out scheduledDate))
                {
                    scheduledDate = DateTime.Today.Date;
                }


                AppSettings.ConnectionString = ConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;
                Console.WriteLine($"Fetching ConnectionString from config file: ");
                var serviceProvider = new ServiceCollection()
                .AddScoped(provider =>
                {
                    return new TargetSavingsDBContext(AppSettings.ConnectionString);
                })
                .AddScoped<IUnitOfWork, TargetSavingsDBContext>()
                .AddScoped<ICustomerService, CustomerService>()
                .AddScoped<IApplicationLogService, ApplicationLogService>()
                .AddScoped<ITransactionService, TransactionService>()
                .AddScoped<IUtilityService, UtilityService>()
                .BuildServiceProvider();

                var transactionService = serviceProvider.GetService<ITransactionService>();
                var customerService = serviceProvider.GetService<ICustomerService>();
                var logger = serviceProvider.GetService<IApplicationLogService>();

                ActionReturn result = transactionService.PrepareTargetGoalTransactionSchedule(scheduledDate);
                if (!result.Flag)
                {
                    //log returned object.
                    //send email for failed schedule.
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = "",
                        EmailType = "FAILED_SCHEDULE",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };
                    logger.AddEmailToLog(emailLog, null);
                }
                else
                {
                    long resultCount = 0;
                    bool flag = Int64.TryParse(Convert.ToString(result.ReturnedObject), out resultCount);
                    //long resultCount =  (long)result.ReturnedObject;
                    if (flag & resultCount > 0)
                    {
                        
                    }
                    else
                    {
                        //send email for no item schedule for transaction processing today.
                        EmailLog emailLog = new EmailLog
                        {
                            EmailTo = "",
                            EmailType = "NO_CUSTOMER_TO_DEBIT",
                            Status = Enumerators.STATUS.NEW.ToString(),
                        };
                        logger.AddEmailToLog(emailLog, null);
                        logger.Add(result);
                    }


                    if (result.HasEmail)
                    {
                        Dictionary<int, object> emailLogDictionary = (Dictionary<int, object>)result.EmailLog;
                        Dictionary<int, object> emailValuesDictionary = (Dictionary<int, object>)result.EmailValues;
                        for(int i=1; i <= emailLogDictionary.Count(); i++)
                        {
                            EmailLog emailLog = (EmailLog)emailLogDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            Dictionary<string, string> emailValue = (Dictionary<string, string>)emailValuesDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            logger.AddEmailToLog(emailLog, emailValue);
                        }

                    }

                    //charge pending goals
                    ActionReturn processScheduledTransactionsResult = transactionService.ProcessScheduledTransactions(scheduledDate, FileLogPath);
                    if (processScheduledTransactionsResult.Flag)
                    {
                        //send success email for card charges


                        ActionReturn completedTargetGoalResult = customerService.GetCompletedGoals();
                        if (completedTargetGoalResult.Flag)
                        {
                            if (completedTargetGoalResult.HasEmail)
                            {
                                Dictionary<int, object> emailLogDictionary = (Dictionary<int, object>)completedTargetGoalResult.EmailLog;
                                Dictionary<int, object> emailValuesDictionary = (Dictionary<int, object>)completedTargetGoalResult.EmailValues;
                                for (int i = 1; i <= emailLogDictionary.Count(); i++)
                                {
                                    EmailLog emailLog = (EmailLog)emailLogDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                                    Dictionary<string, string> emailValue = (Dictionary<string, string>)emailValuesDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                                    logger.AddEmailToLog(emailLog, emailValue);
                                }
                            }
                        }
                    }
                    else
                    {
                        //send failed email for card charges


                        logger.Add(processScheduledTransactionsResult);
                    }
                    if (processScheduledTransactionsResult.HasEmail)
                    {
                        Dictionary<int, object> emailLogDictionary = (Dictionary<int, object>)processScheduledTransactionsResult.EmailLog;
                        Dictionary<int, object> emailValuesDictionary = (Dictionary<int, object>)processScheduledTransactionsResult.EmailValues;
                        for (int i = 1; i <= emailLogDictionary.Count(); i++)
                        {
                            EmailLog emailLog = (EmailLog)emailLogDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            Dictionary<string, string> emailValue = (Dictionary<string, string>)emailValuesDictionary.Where(o => o.Key == i).Select(o => o.Value).FirstOrDefault();
                            logger.AddEmailToLog(emailLog, emailValue);
                        }

                    }
                    logger.Add(processScheduledTransactionsResult);
                }
                
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());Console.ReadLine();
            }
            

        }
    }
}
