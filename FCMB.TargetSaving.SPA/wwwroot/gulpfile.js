﻿var ts = require('gulp-typescript');
var gulp = require('gulp');
var clean = require('gulp-clean');

var destPath = './scripts/libs/';
var tsJsdestPath = './scripts/ngapp/app';

// Delete the dist directory
gulp.task('cleanng', function() {
    return gulp.src(destPath)
        .pipe(clean());
});

gulp.task('cleanTsCompilesNg', function() {
    return gulp.src(tsJsdestPath)
        .pipe(clean());
});

gulp.task("scriptsNStylesng", () => {
    gulp.src([
            'core-js/client/**',
            'systemjs/dist/system.src.js',
            'systemjs/dist/system-polyfills.js',
            'es6-shim/es6-shim.min.js',
            'reflect-metadata/**',
            'rxjs/**',
            'zone.js/dist/**',
            '@angular/**'
        ], {
            cwd: "node_modules/**"
        })
        .pipe(gulp.dest("./scripts/libs"));
});

var tsProject = ts.createProject('./src/tsconfig.json', {
    typescript: require('typescript')
});

try {
    gulp.task('ts', function(done) {
        var tsResult = gulp.src([
                // "component/*.ts",
                // "directives/*.ts",
                // "interfaces/*.ts",
                // "models/*.ts",
                // "services/*.ts",
                "*.ts"
            ], {
                cwd: "src/app/**"
            })
            .pipe(ts({
                declarationFiles: true,
                noExternalResolve: true,
                noImplicitAny: true,
                target: "es2015",
                module: "commonjs",
                moduleResolution: "node",
                sourceMap: true,
                emitDecoratorMetadata: true,
                experimentalDecorators: true,
                removeComments: false,
                outDir: "./scripts/ngapp/app",
                allowJs: true
            }));


        return tsResult.js.pipe(gulp.dest('./scripts/ngapp/app'));
    });
} catch (ex) {
    console.log(ex.message);
}


// gulp.task('watch', ['watch.ts']);
// gulp.task('watch.ts', ['ts']);
gulp.task('rebuildAllng', ['cleanTsCompilesNg', 'scriptsNStylesng', 'watch']);
gulp.task('rebuildAllngWithoutWatch', ['cleanng', 'cleanTsCompilesNg', 'scriptsNStylesng', 'ts']);
// gulp.task('buildAllng', ['scriptsNStylesng', 'watch']);

gulp.task('cleanAllng', ['cleanng', 'cleanTsCompilesNg']);
gulp.task('default', ['rebuildAllngWithoutWatch']);