"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const common_1 = require('@angular/common');
const forms_1 = require('@angular/forms');
const http_1 = require('@angular/http');
const router_1 = require('@angular/router');
const header_service_1 = require("../commons/services/header.service");
const usermanagement_service_1 = require("./services/usermanagement.service");
const home_component_1 = require('./components/home.component');
const adduser_modal_component_1 = require('./components/adduser.modal.component');
const user_list_component_1 = require('./components/user-list.component');
const user_row_component_1 = require('./components/user-row.component');
let UserManagementModule = class UserManagementModule {
};
UserManagementModule = __decorate([
    core_1.NgModule({
        imports: [common_1.CommonModule, forms_1.ReactiveFormsModule, http_1.HttpModule, router_1.RouterModule],
        declarations: [home_component_1.HomeComponent, user_list_component_1.UserListComponent, user_row_component_1.UserRowComponent, adduser_modal_component_1.NewUserModalComponent],
        providers: [header_service_1.HeaderService, usermanagement_service_1.UsermanagementService]
    }), 
    __metadata('design:paramtypes', [])
], UserManagementModule);
exports.UserManagementModule = UserManagementModule;
//# sourceMappingURL=usermanagement.module.js.map