"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const usermanagement_service_1 = require("../services/usermanagement.service");
const adduser_modal_component_1 = require("../components/adduser.modal.component");
let HomeComponent = class HomeComponent {
    constructor(userService) {
        this.userService = userService;
        this.users = new Array();
        this.hideLoadingUsers = false;
    }
    AddNewUser() {
        this.newUserModal.reInitModal();
        this.newUserModal.show();
    }
    ngOnInit() {
        this.newUserModal.newUserPostedEvent
            .subscribe(newUser => {
            this.users.push(newUser);
        });
        this.userService.getUsers()
            .subscribe(data => {
            if (data.Success == true) {
                this.users = data.Results;
            }
            else {
                if (data.Results) {
                    console.log(`Error list returned ${data.Results}`);
                    for (let mess of data.Results) {
                       
                    }
                }
            }
        }, (error) => {
            console.log(`Error thrown ${error}`);
        }, () => {
            this.hideLoadingUsers = true;
        });
    }
};
__decorate([
    core_1.ViewChild(adduser_modal_component_1.NewUserModalComponent), 
    __metadata('design:type', adduser_modal_component_1.NewUserModalComponent)
], HomeComponent.prototype, "newUserModal", void 0);
HomeComponent = __decorate([
    core_1.Component({
        templateUrl: "Assets/html/manage-user.html"
    }), 
    __metadata('design:paramtypes', [usermanagement_service_1.UsermanagementService])
], HomeComponent);
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map