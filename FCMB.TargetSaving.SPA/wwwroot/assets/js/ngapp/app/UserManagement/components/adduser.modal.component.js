"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const forms_1 = require("@angular/forms");
const User_1 = require("../models/User");
const usermanagement_service_1 = require("../services/usermanagement.service");
const EmailValidator_1 = require("../../commons/validators/EmailValidator");
const PhoneNumberValidator_1 = require("../../commons/validators/PhoneNumberValidator");
const RequiredNonZeroIntegerValidator_1 = require("../../commons/validators/RequiredNonZeroIntegerValidator");
let NewUserModalComponent = class NewUserModalComponent {
    constructor(userService, formBuilder) {
        this.userService = userService;
        this.visible = false;
        this.visibleAnimate = false;
        this.newUserPostedEvent = new core_1.EventEmitter();
        this.newUser = new User_1.User();
        this._formBuilder = formBuilder;
        this.newUserFormGroup = formBuilder.group({
            firstname: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])],
            lastname: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])],
            email: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20), EmailValidator_1.EmailValidator.isValid
                ])],
            password: ['', forms_1.Validators.required],
            primary_phone: ['', forms_1.Validators.compose([
                    forms_1.Validators.maxLength(20), PhoneNumberValidator_1.PhoneNumberValidator.isValid
                ])],
            alternate_phone: ['', forms_1.Validators.compose([
                    forms_1.Validators.maxLength(20), PhoneNumberValidator_1.PhoneNumberValidator.isValid
                ])],
            address1: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(25)
                ])],
            address2: ['', forms_1.Validators.maxLength(25)],
            city: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])],
            state_id: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            zip_code: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(10)
                ])],
            date_of_birth: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])]
        });        
    }
    reInitModal() {
        this.newUser = new User_1.User();
        this.newUserFormGroup = this._formBuilder.group({
            firstname: ['kola', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])],
            lastname: ['oladele', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])],
            email: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20), EmailValidator_1.EmailValidator.isValid
                ])],
            password: ['', forms_1.Validators.required],
            primary_phone: ['', forms_1.Validators.compose([
                    forms_1.Validators.maxLength(20), PhoneNumberValidator_1.PhoneNumberValidator.isValid
                ])],
            alternate_phone: ['', forms_1.Validators.compose([
                    forms_1.Validators.maxLength(20), PhoneNumberValidator_1.PhoneNumberValidator.isValid
                ])],
            address1: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(25)
                ])],
            address2: ['', forms_1.Validators.maxLength(25)],
            city: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])],
            state_id: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, RequiredNonZeroIntegerValidator_1.RequiredNonZeroIntegerValidator.isValid
                ])],
            zip_code: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(10)
                ])],
            date_of_birth: ['', forms_1.Validators.compose([
                    forms_1.Validators.required, forms_1.Validators.maxLength(20)
                ])]
        });
    }
    show() {
        this.visible = true;
        setTimeout(() => this.visibleAnimate = true);
    }
    hide() {
        this.visibleAnimate = false;
        setTimeout(() => this.visible = false, 300);
    }
    SaveUser() {
        if (this.newUserFormGroup.valid) {
            this.showLoading = true;
            this.userService.postUser(this.newUserFormGroup.value)
                .subscribe(data => {
                if (data.Success == true) {
                    this.newUser = data.Results[0];
                    this.newUserPostedEvent.emit(this.newUser);
                }
                else {
                    if (data.Results) {
                        
                        for (let mess of data.Results) {
                             
                        }
                    }
                }
            }, (error) => {
                console.log(`Error thrown ${error}`);
            }, () => {
                this.showLoading = false;
            });
        }
        return false;
    }
};
__decorate([
    core_1.Output(), 
    __metadata('design:type', Object)
], NewUserModalComponent.prototype, "newUserPostedEvent", void 0);
NewUserModalComponent = __decorate([
    core_1.Component({
        selector: 'new-user-modal',
        templateUrl: "Assets/html/add-user.html"
    }), 
    __metadata('design:paramtypes', [usermanagement_service_1.UsermanagementService, forms_1.FormBuilder])
], NewUserModalComponent);
exports.NewUserModalComponent = NewUserModalComponent;
//# sourceMappingURL=adduser.modal.component.js.map