"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const http_1 = require("@angular/http");
const header_service_1 = require("../../commons/services/header.service");
let UsermanagementService = class UsermanagementService {
    constructor(http, headerService) {
        this.http = http;
        this.headerService = headerService;
    }
    getUsers() {
        return this.http.get("http://localhost:9405/users", this.headerService.getRequestJsonHeaders())
            .map(response => response.json());
    }
    postUser(userFormGroup) {
        console.log(userFormGroup.value);
        return this.http.post("http://localhost:9405/new-user", JSON.stringify(userFormGroup.value), this.headerService.getRequestJsonHeaders()).
            map(res => {
            console.log(`res.json(): ${res.json()}`);
            return res.json();
        });
    }
};
UsermanagementService = __decorate([
    core_1.Injectable(), 
    __metadata('design:paramtypes', [http_1.Http, header_service_1.HeaderService])
], UsermanagementService);
exports.UsermanagementService = UsermanagementService;
//# sourceMappingURL=usermanagement.service.js.map