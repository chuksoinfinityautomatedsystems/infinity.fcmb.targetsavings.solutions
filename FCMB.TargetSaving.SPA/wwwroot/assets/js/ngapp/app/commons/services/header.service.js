"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const http_1 = require('@angular/http');
let HeaderService = class HeaderService {
    getRequestJsonHeaders() {
        let _headers = new http_1.Headers();
        _headers.append("Accept", "application/json");
        _headers.append("Content-Type", "application/json");
        return { headers: _headers };
    }
    getBearerRequestJsonHeaders(bearerToken) {
        if (bearerToken === undefined || bearerToken.length < 1) {
            throw new Error("bearerToken missing.");
        }
        let _headers = new http_1.Headers();
        _headers.append("Accept", "application/json");
        _headers.append("Content-Type", "application/json");
        _headers.append("Authorization", `Bearer ${bearerToken}`);
        return { headers: _headers };
    }
    getBasicRequestJsonHeaders(base64Token) {
        if (base64Token === undefined || base64Token.length < 1) {
            throw new Error("base64Token missing.");
        }
        let _headers = new http_1.Headers();
        _headers.append("Accept", "application/json");
        _headers.append("Content-Type", "application/json");
        _headers.append("Authorization", `Basic ${base64Token}`);
        return { headers: _headers };
    }
};
HeaderService = __decorate([
    core_1.Injectable(), 
    __metadata('design:paramtypes', [])
], HeaderService);
exports.HeaderService = HeaderService;
//# sourceMappingURL=header.service.js.map