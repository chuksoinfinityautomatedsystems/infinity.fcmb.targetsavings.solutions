"use strict";
class RequiredNonZeroIntegerValidator {
    static isValid(formControl) {
        try {
            let value = parseInt(formControl.value);
            if (value < 1) {
                return { requiredNonZeroInteger: true };
            }
        }
        catch (Ex) {
            return { requiredNonZeroInteger: true };
        }
        return null;
    }
}
exports.RequiredNonZeroIntegerValidator = RequiredNonZeroIntegerValidator;
//# sourceMappingURL=RequiredNonZeroIntegerValidator.js.map