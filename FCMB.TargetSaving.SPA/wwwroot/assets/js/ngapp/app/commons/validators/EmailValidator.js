"use strict";
class EmailValidator {
    static isValid(formControl) {
        let emailRegex = /^[ ]*\w+([-+.']\w+)*@\w+([-.]\\w+)*\.\w+([-.]\w+)*[ ]*$/i;
        if (formControl.value) {
            let isValid = emailRegex.test(formControl.value);
            if (isValid === false) {
                return { emailInvalid: true };
            }
        }
        return null;
    }
}
exports.EmailValidator = EmailValidator;
//# sourceMappingURL=EmailValidator.js.map