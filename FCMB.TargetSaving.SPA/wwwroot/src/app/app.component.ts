

import { Component } from "@angular/core";
import { Router } from "@angular/router";

import { AuthenticationService } from "./authentication/services/authentication.service";
import { UtilService } from "./commons/services/utils.services";
import { Appsettings } from "./commons/constants/appsettings.constant";


@Component({
    selector: "ng-app",
    templateUrl: "html/app.html",
})
export class AppComponent {
    currentPage: string = 'Dashboard';
    isSettings: boolean = false;
    loggedIn: boolean;
    constructor(private authService: AuthenticationService, private router: Router) {
        //let isLoggedInChecker: boolean = false;
        authService.loggedIn$.subscribe(flag => this.loggedIn = flag)
        if (!this.loggedIn) {
            //this.requiresAuth = true;
            this.router.navigate(['Login']);
        }
        else {
            document.body.style.backgroundColor = '#eee';
            //Handle Page refresh
            var currentUrl = window.location.pathname;
            currentUrl = UtilService.trim(currentUrl);
            //this.requiresAuth = false;
            if (currentUrl.length > 0 && currentUrl != "" && currentUrl != "/") {
                if (currentUrl.substr(currentUrl.length - 1) == "/") {
                    router.navigate([currentUrl.substr(1, currentUrl.length - 2)])
                }
                else {
                    router.navigate([currentUrl.substr(1, currentUrl.length - 1)])
                }
            }
            else {
                this.currentPage = 'Dashboard'
                this.router.navigate([Appsettings.DASHBOARD_ROUTER_URL]);
            }
        }
    }

    closeSettings(event) {
        event.stopPropagation()
        let self = this;
        self.isSettings = false;
        return false;
    }
    openSettings(event) {
        event.stopPropagation()
        let self = this;
        self.isSettings = true;
        return false;
    }
    gotoDashboard(event) {
        event.stopPropagation()
        this.currentPage = 'Dashboard'
        this.router.navigate([Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    }
    goToProspects(event) {
        event.stopPropagation()
        this.currentPage = 'Prospects'
        this.router.navigate([Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    }


    goToCustomers(event) {
        event.stopPropagation();
        this.currentPage = 'Customers'
        this.router.navigate([Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL]);
        return false;
    }


    goToCustomerRequests(event) {
        event.stopPropagation();
        this.currentPage = 'Requests'
        this.router.navigate([Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL]);
        return false;
    }

    goToCustomerGoals(event) {
        event.stopPropagation();
        this.currentPage = 'Goals'
        this.router.navigate([Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    }


    goToCustomerTransactions(event) {
        event.stopPropagation();
        this.currentPage = 'Transactions'
        this.router.navigate([Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    }

    goToTargetCategory(event) {
        console.log('goToTargetCategory')
        event.stopPropagation();
        this.currentPage = 'Target Category'
        this.router.navigate([Appsettings.SETTINGS_TARGET_CATEGORY_ROUTER_URL]);
        return false;
    }
    goToTargetFrequency(event) {
        console.log('goToTargetFrequency')
        event.stopPropagation();
        this.currentPage = 'Target Frequency'
        this.router.navigate([Appsettings.SETTINGS_TARGET_FREQUENCY_ROUTER_URL]);
        return false;
    }
    goToTargetRequestType(event) {
        console.log('goToTargetRequestType')
        event.stopPropagation();
        this.currentPage = 'Target Request Type'
        this.router.navigate([Appsettings.SETTINGS_TARGET_REQUEST_TYPE_ROUTER_URL]);
        return false;
    }
    logout(event) {
        event.stopPropagation();
        this.authService.loggedIn(false);
        this.router.navigate(['Login']);
    }
}