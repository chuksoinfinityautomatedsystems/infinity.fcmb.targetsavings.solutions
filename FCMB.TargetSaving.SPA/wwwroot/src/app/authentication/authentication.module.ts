
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { LoginComponent } from './components/login.component';

import { HeaderService } from "../commons/services/header.service";

import { AuthenticationService } from './services/authentication.service';
import { AuthorizeService } from './services/authorization.services';

@NgModule({
  imports: [CommonModule, ReactiveFormsModule, HttpModule, RouterModule],
  declarations: [LoginComponent],
  providers: [HeaderService, AuthenticationService, AuthorizeService]
})
export class AuthenticationPagesModule { }
