

import { Injectable } from "@angular/core";

import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { AuthenticationService } from "../../authentication/services/authentication.service";
import { UtilService } from "../../commons/services/utils.services";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class AuthorizeService implements CanActivate {

    constructor(private _router: Router, private authService: AuthenticationService) { }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {

        if (!AuthenticationService.authUserObj) {
            this._router.navigate(['Login']);
            return false;
        }


        if (localStorage.getItem('X-UserOauthToken')) {
            this.authService.isLoggedIn = true;

            return true;
        }
        else {

            this._router.navigate(['Login'], { queryParams: { returnUrl: state.url } });
            return false;
        }

        // this._router.navigate(['login'], { queryParams: { returnUrl: state.url } });

        // if (this.authService.isLoggedIn) {
        //     console.log(111);
        //     return true;
        // }
        // else {
        //     this._router.navigate(['Login'], { queryParams: { returnUrl: state.url } });
        // }


    };
}