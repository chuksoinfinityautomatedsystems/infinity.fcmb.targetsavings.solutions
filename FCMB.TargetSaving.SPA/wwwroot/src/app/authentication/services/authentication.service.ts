

import { Injectable, EventEmitter, Output } from "@angular/core";
import { Http, Response, RequestOptions, Headers } from "@angular/http";
import "rxjs/Rx";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { EventHandlerService } from "../../commons/services/event_handlers.service";

import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum";

import { HeaderService } from "../../commons/services/header.service";
import { UtilService } from "../../commons/services/utils.services";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { User } from "../models/user.model";
import { Subject } from "rxjs/Rx";



function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Injectable()
export class AuthenticationService {
    isLoggedIn: boolean;
    static authUserObj: User;
    timeoutID: any;
    private _loggedIn = new Subject<boolean>();
    constructor(private _headerService: HeaderService, private http: Http) {

        if (localStorage.getItem("X-UserOauthToken")) {
            this.isLoggedIn = true;
        }
        else {
            this.isLoggedIn = false;
        }

    }

    loggedIn$ = this._loggedIn.asObservable();
    loggedIn(flag: boolean) {
        this._loggedIn.next(flag);
    }

    setAccessToken(accessToken: string) {
        localStorage.setItem("X-UserOauthToken", accessToken);
        this.isLoggedIn = true;
    }

    GetAccessToken() {
        if (localStorage.getItem("X-UserOauthToken")) {
            return JSON.parse(localStorage.getItem("X-UserOauthToken"));
            //return JSON.parse(localStorage.getItem("X-InternetBankingLoggedInUserOauthToken")).access_token;
        }

        return "";
    }


    logout() {
        this.isLoggedIn = false;
        AuthenticationService.authUserObj = undefined;
        localStorage.removeItem("X-UserOauthToken");
        return this.isLoggedIn;
    }

    authenticateUserID(user: User) {

        this.logout();


        if (UtilService.StringIsNullOrEmpty(user.UserId) || UtilService.StringIsNullOrEmpty(user.Password)) {
            throw new Error("UserId or password empty.");
        }

        let _user = new User();
        _user.UserId = user.UserId;
        _user.Password = btoa(user.Password);

        this.isLoggedIn = true;

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {

            return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL, _user,
                this._headerService.getRequestJsonHeaders()
            ).map((_response: Response) => {
                // console.log("Authentication Login: " + _response)
                // console.log("Authentication Login JSON: " + _response.json())
                return _response.json();
            });
        }
        else {
            return Observable.of({
                ResponseCode: ResponseCodes.SUCCESS,
                responseDescription: "",
                ResponseFriendlyMessage: "Username or password is incorrect.",
                AccessToken: "FGNGFG"
            });
        }
    }


    // getSecurityQuestions() {

    //     if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {

    //         return this.http.post(Appsettings.GET_SECURITY_QUESTIONS_URL, null,
    //             this._headerService.getRequestJsonHeaders()
    //         ).map((_response: Response) => {
    //             return _response.json();
    //         });
    //     }
    //     else {
    //         return Observable.of({
    //             ResponseCode: ResponseCodes.SUCCESS,
    //             SessionId: "",
    //             ResponseFriendlyMessage: "",
    //             questionDetailList: [
    //                 {
    //                     "questionId": "1",
    //                     "questionDescription": "What's your dream job.",
    //                     "answer": ""
    //                 },
    //                 {
    //                     "questionId": "2",
    //                     "questionDescription": "What's your spouse name",
    //                     "answer": ""
    //                 },
    //                 {
    //                     "questionId": "3",
    //                     "questionDescription": "What's your first boss name.",
    //                     "answer": ""
    //                 },
    //                 {
    //                     "questionId": "4",
    //                     "questionDescription": "What your favorite soccer club.",
    //                     "answer": ""
    //                 }
    //             ]
    //         });
    //     }
    // }



    stopAutoLogout() {
        if ($('body').off) {
            $('body').off("mousemove");
            $('body').off("mousedown");
            $('body').off("keypress");
            $('body').off("DOMMouseScroll");
            $('body').off("mousewheel");
            $('body').off("touchmove");
            $('body').off("MSPointerMove");
            $('body').off("pointermove");
        }
        else if ($('body').unbind) {
            $('body').unbind("mousemove");
            $('body').unbind("mousedown");
            $('body').unbind("keypress");
            $('body').unbind("DOMMouseScroll");
            $('body').unbind("mousewheel");
            $('body').unbind("touchmove");
            $('body').unbind("MSPointerMove");
            $('body').unbind("pointermove");
        }

        if (this.timeoutID) {
            _window().clearTimeout(this.timeoutID);
        }
    }


    setupAutoLogout() {
        let self = this;


        if ($('body').off) {
            $('body').off("mousemove");
            $('body').off("mousedown");
            $('body').off("keypress");
            $('body').off("DOMMouseScroll");
            $('body').off("mousewheel");
            $('body').off("touchmove");
            $('body').off("MSPointerMove");
            $('body').off("pointermove");
            $('body').off("keyup");
            $('body').off("keydown");
        }
        else if ($('body').unbind) {
            $('body').unbind("mousemove");
            $('body').unbind("mousedown");
            $('body').unbind("keypress");
            $('body').unbind("DOMMouseScroll");
            $('body').unbind("mousewheel");
            $('body').unbind("touchmove");
            $('body').unbind("MSPointerMove");
            $('body').unbind("pointermove");
            $('body').unbind("keyup");
            $('body').unbind("keydown");

        }


        if (self.timeoutID) {
            _window().clearTimeout(self.timeoutID);
        }

        if ($.on) {

            $('body').on("mousemove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("mousedown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("keypress", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("keydown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("keyup", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("DOMMouseScroll", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("mousewheel", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("touchmove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("MSPointerMove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').on("pointermove", function () {
                self.resetAutoLogoutTimer(self);
            });

        }
        else if ($.bind) {


            $('body').bind("mousemove", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("mousedown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("keypress", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("DOMMouseScroll", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("mousewheel", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("touchmove", function () {
                self.resetAutoLogoutTimer(self);
            });

            $('body').bind("MSPointerMove", function () {
                self.resetAutoLogoutTimer(self);
            });

            $('body').bind("pointermove", function () {
                self.resetAutoLogoutTimer(self);
            });

            $('body').bind("keydown", function () {
                self.resetAutoLogoutTimer(self);
            });
            $('body').bind("keyup", function () {
                self.resetAutoLogoutTimer(self);
            });
        }

        self.startAutoLogoutTimer();
    }

    startAutoLogoutTimer() {
        let self = this;
        self.timeoutID = _window().setTimeout(function () {

            // EventHandlerService.emitSessionTimeoutEvent();


            //self.logout();
            self.stopAutoLogout();
        }, Appsettings.autoLogoutTimeInMicrosec);
    }

    resetAutoLogoutTimer(self: AuthenticationService) {
        _window().clearTimeout(self.timeoutID);
        self.startAutoLogoutTimer();
    }








}