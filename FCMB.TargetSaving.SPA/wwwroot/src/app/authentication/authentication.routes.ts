

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { LoginComponent } from './components/login.component';

export const authenticationRoutes = RouterModule.forChild([
    {
        path: "Login",
        component: LoginComponent,
        pathMatch: 'full'
    },
]);