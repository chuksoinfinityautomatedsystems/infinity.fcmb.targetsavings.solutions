

import { Component, ChangeDetectorRef, OnInit, OnDestroy, ElementRef, Renderer } from "@angular/core";
import "rxjs/Rx";
import { ActivatedRoute, Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthenticationService } from '../services/authentication.service';
import { UtilService } from "../../commons/services/utils.services";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { User } from "../models/user.model";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";



function _window(): any {
    return window;
}
declare var $: any;

@Component({
    templateUrl: "html/authentication/login.html",
    styles: ['body {  background-color: #23bde3}']
})
export class LoginComponent implements OnInit, OnDestroy {


    loginFormGroup: FormGroup;
    loginSub: any;
    returnUrlSub: any;
    returnUrl: string;
    formSubmitted: boolean;
    requestOngoing: boolean;
    user: User;

    constructor(private formBuilder: FormBuilder, private activatedRoute: ActivatedRoute, private renerer: Renderer,
        private elementRef: ElementRef,
        private router: Router, private authService: AuthenticationService, private cdRef: ChangeDetectorRef) {

        document.body.style.backgroundColor = '#23bde3';
        //this.renerer.setElementStyle(this.elementRef.nativeElement, 'background-color', '#23bde3');
        this.loginFormGroup = formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        this.formSubmitted = false;

        this.requestOngoing = false;

    }

    ngOnInit() {
        this.authService.logout();
        this.returnUrlSub = this.activatedRoute.queryParams.subscribe(queryParams => {
            this.returnUrl = queryParams['returnUrl'];
        });
    }

    clearFormsubmittedOnFocus() {
        this.formSubmitted = false;
    }

    loginUser() {

        let self = this;
        this.formSubmitted = true;

        this.requestOngoing = true;

        if (this.loginFormGroup.valid) {

            let username = this.loginFormGroup.controls['username'].value;
            let pass = this.loginFormGroup.controls['password'].value;


            if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {
                let loadingModalInterval = setInterval(function () {
                    self.requestOngoing = false;

                    self.cdRef.detectChanges();
                    self._authenticateUser();
                    _window().clearInterval(loadingModalInterval);
                }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
            }
            else {
                self._authenticateUser();
            }
        }
        else {
            this.requestOngoing = false;
        }
    }



    _authenticateUser() {

        let self = this;

        self.user = new User();

        self.user.UserId = UtilService.trim(self.loginFormGroup.controls['username'].value);
        self.user.Password = UtilService.trim(self.loginFormGroup.controls['password'].value);


        self.loginSub = self.authService.authenticateUserID(self.user)
            .subscribe(response => {
                self.requestOngoing = false;

                if (response.ResponseCode == ResponseCodes.SUCCESS) {
                    self.user.AccessToken = response.AccessToken;
                    self.authService.setAccessToken(self.user.AccessToken);
                    AuthenticationService.authUserObj = self.user;
                    this.authService.loggedIn(true);

                    if (self.returnUrl && self.returnUrl.length > 0) {
                        self.router.navigate([self.returnUrl]);
                    }
                    else {
                        self.router.navigate([Appsettings.DASHBOARD_ROUTER_URL]);
                    }
                }
                else {
                    UtilService.toastError(response.ResponseFriendlyMessage);
                }

                self.cdRef.detectChanges();
            },
                (error: any) => {
                    UtilService.toastError(Appsettings.TECHNICAL_ERROR_MESSAGE);
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                },
                () => {
                    // self.requestOngoing = false;
                    // self.cdRef.detectChanges();
                });

    }


    ngOnDestroy() {
        if (this.loginSub) {
            this.loginSub.unsubscribe();
        }

        if (this.returnUrlSub) {
            this.returnUrlSub.unsubscribe();
        }
    }
}