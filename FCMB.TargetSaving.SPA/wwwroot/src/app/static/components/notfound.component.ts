
import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    templateUrl: "html/notfound.html"
})
export class NotFoundComponent {

    constructor(private router: Router) { }

    Goto(url: string) {
        this.router.navigate([url]);
        return false;
    }
}