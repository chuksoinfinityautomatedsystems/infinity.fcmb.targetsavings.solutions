import { Injectable } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import "rxjs/Rx";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { HeaderService } from "../../commons/services/header.service";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { AuthenticationService } from "../../authentication/services/authentication.service";
import { UtilService } from "../../commons/services/utils.services";
import { PaginationRequest } from "../../commons/models/pagination-model";
import { TargetFrequency } from "../models/target-frequency.model";
import { TargetRequestType } from "../models/request-type.model";
import { TargetCategory } from "../models/target-category.model";


declare var jsPDF: any;

function _window(): any {
    // return the global native browser window object
    return window;
}

@Injectable()
export class SettingsService {

    constructor(private authService: AuthenticationService,
        private _headerService: HeaderService, private http: Http) {
    }

    getTargetCategory() {
        return this.http.post(Appsettings.GET_TARGET_CATEGORY_URL,
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    createTargetCategory(targetCategory: TargetCategory) {
        return this.http.post(Appsettings.ADD_TARGET_CATEGORY_URL, JSON.stringify(targetCategory),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    updateTargeCategory(targetCategory: TargetCategory) {
        return this.http.post(Appsettings.UPDATE_TARGET_CATEGORY_URL, JSON.stringify(targetCategory),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    deleteTargetCategory(targetCategory: TargetCategory) {
        return this.http.post(Appsettings.DELETE_TARGET_CATEGORY_URL, JSON.stringify(targetCategory.id),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    getTargetFrequency() {
        return this.http.post(Appsettings.GET_TARGET_FREQUENCY_URL,
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    createTargetFrequency(targetFrequency: TargetFrequency) {
        return this.http.post(Appsettings.ADD_TARGET_FREQUENCY_URL, JSON.stringify(targetFrequency),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    updateTargetFrequency(targetFrequency: TargetFrequency) {
        return this.http.post(Appsettings.UPDATE_TARGET_FREQUENCY_URL, JSON.stringify(targetFrequency),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    deleteTargetFrequency(targetFrequency: TargetFrequency) {
        return this.http.post(Appsettings.DELETE_TARGET_FREQUENCY_URL, JSON.stringify(targetFrequency.id),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    getTargetRequestType() {
        return this.http.post(Appsettings.GET_TARGET_REQUEST_TYPE_URL,
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    createTargetRequestType(requestType: TargetRequestType) {
        return this.http.post(Appsettings.ADD_TARGET_REQUEST_TYPE_URL, JSON.stringify(requestType),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    updateTargetRequestType(requestType: TargetRequestType) {
        return this.http.post(Appsettings.UPDATE_TARGET_REQUEST_TYPE_URL, JSON.stringify(requestType),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    deleteTargetRequestType(requestType: TargetRequestType) {
        return this.http.post(Appsettings.DELETE_TARGET_REQUEST_TYPE_URL, JSON.stringify(requestType.requestTypeId),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
}