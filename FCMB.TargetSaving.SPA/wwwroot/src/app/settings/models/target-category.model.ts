export class TargetCategory {
    id: number;
    name: string;
    image: string;
    isActive: boolean;
}