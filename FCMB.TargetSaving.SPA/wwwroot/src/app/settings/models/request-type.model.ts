export class TargetRequestType {
    requestTypeId: number;
    requestType: string;
    isActive: boolean;
}