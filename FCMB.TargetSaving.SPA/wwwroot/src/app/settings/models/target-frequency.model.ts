export class TargetFrequency {
    id: number;
    frequency: string;
    valueInDays: number;
    isActive: boolean;
}