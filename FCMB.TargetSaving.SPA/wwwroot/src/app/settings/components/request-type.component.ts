import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild, ElementRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { AuthenticationService } from "../../authentication/services/authentication.service";
import { BaseComponent } from "../../commons/components/base.component";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { PaginationRequest } from "../../commons/models/pagination-model";
import { SettingsService } from "../services/settings.service";
import { TargetImage } from "../models/target-images.model";
import { Subscription } from "rxjs";
import { TargetRequestType } from "../models/request-type.model";

function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Component({
    templateUrl: "html/settings/request-type.html"
})
export class TargetRequestTypeComponent extends BaseComponent implements OnInit, OnDestroy {

    constructor(private authService: AuthenticationService, private settingsService: SettingsService,
        private router: Router, private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef
    ) {

        super(authService, router, formBuilder, cdRef)
        this.requestOngoing = false;

        document.title = "Target Request Type";
        this.targetRequestTypes = new Array<TargetRequestType>();
        this.targetRequestType = new TargetRequestType();
        this.newTargetRequestType = new TargetRequestType();
        this.createForm();

        document.body.style.backgroundColor = '#eee';

    }
    showDetailViews: boolean;
    requestOngoing: boolean;
    targetRequestTypes: Array<TargetRequestType>;
    targetRequestType: TargetRequestType
    newTargetRequestType: TargetRequestType;
    getTargetRequestTypeSub: Subscription;
    isAddNew: boolean;
    requestTypeForm: FormGroup;

    ngOnInit(): void {
        this.loadRequestTypes();

    }
    ngOnDestroy() {
        this.getTargetRequestTypeSub.unsubscribe();
    }
    loadRequestTypes() {
        let self = this;
        self.getTargetRequestTypeSub = self.settingsService.getTargetRequestType()
            .subscribe(response => {
                console.log('getTargetRequestType', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    self.targetRequestTypes = response.responseItem;
                    this.showDetailViews = false;
                    this.targetRequestType = new TargetRequestType();
                    this.newTargetRequestType = new TargetRequestType();
                    this.isAddNew = false;
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
    createForm() {
        this.requestTypeForm = this.formBuilder.group({
            requestType: ['', Validators.required]
        });
    }
    goToDetails(requestType: TargetRequestType) {
        let self = this;
        self.targetRequestType = requestType;
        self.showDetailViews = true;
        self.isAddNew = false;
        self.cdRef.detectChanges();
    }
    addNew(event) {
        event.stopPropagation();
        let self = this;
        self.targetRequestType = new TargetRequestType();
        self.showDetailViews = true;
        self.isAddNew = true;
        self.cdRef.detectChanges();
    }

    goBackToList() {
        this.targetRequestType = new TargetRequestType();
        this.showDetailViews = false;
        this.isAddNew = false;
        this.cdRef.detectChanges();
    }

    save() {
        const formModel = this.requestTypeForm.value;
        //this.newTargetFrequency.image = formModel.image.value;
        console.log('', JSON.stringify(formModel));
        console.log('save Request Type', JSON.stringify(this.newTargetRequestType));
        let self = this;
        self.getTargetRequestTypeSub = self.settingsService.createTargetRequestType(this.newTargetRequestType)
            .subscribe(response => {
                console.log('newTargetRequestType', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    //self.targetImages = response.responseItem;
                    this.loadRequestTypes();
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    console.log(error)
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
    update() {
        let self = this;
        console.log(' update', this.targetRequestType)
        self.getTargetRequestTypeSub = self.settingsService.updateTargetRequestType(this.targetRequestType)
            .subscribe(response => {
                console.log('update target freq', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    //self.targetImages = response.responseItem;
                    this.loadRequestTypes();
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    console.log(error)
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
    delete() {
        let self = this;
        self.getTargetRequestTypeSub = self.settingsService.deleteTargetRequestType(this.targetRequestType)
            .subscribe(response => {
                console.log('delete', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    //self.targetImages = response.responseItem;
                    this.loadRequestTypes();
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    console.log(error)
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
}