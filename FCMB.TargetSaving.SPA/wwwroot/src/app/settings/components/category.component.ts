import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild, ElementRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";


import { AuthenticationService } from "../../authentication/services/authentication.service";
import { BaseComponent } from "../../commons/components/base.component";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { PaginationRequest } from "../../commons/models/pagination-model";
import { SettingsService } from "../services/settings.service";
import { DomSanitizer } from "@angular/platform-browser";
import { TargetCategory } from "../models/target-category.model";

function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Component({
    templateUrl: "html/settings/category.html"
})
export class CategoryComponent extends BaseComponent implements OnInit, OnDestroy {

    @ViewChild('fileInput') fileInput: ElementRef;
    constructor(private authService: AuthenticationService, private settingsService: SettingsService,
        private router: Router, private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef,
        private sanitizer: DomSanitizer) {

        super(authService, router, formBuilder, cdRef)
        this.requestOngoing = false;

        document.title = "Target Category";
        this.targetCategories = new Array<TargetCategory>();
        this.targetCategory = new TargetCategory();
        this.newTargetCategory = new TargetCategory();
        this.createForm();

        document.body.style.backgroundColor = '#eee';

    }
    requestOngoing: boolean;
    isSettings: boolean = false;
    showDetailViews: boolean;
    getTargetCategorySub: any;
    targetCategories: Array<TargetCategory>;
    targetCategory: TargetCategory;
    newTargetCategory: TargetCategory;
    isAddNewTarget: boolean = false;
    categoryForm: FormGroup;

    ngOnInit(): void {
        this.loadTargetCategory();

    }
    loadTargetCategory() {
        // let self = this;
        // self.requestOngoing = true;
        // self.isSettings = true;
        // this.showDetailViews = false;
        // self.isAddNewTarget = false;
        // console.log('Appsettings.APP_MODE.toLowerCase()', Appsettings.APP_MODE.toLowerCase());
        // if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {

        //     let loadingModalInterval = setInterval(function () {
        //         self.getTargetImages();
        //         _window().clearInterval(loadingModalInterval);
        //     }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        // }
        // else {
        //     self.getTargetImages();
        // }
        let self = this;
        self.requestOngoing = true;
        self.isSettings = true;
        this.showDetailViews = false;
        self.isAddNewTarget = false;
        let loadingModalInterval = setInterval(function () {
            self.getTargetCategory();
            _window().clearInterval(loadingModalInterval);
        }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
    }
    ngOnDestroy() {

    }
    createForm() {
        this.categoryForm = this.formBuilder.group({
            name: ['', Validators.required],
            image: null
        });
    }

    getTargetCategory() {
        let self = this;
        self.getTargetCategorySub = self.settingsService.getTargetCategory()
            .subscribe(response => {
                console.log('getTargetCategory', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    self.targetCategories = response.responseItem;
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }

    goToDetails(targetCategory: TargetCategory) {
        let self = this;
        self.targetCategory = targetCategory;
        self.showDetailViews = true;
        self.isAddNewTarget = false;
        self.cdRef.detectChanges();
    }
    addNewTargetCategory(event) {
        event.stopPropagation();
        let self = this;
        self.targetCategory = new TargetCategory();
        self.showDetailViews = true;
        self.isAddNewTarget = true;
        self.cdRef.detectChanges();
    }

    goBackToList() {
        this.targetCategory = new TargetCategory();
        this.showDetailViews = false;
        this.isAddNewTarget = false;
        this.cdRef.detectChanges();
    }

    onFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.categoryForm.get('image').setValue({
                    filename: file.name,
                    filetype: file.type,
                    value: reader.result.split(',')[1]
                })
            };
        }
    }
    onFileUpdateChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.targetCategory.image = reader.result.split(',')[1];
            };
        }
    }

    saveCategory() {
        const formModel = this.categoryForm.value;
        this.newTargetCategory.image = formModel.image.value;
        console.log('', JSON.stringify(formModel));
        console.log('saveCategory', JSON.stringify(this.newTargetCategory));
        let self = this;
        self.getTargetCategorySub = self.settingsService.createTargetCategory(this.newTargetCategory)
            .subscribe(response => {
                console.log('createTargetCategory', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    //self.targetImages = response.responseItem;
                    this.loadTargetCategory();
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    console.log(error)
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
    updateCategory() {
        let self = this;
        self.getTargetCategorySub = self.settingsService.updateTargeCategory(this.targetCategory)
            .subscribe(response => {
                console.log('updateTargeCategory', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    //self.targetImages = response.responseItem;
                    this.loadTargetCategory();
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    console.log(error)
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
    deleteCategory() {
        let self = this;
        self.getTargetCategorySub = self.settingsService.deleteTargetCategory(this.targetCategory)
            .subscribe(response => {
                console.log('getTargetImages', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    //self.targetImages = response.responseItem;
                    this.loadTargetCategory();
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    console.log(error)
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
}