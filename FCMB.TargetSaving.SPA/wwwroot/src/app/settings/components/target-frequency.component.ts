import { Component, OnInit, OnDestroy, ChangeDetectorRef, ViewChild, ElementRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { AuthenticationService } from "../../authentication/services/authentication.service";
import { BaseComponent } from "../../commons/components/base.component";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { PaginationRequest } from "../../commons/models/pagination-model";
import { SettingsService } from "../services/settings.service";
import { TargetImage } from "../models/target-images.model";
import { DomSanitizer } from "@angular/platform-browser";
import { TargetFrequency } from "../models/target-frequency.model";
import { Subscription } from "rxjs";

function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Component({
    templateUrl: "html/settings/target-frequency.html"
})
export class TargetFrequencyComponent extends BaseComponent implements OnInit, OnDestroy {

    constructor(private authService: AuthenticationService, private settingsService: SettingsService,
        private router: Router, private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef,
        private sanitizer: DomSanitizer) {

        super(authService, router, formBuilder, cdRef)
        this.requestOngoing = false;

        document.title = "Target Frequency";
        this.targetFrequencies = new Array<TargetFrequency>();
        this.targetFrequency = new TargetFrequency();
        this.newTargetFrequency = new TargetFrequency();
        this.createForm();

        document.body.style.backgroundColor = '#eee';

    }
    showDetailViews: boolean;
    requestOngoing: boolean;
    targetFrequencies: Array<TargetFrequency>;
    targetFrequency: TargetFrequency
    newTargetFrequency: TargetFrequency;
    getTargetFrequencySub: Subscription;
    isAddNew: boolean;
    frequencyForm: FormGroup;

    ngOnInit(): void {
        this.loadFrequencies();

    }
    ngOnDestroy() {
        this.getTargetFrequencySub.unsubscribe();
    }
    loadFrequencies() {
        let self = this;
        self.getTargetFrequencySub = self.settingsService.getTargetFrequency()
            .subscribe(response => {
                console.log('getTargetFrequency', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    self.targetFrequencies = response.responseItem;
                    this.showDetailViews = false;
                    this.targetFrequency = new TargetFrequency();
                    this.newTargetFrequency = new TargetFrequency();
                    this.isAddNew = false;
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
    createForm() {
        this.frequencyForm = this.formBuilder.group({
            frequency: ['', Validators.required],
            valueInDays: ['', Validators.required]
        });
    }
    goToDetails(targetFrequency: TargetFrequency) {
        let self = this;
        self.targetFrequency = targetFrequency;
        self.showDetailViews = true;
        self.isAddNew = false;
        self.cdRef.detectChanges();
    }
    addNew(event) {
        event.stopPropagation();
        let self = this;
        self.targetFrequency = new TargetFrequency();
        self.showDetailViews = true;
        self.isAddNew = true;
        self.cdRef.detectChanges();
    }

    goBackToList() {
        this.targetFrequency = new TargetFrequency();
        this.showDetailViews = false;
        this.isAddNew = false;
        this.cdRef.detectChanges();
    }

    save() {
        const formModel = this.frequencyForm.value;
        //this.newTargetFrequency.image = formModel.image.value;
        console.log('', JSON.stringify(formModel));
        console.log('saveImage', JSON.stringify(this.newTargetFrequency));
        let self = this;
        self.getTargetFrequencySub = self.settingsService.createTargetFrequency(this.newTargetFrequency)
            .subscribe(response => {
                console.log('newTargetFrequency', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    //self.targetImages = response.responseItem;
                    this.loadFrequencies();
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    console.log(error)
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
    update() {
        let self = this;
        console.log(' update', this.targetFrequency)
        self.getTargetFrequencySub = self.settingsService.updateTargetFrequency(this.targetFrequency)
            .subscribe(response => {
                console.log('update target freq', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    //self.targetImages = response.responseItem;
                    this.loadFrequencies();
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    console.log(error)
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
    delete() {
        let self = this;
        self.getTargetFrequencySub = self.settingsService.deleteTargetFrequency(this.targetFrequency)
            .subscribe(response => {
                console.log('delete', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    //self.targetImages = response.responseItem;
                    this.loadFrequencies();
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    console.log(error)
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
}