import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthorizeService } from "../authentication/services/authorization.services";
import { Appsettings } from "../commons/constants/appsettings.constant";
import { TargetFrequencyComponent } from "./components/target-frequency.component";
import { TargetRequestTypeComponent } from "./components/request-type.component";
import { CategoryComponent } from "./components/category.component";



export const settingsRoutes = RouterModule.forChild([
    {
        path: Appsettings.SETTINGS_TARGET_CATEGORY_ROUTER_URL,
        component: CategoryComponent,
        pathMatch: 'full'
    }//, canActivate: [AuthorizeService] }
    ,
    {
        path: Appsettings.SETTINGS_TARGET_FREQUENCY_ROUTER_URL,
        component: TargetFrequencyComponent,
        pathMatch: 'full'
    }
    ,
    {
        path: Appsettings.SETTINGS_TARGET_REQUEST_TYPE_ROUTER_URL,
        component: TargetRequestTypeComponent,
        pathMatch: 'full'
    },
    // {
    //     path: Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL,
    //     //component: CustomerRequestComponent,
    //     pathMatch: 'full'
    // },
    // {
    //     path: Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL,
    //     //component: CustomerTransactionComponent,
    //     pathMatch: 'full'
    // }
]);