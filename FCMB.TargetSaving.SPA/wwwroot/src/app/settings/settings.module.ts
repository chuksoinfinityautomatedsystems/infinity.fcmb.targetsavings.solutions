


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

//import { ProspectsComponent } from './components/prospects.component';

import { HeaderService } from "../commons/services/header.service";
import { UtilService } from "../commons/services/utils.services";
//import { CustomerService } from "./services/customer.services";
import { AuthenticationService } from "../authentication/services/authentication.service";
import { SettingsService } from './services/settings.service';
import { TargetFrequencyComponent } from './components/target-frequency.component';
import { TargetRequestTypeComponent } from './components/request-type.component';
import { CategoryComponent } from './components/category.component';
//import { RegisteredCustomersComponent } from './components/registeredCustomers.component';
//import { TargetGoalsComponent } from './components/goal.component';
//import { CustomerRequestComponent } from './components/request.component';
//import { CustomerTransactionComponent } from './components/transactions.component';


@NgModule({
    imports: [CommonModule, ReactiveFormsModule, HttpModule, RouterModule],
    declarations: [CategoryComponent, TargetFrequencyComponent, TargetRequestTypeComponent],
    providers: [AuthenticationService, HeaderService, UtilService, SettingsService]
})
export class SettingsModule { }
