


import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";

// import { EventHandlerService } from "../../commons/services/event_handlers.service";

// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 

import { AuthenticationService } from "../../authentication/services/authentication.service";
import { UtilService } from "../../commons/services/utils.services";
import { BaseComponent } from "../../commons/components/base.component";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { CustomerService } from "../services/customer.services";
import { Transaction } from "../models/transaction.model";
import { PaginationRequest } from "../../commons/models/pagination-model";

function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Component({
    templateUrl: "html/customer/transactions.html"
})
export class CustomerTransactionComponent extends BaseComponent implements OnInit, OnDestroy {

    requestOngoing: boolean;

    customerTransactions: Array<Transaction>;
    getTransactionSub: any;
    transaction: Transaction;
    pageIndex: number;
    pageSubtitle: string;
    tabIndex: number;
    lastId: number = 0;
    totalItemsCount: number;
    totalItemsInView: number;

    constructor(private authService: AuthenticationService, private customerService: CustomerService,
        private router: Router, private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef) {

        super(authService, router, formBuilder, cdRef)
        this.requestOngoing = false;

        document.title = "Customer Transactions";
        this.customerTransactions = new Array<Transaction>();
        this.pageIndex = 1;
        this.transaction = new Transaction();
        this.tabIndex = 1;

        document.body.style.backgroundColor = '#eee';
    }

    ngOnInit(): void {

        let self = this;
        self.requestOngoing = true;

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {

            let loadingModalInterval = setInterval(function () {
                self._getTransactions();
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getTransactions();
        }

        //  self.SetupTimeout();
    }

    _getTransactions() {
        let self = this;
        //let pagination: PaginationRequest = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 }
        let pagination: PaginationRequest = {
            pageSize: 2,
            lastIdFetched: this.lastId,
            pageNumber: 0,
            filterByDateRange: false,
            startDate: new Date(),
            endDate: new Date()
        }
        self.getTransactionSub = self.customerService.getTransactions(pagination)
            .subscribe(response => {

                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    //let transactions = response.responseItem.items;
                    console.log('transactions', response.responseItem)
                    // if (transactions.length > 0) {
                    //     self.customerTransactions = transactions.filter(o => o.approvedBy === '');
                    // }
                    if (this.lastId > 0) {
                        for (let i of response.responseItem.items) {
                            self.customerTransactions.push(i);
                        }

                    } else {
                        self.customerTransactions = response.responseItem.items;
                    }
                    this.totalItemsCount = response.responseItem.totalItemsCount;
                    this.totalItemsInView = self.customerTransactions.length;
                }
            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }

    _getApprovedCustomerTransactions() {
        let self = this;
        let pagination: PaginationRequest = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 }
        self.getTransactionSub = self.customerService.getPendingCustomerTransactions(pagination)
            .subscribe(response => {

                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    let transactions = response.responseItem.items;
                    if (transactions.length > 0) {
                        self.customerTransactions = transactions.filter(o => o.approvedBy != '');
                    }
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });

    }
    getMore(event) {
        event.stopPropagation();
        if (this.customerTransactions && this.customerTransactions.length > 0) {
            this.lastId = this.customerTransactions[this.customerTransactions.length - 1].transactionId;
            console.log('Last Id', this.lastId)
            this._getTransactions();

        }
    }

    GetPendingTransactions(event) {
        event.stopPropagation();
        let self = this;
        self.requestOngoing = true;
        self.tabIndex = 1;
        self.customerTransactions.length = 0

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {

            let loadingModalInterval = setInterval(function () {
                self._getTransactions();
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getTransactions();
        }
    }

    GetApprovedTransactions(event) {
        event.stopPropagation();
        let self = this;
        self.requestOngoing = true;
        self.tabIndex = 2;
        self.customerTransactions.length = 0

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {

            let loadingModalInterval = setInterval(function () {
                self._getApprovedCustomerTransactions();
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getApprovedCustomerTransactions();
        }
    }

    goToTransactionDetails(transactionId: number) {
        if (this.customerTransactions && this.customerTransactions.length > 0) {

            let transactionSelected = this.customerTransactions.filter(x => x.transactionId == transactionId)[0];
            console.log('transactionSelected', transactionSelected)
            if (transactionSelected) {
                this.transaction = transactionSelected;
                console.log('this.transaction', this.transaction)
                this.pageIndex = 2;
                this.pageSubtitle = 'Details'
                this.cdRef.detectChanges();
            }
        }
    }

    goBackToList() {
        this.transaction = new Transaction();
        this.pageIndex = 1;
        this.pageSubtitle = '';
        this.cdRef.detectChanges();
    }


    addNewTransaction() {
        this.transaction = new Transaction();
        this.pageIndex = 3;
        this.pageSubtitle = 'New Transaction';
        this.cdRef.detectChanges();
    }


    approveTransaction(event) {
        event.stopPropagation();

        let self = this;
        self.requestOngoing = true;

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {

            let loadingModalInterval = setInterval(function () {
                UtilService.toastSuccess("Transaction approved successfully.");
                self.pageIndex = 1;
                self.pageSubtitle = ''
                self.requestOngoing = false;
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            UtilService.toastSuccess("Transaction approved Successfully.");
            self.pageIndex = 1;
            self.pageSubtitle = ''
        }
    }



    addTransaction(event) {
        event.stopPropagation();

        let self = this;
        self.requestOngoing = true;

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {

            let loadingModalInterval = setInterval(function () {
                UtilService.toastSuccess("Transaction added successfully.");
                self.pageIndex = 1;
                self.pageSubtitle = ''
                self.requestOngoing = false;
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            UtilService.toastSuccess("Transaction added Successfully.");
            self.pageIndex = 1;
            self.pageSubtitle = ''
        }
    }


    goHome(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    }



    goToProspects(event) {
        event.stopPropagation()
        this.router.navigate([Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    }


    goToCustomers(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL]);
        return false;
    }



    goToCustomerGoals(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    }

    goToCustomerRequests(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL]);
        return false;
    }

    goToCustomerTransactions(event) {
        event.stopPropagation();
        return false;
    }



    ngOnDestroy() {
        if (this.getTransactionSub) {
            this.getTransactionSub.unsubscribe();
        }

    }
}