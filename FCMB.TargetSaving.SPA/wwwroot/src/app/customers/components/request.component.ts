


import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";

// import { EventHandlerService } from "../../commons/services/event_handlers.service";

// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 

import { AuthenticationService } from "../../authentication/services/authentication.service";
import { UtilService } from "../../commons/services/utils.services";
import { BaseComponent } from "../../commons/components/base.component";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { CustomerService } from "../services/customer.services";
import { Request } from "../models/request.model";
import { PaginationRequest } from "../../commons/models/pagination-model";

function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Component({
    templateUrl: "html/customer/requests.html"
})
export class CustomerRequestComponent extends BaseComponent implements OnInit, OnDestroy {

    requestOngoing: boolean;

    pendingCustomerRequests: Array<Request>;
    treatedCustomerRequests: Array<Request>;
    getRequestsSub: any;
    request: Request;
    showDetailViews: boolean;
    tabIndex: number;
    lastIdTreated: number = 0;
    totalItemsCountTreated: number;
    totalItemsInViewTreated: number;
    lastIdNew: number = 0;
    totalItemsCountNew: number;
    totalItemsInViewNew: number;

    constructor(private authService: AuthenticationService, private customerService: CustomerService,
        private router: Router, private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef) {

        super(authService, router, formBuilder, cdRef)
        this.requestOngoing = false;

        document.title = "Customer Requests";
        this.pendingCustomerRequests = new Array<Request>();
        this.treatedCustomerRequests = new Array<Request>();
        this.showDetailViews = false;
        this.request = new Request();
        this.tabIndex = 1;

        document.body.style.backgroundColor = '#eee';
    }

    ngOnInit(): void {

        let self = this;
        self.requestOngoing = true;

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {

            let loadingModalInterval = setInterval(function () {
                self._getPendingCustomerRequests();
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getPendingCustomerRequests();
        }

        //  self.SetupTimeout();
    }

    _getPendingCustomerRequests() {
        let self = this;
        this.request = null;
        //let pagination: PaginationRequest = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 }
        let pagination: PaginationRequest = {
            pageSize: 2,
            lastIdFetched: this.lastIdNew,
            pageNumber: 0,
            filterByDateRange: false,
            startDate: new Date(),
            endDate: new Date()
        }
        self.getRequestsSub = self.customerService.getNewCustomerRequests(pagination)
            .subscribe(response => {

                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    //let requests = response.responseItem.items;
                    console.log('requests.length', response.responseItem)
                    if (response.responseItem.items.length > 0) {
                        let requests = response.responseItem.items.filter(o => o.status === 'NEW');
                        if (this.lastIdNew > 0) {
                            for (let i of response.responseItem.items) {
                                self.pendingCustomerRequests.push(i);
                            }

                        } else {
                            self.pendingCustomerRequests = response.responseItem.items;
                        }
                        this.totalItemsCountNew = response.responseItem.totalItemsCount;
                        this.totalItemsInViewNew = self.pendingCustomerRequests.length;
                    }
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }

    _getTreatedCustomerRequests() {
        let self = this;
        this.request = null;
        //let pagination: PaginationRequest = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 }
        let pagination: PaginationRequest = {
            pageSize: 2,
            lastIdFetched: this.lastIdTreated,
            pageNumber: 0,
            filterByDateRange: false,
            startDate: new Date(),
            endDate: new Date()
        }
        self.getRequestsSub = self.customerService.getCustomerTreatedRequests(pagination)
            .subscribe(response => {

                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    // let requests = response.responseItem.items;
                    // if (requests.length > 0) {
                    //     self.customerRequests = requests.filter(o => o.status === 'TREATED');
                    // }
                    console.log('getCustomerTreatedRequests', response.responseItem)
                    if (response.responseItem.items.length > 0) {
                        let requests = response.responseItem.items.filter(o => o.status === 'TREATED');
                        if (this.lastIdTreated > 0) {
                            for (let i of response.responseItem.items) {
                                self.treatedCustomerRequests.push(i);
                            }

                        } else {
                            self.treatedCustomerRequests = response.responseItem.items;
                        }
                        this.totalItemsCountTreated = response.responseItem.totalItemsCount;
                        this.totalItemsInViewTreated = self.treatedCustomerRequests.length;
                    }
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });

    }

    getMoreNew(event) {
        event.stopPropagation();
        if (this.pendingCustomerRequests && this.pendingCustomerRequests.length > 0) {
            this.lastIdNew = this.pendingCustomerRequests[this.pendingCustomerRequests.length - 1].requestId;
            console.log('Last Id', this.lastIdNew)
            this._getPendingCustomerRequests();

        }
    }

    getMoreTreated(event) {
        event.stopPropagation();
        if (this.treatedCustomerRequests && this.treatedCustomerRequests.length > 0) {
            this.lastIdTreated = this.treatedCustomerRequests[this.treatedCustomerRequests.length - 1].requestId;
            console.log('Last Id', this.lastIdTreated)
            this._getTreatedCustomerRequests();

        }
    }

    GetPendingRequest(event) {
        event.stopPropagation();
        let self = this;
        self.requestOngoing = true;
        self.tabIndex = 1;
        this.lastIdNew = 0;
        self.pendingCustomerRequests.length = 0

        self._getPendingCustomerRequests();
    }

    GetTreatedRequest(event) {
        event.stopPropagation();
        let self = this;
        self.requestOngoing = true;
        self.tabIndex = 2;
        self.treatedCustomerRequests.length = 0

        self._getTreatedCustomerRequests();
    }

    goToNewDetails(requestId: number) {
        if (this.pendingCustomerRequests && this.pendingCustomerRequests.length > 0) {

            let requestSelected = this.pendingCustomerRequests.filter(x => x.requestId == requestId)[0];
            if (requestSelected) {
                this.request = requestSelected;
                this.showDetailViews = true;
                this.cdRef.detectChanges();
            }
        }
    }
    goToTreatedDetails(requestId: number) {
        if (this.treatedCustomerRequests && this.treatedCustomerRequests.length > 0) {

            let requestSelected = this.treatedCustomerRequests.filter(x => x.requestId == requestId)[0];
            if (requestSelected) {
                this.request = requestSelected;
                this.showDetailViews = true;
                this.cdRef.detectChanges();
            }
        }
    }

    goBackToList() {
        this.request = new Request();
        this.showDetailViews = false;
        this.cdRef.detectChanges();
    }

    treatRequest(event) {
        event.stopPropagation();

        let self = this;
        self.requestOngoing = true;

        self.request.approvalStatus = 'APPROVED'
        self.getRequestsSub = self.customerService.treatRequests(self.request)
            .subscribe(response => {

                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    let loadingModalInterval = setInterval(function () {
                        UtilService.toastSuccess("Request treated successfully.");
                        self.showDetailViews = false;
                        self.requestOngoing = false;
                        _window().clearInterval(loadingModalInterval);
                        this.GetTreatedRequest();
                    }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
                } else {
                    UtilService.toastSuccess(response.apiResponse.friendlyMessage);
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }
    rejectRequest(event) {
        event.stopPropagation();

        let self = this;
        self.requestOngoing = true;

        this.request.approvalStatus = 'REJECTED'
        self.getRequestsSub = self.customerService.treatRequests(this.request)
            .subscribe(response => {

                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    let loadingModalInterval = setInterval(function () {
                        UtilService.toastSuccess("Request treated successfully.");
                        self.showDetailViews = false;
                        self.requestOngoing = false;
                        _window().clearInterval(loadingModalInterval);
                        this.GetTreatedRequest();
                    }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
                } else {
                    UtilService.toastSuccess(response.apiResponse.friendlyMessage);
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });
    }


    goHome(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    }



    goToProspects(event) {
        event.stopPropagation()
        this.router.navigate([Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    }


    goToCustomers(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL]);
        return false;
    }


    goToCustomerTransactions(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    }

    goToCustomerGoals(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    }




    goToCustomerRequests(event) {
        event.stopPropagation();
        return false;
    }



    ngOnDestroy() {
        if (this.getRequestsSub) {
            this.getRequestsSub.unsubscribe();
        }

    }
}