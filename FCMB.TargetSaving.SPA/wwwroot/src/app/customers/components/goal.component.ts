


import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";

// import { EventHandlerService } from "../../commons/services/event_handlers.service";

// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 

import { AuthenticationService } from "../../authentication/services/authentication.service";
import { BaseComponent } from "../../commons/components/base.component";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { CustomerService } from "../services/customer.services";
import { Goal } from "../models/goal.model";
import { PaginationRequest } from "../../commons/models/pagination-model";

function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Component({
    templateUrl: "html/customer/goals.html"
})
export class TargetGoalsComponent extends BaseComponent implements OnInit, OnDestroy {

    requestOngoing: boolean;

    customerGoals: Array<Goal>;
    getGoalsSub: any;
    goal: Goal;
    showDetailViews: boolean;
    lastId: number = 0;
    totalItemsCount: number;
    totalItemsInView: number;

    constructor(private authService: AuthenticationService, private customerService: CustomerService,
        private router: Router, private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef) {

        super(authService, router, formBuilder, cdRef)
        this.requestOngoing = false;

        document.title = "Customer Goals";
        this.customerGoals = new Array<Goal>();
        this.showDetailViews = false;
        this.goal = new Goal();

        document.body.style.backgroundColor = '#eee';
    }

    ngOnInit(): void {

        let self = this;
        self.requestOngoing = true;

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {

            let loadingModalInterval = setInterval(function () {
                self._getCustomerGoals();
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getCustomerGoals();
        }

        //  self.SetupTimeout();
    }

    _getCustomerGoals() {
        let self = this;
        console.log('_getCustomerGoals');
        //let pagination: PaginationRequest = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 }
        let pagination: PaginationRequest = {
            pageSize: 2,
            lastIdFetched: this.lastId,
            pageNumber: 0,
            filterByDateRange: false,
            startDate: new Date(),
            endDate: new Date()
        }
        self.getGoalsSub = self.customerService.getCustomerGoals(pagination)
            .subscribe(response => {
                console.log('_getCustomerGoals', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    //self.customerGoals = response.responseItem.items;
                    //console.log('self.customerGoals', self.customerGoals)
                    if (this.lastId > 0) {
                        for (let i of response.responseItem.items) {
                            self.customerGoals.push(i);
                        }

                    } else {
                        self.customerGoals = response.responseItem.items;
                    }
                    // self.customerGoals = self.customerGoals.sort((leftSide, rightSide): number => {
                    //     if (leftSide.targetGoalsId > rightSide.targetGoalsId) return 1;
                    //     if (leftSide.targetGoalsId < rightSide.targetGoalsId) return -1;
                    //     return 0
                    // })
                    this.totalItemsCount = response.responseItem.totalItemsCount;
                    this.totalItemsInView = self.customerGoals.length;
                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });

    }
    getMore(event) {
        event.stopPropagation();
        if (this.customerGoals && this.customerGoals.length > 0) {
            this.lastId = this.customerGoals[this.customerGoals.length - 1].targetGoalsId;
            console.log('Last Id', this.lastId)
            this._getCustomerGoals();

        }
    }

    goToGoalDetails(goalId: number) {
        if (this.customerGoals && this.customerGoals.length > 0) {

            let goalSelected = this.customerGoals.filter(x => x.targetGoalsId == goalId)[0];
            if (goalSelected) {
                goalSelected.targetGoalPics = 'data:image/jpg;base64,' + goalSelected.targetGoalPics;
                this.goal = goalSelected;
                this.showDetailViews = true;
                this.cdRef.detectChanges();
            }
        }
    }

    goBackToList() {
        this.goal = new Goal();
        this.showDetailViews = false;
        this.cdRef.detectChanges();
    }




    goHome(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    }



    goToProspects(event) {
        event.stopPropagation()
        this.router.navigate([Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    }


    goToCustomers(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL]);
        return false;
    }



    goToCustomerRequests(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL]);
        return false;
    }


    goToCustomerTransactions(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    }

    goToCustomerGoals(event) {
        event.stopPropagation();
        return false;
    }




    ngOnDestroy() {
        if (this.getGoalsSub) {
            this.getGoalsSub.unsubscribe();
        }

    }
}