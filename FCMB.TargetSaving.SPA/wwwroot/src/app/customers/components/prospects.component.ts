


import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";

// import { EventHandlerService } from "../../commons/services/event_handlers.service";

// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 

import { AuthenticationService } from "../../authentication/services/authentication.service";
import { BaseComponent } from "../../commons/components/base.component";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { CustomerService } from "../services/customer.services";
import { Prospect } from "../models/prospects.model";
import { PaginationRequest } from "../../commons/models/pagination-model";

function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Component({
    templateUrl: "html/customer/prospects.html"
})
export class ProspectsComponent extends BaseComponent implements OnInit, OnDestroy {

    requestOngoing: boolean;

    prospects: Array<Prospect>;
    getProspectsSub: any;
    prospect: Prospect;
    showDetailViews: boolean;
    lastId: number = 0;
    totalItemsCount: number;
    totalItemsInView: number;

    constructor(private authService: AuthenticationService, private customerService: CustomerService,
        private router: Router, private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef) {

        super(authService, router, formBuilder, cdRef)
        this.requestOngoing = false;

        document.title = "Prospects";
        this.prospects = new Array<Prospect>();
        this.showDetailViews = false;
        this.prospect = new Prospect();

        document.body.style.backgroundColor = '#eee';
    }

    ngOnInit(): void {

        let self = this;
        self.requestOngoing = true;
        console.log('Appsettings.APP_MODE.toLowerCase()', Appsettings.APP_MODE.toLowerCase());
        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {

            let loadingModalInterval = setInterval(function () {
                self._getProspects();
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getProspects();
        }

        //  self.SetupTimeout();
    }

    _getProspects() {
        let self = this;
        //let pagination: PaginationRequest = { pageNumber: 0, pageSize: 2, lastIdFetched: this.lastId, filterByDateRange: false }
        let pagination: PaginationRequest = {
            pageSize: 2,
            lastIdFetched: this.lastId,
            pageNumber: 0,
            filterByDateRange: false,
            startDate: new Date(),
            endDate: new Date()
        }
        console.log('Page', JSON.stringify(pagination));
        self.getProspectsSub = self.customerService.getProspects(pagination)
            .subscribe(response => {
                console.log('Prospect', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    if (this.lastId > 0) {
                        for (let i of response.responseItem.items) {
                            self.prospects.push(i);
                        }

                    } else {
                        self.prospects = response.responseItem.items;
                    }
                    // self.prospects = self.prospects.sort((leftSide, rightSide): number => {
                    //     if (leftSide.prospectId > rightSide.prospectId) return 1;
                    //     if (leftSide.prospectId < rightSide.prospectId) return -1;
                    //     return 0
                    // })
                    console.log('sorted', self.prospects)
                    this.totalItemsCount = response.responseItem.totalItemsCount;
                    this.totalItemsInView = self.prospects.length;

                }

            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });

    }
    getMoreProspect(event) {
        event.stopPropagation();
        if (this.prospects && this.prospects.length > 0) {
            this.lastId = this.prospects[this.prospects.length - 1].prospectId;
            console.log('Last Id', this.lastId)
            this._getProspects();

        }
    }

    goToProspectDetails(prospectId: number) {
        if (this.prospects && this.prospects.length > 0) {

            let prospectSelected = this.prospects.filter(x => x.prospectId == prospectId)[0];
            if (prospectSelected) {
                this.prospect = prospectSelected;
                this.showDetailViews = true;
                this.cdRef.detectChanges();
            }
        }
    }

    goBackToList() {
        this.prospect = new Prospect();
        this.showDetailViews = false;
        this.cdRef.detectChanges();
    }



    goToProspects(event) {
        event.stopPropagation();
        return false;
    }


    goHome(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    }


    goToCustomers(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL]);
        return false;
    }


    goToCustomerRequests(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL]);
        return false;
    }

    goToCustomerGoals(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    }


    goToCustomerTransactions(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    }


    ngOnDestroy() {
        if (this.getProspectsSub) {
            this.getProspectsSub.unsubscribe();
        }

    }
}
