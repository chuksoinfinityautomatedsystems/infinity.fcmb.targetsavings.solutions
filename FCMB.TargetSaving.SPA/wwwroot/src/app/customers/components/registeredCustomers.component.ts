


import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";

// import { EventHandlerService } from "../../commons/services/event_handlers.service";

// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 

import { AuthenticationService } from "../../authentication/services/authentication.service";
import { BaseComponent } from "../../commons/components/base.component";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { CustomerService } from "../services/customer.services";
import { Customer } from "../models/customer.model";
import { PaginationRequest } from "../../commons/models/pagination-model";

function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Component({
    templateUrl: "html/customer/registered-customers.html"
})
export class RegisteredCustomersComponent extends BaseComponent implements OnInit, OnDestroy {

    requestOngoing: boolean;

    customers: Array<Customer>;
    getCustomersSub: any;
    customer: Customer;
    showDetailViews: boolean;
    lastId: number = 0;
    totalItemsCount: number;
    totalItemsInView: number;

    constructor(private authService: AuthenticationService, private customerService: CustomerService,
        private router: Router, private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef) {

        super(authService, router, formBuilder, cdRef)
        this.requestOngoing = false;

        document.title = "Registered Customers";
        this.customers = new Array<Customer>();

        this.showDetailViews = false;
        this.customer = new Customer();

        document.body.style.backgroundColor = '#eee';
    }

    ngOnInit(): void {

        let self = this;
        self.requestOngoing = true;

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {

            let loadingModalInterval = setInterval(function () {
                self._getCustomers();
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getCustomers();
        }

        //  self.SetupTimeout();
    }

    _getCustomers() {
        let self = this;
        console.log('_getCustomers');
        //let pagination: PaginationRequest = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 }
        let pagination: PaginationRequest = {
            pageSize: 2,
            lastIdFetched: this.lastId,
            pageNumber: 0,
            filterByDateRange: false,
            startDate: new Date(),
            endDate: new Date()
        }
        self.getCustomersSub = self.customerService.getNRegisteredCustomers(pagination)
            .subscribe(response => {
                console.log('_getCustomers', response);
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS &&
                    response.responseItem) {
                    //self.customers = response.responseItem.items;
                    if (this.lastId > 0) {
                        for (let i of response.responseItem.items) {
                            self.customers.push(i);
                        }

                    } else {
                        self.customers = response.responseItem.items;
                    }
                    // self.customers = self.customers.sort((leftSide, rightSide): number => {
                    //     if (leftSide.customerProfileId > rightSide.customerProfileId) return 1;
                    //     if (leftSide.customerProfileId < rightSide.customerProfileId) return -1;
                    //     return 0
                    // })
                    console.log('sorted', self.customers)
                    this.totalItemsCount = response.responseItem.totalItemsCount;
                    this.totalItemsInView = self.customers.length;
                }
            },
                (error: any) => {
                    self.requestOngoing = false;
                    //show error toast.
                    self.cdRef.detectChanges();
                },
                () => {
                    self.requestOngoing = false;
                    self.cdRef.detectChanges();
                });

    }
    getMore(event) {
        event.stopPropagation();
        if (this.customers && this.customers.length > 0) {
            this.lastId = this.customers[this.customers.length - 1].customerProfileId;
            console.log('Last Id', this.lastId)
            this._getCustomers();

        }
    }

    goToCustomerDetails(customerId: number) {
        if (this.customers && this.customers.length > 0) {

            let customerSelected = this.customers.filter(x => x.customerProfileId == customerId)[0];
            if (customerSelected) {
                customerSelected.profilePicture = 'data:image/jpg;base64,' + customerSelected.profilePicture;
                this.customer = customerSelected;

                this.showDetailViews = true;
                this.cdRef.detectChanges();
            }
        }
    }

    goToProspects(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    }

    goToCustomers(event) {
        event.stopPropagation();
        return false;
    }


    goToCustomerGoals(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    }


    goToCustomerRequests(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL]);
        return false;
    }


    goToCustomerTransactions(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    }

    goBackToList() {
        this.customer = new Customer();
        this.showDetailViews = false;
        this.cdRef.detectChanges();
    }


    goHome(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.DASHBOARD_ROUTER_URL]);
        return false;
    }



    ngOnDestroy() {
        if (this.getCustomersSub) {
            this.getCustomersSub.unsubscribe();
        }

    }
}