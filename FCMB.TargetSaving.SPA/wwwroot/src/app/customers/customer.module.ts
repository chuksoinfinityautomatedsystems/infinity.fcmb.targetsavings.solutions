


import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { ProspectsComponent } from './components/prospects.component';

import { HeaderService } from "../commons/services/header.service";
import { UtilService } from "../commons/services/utils.services";
import { CustomerService } from "./services/customer.services";
import { AuthenticationService } from "../authentication/services/authentication.service";
import { RegisteredCustomersComponent } from './components/registeredCustomers.component';
import { TargetGoalsComponent } from './components/goal.component';
import { CustomerRequestComponent } from './components/request.component';
import { CustomerTransactionComponent } from './components/transactions.component';


@NgModule({
    imports: [CommonModule, ReactiveFormsModule, HttpModule, RouterModule],
    declarations: [ProspectsComponent, RegisteredCustomersComponent,
        TargetGoalsComponent, CustomerRequestComponent, CustomerTransactionComponent],
    providers: [AuthenticationService, HeaderService, UtilService, CustomerService]
})
export class CustomerModule { }
