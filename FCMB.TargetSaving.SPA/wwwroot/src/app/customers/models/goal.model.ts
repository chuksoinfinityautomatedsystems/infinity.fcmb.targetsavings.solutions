import { Customer } from "./customer.model";





export class Goal {
    targetGoalsId: number;
    goalName: string;
    goalDescription: string;
    setupDate: string;
    targetAmount: number;
    targetFrequency: string;
    targetFrequencyAmount: number;
    lastRunDate: string;
    nextRunDate: string;

    goalBalance: number;

    targetGoalPics: string;
    goalStartDate: string;
    goalEndDate: string;
    goalStartAmount: number;


    goalDebitType: string;
    goalDebitBank: string;
    goalDebitAccountNo: string;
    goalStatus: string;
    goalnterestEarned: number;
    goalCardTokenDetails: string;
    customerName: string;
    customerProfileId: number;
    customerProfile: Customer;
}