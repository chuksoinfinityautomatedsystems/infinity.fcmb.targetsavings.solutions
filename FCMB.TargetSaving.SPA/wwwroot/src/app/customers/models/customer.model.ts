


export class Customer {
    customerProfileId: number;
    biometricVerificationNo: string;
    prospectId: number;
    surname: string;
    firstName: string;
    emailAddress: string;
    phoneNumber: string;
    isFCMBCustomer: boolean;
    userName: string;
    profileStatus: string;
    dateCreated: string;
    address: string;
    nextOfKinPhone: string;
    nextOfKinEmail: string;
    nextOfKinFullName: string;
    lastLoginDate: string;
    lastPasswordChanged: string;
    computerDetails: string;
    profilePicture: string;
}