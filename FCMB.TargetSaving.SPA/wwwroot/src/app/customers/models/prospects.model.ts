

export class Prospect {
    prospectId: number;
    biometricVerificationNo: string
    surname: string;
    firstName: string;
    emailAddress: string;
    phoneNumber: string;
    isFCMBCustomer: boolean;
    dateCreated: string;
}



