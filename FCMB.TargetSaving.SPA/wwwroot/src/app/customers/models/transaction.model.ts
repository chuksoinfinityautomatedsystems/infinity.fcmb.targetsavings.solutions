import { Goal } from "./goal.model";





export class Transaction {
    transactionId: number;
    targetGoalsId: number;
    goalName: string;
    customerName: string;
    customerProfileId: number;
    transactionType: string;
    transactionDate: string;
    amount: number;
    narration: string;
    postedBy: string;
    postedDate: string;
    approvedBy: string;
    approvedDate: string;
    status: string;
    targetGoal: Goal
}