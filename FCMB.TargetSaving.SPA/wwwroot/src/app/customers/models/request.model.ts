




export class Request {
    targetGoalsId: number;
    goalName: string;
    requestId: number;
    customerName: string;
    customerProfileId: number;
    requestType: string;
    requestDate: string;
    requestDescription: string;
    status: string;
    treatedBy: string;
    dateTreated: string;
    goalStatus: string;
    approvalStatus: string;
}