



import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthorizeService } from "../authentication/services/authorization.services";
import { Appsettings } from "../commons/constants/appsettings.constant";

import { ProspectsComponent } from './components/prospects.component';
import { RegisteredCustomersComponent } from './components/registeredCustomers.component';
import { TargetGoalsComponent } from './components/goal.component';
import { CustomerRequestComponent } from './components/request.component';
import { CustomerTransactionComponent } from './components/transactions.component';

export const customerRoutes = RouterModule.forChild([
    {
        path: Appsettings.PROSPECTS_ROUTER_URL,
        component: ProspectsComponent,
        pathMatch: 'full'
    }//, canActivate: [AuthorizeService] }
    ,
    {
        path: Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL,
        component: RegisteredCustomersComponent,
        pathMatch: 'full'
    },
    {
        path: Appsettings.CUSTOMERS_GOALS_ROUTER_URL,
        component: TargetGoalsComponent,
        pathMatch: 'full'
    },
    {
        path: Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL,
        component: CustomerRequestComponent,
        pathMatch: 'full'
    },
    {
        path: Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL,
        component: CustomerTransactionComponent,
        pathMatch: 'full'
    }
]);