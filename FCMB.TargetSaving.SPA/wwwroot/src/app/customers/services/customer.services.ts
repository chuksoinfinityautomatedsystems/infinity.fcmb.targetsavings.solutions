



import { Injectable } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import "rxjs/Rx";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { HeaderService } from "../../commons/services/header.service";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { AuthenticationService } from "../../authentication/services/authentication.service";
import { UtilService } from "../../commons/services/utils.services";
import { PaginationRequest } from "../../commons/models/pagination-model";
import { Request } from "../models/request.model";


declare var jsPDF: any;

function _window(): any {
    // return the global native browser window object
    return window;
}

@Injectable()
export class CustomerService {

    constructor(private authService: AuthenticationService,
        private _headerService: HeaderService, private http: Http) {
    }


    getProspects_old() {
        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "offline") {
            return this.http.post(Appsettings.GET_PROSPECTIVE_CUSTOMERS_URL,
                this._headerService.getRequestJsonHeaders()
            ).map((_response: Response) => {
                return _response.json();
            });
        }
        else {
            return Observable.of({
                apiResponse: {
                    responseCode: ResponseCodes.SUCCESS,
                    responseDescription: "string",
                    friendlyMessage: "string"
                },
                responseItem: [
                    {
                        prospectId: 1,
                        biometricVerificationNo: '37388337473',
                        surname: "Putin",
                        firstName: 'Donald',
                        emailAddress: 'donald@trump.com',
                        phoneNumber: "234058493984",
                        isFCMBCustomer: 'y',
                        dateCreated: '04-04-2018'
                    },
                    {
                        prospectId: 2,
                        biometricVerificationNo: '54572883',
                        surname: "Messi",
                        firstName: 'Leo',
                        emailAddress: 'leo@messi.com',
                        phoneNumber: "2345783937884",
                        isFCMBCustomer: 'n',
                        dateCreated: '04-04-2018'
                    }]
            });
        }
    }

    getProspects(pagination: PaginationRequest) {
        return this.http.post(Appsettings.GET_N_PROSPECTIVE_CUSTOMERS_URL, JSON.stringify(pagination),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    getNRegisteredCustomers(pagination: PaginationRequest) {
        return this.http.post(Appsettings.GET_REGISTERED_CUSTOMERS_URL, JSON.stringify(pagination),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    getRegisteredCustomers() {
        return this.http.post(Appsettings.GET_REGISTERED_CUSTOMERS_URL,
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }




    getDailyIncrementStat() {

        return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    getRecentTransactions() {

        return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }


    getRecentRequests() {

        return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    getCustomerGoals(pagination: PaginationRequest) {
        return this.http.post(Appsettings.GET_TARGET_GOAL_URL, JSON.stringify(pagination),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }



    getNewCustomerRequests(pagination: PaginationRequest) {
        return this.http.post(Appsettings.GET_N_NEW_CUSTOMER_REQUEST_LIST_URL, JSON.stringify(pagination),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }


    getCustomerTreatedRequests(pagination: PaginationRequest) {
        return this.http.post(Appsettings.GET_N_TREATED_CUSTOMER_REQUEST_LIST_URL, JSON.stringify(pagination),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    treatRequests(request: Request) {
        console.log('request', request)
        let body = {
            requestId: request.requestId,
            approvalStatus: request.approvalStatus
        }
        return this.http.post(Appsettings.GET_TREAT_CUSTOMER_REQUEST_LIST_URL, JSON.stringify(body),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    getTransactions(pagination: PaginationRequest) {
        return this.http.post(Appsettings.GET_TRANSACTIONS_URL, JSON.stringify(pagination),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }


    getPendingCustomerTransactions(pagination: PaginationRequest) {
        return this.http.post(Appsettings.GET_TRANSACTIONS_URL, JSON.stringify(pagination),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }



    getApprovedCustomerTransactions() {
        return this.http.post(Appsettings.GET_REGISTERED_CUSTOMERS_URL,
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }



    // getDashboardSummary() { 

    //     // if (UtilService.StringIsNullOrEmpty(accountNo) || UtilService.StringIsNullOrEmpty(startDate) || UtilService.StringIsNullOrEmpty(endDate)) {
    //     //     throw new Error("Invalid parameters.");
    //     // }

    //     // if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
    //     //     return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
    //     //         { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
    //     //         this._headerService.getRequestJsonHeaders()
    //     //     ).map((_response: Response) => {
    //     //         return _response.json();
    //     //     });
    //     // }

    //     if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
    //         return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
    //            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
    //             this._headerService.getRequestJsonHeaders()
    //         ).map((_response: Response) => {
    //             return _response.json();
    //         });
    //     }
    //     else {
    //         return Observable.of({
    //             ResponseCode: ResponseCodes.SUCCESS,
    //             Summary: [
    //                 {
    //                     date: "2018-03-12T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 1967882.49,
    //                     balance: 529191.00,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-12T12:14:30.6534304+01:00",
    //                     description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
    //                     amount: 18000000000000000,
    //                     balance: 2497073.49,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-11T12:14:30.6534304+01:00",
    //                     description: "BO KNIGHT METAL MANUFACTURING CO LTD",
    //                     amount: 647383929.0,
    //                     balance: 6738892.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-11T12:14:30.6534304+01:00",
    //                     description: "Tud Nuru Zinc",
    //                     amount: 1039499223.0,
    //                     balance: 6342552939.0,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-03-10T12:14:30.6534304+01:00",
    //                     description: "FASTPAY_CHARGES",
    //                     amount: 4488438848.0,
    //                     balance: 2738897332.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-10T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 7827781392.0,
    //                     balance: 4892023773.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-12T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 35788582.21,
    //                     balance: 3674833.49,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-09T12:14:30.6534304+01:00",
    //                     description: "FASTPAY_CHARGES",
    //                     amount: 123748839.0,
    //                     balance: 9048377282.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-08T12:14:30.6534304+01:00",
    //                     description: "Tud Nuru Zinc",
    //                     amount: 1112737733.0,
    //                     balance: 490483822.0,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-03-08T12:14:30.6534304+01:00",
    //                     description: "BO KNIGHT METAL MANUFACTURING CO LTD",
    //                     amount: 384938823.0,
    //                     balance: 400483724.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-07T12:14:30.6534304+01:00",
    //                     description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
    //                     amount: 56373883.0,
    //                     balance: 4773883.30,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-03-07T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 312494993.0,
    //                     balance: 444004982.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-06T12:14:30.6534304+01:00",
    //                     description: "Tud Nuru Zinc",
    //                     amount: 321849493.0,
    //                     balance: 430488833.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-08T12:14:30.6534304+01:00",
    //                     description: "BO KNIGHT METAL MANUFACTURING CO LTD",
    //                     amount: 56737883.0,
    //                     balance: 400483724.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-14T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 11903002.67,
    //                     balance: 4242233.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-02-14T12:14:30.6534304+01:00",
    //                     description: "Tud Nuru Zinc",
    //                     amount: 33434534.0,
    //                     balance: 42254543.0,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-01-14T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 342335233.0,
    //                     balance: 4353243233.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-14T12:14:30.6534304+01:00",
    //                     description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
    //                     amount: 89383892.0,
    //                     balance: 9594003.98,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-03-14T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 24643233.0,
    //                     balance: 4242233.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-05-14T12:14:30.6534304+01:00",
    //                     description: "BO KNIGHT METAL MANUFACTURING CO LTD",
    //                     amount: 33445432.0,
    //                     balance: 343543533.0,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-01-14T12:14:30.6534304+01:00",
    //                     description: "FASTPAY_CHARGES",
    //                     amount: 333234.0,
    //                     balance: 445343223.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-14T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 32334234.40,
    //                     balance: 563533224.04,
    //                     transactionType: "C"
    //                 },

    //             ]

    //         });
    //     }
    // }







}
