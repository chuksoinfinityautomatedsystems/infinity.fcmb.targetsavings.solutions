export class DailyStatistic {
    name: string;
    totalCount: number;
    dailyCount: number;
    dailyCountPercentage: number;
    statisticsDateTime: Date;
}
