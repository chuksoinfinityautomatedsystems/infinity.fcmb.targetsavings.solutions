
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { DecimalPipe } from '@angular/common';

import { DashboardComponent } from './components/dashboard.component';

import { HeaderService } from "../commons/services/header.service";
import { UtilService } from "../commons/services/utils.services";
import { DashboardService } from "./services/dashboard.services";
import { AuthenticationService } from "../authentication/services/authentication.service";


@NgModule({
    imports: [CommonModule, ReactiveFormsModule, HttpModule, RouterModule],
    declarations: [DashboardComponent],
    providers: [AuthenticationService, HeaderService, UtilService, DashboardService, DecimalPipe]
})
export class DashboardModule { }
