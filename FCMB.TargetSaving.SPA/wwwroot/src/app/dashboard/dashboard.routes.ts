

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { AuthorizeService } from "../authentication/services/authorization.services";

import { DashboardComponent } from './components/dashboard.component';

export const dashboardRoutes = RouterModule.forChild([
    { path: "Dashboard", component: DashboardComponent }//, canActivate: [AuthorizeService] }
]);