

import { Injectable } from "@angular/core";
import { Http, Response, Headers } from "@angular/http";
import { FormControl, FormGroup, Validators } from '@angular/forms';
import "rxjs/Rx";
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { HeaderService } from "../../commons/services/header.service";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { AuthenticationService } from "../../authentication/services/authentication.service";
import { UtilService } from "../../commons/services/utils.services";
import { PaginationRequest } from "../../commons/models/pagination-model";


declare var jsPDF: any;

function _window(): any {
    // return the global native browser window object
    return window;
}

@Injectable()
export class DashboardService {

    constructor(private authService: AuthenticationService,
        private _headerService: HeaderService, private http: Http) {
    }

    getProspectCount() {
        return this.http.post(Appsettings.GET_PROSPECT_COUNT_URL,
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    getDailyStatistics() {
        let today = new Date();
        console.log('', JSON.stringify(today))
        return this.http.post(Appsettings.GET_DAILY_STATISTICS_URL, JSON.stringify(today),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    getCustomerProfileCount() {
        return this.http.post(Appsettings.GET_CUSTOMER_PROFILE_COUNT_URL,
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }
    GetTargetsCount() {
        return this.http.post(Appsettings.GET_TARGET_GOAL_COUNT_URL,
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    GetTargetSavingsRequestCountByStatus(status: string) {
        return this.http.post(Appsettings.GET_TARGET_GOAL_COUNT_URL, JSON.stringify(status),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    GetTopNPendingTransactions(pagination: PaginationRequest) {
        return this.http.post(Appsettings.GET_RECENT_TRANSACTION_URL, JSON.stringify(pagination),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    GetTopNRecentRequests(pagination: PaginationRequest) {
        return this.http.post(Appsettings.GET_N_NEW_CUSTOMER_REQUEST_LIST_URL, JSON.stringify(pagination),
            this._headerService.getRequestJsonHeaders()
        ).map((_response: Response) => {
            return _response.json();
        });
    }

    getDashboardSummary() {

        // if (UtilService.StringIsNullOrEmpty(accountNo) || UtilService.StringIsNullOrEmpty(startDate) || UtilService.StringIsNullOrEmpty(endDate)) {
        //     throw new Error("Invalid parameters.");
        // }

        // if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
        //     return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
        //         { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
        //         this._headerService.getRequestJsonHeaders()
        //     ).map((_response: Response) => {
        //         return _response.json();
        //     });
        // }

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
                // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
                this._headerService.getRequestJsonHeaders()
            ).map((_response: Response) => {
                return _response.json();
            });
        }
        else {
            return Observable.of({
                ResponseCode: ResponseCodes.SUCCESS,
                Summaries: [
                    {
                        num: 1000,
                        type: 'prospect'
                    },
                    {
                        num: 2100,
                        type: 'customer'
                    },
                    {
                        num: 200,
                        type: 'request'
                    },
                    {
                        num: 1500,
                        type: 'goal'
                    }
                ]
            });
        }
    }



    getDailyIncrementStat() {

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
                // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
                this._headerService.getRequestJsonHeaders()
            ).map((_response: Response) => {
                return _response.json();
            });
        }
        else {
            return Observable.of({
                ResponseCode: ResponseCodes.SUCCESS,
                Summaries: [
                    {
                        num: 20,
                        type: 'prospect'
                    },
                    {
                        num: 52,
                        type: 'customer'
                    },
                    {
                        num: 21,
                        type: 'goal'
                    }
                ]
            });
        }
    }

    getRecentTransactions() {

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
                // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
                this._headerService.getRequestJsonHeaders()
            ).map((_response: Response) => {
                return _response.json();
            });
        }
        else {
            return Observable.of({
                ResponseCode: ResponseCodes.SUCCESS,
                Transactions: [
                    {
                        customerName: "Chuks Popoola",
                        targetGoal: 'Ford Explorer Limited Edition',
                        amount: 700768
                    },
                    {
                        customerName: "Andy Rubin",
                        targetGoal: 'Said Business School',
                        amount: 675029
                    },
                    {
                        customerName: "Sage Rodriguez",
                        targetGoal: 'Wifey Business',
                        amount: 540292
                    },
                    {
                        customerName: "Philip Chaney",
                        targetGoal: 'Europe Tour',
                        amount: 9083927
                    },
                    {
                        customerName: "Alade Bekky",
                        targetGoal: 'Property Purchase',
                        amount: 8927811
                    }
                ]
            });
        }
    }


    getRecentRequests() {

        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
            return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
                // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
                this._headerService.getRequestJsonHeaders()
            ).map((_response: Response) => {
                return _response.json();
            });
        }
        else {
            return Observable.of({
                ResponseCode: ResponseCodes.SUCCESS,
                Requests: [
                    {
                        customerName: "Warren Buffet",
                        requestType: 'Liquidation',
                    },
                    {
                        customerName: "Akande Adebola",
                        requestType: 'Password Reset'
                    },
                    {
                        customerName: "Uju BRed",
                        requestType: 'Wallet Complaint'
                    },
                    {
                        customerName: "Abe Shinzo",
                        requestType: 'Liquidation'
                    },
                    {
                        customerName: "Yinka Coker",
                        requestType: 'Password Reset'
                    }
                ]
            });
        }
    }


    // getDashboardSummary() { 

    //     // if (UtilService.StringIsNullOrEmpty(accountNo) || UtilService.StringIsNullOrEmpty(startDate) || UtilService.StringIsNullOrEmpty(endDate)) {
    //     //     throw new Error("Invalid parameters.");
    //     // }

    //     // if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
    //     //     return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
    //     //         { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
    //     //         this._headerService.getRequestJsonHeaders()
    //     //     ).map((_response: Response) => {
    //     //         return _response.json();
    //     //     });
    //     // }

    //     if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() == "online") {
    //         return this.http.post(Appsettings.AUTHENTICATE_USER_ID_URL,
    //            // { AccountNumber: accountNo, TransactionOwner: 0 }, // TransactionOwner == 1 for beneficiary transactions
    //             this._headerService.getRequestJsonHeaders()
    //         ).map((_response: Response) => {
    //             return _response.json();
    //         });
    //     }
    //     else {
    //         return Observable.of({
    //             ResponseCode: ResponseCodes.SUCCESS,
    //             Summary: [
    //                 {
    //                     date: "2018-03-12T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 1967882.49,
    //                     balance: 529191.00,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-12T12:14:30.6534304+01:00",
    //                     description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
    //                     amount: 18000000000000000,
    //                     balance: 2497073.49,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-11T12:14:30.6534304+01:00",
    //                     description: "BO KNIGHT METAL MANUFACTURING CO LTD",
    //                     amount: 647383929.0,
    //                     balance: 6738892.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-11T12:14:30.6534304+01:00",
    //                     description: "Tud Nuru Zinc",
    //                     amount: 1039499223.0,
    //                     balance: 6342552939.0,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-03-10T12:14:30.6534304+01:00",
    //                     description: "FASTPAY_CHARGES",
    //                     amount: 4488438848.0,
    //                     balance: 2738897332.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-10T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 7827781392.0,
    //                     balance: 4892023773.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-12T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 35788582.21,
    //                     balance: 3674833.49,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-09T12:14:30.6534304+01:00",
    //                     description: "FASTPAY_CHARGES",
    //                     amount: 123748839.0,
    //                     balance: 9048377282.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-08T12:14:30.6534304+01:00",
    //                     description: "Tud Nuru Zinc",
    //                     amount: 1112737733.0,
    //                     balance: 490483822.0,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-03-08T12:14:30.6534304+01:00",
    //                     description: "BO KNIGHT METAL MANUFACTURING CO LTD",
    //                     amount: 384938823.0,
    //                     balance: 400483724.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-07T12:14:30.6534304+01:00",
    //                     description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
    //                     amount: 56373883.0,
    //                     balance: 4773883.30,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-03-07T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 312494993.0,
    //                     balance: 444004982.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-06T12:14:30.6534304+01:00",
    //                     description: "Tud Nuru Zinc",
    //                     amount: 321849493.0,
    //                     balance: 430488833.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-08T12:14:30.6534304+01:00",
    //                     description: "BO KNIGHT METAL MANUFACTURING CO LTD",
    //                     amount: 56737883.0,
    //                     balance: 400483724.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-14T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 11903002.67,
    //                     balance: 4242233.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-02-14T12:14:30.6534304+01:00",
    //                     description: "Tud Nuru Zinc",
    //                     amount: 33434534.0,
    //                     balance: 42254543.0,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-01-14T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 342335233.0,
    //                     balance: 4353243233.0,
    //                     transactionType: "C"
    //                 },
    //                 {
    //                     date: "2018-03-14T12:14:30.6534304+01:00",
    //                     description: "TRF IFO GOLDEN THREAD COMPANY LIMITED",
    //                     amount: 89383892.0,
    //                     balance: 9594003.98,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-03-14T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 24643233.0,
    //                     balance: 4242233.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-05-14T12:14:30.6534304+01:00",
    //                     description: "BO KNIGHT METAL MANUFACTURING CO LTD",
    //                     amount: 33445432.0,
    //                     balance: 343543533.0,
    //                     transactionType: "C"
    //                 }, {
    //                     date: "2018-01-14T12:14:30.6534304+01:00",
    //                     description: "FASTPAY_CHARGES",
    //                     amount: 333234.0,
    //                     balance: 445343223.0,
    //                     transactionType: "D"
    //                 },
    //                 {
    //                     date: "2018-03-14T12:14:30.6534304+01:00",
    //                     description: "INWARD CHQ IFO WESTERN METAL PRODUCTS CO LTD",
    //                     amount: 32334234.40,
    //                     balance: 563533224.04,
    //                     transactionType: "C"
    //                 },

    //             ]

    //         });
    //     }
    // }







}