
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup } from "@angular/forms";
import { DecimalPipe } from '@angular/common';

// import { EventHandlerService } from "../../commons/services/event_handlers.service";

// import { ComponentEventPublisherEnum } from "../../commons/enums/component_event_publisher.enum"; 

import { AuthenticationService } from "../../authentication/services/authentication.service";
import { BaseComponent } from "../../commons/components/base.component";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { DashboardService } from "../services/dashboard.services";
import { RecentTransaction } from "../models/transaction.model";
import { CustomerRequest } from "../models/request.model";
import { PaginationRequest } from "../../commons/models/pagination-model";
import { DailyStatistic } from "../models/daily-statistics.model";
import { Request } from "../../customers/models/request.model";

function _window(): any {
    // return the global native browser window object
    return window;
}

declare var $: any;

@Component({
    templateUrl: "html/dashboard.html"
})
export class DashboardComponent extends BaseComponent implements OnInit, OnDestroy {

    requestOngoing: boolean;

    getSummarySub: any; getDailyStatSub: any; getRecentRequestSub: any;
    getRecentTransactionSub: any;

    totalNumOfProspects: number; totalNumOfCustomers: number; totalNumOfGoals: number; totalNumOfRequests: number; summaryFetched: boolean;

    prospectsDailyIncrementStat: number;
    customersDailyIncrementStat: number;
    goalsDailyIncrementStat: number;
    incrementStatFetched: boolean;
    recentTransactionFetched: boolean;
    recentRequestFetched: boolean;

    recentTransactions: Array<RecentTransaction>;
    recentRequests: Array<Request>;

    hasProspectCount: boolean;
    prospectCount: string;
    hasCustomerProfileCount: boolean;
    customerProfileCount: string;
    hasTargetGoalCount: boolean;
    targetGoalCount: string;
    hasRequestCount: boolean;
    requestCount: string;
    isSettings: boolean = false;
    dailyProspectStatistics: DailyStatistic;
    dailyCustomerStatistics: DailyStatistic;
    dailyGoalStatistics: DailyStatistic;
    isDailyStatisticsDone: boolean = false;


    constructor(private authService: AuthenticationService, private dashService: DashboardService,
        private decimalPipe: DecimalPipe,
        private router: Router, private formBuilder: FormBuilder, private cdRef: ChangeDetectorRef) {

        super(authService, router, formBuilder, cdRef)
        this.requestOngoing = false;

        this.summaryFetched = this.incrementStatFetched = this.recentTransactionFetched = this.recentRequestFetched = false;

        document.title = "Dashboard";
        this.recentTransactions = new Array<RecentTransaction>();
        this.recentRequests = new Array<Request>();
        this.dailyProspectStatistics = new DailyStatistic();
        this.dailyCustomerStatistics = new DailyStatistic();
        this.dailyGoalStatistics = new DailyStatistic();

        document.body.style.backgroundColor = '#eee';
    }

    ngOnInit(): void {

        let self = this;
        self.requestOngoing = true;
        self.isSettings = false;


        if (Appsettings.APP_MODE && Appsettings.APP_MODE.toLowerCase() != "online") {

            let loadingModalInterval = setInterval(function () {
                // self.requestOngoing = false;
                self._getDashboardSummary();
                self.getRecentRequests();
                self.getRecentTransactions();
                self.getDailyStat();
                self.getProspectCount();
                self.getCustomerProfileCount();
                self.GetTargetsCount();
                self.GetTargetSavingsRequestCount();
                self.getDailyStatistics();
                _window().clearInterval(loadingModalInterval);
            }, Appsettings.OFFLINE_LOADING_INTERVAL_IN_MICROSECOND);
        }
        else {
            self._getDashboardSummary();
            self.getRecentRequests();
            self.getRecentTransactions();
            self.getDailyStat();
            self.getProspectCount();
            self.getCustomerProfileCount();
            self.GetTargetsCount();
            self.GetTargetSavingsRequestCount();
            self.getDailyStatistics();
        }

        let allDashboardDataLoadDoneInterval = setInterval(function () {

            if (self.incrementStatFetched && self.recentRequestFetched && self.recentTransactionFetched
                && self.summaryFetched) {
                self.requestOngoing = false;
            }
            _window().clearInterval(allDashboardDataLoadDoneInterval);
        }, 500);

        //  self.SetupTimeout();
    }

    closeSettings(event) {
        event.stopPropagation()
        let self = this;
        self.isSettings = false;
        return false;
    }
    openSettings(event) {
        event.stopPropagation()
        let self = this;
        self.isSettings = true;
        return false;
    }

    getProspectCount() {
        let self = this;
        console.log('getProspectCount')
        self.getRecentRequestSub = self.dashService.getProspectCount()
            .subscribe(response => {
                console.log('getProspectCount', response)
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    this.totalNumOfProspects = response.responseItem;
                    this.prospectCount = this.decimalPipe.transform(response.responseItem);
                    this.hasProspectCount = true;
                } else {
                    this.hasProspectCount = false;
                }

                self.recentRequestFetched = true;
                self.cdRef.detectChanges();
            },
                (error: any) => {
                    self.recentRequestFetched = true;
                    self.cdRef.detectChanges();
                },
                () => {
                });

    }
    getDailyStatistics() {
        let self = this;
        console.log('getDailyStatistics')
        self.getRecentRequestSub = self.dashService.getDailyStatistics()
            .subscribe(response => {
                console.log('getDailyStatistics', response)
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    let dailyStatistics: DailyStatistic[] = response.responseItem;
                    console.log('dailyStatistics', dailyStatistics)
                    this.dailyCustomerStatistics = dailyStatistics.filter(o => o.name === 'Customer')[0];
                    this.dailyProspectStatistics = dailyStatistics.filter(o => o.name === 'Prospect')[0];
                    this.dailyGoalStatistics = dailyStatistics.filter(o => o.name === 'Goal')[0];
                    this.isDailyStatisticsDone = true;
                } else {
                    this.isDailyStatisticsDone = false;
                }

                //self.recentRequestFetched = true;
                self.cdRef.detectChanges();
            },
                (error: any) => {
                    self.recentRequestFetched = true;
                    self.cdRef.detectChanges();
                },
                () => {
                });

    }
    getCustomerProfileCount() {
        let self = this;
        console.log('getCustomerProfileCount')
        self.getRecentRequestSub = self.dashService.getCustomerProfileCount()
            .subscribe(response => {
                console.log('getCustomerProfileCount', response)
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    this.totalNumOfCustomers = response.responseItem;
                    this.customerProfileCount = this.decimalPipe.transform(response.responseItem);
                    this.hasCustomerProfileCount = true;
                } else {
                    this.hasCustomerProfileCount = false;
                }

                self.recentRequestFetched = true;
                self.cdRef.detectChanges();
            },
                (error: any) => {
                    self.recentRequestFetched = true;
                    self.cdRef.detectChanges();
                },
                () => {
                });

    }
    GetTargetsCount() {
        let self = this;
        console.log('GetTargetsCount')
        self.getRecentRequestSub = self.dashService.GetTargetsCount()
            .subscribe(response => {
                console.log('GetTargetsCount', response)
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    this.totalNumOfGoals = response.responseItem;
                    this.targetGoalCount = this.decimalPipe.transform(response.responseItem);
                    this.hasTargetGoalCount = true;
                } else {
                    this.hasTargetGoalCount = false;
                }

                self.recentRequestFetched = true;
                self.cdRef.detectChanges();
            },
                (error: any) => {
                    self.recentRequestFetched = true;
                    self.cdRef.detectChanges();
                },
                () => {
                });

    }

    GetTargetSavingsRequestCount() {
        let self = this;
        console.log('GetTargetSavingsRequestCount')
        let status = 'NEW';
        self.getRecentRequestSub = self.dashService.GetTargetSavingsRequestCountByStatus(status)
            .subscribe(response => {
                console.log('GetTargetSavingsRequestCount', response)
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {
                    this.totalNumOfRequests = response.responseItem;
                    this.requestCount = this.decimalPipe.transform(response.responseItem);
                    this.hasRequestCount = true;
                } else {
                    this.hasRequestCount = false;
                }

                self.recentRequestFetched = true;
                self.cdRef.detectChanges();
            },
                (error: any) => {
                    self.recentRequestFetched = true;
                    self.cdRef.detectChanges();
                },
                () => {
                });

    }

    goToTargetImages(event) {
        console.log('goToTargetImages')
        event.stopPropagation();
        this.router.navigate([Appsettings.SETTINGS_TARGET_CATEGORY_ROUTER_URL]);
        return false;
    }


    _getDashboardSummary() {
        let self = this;

        self.getSummarySub = self.dashService.getDashboardSummary()
            .subscribe(response => {

                if (response.ResponseCode == ResponseCodes.SUCCESS) {

                    let summaries = response.Summaries;

                    if (summaries && summaries.length > 0) {
                        let prospects = summaries.filter(x => x.type.toLowerCase() == 'prospect');
                        let customers = summaries.filter(x => x.type.toLowerCase() == 'customer');
                        let requests = summaries.filter(x => x.type.toLowerCase() == 'request');
                        let goals = summaries.filter(x => x.type.toLowerCase() == 'goal');

                        if (prospects && prospects.length > 0) {
                            self.totalNumOfProspects = prospects[0].num;
                        }
                        else {
                            self.totalNumOfProspects = 0;
                        }

                        if (customers && customers.length > 0) {
                            self.totalNumOfCustomers = customers[0].num;
                        }
                        else {
                            self.totalNumOfCustomers = 0;
                        }

                        if (requests && requests.length > 0) {
                            self.totalNumOfRequests = requests[0].num;
                        }
                        else {
                            self.totalNumOfRequests = 0;
                        }

                        if (goals && goals.length > 0) {
                            self.totalNumOfGoals = goals[0].num;
                        }
                        else {
                            self.totalNumOfGoals = 0;
                        }
                    }
                    else {
                        self.totalNumOfGoals = self.totalNumOfRequests = self.totalNumOfProspects = self.totalNumOfCustomers = 0;
                    }
                }

                self.summaryFetched = true;
                self.cdRef.detectChanges();
            },
                (error: any) => {
                    self.summaryFetched = true;
                    self.cdRef.detectChanges();
                },
                () => {
                });
    }



    getDailyStat() {
        let self = this;

        self.getDailyStatSub = self.dashService.getDailyIncrementStat()
            .subscribe(response => {

                if (response.ResponseCode == ResponseCodes.SUCCESS) {

                    let summaries = response.Summaries;

                    if (summaries && summaries.length > 0) {
                        let prospects = summaries.filter(x => x.type.toLowerCase() == 'prospect');
                        let customers = summaries.filter(x => x.type.toLowerCase() == 'customer');
                        let goals = summaries.filter(x => x.type.toLowerCase() == 'goal');

                        if (prospects && prospects.length > 0) {
                            self.prospectsDailyIncrementStat = prospects[0].num;
                        }
                        else {
                            self.prospectsDailyIncrementStat = 0;
                        }

                        if (customers && customers.length > 0) {
                            self.customersDailyIncrementStat = customers[0].num;
                        }
                        else {
                            self.customersDailyIncrementStat = 0;
                        }

                        if (goals && goals.length > 0) {
                            self.goalsDailyIncrementStat = goals[0].num;
                        }
                        else {
                            self.goalsDailyIncrementStat = 0;
                        }
                    }
                    else {
                        self.goalsDailyIncrementStat = self.customersDailyIncrementStat = self.prospectsDailyIncrementStat = 0;
                    }
                }

                self.incrementStatFetched = true;
                self.cdRef.detectChanges();
            },
                (error: any) => {
                    self.incrementStatFetched = true;
                    self.cdRef.detectChanges();
                },
                () => {
                });
    }



    getRecentTransactions() {
        let self = this;
        console.log('getRecentTransactions');
        let pagination: PaginationRequest = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 }
        self.getDailyStatSub = self.dashService.GetTopNPendingTransactions(pagination)
            .subscribe(response => {
                console.log('getRecentTransactions', response)
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {

                    // if (!self.recentTransactions) {
                    //     self.recentTransactions = new Array<RecentTransaction>();
                    // }

                    self.recentTransactions = response.responseItem.items;
                }

                self.recentTransactionFetched = true;
                self.cdRef.detectChanges();
                self.requestOngoing = false;
            },
                (error: any) => {
                    self.recentTransactionFetched = true;
                    self.cdRef.detectChanges();
                },
                () => {
                });
    }

    getRecentRequests() {
        let self = this;
        console.log('getRecentRequests');
        let pagination: PaginationRequest = { pageNumber: 0, pageSize: 5, lastIdFetched: 0 }
        self.getRecentRequestSub = self.dashService.GetTopNRecentRequests(pagination)
            .subscribe(response => {

                console.log('getRecentRequests', response)
                if (response.apiResponse.responseCode == ResponseCodes.SUCCESS) {

                    // if (!self.recentTransactions) {
                    //     self.recentTransactions = new Array<RecentTransaction>();
                    // }

                    self.recentRequests = response.responseItem.items;
                }

                self.recentTransactionFetched = true;
                self.cdRef.detectChanges();
                self.requestOngoing = false;
            },
                (error: any) => {
                    self.recentRequestFetched = true;
                    self.cdRef.detectChanges();
                },
                () => {
                });
    }


    goToProspects(event) {
        event.stopPropagation()
        this.router.navigate([Appsettings.PROSPECTS_ROUTER_URL]);
        return false;
    }


    goToCustomers(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.REGISTERED_CUSTOMERS_ROUTER_URL]);
        return false;
    }


    goToCustomerRequests(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_REQUESTS_ROUTER_URL]);
        return false;
    }

    goToCustomerGoals(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_GOALS_ROUTER_URL]);
        return false;
    }


    goToCustomerTransactions(event) {
        event.stopPropagation();
        this.router.navigate([Appsettings.CUSTOMERS_TRANSACTIONS_ROUTER_URL]);
        return false;
    }

    ngOnDestroy() {
        if (this.getRecentRequestSub) {
            this.getRecentRequestSub.unsubscribe();
        }

        if (this.getDailyStatSub) {
            this.getDailyStatSub.unsubscribe();
        }
        if (this.getDailyStatSub) {
            this.getDailyStatSub.unsubscribe();
        }

        if (this.getSummarySub) {
            this.getSummarySub.unsubscribe();
        }
    }
}