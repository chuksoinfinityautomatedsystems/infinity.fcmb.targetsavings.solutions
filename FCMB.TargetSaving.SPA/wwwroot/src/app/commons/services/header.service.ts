
import { Injectable } from '@angular/core';
import { Headers } from '@angular/http';


@Injectable()
export class HeaderService {

    getRequestJsonHeaders() {
        let _headers = new Headers();
        _headers.append("Accept", "application/json");
        _headers.append("Content-Type", "application/json");

        return { headers: _headers };
    }


    getBearerRequestJsonHeaders(bearerToken: string) {

        if (bearerToken === undefined || bearerToken.length < 1) {
            bearerToken = "";
            //throw new Error("bearerToken missing.");
        }

        let _headers = new Headers();
        _headers.append("Accept", "application/json");
        _headers.append("Content-Type", "application/json");
        _headers.append("Authorization", `Bearer ${bearerToken}`);

        return { headers: _headers };
    }


    getBasicRequestJsonHeaders(base64Token: string) {

        if (base64Token === undefined || base64Token.length < 1) {
            base64Token = "";
            // throw new Error("base64Token missing.");
        }

        let _headers = new Headers();
        _headers.append("Accept", "application/json");
        _headers.append("Content-Type", "application/json");
        _headers.append("Authorization", `Basic ${base64Token}`);

        return { headers: _headers };
    }

    getFormRequestJsonHeaders() {

        let _headers = new Headers();
        _headers.append("Accept", "application/x-www-form-urlencoded");
        _headers.append("Content-Type", "application/x-www-form-urlencoded");

        return { headers: _headers };
    }


}