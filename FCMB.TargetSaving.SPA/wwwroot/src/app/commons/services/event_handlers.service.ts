
import { Injectable, Output, EventEmitter } from '@angular/core';
import { ComponentEventPublisherEnum } from "../enums/component_event_publisher.enum";


@Injectable()
export class EventHandlerService {

    //@Output() static accountDetailsEventPublisher = new EventEmitter<{ forComponent: ComponentEventPublisherEnum, accountNo: string }>();

    @Output() static editObjectEventPublisher = new EventEmitter<{ forComponent: ComponentEventPublisherEnum, obj: any }>();


    @Output() static isSeletectedEventPublisher = new EventEmitter<{ forComponent: ComponentEventPublisherEnum, isSelected: boolean, id: number }>();

    // static emitNumberOfStates(forComponent: ComponentEventPublisherEnum, numberOfStates: number) {
    //     this.stateNumEventPublisher.emit({ forComponent, numberOfStates });
    // }

}