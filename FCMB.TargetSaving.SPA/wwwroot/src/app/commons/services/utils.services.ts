
import { Injectable, Output, EventEmitter } from '@angular/core';

declare var $: any;
function _window(): any {
    return window;
}

interface IBrowserSpec {
    name: string, version: string
}

@Injectable()
export class UtilService {

    private static interval: any;
    public static authErrorMessage = "You are not authorized to perform this operation. Please signin to continue.";

    static clone(srcObj: any) {
        return (Object as any).assign({}, srcObj);
    }

    static copy(srcObj: any, destObj: any) {
        return (Object as any).assign(destObj, srcObj);
    }


    static trim(txt: string) {

        txt = (txt as string).toString();

        if (String.prototype.trim) {
            return txt.trim();
        }
        else {
            return txt.replace(/^\s+|\s+$/g, '');
        }
    }

    static convertToDate(date: string) {
        var parts = date.split('/');
        return new Date(parseInt(parts[2]), parseInt(parts[1]) - 1, parseInt(parts[0])); // months start from 0;
    }

    static getScreenHeight() {
        return _window().screen.height;
    }

    static setElementWidth() {
        return _window().document.getElementsByTagName('body')[0].offsetWidth - 17;
    }

    static getElementWidth() {
        return document.getElementsByTagName('body')[0].offsetWidth;
    }

    // static groupArray(list: Array<any>) {

    //     let result = list.reduce(function (r, a) {
    //     r[a.make] = r[a.make] || [];
    //     r[a.make].push(a);
    //     return r;
    // }, Object.create(null));

    // }

    static StringIsNullOrEmpty(text: string): boolean {

        if (!text) {
            return true;
        }

        if (UtilService.trim(text).length < 1) {
            return true;
        }

        return false;
    }

    static ObjectKeysLength(obj: any) {

        if (!Object.keys) {

            // Object.keys = function (obj) {
            (Object as any).keys = function (obj) {
                var keys = [], k;

                for (k in obj) {
                    if (Object.prototype.hasOwnProperty.call(obj, k)) {
                        keys.push(k);
                    }
                }
                return keys.length;
            };
        }
        else {
            return Object.keys(obj).length
        }
    }

    static GetUniqueArray(array: Array<any>) {
        var arr = [];
        for (var i = 0; i < array.length; i++) {
            if (arr.indexOf(array[i]) == -1) {
                arr.push(array[i]);
            }
        }

        return arr;
    }


    static StringContainsDigit(value: string) {
        if (value) {
            return /\d/.test(value);
        }

        return false;
    }

    static StringContainsUpperCaseCharacters(value: string) {

        if (value) {
            return value.toLowerCase() != value;
        }

        return false;
    }

    static StringContainsLowerCaseCharacters(value: string) {
        if (value) {
            return value.toUpperCase() != value;
        }

        return false;
    }


    static getBrowserObj(): IBrowserSpec {

        let userAgent = (_window().navigator.userAgent as string);
        let tempArray;
        let M = userAgent.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];

        if (/trident/i.test(M[1])) {
            tempArray = /\brv[ :]+(\d+)/g.exec(userAgent) || [];

            if (tempArray != null) {
                return { name: 'IE', version: (tempArray[1] || '') };
            }
        }

        if (M[1] === 'Chrome') {
            tempArray = userAgent.match(/\b(OPR|Edge)\/(\d+)/);

            if (tempArray != null) {
                return { name: tempArray[1].replace('OPR', 'Opera'), version: tempArray[2] };
            }
        }

        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];

        if ((tempArray = userAgent.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tempArray[1]);
        return { name: M[0], version: M[1] };



        // {name: "MSIE", version: "8"}
        // {name: "Chrome", version: "39"}
        // {name: "Firefox", version: "36"}
        // {name: "Opera", version: "26"}
        // {name: "Edge", version: "14"}
        // {name: "Safari", version: "9"}
    }



    static toastError(message: string) {
        let type = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];

        $.notify({
            // icon: "notifications",
            message: message
        }, {
                type: 'danger',
                timer: 200000,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });
    }


    static toastSuccess(message: string) {
        let type = ['', 'info', 'danger', 'success', 'warning', 'rose', 'primary'];

        $.notify({
            // icon: "notifications",
            message: message
        }, {
                type: 'success',
                timer: 200000,
                placement: {
                    from: 'top',
                    align: 'center'
                }
            });
    }


}