export class PaginationRequest {
    pageSize: number;
    lastIdFetched: number;
    pageNumber: number;
    filterByDateRange?: Boolean;
    startDate?: Date;
    endDate?: Date;
}