
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


@NgModule({
    imports: [CommonModule],
    declarations: [],
    providers: [],
    // exports: [DropdownComponent, CountryCodeDropdownComponent, SchedulePaymentFrequencyDropdownComponent]
})
export class SharedModule { }
