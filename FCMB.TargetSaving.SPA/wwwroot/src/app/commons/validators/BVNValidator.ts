

import { FormControl } from '@angular/forms';

export class BVNValidator {
    static isValid(formControl: FormControl) {

        if (formControl.value && formControl.value.length > 0) {

            if (formControl.value.length > 11) {
                return { bvnInvalid: true };
            }
        }

        return null;
    }
}
