
import {FormControl} from '@angular/forms';

export class EmailValidator{
    static isValid(formControl: FormControl){
        let emailRegex = /^[ ]*\w+([-+.']\w+)*@\w+([-.]\\w+)*\.\w+([-.]\w+)*[ ]*$/i;

        if(formControl.value){
            let isValid = emailRegex.test(formControl.value);
            if(isValid === false){
                return {emailInvalid: true};
            }
        }       
       
        return null;
    }
}