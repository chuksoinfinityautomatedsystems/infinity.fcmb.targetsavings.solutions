
import {FormControl} from '@angular/forms';

export class PhoneNumberValidator{
    static isValid(formControl: FormControl){
        let phoneRegex = /^[0-9+][ ]*[0-9][0-9\s+]+$/i;

        if(formControl.value){
            let isValid = phoneRegex.test(formControl.value);
            if(isValid === false){
                return {phoneNumberInvalid: true};
            }
        }      
       
        return null;
    }
}