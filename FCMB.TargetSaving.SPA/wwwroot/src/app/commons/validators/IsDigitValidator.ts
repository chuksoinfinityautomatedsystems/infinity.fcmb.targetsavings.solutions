



import { FormControl } from '@angular/forms';

export class IsDigitValidator {
    static isValid(formControl: FormControl) {

        if (formControl.value) {

            let val = formControl.value as string;
            if (val && val.length > 0) {
                val = val.replace(/\s/g, '');
                if (/^[0-9]+$/.test(val) === false) {
                    return { isDigitInvalid: true };
                }
            }
        }

        return null;
    }
}