

import { FormControl } from '@angular/forms';

export class RequiredNonZeroIntegerValidator {
    static isValid(formControl: FormControl) {

        try {
            let value = parseInt(formControl.value);
            if (value < 1) {
                return { requiredNonZeroInteger: true };
            }
        }
        catch (Ex) {
            return { requiredNonZeroInteger: true };
        }

        return null;
    }
}