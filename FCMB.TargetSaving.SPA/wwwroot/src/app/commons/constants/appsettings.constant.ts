


export class Appsettings {
  //static APP_MODE = "online";
  static APP_MODE = "offline";
  static API_ENDPOINT = "http://tsaservices.infinityautomatedsystems.com/api/";
  //static API_ENDPOINT = "https://localhost:8444/api/";
  //static API_ENDPOINT = "http://localhost:37067/api/";
  static AUTHENTICATE_USER_ID_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/AuthenticateRIBUser";


  static TECHNICAL_ERROR_MESSAGE = "Your request cannot be completed at the moment due to a technical error";
  static LOCKED_USER_ERROR_MESSAGE = "Your account has been blocked. Contact a branch administrator to reser your password";
  static TIMEOUT_LOGOUT_MESSAGE = "To protect your account, we have automatically logged you out of internet banking. You can login again when you are ready."
  static LOGOUT_MESSAGE = "You have been successfully logged out";
  static OFFLINE_LOADING_INTERVAL_IN_MICROSECOND = 500;
  static TOAST_INTERVAL_IN_MICROSECOND = 1500;
  static TOAST_FADE_OUT_IN_MICROSECOND = 1600;
  static LOGIN_ERROR_SHAKE_INTERVAL = 500;
  static SESSION_LOGOUT_COUNTER = 60;
  static autoLogoutTimeInMicrosec = 300000;


  static LOGIN_ROUTER_URL = 'Login';

  static BASE_CUSTOMER_INFORMATION_URL = Appsettings.API_ENDPOINT + 'CustomerInformation/';
  static GET_PROSPECTIVE_CUSTOMERS_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + 'GetPropectiveCustomers';
  static GET_N_PROSPECTIVE_CUSTOMERS_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + 'GetNProspectiveCustomers';
  static GET_PROSPECT_COUNT_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + 'GetProspectsCount';
  static GET_CUSTOMER_PROFILE_COUNT_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + 'GetCustomerProfilesCount';
  static GET_TARGET_GOAL_COUNT_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + 'GetTargetsCount';
  static GET_TARGET_REQUEST_COUNT_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + 'GetTargetSavingsRequestCountByStatus';
  static GET_RECENT_REQUESTS_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + 'GetNRecentTargetSavingsRequest';
  static GET_REGISTERED_CUSTOMERS_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + "GetNCustomerProfile";
  static GET_TARGET_GOAL_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + "GetNTargetGoalAsync";
  static GET_N_NEW_CUSTOMER_REQUEST_LIST_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + "GetNNewTargetSavingsRequest";
  static GET_N_TREATED_CUSTOMER_REQUEST_LIST_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + "GetNTreatedTargetSavingsRequest";
  static GET_TREAT_CUSTOMER_REQUEST_LIST_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + "TreatCustomerRequests";
  static GET_DAILY_STATISTICS_URL = Appsettings.BASE_CUSTOMER_INFORMATION_URL + "GetDailyStatistics";

  static BASE_TRANSACTIONS_URL = Appsettings.API_ENDPOINT + 'Transactions/';
  static GET_RECENT_TRANSACTION_URL = Appsettings.BASE_TRANSACTIONS_URL + 'GetNPendingTransactions';
  static GET_TRANSACTIONS_URL = Appsettings.BASE_TRANSACTIONS_URL + 'GetNTransactions';

  static BASE_UTILITY_URL = Appsettings.API_ENDPOINT + 'Utility/';
  static GET_TARGET_CATEGORY_URL = Appsettings.BASE_UTILITY_URL + 'GetCategory';
  static ADD_TARGET_CATEGORY_URL = Appsettings.BASE_UTILITY_URL + 'CreateCategory';
  static UPDATE_TARGET_CATEGORY_URL = Appsettings.BASE_UTILITY_URL + 'UpdatECategory';
  static DELETE_TARGET_CATEGORY_URL = Appsettings.BASE_UTILITY_URL + 'RemoveCategory';

  static GET_TARGET_FREQUENCY_URL = Appsettings.BASE_UTILITY_URL + 'GetTargetSavingsFrequency';
  static ADD_TARGET_FREQUENCY_URL = Appsettings.BASE_UTILITY_URL + 'CreteTargetSavingsFrequency';
  static UPDATE_TARGET_FREQUENCY_URL = Appsettings.BASE_UTILITY_URL + 'UpdateTargetSavingsFrequency';
  static DELETE_TARGET_FREQUENCY_URL = Appsettings.BASE_UTILITY_URL + 'RemoveTargetSavingsFrequency';

  static GET_TARGET_REQUEST_TYPE_URL = Appsettings.BASE_UTILITY_URL + 'GetTargetSavingsRequestType';
  static ADD_TARGET_REQUEST_TYPE_URL = Appsettings.BASE_UTILITY_URL + 'CreateTargetSavingsRequestType';
  static UPDATE_TARGET_REQUEST_TYPE_URL = Appsettings.BASE_UTILITY_URL + 'UpdateTargetSavingsRequestType';
  static DELETE_TARGET_REQUEST_TYPE_URL = Appsettings.BASE_UTILITY_URL + 'RemoveTargetSavingsRequestType';



  static DASHBOARD_ROUTER_URL = 'Dashboard';
  static PROSPECTS_ROUTER_URL = 'Customer/prospects';
  static REGISTERED_CUSTOMERS_ROUTER_URL = 'Customer/registered';
  static CUSTOMERS_GOALS_ROUTER_URL = 'Customer/goals';
  static CUSTOMERS_REQUESTS_ROUTER_URL = 'Customer/requests';
  static CUSTOMERS_TRANSACTIONS_ROUTER_URL = 'Customer/transactions';

  static SETTINGS_TARGET_CATEGORY_ROUTER_URL = 'Settings/target-category'
  static SETTINGS_TARGET_FREQUENCY_ROUTER_URL = 'Settings/target-frequency'
  static SETTINGS_TARGET_REQUEST_TYPE_ROUTER_URL = 'Settings/target-request-type'



  static GET_PROSPECTS_URL = Appsettings.API_ENDPOINT + "UserProfileManagement/AuthenticateRIBUser";




  // static DOWNLOAD_TYPES = [{ Name: 'CSV', Id: 1 }, { Name: 'PDF', Id: 2 }]
  // static SCHEDUE_PAYMENT_FREQUENCY_TYPES = [{ Name: 'Just once', Id: 1 }, { Name: 'Daily', Id: 2 },
  // { Name: 'Weekly', Id: 3 }, { Name: 'Monthly', Id: 4 }, { Name: 'Quarterly', Id: 5 }, , { Name: 'Yearly', Id: 6 }]


}

