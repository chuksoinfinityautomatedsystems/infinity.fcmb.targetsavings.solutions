

export class ResponseCodes {
    static SUCCESS = "00";
    static TechnicalError = "99";
    static NoError = "XX";
}