

import { ChangeDetectorRef } from "@angular/core";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ResponseCodes } from "../../commons/constants/response_codes.constant";
import { Appsettings } from "../../commons/constants/appsettings.constant";
import { EventHandlerService } from "../../commons/services/event_handlers.service";
import { AuthenticationService } from "../../authentication/services/authentication.service";
import { UtilService } from "../../commons/services/utils.services";




function _window(): any {
    return window;
}
declare var $: any;

export class BaseComponent {
    requestOngoing: boolean;

    sessionTimeoutInterval: any;
    sessionTimeoutSub: any;
    showSessionTimeoutPopup: boolean;
    timeoutSecondLabel: number;

    _authService: AuthenticationService;
    _router: Router;
    _cdRef: ChangeDetectorRef;
    _formBuilder: FormBuilder;







    constructor(_authService: AuthenticationService, _router: Router, _formBuilder?: FormBuilder, _cdRef?: ChangeDetectorRef) {
        this.requestOngoing = false;
        this._authService = _authService;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this._cdRef = _cdRef;
    }




    logout() {
        this._authService.logout();
        this._router.navigate([Appsettings.LOGIN_ROUTER_URL]);
    }


    // SetupTimeout() {
    //     let self = this;
    //     self.sessionTimeoutSub = EventHandlerService.SessionTimeoutEventPublisher
    //         .subscribe((object) => {
    //             self.showSessionTimeoutPopup = true; 

    //             self.timeoutSecondLabel = Appsettings.SESSION_LOGOUT_COUNTER;

    //             self.sessionTimeoutInterval = setInterval(function () { 

    //                 self.timeoutSecondLabel -= 1;

    //                 // console.log(999);    


    //             if (self.timeoutSecondLabel < 1) {
    //                 if (self.sessionTimeoutInterval) {
    //                     _window().clearInterval(self.sessionTimeoutInterval);
    //                 }

    //                 self._authService.logout();

    //                 if (self.sessionTimeoutSub) {
    //                     self.sessionTimeoutSub.unsubscribe();
    //                 }

    //                // console.log(888);

    //                 AuthenticationService.authLogoutMessage = Appsettings.TIMEOUT_LOGOUT_MESSAGE;

    //                 self._router.navigate(['login']);
    //             }
    //             }, 1000);
    //         });
    // }






}