import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AuthenticationPagesModule } from './authentication/authentication.module';
import { authenticationRoutes } from './authentication/authentication.routes';

import { AppComponent } from './app.component';

//  Routes
import { rootRoutes } from './app.routing';

import { StaticPagesModule } from './static/static.module'
import { staticPageRoutes } from './static/static.routes'


import { DashboardModule } from './dashboard/dashboard.module'
import { dashboardRoutes } from './dashboard/dashboard.routes'

import { CustomerModule } from './customers/customer.module'
import { customerRoutes } from './customers/customer.routes'



import { EventHandlerService } from "./commons/services/event_handlers.service";
import { settingsRoutes } from './settings/settings.routes';
import { SettingsModule } from './settings/settings.module';

@NgModule({
  imports: [BrowserModule, FormsModule, ReactiveFormsModule, HttpModule, RouterModule,
    AuthenticationPagesModule, DashboardModule, dashboardRoutes,
    CustomerModule, customerRoutes, StaticPagesModule,
    authenticationRoutes, staticPageRoutes, SettingsModule, settingsRoutes,

    rootRoutes],

  declarations: [AppComponent],
  providers: [EventHandlerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
