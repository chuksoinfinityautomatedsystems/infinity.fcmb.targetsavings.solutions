﻿using Infinity.FCMB.EntityFramework.Extensions.Repository;
using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Data;
using System;


namespace Infinity.FCMB.TargetSavings.Services
{
    internal class TargetSavingsRepositoryBase<TEntity> : Repository<TEntity> where TEntity : class
    {
        public TargetSavingsRepositoryBase(IUnitOfWork unitOfWork) : base(unitOfWork as TargetSavingsDBContext)
        {
        }
    }
}
