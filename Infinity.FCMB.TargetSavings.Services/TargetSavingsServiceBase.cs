﻿using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Data;
using System;

namespace Infinity.FCMB.TargetSavings.Services
{
    public abstract class TargetSavingsServiceBase
    {
        protected IUnitOfWork TargetSavingsUnitOfWorkUnitOfWork;

        protected TargetSavingsServiceBase(IUnitOfWork targetSavingsUnitOfWork = null)
        {
            if (targetSavingsUnitOfWork == null)
            {
                targetSavingsUnitOfWork = new TargetSavingsDBContext();
            }
            this.TargetSavingsUnitOfWorkUnitOfWork = targetSavingsUnitOfWork;
        }

        protected virtual void Dispose(bool disposing)
        {
            TargetSavingsUnitOfWorkUnitOfWork?.Dispose();
        }
    }
}
