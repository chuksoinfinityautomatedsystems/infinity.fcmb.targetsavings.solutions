﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Services.Interfaces
{
    public interface IApplicationLogService
    {
        bool Add(ApplicationLog applicationlog);
        bool Add(ActionReturn result);
        bool Add(string className, string methodName, Exception exception);
        bool AddEmailToLog(EmailLog emailLog, Dictionary<string, string> values);
        void AddToFile(string message, string log_file, string logDirectoryPath);
        void LogToFile(object log, string logDirectory, string fileName);
        bool UpdateEmailLog(EmailLog emailLog);
        bool SendPendingEmails();
        ActionReturn AddServiceLog(ServiceRequestLog serviceLog);
        bool AddServiceLog(ServiceRequestLog serviceLog, string exception);
    }
}
