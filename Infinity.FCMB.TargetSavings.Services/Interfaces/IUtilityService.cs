﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Services.Interfaces
{
    public interface IUtilityService
    {
        ActionReturn AddCategory(Category category);
        ActionReturn UpdateCategory(Category category);
        ActionReturn RemoveCategory(int id);
        Task<ActionReturn> GetCategory();

        ActionReturn AddTargetResultNote(TargetResultNote targetResultNote);
        Task<ActionReturn> GetTargetResultNote();
        ActionReturn RemoveTargetResultNote(int TargetResultNoteId);
        ActionReturn UpdateTargetResultNote(TargetResultNote targetResultNote);

        ActionReturn AddTargetSavingsFrequency(TargetSavingsFrequency targetSavingsFrequency);
        Task<ActionReturn> GetTargetSavingsFrequency();
        ActionReturn GetTargetSavingsFrequencySync();
        ActionReturn RemoveTargetSavingsFrequency(int TargetSavingsFrequencyId);
        ActionReturn UpdateTargetSavingsFrequency(TargetSavingsFrequency targetSavingsFrequency);

        ActionReturn AddTargetSavingsRequestType(TargetSavingsRequestType targetSavingsRequestType);
        Task<ActionReturn> GetTargetSavingsRequestType();
        ActionReturn RemoveTargetSavingsRequestType(int TargetSavingsRequestTypeId);
        ActionReturn UpdateTargetSavingsRequestType(TargetSavingsRequestType targetSavingsRequestType);
        Task<ActionReturn> GetInternalWebPaymentKey();
        Task<ActionReturn> GetWebPaymentKey();
        ActionReturn UpdateWebPaymentKey(TargetWebPaymentProfile targetWebPaymentProfile);
        ActionReturn GetAppConfigSettings();
        string GetHash(string hashString);
        ActionReturn SetupCustomerPendingMandates();
        Task<ActionReturn> GetBanks();
        Task<ActionReturn> GetBanksFromFCMB();
        ActionReturn ValidateAdminUser(string username, string password);
        ActionReturn ChargeCustomerCard(DateTime dateTime);
        ActionReturn ValidateMandateOTP(long targetGoalId, string remitaTransRef, string otp, string cardLast4Digit);
        ActionReturn RequestMandateValidationOTP(long targetGoalId);

        Task<ActionReturn> GetLiquidationAccountsByCustomerAsync(long customerId);
    }
}
