﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.FCMB;
using Infinity.FCMB.TargetSavings.Domain.Models.Remita;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Services.Interfaces
{
    public interface ITransactionService
    {
        ActionReturn AddTransaction(Transaction transaction);
        Task<ActionReturn> GetNTransactionByCustomerProfileId(Pagination pagination, long customerId);
        Task<ActionReturn> GetNTransactionByTargetId(Pagination pagination, long id);
        Task<ActionReturn> GetNTransactions(Pagination pagination);
        Task<ActionReturn> GetNPendingTransactions(Pagination pagination);
        ActionReturn ProcessScheduledTransactions(DateTime dateTime, string logPath);
        ActionReturn PostTransactionNotification(StandingOrderNotification standingOrderNotification);
        ActionReturn PrepareTargetGoalTransactionSchedule(DateTime dateTime);
        ActionReturn DirectCreditTest(DirectCreditRequest request);
        ActionReturn ProcessLiquidationRequests();
        ActionReturn ProcessLiquidation();
        ActionReturn ProcessDailyTargetSavingsInterest(DateTime today);
        ActionReturn ProcessMonthlyTargetSavingsInterest(DateTime runDate);
        ActionReturn ProcessCardPaymentSweep(DateTime runDate, string debitType);
        ActionReturn ProcessSweepForCardFees(DateTime runDate);
        ActionReturn ProcessLiquidationInterestSweep();
        Task<ActionReturn> GetNFailedTransactionSchedule(Pagination pagination);

    }
}
