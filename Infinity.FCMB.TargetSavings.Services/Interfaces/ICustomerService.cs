﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Services.Interfaces
{
    public interface ICustomerService
    {
        ActionReturn AddProspectiveCustomer(ProspectiveCustomer prospectiveCustomer);

        Task<ActionReturn> GetAllProspectiveCustomerAsync();

        ActionReturn GetAllProspectiveCustomer();
        Task<ActionReturn> GetNProspectiveCustomersAsync(Pagination pagination);

        Task<ActionReturn> GetProspectiveCustomerById(long prospectiveId);
        Task<ActionReturn> GetProspectsByStatus(string status);
        Task<ActionReturn> GetDailyProspects(DateTime dateTime, string status);
        Task<ActionReturn> GetProspectsCount();
        
        ActionReturn AddCustomerProfile(CustomerProfile customerProfile);

        Task<ActionReturn> GetAllCustomerProfileAsync();
        Task<ActionReturn> GetNCustomerProfileAsync(Pagination pagination);
        Task<ActionReturn> GetCustomerProfileById(long profileId);
        ActionReturn UpdateCustomerProfile(CustomerProfile customerProfile);
        Task<ActionReturn> GetDailyCustomerProfiles(DateTime dateTime, string status);
        Task<ActionReturn> GetCustomerProfilesCount();
        ActionReturn GetCustomerRegistrationStatus(string email, string phone);

        ActionReturn AddTargetGoalWithCard(TargetGoal targetGoal);
        ActionReturn AddTargetGoalWithAccountWithoutOtp(TargetGoal targetGoal);

        ActionReturn AddTargetGoalWithAccount(TargetGoal targetGoal);

        ActionReturn ValidateOTP(string accountNo, string otp, long customerProfileId, string referenceId);
        ActionReturn SelfTopUp(long goalId, string cardToken, decimal amount, bool isCardTransaction);

        Task<ActionReturn> GetAllTargetGoalAsync();
        Task<ActionReturn> GetNTargetGoalAsync(Pagination pagination);
        Task<ActionReturn> GetAllNTargetGoalAsync(Pagination pagination);
        Task<ActionReturn> GetAllTargetGoalsByCustomerAsync(long customerId);
        Task<ActionReturn> GetTargetGoalsByCustomerAsync(long customerId);
        Task<ActionReturn> GetCompletedTargetGoalsByCustomerAsync(long customerId);

        Task<ActionReturn> GetTargetGoalById(long targetId);
        ActionReturn GetTargetGoalByIdSync(long targetId);
        Task<ActionReturn> GetDailyTargets(DateTime dateTime, string status);

        Task<ActionReturn> GetTargetsCount();
        Task<ActionReturn> GetTotalTargetGoalAndBalanceByCustomer(long customerId);
        Task<ActionReturn> GetCompletedTotalTargetGoalAndBalanceByCustomer(long customerId);
        ActionReturn GetTargetGoalForTransactionProcessing(DateTime dateTime);
        ActionReturn UpdateTargetPicture(TargetGoal targetGoal);
        ActionReturn UpdateLiquidationDetails(long targetId, string account, string bankCode);
        ActionReturn AddTargetSavingsRequest(TargetSavingsRequest targetSavingsRequest);

        Task<ActionReturn> GetAllTargetSavingsRequestAsync();

        Task<ActionReturn> GetTargetSavingsRequestByTargetGoalAsync(long targetId);

        Task<ActionReturn> GetTargetSavingsRequestById(long requestId);
        Task<ActionReturn> GetNCustomerRequests(Pagination pagination);
        Task<ActionReturn> GetNRequestsByCustomerProfileId(Pagination pagination, long customerId);
        ActionReturn TreatCustomerRequests(TargetSavingsRequest targetSavingsRequest);
        Task<ActionReturn> GetTargetSavingsRequestCountByStatus(string status);
        Task<ActionReturn> GetNRecentTargetSavingsRequest(Pagination pagination);
        Task<ActionReturn> GetNTreatedTargetSavingsRequest(Pagination pagination);
        Task<ActionReturn> GetNNewTargetSavingsRequest(Pagination pagination);
        ActionReturn ValidateUser(string username, string password);
        ActionReturn ValidateProspectData(string email, string phoneNumber);
        ActionReturn ValidateNewCustomerData(string email, string phoneNumber, string username);
        ActionReturn ResetPassword(string username);
        ActionReturn ChangePassword(long customerProfileId, string oldPassword, string newPassword);
        Task<ActionReturn> GetDailyStatistics(DateTime dateTime);
        ActionReturn GetCompletedGoals();

        ActionReturn ValidateBVN(string bvn, string firstName, string lastName, string phone);
        ActionReturn NameEnquiry(string accountNumber, string bankCode);
        ActionReturn GetCustomerInformationByAccountNo(string accountNo);

        ActionReturn AddCompletedLiquidationRequest(string requestType, long targetGoalId);
        ActionReturn AddNonCompletedLiquidationRequest(string requestType, long targetGoalId, decimal amount);
        Task<ActionReturn> GetNLiquidationRequestsByCustomerProfileId(Pagination pagination, long customerId);
        Task<ActionReturn> UpdateTargetGoalExtentedStatus(long targetGoalId, string status);
    }
}
