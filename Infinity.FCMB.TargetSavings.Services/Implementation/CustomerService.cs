﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.EntityFramework.Extensions.Repository;
using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Transactions;
using System.Threading.Tasks;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System.Text;
using Infinity.FCMB.TargetSavings.Services.Helper;
using Infinity.FCMB.TargetSavings.Domain.Models.DTO;
using Infinity.FCMB.TargetSavings.Domain.Models.Remita;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using System.Net;
using Infinity.FCMB.TargetSavings.Domain.Models.FCMB;
using Newtonsoft.Json;
using Infinity.FCMB.TargetSavings.Domain.Models.Paystack;

namespace Infinity.FCMB.TargetSavings.Services.Implementation
{
    public class CustomerService : TargetSavingsServiceBase, ICustomerService
    {
        private IRepository<ProspectiveCustomer> prospectiveCustomerRepository
        {
            get;
            set;
        }

        private IRepository<CustomerProfile> customerProfileRepository
        {
            get;
            set;
        }

        private IRepository<TargetGoal> targetGoalRepository
        {
            get;
            set;
        }

        private IRepository<TargetSavingsRequest> targetSavingsRequestRepository
        {
            get;
            set;
        }
        private IRepository<TargetDebitAccountProfile> targetDebitAccontProfileRepository { get; set; }

        private IRepository<TargetSavingsFrequency> targetSavingsFrequencyRepository { get; set; }
        private IRepository<AppConfigSetting> appConfigSettingRepository { get; set; }
        private IRepository<TargetSavingsRequestType> targetSavingsRequestTypeRepository { get; set; }
        private IRepository<Domain.Models.Common.Transaction> transactionRepository
        {
            get;
            set;
        }

        private IRepository<AccountValidationLog> accountValidationLogRepository { get; set; }
        private IRepository<LiquidationTransaction> liquidationTransactionRepository { get; set; }
        private IRepository<LiquidationServiceRequest> liquidationServiceRequestRepository { get; set; }

        //private IUtilityService utilityService { get; set; }
        private IUnitOfWork unitOfWork
        {
            get;
            set;
        }

        public CustomerService(IUnitOfWork unitOfWork) : base(null)
        {
            unitOfWork.Database.CommandTimeout = new int?(20000);
            this.TargetSavingsUnitOfWorkUnitOfWork = unitOfWork;
            this.prospectiveCustomerRepository = new TargetSavingsRepositoryBase<ProspectiveCustomer>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.customerProfileRepository = new TargetSavingsRepositoryBase<CustomerProfile>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetGoalRepository = new TargetSavingsRepositoryBase<TargetGoal>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetSavingsRequestRepository = new TargetSavingsRepositoryBase<TargetSavingsRequest>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetDebitAccontProfileRepository = new TargetSavingsRepositoryBase<TargetDebitAccountProfile>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetSavingsFrequencyRepository = new TargetSavingsRepositoryBase<TargetSavingsFrequency>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.appConfigSettingRepository = new TargetSavingsRepositoryBase<AppConfigSetting>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetSavingsRequestTypeRepository = new TargetSavingsRepositoryBase<TargetSavingsRequestType>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.transactionRepository = new TargetSavingsRepositoryBase<Domain.Models.Common.Transaction>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.accountValidationLogRepository = new TargetSavingsRepositoryBase<AccountValidationLog>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.liquidationTransactionRepository = new TargetSavingsRepositoryBase<LiquidationTransaction>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.liquidationServiceRequestRepository = new TargetSavingsRepositoryBase<LiquidationServiceRequest>(this.TargetSavingsUnitOfWorkUnitOfWork);

        }
        #region Dispose
        protected override void Dispose(bool disposing)
        {
            prospectiveCustomerRepository?.Dispose();
            customerProfileRepository?.Dispose();
            targetGoalRepository?.Dispose();
            targetSavingsRequestRepository?.Dispose();
            targetSavingsRequestTypeRepository?.Dispose();
            liquidationTransactionRepository?.Dispose();
            liquidationServiceRequestRepository?.Dispose();
            base.Dispose(disposing);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public ActionReturn AddProspectiveCustomer(ProspectiveCustomer prospectiveCustomer)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                prospectiveCustomer.BiometricVerificationNo = "NA";
                prospectiveCustomer.Status = Enumerators.STATUS.NEW.ToString();
                prospectiveCustomer.DateCreated = new DateTime?(DateTime.Now.Date);
                this.prospectiveCustomerRepository.Add(prospectiveCustomer);
                long Id = this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.ReturnedObject = prospectiveCustomer.ProspectId;
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Prospective customer created";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Prospective customer was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddProspectiveCustomer(ProspectiveCustomer prospectiveCustomer)";
            }
            return result;
        }

        public async Task<ActionReturn> GetAllProspectiveCustomerAsync()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                var prospectiveCustomers = prospectiveCustomerRepository.GetAll().OrderByDescending(o=>o.ProspectId);
                result.ReturnedObject = prospectiveCustomers;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Prospective customers were retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Prospective customers could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetAllProspectiveCustomerAsync()";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetNProspectiveCustomersAsync(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                var prospectiveCustomers = prospectiveCustomerRepository.All.OrderByDescending(o => o.ProspectId).AsNoTracking();

                var totalCount = prospectiveCustomers.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    prospectiveCustomers = prospectiveCustomers.OrderByDescending(o => o.ProspectId).Where(x => x.ProspectId < pagination.lastIdFetched);
                }
                else
                {
                    prospectiveCustomers = prospectiveCustomers.OrderByDescending(o => o.ProspectId);
                }

               var items = await prospectiveCustomers.OrderByDescending(o => o.ProspectId)
                                       .Take(pagination.pageSize)                                       
                                       .ToListAsync();

                var pagedResult = new Paged<ProspectiveCustomer>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Prospective customers were retireived successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Prospective customers could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNProspectiveCustomersAsync(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn GetAllProspectiveCustomer()
        {
            throw new NotImplementedException();
        }

        public async Task<ActionReturn> GetProspectiveCustomerById(long prospectiveId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                IEnumerable<ProspectiveCustomer> prospectiveCustomers = prospectiveCustomerRepository.GetAll();
                result.ReturnedObject = prospectiveCustomers.Where(o=>o.ProspectId==prospectiveId).FirstOrDefault();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Prospective customer retrieved";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Prospective customer could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetProspectiveCustomerById(long prospectiveId)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetProspectsByStatus(string status)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                IEnumerable<ProspectiveCustomer> prospectiveCustomers = prospectiveCustomerRepository.GetAll();
                result.ReturnedObject = prospectiveCustomers.Where(o => o.Status == status).FirstOrDefault();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Prospective customers was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Prospective customer could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetProspectsByStatus(string status)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetDailyProspects(DateTime dateTime, string status)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                IEnumerable<ProspectiveCustomer> prospectiveCustomers = prospectiveCustomerRepository.GetAll();
                result.ReturnedObject = prospectiveCustomers.Where(o => o.Status == status && o.DateCreated==dateTime).FirstOrDefault();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Prospective customers retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Prospective customers could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetDailyProspects(DateTime dateTime, string status)";
            }
            return await Task.FromResult(result);
        }
        public async Task<ActionReturn> GetProspectsCount()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = prospectiveCustomerRepository.GetAll().Count();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Prospective customer's count was successful";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Prospective customer's count was not successful";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetProspectsCount()";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn AddCustomerProfile(CustomerProfile customerProfile)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                customerProfile.DateCreated = DateTime.Now.Date;
                customerProfile.HashValue = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, customerProfile);
                this.customerProfileRepository.Add(customerProfile);
                //remove user from prospect
                //this.prospectiveCustomerRepository.Remove(prospectiveCustomer);
                ProspectiveCustomer prospectiveCustomer = prospectiveCustomerRepository.Get(customerProfile.ProspectId);
                if(prospectiveCustomer != null)
                {
                    using (System.Transactions.TransactionScope scope = new TransactionScope())
                    {
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        scope.Complete();
                    }
                    result.ReturnedObject = customerProfile.CustomerProfileId;
                    result.UpdateReturn = UpdateReturn.ItemCreated;
                    result.Message = "Customer was created successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.Message = "Invalid prospect profile provided";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "An error occurred, customer was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddCustomerProfile(CustomerProfile customerProfile)";
            }
            return result;
        }

        public async Task<ActionReturn> GetAllCustomerProfileAsync()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                List<CustomerProfile> customerProfileList = new List<CustomerProfile>();
                IEnumerable<CustomerProfile> customerProfile = customerProfileRepository.GetAll().OrderByDescending(o => o.CustomerProfileId);
                foreach (CustomerProfile customer in customerProfile)
                {
                    customer.Password = null;
                    string hash = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, customer);
                    if (hash == customer.HashValue)
                    {
                        customerProfileList.Add(customer);
                    }
                }
                result.ReturnedObject = customerProfileList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Customer list was retrieve successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Customer list retrieval failed";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetAllCustomerProfileAsync()";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetNCustomerProfileAsync(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                List<CustomerProfile> customerProfiles = new List<CustomerProfile>();
                IEnumerable<CustomerProfile> activeCustomerProfiles = customerProfileRepository.All.OrderByDescending(o => o.CustomerProfileId).AsNoTracking();
                foreach(CustomerProfile customer in activeCustomerProfiles)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, customer);
                    if (hash == customer.HashValue)
                    {
                        customer.Password = null;
                        customerProfiles.Add(customer);
                    }
                }
                IEnumerable<CustomerProfile> customerProfilesList = customerProfiles;
                var totalCount = customerProfilesList.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    customerProfilesList = customerProfilesList.OrderByDescending(o => o.CustomerProfileId).Where(x => x.CustomerProfileId < pagination.lastIdFetched);
                }
                else
                {
                    customerProfilesList = customerProfilesList.OrderByDescending(o => o.CustomerProfileId);
                }

                List<CustomerProfile> items = customerProfilesList.OrderByDescending(o => o.CustomerProfileId)
                                        .Take(pagination.pageSize)
                                        
                                        .ToList();
                
                var pagedResult = new Paged<CustomerProfile>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Customer list was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Customer list could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNCustomerProfileAsync(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetCustomerProfileById(long profileId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                CustomerProfile customerProfile = customerProfileRepository.GetAll().Where(o => o.CustomerProfileId == profileId).FirstOrDefault();
                customerProfile.Password = null;
                string hash = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, customerProfile);
                if (hash == customerProfile.HashValue)
                {
                    result.ReturnedObject = customerProfile;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Customer retrieved successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "Customer could not be be retrieved, invalid user profile";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Customer could not be be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetCustomerProfileById(long profileId)";
            }
            return await Task.FromResult(result);
        }
        public ActionReturn UpdateCustomerProfile(CustomerProfile customerProfile)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                CustomerProfile thisCustomerProfile = customerProfileRepository.Find(o=>o.CustomerProfileId== customerProfile.CustomerProfileId).FirstOrDefault();
                if (thisCustomerProfile != null)
                {
                    thisCustomerProfile.Address = customerProfile.Address;
                    thisCustomerProfile.ComputerDetails = customerProfile.ComputerDetails;
                    thisCustomerProfile.EmailAddress = customerProfile.EmailAddress;
                    thisCustomerProfile.FirstName = customerProfile.FirstName;
                    thisCustomerProfile.LastPasswordChanged = customerProfile.LastPasswordChanged;
                    thisCustomerProfile.NextOfKinEmail = customerProfile.NextOfKinEmail;
                    thisCustomerProfile.NextOfKinFullName = customerProfile.NextOfKinFullName;
                    thisCustomerProfile.NextOfKinPhone = customerProfile.NextOfKinPhone;
                    thisCustomerProfile.PhoneNumber = customerProfile.PhoneNumber;
                    thisCustomerProfile.ProfilePicture = customerProfile.ProfilePicture;
                    thisCustomerProfile.ProfileStatus = customerProfile.ProfileStatus;
                    thisCustomerProfile.Surname = customerProfile.Surname;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Customer was updated";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                    result.Message = "Customer was not updated";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Customer was not updated";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "UpdateCustomerProfile(CustomerProfile customerProfile)";
            }
            return result;
        }
        public ActionReturn UpdateCustomerProfileLastLogin(CustomerProfile customerProfile)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                CustomerProfile thisCustomerProfile = customerProfileRepository.Find(o => o.CustomerProfileId == customerProfile.CustomerProfileId).FirstOrDefault();
                if (thisCustomerProfile != null)
                {
                    thisCustomerProfile.LastLoginDate = customerProfile.LastLoginDate;                   
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Customer was updated";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                    result.Message = "Customer was not updated";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Customer was not updated";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "UpdateCustomerProfile(CustomerProfile customerProfile)";
            }
            return result;
        }

        public async Task<ActionReturn> GetDailyCustomerProfiles(DateTime dateTime,string status)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                List<CustomerProfile> customerProfileList = new List<CustomerProfile>();
                IEnumerable<CustomerProfile> customerProfile = customerProfileRepository.GetAll().Where(o => o.DateCreated == dateTime && o.ProfileStatus == status).ToList();
                foreach (CustomerProfile customer in customerProfile)
                {
                    customer.Password = null;
                    string hash = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, customer);
                    if (hash == customer.HashValue)
                    {
                        customerProfileList.Add(customer);
                    }
                }
                result.ReturnedObject = customerProfileList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Customer list was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Customer list retrieval failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetDailyCustomerProfiles(DateTime dateTime,string status)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetCustomerProfilesCount()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = customerProfileRepository.All.Count();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Customer count was successful";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Customer count failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetCustomerProfilesCount()";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn GetCustomerRegistrationStatus(string email,string phone)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                CustomerRegistrationStatusDTO customerRegistration = new CustomerRegistrationStatusDTO();
                ProspectiveCustomer prospectiveCustomer = prospectiveCustomerRepository.Find(o => o.PhoneNumber == phone || o.EmailAddress == email).FirstOrDefault();
                if(prospectiveCustomer != null)
                {
                    CustomerProfile customerProfile = customerProfileRepository.Find(o =>o.PhoneNumber == phone && o.ProspectId == prospectiveCustomer.ProspectId).FirstOrDefault();
                    if(customerProfile != null)
                    {
                        string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                        //IEnumerable<TargetGoal> targetGoals = targetGoalRepository.GetAll().Where(o => o.GoalStatus == goalStatus && o.CustomerProfileId == customerProfile.CustomerProfileId).OrderByDescending(o => o.TargetGoalsId);
                        IEnumerable<TargetGoal> targetGoals = customerProfile.TargetGoals.Where(o => o.GoalStatus == goalStatus && o.CustomerProfileId == customerProfile.CustomerProfileId).OrderByDescending(o => o.TargetGoalsId);
                        List<TargetGoal> targetGoalList = new List<TargetGoal>();
                        foreach (TargetGoal goal in targetGoals)
                        {
                            string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                            if (hash == goal.HashValue)
                            {
                                targetGoalList.Add(goal);
                            }
                        }
                        {
                            customerRegistration.HasCustomerProfileId = true;
                            customerRegistration.Profile = customerProfile;                            
                            customerRegistration.Targets = targetGoalList;
                            customerRegistration.HasGoals = targetGoalList.Count() > 0;
                            customerProfile.TargetGoals = null;
                        };
                    }
                    else
                    {
                        //prospective customer
                        customerRegistration.HasCustomerProfileId = false;
                        
                    }
                    customerRegistration.HasProspectId = true;
                    customerRegistration.Prospect = prospectiveCustomer;
                }
                else
                {
                    //new customer
                    customerRegistration.HasCustomerProfileId = false;
                    customerRegistration.HasProspectId = false;
                }
                result.ReturnedObject = customerRegistration;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Customer status check was successful";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch(Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Customer registration validation failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetCustomerRegistrationStatus(string email,string phone)";
            }
            return result;
        }

        

        public ActionReturn GetAppConfigSettings()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = appConfigSettingRepository.GetAll().Where(o => o.IsActive == true).ToList();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Config retreived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Config could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetAppConfigSettings()";
            }
            return result;
        }

        public ActionReturn AddTargetGoalWithCard(TargetGoal targetGoal)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.HasEmail = true;
                result.ReturnedObject = "";
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();

                //ActionReturn frequency = utilityService.GetTargetSavingsFrequencySync();
                CustomerProfile customerProfile = customerProfileRepository.Get(targetGoal.CustomerProfileId);
                if (customerProfile != null)
                {
                    TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //send email to customer
                        EmailLog emailLog = new EmailLog
                        {
                            EmailTo = customerProfile.EmailAddress,
                            EmailType = "FIRST_EMAIL",
                            Status = Enumerators.STATUS.NEW.ToString(),
                        };
                        result.EmailLog = emailLog;

                        Dictionary<string, string> emailValues = new Dictionary<string, string>();
                        emailValues.Add("NAME", $"{customerProfile.Surname} {customerProfile.FirstName}");
                        emailValues.Add("AMOUNT", targetGoal.TargetFrequencyAmount.ToString("N"));
                        emailValues.Add("STARTDATE", Convert.ToDateTime(targetGoal.GoalStartDate).ToString("MMMM dd, yyyy"));
                        emailValues.Add("ENDDATE", Convert.ToDateTime(targetGoal.GoalEndDate).ToString("MMMM dd, yyyy"));
                        emailValues.Add("FREQUENCY", frequency.Frequency);
                        emailValues.Add("PLAN", targetGoal.GoalName);
                        result.EmailValues = emailValues;

                        targetGoal.SetUpDate = DateTime.Now.Date;
                        targetGoal.NextRunDate = Util.GetNextRunDate(DateTime.Now.Date, frequency.ValueInDays);
                        targetGoal.GoalStatus = Enumerators.STATUS.NEW.ToString();


                        ActionReturn configs = this.GetAppConfigSettings();
                        if (configs.Flag)
                        {
                            List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                            TargetDebitAccountProfile tartgetDebitAccontProfile = new TargetDebitAccountProfile
                            {
                                DateCreated = DateTime.Now,
                                HashValue = "",
                                IsActive = true,
                                LastModifiedDate = DateTime.Now,
                                Status = Enumerators.STATUS.NEW.ToString(),
                                MandateId = ""
                            };

                            if (targetGoal.GoalDebitType == CONSTANT.CARD_DEBIT_TYPE)
                            {
                                //payment with card
                                //confirm reference 
                                TargetGoal existingGoalToken = this.targetGoalRepository.Find(o => o.GoalCardTokenDetails == targetGoal.GoalCardTokenDetails).FirstOrDefault();
                                if (existingGoalToken == null)
                                {
                                    string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                                    string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                                    WebHeaderCollection header = new WebHeaderCollection();
                                    header.Add("Authorization", $"Bearer {SECRET_KEY}");
                                    //verify transaction
                                    VERIFY_URL = $"{VERIFY_URL}/{targetGoal.GoalCardTokenDetails}";
                                    ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                                    if (serverVerifyResult.Flag)
                                    {
                                        ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                        if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                        {
                                            //targetGoal.GoalCardTokenDetails = jsonVerifyResponse.data.authorization.authorization_code;
                                            targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                            targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                            this.targetGoalRepository.Add(targetGoal);
                                            result.ReturnedObject = targetGoal.TargetGoalsId;
                                            result.UpdateReturn = UpdateReturn.ItemCreated;
                                            result.Message = "Goal created successfully";
                                            result.Flag = true;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                                            //add transaction object.
                                            Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                            {
                                                Amount =  100,
                                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                ApprovedDate = DateTime.Now,
                                                Narration = $"TSA Goal top up for {targetGoal.GoalName} (Reference:{jsonVerifyResponse.data.reference})",
                                                PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                PostedDate = DateTime.Now,
                                                TargetGoalsId = targetGoal.TargetGoalsId,
                                                TransactionDate = DateTime.Now,
                                                TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()

                                            };
                                            transaction.ChargedFee = Util.ComputeCardFee(100, settings);
                                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);

                                            this.transactionRepository.Add(transaction);

                                            targetGoal.GoalBalance = targetGoal.GoalBalance - 100;

                                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                            scope.Complete();
                                        }
                                        else
                                        {
                                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                            result.Message = "Goal was not created, payment could not be verified.";
                                            result.Flag = false;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                            result.ReturnedObject = "";
                                        }

                                    }
                                    else
                                    {
                                        result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                        result.Message = "Goal was not created, error vadidating payment as token may be invalid.";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                        result.ReturnedObject = "";
                                    }
                                }
                                else
                                {
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, error vadidating payment as token already exist.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = "";
                                }


                            }
                            else
                            {
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, invalid bank or token detail.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";

                            }
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                            result.Message = "Goal was not created, kindly contact your support person";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = "";
                        }


                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                    result.Message = "Goal was not created, customer does not exist.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = "";

                }


            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();

            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddTargetGoalWithCard(TargetGoal targetGoal)";
            }
            return result;
        }
        public ActionReturn AddTargetGoalWithCard_V2(TargetGoal targetGoal)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.HasEmail = true;
                result.ReturnedObject = "";
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();

                //ActionReturn frequency = utilityService.GetTargetSavingsFrequencySync();
                CustomerProfile customerProfile = customerProfileRepository.Get(targetGoal.CustomerProfileId);
                if(customerProfile != null)
                {
                    TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();
                    using (TransactionScope scope = new TransactionScope())
                    {

                        //send email to customer
                        EmailLog emailLog = new EmailLog
                        {
                            EmailTo = customerProfile.EmailAddress,
                            EmailType = "FIRST_EMAIL",
                            Status = Enumerators.STATUS.NEW.ToString(),
                        };
                        result.EmailLog = emailLog;

                        Dictionary<string, string> emailValues = new Dictionary<string, string>();
                        emailValues.Add("NAME", $"{customerProfile.Surname} {customerProfile.FirstName}");
                        emailValues.Add("AMOUNT", targetGoal.TargetFrequencyAmount.ToString("N"));
                        emailValues.Add("STARTDATE", Convert.ToDateTime(targetGoal.GoalStartDate).ToString("MMMM dd, yyyy"));
                        emailValues.Add("ENDDATE", Convert.ToDateTime(targetGoal.GoalEndDate).ToString("MMMM dd, yyyy"));
                        emailValues.Add("FREQUENCY", frequency.Frequency);
                        emailValues.Add("PLAN", targetGoal.GoalName);
                        result.EmailValues = emailValues;

                        targetGoal.SetUpDate = DateTime.Now.Date;
                        targetGoal.NextRunDate = Util.GetNextRunDate(DateTime.Now.Date, frequency.ValueInDays);
                        targetGoal.GoalStatus = Enumerators.STATUS.NEW.ToString();


                        ActionReturn configs = this.GetAppConfigSettings();
                        if (configs.Flag)
                        {
                            List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                            TargetDebitAccountProfile tartgetDebitAccontProfile = new TargetDebitAccountProfile
                            {
                                DateCreated = DateTime.Now,
                                HashValue = "",
                                IsActive = true,
                                LastModifiedDate = DateTime.Now,
                                Status = Enumerators.STATUS.NEW.ToString(),
                                MandateId = ""
                            };

                            if (targetGoal.GoalDebitType == CONSTANT.CARD_DEBIT_TYPE)
                            {
                                //payment with card
                                //confirm reference 
                                TargetGoal existingGoalToken = this.targetGoalRepository.Find(o => o.GoalCardTokenDetails == targetGoal.GoalCardTokenDetails).FirstOrDefault();
                                if (existingGoalToken == null)
                                {
                                    string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                                    string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                                    WebHeaderCollection header = new WebHeaderCollection();
                                    header.Add("Authorization", $"Bearer {SECRET_KEY}");
                                    //verify transaction
                                    VERIFY_URL = $"{VERIFY_URL}/{targetGoal.GoalCardTokenDetails}";
                                    ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                                    if (serverVerifyResult.Flag)
                                    {
                                        ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                        if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                        {
                                            //targetGoal.GoalCardTokenDetails = jsonVerifyResponse.data.authorization.authorization_code;
                                            targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                            targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                            this.targetGoalRepository.Add(targetGoal);
                                            result.ReturnedObject = targetGoal.TargetGoalsId;
                                            result.UpdateReturn = UpdateReturn.ItemCreated;
                                            result.Message = "Goal created successfully";
                                            result.Flag = true;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                                            //add transaction object.
                                            Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                            {
                                                Amount = targetGoal.GoalStartDate==DateTime.Now.Date? targetGoal.TargetFrequencyAmount: 100,
                                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                ApprovedDate = DateTime.Now,
                                                Narration = $"TSA Goal top up for {targetGoal.GoalName} (Reference:{jsonVerifyResponse.data.reference})",
                                                PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                PostedDate = DateTime.Now,
                                                TargetGoalsId = targetGoal.TargetGoalsId,
                                                TransactionDate = DateTime.Now,
                                                TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                                                
                                            };
                                            transaction.ChargedFee = Util.ComputeCardFee(100, settings);
                                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);

                                            this.transactionRepository.Add(transaction);

                                            targetGoal.GoalBalance = targetGoal.GoalBalance - 100;

                                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                            scope.Complete();
                                        }
                                        else
                                        {
                                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                            result.Message = "Goal was not created, payment could not be verified.";
                                            result.Flag = false;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                            result.ReturnedObject = "";
                                        }

                                    }
                                    else
                                    {
                                        result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                        result.Message = "Goal was not created, error vadidating payment as token may be invalid.";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                        result.ReturnedObject = "";
                                    }
                                }
                                else
                                {
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, error vadidating payment as token already exist.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = "";
                                }


                            }
                            else
                            {
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, invalid bank or token detail.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";

                            }
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                            result.Message = "Goal was not created, kindly contact your support person";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = "";
                        }
                        

                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                    result.Message = "Goal was not created, customer does not exist.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = "";

                }
                
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
                
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddTargetGoalWithCard(TargetGoal targetGoal)";
            }
            return result;
        }

        public ActionReturn AddTargetGoalWithAccount(TargetGoal targetGoal)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                
                CustomerProfile customerProfile = customerProfileRepository.Get(targetGoal.CustomerProfileId);
                if(customerProfile != null)
                {
                    TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();
                    using (TransactionScope scope = new TransactionScope())
                    {

                        targetGoal.SetUpDate = DateTime.Now.Date;
                        targetGoal.NextRunDate = Util.GetNextRunDate(DateTime.Now.Date, frequency.ValueInDays);
                        targetGoal.GoalStatus = Enumerators.STATUS.NEW.ToString();
                        targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);

                        ActionReturn configs = this.GetAppConfigSettings();
                        if (configs.Flag)
                        {
                            List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;

                            string FCMBBankCode = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                            if (targetGoal.GoalDebitType == CONSTANT.BANK_DEBIT_TYPE & targetGoal.GoalDebitBank == FCMBBankCode)
                            {
                                ActionReturn customerEnquiryResult = FCMBIntegrator.GetCustomerInformationByAccountNo(targetGoal.GoalDebitAccountNo, settings);
                                if (customerEnquiryResult.Flag)
                                {
                                    CustomerAccountInformation customerAccountInformation = (CustomerAccountInformation)customerEnquiryResult.ReturnedObject;
                                    if (!string.IsNullOrEmpty(customerAccountInformation.PhoneNumber))
                                    {

                                        string SMSOtpTemplate = settings.Where(o => o.Key == "FCMB_SMS_OTP_TEMPLATE").Select(o => o.Value).FirstOrDefault();
                                        string FCMB_OTP_EXPIRY_TIME_IN_SEC = settings.Where(o => o.Key == "FCMB_OTP_EXPIRY_TIME_IN_SEC").Select(o => o.Value).FirstOrDefault();

                                        ActionReturn otpResult = Util.GenerateOtp();
                                        if (otpResult.Flag)
                                        {
                                            string otp = Convert.ToString(otpResult.ReturnedObject);
                                            string message = SMSOtpTemplate.Replace("#OTP#", otp);
                                            ActionReturn sendSMSResult = FCMBIntegrator.SendOTP(targetGoal.GoalDebitAccountNo, customerAccountInformation.PhoneNumber, message, settings);
                                            if (sendSMSResult.Flag)
                                            {
                                                DateTime now = DateTime.Now;
                                                long milliseconds = DateTime.Now.Ticks;
                                                string referenceId = milliseconds.ToString();
                                                string otpHash = $"{otp}|{FCMB_OTP_EXPIRY_TIME_IN_SEC}|{now.Year}|{now.Month}|{now.Day}|{now.Hour}|{now.Minute}|{now.Second}";
                                                targetGoal.GoalCardTokenDetails = Util.Encrypt(otpHash);
                                                AccountValidationLog accountValidationLog = new AccountValidationLog
                                                {
                                                    CreatedDate = now,
                                                    CustomerId = targetGoal.CustomerProfileId,
                                                    HashValue = Util.Encrypt(JsonConvert.SerializeObject(targetGoal)),
                                                    LastModifiedDate = now,
                                                    ReferenceId = referenceId,
                                                    TargetGoalId = 0
                                                };
                                                this.accountValidationLogRepository.Add(accountValidationLog);
                                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                                scope.Complete();
                                                AccountValidationResponse accountValidationResponse = new AccountValidationResponse
                                                {
                                                    ReferenceId = referenceId,
                                                    Details = "otp"
                                                };
                                                result.ReturnedObject = accountValidationResponse;
                                                result.UpdateReturn = UpdateReturn.ItemCreated;
                                                result.Message = "Goal created successfully, kindly validate your account.";
                                                result.Flag = true;
                                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                            }
                                            else
                                            {
                                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                                result.Message = "Goal was not created, could not retrieve account details for validation";
                                                result.Flag = false;
                                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                                result.ReturnedObject = sendSMSResult;
                                            }
                                        }
                                        else
                                        {
                                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                            result.Message = "Goal was not created, error generating account validation code";
                                            result.Flag = false;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                            result.ReturnedObject = otpResult;
                                        }


                                    }
                                    else
                                    {
                                        // invalid phone number
                                        result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                        result.Message = "Goal was not created, invalid phone number";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                        result.ReturnedObject = customerEnquiryResult;

                                    }

                                }
                                else
                                {
                                    //could not retrieve account details for validation
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, could not retrieve account details for validation";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = customerEnquiryResult;
                                }

                            }
                            else
                            {
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, invalid bank or token detail.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";

                            }
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                            result.Message = "Goal was not created, kindly contact your support person.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = "";
                        }
                        

                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                    result.Message = "Goal was not created, customer does not exist.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = "";

                }
                

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
               
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddTargetGoalWithAccount(TargetGoal targetGoal)";
            }
            return result;
        }

        public ActionReturn AddTargetGoalWithAccountWithoutOtp(TargetGoal targetGoal)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                CustomerProfile customerProfile = customerProfileRepository.Get(targetGoal.CustomerProfileId);
                if (customerProfile != null)
                {
                    TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();
                    targetGoal.SetUpDate = DateTime.Now.Date;
                    targetGoal.NextRunDate = Util.GetNextRunDate(DateTime.Now.Date, frequency.ValueInDays);
                    targetGoal.GoalStatus = Enumerators.STATUS.NEW.ToString();
                    targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);

                    ActionReturn configs = this.GetAppConfigSettings();
                    if (configs.Flag)
                    {
                        List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;

                        string FCMBBankCode = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                        if (targetGoal.GoalDebitType == CONSTANT.BANK_DEBIT_TYPE & targetGoal.GoalDebitBank == FCMBBankCode)
                        {
                            ActionReturn customerEnquiryResult = FCMBIntegrator.GetCustomerInformationByAccountNo(targetGoal.GoalDebitAccountNo, settings);
                            if (customerEnquiryResult.Flag)
                            {
                                CustomerAccountInformation customerAccountInformation = (CustomerAccountInformation)customerEnquiryResult.ReturnedObject;
                                if (!string.IsNullOrEmpty(customerAccountInformation.PhoneNumber))
                                {
                                    //TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();

                                    //send email to customer
                                    EmailLog emailLog = new EmailLog
                                    {
                                        EmailTo = customerProfile.EmailAddress,
                                        EmailType = "FIRST_EMAIL",
                                        Status = Enumerators.STATUS.NEW.ToString(),
                                    };
                                    result.EmailLog = emailLog;

                                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                                    emailValues.Add("NAME", $"{customerProfile.Surname} {customerProfile.FirstName}");
                                    emailValues.Add("AMOUNT", targetGoal.TargetFrequencyAmount.ToString("N"));
                                    emailValues.Add("STARTDATE", Convert.ToDateTime(targetGoal.GoalStartDate).ToString("MMMM dd, yyyy"));
                                    emailValues.Add("ENDDATE", Convert.ToDateTime(targetGoal.GoalEndDate).ToString("MMMM dd, yyyy"));
                                    emailValues.Add("FREQUENCY", frequency.Frequency);
                                    emailValues.Add("PLAN", targetGoal.GoalName);
                                    result.EmailValues = emailValues;


                                    if (targetGoal.GoalStartDate == DateTime.Now.Date)
                                    {
                                        //targetGoal.NextRunDate = DateTime.Now.Date.AddDays(1);
                                        //Debit customer
                                        ActionReturn debitResult = DebitAccountCustomer(targetGoal, settings);
                                        if (debitResult.Flag)
                                        {
                                            targetGoal.GoalBalance = targetGoal.GoalBalance - targetGoal.TargetFrequencyAmount;
                                            DateTime nextRunDate = Util.GetNextRunDate(Convert.ToDateTime(targetGoal.GoalStartDate), frequency.ValueInDays);
                                            targetGoal.GoalCardTokenDetails = "";
                                            targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                            targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                            this.targetGoalRepository.Add(targetGoal);

                                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();


                                            result.ReturnedObject = targetGoal.TargetGoalsId;
                                            result.UpdateReturn = UpdateReturn.ItemCreated;
                                            result.Message = "Goal was successfully created.";
                                            result.Flag = true;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                        }
                                        else
                                        {
                                            result.HasMultipleLog = true;
                                            result.MultipleLog = debitResult;
                                            result.UpdateReturn = UpdateReturn.ItemCreated;
                                            result.Message = debitResult.Message;
                                            result.Flag = false;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                        }
                                        result.ServiceRequest = debitResult.ServiceRequest;
                                        result.ServiceResponse = debitResult.ServiceResponse;

                                    }
                                    else
                                    {
                                        targetGoal.NextRunDate = targetGoal.GoalStartDate;
                                        targetGoal.GoalCardTokenDetails = "";
                                        targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                        targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                        this.targetGoalRepository.Add(targetGoal);

                                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                    

                                        result.ReturnedObject = targetGoal.TargetGoalsId;
                                        result.UpdateReturn = UpdateReturn.ItemCreated;
                                        result.Message = "Goal created successfully.";
                                        result.Flag = true;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                                    }

                                }
                                else
                                {
                                    // invalid phone number
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, invalid phone number";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = customerEnquiryResult;

                                }

                            }
                            else
                            {
                                //could not retrieve account details for validation
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, could not retrieve account details for validation";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = customerEnquiryResult;
                            }

                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                            result.Message = "Goal was not created, invalid bank or token detail.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = "";

                        }
                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                        result.Message = "Goal was not created, kindly contact your support person.";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = "";
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                    result.Message = "Goal was not created, customer does not exist.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = "";

                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();

            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddTargetGoalWithAccountWithoutOtp(TargetGoal targetGoal)";
            }
            return result;
        }

        public ActionReturn AddTargetGoalWithAccount_Old(TargetGoal targetGoal)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                CustomerProfile customerProfile = customerProfileRepository.Get(targetGoal.CustomerProfileId);
                TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();
                using (TransactionScope scope = new TransactionScope())
                {

                    targetGoal.SetUpDate = DateTime.Now.Date;
                    targetGoal.NextRunDate = Util.GetNextRunDate(DateTime.Now.Date, frequency.ValueInDays);
                    targetGoal.GoalStatus = Enumerators.STATUS.NEW.ToString();
                    targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);

                    ActionReturn configs = this.GetAppConfigSettings();
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;

                    string FCMBBankCode = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    if (targetGoal.GoalDebitType == CONSTANT.BANK_DEBIT_TYPE & targetGoal.GoalDebitBank == FCMBBankCode)
                    {

                        ActionReturn validateAccountAndSendOtpResult = FCMBIntegrator.ValidateAccountAndSendOTP(targetGoal.GoalDebitAccountNo, settings);
                        if (validateAccountAndSendOtpResult.Flag)
                        {
                            DateTime now = DateTime.Now;
                            long milliseconds = DateTime.Now.Ticks;
                            string referenceId = milliseconds.ToString();
                            AccountValidationLog accountValidationLog = new AccountValidationLog
                            {
                                CreatedDate = now,
                                CustomerId = targetGoal.CustomerProfileId,
                                HashValue = Util.Encrypt(JsonConvert.SerializeObject(targetGoal)),
                                LastModifiedDate = now,
                                ReferenceId = referenceId,
                                TargetGoalId = 0
                            };
                            this.accountValidationLogRepository.Add(accountValidationLog);
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            AccountValidationResponse accountValidationResponse = new AccountValidationResponse
                            {
                                ReferenceId = referenceId,
                                Details = JsonConvert.SerializeObject(validateAccountAndSendOtpResult.ReturnedObject)
                            };
                            result.ReturnedObject = accountValidationResponse;
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Goal created successfully, kindly validate your account.";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            // could not validate account number
                            // could not retrieve account details for validation

                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                            result.Message = "Goal was not created, could not retrieve account details for validation";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = "";
                        }

                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                        result.Message = "Goal was not created, invalid bank or token detail.";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = "";

                    }

                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();

            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddTargetGoalWithAccount(TargetGoal targetGoal)";
            }
            return result;
        }

        public ActionReturn ValidateOTP(string accountNo, string otp, long customerProfileId,string referenceId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.HasEmail = true;
                result.ReturnedObject = "";
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();

                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;

                    CustomerProfile customerProfile = customerProfileRepository.Get(customerProfileId);
                    if (customerProfile != null)
                    {
                        AccountValidationLog accountValidationLog = this.accountValidationLogRepository.Find(o => o.CustomerId == customerProfileId & o.ReferenceId == referenceId & o.TargetGoalId == 0).FirstOrDefault();
                        if (accountValidationLog != null)
                        {
                            string hashValue = Util.Decrypt(accountValidationLog.HashValue);
                            TargetGoal targetGoal = JsonConvert.DeserializeObject<TargetGoal>(hashValue);

                            if (targetGoal != null)
                            {
                                if (targetGoal.GoalStatus == Enumerators.STATUS.NEW.ToString())
                                {
                                    
                                    string savedOtp = Util.Decrypt(targetGoal.GoalCardTokenDetails);
                                    //$"{otp}|{FCMB_OTP_EXPIRY_TIME_IN_SEC}|{now.Year}|{now.Month}|{now.Day}|{now.Hour}|{now.Minute}|{now.Second}";
                                    string[] statusCode = savedOtp.Split('|');
                                    if (statusCode.Length > 7)
                                    {
                                        DateTime now = DateTime.Now;
                                        DateTime created = new DateTime(Convert.ToInt32(statusCode[2]),
                                            Convert.ToInt32(statusCode[3]), Convert.ToInt32(statusCode[4]), Convert.ToInt32(statusCode[5]),
                                            Convert.ToInt32(statusCode[6]), Convert.ToInt32(statusCode[7]));

                                        if (statusCode[0] == otp & now <= created.AddSeconds(Convert.ToInt32(statusCode[1])))
                                        {
                                            TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();

                                            //send email to customer
                                            EmailLog emailLog = new EmailLog
                                            {
                                                EmailTo = customerProfile.EmailAddress,
                                                EmailType = "FIRST_EMAIL",
                                                Status = Enumerators.STATUS.NEW.ToString(),
                                            };
                                            result.EmailLog = emailLog;

                                            Dictionary<string, string> emailValues = new Dictionary<string, string>();
                                            emailValues.Add("NAME", $"{customerProfile.Surname} {customerProfile.FirstName}");
                                            emailValues.Add("AMOUNT", targetGoal.TargetFrequencyAmount.ToString("N"));
                                            emailValues.Add("STARTDATE", Convert.ToDateTime(targetGoal.GoalStartDate).ToString("MMMM dd, yyyy"));
                                            emailValues.Add("ENDDATE", Convert.ToDateTime(targetGoal.GoalEndDate).ToString("MMMM dd, yyyy"));
                                            emailValues.Add("FREQUENCY", frequency.Frequency);
                                            emailValues.Add("PLAN", targetGoal.GoalName);
                                            result.EmailValues = emailValues;


                                            if (targetGoal.GoalStartDate == DateTime.Now.Date)
                                            {
                                                targetGoal.GoalBalance = targetGoal.GoalBalance - targetGoal.TargetFrequencyAmount;
                                                DateTime nextRunDate = Util.GetNextRunDate(Convert.ToDateTime(targetGoal.GoalStartDate), frequency.ValueInDays);
                                                targetGoal.GoalCardTokenDetails = "";
                                                targetGoal.GoalStatus = Enumerators.STATUS.PROCESSING.ToString();
                                                targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                                this.targetGoalRepository.Add(targetGoal);
                                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                                accountValidationLog.TargetGoalId = targetGoal.TargetGoalsId;

                                                //Debit customer
                                                ActionReturn debitResult = DebitAccountCustomer(targetGoal, settings);
                                                if (debitResult.Flag)
                                                {
                                                    //get saved goal and update 
                                                    TargetGoal thisGoal = targetGoalRepository.Find(o => o.TargetGoalsId == targetGoal.TargetGoalsId).FirstOrDefault();
                                                    if(thisGoal != null)
                                                    {
                                                        thisGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                                        thisGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, thisGoal);

                                                        accountValidationLog.HashValue = "VALIDATED";
                                                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();


                                                        result.ReturnedObject = targetGoal.TargetGoalsId;
                                                        result.UpdateReturn = UpdateReturn.ItemCreated;
                                                        result.Message = "Goal created successfully.";
                                                        result.Flag = true;
                                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                                    }
                                                    else
                                                    {
                                                        //Goal was created, customer was debited but an error occurred while updaing goal status
                                                    }
                                                    
                                                }
                                                else
                                                {
                                                    result.HasMultipleLog = true;
                                                    result.MultipleLog = debitResult;
                                                    result.UpdateReturn = UpdateReturn.ItemCreated;
                                                    result.Message = debitResult.Message;
                                                    result.Flag = false;
                                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                                }
                                                result.ServiceRequest = debitResult.ServiceRequest;
                                                result.ServiceResponse = debitResult.ServiceResponse;

                                                /*targetGoal.NextRunDate = targetGoal.GoalStartDate;
                                                targetGoal.GoalCardTokenDetails = "";
                                                targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                                targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                                this.targetGoalRepository.Add(targetGoal);
                                                accountValidationLog.TargetGoalId = targetGoal.TargetGoalsId;


                                                accountValidationLog.HashValue = "VALIDATED";
                                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                                scope.Complete();

                                                result.ReturnedObject = targetGoal.TargetGoalsId;
                                                result.UpdateReturn = UpdateReturn.ItemCreated;
                                                result.Message = "Goal created successfully.";
                                                result.Flag = true;
                                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                                */
                                            }
                                            else
                                            {
                                                targetGoal.NextRunDate = targetGoal.GoalStartDate;
                                                targetGoal.GoalCardTokenDetails = "";
                                                targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                                targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                                this.targetGoalRepository.Add(targetGoal);
                                                accountValidationLog.TargetGoalId = targetGoal.TargetGoalsId;


                                                accountValidationLog.HashValue = "VALIDATED";
                                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                            

                                                result.ReturnedObject = targetGoal.TargetGoalsId;
                                                result.UpdateReturn = UpdateReturn.ItemCreated;
                                                result.Message = "Goal created successfully.";
                                                result.Flag = true;
                                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                                            }
                                        }
                                        else
                                        {
                                            //Could not validate otp, it may have been used, expired or does not exist.
                                            result.ReturnedObject = null;
                                            result.UpdateReturn = UpdateReturn.NoChange;
                                            result.Message = "Could not validate otp, it may have been used, expired or does not exist.";
                                            result.Flag = false;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                        }
                                    }
                                    else
                                    {
                                        //could not validate code
                                        result.ReturnedObject = null;
                                        result.UpdateReturn = UpdateReturn.NoChange;
                                        result.Message = "Invalid otp, it may have been used, expired or does not exist.";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    }

                                }
                                else
                                {
                                    //invalid goal
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, invalid goal details.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = "";
                                }
                            }
                            else
                            {
                                //could not load created goal
                                //invalid goal
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, could not load details.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";
                            }
                        }
                        else
                        {
                            //
                            // Could not validate otp, it may have been used, expired or does not exist.
                            result.ReturnedObject = null;
                            result.UpdateReturn = UpdateReturn.NoChange;
                            result.Message = "Could not validate otp, it may have been used, expired or does not exist.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }

                    }
                }
                else
                {
                    //invalid config result
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Could not validate your details, kindly contact your support team.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal could not created at this moment.";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();

            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "ValidateOTP(string accountNo, string otp, long customerProfileId,string referenceId)";
            }
            return result;
        }

        public ActionReturn SelfTopUp(long goalId,string cardToken,decimal amount,bool isCardTransaction)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetGoal goal = targetGoalRepository.Find(o => o.TargetGoalsId == goalId).FirstOrDefault();
                if(goal != null)
                {
                    ActionReturn configs = this.GetAppConfigSettings();
                    if (configs.Flag)
                    {
                        List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                        if (isCardTransaction)
                        {
                            if (!string.IsNullOrEmpty(cardToken))
                            {
                                //get card details and charge card
                                TargetGoal existingGoalToken = this.targetGoalRepository.Find(o => o.GoalCardTokenDetails == cardToken).FirstOrDefault();
                                if (existingGoalToken == null)
                                {
                                    string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                                    string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                                    WebHeaderCollection header = new WebHeaderCollection();
                                    header.Add("Authorization", $"Bearer {SECRET_KEY}");
                                    //verify transaction
                                    VERIFY_URL = $"{VERIFY_URL}/{cardToken}";
                                    ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                                    if (serverVerifyResult.Flag)
                                    {
                                        ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                        if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                        {
                                            ////targetGoal.GoalCardTokenDetails = jsonVerifyResponse.data.authorization.authorization_code;
                                            //targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                            //targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                            //this.targetGoalRepository.Add(targetGoal);
                                            //result.ReturnedObject = targetGoal.TargetGoalsId;
                                            //result.UpdateReturn = UpdateReturn.ItemCreated;
                                            //result.Message = "Goal created successfully";
                                            //result.Flag = true;
                                            //result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                                            //add transaction object.
                                            Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                            {
                                                Amount = amount,
                                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                ApprovedDate = DateTime.Now,
                                                Narration = $"TSA self top up for {goal.GoalName} (Reference:{jsonVerifyResponse.data.reference})",
                                                PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                PostedDate = DateTime.Now,
                                                TargetGoalsId = goal.TargetGoalsId,
                                                TransactionDate = DateTime.Now,
                                                TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()

                                            };
                                            transaction.ChargedFee = Util.ComputeCardFee(100, settings);
                                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);

                                            this.transactionRepository.Add(transaction);

                                            goal.GoalBalance = goal.GoalBalance - amount;

                                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                        }
                                        else
                                        {
                                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                            result.Message = "Payment could not be verified.";
                                            result.Flag = false;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                            result.ReturnedObject = "";
                                        }

                                    }
                                    else
                                    {
                                        result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                        result.Message = "Error vadidating payment as token may be invalid.";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                        result.ReturnedObject = "";
                                    }
                                }
                                else
                                {
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Error vadidating payment as token already exist.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = "";
                                }
                            }
                            else
                            {
                                //invalid input
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Error vadidating empty payment. kindly provide one.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";
                            }
                            
                        }
                        else
                        {
                            //account top up
                            string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                            string FCMB_FIXED_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                            string FCMB_SAVINGS_ACCOUNT = "";
                            //check if goal is fixed or flexible to know the accounting entries
                            if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                            {
                                FCMB_SAVINGS_ACCOUNT = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                            }
                            else if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE)
                            {
                                FCMB_SAVINGS_ACCOUNT = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                            }

                            string transactionId = goal.TargetGoalsId.ToString();
                            string Narration1 = $"TSA Goal self top up for {goal.GoalName}";
                            string Narration2 = "";
                            long milliseconds = DateTime.Now.Ticks;

                            ActionReturn accountSelfTopUpResult = DebitAccountCustomer(goal, settings);
                            if (accountSelfTopUpResult.Flag)
                            {
                                Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                {
                                    Amount = Decimal.Round(amount, 2),
                                    ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    ApprovedDate = DateTime.Now,
                                    Narration = $"TSA Goal self top up for {goal.GoalName}",
                                    PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    PostedDate = DateTime.Now,
                                    TargetGoalsId = goal.TargetGoalsId,
                                    TransactionDate = DateTime.Now,
                                    TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                                };
                                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                this.transactionRepository.Add(transaction);

                                goal.GoalBalance = goal.GoalBalance - amount;

                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                result.UpdateReturn = UpdateReturn.ItemCreated;
                                result.Message = "Goal top up was successfully.";
                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                            else
                            {
                                result.HasMultipleLog = true;
                                result.MultipleLog = accountSelfTopUpResult;
                                result.UpdateReturn = UpdateReturn.ItemCreated;
                                result.Message = accountSelfTopUpResult.Message;
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }
                        }
                    }
                    else
                    {
                        //could not load config
                        result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                        result.Message = "Goal top up was not completed, kindly contact your support person";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = "";
                    }
                    
                    

                }
                else
                {
                    //could not load goal
                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                    result.Message = "Goal top up was not completed, error occurred while validating your goal status. Kindly contact your support person";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = "";
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal top up could not created at this moment.";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();

            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "SelfTopUp(long goalId,string cardToken,decimal amount,bool isCardTransaction)";
            }
            return result;
        }

        private ActionReturn DebitAccountCustomer(TargetGoal goal, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string FCMB_FIXED_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string FCMB_SAVINGS_ACCOUNT = "";
                //check if goal is fixed or flexible to know the accounting entries
                if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                {
                    FCMB_SAVINGS_ACCOUNT = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                }
                else if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE)
                {
                    FCMB_SAVINGS_ACCOUNT = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                }

                string transactionId = goal.TargetGoalsId.ToString();
                string Narration1 = $"TSA Goal top up for {goal.GoalName}";
                string Narration2 = "";
                long milliseconds = DateTime.Now.Ticks;
                string requestId = milliseconds.ToString() + transactionId;
                ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(goal.GoalDebitAccountNo, FCMB_SAVINGS_ACCOUNT, Decimal.Round(goal.TargetFrequencyAmount, 2), CONSTANT.EMPTY,
                    $"{goal.TargetGoalsId}B{goal.CustomerProfileId}", requestId, goal.GoalDebitBank, Narration1, Narration2, settings);

                if (directCreditResult.Flag)
                {
                    FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                    //DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                    {
                        Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        ApprovedDate = DateTime.Now,
                        Narration = $"TSA Goal top up for {goal.GoalName} ({response.StatusMessage})",
                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        PostedDate = DateTime.Now,
                        TargetGoalsId = goal.TargetGoalsId,
                        TransactionDate = DateTime.Now,
                        TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                    };
                    transaction.TransactionType = transaction.TransactionType.ToUpper();
                    transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                    this.transactionRepository.Add(transaction);
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    //goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                    //DateTime goalTransactionDate = DateTime.Now;
                    //transactionSchedule.DateProcessed = goalTransactionDate.Date;
                    //transactionSchedule.DatetimeProcessed = goalTransactionDate;
                    //transactionSchedule.ProcessingStatus = response.StatusMessage;
                    //transactionSchedule.ProcessingResponse = $"SUCCESS:TransactionId:{JsonConvert.SerializeObject(directCreditResult)}";
                    //transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                    //transactionSchedule.Description = $"SUCCESS";

                    result.Flag = true;

                    result.ServiceRequest = directCreditResult.ServiceRequest;
                    result.ServiceResponse = directCreditResult.ServiceResponse;
                }
                else
                {
                    //verify transaction status

                    ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                    if (verifyDirectCreditResult.Flag)
                    {
                        FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                        //GetTransactionStatusResponse verifyDataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(verifyDCResponse.ResponseData));
                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                        {
                            Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            ApprovedDate = DateTime.Now,
                            Narration = $"TSA Goal top up for {goal.GoalName} ({verifyDCResponse.StatusMessage})",
                            PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            PostedDate = DateTime.Now,
                            TargetGoalsId = goal.TargetGoalsId,
                            TransactionDate = DateTime.Now,
                            TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                        };
                        transaction.TransactionType = transaction.TransactionType.ToUpper();
                        transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                        this.transactionRepository.Add(transaction);
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                        //DateTime goalTransactionDate = DateTime.Now;
                        //transactionSchedule.DateProcessed = goalTransactionDate.Date;
                        //transactionSchedule.DatetimeProcessed = goalTransactionDate;
                        //transactionSchedule.ProcessingStatus = verifyDCResponse.StatusMessage;
                        //transactionSchedule.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(verifyDirectCreditResult)}";
                        //transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                        //transactionSchedule.Description = $"SUCCESS";

                        //goal.GoalBalance = goal.GoalBalance - goal.TargetFrequencyAmount;
                        result.Flag = true;
                        result.ServiceRequest =$"DC:{directCreditResult.ServiceRequest} VERIFY:{verifyDirectCreditResult.ServiceRequest}" ;
                        result.ServiceResponse = $"DC:{directCreditResult.ServiceResponse} VERIFY:{verifyDirectCreditResult.ServiceResponse}"; ;

                    }
                    else
                    {
                        //DateTime goalErrorDate = DateTime.Now;
                        //transactionSchedule.DateProcessed = goalErrorDate.Date;
                        //transactionSchedule.DatetimeProcessed = goalErrorDate;
                        //transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                        //transactionSchedule.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(directCreditResult)} VERIFY ERROR:{requestId}:{JsonConvert.SerializeObject(verifyDirectCreditResult)}";
                        //transactionSchedule.Description = $"Account was not debited.";

                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject= $"DEBIT ERROR:{JsonConvert.SerializeObject(directCreditResult)} VERIFY ERROR:{requestId}:{JsonConvert.SerializeObject(verifyDirectCreditResult)}";
                        result.Message= $"We were not able to debit your account, kindly try again.";
                    }
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal top up error";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();

            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "DebitAccountCustomer(TargetGoal goal, List<AppConfigSetting> settings)";
            }
            return result;
        }

        public ActionReturn ValidateOTP_old(string accountNo, string otp, long customerProfileId, string referenceId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.HasEmail = true;
                result.ReturnedObject = "";
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();

                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                ActionReturn validateOtpResult = FCMBIntegrator.ValidateOTP(accountNo, otp, settings);
                if (validateOtpResult.Flag)
                {
                    CustomerProfile customerProfile = customerProfileRepository.Get(customerProfileId);
                    if (customerProfile != null)
                    {
                        AccountValidationLog accountValidationLog = this.accountValidationLogRepository.Find(o => o.CustomerId == customerProfileId & o.ReferenceId == referenceId & o.TargetGoalId == 0).FirstOrDefault();
                        if (accountValidationLog != null)
                        {
                            string hashValue = Util.Decrypt(accountValidationLog.HashValue);
                            TargetGoal targetGoal = JsonConvert.DeserializeObject<TargetGoal>(hashValue);

                            if (targetGoal != null)
                            {
                                if (targetGoal.GoalStatus == Enumerators.STATUS.NEW.ToString())
                                {
                                    TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();

                                    //send email to customer
                                    EmailLog emailLog = new EmailLog
                                    {
                                        EmailTo = customerProfile.EmailAddress,
                                        EmailType = "FIRST_EMAIL",
                                        Status = Enumerators.STATUS.NEW.ToString(),
                                    };
                                    result.EmailLog = emailLog;

                                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                                    emailValues.Add("NAME", $"{customerProfile.Surname} {customerProfile.FirstName}");
                                    emailValues.Add("AMOUNT", targetGoal.TargetFrequencyAmount.ToString("N"));
                                    emailValues.Add("STARTDATE", Convert.ToDateTime(targetGoal.GoalStartDate).ToString("MMMM dd, yyyy"));
                                    emailValues.Add("ENDDATE", Convert.ToDateTime(targetGoal.GoalEndDate).ToString("MMMM dd, yyyy"));
                                    emailValues.Add("FREQUENCY", frequency.Frequency);
                                    result.EmailValues = emailValues;


                                    using (TransactionScope scope = new TransactionScope())
                                    {

                                        targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                        targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                        this.targetGoalRepository.Add(targetGoal);
                                        accountValidationLog.TargetGoalId = targetGoal.TargetGoalsId;
                                        accountValidationLog.HashValue = Util.Encrypt(JsonConvert.SerializeObject(targetGoal));
                                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                        scope.Complete();

                                        result.ReturnedObject = targetGoal.TargetGoalsId;
                                        result.UpdateReturn = UpdateReturn.ItemCreated;
                                        result.Message = "Goal created successfully.";
                                        result.Flag = true;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                                    }

                                }
                                else
                                {
                                    //invalid goal
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, invalid goal details.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = "";
                                }
                            }
                            else
                            {
                                //could not load created goal
                                //invalid goal
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, could not load details.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";
                            }
                        }
                        else
                        {
                            //
                            // Could not validate otp, it may have been used, expired or does not exist.
                            result.ReturnedObject = null;
                            result.UpdateReturn = UpdateReturn.NoChange;
                            result.Message = "Could not validate otp, it may have been used, expired or does not exist.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }

                    }

                }
                else
                {
                    //invalid otp
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Could not validate otp, it is invalid.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }



            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();

            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "ValidateOTP(string accountNo, string otp, long customerProfileId,string referenceId)";
            }
            return result;
        }
        public ActionReturn AddTargetGoal_Old4(TargetGoal targetGoal)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.HasEmail = true;
                result.ReturnedObject = "";
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();

                //ActionReturn frequency = utilityService.GetTargetSavingsFrequencySync();
                CustomerProfile customerProfile = customerProfileRepository.Get(targetGoal.CustomerProfileId);
                TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();
                using (TransactionScope scope = new TransactionScope())
                {

                    //send email to customer
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = customerProfile.EmailAddress,
                        EmailType = "FIRST_EMAIL",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };
                    result.EmailLog = emailLog;

                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                    emailValues.Add("NAME", $"{customerProfile.Surname} {customerProfile.FirstName}");
                    emailValues.Add("AMOUNT", targetGoal.TargetFrequencyAmount.ToString("N"));
                    emailValues.Add("STARTDATE", Convert.ToDateTime(targetGoal.GoalStartDate).ToString("MMMM dd, yyyy"));
                    emailValues.Add("ENDDATE", Convert.ToDateTime(targetGoal.GoalEndDate).ToString("MMMM dd, yyyy"));
                    emailValues.Add("FREQUENCY", frequency.Frequency);
                    result.EmailValues = emailValues;

                    targetGoal.SetUpDate = DateTime.Now.Date;
                    targetGoal.NextRunDate = Util.GetNextRunDate(DateTime.Now.Date, frequency.ValueInDays);
                    targetGoal.GoalStatus = Enumerators.STATUS.NEW.ToString();


                    ActionReturn configs = this.GetAppConfigSettings();
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    TargetDebitAccountProfile tartgetDebitAccontProfile = new TargetDebitAccountProfile
                    {
                        DateCreated = DateTime.Now,
                        HashValue = "",
                        IsActive = true,
                        LastModifiedDate = DateTime.Now,
                        Status = Enumerators.STATUS.NEW.ToString(),
                        MandateId = ""
                    };
                    string FCMBBankCode = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    if (targetGoal.GoalDebitType == CONSTANT.BANK_DEBIT_TYPE & targetGoal.GoalDebitBank == FCMBBankCode)
                    {
                        targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                        this.targetGoalRepository.Add(targetGoal);
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        TargetGoal savedTargetGoal = targetGoalRepository.Get(targetGoal.TargetGoalsId);

                        ActionReturn customerEnquiryResult = FCMBIntegrator.GetCustomerInformationByAccountNo(targetGoal.GoalDebitAccountNo, settings);
                        if (customerEnquiryResult.Flag)
                        {
                            CustomerAccountInformation customerAccountInformation = (CustomerAccountInformation)customerEnquiryResult.ReturnedObject;
                            if (!string.IsNullOrEmpty(customerAccountInformation.PhoneNumber))
                            {
                                //send otp to phone number tied to account number
                                string sbuCode = settings.Where(o => o.Key == "FCMB_SMS_SBU_CODE").Select(o => o.Value).FirstOrDefault();
                                string sender = settings.Where(o => o.Key == "FCMB_SMS_SENDER_ID").Select(o => o.Value).FirstOrDefault();
                                string channel = settings.Where(o => o.Key == "FCMB_SMS_CHANNEL").Select(o => o.Value).FirstOrDefault();
                                string branchCode = settings.Where(o => o.Key == "FCMB_SMS_BRANCH_CODE").Select(o => o.Value).FirstOrDefault();
                                string SMSOtpTemplate = settings.Where(o => o.Key == "FCMB_SMS_OTP_TEMPLATE").Select(o => o.Value).FirstOrDefault();
                                string FCMB_OTP_EXPIRY_TIME_IN_SEC = settings.Where(o => o.Key == "FCMB_OTP_EXPIRY_TIME_IN_SEC").Select(o => o.Value).FirstOrDefault();

                                ActionReturn otpResult = Util.GenerateOtp();
                                if (otpResult.Flag)
                                {
                                    string otp = Convert.ToString(otpResult.ReturnedObject);
                                    string message = SMSOtpTemplate.Replace("#OTP#", otp);
                                    ActionReturn sendSMSResult = FCMBIntegrator.SendSms(targetGoal.GoalDebitAccountNo, message, sender, customerAccountInformation.PhoneNumber, sbuCode, channel, branchCode, "");
                                    if (sendSMSResult.Flag)
                                    {
                                        savedTargetGoal.GoalStatus = Enumerators.STATUS.PENDING.ToString();
                                        savedTargetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, savedTargetGoal);


                                        long milliseconds = DateTime.Now.Ticks;
                                        string remitaTransRef = milliseconds.ToString();
                                        ValidateMandateParam param1 = new ValidateMandateParam { param1 = remitaTransRef, description1 = "OTP" };
                                        List<ValidateMandateParam> authParam = new List<ValidateMandateParam>();
                                        authParam.Add(param1);
                                        ValidateMandateResponse validateMandateResponse = new ValidateMandateResponse
                                        {
                                            authParams = authParam,
                                            TargetGoalId = targetGoal.TargetGoalsId,
                                            requestId = targetGoal.CustomerProfileId.ToString(),
                                            remitaTransRef = remitaTransRef
                                        };
                                        DateTime now = DateTime.Now;
                                        ValidateMandateResponse validateMandateResponseLog = validateMandateResponse;
                                        validateMandateResponseLog.statuscode = $"{Enumerators.STATUS.NEW.ToString()}|{FCMB_OTP_EXPIRY_TIME_IN_SEC}|{now.Year}|{now.Month}|{now.Day}|{now.Hour}|{now.Minute}|{now.Second}";
                                        AccountValidationLog accountValidationLog = new AccountValidationLog
                                        {
                                            CreatedDate = now,
                                            CustomerId = targetGoal.CustomerProfileId,
                                            HashValue = Util.Encrypt(JsonConvert.SerializeObject(validateMandateResponseLog)),
                                            LastModifiedDate = now,
                                            ReferenceId = remitaTransRef,
                                            TargetGoalId = targetGoal.TargetGoalsId
                                        };
                                        this.accountValidationLogRepository.Add(accountValidationLog);

                                        validateMandateResponse.statuscode = Enumerators.STATUS.NEW.ToString();
                                        result.ReturnedObject = validateMandateResponse;// sendSMSResult.ReturnedObject;
                                        result.UpdateReturn = UpdateReturn.ItemCreated;
                                        result.Message = "Goal created successfully, kindly validate your account.";
                                        result.Flag = true;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                    }
                                    else
                                    {
                                        targetGoalRepository.Remove(savedTargetGoal);
                                        result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                        result.Message = "Goal was not created, error sending account validation code";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                        result.ReturnedObject = "";
                                    }
                                }
                                else
                                {
                                    targetGoalRepository.Remove(savedTargetGoal);
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, error generating account validation code";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = "";
                                }
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                scope.Complete();
                            }
                            else
                            {
                                // invalid phone number
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, invalid phone number";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";

                            }

                        }
                        else
                        {
                            //could not retrieve account details for validation
                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                            result.Message = "Goal was not created, could not retrieve account details for validation";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = "";
                        }

                    }
                    else if (targetGoal.GoalDebitType == CONSTANT.CARD_DEBIT_TYPE)
                    {
                        //payment with card
                        //confirm reference 
                        TargetGoal existingGoalToken = this.targetGoalRepository.Find(o => o.GoalCardTokenDetails == targetGoal.GoalCardTokenDetails).FirstOrDefault();
                        if (existingGoalToken == null)
                        {
                            string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                            string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                            WebHeaderCollection header = new WebHeaderCollection();
                            header.Add("Authorization", $"Bearer {SECRET_KEY}");
                            //verify transaction
                            VERIFY_URL = $"{VERIFY_URL}/{targetGoal.GoalCardTokenDetails}";
                            ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                            if (serverVerifyResult.Flag)
                            {
                                ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                {
                                    targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                    targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                    this.targetGoalRepository.Add(targetGoal);
                                    result.ReturnedObject = targetGoal.TargetGoalsId;
                                    result.UpdateReturn = UpdateReturn.ItemCreated;
                                    result.Message = "Goal created successfully";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                                    //add transaction object.
                                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                    {
                                        Amount = 100,
                                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                        ApprovedDate = DateTime.Now,
                                        Narration = $"{jsonVerifyResponse.data.reference}",
                                        PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                        PostedDate = DateTime.Now,
                                        TargetGoalsId = targetGoal.TargetGoalsId,
                                        TransactionDate = DateTime.Now,
                                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                    };
                                    transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);

                                    this.transactionRepository.Add(transaction);

                                    targetGoal.GoalBalance = targetGoal.GoalBalance - 100;

                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                    scope.Complete();
                                }
                                else
                                {
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, payment could not be verified.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = "";
                                }

                            }
                            else
                            {
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, error vadidating payment as token may be invalid.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";
                            }
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                            result.Message = "Goal was not created, error vadidating payment as token already exist.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = "";
                        }


                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                        result.Message = "Goal was not created, invalid bank or token detail.";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = "";

                    }

                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
                //if (targetGoal.GoalDebitType == CONSTANT.BANK_DEBIT_TYPE)
                //{
                //    TargetGoal savedTargetGoal = targetGoalRepository.Get(targetGoal.TargetGoalsId);
                //    targetGoalRepository.Remove(savedTargetGoal);
                //    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                //}


            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddTargetGoal(TargetGoal targetGoal)";
            }
            return result;
        }

        public ActionReturn AddTargetGoal_Old3(TargetGoal targetGoal, string SMSConnectionString)
        {
            ActionReturn result = new ActionReturn();
            try
            {


                result.HasEmail = true;
                result.ReturnedObject = "";
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();

                //ActionReturn frequency = utilityService.GetTargetSavingsFrequencySync();
                CustomerProfile customerProfile = customerProfileRepository.Get(targetGoal.CustomerProfileId);
                TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();
                using (TransactionScope scope = new TransactionScope())
                {

                    //send email to customer
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = customerProfile.EmailAddress,
                        EmailType = "FIRST_EMAIL",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };
                    result.EmailLog = emailLog;

                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                    emailValues.Add("NAME", $"{customerProfile.Surname} {customerProfile.FirstName}");
                    emailValues.Add("AMOUNT", targetGoal.TargetFrequencyAmount.ToString("N"));
                    emailValues.Add("STARTDATE", Convert.ToDateTime(targetGoal.GoalStartDate).ToString("MMMM dd, yyyy"));
                    emailValues.Add("ENDDATE", Convert.ToDateTime(targetGoal.GoalEndDate).ToString("MMMM dd, yyyy"));
                    emailValues.Add("FREQUENCY", frequency.Frequency);
                    result.EmailValues = emailValues;

                    targetGoal.SetUpDate = DateTime.Now.Date;
                    targetGoal.NextRunDate = Util.GetNextRunDate(DateTime.Now.Date, frequency.ValueInDays);
                    targetGoal.GoalStatus = Enumerators.STATUS.NEW.ToString();


                    ActionReturn configs = this.GetAppConfigSettings();
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    TargetDebitAccountProfile tartgetDebitAccontProfile = new TargetDebitAccountProfile
                    {
                        DateCreated = DateTime.Now,
                        HashValue = "",
                        IsActive = true,
                        LastModifiedDate = DateTime.Now,
                        Status = Enumerators.STATUS.NEW.ToString(),
                        MandateId = ""
                    };
                    string FCMBBankCode = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    if (targetGoal.GoalDebitType == CONSTANT.BANK_DEBIT_TYPE)
                    {
                        targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                        this.targetGoalRepository.Add(targetGoal);
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        TargetGoal savedTargetGoal = targetGoalRepository.Get(targetGoal.TargetGoalsId);

                        if (targetGoal.GoalDebitBank == FCMBBankCode)
                        {
                            ActionReturn customerEnquiryResult = FCMBIntegrator.GetCustomerInformationByAccountNo(targetGoal.GoalDebitAccountNo, settings);
                            if (customerEnquiryResult.Flag)
                            {

                            }
                            else
                            {

                            }


                            //send otp to phone number tied to account number
                            string sbuCode = settings.Where(o => o.Key == "FCMB_SMS_SBU_CODE").Select(o => o.Value).FirstOrDefault();
                            string sender = settings.Where(o => o.Key == "FCMB_SMS_SENDER_ID").Select(o => o.Value).FirstOrDefault();
                            string channel = settings.Where(o => o.Key == "FCMB_SMS_CHANNEL").Select(o => o.Value).FirstOrDefault();
                            string branchCode = settings.Where(o => o.Key == "FCMB_SMS_BRANCH_CODE").Select(o => o.Value).FirstOrDefault();
                            string SMSOtpTemplate = settings.Where(o => o.Key == "FCMB_SMS_OTP_TEMPLATE").Select(o => o.Value).FirstOrDefault();
                            string FCMB_OTP_EXPIRY_TIME_IN_SEC = settings.Where(o => o.Key == "FCMB_OTP_EXPIRY_TIME_IN_SEC").Select(o => o.Value).FirstOrDefault();
                            ActionReturn otpResult = Util.GenerateOtp();
                            if (otpResult.Flag)
                            {
                                string otp = Convert.ToString(otpResult.ReturnedObject);
                                string message = SMSOtpTemplate.Replace("#OTP#", otp);
                                ActionReturn sendSMSResult = FCMBIntegrator.SendSms(targetGoal.GoalDebitAccountNo, message, sender, customerProfile.PhoneNumber, sbuCode, channel, branchCode, SMSConnectionString);
                                if (sendSMSResult.Flag)
                                {
                                    savedTargetGoal.GoalStatus = Enumerators.STATUS.PENDING.ToString();
                                    savedTargetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, savedTargetGoal);


                                    long milliseconds = DateTime.Now.Ticks;
                                    string remitaTransRef = milliseconds.ToString();
                                    ValidateMandateParam param1 = new ValidateMandateParam { param1 = remitaTransRef, description1 = "OTP" };
                                    List<ValidateMandateParam> authParam = new List<ValidateMandateParam>();
                                    authParam.Add(param1);
                                    ValidateMandateResponse validateMandateResponse = new ValidateMandateResponse
                                    {
                                        authParams = authParam,
                                        TargetGoalId = targetGoal.TargetGoalsId,
                                        requestId = targetGoal.CustomerProfileId.ToString(),
                                        remitaTransRef = remitaTransRef
                                    };
                                    DateTime now = DateTime.Now;
                                    ValidateMandateResponse validateMandateResponseLog = validateMandateResponse;
                                    validateMandateResponseLog.statuscode = $"{Enumerators.STATUS.NEW.ToString()}|{FCMB_OTP_EXPIRY_TIME_IN_SEC}|{now.Year}|{now.Month}|{now.Day}|{now.Hour}|{now.Minute}|{now.Second}";
                                    AccountValidationLog accountValidationLog = new AccountValidationLog
                                    {
                                        CreatedDate = now,
                                        CustomerId = targetGoal.CustomerProfileId,
                                        HashValue = Util.Encrypt(JsonConvert.SerializeObject(validateMandateResponseLog)),
                                        LastModifiedDate = now,
                                        ReferenceId = remitaTransRef,
                                        TargetGoalId = targetGoal.TargetGoalsId
                                    };
                                    this.accountValidationLogRepository.Add(accountValidationLog);
                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                    scope.Complete();

                                    result.ReturnedObject = validateMandateResponse;// sendSMSResult.ReturnedObject;
                                    result.UpdateReturn = UpdateReturn.ItemCreated;
                                    result.Message = "Goal created successfully, kindly validate your account.";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                }
                                else
                                {
                                    targetGoalRepository.Remove(savedTargetGoal);
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, error sending account validation code";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = "";
                                }
                            }
                            else
                            {
                                targetGoalRepository.Remove(savedTargetGoal);
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, error generating account validation code";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";
                            }

                        }
                        else
                        {
                            Mandate mandate = new Mandate
                            {
                                amount = (Decimal.Round(targetGoal.TargetFrequencyAmount, 2)).ToString(),
                                endDate = (Convert.ToDateTime(targetGoal.GoalEndDate)).ToString("dd/MM/yyyy"),
                                frequency = "1",
                                hash = "",
                                mandateType = "",
                                maxNoOfDebits = "",
                                merchantId = "",
                                payerAccount = targetGoal.GoalDebitAccountNo,
                                payerBankCode = targetGoal.GoalDebitBank,
                                payerEmail = targetGoal.CustomerProfile.EmailAddress,
                                payerName = $"{targetGoal.CustomerProfile.FirstName} {targetGoal.CustomerProfile.Surname}",
                                payerPhone = targetGoal.CustomerProfile.PhoneNumber,
                                requestId = "",
                                serviceTypeId = "",
                                startDate = (Convert.ToDateTime(targetGoal.GoalStartDate)).ToString("dd/MM/yyyy")
                            };


                            ActionReturn setupResult = Util.SetupMandate(mandate, settings);
                            if (setupResult.Flag)
                            {
                                MandateResponse response = (MandateResponse)setupResult.ReturnedObject;
                                tartgetDebitAccontProfile.MandateId = response.mandateId;
                                tartgetDebitAccontProfile.RequestId = response.requestId;
                                //set goal status to pending so its not shown until mandate is authorized with an otp.
                                savedTargetGoal.GoalStatus = Enumerators.STATUS.PENDING.ToString();

                                //request validation so the user can complete it here.
                                ActionReturn requestOtpResult = Util.RequestMandateOTP(tartgetDebitAccontProfile, settings);
                                if (requestOtpResult.Flag)
                                {
                                    ValidateMandateResponse validateMandateResponse = (ValidateMandateResponse)requestOtpResult.ReturnedObject;
                                    validateMandateResponse.TargetGoalId = targetGoal.TargetGoalsId;
                                    result.ReturnedObject = validateMandateResponse;
                                    result.UpdateReturn = UpdateReturn.ItemCreated;
                                    result.Message = "Goal created successfully, kindly authorize future debit standing order.";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                }
                                else
                                {
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was created, kindly validate debit standing order.";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = targetGoal.TargetGoalsId;
                                }
                                tartgetDebitAccontProfile.TargetGoalId = targetGoal.TargetGoalsId;
                                tartgetDebitAccontProfile.HashValue = Util.GetEncryptedHashValue(CONSTANT.DEBIT_ACCOUNT_PROFILE, tartgetDebitAccontProfile);
                                this.targetDebitAccontProfileRepository.Add(tartgetDebitAccontProfile);
                                savedTargetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, savedTargetGoal);
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                scope.Complete();

                            }
                            else
                            {
                                targetGoalRepository.Remove(savedTargetGoal);
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, error setting up payment order";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";
                            }
                        }

                    }
                    else
                    {
                        //payment with card
                        //confirm reference 
                        TargetGoal existingGoalToken = this.targetGoalRepository.Find(o => o.GoalCardTokenDetails == targetGoal.GoalCardTokenDetails).FirstOrDefault();
                        if (existingGoalToken == null)
                        {
                            string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                            string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                            WebHeaderCollection header = new WebHeaderCollection();
                            header.Add("Authorization", $"Bearer {SECRET_KEY}");
                            //verify transaction
                            VERIFY_URL = $"{VERIFY_URL}/{targetGoal.GoalCardTokenDetails}";
                            ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                            if (serverVerifyResult.Flag)
                            {
                                ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                {
                                    targetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                    targetGoal.HashValue = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, targetGoal);
                                    this.targetGoalRepository.Add(targetGoal);
                                    result.ReturnedObject = targetGoal.TargetGoalsId;
                                    result.UpdateReturn = UpdateReturn.ItemCreated;
                                    result.Message = "Goal created successfully";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                                    //add transaction object.
                                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                    {
                                        Amount = 100,
                                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                        ApprovedDate = DateTime.Now,
                                        Narration = $"{jsonVerifyResponse.data.reference}",
                                        PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                        PostedDate = DateTime.Now,
                                        TargetGoalsId = targetGoal.TargetGoalsId,
                                        TransactionDate = DateTime.Now,
                                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                    };
                                    transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);

                                    this.transactionRepository.Add(transaction);

                                    targetGoal.GoalBalance = targetGoal.GoalBalance - 100;

                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                    scope.Complete();
                                }
                                else
                                {
                                    result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                    result.Message = "Goal was not created, payment could not be verified.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    result.ReturnedObject = "";
                                }

                            }
                            else
                            {
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was not created, error vadidating payment as token may be invalid.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = "";
                            }
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                            result.Message = "Goal was not created, error vadidating payment as token already exist.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = "";
                        }


                    }

                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddTargetGoal(TargetGoal targetGoal)";
            }
            return result;
        }

        public ActionReturn AddTargetGoal_Old2(TargetGoal targetGoal)
        {
            ActionReturn result = new ActionReturn();
            try
            {


                result.HasEmail = true;
                result.ReturnedObject = "";
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();

                //ActionReturn frequency = utilityService.GetTargetSavingsFrequencySync();
                CustomerProfile customerProfile = customerProfileRepository.Get(targetGoal.CustomerProfileId);
                TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();
                using (TransactionScope scope = new TransactionScope())
                {

                    //send email to customer
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = customerProfile.EmailAddress,
                        EmailType = "FIRST_EMAIL",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };
                    result.EmailLog = emailLog;

                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                    emailValues.Add("NAME", $"{customerProfile.Surname} {customerProfile.FirstName}");
                    emailValues.Add("AMOUNT", targetGoal.TargetFrequencyAmount.ToString("N"));
                    emailValues.Add("STARTDATE", Convert.ToDateTime(targetGoal.GoalStartDate).ToString("MMMM dd, yyyy"));
                    emailValues.Add("ENDDATE", Convert.ToDateTime(targetGoal.GoalEndDate).ToString("MMMM dd, yyyy"));
                    emailValues.Add("FREQUENCY", frequency.Frequency);
                    result.EmailValues = emailValues;

                    targetGoal.SetUpDate = DateTime.Now.Date;
                    targetGoal.NextRunDate = Util.GetNextRunDate(DateTime.Now.Date, frequency.ValueInDays);
                    targetGoal.GoalStatus = Enumerators.STATUS.NEW.ToString();
                    this.targetGoalRepository.Add(targetGoal);



                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();



                    TargetDebitAccountProfile tartgetDebitAccontProfile = new TargetDebitAccountProfile
                    {
                        DateCreated = DateTime.Now,
                        HashValue = $"{targetGoal.CustomerProfileId}|{targetGoal.GoalDebitAccountNo}|{targetGoal.GoalDebitBank}|{targetGoal.GoalEndDate}|{targetGoal.GoalStartDate}|{targetGoal.TargetFrequency}|{targetGoal.TargetFrequencyAmount}",
                        IsActive = true,
                        LastModifiedDate = DateTime.Now,
                        Status = Enumerators.STATUS.NEW.ToString(),
                        TargetGoalId = targetGoal.TargetGoalsId,
                        MandateId = ""
                    };
                    ActionReturn configs = this.GetAppConfigSettings();
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    TargetGoal savedTargetGoal = targetGoalRepository.Get(targetGoal.TargetGoalsId);
                    if (targetGoal.GoalDebitType == CONSTANT.BANK_DEBIT_TYPE)
                    {
                        Mandate mandate = new Mandate
                        {
                            amount = (Decimal.Round(targetGoal.TargetFrequencyAmount, 2)).ToString(),
                            endDate = (Convert.ToDateTime(targetGoal.GoalEndDate)).ToString("dd/MM/yyyy"),
                            frequency = "1",
                            hash = "",
                            mandateType = "",
                            maxNoOfDebits = "",
                            merchantId = "",
                            payerAccount = targetGoal.GoalDebitAccountNo,
                            payerBankCode = targetGoal.GoalDebitBank,
                            payerEmail = targetGoal.CustomerProfile.EmailAddress,
                            payerName = $"{targetGoal.CustomerProfile.FirstName} {targetGoal.CustomerProfile.Surname}",
                            payerPhone = targetGoal.CustomerProfile.PhoneNumber,
                            requestId = "",
                            serviceTypeId = "",
                            startDate = (Convert.ToDateTime(targetGoal.GoalStartDate)).ToString("dd/MM/yyyy")
                        };


                        ActionReturn setupResult = Util.SetupMandate(mandate, settings);
                        if (setupResult.Flag)
                        {
                            MandateResponse response = (MandateResponse)setupResult.ReturnedObject;
                            tartgetDebitAccontProfile.MandateId = response.mandateId;
                            tartgetDebitAccontProfile.RequestId = response.requestId;
                            //set goal status to pending so its not shown until mandate is authorized with an otp.
                            savedTargetGoal.GoalStatus = Enumerators.STATUS.PENDING.ToString();

                            //request validation so the user can complete it here.
                            ActionReturn requestOtpResult = Util.RequestMandateOTP(tartgetDebitAccontProfile, settings);
                            if (requestOtpResult.Flag)
                            {
                                ValidateMandateResponse validateMandateResponse = (ValidateMandateResponse)requestOtpResult.ReturnedObject;
                                validateMandateResponse.TargetGoalId = targetGoal.TargetGoalsId;
                                result.ReturnedObject = validateMandateResponse;
                                result.UpdateReturn = UpdateReturn.ItemCreated;
                                result.Message = "Goal created successfully, kindly authorize future debit standing order.";
                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                            else
                            {
                                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                                result.Message = "Goal was created, kindly authorize debit standing order.";
                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                result.ReturnedObject = targetGoal.TargetGoalsId;
                            }

                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                            result.Message = "Goal was not created";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = "";
                        }
                    }
                    else
                    {
                        //confirm reference                        
                        string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                        string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                        WebHeaderCollection header = new WebHeaderCollection();
                        header.Add("Authorization", $"Bearer {SECRET_KEY}");
                        //verify transaction
                        VERIFY_URL = $"{VERIFY_URL}/{targetGoal.GoalCardTokenDetails}";
                        ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                        if (serverVerifyResult.Flag)
                        {
                            savedTargetGoal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                            result.ReturnedObject = targetGoal.TargetGoalsId;
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Goal created";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {

                        }

                    }

                    this.targetDebitAccontProfileRepository.Add(tartgetDebitAccontProfile);
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    scope.Complete();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public ActionReturn AddTargetGoal_old(TargetGoal targetGoal)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = "";
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();

                //ActionReturn frequency = utilityService.GetTargetSavingsFrequencySync();
                TargetSavingsFrequency frequency = targetSavingsFrequencyRepository.GetAll().Where(o => o.Id == targetGoal.TargetFrequency).FirstOrDefault();
                using (TransactionScope scope = new TransactionScope())
                {
                    targetGoal.SetUpDate = DateTime.Now.Date;
                    targetGoal.NextRunDate = Util.GetNextRunDate(DateTime.Now.Date, frequency.ValueInDays);
                    this.targetGoalRepository.Add(targetGoal);

                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    TargetDebitAccountProfile tartgetDebitAccontProfile = new TargetDebitAccountProfile
                    {
                        DateCreated = DateTime.Now,
                        HashValue = $"{targetGoal.CustomerProfileId}|{targetGoal.GoalDebitAccountNo}|{targetGoal.GoalDebitBank}|{targetGoal.GoalEndDate}|{targetGoal.GoalStartDate}|{targetGoal.TargetFrequency}|{targetGoal.TargetFrequencyAmount}",
                        IsActive = true,
                        LastModifiedDate = DateTime.Now,
                        Status = Enumerators.STATUS.NEW.ToString(),
                        TargetGoalId = targetGoal.TargetGoalsId,
                    };
                    this.targetDebitAccontProfileRepository.Add(tartgetDebitAccontProfile);
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    

                    scope.Complete();

                    result.ReturnedObject = targetGoal.TargetGoalsId;
                    result.UpdateReturn = UpdateReturn.ItemCreated;
                    result.Message = "Goal created";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public async Task<ActionReturn> GetAllTargetGoalAsync()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus=Enumerators.STATUS.COMPLETED.ToString();
                IEnumerable<TargetGoal> targetGoals = targetGoalRepository.GetAll().Where(o=>o.GoalStatus==goalStatus ).OrderByDescending(o=>o.TargetGoalsId);

                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                foreach(TargetGoal goal in targetGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        targetGoalList.Add(goal);
                    }
                }
                result.ReturnedObject = targetGoalList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target goal retrieval was successful";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetAllTargetGoalAsync()";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetNTargetGoalAsync(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                var activeGoals = targetGoalRepository.All.Where(o => o.GoalStatus == goalStatus ).OrderByDescending(o => o.TargetGoalsId).Include(o => o.CustomerProfile).AsNoTracking();
                List<TargetGoal> targetGoals = new List<TargetGoal>();
                foreach (TargetGoal goal in activeGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        targetGoals.Add(goal);
                    }
                }
                IEnumerable<TargetGoal> targetGoalList = targetGoals;
                var totalCount = targetGoalList.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    targetGoalList = targetGoalList.OrderByDescending(o => o.TargetGoalsId).Where(x => x.TargetGoalsId < pagination.lastIdFetched);
                }
                else
                {
                    targetGoalList = targetGoalList.OrderByDescending(o => o.TargetGoalsId);
                }

                List<TargetGoal> items = targetGoalList.OrderByDescending(o => o.TargetGoalsId)
                                        .Take(pagination.pageSize)                                        
                                        .ToList();
                foreach(TargetGoal t in items)
                {
                    t.CustomerProfile.TargetGoals = null;
                    t.CustomerProfile.Password = null;
                }

                var pagedResult = new Paged<TargetGoal>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target goal was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNTargetGoalAsync(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetAllNTargetGoalAsync(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                var activeGoals = targetGoalRepository.All.Where(o => (o.GoalStatus == goalStatus || o.GoalStatus == completedStatus)).OrderByDescending(o => o.TargetGoalsId).Include(o => o.CustomerProfile).AsNoTracking();
                List<TargetGoal> targetGoals = new List<TargetGoal>();
                foreach (TargetGoal goal in activeGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        targetGoals.Add(goal);
                    }
                }
                IEnumerable<TargetGoal> targetGoalList = targetGoals;
                var totalCount = targetGoalList.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    targetGoalList = targetGoalList.OrderByDescending(o => o.TargetGoalsId).Where(x => x.TargetGoalsId < pagination.lastIdFetched);
                }
                else
                {
                    targetGoalList = targetGoalList.OrderByDescending(o => o.TargetGoalsId);
                }

                List<TargetGoal> items = targetGoalList.OrderByDescending(o => o.TargetGoalsId)
                                        .Take(pagination.pageSize)
                                        .ToList();
                foreach (TargetGoal t in items)
                {
                    t.CustomerProfile.TargetGoals = null;
                    t.CustomerProfile.Password = null;
                }

                var pagedResult = new Paged<TargetGoal>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target goal was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNTargetGoalAsync(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetAllTargetGoalsByCustomerAsync(long customerId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                IEnumerable<TargetGoal> targetGoals = targetGoalRepository.GetAll().Where(o => (o.GoalStatus == goalStatus || o.GoalStatus == completedStatus) && o.CustomerProfileId == customerId).OrderByDescending(o => o.TargetGoalsId);
                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                foreach (TargetGoal goal in targetGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        targetGoalList.Add(goal);
                    }
                }
                result.ReturnedObject = targetGoalList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target goal was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetGoalsByCustomerAsync(long customerId)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetTargetGoalsByCustomerAsync(long customerId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                IEnumerable<TargetGoal> targetGoals = targetGoalRepository.GetAll().Where(o => o.GoalStatus == goalStatus && o.CustomerProfileId == customerId).OrderByDescending(o => o.TargetGoalsId);
                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                foreach (TargetGoal goal in targetGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        targetGoalList.Add(goal);
                    }
                }
                result.ReturnedObject = targetGoalList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target goal was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetGoalsByCustomerAsync(long customerId)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetCompletedTargetGoalsByCustomerAsync(long customerId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                IEnumerable<TargetGoal> targetGoals = targetGoalRepository.GetAll().Where(o => o.GoalStatus == completedStatus && o.CustomerProfileId == customerId).OrderByDescending(o => o.TargetGoalsId);
                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                foreach (TargetGoal goal in targetGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        targetGoalList.Add(goal);
                    }
                }
                result.ReturnedObject = targetGoalList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target goal was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetCompletedTargetGoalsByCustomerAsync(long customerId)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetTargetGoalById(long targetId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                IEnumerable<TargetGoal> targetGoals = targetGoalRepository.GetAll().Where(o => o.GoalStatus == goalStatus && o.TargetGoalsId == targetId);
                
                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                foreach (TargetGoal goal in targetGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        targetGoalList.Add(goal);
                    }
                }
                result.ReturnedObject = targetGoalList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target goal was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetGoalById(long targetId)";
            }
            return await Task.FromResult(result);
        }
        public ActionReturn GetTargetGoalByIdSync(long targetId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                var goal= targetGoalRepository.GetAll().Where(o => o.TargetGoalsId == targetId && o.GoalStatus == goalStatus).FirstOrDefault();
                string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                if (hash == goal.HashValue)
                {
                    result.ReturnedObject = goal;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Target goal was retrieved successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Target goal could not be retrieved";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetGoalByIdSync(long targetId)";
            }
            return result;
        }

        public async Task<ActionReturn> GetDailyTargets(DateTime dateTime, string status)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                //string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                IEnumerable<TargetGoal> targetGoals = targetGoalRepository.GetAll();
                result.ReturnedObject = targetGoals.Where(o => o.SetUpDate == dateTime && o.GoalStatus == status).ToList();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target goal was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetGoalByIdSync(long targetId)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetTargetsCount()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                result.ReturnedObject = targetGoalRepository.All.Where(o => o.GoalStatus == goalStatus).Count();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "target goal count was successful";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal count failed with exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetsCount()";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetTotalTargetGoalAndBalanceByCustomer(long customerId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                IEnumerable<TargetGoal> targetGoals = targetGoalRepository.GetAll().Where(o => o.CustomerProfileId == customerId && o.GoalStatus == goalStatus).ToList();
                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                foreach (TargetGoal goal in targetGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        targetGoalList.Add(goal);
                    }
                }
                result.ReturnedObject= from i in targetGoalList
                                       group i by i.CustomerProfileId into x
                                       select new
                                       {
                                           TotalGoalCount = x.Count(),
                                           TotalGoalBalance = x.Sum(o => o.GoalBalance)
                                       };
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Customer's target goal information was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Customer's target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTotalTargetGoalAndBalanceByCustomer(long customerId)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetCompletedTotalTargetGoalAndBalanceByCustomer(long customerId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                string completedStatus = Enumerators.STATUS.COMPLETED.ToString();
                IEnumerable<TargetGoal> targetGoals = targetGoalRepository.GetAll().Where(o => o.CustomerProfileId == customerId && o.GoalStatus == completedStatus).ToList();
                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                foreach (TargetGoal goal in targetGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        targetGoalList.Add(goal);
                    }
                }
                result.ReturnedObject = from i in targetGoalList
                                        group i by i.CustomerProfileId into x
                                        select new
                                        {
                                            TotalGoalCount = x.Count(),
                                            TotalGoalBalance = x.Sum(o => o.GoalBalance)
                                        };
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Customer's target goal information was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Customer's target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetCompletedTotalTargetGoalAndBalanceByCustomer(long customerId)";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn GetTargetGoalForTransactionProcessing(DateTime dateTime)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string status = Enumerators.STATUS.PROCESSED.ToString();
                var targetGoals = targetGoalRepository.All.Where(o => o.NextRunDate == dateTime && o.GoalStatus == status).Include(o => o.CustomerProfile).Include(o => o.TargetSavingsFrequency).AsNoTracking();
                //List<TargetGoal> goals = allGoals.Where(o => o.NextRunDate == dateTime && o.GoalStatus == status).ToList();
                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                foreach (TargetGoal goal in targetGoals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        CustomerProfile profile = goal.CustomerProfile;
                        string hashed = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, profile);
                        if(hashed == profile.HashValue)
                        {
                            targetGoalList.Add(goal);
                        }                        
                    }
                }
                result.ReturnedObject = targetGoalList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Goals retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetGoalForTransactionProcessing(DateTime dateTime)";
            }
            return result;
        }

        public ActionReturn GetCompletedGoals()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                //get target goals due for debit in three days time
                //we put the items in dictionary so we can reference them thogether without creating a typed object.
                Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                int index = 1;

                DateTime dateTime = DateTime.Now.Date;
                string status = Enumerators.STATUS.PROCESSED.ToString();
                var allGoals = targetGoalRepository.All.Include(o => o.CustomerProfile).Include(o => o.TargetSavingsFrequency).AsNoTracking();
                List<TargetGoal> goals = allGoals.Where(o => o.GoalEndDate < dateTime && o.GoalStatus == status).ToList();

                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                foreach (TargetGoal goal in goals)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        CustomerProfile profile = goal.CustomerProfile;
                        string hashed = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, profile);
                        if (hashed == profile.HashValue)
                        {
                            //send email;
                            EmailLog emailLog = new EmailLog
                            {
                                EmailTo = goal.CustomerProfile.EmailAddress,
                                EmailType = "COMPLETED_CUSTOMER",
                                Status = Enumerators.STATUS.NEW.ToString(),
                            };

                            dictionaryEmailLog.Add(index, emailLog);

                            Dictionary<string, string> emailValues = new Dictionary<string, string>();
                            emailValues.Add("NAME", $"{goal.CustomerProfile.Surname} {goal.CustomerProfile.FirstName}");
                            emailValues.Add("AMOUNT", goal.TargetAmount.ToString("N"));
                            emailValues.Add("STARTDATE", Convert.ToDateTime(goal.GoalStartDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("ENDDATE", Convert.ToDateTime(goal.GoalEndDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("FREQUENCY", goal.TargetSavingsFrequency.Frequency);
                            emailValues.Add("PLANNAME", goal.GoalName);

                            dictionaryEmailValues.Add(index, emailValues);
                            index++;
                            goal.GoalStatus = Enumerators.STATUS.COMPLETED.ToString();
                        }
                    }
                }
                
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    scope.Complete();
                }

                if (dictionaryEmailLog.Count > 0)
                {
                    result.HasEmail = true;
                    result.EmailValues = dictionaryEmailValues;
                    result.EmailLog = dictionaryEmailLog;
                }
                result.ReturnedObject = goals;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Goals retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetCompletedGoals()";
            }
            return result;
        }

        public ActionReturn UpdateTargetPicture(TargetGoal targetGoal)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetGoal thisTargetGoals = targetGoalRepository.GetAll().Where(o=>o.TargetGoalsId==targetGoal.TargetGoalsId).FirstOrDefault();
                thisTargetGoals.TargetGoalPics = targetGoal.TargetGoalPics;
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.ReturnedObject = null;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Picture was updated successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Picture update failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "UpdateTargetPicture(TargetGoal targetGoal)";
            }
            return result;
        }

        public ActionReturn UpdateLiquidationDetails(long targetId, string account, string bankCode)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetGoal thisTargetGoals = targetGoalRepository.GetAll().Where(o => o.TargetGoalsId == targetId).FirstOrDefault();
                thisTargetGoals.LiquidationAccount = account;
                thisTargetGoals.LiquidationAccountBankCode = bankCode;
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.ReturnedObject = null;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Liquidation details were updated successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Liquidation detail update failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "UpdateLiquidationDetails(long targetId, string account, string bankCode)";
            }
            return result;
        }

        public ActionReturn AddTargetSavingsRequest(TargetSavingsRequest targetSavingsRequest)
        {
            ActionReturn result = new ActionReturn();
            //bool isRequestLogged = false;
            //bool isTransactionLogged = false;
            try
            {
                bool hasExistingLiquidationRequest = false;
                string pendingStatus = Enumerators.STATUS.PENDING.ToString();
                string approvedStatus = Enumerators.STATUS.PENDING.ToString();
                
                AppConfigSetting config= appConfigSettingRepository.GetAll().Where(o => o.IsActive == true && o.Key== "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").FirstOrDefault();
                TargetGoal goal = this.targetGoalRepository.Get(targetSavingsRequest.TargetGoalId);
                if (goal != null)
                {
                    TargetSavingsRequestType targetSavingsRequestType = targetSavingsRequestTypeRepository.Find(o => o.RequestTypeId == targetSavingsRequest.RequestTypeId).FirstOrDefault();
                    if (targetSavingsRequestType != null)
                    {
                        if (targetSavingsRequestType.IsLiquidation || targetSavingsRequestType.IsInterestLiquidation)
                        {
                             
                            targetSavingsRequest.TreatedBy = "SYSTEM";
                            targetSavingsRequest.Status = Enumerators.STATUS.PENDING.ToString();
                            targetSavingsRequest.ApprovalStatus = Enumerators.APPROVAL_STATUS.APPROVED.ToString();
                            targetSavingsRequest.DateTreated = DateTime.Now;

                            //TargetSavingsRequest existingRequest = targetSavingsRequestRepository.Find(o => o.Status == pendingStatus && o.TargetGoalId == targetSavingsRequest.TargetGoalId).FirstOrDefault();
                            //if (existingRequest != null)
                            //{
                            //    hasExistingLiquidationRequest = true;
                            //}
                            
                        }

                        //decimal contributedAmount = goal.TargetAmount - goal.GoalBalance;
                        //decimal interestRate = Convert.ToDecimal(config.Value) / 100;
                        //decimal penalChargeAmount = targetSavingsRequest.LiquidationAmount * interestRate;
                        //decimal totalForFixedGoal = contributedAmount + penalChargeAmount;
                        //decimal penalChargeAmountBalance = goal.GoalnterestEarned - penalChargeAmount;

                        decimal contributedAmount = goal.TargetAmount - goal.GoalBalance;
                        decimal interestRate = Convert.ToDecimal(config.Value) / 100;

                        decimal penalChargeAmount = goal.GoalnterestEarned * interestRate;                        
                        decimal penalChargeAmountBalance = goal.GoalnterestEarned - penalChargeAmount;
                        decimal totalForFixedGoal = contributedAmount + penalChargeAmountBalance;

                        //if (hasExistingLiquidationRequest)
                        //{
                        //    result.UpdateReturn = UpdateReturn.ItemCreated;
                        //    result.Message = "You have a pending liquidation request for this goal, kindly contact your administrator";
                        //    result.Flag = false;
                        //    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        //}

                        if (targetSavingsRequestType.IsLiquidation && goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE
                                && targetSavingsRequest.LiquidationAmount > contributedAmount)
                        {
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Requested amount is greater than your total contribution";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();

                        }
                        else if (targetSavingsRequestType.IsLiquidation && goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED
                                && targetSavingsRequest.LiquidationAmount > totalForFixedGoal)
                        {
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Requested amount is greater than your penal charge and total contribution";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }
                        else if (targetSavingsRequestType.IsInterestLiquidation && targetSavingsRequest.LiquidationAmount > goal.GoalnterestEarned)
                        {
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Requested amount is greater than your earned interest";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }
                        else
                        {
                            targetSavingsRequest.RequestDate = DateTime.Now.Date;
                            targetSavingsRequest.HashValue = Util.GetEncryptedHashValue(CONSTANT.REQUEST, targetSavingsRequest);                           


                            this.targetSavingsRequestRepository.Add(targetSavingsRequest);
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            //isRequestLogged = true;

                            
                            result.ReturnedObject = targetSavingsRequest.RequestId;
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Request created successfully";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }

                      
                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = "Could not create request with an invalid request type";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemCreated;
                    result.Message = "Could not create request. Goal details could not be retrieved.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Request was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
                //if(isRequestLogged && !isTransactionLogged)
                //{
                //    //remove request from db
                //    this.targetSavingsRequestRepository.Remove(targetSavingsRequest);
                //    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                //}
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddTargetSavingsRequest(TargetSavingsRequest targetSavingsRequest)";
            }
            return result;
        }

        public ActionReturn AddTargetSavingsRequest_new(TargetSavingsRequest targetSavingsRequest)
        {
            ActionReturn result = new ActionReturn();
            bool isRequestLogged = false;
            bool isTransactionLogged = false;
            try
            {
                //check if the user has pending liquidation requests and if the sum of request

                AppConfigSetting config = appConfigSettingRepository.GetAll().Where(o => o.IsActive == true && o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").FirstOrDefault();
                TargetGoal goal = this.targetGoalRepository.Find(o=>o.TargetGoalsId==targetSavingsRequest.TargetGoalId).FirstOrDefault();
                if (goal != null)
                {
                    
                    TargetSavingsRequestType targetSavingsRequestType = targetSavingsRequestTypeRepository.Find(o => o.RequestTypeId == targetSavingsRequest.RequestTypeId).FirstOrDefault();
                    if (targetSavingsRequestType != null)
                    {
                        if (targetSavingsRequestType.IsLiquidation || targetSavingsRequestType.IsInterestLiquidation)
                        {
                            targetSavingsRequest.TreatedBy = "SYSTEM";
                            targetSavingsRequest.Status = Enumerators.STATUS.NEW.ToString();
                            targetSavingsRequest.ApprovalStatus = Enumerators.APPROVAL_STATUS.NEW.ToString();
                            targetSavingsRequest.DateTreated = DateTime.Now;

                            
                            if (!goal.IsLiquidated)
                            {
                                targetSavingsRequest.RequestDate = DateTime.Now.Date;
                                targetSavingsRequest.HashValue = Util.GetEncryptedHashValue(CONSTANT.REQUEST, targetSavingsRequest);

                                this.targetSavingsRequestRepository.Add(targetSavingsRequest);
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                isRequestLogged = true;

                                //goal has not been fully liquidated
                                decimal contributedAmount = goal.TargetAmount - goal.GoalBalance;
                                DateTime today = DateTime.Now;
                                if(goal.GoalEndDate <= today || goal.GoalStatus==Enumerators.STATUS.COMPLETED.ToString())
                                {
                                    //goal is complete and customer wants his money.
                                    //Contributted Amount
                                    LiquidationTransaction contributedLiquidationTransaction = new LiquidationTransaction
                                    {
                                        CreatedDate = today.Date,
                                        CreatedDateTime = today,
                                        LastUpdatedDate = today.Date,
                                        LastUpdatedDateTime = today,
                                        Amount = contributedAmount,
                                        Narration1 = $"TSA FULL LIQUIDATION FOR {goal.GoalName}",
                                        Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                                        ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                        ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                                        RequestId = targetSavingsRequest.RequestId,
                                        Response = Enumerators.STATUS.NEW.ToString(),
                                        Status = Enumerators.STATUS.NEW.ToString(),
                                        StatusDescription = Enumerators.STATUS.NEW.ToString(),
                                        TransactionType = goal.LiquidationMode==(int)Enumerators.LIQUIDATION_MODE.FIXED
                                            ? Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FIXED_CONTRIBUTED.ToString(): Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FLEXIBLE_CONTRIBUTED.ToString(),
                                        TransactionTypeDescription = goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED
                                            ? Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FIXED_CONTRIBUTED.ToString() : Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FLEXIBLE_CONTRIBUTED.ToString(),
                                        Trial = 0,

                                    };
                                    contributedLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, contributedLiquidationTransaction);
                                    this.liquidationTransactionRepository.Add(contributedLiquidationTransaction);

                                    //Interest Earned
                                    LiquidationTransaction interestLiquidationTransaction = new LiquidationTransaction
                                    {
                                        CreatedDate = today.Date,
                                        CreatedDateTime = today,
                                        LastUpdatedDate = today.Date,
                                        LastUpdatedDateTime = today,
                                        Amount = goal.GoalnterestEarned,
                                        Narration1 = $"TSA FULL INTEREST LIQUIDATION FOR {goal.GoalName}",
                                        Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                                        ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                        ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                                        RequestId = targetSavingsRequest.RequestId,
                                        Response = Enumerators.STATUS.NEW.ToString(),
                                        Status = Enumerators.STATUS.NEW.ToString(),
                                        StatusDescription = Enumerators.STATUS.NEW.ToString(),
                                        TransactionType = goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED
                                            ? Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FIXED_INTEREST.ToString() : Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FLEXIBLE_INTEREST.ToString(),
                                        TransactionTypeDescription = goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED
                                            ? Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FIXED_INTEREST.ToString() : Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FLEXIBLE_INTEREST.ToString(),
                                        Trial = 0,

                                    };
                                    interestLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, interestLiquidationTransaction);
                                    this.liquidationTransactionRepository.Add(interestLiquidationTransaction);

                                }
                                else
                                {
                                    //goal is not complete but customer wants part or all his money
                                    if(targetSavingsRequest.LiquidationAmount < contributedAmount)
                                    {
                                        if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                                        {
                                            decimal interestRate = Convert.ToDecimal(config.Value) / 100;
                                            decimal penalChargeAmount = goal.GoalnterestEarned * interestRate;
                                            decimal penalChargeAmountBalance = goal.GoalnterestEarned - penalChargeAmount;

                                            //Penal Charge amount
                                            LiquidationTransaction fixedPenalChargeLiquidationTransaction = new LiquidationTransaction
                                            {
                                                CreatedDate = today.Date,
                                                CreatedDateTime = today,
                                                LastUpdatedDate = today.Date,
                                                LastUpdatedDateTime = today,
                                                Amount = penalChargeAmount,
                                                Narration1 = $"TSA LIQUIDATION PENAL CHARGE ON INTEREST FOR {goal.GoalName}",
                                                Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                                                ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                                ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                                                RequestId = targetSavingsRequest.RequestId,
                                                Response = Enumerators.STATUS.NEW.ToString(),
                                                Status = Enumerators.STATUS.NEW.ToString(),
                                                StatusDescription = Enumerators.STATUS.NEW.ToString(),
                                                TransactionType = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE.ToString(),
                                                TransactionTypeDescription = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE.ToString(),
                                                Trial = 0,

                                            };
                                            fixedPenalChargeLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, fixedPenalChargeLiquidationTransaction);
                                            this.liquidationTransactionRepository.Add(fixedPenalChargeLiquidationTransaction);
                                            
                                            //penal charge balance
                                            LiquidationTransaction fixedPenalChargeBalanceLiquidationTransaction = new LiquidationTransaction
                                            {
                                                CreatedDate = today.Date,
                                                CreatedDateTime = today,
                                                LastUpdatedDate = today.Date,
                                                LastUpdatedDateTime = today,
                                                Amount = penalChargeAmountBalance,
                                                Narration1 = $"TSA LIQUIDATION PENAL CHARGE BALANCE ON INTEREST FOR {goal.GoalName}",
                                                Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                                                ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                                ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                                                RequestId = targetSavingsRequest.RequestId,
                                                Response = Enumerators.STATUS.NEW.ToString(),
                                                Status = Enumerators.STATUS.NEW.ToString(),
                                                StatusDescription = Enumerators.STATUS.NEW.ToString(),
                                                TransactionType = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE_BALANCE.ToString(),
                                                TransactionTypeDescription = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE_BALANCE.ToString(),
                                                Trial = 0,

                                            };
                                            fixedPenalChargeBalanceLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, fixedPenalChargeBalanceLiquidationTransaction);

                                            this.liquidationTransactionRepository.Add(fixedPenalChargeBalanceLiquidationTransaction);

                                            //goal balance

                                            //penal charge balance
                                            LiquidationTransaction fixedGoalBalanceLiquidationTransaction = new LiquidationTransaction
                                            {
                                                CreatedDate = today.Date,
                                                CreatedDateTime = today,
                                                LastUpdatedDate = today.Date,
                                                LastUpdatedDateTime = today,
                                                Amount = contributedAmount- targetSavingsRequest.LiquidationAmount,
                                                Narration1 = $"TSA GOAL BALANCE SWEEP FOR {goal.GoalName}",
                                                Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                                                ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                                ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                                                RequestId = targetSavingsRequest.RequestId,
                                                Response = Enumerators.STATUS.NEW.ToString(),
                                                Status = Enumerators.STATUS.NEW.ToString(),
                                                StatusDescription = Enumerators.STATUS.NEW.ToString(),
                                                TransactionType = Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED_GOAL_BALANCE.ToString(),
                                                TransactionTypeDescription = Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED_GOAL_BALANCE.ToString(),
                                                Trial = 0,

                                            };
                                            fixedGoalBalanceLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, fixedGoalBalanceLiquidationTransaction);

                                            this.liquidationTransactionRepository.Add(fixedGoalBalanceLiquidationTransaction);
                                        }
                                        //requested amount
                                        LiquidationTransaction requestedLiquidationTransaction = new LiquidationTransaction
                                        {
                                            CreatedDate = today.Date,
                                            CreatedDateTime = today,
                                            LastUpdatedDate = today.Date,
                                            LastUpdatedDateTime = today,
                                            Amount = targetSavingsRequest.LiquidationAmount,
                                            Narration1 = $"TSA PARTIAL LIQUIDATION FOR {goal.GoalName}",
                                            Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                                            ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                            ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                                            RequestId = targetSavingsRequest.RequestId,
                                            Response = Enumerators.STATUS.NEW.ToString(),
                                            Status = Enumerators.STATUS.NEW.ToString(),
                                            StatusDescription = Enumerators.STATUS.NEW.ToString(),
                                            TransactionType = goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED
                                            ? Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED.ToString() : Enumerators.LIQUIDATION_TRANSACTION_MODE.FLEXIBLE.ToString(),
                                            TransactionTypeDescription = goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED
                                            ? Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED.ToString() : Enumerators.LIQUIDATION_TRANSACTION_MODE.FLEXIBLE.ToString(),
                                            Trial = 0,

                                        };
                                        requestedLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, requestedLiquidationTransaction);
                                        this.liquidationTransactionRepository.Add(requestedLiquidationTransaction);
                                    }
                                    else
                                    {
                                        result.UpdateReturn = UpdateReturn.ItemCreated;
                                        result.Message = "Requested amount is greater or equal to your total contribution";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    }
                                }
                                if (isRequestLogged)
                                {
                                    //update the status so it can be processed.
                                    TargetSavingsRequest thisRequest = targetSavingsRequestRepository.Find(o => o.RequestId == targetSavingsRequest.RequestId).FirstOrDefault();
                                    thisRequest.Status = Enumerators.STATUS.PENDING.ToString();
                                    thisRequest.ApprovalStatus = Enumerators.APPROVAL_STATUS.APPROVED.ToString();
                                }
                                using (TransactionScope scope = new TransactionScope())
                                {
                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                    scope.Complete();
                                    isTransactionLogged = true;

                                    result.ReturnedObject = targetSavingsRequest.RequestId;
                                    result.UpdateReturn = UpdateReturn.ItemCreated;
                                    result.Message = "Request created successfully";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                }
                            }
                            else
                            {
                                //goal has been fully liquidated
                                result.UpdateReturn = UpdateReturn.ItemCreated;
                                result.Message = "Could not create request with an invalid request type";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }
                        }
                        else
                        {
                            //not a liquidation request save

                            targetSavingsRequest.RequestDate = DateTime.Now.Date;
                            targetSavingsRequest.HashValue = Util.GetEncryptedHashValue(CONSTANT.REQUEST, targetSavingsRequest);

                            this.targetSavingsRequestRepository.Add(targetSavingsRequest);
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            isRequestLogged = true;

                            result.ReturnedObject = targetSavingsRequest.RequestId;
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Request created successfully";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }

                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = "Could not create request with an invalid request type";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemCreated;
                    result.Message = "Could not create request. Goal details could not be retrieved.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Request was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
                if (isRequestLogged && !isTransactionLogged)
                {
                    //remove request from db
                    TargetSavingsRequest thisRequest = targetSavingsRequestRepository.Find(o => o.RequestId == targetSavingsRequest.RequestId).FirstOrDefault();
                    this.targetSavingsRequestRepository.Remove(thisRequest);
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                }
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddTargetSavingsRequest(TargetSavingsRequest targetSavingsRequest)";
            }
            return result;
        }

 
        public async Task<ActionReturn> GetAllTargetSavingsRequestAsync()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                IEnumerable<TargetSavingsRequest> targetSavingsRequest = targetSavingsRequestRepository.All
                        .OrderByDescending(o => o.RequestId).Include(o=>o.RequestType).ToList();
                List<TargetSavingsRequest> requestList = new List<TargetSavingsRequest>();
                foreach(TargetSavingsRequest request in targetSavingsRequest)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.REQUEST, request);
                    if (hash == request.HashValue)
                    {
                        request.RequestType.TargetSavingsRequest = null;
                        requestList.Add(request);
                    }
                   
                }
                result.ReturnedObject = requestList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Request list was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Request list could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetAllTargetSavingsRequestAsync()";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetTargetSavingsRequestByTargetGoalAsync(long targetId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                IEnumerable<TargetSavingsRequest> targetSavingsRequest = targetSavingsRequestRepository.All
                    .Where(o => o.TargetGoalId == targetId)
                    .OrderByDescending(o => o.RequestId).Include(o => o.RequestType).ToList();
                List<TargetSavingsRequest> requestList = new List<TargetSavingsRequest>();
                foreach (TargetSavingsRequest request in targetSavingsRequest)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.REQUEST, request);
                    if (hash == request.HashValue)
                    {
                        request.RequestType.TargetSavingsRequest = null;
                        requestList.Add(request);
                    }

                }
                result.ReturnedObject = requestList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Request was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Request could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetSavingsRequestByTargetGoalAsync(long targetId)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetTargetSavingsRequestById(long requestId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                IEnumerable<TargetSavingsRequest> targetSavingsRequest = targetSavingsRequestRepository.All
                    .Where(o => o.RequestId == requestId).Include(o => o.RequestType).ToList();
                List<TargetSavingsRequest> requestList = new List<TargetSavingsRequest>();
                foreach (TargetSavingsRequest request in targetSavingsRequest)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.REQUEST, request);
                    if (hash == request.HashValue)
                    {
                        request.RequestType.TargetSavingsRequest = null;
                        requestList.Add(request);
                    }

                }
                result.ReturnedObject = requestList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Request was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Request could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetSavingsRequestById(long requestId)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetNCustomerRequests(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                var targetSavingsRequest = targetSavingsRequestRepository.All.OrderByDescending(o => o.RequestId).Include(o => o.RequestType).Include(o => o.TargetGoal.CustomerProfile).AsNoTracking();

                List<TargetSavingsRequest> requests = new List<TargetSavingsRequest>();
                foreach (TargetSavingsRequest request in targetSavingsRequest)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.REQUEST, request);
                    if (hash == request.HashValue)
                    {
                        request.RequestType.TargetSavingsRequest = null;
                        request.TargetGoal.CustomerProfile.TargetGoals = null;
                        request.TargetGoal.CustomerProfile.Password = null;
                        request.TargetGoal.Transactions = null;
                        request.TargetGoal.TargetSavingsRequests = null;
                       
                        requests.Add(request);
                    }

                }
                IEnumerable<TargetSavingsRequest> requestList = requests;
                var totalCount = requestList.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    requestList = requestList.OrderByDescending(o => o.RequestId).Where(x => x.RequestId < pagination.lastIdFetched);
                }
                else
                {
                    requestList = requestList.OrderByDescending(o => o.RequestId);
                }

                

                List<TargetSavingsRequest> items = requestList.OrderByDescending(o => o.RequestId)
                                        .Take(pagination.pageSize)
                                        .ToList();

                
                var pagedResult = new Paged<TargetSavingsRequest>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target Savings Request Retireived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target Savings Request Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNCustomerRequests(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }
        public async Task<ActionReturn> GetNRequestsByCustomerProfileId(Pagination pagination, long customerId)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                var targetSavingsRequest = targetSavingsRequestRepository.All
                    .Where(x => x.TargetGoal.CustomerProfileId == customerId)
                    .OrderByDescending(o => o.RequestId).Include(o=>o.TargetGoal).Include(o => o.RequestType).AsNoTracking();

                List<TargetSavingsRequest> requests = new List<TargetSavingsRequest>();
                foreach (TargetSavingsRequest request in targetSavingsRequest)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.REQUEST, request);
                    if (hash == request.HashValue)
                    {
                        request.TargetGoal.CustomerProfile = null;
                        //s.TargetGoal.CustomerProfile.Password = null;
                        request.TargetGoal.Transactions = null;
                        request.TargetGoal.TargetSavingsRequests = null;
                        request.RequestType.TargetSavingsRequest = null;

                        requests.Add(request);
                    }

                }
                IEnumerable<TargetSavingsRequest> requestList = requests;
                var totalCount = requestList.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    requestList = requestList.OrderByDescending(o => o.RequestId).Where(x => x.RequestId < pagination.lastIdFetched && x.TargetGoal.CustomerProfileId==customerId);
                }
                else
                {
                    requestList = requestList.OrderByDescending(o => o.RequestId).Where(x => x.TargetGoal.CustomerProfileId == customerId);
                }

                List<TargetSavingsRequest> items = requestList.OrderByDescending(o => o.RequestId)
                                        .Take(pagination.pageSize)
                                        .ToList();
                

                var pagedResult = new Paged<TargetSavingsRequest>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target Savings Request Retireived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target Savings Request Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNRequestsByCustomerProfileId(Pagination pagination, long customerId)";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn TreatCustomerRequests(TargetSavingsRequest targetSavingsRequest)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetSavingsRequest thisTargetSavingsRequest = targetSavingsRequestRepository.Find(o => o.RequestId == targetSavingsRequest.RequestId).FirstOrDefault();
                if (thisTargetSavingsRequest != null)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.REQUEST, thisTargetSavingsRequest);
                    if (hash == thisTargetSavingsRequest.HashValue)
                    {
                        thisTargetSavingsRequest.TreatedBy = targetSavingsRequest.TreatedBy;
                        thisTargetSavingsRequest.Status = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                        thisTargetSavingsRequest.ApprovalStatus = targetSavingsRequest.ApprovalStatus;
                        thisTargetSavingsRequest.DateTreated = targetSavingsRequest.DateTreated;
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                        result.ReturnedObject = null;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Target Savings Request was treated successfully";
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        result.ReturnedObject = null;
                        result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                        result.Message = "Target Savings Request was not treated";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                    
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                    result.Message = "Target Savings Request was not treated";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:request was not treated";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "TreatCustomerRequests(TargetSavingsRequest targetSavingsRequest)";
            }
            return result;
        }

        public async Task<ActionReturn> GetTargetSavingsRequestCountByStatus(string status)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = targetSavingsRequestRepository.All.Where(o=>o.Status==status).Count();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Request count was retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Request count failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetSavingsRequestCountByStatus(string status)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetNRecentTargetSavingsRequest(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                var targetSavingsRequests = targetSavingsRequestRepository.All.Include(o => o.RequestType).AsNoTracking();

                string status = Enumerators.STATUS.PENDING.ToString();


                //if (pagination.lastIdFetched > 0)
                //{
                //    requests = requests.OrderByDescending(o => o.RequestId)
                //        .Where(x => x.RequestId < pagination.lastIdFetched && string.IsNullOrEmpty(x.TreatedBy) && x.TargetGoal.GoalStatus == status)
                //        .Include(o => o.TargetGoal).Include(o => o.TargetGoal.CustomerProfile);
                //}
                //else
                //{
                //    requests = requests.OrderByDescending(o => o.RequestId)
                //        .Where(x => string.IsNullOrEmpty(x.TreatedBy) && x.TargetGoal.GoalStatus == status)
                //        .Include(o => o.TargetGoal).Include(o => o.TargetGoal.CustomerProfile);
                //}
                //var totalCount = requests.Count();
                //var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                targetSavingsRequests = targetSavingsRequests.OrderByDescending(o => o.RequestId)
                        .Where(x => string.IsNullOrEmpty(x.TreatedBy) && x.TargetGoal.GoalStatus == status)
                        .Include(o => o.TargetGoal).Include(o => o.TargetGoal.CustomerProfile);

                List<TargetSavingsRequest> requests = new List<TargetSavingsRequest>();
                foreach (TargetSavingsRequest request in targetSavingsRequests)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.REQUEST, request);
                    if (hash == request.HashValue)
                    {
                        requests.Add(request);
                    }

                }
                IEnumerable<TargetSavingsRequest> requestList = requests;
                var totalCount = requests.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    requestList = requestList.OrderByDescending(o => o.RequestId)
                        .Where(x => x.RequestId < pagination.lastIdFetched);
                }
                else
                {
                    //requests = requests;
                }
                

                List<RecentTargetSavingsRequest> items = (from t in requestList
                                                                select new RecentTargetSavingsRequest
                                                       {
                                                           RequestId=t.RequestId,
                                                           RequestDate=t.RequestDate,
                                                           RequestType=t.RequestType.RequestType,
                                                           CustomerName = t.TargetGoal.CustomerProfile.Surname + " " + t.TargetGoal.CustomerProfile.FirstName,
                                                       }).Take(pagination.pageSize).ToList();

                var pagedResult = new Paged<RecentTargetSavingsRequest>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target Savings Request Retireived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target Savings Request Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNRecentTargetSavingsRequest(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetNTreatedTargetSavingsRequest(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string status = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                var targetSavingsRequest = targetSavingsRequestRepository.All
                    .Where(x => x.Status == status).OrderByDescending(o => o.RequestId).Include(o => o.RequestType).Include(o => o.TargetGoal.CustomerProfile).AsNoTracking();

                

                List<TargetSavingsRequest> requests = new List<TargetSavingsRequest>();
                foreach (TargetSavingsRequest request in targetSavingsRequest)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.REQUEST, request);
                    if (hash == request.HashValue)
                    {
                        request.TargetGoal.CustomerProfile.TargetGoals = null;
                        request.TargetGoal.CustomerProfile.Password = null;
                        request.TargetGoal.Transactions = null;
                        request.TargetGoal.TargetSavingsRequests = null;
                        request.RequestType.TargetSavingsRequest = null;
                        requests.Add(request);
                    }

                }
                IEnumerable<TargetSavingsRequest> requestList = requests;
                var totalCount = requestList.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);
                
                if (pagination.lastIdFetched > 0)
                {
                    requestList = requestList.OrderByDescending(o => o.RequestId).Where(x => x.RequestId < pagination.lastIdFetched && x.Status == status);
                }
                else
                {
                    requestList = requestList.OrderByDescending(o => o.RequestId).Where(x => x.Status == status);
                }

                List<TargetSavingsRequest> items = requestList.OrderByDescending(o => o.RequestId)
                                        .Take(pagination.pageSize)
                                        .ToList();

              

                var pagedResult = new Paged<TargetSavingsRequest>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target Savings Request Retireived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target Savings Request Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNTreatedTargetSavingsRequest(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetNNewTargetSavingsRequest(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string status = Enumerators.STATUS.PENDING.ToString();
                var targetSavingsRequest = targetSavingsRequestRepository.All.Where(x => x.Status == status).OrderByDescending(o => o.RequestId).Include(o => o.RequestType).Include(o => o.TargetGoal.CustomerProfile).AsNoTracking();

                List<TargetSavingsRequest> requests = new List<TargetSavingsRequest>();
                foreach (TargetSavingsRequest request in targetSavingsRequest)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.REQUEST, request);
                    if (hash == request.HashValue)
                    {
                        request.TargetGoal.CustomerProfile.TargetGoals = null;
                        request.TargetGoal.CustomerProfile.Password = null;
                        request.TargetGoal.Transactions = null;
                        request.TargetGoal.TargetSavingsRequests = null;
                        request.RequestType.TargetSavingsRequest = null;
                        requests.Add(request);
                    }

                }
                IEnumerable<TargetSavingsRequest> requestList = requests;
                var totalCount = requestList.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);
                
                if (pagination.lastIdFetched > 0)
                {
                    requestList = requestList.OrderByDescending(o => o.RequestId).Where(x => x.RequestId < pagination.lastIdFetched && x.Status == status);
                }
                else
                {
                    requestList = requestList.OrderByDescending(o => o.RequestId).Where(x => x.Status == status);
                }

                List<TargetSavingsRequest> items = requestList.OrderByDescending(o => o.RequestId)
                                        .Take(pagination.pageSize)
                                        .ToList();

                
                var pagedResult = new Paged<TargetSavingsRequest>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Target Savings Request Retireived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target Savings Request Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNNewTargetSavingsRequest(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn ValidateUser(string username,string password)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                byte[] encryptedPassword = Encoding.UTF8.GetBytes(password);
                CustomerProfile thisCustomerProfile = customerProfileRepository.Find(o => o.UserName == username && o.Password== encryptedPassword).FirstOrDefault();
                if (thisCustomerProfile != null)
                {
                    thisCustomerProfile.LastLoginDate = DateTime.Now;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    result.ReturnedObject = thisCustomerProfile;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "User authenticated successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "Invalid credentials/wrong password";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Error authenticating user";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "ValidateUser(string username,string password)";
            }
            return result;
        }

        public ActionReturn ValidateProspectData(string email, string phoneNumber)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                List<ProspectiveCustomer> thisProspects = prospectiveCustomerRepository.Find(o => o.EmailAddress==email || o.PhoneNumber==phoneNumber).ToList();
                if (thisProspects != null && thisProspects.Count>0)
                {
                    List<string> errorList = new List<string>();
                    foreach(ProspectiveCustomer p in thisProspects)
                    {
                        if (p.EmailAddress == email)
                        {
                            errorList.Add($"Email: {email} is already registered by another user.");
                            break;
                        }
                    }
                    foreach (ProspectiveCustomer p in thisProspects)
                    {
                        if (p.PhoneNumber == phoneNumber)
                        {
                            errorList.Add($"Phone Number: {phoneNumber} is already registered by another user.");
                            break;
                        }
                    }

                    result.ReturnedObject = errorList;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "User email or phone already exist in the system";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "User data is valid";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception occured";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "ValidateProspectData(string email, string phoneNumber)";
            }
            return result;
        }

        public ActionReturn ValidateNewCustomerData(string email, string phoneNumber,string username)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                List<CustomerProfile> thisCustomers = customerProfileRepository.Find(o => o.EmailAddress == email || o.PhoneNumber == phoneNumber || o.UserName==username).ToList();
                if (thisCustomers != null && thisCustomers.Count > 0)
                {
                    List<string> errorList = new List<string>();
                    foreach (CustomerProfile p in thisCustomers)
                    {
                        if (p.EmailAddress == email)
                        {
                            errorList.Add($"Email: {email} is already registered by another user.");
                            break;
                        }
                    }
                    foreach (CustomerProfile p in thisCustomers)
                    {
                        if (p.PhoneNumber == phoneNumber)
                        {
                            errorList.Add($"Phone Number: {phoneNumber} is already registered by another user.");
                            break;
                        }
                    }
                    foreach (CustomerProfile p in thisCustomers)
                    {
                        if (p.UserName == username)
                        {
                            errorList.Add($"Username: {username} is already registered by another user.");
                            break;
                        }
                    }

                    result.ReturnedObject = errorList;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "User username, email or phone already exist in the system";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "User data is valid";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception occured";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "ValidateNewCustomerData(string email, string phoneNumber,string username)";
            }
            return result;
        }

        public ActionReturn ResetPassword(string username)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                CustomerProfile thisCustomerProfile = customerProfileRepository.Find(o => o.UserName == username).FirstOrDefault();
                if (thisCustomerProfile != null)
                {
                    //generate new password
                    string newPassword = new PasswordGenerator().GeneratePassword();
                    //update password field
                    
                    byte[] encryptedPassword = Encoding.UTF8.GetBytes(newPassword);
                    string base64Password = Convert.ToBase64String(encryptedPassword);
                    thisCustomerProfile.Password = Encoding.UTF8.GetBytes(base64Password);
                    //thisCustomerProfile.Password = encryptedPassword;
                    //get email template
                    //send email to customer
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = thisCustomerProfile.EmailAddress,
                        EmailType = "RESET_PASSWORD",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };
                    result.EmailLog = emailLog;

                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                    emailValues.Add("PASSWORD", $"{newPassword}");
                    emailValues.Add("NAME", $"{thisCustomerProfile.Surname} {thisCustomerProfile.FirstName}");

                    result.EmailValues = emailValues;
                    
                    //append generated password
                    //send email to user
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    result.HasEmail = true;
                    result.ReturnedObject = null; 
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "User password was reset successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "User does not exist";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Error resetting user password";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "ResetPassword(string username)";
            }
            return result;
        }

        public ActionReturn ChangePassword(long customerProfileId,string oldPassword,string newPassword)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                byte[] encryptedPassword = Encoding.UTF8.GetBytes(oldPassword);
                CustomerProfile thisCustomerProfile = customerProfileRepository.Find(o => o.CustomerProfileId == customerProfileId && o.Password == encryptedPassword).FirstOrDefault();
                if (thisCustomerProfile != null)
                {
                    //update password field
                    byte[] newEncryptedPassword = Encoding.UTF8.GetBytes(newPassword);
                    thisCustomerProfile.Password = newEncryptedPassword;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "User password was changed successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "User does not exist";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Error changing user password";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "ChangePassword(long customerProfileId,string oldPassword,string newPassword)";
            }
            return result;
        }

        public async Task<ActionReturn> GetDailyStatistics(DateTime dateTime)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                DateTime now = DateTime.Now;
                var prospectiveCustomers = prospectiveCustomerRepository.GetAll();
                var customers = customerProfileRepository.GetAll();
                var goals = targetGoalRepository.GetAll();

                decimal totalProspectiveCount = prospectiveCustomers.Count();
                decimal totalCustomerCount = customers.Count();
                decimal totalGoalCount = goals.Count();

                decimal totalProspectiveCountForToday = prospectiveCustomers.Where(o => o.DateCreated >= dateTime).Count();
                decimal totalCustomerCountForToday = customers.Where(o => o.DateCreated >= dateTime).Count();
                decimal totalGoalCountForToday = goals.Where(o => o.SetUpDate >= dateTime).Count();

                List<DailyStatistic> statistics = new List<DailyStatistic>();
                decimal totalProspectivePercentageForToday = 0;
                if (totalProspectiveCountForToday > 0)
                {
                    totalProspectivePercentageForToday = (totalProspectiveCountForToday / totalProspectiveCount) * 100;
                }
                statistics.Add(new DailyStatistic
                {
                    Name= "Prospect",
                    DailyCount = Convert.ToInt32(totalProspectiveCountForToday),
                    DailyCountPercentage = decimal.Round(totalProspectivePercentageForToday,2),
                    StatisticsDateTime=now,
                    TotalCount= Convert.ToInt32(totalProspectiveCount)
                });

                decimal totalCustomerPercentageForToday = 0;
                if (totalCustomerCountForToday > 0)
                {
                    totalCustomerPercentageForToday = (totalCustomerCountForToday / totalCustomerCount) * 100;
                }
                statistics.Add(new DailyStatistic
                {
                    Name = "Customer",
                    DailyCount = Convert.ToInt32(totalCustomerCountForToday),
                    DailyCountPercentage = decimal.Round(totalCustomerPercentageForToday,2),
                    StatisticsDateTime = now,
                    TotalCount = Convert.ToInt32(totalCustomerCount)
                });

                decimal totalGoalPercentageForToday = 0;
                if (totalGoalCountForToday > 0)
                {
                    totalGoalPercentageForToday = (totalGoalCountForToday / totalGoalCount) * 100;
                }
                statistics.Add(new DailyStatistic
                {
                    Name = "Goal",
                    DailyCount = Convert.ToInt32(totalGoalCountForToday),
                    DailyCountPercentage = decimal.Round(totalGoalPercentageForToday,2),
                    StatisticsDateTime = now,
                    TotalCount = Convert.ToInt32(totalGoalCount)
                });

                result.ReturnedObject = statistics;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Daily Statistics Retireived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Daily Statistics Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetDailyStatistics(DateTime dateTime)";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn ValidateBVN(string bvn,string firstName, string lastName, string phone)
        {
            ActionReturn result = new ActionReturn();
            BvnValidationResponseObject bvnMatchResult = new BvnValidationResponseObject();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();                
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    ActionReturn bvnValidatorResult = FCMBIntegrator.BvnValidation(bvn, settings);
                    if (bvnValidatorResult.Flag)
                    {
                        
                        FCMBResponse response = (FCMBResponse)bvnValidatorResult.ReturnedObject;
                        BvnValidationResponse dataResponse = JsonConvert.DeserializeObject<BvnValidationResponse>(Convert.ToString(response.ResponseData));
                        bvnMatchResult.BVN = dataResponse.Data.BVN.BVN;
                        if (dataResponse.Data.BVN.FirstName.Trim().ToLower() == firstName.Trim().ToLower())
                        {
                            bvnMatchResult.FirstNameMatch = true;
                        }
                        if (dataResponse.Data.BVN.LastName.Trim().ToLower() == lastName.Trim().ToLower())
                        {
                            bvnMatchResult.LastNameMatch = true;
                        }
                        if (dataResponse.Data.BVN.PhoneNumber1 == phone || dataResponse.Data.BVN.PhoneNumber2 == phone)
                        {
                            bvnMatchResult.PhoneNumberMatch = true;
                        }
                        bvnMatchResult.Details = dataResponse;
                        result.ReturnedObject = bvnMatchResult;
                        //result.ReturnedObject = dataResponse;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = dataResponse.Message;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        result.ReturnedObject = bvnValidatorResult.ReturnedObject;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = bvnValidatorResult.Message;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    //result.ReturnedObject = configs.ReturnedObject;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = configs.Message;
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                //bvnMatchResult.BVN = bvn;
                //bvnMatchResult.FirstNameMatch = true;
                //bvnMatchResult.LastNameMatch = true;
                //bvnMatchResult.PhoneNumberMatch = true;
                
                //result.ReturnedObject = bvnMatchResult;
                ////result.ReturnedObject = dataResponse;
                //result.UpdateReturn = UpdateReturn.ItemRetrieved;
                //result.Message = "Success";
                //result.Flag = true;
                //result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Error validating BVN ";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "ValidateBVN(string bvn,string firstName, string lastName, string phone)";
            }
            return result;

        }

        public ActionReturn NameEnquiry(string accountNumber, string bankCode)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(accountNumber,bankCode, settings);
                    if (nameEnquiryResult.Flag)
                    {
                        FCMBResponse response = (FCMBResponse)nameEnquiryResult.ReturnedObject;
                        NameEnquiryResponse dataResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(response.ResponseData));
                        result.ReturnedObject = dataResponse;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = nameEnquiryResult.Message;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        result.ReturnedObject = nameEnquiryResult.ReturnedObject;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = nameEnquiryResult.Message;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    result.ReturnedObject = configs.ReturnedObject;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = configs.Message;
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Error getting account information";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "NameEnquiry(string accountNumber, string bankCode)";
            }
            return result;

        }

        public ActionReturn GetCustomerInformationByAccountNo(string accountNo)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    ActionReturn customerEnquiryResult = FCMBIntegrator.GetCustomerInformationByAccountNo(accountNo,settings);
                    if (customerEnquiryResult.Flag)
                    {
                        //FCMBResponse response = (FCMBResponse)customerEnquiryResult.ReturnedObject;
                        //NameEnquiryResponse dataResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(response.ResponseData));

                        result.ReturnedObject = customerEnquiryResult.ReturnedObject;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = customerEnquiryResult.Message;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        result.ReturnedObject = customerEnquiryResult.ReturnedObject;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = customerEnquiryResult.Message;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    result.ReturnedObject = configs.ReturnedObject;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = configs.Message;
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Error getting customer information";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetCustomerInformationByAccountNo(string accountNo)";
            }
            return result;
        }

        public ActionReturn AddCompletedLiquidationRequest(string requestType,long targetGoalId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                if (requestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_INTEREST.ToString()
                    || requestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                {
                    string completedGoalStatus = Enumerators.STATUS.COMPLETED.ToString();
                    string processedGoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                    DateTime now = DateTime.Now.Date;
                    TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == targetGoalId && o.GoalEndDate <= now && ( o.GoalStatus == completedGoalStatus || o.GoalStatus==processedGoalStatus)).FirstOrDefault();
                    if (goal != null && !goal.IsLiquidated)
                    {
                        
                        //check if user has pending request with same request type
                        string pendingStatus = Enumerators.STATUS.NEW.ToString();
                        LiquidationServiceRequest pendingLiquidationRequest = this.liquidationServiceRequestRepository.Find(o =>o.TargetId==targetGoalId && o.Status == pendingStatus).FirstOrDefault();
                        if (pendingLiquidationRequest == null)
                        {
                            decimal amount = 0;
                            if (requestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_INTEREST.ToString())
                            {
                                amount = goal.GoalnterestEarned;
                            }
                            else if (requestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                            {
                                amount = (goal.TargetAmount - goal.GoalBalance) + goal.GoalnterestEarned;
                            }
                            LiquidationServiceRequest liquidationServiceRequest = new LiquidationServiceRequest
                            {
                                Amount = amount,
                                Description = requestType,
                                ProcessingResponse = Enumerators.STATUS.NEW.ToString(),
                                ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                RequestDate = DateTime.Now.Date,
                                RequestDateTime = DateTime.Now,
                                RequestType = requestType,
                                Status = Enumerators.STATUS.NEW.ToString(),
                                TargetId = targetGoalId,
                                Trial = 0,

                            };
                            liquidationServiceRequest.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_REQUEST, liquidationServiceRequest);
                            this.liquidationServiceRequestRepository.Add(liquidationServiceRequest);
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                            result.ReturnedObject = null;
                            result.UpdateReturn = UpdateReturn.ItemRetrieved;
                            result.Message = $"Your liquidation request was logged successfully. Amount payable is N{amount.ToString("N")}";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            result.ReturnedObject = null;
                            result.UpdateReturn = UpdateReturn.ItemRetrieved;
                            result.Message = "Your liquidation request was not logged, you have a pending request in the system. Please contact your support team.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }
                    }
                    else
                    {
                        result.ReturnedObject = null;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Your liquidation request was not logged, could not find a reference to your goal or you have fully liquidated before. Please contact your support team.";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Your liquidation request was not logged, requested goal must be complete. Please contact your support team.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
                
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Error processing liquidation request";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddCompletedLiquidationRequest(string requestType,long targetGoalId)";
            }
            return result;
        }

        public ActionReturn AddNonCompletedLiquidationRequest(string requestType, long targetGoalId,decimal amount)
        {
            ActionReturn result = new ActionReturn();
            bool canLiquidate = false;
            try
            {
                AppConfigSetting config = appConfigSettingRepository.GetAll().Where(o => o.IsActive == true && o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").FirstOrDefault();
                if(config != null)
                {
                    if (requestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString()
                    || requestType == Enumerators.LIQUIDATION_REQUEST_TYPE.PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                    {
                        string runningGoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                        DateTime now = DateTime.Now.Date;
                        TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == targetGoalId && o.GoalStatus == runningGoalStatus &&  o.GoalEndDate > now ).FirstOrDefault();
                        if (goal != null && !goal.IsLiquidated)
                        {
                            //check if user has pending request with same request type
                            string pendingStatus = Enumerators.STATUS.NEW.ToString();
                            LiquidationServiceRequest pendingLiquidationRequest = this.liquidationServiceRequestRepository.Find(o =>o.TargetId==targetGoalId && o.Status == pendingStatus).FirstOrDefault();
                            if (pendingLiquidationRequest == null)
                            {
                                decimal contributedAmount = goal.TargetAmount - goal.GoalBalance;

                                decimal interestRate = Convert.ToDecimal(config.Value) / 100;
                                decimal penalChargeAmount = goal.GoalnterestEarned * interestRate;
                                decimal penalChargeAmountBalance = goal.GoalnterestEarned - penalChargeAmount;

                                decimal totalForFixedGoal = contributedAmount + penalChargeAmountBalance;
                                decimal totalForFlexibleGoal = contributedAmount + goal.GoalnterestEarned;

                                if (requestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                                {
                                    if(goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE)
                                    {
                                        amount = totalForFlexibleGoal;
                                    }
                                    else if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                                    {
                                        amount = totalForFixedGoal;
                                    }
                                    canLiquidate = true;
                                }
                                else if (requestType == Enumerators.LIQUIDATION_REQUEST_TYPE.PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                                {
                                    //if goal is fixed penal charge will be applied on interest
                                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE && amount < totalForFlexibleGoal)
                                    {
                                        canLiquidate = true;
                                    }
                                    else if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED && amount < totalForFixedGoal)
                                    {
                                        canLiquidate = true;
                                    }
                                    else
                                    {
                                        canLiquidate = false;
                                  
                                    }
                                    
                                }
                                if (canLiquidate)
                                {
                                    LiquidationServiceRequest liquidationServiceRequest = new LiquidationServiceRequest
                                    {
                                        Amount = amount,
                                        Description = requestType,
                                        ProcessingResponse = Enumerators.STATUS.NEW.ToString(),
                                        ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                        RequestDate = DateTime.Now.Date,
                                        RequestDateTime = DateTime.Now,
                                        RequestType = requestType,
                                        Status = Enumerators.STATUS.NEW.ToString(),
                                        TargetId = targetGoalId,
                                        Trial = 0,

                                    };

                                    liquidationServiceRequest.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_REQUEST, liquidationServiceRequest);
                                    this.liquidationServiceRequestRepository.Add(liquidationServiceRequest);
                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                    result.ReturnedObject = null;
                                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                    result.Message = $"Your liquidation request was logged successfully. Amount payable is N{amount.ToString("N")}";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                }
                                else
                                {
                                    decimal accessableAmount = goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE ? totalForFlexibleGoal : totalForFixedGoal;
                                    result.ReturnedObject = null;
                                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                    result.Message = $"Your liquidation request was not logged, you only have access to amount lower than N{accessableAmount.ToString("N")}";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                }
                                
                            }
                            else
                            {
                                result.ReturnedObject = null;
                                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                result.Message = "Your liquidation request was not logged, you have a pending request in the system. Please contact your support team.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }
                        }
                        else
                        {
                            result.ReturnedObject = null;
                            result.UpdateReturn = UpdateReturn.ItemRetrieved;
                            result.Message = "Your liquidation request was not logged, could not find a reference to your goal or you have fully liquidated before. Please contact your support team.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }
                       
                    }
                    else
                    {
                        result.ReturnedObject = null;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Your liquidation request was not logged, requested goal must be complete. Please contact your support team.";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Your liquidation request was not logged. Please contact your support team.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Error processing liquidation request";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "AddNonCompletedLiquidationRequest(string requestType, long targetGoalId,decimal amount)";
            }
            return result;
        }

        public async Task<ActionReturn> GetNLiquidationRequestsByCustomerProfileId(Pagination pagination, long customerId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                List<TargetGoal> goals = targetGoalRepository.All.Where(o => o.CustomerProfileId == customerId).ToList();
                if(goals.Count > 0)
                {
                    List<long> targetGoalIds = goals.Select(o => o.TargetGoalsId).ToList();
                    List<LiquidationServiceRequest> liquidationServiceRequests = 
                        liquidationServiceRequestRepository.All.Where(o => targetGoalIds.Contains(o.TargetId)).ToList();

                    IEnumerable<LiquidationServiceRequest> requestList = liquidationServiceRequests;
                    var totalCount = requestList.Count();
                    var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                    if (pagination.lastIdFetched > 0)
                    {
                        requestList = requestList.OrderByDescending(o => o.Id).Where(x => x.Id < pagination.lastIdFetched );
                    }
                    else
                    {
                        requestList = requestList.OrderByDescending(o => o.Id);
                    }

                    List<LiquidationServiceRequest> items = requestList.OrderByDescending(o => o.Id)
                                            .Take(pagination.pageSize)
                                            .ToList();


                    var pagedResult = new Paged<LiquidationServiceRequest>
                    {
                        TotalItemsCount = totalCount,
                        TotalNumberOfPages = totalPages,
                        Items = items,
                        PageSize = pagination.pageSize,
                        PageNumber = pagination.pageNumber,
                        StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                        Keyword = ""
                    };

                    result.ReturnedObject = pagedResult;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Target Savings Liquidation Request Retireived";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
               
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target Savings Liquidation Request Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetNRequestsByCustomerProfileId(Pagination pagination, long customerId)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> UpdateTargetGoalExtentedStatus(long targetGoalId, string status)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetGoal thisTargetGoals = targetGoalRepository.GetAll().Where(o => o.TargetGoalsId == targetGoalId).FirstOrDefault();
                thisTargetGoals.ExtendedGoalStatus = status;
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.ReturnedObject = null;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Goal was updated successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal update failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "UpdateTargetGoalExtentedStatus";
            }
            return await Task.FromResult(result);
        }
    }
}
