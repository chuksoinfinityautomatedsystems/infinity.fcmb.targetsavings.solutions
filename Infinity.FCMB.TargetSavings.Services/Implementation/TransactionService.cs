﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.EntityFramework.Extensions.Repository;
using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using Infinity.FCMB.TargetSavings.Domain.Models.FCMB;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.TargetSavings.Domain.Models.Paystack;
using Infinity.FCMB.TargetSavings.Domain.Models.Remita;
using Infinity.FCMB.TargetSavings.Services.Helper;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Services.Implementation
{
    public class TransactionService : TargetSavingsServiceBase, ITransactionService
    {
        private IRepository<Transaction> transactionRepository
        {
            get;
            set;
        }
        private IRepository<TargetDebitAccountProfile> targetDebitAccontProfileRepository { get; set; }
        private IRepository<AppConfigSetting> appConfigSettingRepository { get; set; }
        private IRepository<TargetGoal> targetGoalRepository { get; set; }
        private IRepository<TransactionSchedule> transactionScheduleRepository { get; set; }
        private IRepository<TargetSavingsRequest> targetSavingsRequestRepository
        {
            get;
            set;
        }
        private IRepository<TargetSavingsRequestType> targetSavingsRequestTypeRepository { get; set; }
        private IRepository<TransactionInterest> transactionInterestRepository { get; set; }
        private IRepository<PaymentSweepItem> paymentSweepItemRepository { get; set; }
        private IRepository<CardPaymentChargedFeeSweepItem> cardPaymentChargedFeeSweepItemRepository { get; set; }
        private IRepository<LiquidationTransaction> liquidationTransactionRepository { get; set; }
        private IRepository<LiquidationServiceRequest> liquidationServiceRequestRepository { get; set; }

        //private ICustomerService customerService { get; set; }
        //private IUtilityService utilityService { get; set; }

        private IUnitOfWork unitOfWork
        {
            get;
            set;
        }

        public TransactionService(IUnitOfWork unitOfWork) : base(null)
        {
            unitOfWork.Database.CommandTimeout = new int?(20000);
            this.TargetSavingsUnitOfWorkUnitOfWork = unitOfWork;
            this.transactionRepository = new TargetSavingsRepositoryBase<Transaction>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetDebitAccontProfileRepository = new TargetSavingsRepositoryBase<TargetDebitAccountProfile>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.appConfigSettingRepository = new TargetSavingsRepositoryBase<AppConfigSetting>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetGoalRepository = new TargetSavingsRepositoryBase<TargetGoal>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.transactionScheduleRepository = new TargetSavingsRepositoryBase<TransactionSchedule>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetSavingsRequestRepository = new TargetSavingsRepositoryBase<TargetSavingsRequest>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetSavingsRequestTypeRepository = new TargetSavingsRepositoryBase<TargetSavingsRequestType>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.transactionInterestRepository = new TargetSavingsRepositoryBase<TransactionInterest>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.paymentSweepItemRepository = new TargetSavingsRepositoryBase<PaymentSweepItem>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.liquidationTransactionRepository = new TargetSavingsRepositoryBase<LiquidationTransaction>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.liquidationServiceRequestRepository = new TargetSavingsRepositoryBase<LiquidationServiceRequest>(this.TargetSavingsUnitOfWorkUnitOfWork);

            //this.customerService = customerService;
            //this.utilityService = utilityService;
        }

        #region Dispose
        protected override void Dispose(bool disposing)
        {
            transactionRepository?.Dispose();
            transactionInterestRepository?.Dispose();
            liquidationTransactionRepository?.Dispose();
            liquidationServiceRequestRepository?.Dispose();
            base.Dispose(disposing);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public ActionReturn AddTransaction(Transaction transaction)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                transaction.TransactionType = transaction.TransactionType.ToUpper();
                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                this.transactionRepository.Add(transaction);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Transaction created";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Transaction was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "AddTransaction(Transaction transaction)";
            }
            return result;
        }

        public async Task<ActionReturn> GetNTransactionByCustomerProfileId(Pagination pagination, long customerId)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                var activeTransactions = transactionRepository.All.Where(x => x.TargetGoal.CustomerProfileId == customerId && x.Amount >0).OrderByDescending(o => o.TransactionId).Include(o => o.TargetGoal).Include(o => o.TargetGoal.CustomerProfile).AsNoTracking();
                List<Transaction> transactions = new List<Transaction>();
                foreach (Transaction transaction in activeTransactions)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                    if (hash == transaction.HashValue)
                    {
                        transaction.TargetGoal.CustomerProfile.TargetGoals = null;
                        transaction.TargetGoal.CustomerProfile.Password = null;
                        transaction.TargetGoal.Transactions = null;
                        transaction.TargetGoal.TargetSavingsRequests = null;
                        transactions.Add(transaction);
                    }

                }
                IEnumerable<Transaction> transactionList = transactions;
                var totalCount = transactions.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    transactionList = transactionList.OrderByDescending(o => o.TransactionId).Where(x => x.TransactionId < pagination.lastIdFetched && x.TargetGoal.CustomerProfileId == customerId);
                }
                else
                {
                    transactionList = transactionList.OrderByDescending(o => o.TransactionId).Where(x => x.TargetGoal.CustomerProfileId == customerId);
                }
                if (pagination.FilterByDateRange)
                {
                    transactionList = transactionList.Where(x => x.TransactionDate >= pagination.StartDate && x.TransactionDate <= pagination.EndDate );
                }
                List<Transaction> items = transactionList.OrderByDescending(o => o.TransactionId)
                                        .Take(pagination.pageSize)
                                        .ToList();

                var pagedResult = new Paged<Transaction>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Transaction list was retireived successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Transaction list Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "GetNTransactionByCustomerProfileId(Pagination pagination, long customerId)";
            }
            return await Task.FromResult(result);
        }


        public async Task<ActionReturn> GetNTransactionByTargetId(Pagination pagination, long id)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                var activeTransactions = transactionRepository.All.Where(x => x.TargetGoalsId == id && x.Amount >0).OrderByDescending(o => o.TransactionId).Include(o => o.TargetGoal).Include(o => o.TargetGoal.CustomerProfile).AsNoTracking();
                List<Transaction> transactions = new List<Transaction>();
                foreach (Transaction transaction in activeTransactions)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                    if (hash == transaction.HashValue)
                    {
                        transaction.TargetGoal.CustomerProfile.TargetGoals = null;
                        transaction.TargetGoal.CustomerProfile.Password = null;
                        transaction.TargetGoal.Transactions = null;
                        transaction.TargetGoal.TargetSavingsRequests = null;
                        transactions.Add(transaction);
                    }

                }
                IEnumerable<Transaction> transactionList = transactions;
                var totalCount = transactions.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    transactionList = transactionList.OrderByDescending(o => o.TransactionId).Where(x => x.TransactionId < pagination.lastIdFetched && x.TargetGoalsId == id);
                }
                else
                {
                    transactionList = transactionList.OrderByDescending(o => o.TransactionId).Where(x => x.TargetGoalsId == id);
                }
                if (pagination.FilterByDateRange)
                {
                    transactionList = transactionList.OrderByDescending(o => o.TransactionId).Where(x => x.TransactionDate >= pagination.StartDate && x.TransactionDate <= pagination.EndDate);
                }
                List<Transaction> items = transactionList.OrderByDescending(o => o.TransactionId)
                                        .Take(pagination.pageSize)
                                        .ToList();

               
                var pagedResult = new Paged<Transaction>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Transaction was retireived successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Transaction Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "GetNTransactionByTargetId(Pagination pagination, long id)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetNTransactions(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                var activeTransactions = transactionRepository.All.Where(x=>x.Amount >0).Include(o => o.TargetGoal).Include(o => o.TargetGoal.CustomerProfile).AsNoTracking();
                List<Transaction> transactions = new List<Transaction>();
                foreach (Transaction transaction in activeTransactions)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                    if (hash == transaction.HashValue)
                    {
                        transaction.TargetGoal.CustomerProfile.TargetGoals = null;
                        transaction.TargetGoal.CustomerProfile.Password = null;
                        transaction.TargetGoal.Transactions = null;
                        transaction.TargetGoal.TargetSavingsRequests = null;
                        transactions.Add(transaction);
                    }

                }
                IEnumerable<Transaction> transactionList = transactions;
                var totalCount = transactions.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    transactionList = transactionList.OrderByDescending(o => o.TransactionId).Where(x => x.TransactionId < pagination.lastIdFetched);
                }
                else
                {
                    transactionList = transactionList.OrderByDescending(o => o.TransactionId);
                }
                

                List<Transaction> items =  transactionList.OrderByDescending(o => o.TransactionId)
                                        .Take(pagination.pageSize)
                                        .ToList();
                
                var pagedResult = new Paged<Transaction>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Transaction list was retireived successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Transaction List Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "GetNTransactions(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetNPendingTransactions(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                var activeTransactions = transactionRepository.All.AsNoTracking();



                //if (pagination.lastIdFetched > 0)
                //{
                //    string status = Enumerators.STATUS.NEW.ToString();
                //    transactions = transactions.OrderByDescending(o => o.TransactionId)
                //        .Where(x => x.TransactionId < pagination.lastIdFetched && string.IsNullOrEmpty(x.ApprovedBy))
                //        .Include(o=>o.TargetGoal).Include(o=>o.TargetGoal.CustomerProfile);
                //}
                //else
                //{
                //    string status = Enumerators.STATUS.DELETED.ToString();
                //    transactions = transactions.OrderByDescending(o => o.TransactionId)
                //        .Where(x=>string.IsNullOrEmpty(x.ApprovedBy) && x.TargetGoal.GoalStatus !=status)
                //        .Include(o => o.TargetGoal).Include(o => o.TargetGoal.CustomerProfile);
                //}
                //var totalCount = transactions.Count();
                //var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);
                string status = Enumerators.STATUS.NEW.ToString();

                activeTransactions = activeTransactions.OrderByDescending(o => o.TransactionId)
                        .Where(x => string.IsNullOrEmpty(x.ApprovedBy) && x.TargetGoal.GoalStatus != status && x.Amount >0)
                        .Include(o => o.TargetGoal).Include(o => o.TargetGoal.CustomerProfile);
                List<Transaction> transactions = new List<Transaction>();
                foreach (Transaction transaction in activeTransactions)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                    if (hash == transaction.HashValue)
                    {
                        
                        transactions.Add(transaction);
                    }

                }
                IEnumerable<Transaction> transactionList = transactions;
                var totalCount = transactions.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);
                if (pagination.lastIdFetched > 0)
                {

                    transactionList = transactionList.OrderByDescending(o => o.TransactionId)
                        .Where(x => x.TransactionId < pagination.lastIdFetched);
                }
                else
                {
                    
                }
                

                List<RecentTransaction> items = (from t in transactionList
                                                       select new RecentTransaction
                                                     {
                                                         Amount=t.Amount,
                                                         CustomerName = t.TargetGoal.CustomerProfile.Surname+ " "+ t.TargetGoal.CustomerProfile.FirstName,
                                                         TransactionType=t.TransactionType,
                                                         TargetGoal=t.TargetGoal.GoalName,
                                                         TransactionId=t.TransactionId
                                                     }).Take(pagination.pageSize).ToList();

                var pagedResult = new Paged<RecentTransaction>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Transaction list was retireived successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Transaction List Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "GetNPendingTransactions(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        private ActionReturn GetAppConfigSettings()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = appConfigSettingRepository.GetAll().Where(o => o.IsActive == true).ToList();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Config retreived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Config could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "GetAppConfigSettings()";
            }
            return result;
        }

        private ActionReturn GetTargetGoalByIdSync(long targetId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                IEnumerable<TargetGoal> targetGoal = targetGoalRepository.All.Include(o => o.CustomerProfile).Include(o => o.TargetSavingsFrequency).AsNoTracking();
                TargetGoal goal= targetGoal.Where(o => o.TargetGoalsId == targetId && o.GoalStatus == goalStatus).FirstOrDefault();
                string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                if (hash == goal.HashValue)
                {
                    result.ReturnedObject = goal;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Target goal was retrieved successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                    
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Target goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "GetTargetGoalByIdSync(long targetId)";
            }
            return result;
        }

        private ActionReturn GetTargetGoalForTransactionProcessing(DateTime dateTime)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string status = Enumerators.STATUS.PROCESSED.ToString();
                var allGoals = targetGoalRepository.All.Include(o => o.CustomerProfile).Include(o => o.TargetSavingsFrequency).AsNoTracking();
                //List<TargetGoal> goals = allGoals.Where(o => o.NextRunDate == dateTime && o.GoalStatus == status && o.GoalDebitType==CONSTANT.CARD_DEBIT_TYPE).ToList();
                List<TargetGoal> goals = allGoals.Where(o => o.NextRunDate == dateTime && o.GoalStatus == status).ToList();
                List<TargetGoal> targetGoalList = new List<TargetGoal>();
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                foreach (TargetGoal goal in goals)
                {
                    if(goal.GoalDebitType == CONSTANT.CARD_DEBIT_TYPE || goal.GoalDebitBank == FCMB_BANK_CODE)
                    {
                        string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                        if (hash == goal.HashValue)
                        {
                            CustomerProfile profile = goal.CustomerProfile;
                            string hashed = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, profile);
                            if (hashed == profile.HashValue)
                            {
                                targetGoalList.Add(goal);
                            }
                        }
                    }
                    
                }
                result.ReturnedObject = targetGoalList;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Goals retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Goal could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "GetTargetGoalForTransactionProcessing(DateTime dateTime)";
            }
            return result;
        }

        public ActionReturn PrepareTargetGoalTransactionSchedule(DateTime dateTime)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string status = Enumerators.STATUS.PROCESSED.ToString();
                string extendedStatus = Enumerators.EXTENDED_STATUS.NONE.ToString();
                var allGoals = targetGoalRepository.All.Include(o => o.CustomerProfile).Include(o => o.TargetSavingsFrequency).AsNoTracking();
                //var allGoals = targetGoalRepository.All;
                //List<TargetGoal> goals = allGoals.Where(o => o.NextRunDate == dateTime && o.GoalStatus == status && o.GoalDebitType == CONSTANT.CARD_DEBIT_TYPE).ToList();
                List<TargetGoal> goals = allGoals.Where(o => o.NextRunDate == dateTime 
                && o.GoalEndDate >= dateTime && o.GoalStatus == status 
                && o.ExtendedGoalStatus== extendedStatus).ToList();

                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();

                DateTime today = DateTime.Now;
                foreach (TargetGoal goal in goals)
                {
                    if (goal.GoalDebitType == CONSTANT.CARD_DEBIT_TYPE || goal.GoalDebitBank == FCMB_BANK_CODE)
                    {
                        string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                        if (hash == goal.HashValue)
                        {
                            CustomerProfile profile = goal.CustomerProfile;
                            string hashed = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, profile);
                            if (hashed == profile.HashValue)
                            {
                                DateTime nextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);
                                TransactionSchedule transactionSchedule = new TransactionSchedule
                                {
                                    DateCreated = today.Date,
                                    DatetimeCreated = today,
                                    DebitType = goal.GoalDebitType,
                                    Description = Enumerators.STATUS.NEW.ToString(),
                                    NextRunDate = nextRunDate,
                                    RunDate = dateTime,
                                    Status = Enumerators.STATUS.NEW.ToString(),
                                    TargetGoalId = goal.TargetGoalsId,
                                    ProcessingStatus=Enumerators.STATUS.NEW.ToString()
                                };
                                this.transactionScheduleRepository.Add(transactionSchedule);
                                if (goal.GoalDebitType == CONSTANT.CARD_DEBIT_TYPE || goal.GoalDebitBank == FCMB_BANK_CODE)
                                {
                                    goal.NextRunDate = nextRunDate;
                                    TargetGoal thisGoal = targetGoalRepository.Find(o=>o.TargetGoalsId==goal.TargetGoalsId).FirstOrDefault();
                                    if (thisGoal != null)
                                    {
                                        
                                        //if (nextRunDate > thisGoal.GoalEndDate)
                                        //{
                                        //    thisGoal.GoalStatus = Enumerators.STATUS.COMPLETED.ToString();
                                        //}
                                        //else
                                        //{
                                        //    thisGoal.NextRunDate = nextRunDate;
                                        //}
                                        thisGoal.NextRunDate = nextRunDate;
                                    }
                                }
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            }
                        }
                    }
                    
                }
                //get target goals due for debit in three days time
                //we put the items in dictionary so we can reference them thogether without creating a typed object.
                Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                int index = 1;
                DateTime dueDate = today.AddDays(3);
                List<TargetGoal> goalsDueInThreeDays = allGoals.Where(o => o.NextRunDate == dueDate.Date && o.GoalStatus == status).ToList();
                foreach(TargetGoal goal in goalsDueInThreeDays)
                {
                    string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                    if (hash == goal.HashValue)
                    {
                        CustomerProfile profile = goal.CustomerProfile;
                        string hashed = Util.GetEncryptedHashValue(CONSTANT.CUSTOMER_PROFILE, profile);
                        if (hashed == profile.HashValue)
                        {
                            //send email to customer
                            EmailLog emailLog = new EmailLog
                            {
                                EmailTo = goal.CustomerProfile.EmailAddress,
                                EmailType = "THREE_DAYS_DUE",
                                Status = Enumerators.STATUS.NEW.ToString(),
                            };

                            dictionaryEmailLog.Add(index, emailLog);

                            Dictionary<string, string> emailValues = new Dictionary<string, string>();
                            emailValues.Add("NAME", $"{goal.CustomerProfile.Surname} {goal.CustomerProfile.FirstName}");
                            emailValues.Add("AMOUNT", goal.TargetAmount.ToString("N"));
                            emailValues.Add("STARTDATE", Convert.ToDateTime(goal.GoalStartDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("ENDDATE", Convert.ToDateTime(goal.GoalEndDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("FREQUENCY", goal.TargetSavingsFrequency.Frequency);
                            emailValues.Add("DUEDATE", Convert.ToDateTime(goal.NextRunDate).ToString("MMMM dd, yyyy"));

                            dictionaryEmailValues.Add(index, emailValues);
                            index++;
                        }
                    }

                    
                }
                //using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                //{
                //    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                //    scope.Complete();
                //}
                result.HasEmail = true;
                result.EmailLog = dictionaryEmailLog;
                result.EmailValues = dictionaryEmailValues;
                result.ReturnedObject = goals.Count();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Transaction schedule created successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Transaction schedule could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "PrepareTargetGoalTransactionSchedule(DateTime dateTime)";
            }
            return result;
        }

        public ActionReturn DirectCreditTest(DirectCreditRequest request)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    ActionReturn nameEnquiryResult = FCMBIntegrator.DirectCreditTest(request, settings);
                    //if (nameEnquiryResult.Flag)
                    //{
                    //    FCMBResponse response = (FCMBResponse)nameEnquiryResult.ReturnedObject;
                    //    DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));
                    //    result.ReturnedObject = dataResponse;
                    //    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    //    result.Message = nameEnquiryResult.Message;
                    //    result.Flag = true;
                    //    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    //}
                    //else
                    //{
                    //    result.ReturnedObject = nameEnquiryResult.ReturnedObject;
                    //    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    //    result.Message = nameEnquiryResult.Message;
                    //    result.Flag = false;
                    //    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    //}
                    result.ReturnedObject = nameEnquiryResult.ReturnedObject;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = nameEnquiryResult.Message;
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = configs.ReturnedObject;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = configs.Message;
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Error completing direct credit request";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "DirectCreditTest(DirectCreditRequest request)";
            }
            return result;

        }
        public ActionReturn ProcessScheduledTransactions(DateTime dateTime,string logPath)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                string CHARGE_URL = settings.Where(o => o.Key == "Paystack_CHARGE_URL").Select(o => o.Value).FirstOrDefault();
                string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                string TRANSACTION_TRIAL_LIMIT = settings.Where(o => o.Key == "Paystack_TRANSACTION_TRIAL_LIMIT").Select(o => o.Value).FirstOrDefault();
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                string Paystack_EMAIL = settings.Where(o => o.Key == "Paystack_EMAIL").Select(o => o.Value).FirstOrDefault();
                int TRANSACTION_TRIAL_LIMIT_VALUE = 0;
                bool limitFlag = int.TryParse(TRANSACTION_TRIAL_LIMIT, out TRANSACTION_TRIAL_LIMIT_VALUE);
                if (!limitFlag)
                {
                    TRANSACTION_TRIAL_LIMIT_VALUE = 3;
                }

                string status = Enumerators.STATUS.NEW.ToString();
                //List<TransactionSchedule> transactionSchedules = transactionScheduleRepository.GetAll().Where(o => o.Status == status && o.DebitType == CONSTANT.CARD_DEBIT_TYPE).ToList();
                //List<TransactionSchedule> transactionSchedules = transactionScheduleRepository.GetAll().Where(o => o.Status == status && o.Trials < TRANSACTION_TRIAL_LIMIT_VALUE).ToList();
                List<TransactionSchedule> transactionSchedules = transactionScheduleRepository.GetAll().Where(o => o.Status == status && o.DateCreated==dateTime && o.Trials < TRANSACTION_TRIAL_LIMIT_VALUE).ToList();
                //get target goals due for debit in three days time
                //we put the items in dictionary so we can reference them thogether without creating a typed object.
                Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                int index = 1;

                foreach (TransactionSchedule transactionSchedule in transactionSchedules)
                {
                    
                    //ActionReturn goalsResult = this.GetTargetGoalByIdSync(transactionSchedule.TargetGoalId);
                    TargetGoal goal = this.targetGoalRepository.All.Where(o => o.TargetGoalsId == transactionSchedule.TargetGoalId).Include(o=>o.CustomerProfile).Include(o=>o.TargetSavingsFrequency).FirstOrDefault();
                    if (goal!=null)
                    {
                       

                        //update the trial so we don't have double debit.
                        transactionSchedule.Trials = transactionSchedule.Trials + 1;
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                        //string amountString = (Decimal.Round(goal.TargetFrequencyAmount, 2)).ToString();
                        //long amount = Convert.ToInt64(amountString) * 100;

                        if (transactionSchedule.DebitType == CONSTANT.CARD_DEBIT_TYPE)
                        {
                            //get auth_code
                            //string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                            //string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                            WebHeaderCollection authheader = new WebHeaderCollection();
                            authheader.Add("Authorization", $"Bearer {SECRET_KEY}");
                            //verify transaction
                            string AUTH_VERIFY_URL = $"{VERIFY_URL}/{goal.GoalCardTokenDetails}";
                            ActionReturn authTokenResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, AUTH_VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, authheader);
                            new FileLogger().LogToFile(authTokenResult, logPath, "ProcessScheduledTransactions");
                            if (authTokenResult.Flag)
                            {
                                ChargeCardResponse authVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(authTokenResult.ReturnedObject));
                                if (authVerifyResponse.status && authVerifyResponse.data.status == "success")
                                {
                                    long roundedAmount = Convert.ToInt64(Decimal.Round(goal.TargetFrequencyAmount, 2) * 100);
                                    ChargeCardRequest chargeCardRequest = new ChargeCardRequest
                                    {
                                        amount = roundedAmount,
                                        authorization_code = authVerifyResponse.data.authorization.authorization_code,
                                        email = Paystack_EMAIL //goal.CustomerProfile.EmailAddress
                                    };
                                    string jsonRequest = JsonConvert.SerializeObject(chargeCardRequest);
                                    WebHeaderCollection header = new WebHeaderCollection();
                                    header.Add("Authorization", $"Bearer {SECRET_KEY}");
                                    if (transactionSchedule.ProcessingStatus == Enumerators.STATUS.NEW.ToString())
                                    {
                                        ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, CHARGE_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                                        new FileLogger().LogToFile(serverResult, logPath, "ProcessScheduledTransactions");
                                        if (serverResult.Flag)
                                        {
                                            ChargeCardResponse jsonResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverResult.ReturnedObject));
                                            if (jsonResponse.status && jsonResponse.data.status == "success")
                                            {
                                                //verify transaction
                                                string CHARGE_AUTH_VERIFY_URL = $"{VERIFY_URL}/{jsonResponse.data.reference}";
                                                ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, CHARGE_AUTH_VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                                                new FileLogger().LogToFile(serverVerifyResult, logPath, "ProcessScheduledTransactions");
                                                if (serverVerifyResult.Flag)
                                                {
                                                    ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                                    if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                                    {
                                                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                                        {
                                                            Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                            ApprovedDate = transactionSchedule.DatetimeCreated,
                                                            Narration = $"TSA Goal top up for {goal.GoalName} (Reference:{jsonVerifyResponse.data.reference})",
                                                            PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                            PostedDate = DateTime.Now,
                                                            TargetGoalsId = goal.TargetGoalsId,
                                                            TransactionDate = DateTime.Now,
                                                            TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                                                        };
                                                        transaction.ChargedFee = Util.ComputeCardFee(100, settings);
                                                        transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                                        this.AddTransaction(transaction);
                                                        //goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                                        DateTime goalTransactionDate = DateTime.Now;
                                                        transactionSchedule.DateProcessed = goalTransactionDate.Date;
                                                        transactionSchedule.DatetimeProcessed = goalTransactionDate;
                                                        transactionSchedule.ProcessingStatus = jsonVerifyResponse.status.ToString();
                                                        transactionSchedule.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(serverVerifyResult)}";
                                                        transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                                                        transactionSchedule.Description = $"SUCCESS";

                                                        goal.GoalBalance = goal.GoalBalance - goal.TargetFrequencyAmount;
                                                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                                    }
                                                    else
                                                    {
                                                        DateTime goalErrorDate = DateTime.Now;
                                                        transactionSchedule.DateProcessed = goalErrorDate.Date;
                                                        transactionSchedule.DatetimeProcessed = goalErrorDate;
                                                        transactionSchedule.ProcessingStatus = Enumerators.STATUS.NEW.ToString();
                                                        transactionSchedule.ProcessingResponse = $"VERIFY FAILED:{jsonResponse.data.reference}:{JsonConvert.SerializeObject(serverVerifyResult)}";
                                                        transactionSchedule.Status = Enumerators.STATUS.NEW.ToString();
                                                        transactionSchedule.Description = $"Card was charged but validation failed, run only validation";
                                                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                                    }

                                                }
                                                else
                                                {
                                                    DateTime goalErrorDate = DateTime.Now;
                                                    transactionSchedule.DateProcessed = goalErrorDate.Date;
                                                    transactionSchedule.DatetimeProcessed = goalErrorDate;
                                                    transactionSchedule.ProcessingStatus = Enumerators.STATUS.PROCESSING.ToString();
                                                    transactionSchedule.ProcessingResponse = $"VERIFY ERROR:{jsonResponse.data.reference}:{JsonConvert.SerializeObject(serverVerifyResult)}";
                                                    transactionSchedule.Status = Enumerators.STATUS.NEW.ToString();
                                                    transactionSchedule.Description = $"Card was charged but validation could not be completed, run only validation";
                                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                                }

                                            }
                                            else
                                            {
                                                DateTime goalErrorDate = DateTime.Now;
                                                transactionSchedule.DateProcessed = goalErrorDate.Date;
                                                transactionSchedule.DatetimeProcessed = goalErrorDate;
                                                transactionSchedule.ProcessingStatus = jsonResponse.status.ToString();
                                                transactionSchedule.ProcessingResponse = $"CHARGE FAILED:{JsonConvert.SerializeObject(serverResult)}";
                                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                            }
                                        }
                                        else
                                        {
                                            DateTime goalErrorDate = DateTime.Now;
                                            transactionSchedule.DateProcessed = goalErrorDate.Date;
                                            transactionSchedule.DatetimeProcessed = goalErrorDate;
                                            transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                            transactionSchedule.ProcessingResponse = $"CHARGE ERROR:{JsonConvert.SerializeObject(serverResult)}";
                                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                        }
                                    }
                                    else if (transactionSchedule.ProcessingStatus == Enumerators.STATUS.PROCESSING.ToString())
                                    {
                                        //verify transaction only
                                        string[] referenceArray = transactionSchedule.ProcessingResponse.Split(':');
                                        string reference = "";
                                        if (referenceArray.Length > 2)
                                        {
                                            reference = referenceArray[1];
                                        }
                                        string PAY_VERIFY_URL = $"{VERIFY_URL}/{reference}";
                                        ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, PAY_VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                                        new FileLogger().LogToFile(serverVerifyResult, logPath, "ProcessScheduledTransactions");
                                        if (serverVerifyResult.Flag)
                                        {
                                            ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                            if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                            {
                                                Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                                {
                                                    Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                                    ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                    ApprovedDate = transactionSchedule.DatetimeCreated,
                                                    Narration = $"TSA Goal top up for {goal.GoalName} (Reference:{reference})",
                                                    PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                    PostedDate = DateTime.Now,
                                                    TargetGoalsId = goal.TargetGoalsId,
                                                    TransactionDate = DateTime.Now,
                                                    TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                                                };
                                                transaction.ChargedFee = Util.ComputeCardFee(100, settings);
                                                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                                this.AddTransaction(transaction);
                                                //goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                                DateTime goalTransactionDate = DateTime.Now;
                                                transactionSchedule.DateProcessed = goalTransactionDate.Date;
                                                transactionSchedule.DatetimeProcessed = goalTransactionDate;
                                                transactionSchedule.ProcessingStatus = jsonVerifyResponse.status.ToString();
                                                transactionSchedule.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(serverVerifyResult)}";
                                                transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                                                transactionSchedule.Description = $"SUCCESS";

                                                goal.GoalBalance = goal.GoalBalance - goal.TargetFrequencyAmount;
                                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                            }
                                            else
                                            {
                                                DateTime goalErrorDate = DateTime.Now;
                                                transactionSchedule.DateProcessed = goalErrorDate.Date;
                                                transactionSchedule.DatetimeProcessed = goalErrorDate;
                                                transactionSchedule.ProcessingStatus = Enumerators.STATUS.PROCESSING.ToString();
                                                transactionSchedule.ProcessingResponse = $"VERIFY FAILED:{JsonConvert.SerializeObject(serverVerifyResult)}";
                                                transactionSchedule.Status = Enumerators.STATUS.NEW.ToString();
                                                transactionSchedule.Description = $"Card was charged but validation failed, run only validation";
                                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                            }

                                        }
                                        else
                                        {
                                            DateTime goalErrorDate = DateTime.Now;
                                            transactionSchedule.DateProcessed = goalErrorDate.Date;
                                            transactionSchedule.DatetimeProcessed = goalErrorDate;
                                            transactionSchedule.ProcessingStatus = Enumerators.STATUS.PROCESSING.ToString();
                                            transactionSchedule.ProcessingResponse = $"VERIFY ERROR:{JsonConvert.SerializeObject(serverVerifyResult)}";
                                            transactionSchedule.Status = Enumerators.STATUS.NEW.ToString();
                                            transactionSchedule.Description = $"Card was charged but validation could not be completed, run only validation";
                                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    DateTime goalErrorDate = DateTime.Now;
                                    transactionSchedule.DateProcessed = goalErrorDate.Date;
                                    transactionSchedule.DatetimeProcessed = goalErrorDate;
                                    transactionSchedule.ProcessingStatus = Enumerators.STATUS.NEW.ToString();
                                    transactionSchedule.ProcessingResponse = $"CHARGE ERROR:{JsonConvert.SerializeObject(authTokenResult)}";
                                    transactionSchedule.Description = $"Card was not charged, error getting authentication code reference";
                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                }

                                
                            }
                            else
                            {
                                DateTime goalErrorDate = DateTime.Now;
                                transactionSchedule.DateProcessed = goalErrorDate.Date;
                                transactionSchedule.DatetimeProcessed = goalErrorDate;
                                transactionSchedule.ProcessingStatus = Enumerators.STATUS.NEW.ToString();
                                transactionSchedule.ProcessingResponse = $"CHARGE ERROR:{JsonConvert.SerializeObject(authTokenResult)}";
                                transactionSchedule.Description = $"Card was not charged, error fetching reference";
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            }

                            
                        }
                        else if(transactionSchedule.DebitType == CONSTANT.BANK_DEBIT_TYPE && goal.GoalDebitBank == FCMB_BANK_CODE)
                        {
                            //direct debit 
                            //string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                            //get flexible and fixed account
                            string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                            string FCMB_FIXED_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                            string FCMB_SAVINGS_ACCOUNT = "";
                            //check if goal is fixed or flexible to know the accounting entries
                            if(goal.LiquidationMode== (int)Enumerators.LIQUIDATION_MODE.FIXED)
                            {
                                FCMB_SAVINGS_ACCOUNT = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                            }
                            else if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE)
                            {
                                FCMB_SAVINGS_ACCOUNT = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                            }

                            string transactionId = goal.TargetGoalsId.ToString();
                            string Narration1 = $"TSA Goal top up for {goal.GoalName}";
                            string Narration2 = "";
                            long milliseconds = DateTime.Now.Ticks;
                            string requestId = milliseconds.ToString() + transactionId;
                            ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(goal.GoalDebitAccountNo, FCMB_SAVINGS_ACCOUNT, Decimal.Round(goal.TargetFrequencyAmount, 2), CONSTANT.EMPTY,
                                $"{goal.TargetGoalsId}B{goal.CustomerProfileId}", requestId, goal.GoalDebitBank, Narration1, Narration2, settings);
                            new FileLogger().LogToFile(directCreditResult, logPath, "ProcessScheduledTransactions");
                            if (directCreditResult.Flag)
                            {
                                FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                                //DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                                Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                {
                                    Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                    ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    ApprovedDate = transactionSchedule.DatetimeCreated,
                                    Narration = $"TSA Goal top up for {goal.GoalName} ({response.StatusMessage})",
                                    PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    PostedDate = DateTime.Now,
                                    TargetGoalsId = goal.TargetGoalsId,
                                    TransactionDate = DateTime.Now,
                                    TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                                };
                                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                this.AddTransaction(transaction);
                                //goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                DateTime goalTransactionDate = DateTime.Now;
                                transactionSchedule.DateProcessed = goalTransactionDate.Date;
                                transactionSchedule.DatetimeProcessed = goalTransactionDate;
                                transactionSchedule.ProcessingStatus = response.StatusMessage;
                                transactionSchedule.ProcessingResponse = $"SUCCESS:TransactionId:{JsonConvert.SerializeObject(directCreditResult)}";
                                transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                                transactionSchedule.Description = $"SUCCESS";

                                goal.GoalBalance = goal.GoalBalance - goal.TargetFrequencyAmount;
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                            }
                            else
                            {
                                //verify transaction status

                                ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                                new FileLogger().LogToFile(verifyDirectCreditResult, logPath, "ProcessScheduledTransactions");
                                if (verifyDirectCreditResult.Flag)
                                {
                                    FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                                    //GetTransactionStatusResponse verifyDataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(verifyDCResponse.ResponseData));
                                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                    {
                                        Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                        ApprovedDate = transactionSchedule.DatetimeCreated,
                                        Narration = $"TSA Goal top up for {goal.GoalName} ({verifyDCResponse.StatusMessage})",
                                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                        PostedDate = DateTime.Now,
                                        TargetGoalsId = goal.TargetGoalsId,
                                        TransactionDate = DateTime.Now,
                                        TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                                    };
                                    transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                    this.AddTransaction(transaction);
                                    //goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                    DateTime goalTransactionDate = DateTime.Now;
                                    transactionSchedule.DateProcessed = goalTransactionDate.Date;
                                    transactionSchedule.DatetimeProcessed = goalTransactionDate;
                                    transactionSchedule.ProcessingStatus = verifyDCResponse.StatusMessage;
                                    transactionSchedule.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(verifyDirectCreditResult)}";
                                    transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                                    transactionSchedule.Description = $"SUCCESS";

                                    goal.GoalBalance = goal.GoalBalance - goal.TargetFrequencyAmount;
                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                }
                                else
                                {
                                    DateTime goalErrorDate = DateTime.Now;                                    
                                    transactionSchedule.DateProcessed = goalErrorDate.Date;
                                    transactionSchedule.DatetimeProcessed = goalErrorDate;
                                    transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                    transactionSchedule.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(directCreditResult)} VERIFY ERROR:{requestId}:{JsonConvert.SerializeObject(verifyDirectCreditResult)}";
                                    transactionSchedule.Description = $"Account was not debited.";
                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                }                                
                            }
                        }                        
                        else
                        {
                            DateTime goalErrorDate = DateTime.Now;
                            transactionSchedule.DateProcessed = goalErrorDate.Date;
                            transactionSchedule.DatetimeProcessed = goalErrorDate;
                            transactionSchedule.ProcessingStatus = Enumerators.STATUS.NEW.ToString();
                            transactionSchedule.ProcessingResponse = $"DEBIT ERROR:Debit account is not an FCMB account number.";
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        }

                        if (transactionSchedule.Status == Enumerators.STATUS.PROCESSED.ToString())
                        {
                            //send email;
                            EmailLog emailLog = new EmailLog
                            {
                                EmailTo = goal.CustomerProfile.EmailAddress,
                                EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                                EmailType = "SUCCESS_DEBIT_CUSTOMER",
                                Status = Enumerators.STATUS.NEW.ToString(),
                            };

                            dictionaryEmailLog.Add(index, emailLog);

                            Dictionary<string, string> emailValues = new Dictionary<string, string>();
                            emailValues.Add("NAME", $"{goal.CustomerProfile.Surname} {goal.CustomerProfile.FirstName}");
                            emailValues.Add("AMOUNT", goal.TargetAmount.ToString("N"));
                            emailValues.Add("STARTDATE", Convert.ToDateTime(goal.GoalStartDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("ENDDATE", Convert.ToDateTime(goal.GoalEndDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("FREQUENCY", goal.TargetSavingsFrequency.Frequency);
                            emailValues.Add("PLANNAME", goal.GoalName);

                            dictionaryEmailValues.Add(index, emailValues);
                            index++;
                        }
                        else
                        {
                            if (transactionSchedule.Trials == TRANSACTION_TRIAL_LIMIT_VALUE)
                            {
                                //send email;
                                EmailLog emailLog = new EmailLog
                                {
                                    EmailTo = goal.CustomerProfile.EmailAddress,
                                    EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                                    EmailType = "FAILED_DEBIT_CUSTOMER",
                                    Status = Enumerators.STATUS.NEW.ToString(),
                                };

                                dictionaryEmailLog.Add(index, emailLog);

                                Dictionary<string, string> emailValues = new Dictionary<string, string>();
                                emailValues.Add("NAME", $"{goal.CustomerProfile.Surname} {goal.CustomerProfile.FirstName}");
                                emailValues.Add("AMOUNT", goal.TargetAmount.ToString("N"));
                                emailValues.Add("STARTDATE", Convert.ToDateTime(goal.GoalStartDate).ToString("MMMM dd, yyyy"));
                                emailValues.Add("ENDDATE", Convert.ToDateTime(goal.GoalEndDate).ToString("MMMM dd, yyyy"));
                                emailValues.Add("FREQUENCY", goal.TargetSavingsFrequency.Frequency);
                                emailValues.Add("PLANNAME", goal.GoalName);

                                dictionaryEmailValues.Add(index, emailValues);
                                index++;

                            }
                        }
                    }
                    else
                    {
                        DateTime goalErrorDate = DateTime.Now;
                        transactionSchedule.DateProcessed = goalErrorDate.Date;
                        transactionSchedule.DatetimeProcessed = goalErrorDate;
                        transactionSchedule.ProcessingStatus = Enumerators.STATUS.NEW.ToString();
                        transactionSchedule.ProcessingResponse = $"Error fetching goal details";
                        transactionSchedule.Description = $"Account was not debited, could not get a reference to the target goal.";
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    }

                    

                }
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                //using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                //{
                //    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                //    scope.Complete();
                //}


                if (dictionaryEmailLog.Count > 0)
                {
                    result.HasEmail = true;
                    result.EmailValues = dictionaryEmailValues;
                    result.EmailLog = dictionaryEmailLog;
                }

                result.UpdateReturn = UpdateReturn.ItemUpdated;
                result.Message = "Done";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                try
                {
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                }
                catch(Exception ex1)
                {
                    result.ReturnedObject = ex1.ToString();
                    new FileLogger().LogToFile(result, logPath, "ProcessScheduledTransactionsExceptionSave");
                }
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
                new FileLogger().LogToFile(result, logPath, "ProcessScheduledTransactionsException");
            }
            finally
            {
                
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessScheduledTransactions(DateTime dateTime)";
            }
            return result;
        }

        public async Task<ActionReturn> GetNFailedTransactionSchedule(Pagination pagination)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                long customerId = pagination.SearchData != null ? Convert.ToInt64(pagination.SearchData) : 0;
                string status = Enumerators.STATUS.FAILED.ToString();
                DateTime today = DateTime.Now.Date;
                var activeTransactions = transactionScheduleRepository.All.Where(o=>o.Status== status && o.DateCreated < today);
                var joinedTransactions = (from t in transactionScheduleRepository.All
                                         join g in targetGoalRepository.All on t.TargetGoalId equals g.TargetGoalsId
                                         where g.CustomerProfileId==customerId
                                         select new TransactionScheduleReport {
                                             DateCreated=t.DateCreated,
                                             DateProcessed=t.DateProcessed,
                                             DatetimeCreated=t.DatetimeCreated,
                                             DatetimeProcessed=t.DatetimeProcessed,
                                             DebitType=t.DebitType,
                                             Description=t.Description,
                                             Goal=g,
                                             Id=t.Id,
                                             NextRunDate=t.NextRunDate,
                                             ProcessingResponse=t.ProcessingResponse,
                                             ProcessingStatus=t.ProcessingStatus,
                                             RunDate=t.RunDate,
                                             Status=t.Status,
                                             TargetGoalId=t.TargetGoalId,
                                             Trials=t.Trials
                                         }).ToList();


                IEnumerable<TransactionScheduleReport> transactionList = joinedTransactions;
                var totalCount = transactionList.Count();
                var totalPages = (int)Math.Ceiling((double)totalCount / pagination.pageSize);

                if (pagination.lastIdFetched > 0)
                {
                    transactionList = transactionList.OrderByDescending(o => o.Id).Where(x => x.Id < pagination.lastIdFetched);
                }
                else
                {
                    transactionList = transactionList.OrderByDescending(o => o.Id);
                }


                List<TransactionScheduleReport> items = transactionList.OrderByDescending(o => o.Id)
                                        .Take(pagination.pageSize)
                                        .ToList();

                var pagedResult = new Paged<TransactionScheduleReport>
                {
                    TotalItemsCount = totalCount,
                    TotalNumberOfPages = totalPages,
                    Items = items,
                    PageSize = pagination.pageSize,
                    PageNumber = pagination.pageNumber,
                    StartIndex = (pagination.pageNumber - 1) * pagination.pageSize,
                    Keyword = ""
                };

                result.ReturnedObject = pagedResult;
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Transaction list was retireived successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Transaction List Could Not Be Retireived";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "GetNFailedTransactionSchedule(Pagination pagination)";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn ProcessScheduledTransactions_old2(DateTime dateTime)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                string CHARGE_URL = settings.Where(o => o.Key == "Paystack_CHARGE_URL").Select(o => o.Value).FirstOrDefault();
                string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                string TRANSACTION_TRIAL_LIMIT = settings.Where(o => o.Key == "Paystack_TRANSACTION_TRIAL_LIMIT").Select(o => o.Value).FirstOrDefault();
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                int TRANSACTION_TRIAL_LIMIT_VALUE = 0;
                bool limitFlag = int.TryParse(TRANSACTION_TRIAL_LIMIT, out TRANSACTION_TRIAL_LIMIT_VALUE);
                if (!limitFlag)
                {
                    TRANSACTION_TRIAL_LIMIT_VALUE = 3;
                }

                string status = Enumerators.STATUS.NEW.ToString();
                //List<TransactionSchedule> transactionSchedules = transactionScheduleRepository.GetAll().Where(o => o.Status == status && o.DebitType == CONSTANT.CARD_DEBIT_TYPE).ToList();
                List<TransactionSchedule> transactionSchedules = transactionScheduleRepository.GetAll().Where(o => o.Status == status).ToList();
                //get target goals due for debit in three days time
                //we put the items in dictionary so we can reference them thogether without creating a typed object.
                Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                int index = 1;

                foreach (TransactionSchedule transactionSchedule in transactionSchedules)
                {

                    ActionReturn goalsResult = this.GetTargetGoalByIdSync(transactionSchedule.TargetGoalId);
                    if (goalsResult.Flag)
                    {
                        TargetGoal goal = (TargetGoal)goalsResult.ReturnedObject;

                        if (transactionSchedule.Trials == TRANSACTION_TRIAL_LIMIT_VALUE)
                        {
                            //send email;
                            EmailLog emailLog = new EmailLog
                            {
                                EmailTo = goal.CustomerProfile.EmailAddress,
                                EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                                EmailType = "FAILED_DEBIT_CUSTOMER",
                                Status = Enumerators.STATUS.NEW.ToString(),
                            };

                            dictionaryEmailLog.Add(index, emailLog);

                            Dictionary<string, string> emailValues = new Dictionary<string, string>();
                            emailValues.Add("NAME", $"{goal.CustomerProfile.Surname} {goal.CustomerProfile.FirstName}");
                            emailValues.Add("AMOUNT", goal.TargetAmount.ToString("N"));
                            emailValues.Add("STARTDATE", Convert.ToDateTime(goal.GoalStartDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("ENDDATE", Convert.ToDateTime(goal.GoalEndDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("FREQUENCY", goal.TargetSavingsFrequency.Frequency);
                            emailValues.Add("PLANNAME", goal.GoalName);

                            dictionaryEmailValues.Add(index, emailValues);
                            index++;
                            continue;
                        }
                        transactionSchedule.Trials = transactionSchedule.Trials + 1;

                        //string amountString = (Decimal.Round(goal.TargetFrequencyAmount, 2)).ToString();
                        //long amount = Convert.ToInt64(amountString) * 100;

                        if (transactionSchedule.DebitType == CONSTANT.CARD_DEBIT_TYPE)
                        {
                            long roundedAmount = Convert.ToInt64(Decimal.Round(goal.TargetFrequencyAmount, 2) * 100);
                            ChargeCardRequest chargeCardRequest = new ChargeCardRequest
                            {
                                amount = roundedAmount,
                                authorization_code = goal.GoalCardTokenDetails,
                                email = goal.CustomerProfile.EmailAddress
                            };
                            string jsonRequest = JsonConvert.SerializeObject(chargeCardRequest);
                            WebHeaderCollection header = new WebHeaderCollection();
                            header.Add("Authorization", $"Bearer {SECRET_KEY}");
                            if (transactionSchedule.Status == Enumerators.STATUS.NEW.ToString())
                            {
                                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, CHARGE_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                                if (serverResult.Flag)
                                {
                                    ChargeCardResponse jsonResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverResult.ReturnedObject));
                                    if (jsonResponse.status && jsonResponse.data.status == "success")
                                    {
                                        //verify transaction
                                        VERIFY_URL = $"{VERIFY_URL}/{jsonResponse.data.reference}";
                                        ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                                        if (serverVerifyResult.Flag)
                                        {
                                            ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                            if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                            {
                                                Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                                {
                                                    Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                                    ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                    ApprovedDate = DateTime.Now,
                                                    Narration = $"{jsonResponse.data.reference}",
                                                    PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                                    PostedDate = DateTime.Now,
                                                    TargetGoalsId = goal.TargetGoalsId,
                                                    TransactionDate = DateTime.Now,
                                                    TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                                };
                                                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                                this.AddTransaction(transaction);
                                                goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                                DateTime goalTransactionDate = DateTime.Now;
                                                transactionSchedule.DateProcessed = goalTransactionDate.Date;
                                                transactionSchedule.DatetimeProcessed = goalTransactionDate;
                                                transactionSchedule.ProcessingStatus = jsonVerifyResponse.status.ToString();
                                                transactionSchedule.ProcessingResponse = $"SUCCESS:{jsonVerifyResponse}";
                                                transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                                                transactionSchedule.Description = $"SUCCESS";

                                                goal.GoalBalance = goal.GoalBalance - goal.TargetFrequencyAmount;

                                            }
                                            else
                                            {
                                                DateTime goalErrorDate = DateTime.Now;
                                                transactionSchedule.DateProcessed = goalErrorDate.Date;
                                                transactionSchedule.DatetimeProcessed = goalErrorDate;
                                                transactionSchedule.ProcessingStatus = jsonResponse.status.ToString();
                                                transactionSchedule.ProcessingResponse = $"VERIFY FAILED:{jsonResponse.data.reference}:{jsonVerifyResponse}";
                                                transactionSchedule.Status = Enumerators.STATUS.PROCESSING.ToString();
                                                transactionSchedule.Description = $"Card was charged but validation failed, run only validation";
                                            }

                                        }
                                        else
                                        {
                                            DateTime goalErrorDate = DateTime.Now;
                                            transactionSchedule.DateProcessed = goalErrorDate.Date;
                                            transactionSchedule.DatetimeProcessed = goalErrorDate;
                                            transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                            transactionSchedule.ProcessingResponse = $"VERIFY ERROR:{jsonResponse.data.reference}:{serverVerifyResult.FlagDescription}:{serverVerifyResult.Message}";
                                            transactionSchedule.Status = Enumerators.STATUS.PROCESSING.ToString();
                                            transactionSchedule.Description = $"Card was charged but validation could not be completed, run only validation";
                                        }

                                    }
                                    else
                                    {
                                        DateTime goalErrorDate = DateTime.Now;
                                        transactionSchedule.DateProcessed = goalErrorDate.Date;
                                        transactionSchedule.DatetimeProcessed = goalErrorDate;
                                        transactionSchedule.ProcessingStatus = jsonResponse.status.ToString();
                                        transactionSchedule.ProcessingResponse = $"CHARGE FAILED:{jsonResponse}";
                                    }
                                }
                                else
                                {
                                    DateTime goalErrorDate = DateTime.Now;
                                    transactionSchedule.DateProcessed = goalErrorDate.Date;
                                    transactionSchedule.DatetimeProcessed = goalErrorDate;
                                    transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                    transactionSchedule.ProcessingResponse = $"CHARGE ERROR:{serverResult.FlagDescription}:{serverResult.Message}";
                                }
                            }
                            else if (transactionSchedule.Status == Enumerators.STATUS.PROCESSING.ToString())
                            {
                                //verify transaction only
                                string[] referenceArray = transactionSchedule.ProcessingResponse.Split(':');
                                string reference = "";
                                if (referenceArray.Length > 2)
                                {
                                    reference = referenceArray[1];
                                }
                                VERIFY_URL = $"{VERIFY_URL}/{reference}";
                                ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                                if (serverVerifyResult.Flag)
                                {
                                    ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                    if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                    {
                                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                        {
                                            Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                            ApprovedDate = DateTime.Now,
                                            Narration = $"{reference}",
                                            PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                            PostedDate = DateTime.Now,
                                            TargetGoalsId = goal.TargetGoalsId,
                                            TransactionDate = DateTime.Now,
                                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                        };
                                        transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                        this.AddTransaction(transaction);
                                        goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                        DateTime goalTransactionDate = DateTime.Now;
                                        transactionSchedule.DateProcessed = goalTransactionDate.Date;
                                        transactionSchedule.DatetimeProcessed = goalTransactionDate;
                                        transactionSchedule.ProcessingStatus = jsonVerifyResponse.status.ToString();
                                        transactionSchedule.ProcessingResponse = $"SUCCESS:{jsonVerifyResponse}";
                                        transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                                        transactionSchedule.Description = $"SUCCESS";

                                        goal.GoalBalance = goal.GoalBalance - goal.TargetFrequencyAmount;

                                    }
                                    else
                                    {
                                        DateTime goalErrorDate = DateTime.Now;
                                        transactionSchedule.DateProcessed = goalErrorDate.Date;
                                        transactionSchedule.DatetimeProcessed = goalErrorDate;
                                        transactionSchedule.ProcessingStatus = jsonVerifyResponse.status.ToString();
                                        transactionSchedule.ProcessingResponse = $"VERIFY FAILED:{reference}:{jsonVerifyResponse}";
                                        transactionSchedule.Status = Enumerators.STATUS.PROCESSING.ToString();
                                        transactionSchedule.Description = $"Card was charged but validation failed, run only validation";
                                    }

                                }
                                else
                                {
                                    DateTime goalErrorDate = DateTime.Now;
                                    transactionSchedule.DateProcessed = goalErrorDate.Date;
                                    transactionSchedule.DatetimeProcessed = goalErrorDate;
                                    transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                    transactionSchedule.ProcessingResponse = $"VERIFY ERROR:{reference}:{serverVerifyResult.FlagDescription}:{serverVerifyResult.Message}";
                                    transactionSchedule.Status = Enumerators.STATUS.PROCESSING.ToString();
                                    transactionSchedule.Description = $"Card was charged but validation could not be completed, run only validation";
                                }
                            }
                        }
                        else if (transactionSchedule.DebitType == CONSTANT.BANK_DEBIT_TYPE && goal.GoalDebitBank == FCMB_BANK_CODE)
                        {
                            //direct debit 
                            string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                            ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(FCMB_SAVINGS_ACCOUNT, FCMB_BANK_CODE, settings);
                            if (nameEnquiryResult.Flag)
                            {
                                FCMBResponse nameEnquiryFCMBResponse = (FCMBResponse)nameEnquiryResult.ReturnedObject;
                                NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                                string transactionId = goal.TargetGoalsId.ToString();
                                string Narration1 = $"TSA Goal top up for {goal.GoalName}";
                                string Narration2 = "";
                                ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(goal.GoalDebitAccountNo, FCMB_SAVINGS_ACCOUNT, goal.TargetFrequencyAmount, nameEnquiryResponse.FullName,
                                    $"{goal.TargetGoalsId}B{goal.CustomerProfileId}", transactionId, goal.GoalDebitBank, Narration1, Narration2, settings);
                                if (directCreditResult.Flag)
                                {
                                    FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                                    DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                                    ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(dataResponse.TransactionId, settings);
                                    if (verifyDirectCreditResult.Flag)
                                    {
                                        FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                                        GetTransactionStatusResponse verifyDataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(verifyDCResponse.ResponseData));
                                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                        {
                                            Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                            ApprovedDate = DateTime.Now,
                                            Narration = $"{dataResponse.TransactionId}|{dataResponse.FinacleTranID}",
                                            PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                            PostedDate = DateTime.Now,
                                            TargetGoalsId = goal.TargetGoalsId,
                                            TransactionDate = DateTime.Now,
                                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                        };
                                        transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                        this.AddTransaction(transaction);
                                        goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                        DateTime goalTransactionDate = DateTime.Now;
                                        transactionSchedule.DateProcessed = goalTransactionDate.Date;
                                        transactionSchedule.DatetimeProcessed = goalTransactionDate;
                                        transactionSchedule.ProcessingStatus = verifyDCResponse.StatusMessage;
                                        transactionSchedule.ProcessingResponse = $"SUCCESS:{verifyDCResponse.ResponseData}";
                                        transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                                        transactionSchedule.Description = $"SUCCESS";

                                        goal.GoalBalance = goal.GoalBalance - goal.TargetFrequencyAmount;
                                    }
                                    else
                                    {
                                        DateTime goalErrorDate = DateTime.Now;
                                        transactionSchedule.DateProcessed = goalErrorDate.Date;
                                        transactionSchedule.DatetimeProcessed = goalErrorDate;
                                        transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                        transactionSchedule.ProcessingResponse = $"VERIFY ERROR:{dataResponse.TransactionId}:{JsonConvert.SerializeObject(verifyDirectCreditResult)}";
                                        transactionSchedule.Status = Enumerators.STATUS.PROCESSING.ToString();
                                        transactionSchedule.Description = $"Account was debitted but validation could not be completed, run only validation";

                                    }

                                }
                                else
                                {
                                    DateTime goalErrorDate = DateTime.Now;
                                    transactionSchedule.DateProcessed = goalErrorDate.Date;
                                    transactionSchedule.DatetimeProcessed = goalErrorDate;
                                    transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                    transactionSchedule.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(directCreditResult)}";
                                }
                            }
                            else
                            {
                                DateTime goalErrorDate = DateTime.Now;
                                transactionSchedule.DateProcessed = goalErrorDate.Date;
                                transactionSchedule.DatetimeProcessed = goalErrorDate;
                                transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                transactionSchedule.ProcessingResponse = $"ENQUIRY ERROR:{JsonConvert.SerializeObject(nameEnquiryResult)}";
                                transactionSchedule.Status = Enumerators.STATUS.PROCESSING.ToString();
                                transactionSchedule.Description = $"Account was not debitted, error validating account {FCMB_SAVINGS_ACCOUNT}";

                            }


                        }

                    }
                    else
                    {
                        DateTime goalErrorDate = DateTime.Now;
                        transactionSchedule.DateProcessed = goalErrorDate.Date;
                        transactionSchedule.DatetimeProcessed = goalErrorDate;
                        transactionSchedule.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                        transactionSchedule.ProcessingResponse = $"{JsonConvert.SerializeObject(goalsResult)}";
                        transactionSchedule.Description = $"Account was not debitted, could not get a reference to the target goal.";
                    }

                }

                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    scope.Complete();
                }

                if (dictionaryEmailLog.Count > 0)
                {
                    result.HasEmail = true;
                    result.EmailValues = dictionaryEmailValues;
                    result.EmailLog = dictionaryEmailLog;
                }

                result.UpdateReturn = UpdateReturn.ItemUpdated;
                result.Message = "Done";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ChargeCustomerCard(DateTime dateTime)";
            }
            return result;
        }
        public ActionReturn ProcessLiquidationRequests_old()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                //string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string TSA_FIXED_MODE_PENAL_CHARGE = settings.Where(o => o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").Select(o => o.Value).FirstOrDefault();

                string approvalStatus = Enumerators.APPROVAL_STATUS.APPROVED.ToString();
                string status= Enumerators.STATUS.PENDING.ToString();
                List<TargetSavingsRequestType> targetSavingsRequestTypes = targetSavingsRequestTypeRepository.Find(o => o.IsActive).ToList();
                if(targetSavingsRequestTypes.Count > 0)
                {
                    List<TargetSavingsRequest> requests = targetSavingsRequestRepository.FindAll(o => o.Status == status && o.ApprovalStatus == approvalStatus).ToList();
                    foreach(TargetSavingsRequest request in requests)
                    {
                        TargetSavingsRequestType targetSavingsRequestType = targetSavingsRequestTypes.Where(o => o.RequestTypeId == request.RequestTypeId).FirstOrDefault();
                        if(targetSavingsRequestType !=null & targetSavingsRequestType.IsLiquidation)
                        {
                            TargetSavingsRequest thisRequestForLiquidation=processGoalBalanceLiquidation(settings, TSA_FIXED_MODE_PENAL_CHARGE, request);
                            request.ProcessingResponse = thisRequestForLiquidation.ProcessingResponse;
                            request.Status = thisRequestForLiquidation.Status;
                            request.ApprovalStatus= thisRequestForLiquidation.ApprovalStatus;
                        }
                        else if (targetSavingsRequestType != null & targetSavingsRequestType.IsInterestLiquidation)
                        {
                            TargetSavingsRequest thisRequestForInterestLiquidation =ProcessInterestLiquidationRequests(settings, request);
                            request.ProcessingResponse = thisRequestForInterestLiquidation.ProcessingResponse;
                            request.Status = thisRequestForInterestLiquidation.Status;
                            request.ApprovalStatus = thisRequestForInterestLiquidation.ApprovalStatus;
                        }
                        using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                        {
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            scope.Complete();
                        }
                    }

                }
                else
                {
                    //could not load request type
                }               


            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessLiquidationRequests()";
            }
            return result;
        }

        public ActionReturn ProcessLiquidationRequests()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                //string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string TSA_FIXED_MODE_PENAL_CHARGE = settings.Where(o => o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").Select(o => o.Value).FirstOrDefault();

                string TRANSACTION_TRIAL_LIMIT = settings.Where(o => o.Key == "Paystack_TRANSACTION_TRIAL_LIMIT").Select(o => o.Value).FirstOrDefault();
                int TRANSACTION_TRIAL_LIMIT_VALUE = 0;
                bool limitFlag = int.TryParse(TRANSACTION_TRIAL_LIMIT, out TRANSACTION_TRIAL_LIMIT_VALUE);
                if (!limitFlag)
                {
                    TRANSACTION_TRIAL_LIMIT_VALUE = 3;
                }

                string approvalStatus = Enumerators.APPROVAL_STATUS.APPROVED.ToString();
                string status = Enumerators.STATUS.PENDING.ToString();
                List<TargetSavingsRequestType> targetSavingsRequestTypes = targetSavingsRequestTypeRepository.Find(o => o.IsActive).ToList();
                if (targetSavingsRequestTypes.Count > 0)
                {
                    List<TargetSavingsRequest> requests = targetSavingsRequestRepository.FindAll(o => o.Status == status && o.ApprovalStatus == approvalStatus && o.Trial < TRANSACTION_TRIAL_LIMIT_VALUE).ToList();
                    foreach (TargetSavingsRequest request in requests)
                    {
                        TargetSavingsRequestType targetSavingsRequestType = targetSavingsRequestTypes.Where(o => o.RequestTypeId == request.RequestTypeId).FirstOrDefault();
                        if (targetSavingsRequestType != null & targetSavingsRequestType.IsLiquidation)
                        {
                            TargetSavingsRequest thisRequestForLiquidation = processGoalBalanceLiquidation_V2(settings, TSA_FIXED_MODE_PENAL_CHARGE, request);
                            request.ProcessingResponse = thisRequestForLiquidation.ProcessingResponse;
                            request.Status = thisRequestForLiquidation.Status;
                            request.ApprovalStatus = thisRequestForLiquidation.ApprovalStatus;
                        }
                        else if (targetSavingsRequestType != null & targetSavingsRequestType.IsInterestLiquidation)
                        {
                            TargetSavingsRequest thisRequestForInterestLiquidation = ProcessInterestLiquidationRequests_V2(settings, request);
                            request.ProcessingResponse = thisRequestForInterestLiquidation.ProcessingResponse;
                            request.Status = thisRequestForInterestLiquidation.Status;
                            request.ApprovalStatus = thisRequestForInterestLiquidation.ApprovalStatus;
                        }
                        request.Trial = request.Trial + 1;
                        using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                        {
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            scope.Complete();
                        }
                    }
                    result.Message = "COMPLETED";
                    result.Flag = true;

                }
                else
                {
                    //could not load request type
                    result.Message = "REQUEST TYPE IS NOT ACTIVE";
                    result.Flag = false;
                }


            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessLiquidationRequests()";
            }
            return result;
        }

        public ActionReturn ProcessLiquidationRequests_new()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                string TRANSACTION_TRIAL_LIMIT = settings.Where(o => o.Key == "Paystack_TRANSACTION_TRIAL_LIMIT").Select(o => o.Value).FirstOrDefault();


                string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string FCMB_FIXED_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                string FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                bool limitFlag = int.TryParse(TRANSACTION_TRIAL_LIMIT, out int TRANSACTION_TRIAL_LIMIT_VALUE);
                if (!limitFlag)
                {
                    TRANSACTION_TRIAL_LIMIT_VALUE = 3;
                }
                string approvalStatus = Enumerators.APPROVAL_STATUS.APPROVED.ToString();
                string status = Enumerators.STATUS.PENDING.ToString();
                string liquidationStatus= Enumerators.STATUS.NEW.ToString();
                List<TargetSavingsRequestType> targetSavingsRequestTypes = targetSavingsRequestTypeRepository.Find(o => o.IsActive).ToList();
                if (targetSavingsRequestTypes.Count > 0)
                {
                    List<TargetSavingsRequest> requests = targetSavingsRequestRepository.FindAll(o => o.Status == status && o.ApprovalStatus == approvalStatus).ToList();
                    foreach (TargetSavingsRequest request in requests)
                    {
                        TargetSavingsRequestType targetSavingsRequestType = targetSavingsRequestTypes.Where(o => o.RequestTypeId == request.RequestTypeId).FirstOrDefault();
                        if (targetSavingsRequestType.IsLiquidation)
                        {
                            TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == request.TargetGoalId).FirstOrDefault();
                            List<LiquidationTransaction> liquidationTransactions = liquidationTransactionRepository.Find(o => o.RequestId == request.RequestId && o.Status == liquidationStatus && o.Trial < TRANSACTION_TRIAL_LIMIT_VALUE).ToList();
                            if(liquidationTransactions.Count > 0)
                            {
                                foreach (LiquidationTransaction liquidationTransaction in liquidationTransactions)
                                {
                                    string debitAccount = "";
                                    string creditAccount = "";
                                    string creditBankCode = "";
                                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                    {
                                        Amount = Decimal.Round(liquidationTransaction.Amount, 2),
                                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                        ApprovedDate = DateTime.Now,
                                        Narration = "",
                                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                        PostedDate = DateTime.Now,
                                        TargetGoalsId = goal.TargetGoalsId,
                                        TransactionDate = DateTime.Now,
                                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                    };

                                    if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED.ToString())
                                    {
                                        //debit fixed income and credit customer
                                        debitAccount = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                                        creditAccount = goal.LiquidationAccount;
                                        creditBankCode = goal.LiquidationAccountBankCode;
                                        //move customer to flexible
                                        goal.LiquidationMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;
                                        //update goal balance
                                        goal.GoalBalance = goal.GoalBalance + liquidationTransaction.Amount;
                                        //create transaction
                                        transaction.Narration = $"TSA PARTIAL LIQUIDATION";
                                    }
                                    else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.FLEXIBLE.ToString())
                                    {
                                        //debit flexible collection and credit customer
                                        debitAccount = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                                        creditAccount = goal.LiquidationAccount;
                                        creditBankCode = goal.LiquidationAccountBankCode;
                                        //update goal balance
                                        goal.GoalBalance = goal.GoalBalance + liquidationTransaction.Amount;
                                        transaction.Narration = $"TSA PARTIAL LIQUIDATION";
                                    }
                                    else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FIXED_CONTRIBUTED.ToString())
                                    {
                                        //debit fixed income and credit customer
                                        debitAccount = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                                        creditAccount = goal.LiquidationAccount;
                                        creditBankCode = goal.LiquidationAccountBankCode;
                                        //set goal isLiquidated to true if not
                                        goal.IsLiquidated = true;
                                        goal.GoalStatus = Enumerators.STATUS.COMPLETED.ToString();
                                        transaction.Narration = $"TSA FULL LIQUIDATION";
                                    }
                                    else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FLEXIBLE_CONTRIBUTED.ToString())
                                    {
                                        //debit flexible income and credit customer
                                        debitAccount = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                                        creditAccount = goal.LiquidationAccount;
                                        creditBankCode = goal.LiquidationAccountBankCode;
                                        //set goal isLiquidated to true if not
                                        goal.IsLiquidated = true;
                                        goal.GoalStatus = Enumerators.STATUS.COMPLETED.ToString();
                                        transaction.Narration = $"TSA FULL LIQUIDATION";
                                    }
                                    else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FIXED_INTEREST.ToString())
                                    {
                                        //debit fixed interest income and credit customer
                                        debitAccount = FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT;
                                        creditAccount = goal.LiquidationAccount;
                                        creditBankCode = goal.LiquidationAccountBankCode;
                                        //set goal isLiquidated to true if not
                                        goal.IsLiquidated = true;
                                        goal.GoalStatus = Enumerators.STATUS.COMPLETED.ToString();
                                        transaction.Narration = $"TSA FULL INTEREST LIQUIDATION";
                                    }
                                    else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.FULL_FLEXIBLE_INTEREST.ToString())
                                    {
                                        //debit flexible interest income and credit customer
                                        debitAccount = FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT;
                                        creditAccount = goal.LiquidationAccount;
                                        creditBankCode = goal.LiquidationAccountBankCode;
                                        //set goal isLiquidated to true if not
                                        goal.IsLiquidated = true;
                                        goal.GoalStatus = Enumerators.STATUS.COMPLETED.ToString();
                                        transaction.Narration = $"TSA FULL INTEREST LIQUIDATION";
                                    }
                                    else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE.ToString())
                                    {
                                        //debit fixed interest income and credit income
                                        debitAccount = FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT;
                                        creditAccount = FCMB_SAVINGS_ACCOUNT;
                                        creditBankCode = FCMB_BANK_CODE;
                                        transaction.Narration = "TSA LIQUIDATION PENAL CHARGE";
                                        //update goal interest earned
                                        goal.GoalnterestEarned = 0;
                                    }
                                    else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE_BALANCE.ToString())
                                    {
                                        //debit fixed interest income and credit flexible interest
                                        debitAccount = FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT;
                                        creditAccount = FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT;
                                        creditBankCode = FCMB_BANK_CODE;
                                        //update goal interest earned
                                        goal.GoalnterestEarned = 0;
                                        transaction = null;
                                    }
                                    else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED_GOAL_BALANCE.ToString())
                                    {
                                        //debit fixed income and credit flexible income
                                        debitAccount = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                                        creditAccount = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                                        creditBankCode = FCMB_BANK_CODE;
                                        transaction = null;
                                    }

                                    liquidationTransaction.Trial++;

                                    string fullName = "";
                                    if (creditBankCode != FCMB_BANK_CODE)
                                    {
                                        //do name enquiry
                                        ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(creditAccount, creditBankCode, settings);
                                        if (nameEnquiryResult.Flag)
                                        {
                                            FCMBResponse nameEnquiryFCMBResponse = (FCMBResponse)nameEnquiryResult.ReturnedObject;
                                            NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                                            fullName = nameEnquiryResponse.FullName;
                                        }
                                        else
                                        {
                                            //error with name enquiry
                                            //name enquiry Issue
                                            liquidationTransaction.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                            liquidationTransaction.ProcessingStatusDescription = $"name enquiry Issue: Account: { creditAccount}: { JsonConvert.SerializeObject(nameEnquiryResult)}";
                                            liquidationTransaction.LastUpdatedDate = DateTime.Now.Date;
                                            liquidationTransaction.LastUpdatedDateTime = DateTime.Now;
                                            //request.ProcessingResponse = $"name enquiry Issue:Account:{creditAccount}: {JsonConvert.SerializeObject(nameEnquiryResult)}";

                                            continue;
                                        }
                                    }

                                    //perform direct credit
                                    string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{request.RequestId}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                                    string Narration1 = liquidationTransaction.Narration1;
                                    string Narration2 = liquidationTransaction.Narration2;
                                    long milliseconds = DateTime.Now.Ticks;
                                    string requestId = milliseconds.ToString() + transactionId;

                                    ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(debitAccount, creditAccount, decimal.Round(liquidationTransaction.Amount, 2), fullName,
                                        $"{goal.TargetGoalsId}B{goal.CustomerProfileId}B{request.RequestId}", requestId, creditBankCode, Narration1, Narration2, settings);
                                    if (directCreditResult.Flag)
                                    {
                                        FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                                        //DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                                        if (transaction != null)
                                        {
                                            transaction.Narration = $"{transaction.Narration} :({response.StatusMessage})";
                                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                            this.transactionRepository.Add(transaction);
                                        }

                                        liquidationTransaction.Status = Enumerators.STATUS.PROCESSED.ToString();
                                        liquidationTransaction.StatusDescription = "SUCCESS";
                                        liquidationTransaction.ProcessingStatus = Enumerators.STATUS.SUCCESS.ToString();
                                        liquidationTransaction.ProcessingStatusDescription = $"SUCCESS : Credit Account: { creditAccount} Credit bank Code: {creditBankCode} Debit Account:{debitAccount} :({response.StatusMessage})";
                                        liquidationTransaction.LastUpdatedDate = DateTime.Now.Date;
                                        liquidationTransaction.LastUpdatedDateTime = DateTime.Now;



                                        //this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                        //request.ProcessingResponse = JsonConvert.SerializeObject(response);

                                        //request.Status = Enumerators.STATUS.PROCESSED.ToString();
                                        //request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                                    }
                                    else
                                    {
                                        //direct credit issue
                                        ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                                        if (verifyDirectCreditResult.Flag)
                                        {
                                            FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                                            GetTransactionStatusResponse verifyDataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(verifyDCResponse.ResponseData));
                                            if (transaction != null)
                                            {
                                                transaction.Narration = $"{transaction.Narration} :(TransactionId:{verifyDataResponse.TransactionId}|FinacleTranID:{verifyDataResponse.FinacleTranID}|Stan:{verifyDataResponse.Stan})";
                                                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                                this.transactionRepository.Add(transaction);
                                            }

                                            liquidationTransaction.Status = Enumerators.STATUS.PROCESSED.ToString();
                                            liquidationTransaction.StatusDescription = "SUCCESS";
                                            liquidationTransaction.ProcessingStatus = Enumerators.STATUS.SUCCESS.ToString();
                                            liquidationTransaction.ProcessingStatusDescription = $"SUCCESS : Credit Account: { creditAccount} Credit bank Code: {creditBankCode} Debit Account:{debitAccount} :(TransactionId:{verifyDataResponse.TransactionId}|FinacleTranID:{verifyDataResponse.FinacleTranID}|Stan:{verifyDataResponse.Stan})";
                                            liquidationTransaction.LastUpdatedDate = DateTime.Now.Date;
                                            liquidationTransaction.LastUpdatedDateTime = DateTime.Now;

                                            //this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                            //request.ProcessingResponse = JsonConvert.SerializeObject(verifyDCResponse);
                                            //request.Status = Enumerators.STATUS.PROCESSED.ToString();
                                            //request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                                        }
                                        else
                                        {
                                            liquidationTransaction.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                            liquidationTransaction.ProcessingStatusDescription = $"verification issue : Credit Account: { creditAccount} Credit bank Code: {creditBankCode} Debit Account:{debitAccount} :{JsonConvert.SerializeObject(verifyDirectCreditResult)}, Initial Debit: {JsonConvert.SerializeObject(directCreditResult)})";
                                            liquidationTransaction.LastUpdatedDate = DateTime.Now.Date;
                                            liquidationTransaction.LastUpdatedDateTime = DateTime.Now;
                                            //verification issue
                                            //verification issue
                                            //request.Status = Enumerators.STATUS.FAILED.ToString();
                                            //request.ProcessingResponse = $"verification issue: {JsonConvert.SerializeObject(verifyDirectCreditResult)}, Initial Debit: {JsonConvert.SerializeObject(directCreditResult)}";
                                        }
                                    }
                                    
                                }
                                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                                {
                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                    scope.Complete();
                                }
                            }
                            else
                            {
                                //update request as completed
                                request.ProcessingResponse = "Processed";
                                request.Status = Enumerators.STATUS.PROCESSED.ToString();
                                request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();

                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            }
                            

                        }
 
                        
                    }

                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Done";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    //could not load request type                    
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = $"could not load request type";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }


            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessLiquidationRequests()";
            }
            return result;
        }
        private TargetSavingsRequest processGoalBalanceLiquidation_old(List<AppConfigSetting> settings, string TSA_FIXED_MODE_PENAL_CHARGE, TargetSavingsRequest request)
        {
            //process payment
            string FCMB_SAVINGS_ACCOUNT = "";
            TargetGoal goal = targetGoalRepository.Find(o => o.TargetGoalsId == request.TargetGoalId).FirstOrDefault();
            if (goal != null)
            {
                string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                if (hash == goal.HashValue)
                {
                    //calculate liquidation amount
                    decimal amount = request.LiquidationAmount;
                    decimal penalChargeAmount = 0;
                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        decimal interestRate = Convert.ToDecimal(TSA_FIXED_MODE_PENAL_CHARGE) / 100;

                        penalChargeAmount = goal.GoalnterestEarned * interestRate;
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }
                    else
                    {
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }

                    //direct debit 
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string fullName = "";
                    if(goal.LiquidationAccountBankCode != FCMB_BANK_CODE)
                    {
                        ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(goal.LiquidationAccount, goal.LiquidationAccountBankCode, settings);
                        if (nameEnquiryResult.Flag)
                        {
                            FCMBResponse nameEnquiryFCMBResponse = (FCMBResponse)nameEnquiryResult.ReturnedObject;
                            NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                            fullName =nameEnquiryResponse.FullName;
                        }
                        else
                        {
                            //error with name enquiry
                            //name enquiry Issue
                            request.ProcessingResponse = $"name enquiry Issue: {JsonConvert.SerializeObject(nameEnquiryResult)}";
                            return request;
                        }
                    }

                    string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{request.RequestId}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string Narration1 = $"TSA LIQUIDATION FOR {goal.GoalName}";
                    string Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    long milliseconds = DateTime.Now.Ticks;
                    string requestId = milliseconds.ToString() + transactionId;

                    ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, goal.LiquidationAccount, decimal.Round(amount, 2), fullName,
                        $"{goal.TargetGoalsId}B{goal.CustomerProfileId}B{request.RequestId}", requestId, goal.LiquidationAccountBankCode, Narration1, Narration2, settings);
                    if (directCreditResult.Flag)
                    {
                        FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                        DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                        {
                            Amount = Decimal.Round(request.LiquidationAmount, 2),
                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            ApprovedDate = DateTime.Now,
                            Narration = $"TSA LIQUIDATION:(TransactionId:{dataResponse.TransactionId}|FinacleTranID:{dataResponse.FinacleTranID}|Stan:{dataResponse.Stan})",
                            PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            PostedDate = DateTime.Now,
                            TargetGoalsId = goal.TargetGoalsId,
                            TransactionDate = DateTime.Now,
                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                        };
                        goal.GoalBalance = goal.GoalBalance + amount;
                        transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                        this.AddTransaction(transaction);
                        if (penalChargeAmount > 0)
                        {
                            Domain.Models.Common.Transaction penalCharegTransaction = new Domain.Models.Common.Transaction
                            {
                                Amount = Decimal.Round(penalChargeAmount, 2),
                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                ApprovedDate = DateTime.Now,
                                Narration = $"TSA LIQUIDATION PENAL CHARGE FOR {goal.GoalName}",
                                PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                PostedDate = DateTime.Now,
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = DateTime.Now,
                                TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                            };
                            penalCharegTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, penalCharegTransaction);
                            this.AddTransaction(penalCharegTransaction);

                            goal.GoalBalance = goal.GoalBalance + penalChargeAmount;
                            goal.LiquidationMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;

                        }
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        request.ProcessingResponse = JsonConvert.SerializeObject(response);
                        request.Status = Enumerators.STATUS.PROCESSED.ToString();
                        request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                    }
                    else
                    {
                        //direct credit issue
                        ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                        if (verifyDirectCreditResult.Flag)
                        {
                            FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                            GetTransactionStatusResponse verifyDataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(verifyDCResponse.ResponseData));
                            Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                            {
                                Amount = Decimal.Round(request.LiquidationAmount, 2),
                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                ApprovedDate = DateTime.Now,
                                Narration = $"TSA LIQUIDATION:(TransactionId:{verifyDataResponse.TransactionId}|FinacleTranID:{verifyDataResponse.FinacleTranID}|Stan:{verifyDataResponse.Stan})",
                                PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                PostedDate = DateTime.Now,
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = DateTime.Now,
                                TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                            };
                            goal.GoalBalance = goal.GoalBalance + amount;
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);
                            if (penalChargeAmount > 0)
                            {
                                Domain.Models.Common.Transaction penalCharegTransaction = new Domain.Models.Common.Transaction
                                {
                                    Amount = Decimal.Round(penalChargeAmount, 2),
                                    ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    ApprovedDate = DateTime.Now,
                                    Narration = $"TSA LIQUIDATION PENAL CHARGE FOR {goal.GoalName}",
                                    PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    PostedDate = DateTime.Now,
                                    TargetGoalsId = goal.TargetGoalsId,
                                    TransactionDate = DateTime.Now,
                                    TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                };
                                penalCharegTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, penalCharegTransaction);
                                this.AddTransaction(penalCharegTransaction);

                                goal.GoalBalance = goal.GoalBalance - penalChargeAmount;
                                goal.LiquidationMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;

                            }
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            request.ProcessingResponse = JsonConvert.SerializeObject(verifyDCResponse);
                            request.Status = Enumerators.STATUS.PROCESSED.ToString();
                            request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                        }
                        else
                        {
                            //verification issue
                            //verification issue
                            request.Status = Enumerators.STATUS.FAILED.ToString();
                            request.ProcessingResponse = $"verification issue: {JsonConvert.SerializeObject(verifyDirectCreditResult)}, Initial Debit: {JsonConvert.SerializeObject(directCreditResult)}";
                        }
                    }
                    
                }
            }
            else
            {
                //could not load goal
                request.ProcessingResponse = $"could not load goal for this request";
            }
            return request;
        }

        private TargetSavingsRequest processGoalBalanceLiquidation(List<AppConfigSetting> settings, string TSA_FIXED_MODE_PENAL_CHARGE, TargetSavingsRequest request)
        {
            //process payment
            string FCMB_SAVINGS_ACCOUNT = "";
            TargetGoal goal = targetGoalRepository.Find(o => o.TargetGoalsId == request.TargetGoalId).FirstOrDefault();
            if (goal != null)
            {
                string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                if (hash == goal.HashValue)
                {
                    //calculate liquidation amount
                    decimal amount = request.LiquidationAmount;
                    decimal penalChargeAmount = 0;
                    decimal penalChargeAmountBalance = 0;
                    // give customer amount
                    


                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        decimal contributedAmount = goal.TargetAmount - goal.GoalBalance;

                        decimal interestRate = Convert.ToDecimal(TSA_FIXED_MODE_PENAL_CHARGE) / 100;

                        penalChargeAmount = goal.GoalnterestEarned * interestRate;
                        penalChargeAmountBalance = goal.GoalnterestEarned - penalChargeAmount;
                        goal.GoalnterestEarned = 0;
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                        //add penalChargeAmountBalance to goal balance
                        // move customer to flexible mode

                        //sweep log                     
                        //1. move new goal balance to flexible account i.e debit fixed account and credit flexible account
                        //2. move penalChargeAmountBalance to flexible account i.e debit interest fixed and credit flexible account
                        //3. move penalChargeAmount from fixed interest to collection account.
                        DateTime today = DateTime.Now;
                        //Penal Charge amount
                        LiquidationTransaction fixedPenalChargeLiquidationTransaction = new LiquidationTransaction
                        {
                            CreatedDate = today.Date,
                            CreatedDateTime = today,
                            LastUpdatedDate = today.Date,
                            LastUpdatedDateTime = today,
                            Amount = penalChargeAmount,
                            Narration1 = $"TSA LIQUIDATION PENAL CHARGE ON INTEREST FOR {goal.GoalName}",
                            Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                            ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                            ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                            RequestId = request.RequestId,
                            Response = Enumerators.STATUS.NEW.ToString(),
                            Status = Enumerators.STATUS.NEW.ToString(),
                            StatusDescription = Enumerators.STATUS.NEW.ToString(),
                            TransactionType = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE.ToString(),
                            TransactionTypeDescription = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE.ToString(),
                            Trial = 0,

                        };
                        fixedPenalChargeLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, fixedPenalChargeLiquidationTransaction);
                        this.liquidationTransactionRepository.Add(fixedPenalChargeLiquidationTransaction);

                        //penal charge balance
                        LiquidationTransaction fixedPenalChargeBalanceLiquidationTransaction = new LiquidationTransaction
                        {
                            CreatedDate = today.Date,
                            CreatedDateTime = today,
                            LastUpdatedDate = today.Date,
                            LastUpdatedDateTime = today,
                            Amount = penalChargeAmountBalance,
                            Narration1 = $"TSA LIQUIDATION PENAL CHARGE BALANCE ON INTEREST FOR {goal.GoalName}",
                            Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                            ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                            ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                            RequestId = request.RequestId,
                            Response = Enumerators.STATUS.NEW.ToString(),
                            Status = Enumerators.STATUS.NEW.ToString(),
                            StatusDescription = Enumerators.STATUS.NEW.ToString(),
                            TransactionType = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE_BALANCE.ToString(),
                            TransactionTypeDescription = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE_BALANCE.ToString(),
                            Trial = 0,

                        };
                        fixedPenalChargeBalanceLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, fixedPenalChargeBalanceLiquidationTransaction);

                        this.liquidationTransactionRepository.Add(fixedPenalChargeBalanceLiquidationTransaction);

                        //contributed balance
                        LiquidationTransaction fixedGoalBalanceLiquidationTransaction = new LiquidationTransaction
                        {
                            CreatedDate = today.Date,
                            CreatedDateTime = today,
                            LastUpdatedDate = today.Date,
                            LastUpdatedDateTime = today,
                            Amount = contributedAmount - request.LiquidationAmount,
                            Narration1 = $"TSA GOAL BALANCE SWEEP FOR {goal.GoalName}",
                            Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                            ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                            ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                            RequestId = request.RequestId,
                            Response = Enumerators.STATUS.NEW.ToString(),
                            Status = Enumerators.STATUS.NEW.ToString(),
                            StatusDescription = Enumerators.STATUS.NEW.ToString(),
                            TransactionType = Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED_GOAL_BALANCE.ToString(),
                            TransactionTypeDescription = Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED_GOAL_BALANCE.ToString(),
                            Trial = 0,
                            
                        };
                        fixedGoalBalanceLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, fixedGoalBalanceLiquidationTransaction);

                        this.liquidationTransactionRepository.Add(fixedGoalBalanceLiquidationTransaction);



                    }
                    else if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE)
                    {
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }

                    //direct debit 
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string fullName = "";
                    if (goal.LiquidationAccountBankCode != FCMB_BANK_CODE)
                    {
                        ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(goal.LiquidationAccount, goal.LiquidationAccountBankCode, settings);
                        if (nameEnquiryResult.Flag)
                        {
                            FCMBResponse nameEnquiryFCMBResponse = (FCMBResponse)nameEnquiryResult.ReturnedObject;
                            NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                            fullName = nameEnquiryResponse.FullName;
                        }
                        else
                        {
                            //error with name enquiry
                            //name enquiry Issue
                            request.ProcessingResponse = $"name enquiry Issue: {JsonConvert.SerializeObject(nameEnquiryResult)}";
                            return request;
                        }
                    }

                    string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{request.RequestId}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string Narration1 = $"TSA LIQUIDATION FOR {goal.GoalName}";
                    string Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    long milliseconds = DateTime.Now.Ticks;
                    string requestId = milliseconds.ToString() + transactionId;

                    ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, goal.LiquidationAccount, decimal.Round(amount, 2), fullName,
                        $"{goal.TargetGoalsId}B{goal.CustomerProfileId}B{request.RequestId}", requestId, goal.LiquidationAccountBankCode, Narration1, Narration2, settings);
                    if (directCreditResult.Flag)
                    {
                        FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                        //DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                        {
                            Amount = Decimal.Round(amount, 2),
                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            ApprovedDate = DateTime.Now,
                            //Narration = $"TSA LIQUIDATION:(TransactionId:{dataResponse.TransactionId}|FinacleTranID:{dataResponse.FinacleTranID}|Stan:{dataResponse.Stan})",
                            Narration = $"TSA LIQUIDATION:({response.StatusMessage})",
                            PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            PostedDate = DateTime.Now,
                            TargetGoalsId = goal.TargetGoalsId,
                            TransactionDate = DateTime.Now,
                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                        };
                        //add amount to how much is left for customer to contribute
                        goal.GoalBalance = goal.GoalBalance + amount;
                        transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                        this.AddTransaction(transaction);
                        if (penalChargeAmount > 0)
                        {
                            Domain.Models.Common.Transaction penalCharegTransaction = new Domain.Models.Common.Transaction
                            {
                                Amount = Decimal.Round(penalChargeAmount, 2),
                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                ApprovedDate = DateTime.Now,
                                Narration = $"TSA LIQUIDATION PENAL CHARGE FOR {goal.GoalName}",
                                PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                PostedDate = DateTime.Now,
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = DateTime.Now,
                                TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                            };
                            penalCharegTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, penalCharegTransaction);
                            this.AddTransaction(penalCharegTransaction);

                            Domain.Models.Common.Transaction penalCharegbalanceTransaction = new Domain.Models.Common.Transaction
                            {
                                Amount = Decimal.Round(penalChargeAmountBalance, 2),
                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                ApprovedDate = DateTime.Now,
                                Narration = $"TSA LIQUIDATION INTEREST BALANCE ON FIXED LIQUIDATION FOR {goal.GoalName}",
                                PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                PostedDate = DateTime.Now,
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = DateTime.Now,
                                TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                            };
                            penalCharegbalanceTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, penalCharegbalanceTransaction);
                            this.AddTransaction(penalCharegbalanceTransaction);

                            //reduce what the customer needs to pay by the fixed balance interest.
                            goal.GoalBalance = goal.GoalBalance - penalChargeAmountBalance;
                            goal.LiquidationMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;

                        }
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        request.ProcessingResponse = JsonConvert.SerializeObject(response);
                        request.Status = Enumerators.STATUS.PROCESSED.ToString();
                        request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                    }
                    else
                    {
                        //direct credit issue
                        ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                        if (verifyDirectCreditResult.Flag)
                        {
                            FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                            GetTransactionStatusResponse verifyDataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(verifyDCResponse.ResponseData));
                            Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                            {
                                Amount = Decimal.Round(amount, 2),
                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                ApprovedDate = DateTime.Now,
                                Narration = $"TSA LIQUIDATION:(TransactionId:{verifyDataResponse.TransactionId}|FinacleTranID:{verifyDataResponse.FinacleTranID}|Stan:{verifyDataResponse.Stan})",
                                PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                PostedDate = DateTime.Now,
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = DateTime.Now,
                                TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                            };
                            goal.GoalBalance = goal.GoalBalance - amount;
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);
                            if (penalChargeAmount > 0)
                            {
                                Domain.Models.Common.Transaction penalCharegTransaction = new Domain.Models.Common.Transaction
                                {
                                    Amount = Decimal.Round(penalChargeAmount, 2),
                                    ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    ApprovedDate = DateTime.Now,
                                    Narration = $"TSA LIQUIDATION PENAL CHARGE FOR {goal.GoalName}",
                                    PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    PostedDate = DateTime.Now,
                                    TargetGoalsId = goal.TargetGoalsId,
                                    TransactionDate = DateTime.Now,
                                    TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                };
                                penalCharegTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, penalCharegTransaction);
                                this.AddTransaction(penalCharegTransaction);

                                goal.GoalBalance = goal.GoalBalance + penalChargeAmountBalance;
                                goal.LiquidationMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;

                            }
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            request.ProcessingResponse = JsonConvert.SerializeObject(verifyDCResponse);
                            request.Status = Enumerators.STATUS.PROCESSED.ToString();
                            request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                        }
                        else
                        {
                            //verification issue
                            //verification issue
                            request.Status = Enumerators.STATUS.FAILED.ToString();
                            request.ProcessingResponse = $"verification issue: {JsonConvert.SerializeObject(verifyDirectCreditResult)}, Initial Debit: {JsonConvert.SerializeObject(directCreditResult)}";
                        }
                    }

                }
            }
            else
            {
                //could not load goal
                request.ProcessingResponse = $"could not load goal for this request";
            }


            return request;
        }

        public TargetSavingsRequest ProcessInterestLiquidationRequests(List<AppConfigSetting> settings, TargetSavingsRequest request)
        {
            TargetGoal goal = targetGoalRepository.Find(o => o.TargetGoalsId == request.TargetGoalId).FirstOrDefault();
            if (goal != null)
            {
                string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                if (hash == goal.HashValue)
                {

                    //direct debit 
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string fullName = "";
                    if (goal.LiquidationAccountBankCode != FCMB_BANK_CODE)
                    {
                        ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(goal.LiquidationAccount, goal.LiquidationAccountBankCode, settings);
                        if (nameEnquiryResult.Flag)
                        {
                            FCMBResponse nameEnquiryFCMBResponse = (FCMBResponse)nameEnquiryResult.ReturnedObject;
                            NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                            fullName = nameEnquiryResponse.FullName;
                        }
                        else
                        {
                            //error with name enquiry
                            //name enquiry Issue
                            request.ProcessingResponse = $"name enquiry Issue: {JsonConvert.SerializeObject(nameEnquiryResult)}";
                            return request;
                        }
                    }
                    string FCMB_SAVINGS_ACCOUNT = "";
                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }
                    else
                    {
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }

                    string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{request.RequestId}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string Narration1 = $"TSA INTEREST LIQUIDATION FOR {goal.GoalName}";
                    string Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    long milliseconds = DateTime.Now.Ticks;
                    string requestId = milliseconds.ToString() + transactionId;

                    ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, goal.LiquidationAccount, decimal.Round(request.LiquidationAmount, 2), fullName,
                        $"{goal.TargetGoalsId}B{goal.CustomerProfileId}B{request.RequestId}", requestId, goal.LiquidationAccountBankCode, Narration1, Narration2, settings);
                    if (directCreditResult.Flag)
                    {
                        FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                        //DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                        {
                            Amount = Decimal.Round(request.LiquidationAmount, 2),
                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            ApprovedDate = DateTime.Now,
                            Narration = $"TSA INTEREST LIQUIDATION:({response.StatusMessage})",
                            PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            PostedDate = DateTime.Now,
                            TargetGoalsId = goal.TargetGoalsId,
                            TransactionDate = DateTime.Now,
                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                        };
                        transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                        this.AddTransaction(transaction);

                        goal.GoalnterestEarned = goal.GoalnterestEarned - request.LiquidationAmount;
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        request.ProcessingResponse = JsonConvert.SerializeObject(response);
                        request.Status = Enumerators.STATUS.PROCESSED.ToString();
                        request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                    }
                    else
                    {
                        //direct credit issue, verify and post if successful

                        ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                        if (verifyDirectCreditResult.Flag)
                        {
                            FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                            //GetTransactionStatusResponse verifyDataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(verifyDCResponse.ResponseData));
                            Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                            {
                                Amount = Decimal.Round(request.LiquidationAmount, 2),
                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                ApprovedDate = DateTime.Now,
                                Narration = $"TSA INTEREST LIQUIDATION:({verifyDCResponse.StatusMessage})",
                                PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                PostedDate = DateTime.Now,
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = DateTime.Now,
                                TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                            };
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);

                            goal.GoalnterestEarned = goal.GoalnterestEarned - request.LiquidationAmount;
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            request.ProcessingResponse = $"Success: {JsonConvert.SerializeObject(verifyDCResponse)}";
                            request.Status = Enumerators.STATUS.PROCESSED.ToString();
                            request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                        }
                        else
                        {
                            //verification issue
                            request.Status = Enumerators.STATUS.FAILED.ToString();
                            request.ProcessingResponse = $"verification issue:DEBIT:{JsonConvert.SerializeObject(directCreditResult)}  VERIFY:{JsonConvert.SerializeObject(verifyDirectCreditResult)}";
                        }

                    }

                   
                }
            }
            else
            {
                //could not load goal
                request.ProcessingResponse = $"could not load goal for this request";
            }
            return request;
        }

        private TargetSavingsRequest processGoalBalanceLiquidation_V2(List<AppConfigSetting> settings, string TSA_FIXED_MODE_PENAL_CHARGE, TargetSavingsRequest request)
        {
            //process payment
            string FCMB_SAVINGS_ACCOUNT = "";
            TargetGoal goal = targetGoalRepository.Find(o => o.TargetGoalsId == request.TargetGoalId).FirstOrDefault();
            if (goal != null)
            {
                string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                if (hash == goal.HashValue)
                {
                    //calculate liquidation amount
                    decimal amount = request.LiquidationAmount;
                    decimal penalChargeAmount = 0;
                    decimal penalChargeAmountBalance = 0;
                    // give customer amount



                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        decimal contributedAmount = goal.TargetAmount - goal.GoalBalance;

                        decimal interestRate = Convert.ToDecimal(TSA_FIXED_MODE_PENAL_CHARGE) / 100;

                        penalChargeAmount = goal.GoalnterestEarned * interestRate;
                        penalChargeAmountBalance = goal.GoalnterestEarned - penalChargeAmount;
                        goal.GoalnterestEarned = 0;
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                        //add penalChargeAmountBalance to goal balance
                        // move customer to flexible mode

                        //sweep log                     
                        //1. move new goal balance to flexible account i.e debit fixed account and credit flexible account
                        //2. move penalChargeAmountBalance to flexible account i.e debit interest fixed and credit flexible account
                        //3. move penalChargeAmount from fixed interest to collection account.
                        DateTime today = DateTime.Now;
                        //Penal Charge amount
                        LiquidationTransaction fixedPenalChargeLiquidationTransaction = new LiquidationTransaction
                        {
                            CreatedDate = today.Date,
                            CreatedDateTime = today,
                            LastUpdatedDate = today.Date,
                            LastUpdatedDateTime = today,
                            Amount = penalChargeAmount,
                            Narration1 = $"TSA LIQUIDATION PENAL CHARGE ON INTEREST FOR {goal.GoalName}",
                            Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                            ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                            ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                            RequestId = request.RequestId,
                            Response = Enumerators.STATUS.NEW.ToString(),
                            Status = Enumerators.STATUS.NEW.ToString(),
                            StatusDescription = Enumerators.STATUS.NEW.ToString(),
                            TransactionType = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE.ToString(),
                            TransactionTypeDescription = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE.ToString(),
                            Trial = 0,

                        };
                        fixedPenalChargeLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, fixedPenalChargeLiquidationTransaction);
                        this.liquidationTransactionRepository.Add(fixedPenalChargeLiquidationTransaction);

                        //penal charge balance
                        LiquidationTransaction fixedPenalChargeBalanceLiquidationTransaction = new LiquidationTransaction
                        {
                            CreatedDate = today.Date,
                            CreatedDateTime = today,
                            LastUpdatedDate = today.Date,
                            LastUpdatedDateTime = today,
                            Amount = penalChargeAmountBalance,
                            Narration1 = $"TSA LIQUIDATION PENAL CHARGE BALANCE ON INTEREST FOR {goal.GoalName}",
                            Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                            ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                            ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                            RequestId = request.RequestId,
                            Response = Enumerators.STATUS.NEW.ToString(),
                            Status = Enumerators.STATUS.NEW.ToString(),
                            StatusDescription = Enumerators.STATUS.NEW.ToString(),
                            TransactionType = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE_BALANCE.ToString(),
                            TransactionTypeDescription = Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE_BALANCE.ToString(),
                            Trial = 0,

                        };
                        fixedPenalChargeBalanceLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, fixedPenalChargeBalanceLiquidationTransaction);

                        this.liquidationTransactionRepository.Add(fixedPenalChargeBalanceLiquidationTransaction);

                        //contributed balance
                        LiquidationTransaction fixedGoalBalanceLiquidationTransaction = new LiquidationTransaction
                        {
                            CreatedDate = today.Date,
                            CreatedDateTime = today,
                            LastUpdatedDate = today.Date,
                            LastUpdatedDateTime = today,
                            Amount = contributedAmount - request.LiquidationAmount,
                            Narration1 = $"TSA GOAL BALANCE SWEEP FOR {goal.GoalName}",
                            Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}",
                            ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                            ProcessingStatusDescription = Enumerators.STATUS.NEW.ToString(),
                            RequestId = request.RequestId,
                            Response = Enumerators.STATUS.NEW.ToString(),
                            Status = Enumerators.STATUS.NEW.ToString(),
                            StatusDescription = Enumerators.STATUS.NEW.ToString(),
                            TransactionType = Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED_GOAL_BALANCE.ToString(),
                            TransactionTypeDescription = Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED_GOAL_BALANCE.ToString(),
                            Trial = 0,

                        };
                        fixedGoalBalanceLiquidationTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.LIQUIDATION_TRANSACTION, fixedGoalBalanceLiquidationTransaction);

                        this.liquidationTransactionRepository.Add(fixedGoalBalanceLiquidationTransaction);



                    }
                    else if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE)
                    {
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }

                    //direct debit 
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string fullName = "";
                    ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(goal.LiquidationAccount, goal.LiquidationAccountBankCode, settings);
                    if (nameEnquiryResult.Flag)
                    {
                        FCMBResponse nameEnquiryFCMBResponse = (FCMBResponse)nameEnquiryResult.ReturnedObject;
                        NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                        fullName = nameEnquiryResponse.FullName;

                        string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{request.RequestId}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                        string Narration1 = $"TSA LIQUIDATION FOR {goal.GoalName}";
                        string Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                        long milliseconds = DateTime.Now.Ticks;
                        //string requestId = milliseconds.ToString() + transactionId;
                        string instrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                        {
                            Amount = Decimal.Round(amount, 2),
                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            ApprovedDate = DateTime.Now,
                            //Narration = $"TSA LIQUIDATION:(TransactionId:{dataResponse.TransactionId}|FinacleTranID:{dataResponse.FinacleTranID}|Stan:{dataResponse.Stan})",
                            //Narration = $"TSA LIQUIDATION:({response.StatusMessage})",
                            PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            PostedDate = DateTime.Now,
                            TargetGoalsId = goal.TargetGoalsId,
                            TransactionDate = DateTime.Now,
                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                        };
                        bool isSuccessful = false;
                        string processingResponse = "";
                        if (goal.LiquidationAccountBankCode == FCMB_BANK_CODE)
                        {
                            ActionReturn principalPaymentResult = ProcessFCMBToFCMBPayments(transactionId, Narration1, Narration2, goal.LiquidationAccount, goal.LiquidationAccount, amount,
                            fullName, instrumentNo, goal.LiquidationAccountBankCode, settings);
                            if (principalPaymentResult.Flag)
                            {
                                FCMBResponse response = (FCMBResponse)principalPaymentResult.ReturnedObject;
                                processingResponse = JsonConvert.SerializeObject(response);
                                transaction.Narration = $"TSA LIQUIDATION:({response.StatusMessage})";
                                goal.GoalBalance = goal.GoalBalance + amount;
                                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                this.AddTransaction(transaction);
                                isSuccessful = true;
                            }
                            else
                            {                                
                                request.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(principalPaymentResult)}"; ;
                            }
                        }
                        else
                        {
                            //other bank
                            LiquidationServiceRequest liquidationServiceRequest = new LiquidationServiceRequest
                            {

                            };
                            ActionReturn liquidationPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(FCMB_SAVINGS_ACCOUNT, FCMB_BANK_CODE, settings,
                           nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, Narration1, transactionId, amount);

                            if (liquidationPaymentOtherBankResult.Flag)
                            {
                                FCMBResponse response = (FCMBResponse)liquidationPaymentOtherBankResult.ReturnedObject;
                                processingResponse = JsonConvert.SerializeObject(response);
                                transaction.Narration = $"TSA LIQUIDATION:({response.StatusMessage})";
                                goal.GoalBalance = goal.GoalBalance + amount;
                                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                this.AddTransaction(transaction);
                                isSuccessful = true;
                            }
                            else
                            {
                                request.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(liquidationPaymentOtherBankResult)}"; ;
                            }
                        }
                        if (isSuccessful)
                        {
                            if (penalChargeAmount > 0)
                            {
                                Domain.Models.Common.Transaction penalCharegTransaction = new Domain.Models.Common.Transaction
                                {
                                    Amount = Decimal.Round(penalChargeAmount, 2),
                                    ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    ApprovedDate = DateTime.Now,
                                    Narration = $"TSA LIQUIDATION PENAL CHARGE FOR {goal.GoalName}",
                                    PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    PostedDate = DateTime.Now,
                                    TargetGoalsId = goal.TargetGoalsId,
                                    TransactionDate = DateTime.Now,
                                    TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                };
                                penalCharegTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, penalCharegTransaction);
                                this.AddTransaction(penalCharegTransaction);

                                Domain.Models.Common.Transaction penalCharegbalanceTransaction = new Domain.Models.Common.Transaction
                                {
                                    Amount = Decimal.Round(penalChargeAmountBalance, 2),
                                    ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    ApprovedDate = DateTime.Now,
                                    Narration = $"TSA LIQUIDATION INTEREST BALANCE ON FIXED LIQUIDATION FOR {goal.GoalName}",
                                    PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                    PostedDate = DateTime.Now,
                                    TargetGoalsId = goal.TargetGoalsId,
                                    TransactionDate = DateTime.Now,
                                    TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                                };
                                penalCharegbalanceTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, penalCharegbalanceTransaction);
                                this.AddTransaction(penalCharegbalanceTransaction);

                                //reduce what the customer needs to pay by the fixed balance interest.
                                goal.GoalBalance = goal.GoalBalance - penalChargeAmountBalance;
                                goal.LiquidationMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;

                            }
                            
                            request.ProcessingResponse = processingResponse;
                            request.Status = Enumerators.STATUS.PROCESSED.ToString();
                            request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                        }
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    }
                    else
                    {
                        //error with name enquiry
                        //name enquiry Issue
                        request.ProcessingResponse = $"name enquiry Issue: {JsonConvert.SerializeObject(nameEnquiryResult)}";
                        return request;
                    }
                    

                }
            }
            else
            {
                //could not load goal
                request.ProcessingResponse = $"could not load goal for this request";
            }


            return request;
        }

        public TargetSavingsRequest ProcessInterestLiquidationRequests_V2(List<AppConfigSetting> settings, TargetSavingsRequest request)
        {
            TargetGoal goal = targetGoalRepository.Find(o => o.TargetGoalsId == request.TargetGoalId).FirstOrDefault();
            if (goal != null)
            {
                string hash = Util.GetEncryptedHashValue(CONSTANT.TARGET_GOAL, goal);
                if (hash == goal.HashValue)
                {

                    //direct debit 
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string fullName = "";
                    
                    string FCMB_SAVINGS_ACCOUNT = "";
                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }
                    else
                    {
                        FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }

                    bool isSuccessful = false;
                    string processingResponse = "";

                    ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(goal.LiquidationAccount, goal.LiquidationAccountBankCode, settings);
                    if (nameEnquiryResult.Flag)
                    {
                        FCMBResponse nameEnquiryFCMBResponse = (FCMBResponse)nameEnquiryResult.ReturnedObject;
                        NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                        fullName = nameEnquiryResponse.FullName;

                        string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{request.RequestId}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                        string Narration1 = $"TSA INTEREST LIQUIDATION FOR {goal.GoalName}";
                        string Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                        long milliseconds = DateTime.Now.Ticks;
                        string requestId = milliseconds.ToString() + transactionId;
                        string instrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                        {
                            Amount = Decimal.Round(request.LiquidationAmount, 2),
                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            ApprovedDate = DateTime.Now,
                            
                            PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            PostedDate = DateTime.Now,
                            TargetGoalsId = goal.TargetGoalsId,
                            TransactionDate = DateTime.Now,
                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                        };
                        if (goal.LiquidationAccountBankCode != FCMB_BANK_CODE)
                        {
                            ActionReturn interestPaymentResult = ProcessFCMBToFCMBPayments(transactionId, Narration1, Narration2, goal.LiquidationAccount, goal.LiquidationAccount, request.LiquidationAmount,
                            fullName, instrumentNo, goal.LiquidationAccountBankCode, settings);
                            if (interestPaymentResult.Flag)
                            {
                                FCMBResponse response = (FCMBResponse)interestPaymentResult.ReturnedObject;
                                processingResponse = JsonConvert.SerializeObject(response);
                                transaction.Narration = $"TSA INTEREST LIQUIDATION:({response.StatusMessage})";
                                goal.GoalnterestEarned = goal.GoalnterestEarned - request.LiquidationAmount;
                                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                this.AddTransaction(transaction);
                            }
                            else
                            {
                                request.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(interestPaymentResult)}"; ;
                            }
                        }
                        else
                        {
                            //other banks
                            LiquidationServiceRequest liquidationServiceRequest = new LiquidationServiceRequest
                            {

                            };
                            ActionReturn liquidationPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(FCMB_SAVINGS_ACCOUNT, FCMB_BANK_CODE, settings,
                           nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, Narration1, transactionId, request.LiquidationAmount);

                            if (liquidationPaymentOtherBankResult.Flag)
                            {
                                FCMBResponse response = (FCMBResponse)liquidationPaymentOtherBankResult.ReturnedObject;
                                processingResponse = JsonConvert.SerializeObject(response);
                                transaction.Narration = $"TSA LIQUIDATION:({response.StatusMessage})";
                                goal.GoalnterestEarned = goal.GoalnterestEarned - request.LiquidationAmount;
                                transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                this.AddTransaction(transaction);
                                isSuccessful = true;
                            }
                            else
                            {
                                request.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(liquidationPaymentOtherBankResult)}"; ;
                            }

                        }
                        if (isSuccessful)
                        {
                            
                            request.ProcessingResponse = processingResponse;
                            request.Status = Enumerators.STATUS.PROCESSED.ToString();
                            request.ApprovalStatus = Enumerators.APPROVAL_STATUS.TREATED.ToString();
                        }
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    }
                    else
                    {
                        //error with name enquiry
                        //name enquiry Issue
                        request.ProcessingResponse = $"name enquiry Issue: {JsonConvert.SerializeObject(nameEnquiryResult)}";
                        return request;
                    }

                }
            }
            else
            {
                //could not load goal
                request.ProcessingResponse = $"could not load goal for this request";
            }
            return request;
        }

        public ActionReturn ProcessLiquidation_old()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                string TSA_FIXED_MODE_PENAL_CHARGE = settings.Where(o => o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").Select(o => o.Value).FirstOrDefault();
                string TRANSACTION_TRIAL_LIMIT = settings.Where(o => o.Key == "Paystack_TRANSACTION_TRIAL_LIMIT").Select(o => o.Value).FirstOrDefault();
                int TRANSACTION_TRIAL_LIMIT_VALUE = 0;
                bool limitFlag = int.TryParse(TRANSACTION_TRIAL_LIMIT, out TRANSACTION_TRIAL_LIMIT_VALUE);
                if (!limitFlag)
                {
                    TRANSACTION_TRIAL_LIMIT_VALUE = 3;
                }                
                string status = Enumerators.STATUS.NEW.ToString();

                List<LiquidationServiceRequest> liquidationServiceRequests = liquidationServiceRequestRepository.Find(o => o.Status == status && o.Trial < TRANSACTION_TRIAL_LIMIT_VALUE).ToList();
                foreach(LiquidationServiceRequest liquidationServiceRequest in liquidationServiceRequests)
                {
                    liquidationServiceRequest.Trial++;

                    TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == liquidationServiceRequest.TargetId).FirstOrDefault();
                    if(goal != null)
                    {

                        
                        //get source account 
                        string FCMB_SAVINGS_ACCOUNT = "";
                        if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                        {

                            if(liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_INTEREST.ToString())
                            {
                                FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                            }
                            
                        }
                        else
                        {
                            FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                        }

                        ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(goal.LiquidationAccount, goal.LiquidationAccountBankCode, settings);
                        if (nameEnquiryResult.Flag)
                        {
                            FCMBResponse nameEnquiryFCMBResponse = (FCMBResponse)nameEnquiryResult.ReturnedObject;

                            string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{liquidationServiceRequest.Id}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                            string Narration1 = $"TSA {liquidationServiceRequest.RequestType} LIQUIDATION FOR {goal.GoalName}";
                            string Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                            long milliseconds = DateTime.Now.Ticks;
                            string requestId = milliseconds.ToString() + transactionId;

                            bool isTransferSuccessful = false;
                            string sourceNameEnq = "";
                            ActionReturn debitResult = new ActionReturn();

                            if (goal.LiquidationAccountBankCode == FCMB_BANK_CODE)
                            {
                                NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));

                                ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, goal.LiquidationAccount, decimal.Round(liquidationServiceRequest.Amount, 2), nameEnquiryResponse.FullName,
                                    $"{goal.TargetGoalsId}B{goal.CustomerProfileId}B{liquidationServiceRequest.Id}", requestId, goal.LiquidationAccountBankCode, Narration1, Narration2, settings);
                                debitResult = directCreditResult;
                                if (directCreditResult.Flag)
                                {
                                    FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                                    //DirectCreditResponse paymentDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));


                                    //send email

                                    //string paymentDescription = $"DESCRIPTION:(TransactionId:{paymentDataResponse.TransactionId}|FinacleTranID:{paymentDataResponse.FinacleTranID}|Stan:{paymentDataResponse.Stan})";

                                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                    {
                                        Amount = Decimal.Round(liquidationServiceRequest.Amount, 2),
                                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                        ApprovedDate = DateTime.Now,
                                        Narration = $"TSA {liquidationServiceRequest.RequestType} LIQUIDATION:({response.StatusMessage})",
                                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                        PostedDate = DateTime.Now,
                                        TargetGoalsId = goal.TargetGoalsId,
                                        TransactionDate = DateTime.Now,
                                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                    };
                                    transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                    this.AddTransaction(transaction);

                                    if (liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_PRINCIPAL_AND_INTEREST.ToString()
                                        || liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                                    {
                                        goal.IsLiquidated = true;
                                    }

                                    

                                    DateTime transactionDate = DateTime.Now;
                                    liquidationServiceRequest.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(directCreditResult)}";
                                    liquidationServiceRequest.ProcessingStatus = response.StatusMessage;
                                    liquidationServiceRequest.TreatedDate = transactionDate.Date;
                                    liquidationServiceRequest.TreatedDateTime = transactionDate;
                                    liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                                    liquidationServiceRequest.Description = $"SUCCESS";

                                    isTransferSuccessful = true;
                                    //Add accounting entries to the Sweep table
                                }
                                else
                                {
                                    DateTime errorDate = DateTime.Now;
                                    liquidationServiceRequest.TreatedDate = errorDate.Date;
                                    liquidationServiceRequest.TreatedDateTime = errorDate;
                                    liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                    liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(debitResult)}";
                                    liquidationServiceRequest.Description = $"Account was not debited.";
                                }
                            }
                            else
                            {
                                // other bank transafer
                            }

                            if (!isTransferSuccessful)
                            {
                                //do status check

                                //verify transaction status

                                ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                                if (verifyDirectCreditResult.Flag)
                                {
                                    FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                                    //GetTransactionStatusResponse verifyDataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(verifyDCResponse.ResponseData));
                                    //string paymentDescription = $"DESCRIPTION:(TransactionId:{verifyDataResponse.TransactionId}|FinacleTranID:{verifyDataResponse.FinacleTranID}|Stan:{verifyDataResponse.Stan})";

                                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                    {
                                        Amount = Decimal.Round(liquidationServiceRequest.Amount, 2),
                                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                        ApprovedDate = DateTime.Now,
                                        Narration = $"TSA {liquidationServiceRequest.RequestType} LIQUIDATION:({verifyDCResponse.StatusMessage})",
                                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                        PostedDate = DateTime.Now,
                                        TargetGoalsId = goal.TargetGoalsId,
                                        TransactionDate = DateTime.Now,
                                        TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                                    };
                                    transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                                    transactionRepository.Add(transaction);
                                    //goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                    DateTime transactionDate = DateTime.Now;
                                    liquidationServiceRequest.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(debitResult)}";
                                    liquidationServiceRequest.ProcessingStatus = verifyDCResponse.StatusMessage;
                                    liquidationServiceRequest.TreatedDate = transactionDate.Date;
                                    liquidationServiceRequest.TreatedDateTime = transactionDate;
                                    liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                                    liquidationServiceRequest.Description = $"SUCCESS";

                                    if (liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_PRINCIPAL_AND_INTEREST.ToString()
                                        || liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                                    {
                                        goal.IsLiquidated = true;
                                    }

                                    //Add accounting entries to the Sweep table
                                }
                                else
                                {
                                    DateTime errorDate = DateTime.Now;
                                    liquidationServiceRequest.TreatedDate = errorDate.Date;
                                    liquidationServiceRequest.TreatedDateTime = errorDate;
                                    liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                    liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(debitResult)} VERIFY ERROR:{requestId}:{JsonConvert.SerializeObject(verifyDirectCreditResult)} DEST NAME ENQ:{JsonConvert.SerializeObject(nameEnquiryResult)} {sourceNameEnq}";
                                    liquidationServiceRequest.Description = $"Account was not debited.";
                                }
                            }
                        }
                        else
                        {
                            //error with name enquiry
                            //name enquiry Issue
                            liquidationServiceRequest.ProcessingResponse = $"name enquiry Issue: {JsonConvert.SerializeObject(nameEnquiryResult)}";
                            
                        }
                    }
                    else
                    {
                        DateTime errorDate = DateTime.Now;
                        liquidationServiceRequest.TreatedDate = errorDate.Date;
                        liquidationServiceRequest.TreatedDateTime = errorDate;
                        liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                        liquidationServiceRequest.ProcessingResponse = $"An error occurred while fetching investment details for this Goal";
                        liquidationServiceRequest.Description = $"Account was not debited, could not get a reference to the investment";
                    }

                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        scope.Complete();
                    }
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessLiquidation()";
            }
            return result;
        }

        public ActionReturn ProcessLiquidation()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                string TSA_FIXED_MODE_PENAL_CHARGE = settings.Where(o => o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").Select(o => o.Value).FirstOrDefault();
                string TRANSACTION_TRIAL_LIMIT = settings.Where(o => o.Key == "Paystack_TRANSACTION_TRIAL_LIMIT").Select(o => o.Value).FirstOrDefault();
                int TRANSACTION_TRIAL_LIMIT_VALUE = 0;
                bool limitFlag = int.TryParse(TRANSACTION_TRIAL_LIMIT, out TRANSACTION_TRIAL_LIMIT_VALUE);
                if (!limitFlag)
                {
                    TRANSACTION_TRIAL_LIMIT_VALUE = 3;
                }
                string status = Enumerators.STATUS.NEW.ToString();

                List<ActionReturn> multipleActionReturnLog = new List<ActionReturn>();
                Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                int index = 1;

                List<LiquidationServiceRequest> liquidationServiceRequests = liquidationServiceRequestRepository.Find(o => o.Status == status && o.Trial < TRANSACTION_TRIAL_LIMIT_VALUE).ToList();
                foreach (LiquidationServiceRequest liquidationServiceRequest in liquidationServiceRequests)
                {
                    liquidationServiceRequest.Trial++;

                    ActionReturn liquidationResult = new ActionReturn();

                    TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == liquidationServiceRequest.TargetId).FirstOrDefault();
                    if (goal != null)
                    {

                        ActionReturn nameEnquiryResult = FCMBIntegrator.NameEnquiry(goal.LiquidationAccount, goal.LiquidationAccountBankCode, settings);
                        if (nameEnquiryResult.Flag)
                        {
                            FCMBResponse nameEnquiryFCMBResponse = (FCMBResponse)nameEnquiryResult.ReturnedObject;

                            if (liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                            {
                                liquidationResult=Process_FULL_COMPLETED_PRINCIPAL_AND_INTEREST(liquidationServiceRequest, settings, nameEnquiryFCMBResponse);

                            }
                            else if (liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_INTEREST.ToString())
                            {
                                liquidationResult = Process_FULL_COMPLETED_INTEREST(liquidationServiceRequest, settings, nameEnquiryFCMBResponse);
                            }
                            else if (liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                            {
                                liquidationResult = Process_PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST(liquidationServiceRequest, settings, nameEnquiryFCMBResponse);
                            }
                            else if (liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST.ToString())
                            {
                                liquidationResult = Process_FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST(liquidationServiceRequest, settings, nameEnquiryFCMBResponse);
                            }
                            else if (liquidationServiceRequest.RequestType == Enumerators.LIQUIDATION_REQUEST_TYPE.FAILED_INTEREST_PAYMENT.ToString())
                            {
                                liquidationResult = Process_FAILED_INTEREST(liquidationServiceRequest, settings, nameEnquiryFCMBResponse);
                            }

                            if (!liquidationResult.Flag)
                            {                                
                                multipleActionReturnLog.Add(liquidationResult);
                            }
                        }
                        else
                        {
                            //error with name enquiry
                            //name enquiry Issue
                            liquidationServiceRequest.ProcessingResponse = $"name enquiry Issue: {JsonConvert.SerializeObject(nameEnquiryResult)}";

                        }
                    }
                    else
                    {
                        DateTime errorDate = DateTime.Now;
                        liquidationServiceRequest.TreatedDate = errorDate.Date;
                        liquidationServiceRequest.TreatedDateTime = errorDate;
                        liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                        liquidationServiceRequest.ProcessingResponse = $"An error occurred while fetching investment details for this Goal";
                        liquidationServiceRequest.Description = $"Account was not debited, could not get a reference to the investment";
                    }

                    //send email here
                    if (liquidationResult.Flag)
                    {
                        EmailLog emailLog = new EmailLog
                        {
                            EmailTo = goal.CustomerProfile.EmailAddress,
                            EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                            EmailType = "SUCCESS_LIQUIDATION",
                            Status = Enumerators.STATUS.NEW.ToString(),
                        };

                        dictionaryEmailLog.Add(index, emailLog);

                        Dictionary<string, string> emailValues = new Dictionary<string, string>();
                        emailValues.Add("NAME", $"{goal.CustomerProfile.Surname} {goal.CustomerProfile.FirstName}");
                        emailValues.Add("AMOUNT", goal.TargetAmount.ToString("N"));
                        emailValues.Add("STARTDATE", Convert.ToDateTime(goal.GoalStartDate).ToString("MMMM dd, yyyy"));
                        emailValues.Add("ENDDATE", Convert.ToDateTime(goal.GoalEndDate).ToString("MMMM dd, yyyy"));
                        emailValues.Add("FREQUENCY", goal.TargetSavingsFrequency.Frequency);
                        emailValues.Add("PLANNAME", goal.GoalName);

                        dictionaryEmailValues.Add(index, emailValues);
                        index++;
                    }
                    else
                    {
                        if (liquidationServiceRequest.Trial == TRANSACTION_TRIAL_LIMIT_VALUE)
                        {
                            //send email;
                            EmailLog emailLog = new EmailLog
                            {
                                EmailTo = goal.CustomerProfile.EmailAddress,
                                EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                                EmailType = "FAILED_LIQUIDATION",
                                Status = Enumerators.STATUS.NEW.ToString(),
                            };

                            dictionaryEmailLog.Add(index, emailLog);

                            Dictionary<string, string> emailValues = new Dictionary<string, string>();
                            emailValues.Add("NAME", $"{goal.CustomerProfile.Surname} {goal.CustomerProfile.FirstName}");
                            emailValues.Add("AMOUNT", goal.TargetAmount.ToString("N"));
                            emailValues.Add("STARTDATE", Convert.ToDateTime(goal.GoalStartDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("ENDDATE", Convert.ToDateTime(goal.GoalEndDate).ToString("MMMM dd, yyyy"));
                            emailValues.Add("FREQUENCY", goal.TargetSavingsFrequency.Frequency);
                            emailValues.Add("PLANNAME", goal.GoalName);

                            dictionaryEmailValues.Add(index, emailValues);
                            index++;

                        }
                    }
                    //send email;
                    if (dictionaryEmailLog.Count > 0)
                    {
                        result.HasEmail = true;
                        result.EmailValues = dictionaryEmailValues;
                        result.EmailLog = dictionaryEmailLog;
                    }
                    //log internal liquidation errors.
                    if(multipleActionReturnLog.Count > 0)
                    {
                        result.HasMultipleLog = true;
                        result.MultipleLog = multipleActionReturnLog;
                    }

                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Done";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessLiquidation()";
            }
            return result;
        }
        /*
        FULL_COMPLETED_PRINCIPAL_AND_INTEREST,FULL_COMPLETED_INTEREST,         
            PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST, FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST
        */
        private ActionReturn ProcessFCMBToFCMBPayments(string transactionId, string Narration1, string Narration2,string SourceAccount,string destinationAccount,decimal Amount,
            string fullName,string instrumentNumber,string destinationBankCode, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                long milliseconds = DateTime.Now.Ticks;
                string requestId = milliseconds.ToString() + transactionId;
                ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(SourceAccount, destinationAccount, decimal.Round(Amount, 2), fullName,
                                    instrumentNumber, requestId, destinationBankCode, Narration1, Narration2, settings);

                if (directCreditResult.Flag)
                {
                    FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                    result.Flag = true;
                    result.ReturnedObject = response;
                    
                }
                else
                {
                    //verify
                    ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                    if (verifyDirectCreditResult.Flag)
                    {
                        FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                        result.Flag = true;
                        result.ReturnedObject = verifyDCResponse;
                    }
                    else
                    {
                        //transaction failed
                        result.Flag = false;
                        result.ReturnedObject = $"DEBIT ERROR:{JsonConvert.SerializeObject(directCreditResult)} VERIFY ERROR:{requestId}:{JsonConvert.SerializeObject(verifyDirectCreditResult)}";
                    }
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessFCMBToFCMBPayments()";
            }
            return result;
        }

        private ActionReturn ProcessFCMBToOtherBankPayments(string sourceAccoount, string sourceAccountBankCode, List<AppConfigSetting> settings,
            FCMBResponse nameEnquiryFCMBResponse, LiquidationServiceRequest liquidationServiceRequest, string destinationAccount,
            string destinationAccountBankCOde, string Narration1, string transactionId,decimal amount, string FCMB_CHANNEL_CODE="1")
        {
            ActionReturn result = new ActionReturn();
            try
            {

                long milliseconds = DateTime.Now.Ticks;
                string requestId = milliseconds.ToString() + transactionId;

                ActionReturn sourceBankNameEnquiryResult = FCMBIntegrator.NameEnquiry(sourceAccoount, sourceAccountBankCode, settings);
                if (sourceBankNameEnquiryResult.Flag)
                {
                    FCMBResponse sourceNameEnquiryFCMBResponse = (FCMBResponse)sourceBankNameEnquiryResult.ReturnedObject;
                    NameEnquiryResponse sourceNameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(sourceNameEnquiryFCMBResponse.ResponseData));
                    string sourceAccountName = sourceNameEnquiryResponse.FullName;

                    InterBankNameEquiryResponse interBankNameEnquiryResponse = JsonConvert.DeserializeObject<InterBankNameEquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                    InterBankTransferRequest interBankTransferRequest = new InterBankTransferRequest
                    {
                        Amount = decimal.Round(amount, 2),
                        BeneficiaryAccountName = interBankNameEnquiryResponse.AccountName,
                        BeneficiaryAccountNumber = destinationAccount,
                        BeneficiaryBankVerificationNumber = interBankNameEnquiryResponse.BankVerificationNumber,
                        BeneficiaryKYCLevel = interBankNameEnquiryResponse.KYCLevel,
                        ChannelCode = FCMB_CHANNEL_CODE,
                        DestinationInstitutionCode = destinationAccountBankCOde,
                        NameEnquiryRef = interBankNameEnquiryResponse.ReferenceNumber,
                        Narration = Narration1,
                        OriginatorAccountName = sourceAccountName,
                        OriginatorAccountNumber = sourceAccoount,
                        PaymentReference = requestId,
                        Token = "",
                        //Token = "987456",
                        TransactionLocation = "TSA",
                        TransactionDate = DateTime.Now.ToString(),
                        Currency = "NGN"
                    };
                    //987456,  501478, 340272
                    ActionReturn interBankTransferResult = FCMBIntegrator.InterBankTransfer(interBankTransferRequest, settings);
                    if (interBankTransferResult.Flag)
                    {
                        FCMBResponse response = (FCMBResponse)interBankTransferResult.ReturnedObject;
                        result.Flag = true;
                        result.ReturnedObject = response;
                    }
                    else
                    {
                        //verify
                        ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                        if (verifyDirectCreditResult.Flag)
                        {
                            FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                            result.Flag = true;
                            result.ReturnedObject = verifyDCResponse;
                        }
                        else
                        {
                            //transaction failed
                            result.Flag = false;
                            result.ReturnedObject = $"DEBIT ERROR:{JsonConvert.SerializeObject(interBankTransferResult)} VERIFY ERROR:{requestId}:{JsonConvert.SerializeObject(verifyDirectCreditResult)}";
                        }
                    }
                }
                else
                {


                    result.Flag = false;
                    result.ReturnedObject = $"LIQUIDATION SOURCE ACCOUNT NAME ENQUIRY ERROR:{JsonConvert.SerializeObject(sourceBankNameEnquiryResult)} ";
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessFCMBToOtherBankPayments()";
            }
            return result;
        }

        private ActionReturn Process_FULL_COMPLETED_PRINCIPAL_AND_INTEREST(LiquidationServiceRequest liquidationServiceRequest, List<AppConfigSetting> settings,
             FCMBResponse nameEnquiryFCMBResponse)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                //Fixed:debit fixed pool acct for principal and debit fixed interest account
                //Flexible:debit flexible pool acct for principal and debit flexible interest account  
                //set goal isLiquidated to True.
                TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == liquidationServiceRequest.TargetId).FirstOrDefault();
                if (goal != null)
                {
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string principalDebitAccount = "";
                    string interestDebitAccount = "";
                    decimal principalAmount = liquidationServiceRequest.Amount- (goal.TargetAmount - goal.GoalBalance);
                    decimal interestAmount = liquidationServiceRequest.Amount - goal.GoalnterestEarned;
                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        principalDebitAccount = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                    }
                    else
                    {
                        principalDebitAccount = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }

                    NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                    string fullName = nameEnquiryResponse.FullName;
                    //Accounting Entries for principal
                    string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{liquidationServiceRequest.Id}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string Narration1 = $"TSA FULL PRINCIPAL LIQUIDATION FOR {goal.GoalName}";
                    string Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    string instrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                    //Accounting Entries for interest
                    string interestTransactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{liquidationServiceRequest.Id}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string interestNarration1 = $"TSA FULL INTEREST LIQUIDATION FOR {goal.GoalName}";
                    string interestNarration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    string interestInstrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                    {
                        Amount = Decimal.Round(principalAmount, 2),
                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        ApprovedDate = DateTime.Now,                        
                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        PostedDate = DateTime.Now,
                        TargetGoalsId = goal.TargetGoalsId,
                        TransactionDate = DateTime.Now,
                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                    };

                    Domain.Models.Common.Transaction InterestTransaction = new Domain.Models.Common.Transaction
                    {
                        Amount = Decimal.Round(interestAmount, 2),
                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        ApprovedDate = DateTime.Now,                        
                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        PostedDate = DateTime.Now,
                        TargetGoalsId = goal.TargetGoalsId,
                        TransactionDate = DateTime.Now,
                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                    };

                    if (goal.LiquidationAccountBankCode == FCMB_BANK_CODE)
                    {
                        //FCMB to FCMB
                        

                        ActionReturn principalPaymentResult = ProcessFCMBToFCMBPayments(transactionId, Narration1, Narration2, principalDebitAccount, goal.LiquidationAccount, principalAmount,
                            fullName, instrumentNo, goal.LiquidationAccountBankCode, settings);
                        if (principalPaymentResult.Flag)
                        {
                            FCMBResponse principalResponse = (FCMBResponse)principalPaymentResult.ReturnedObject;
                            //success
                            //Log to Transaction Table 
                            transaction.Narration = $"TSA FULL PRINCIPAL LIQUIDATION:({principalResponse.StatusMessage})";
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);

                            //mark goal isLiquidated as true.
                            goal.IsLiquidated = true;

                            

                            ActionReturn interestPaymentResult = ProcessFCMBToFCMBPayments(interestTransactionId, interestNarration1, interestNarration2, interestDebitAccount, goal.LiquidationAccount, interestAmount,
                                fullName, interestInstrumentNo, goal.LiquidationAccountBankCode, settings);
                            if (interestPaymentResult.Flag)
                            {
                                FCMBResponse interestResponse = (FCMBResponse)interestPaymentResult.ReturnedObject;
                                //success
                                //Log to Transaction Table
                                InterestTransaction.Narration = $"TSA FULL INTEREST LIQUIDATION:({interestResponse.StatusMessage})";
                                InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                                this.AddTransaction(InterestTransaction);

                                DateTime transactionDate = DateTime.Now;
                                liquidationServiceRequest.ProcessingResponse = $"SUCCESS:PRINCIPAL:{JsonConvert.SerializeObject(principalPaymentResult)} :INTEREST:{JsonConvert.SerializeObject(interestPaymentResult)}";
                                liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                                liquidationServiceRequest.TreatedDate = transactionDate.Date;
                                liquidationServiceRequest.TreatedDateTime = transactionDate;
                                liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                                liquidationServiceRequest.Description = $"SUCCESS";

                                result.Flag = true;
                                result.FlagDescription= Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                            }
                            else
                            {
                                //failed, log for retry
                                LiquidationServiceRequest newLiquidationServiceRequest = new LiquidationServiceRequest
                                {
                                    Amount = interestAmount,
                                    Description = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.",
                                    ProcessingResponse = Enumerators.STATUS.NEW.ToString(),
                                    ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                    RequestDate = DateTime.Now.Date,
                                    RequestDateTime = DateTime.Now,
                                    RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FAILED_INTEREST_PAYMENT.ToString(),
                                    Status = Enumerators.STATUS.NEW.ToString(),
                                    TargetId = goal.TargetGoalsId,
                                    Trial = 0,

                                };
                                this.liquidationServiceRequestRepository.Add(newLiquidationServiceRequest);
                                liquidationServiceRequest.Description = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                                result.ReturnedObject= $"INTEREST: { JsonConvert.SerializeObject(interestPaymentResult)}";
                                result.Message = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                            }
                            
                        }
                        else
                        {
                            //failed, send email if trial exceeds limit
                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(principalPaymentResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(principalPaymentResult)}";
                            result.Message = $"FULL COMPLETED LIQUIDATION FAILD ON THE PRINCIPAL PAYMENT.";
                        }
                    }
                    else
                    {
                        //FCMB to Other Banks
                        ActionReturn principalPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(principalDebitAccount, FCMB_BANK_CODE, settings,
                            nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, Narration1, transactionId, principalAmount);
                        if (principalPaymentOtherBankResult.Flag)
                        {
                            FCMBResponse principalResponse = (FCMBResponse)principalPaymentOtherBankResult.ReturnedObject;
                            //success
                            //Log to Transaction Table 
                            transaction.Narration = $"TSA FULL PRINCIPAL LIQUIDATION:({principalResponse.StatusMessage})";
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);

                            //mark goal isLiquidated as true.
                            goal.IsLiquidated = true;

                            ActionReturn interestPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(interestDebitAccount, FCMB_BANK_CODE, settings,
                            nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, interestNarration1, interestTransactionId, interestAmount);
                            if (interestPaymentOtherBankResult.Flag)
                            {
                                FCMBResponse interestResponse = (FCMBResponse)interestPaymentOtherBankResult.ReturnedObject;
                                //success
                                //Log to Transaction Table
                                InterestTransaction.Narration = $"TSA FULL INTEREST LIQUIDATION:({interestResponse.StatusMessage})";
                                InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                                this.AddTransaction(InterestTransaction);

                                DateTime transactionDate = DateTime.Now;
                                liquidationServiceRequest.ProcessingResponse = $"SUCCESS:PRINCIPAL:{JsonConvert.SerializeObject(principalPaymentOtherBankResult)} :INTEREST:{JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                                liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                                liquidationServiceRequest.TreatedDate = transactionDate.Date;
                                liquidationServiceRequest.TreatedDateTime = transactionDate;
                                liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                                liquidationServiceRequest.Description = $"SUCCESS";

                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                            else
                            {
                                //failed, log for retry
                                LiquidationServiceRequest newLiquidationServiceRequest = new LiquidationServiceRequest
                                {
                                    Amount = interestAmount,
                                    Description = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.",
                                    ProcessingResponse = Enumerators.STATUS.NEW.ToString(),
                                    ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                    RequestDate = DateTime.Now.Date,
                                    RequestDateTime = DateTime.Now,
                                    RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FAILED_INTEREST_PAYMENT.ToString(),
                                    Status = Enumerators.STATUS.NEW.ToString(),
                                    TargetId = goal.TargetGoalsId,
                                    Trial = 0,

                                };
                                this.liquidationServiceRequestRepository.Add(newLiquidationServiceRequest);
                                liquidationServiceRequest.Description = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                                result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                                result.Message = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                            }
                        }
                        else
                        {
                            //failed, send email if trial exceeds limit
                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(principalPaymentOtherBankResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(principalPaymentOtherBankResult)}";
                            result.Message = $"FULL COMPLETED LIQUIDATION FAILD ON THE PRINCIPAL PAYMENT.";
                        }
                    }
                }
                else
                {
                    //could not load goal
                    DateTime errorDate = DateTime.Now;
                    liquidationServiceRequest.TreatedDate = errorDate.Date;
                    liquidationServiceRequest.TreatedDateTime = errorDate;
                    liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                    liquidationServiceRequest.ProcessingResponse = $"An error occurred while fetching investment details for this Goal";
                    liquidationServiceRequest.Description = $"Account was not debited, could not get a reference to the investment";
                }
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();


            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "Process_FULL_COMPLETED_PRINCIPAL_AND_INTEREST";
            }
            return result;

        }

        private ActionReturn Process_FULL_COMPLETED_INTEREST(LiquidationServiceRequest liquidationServiceRequest, List<AppConfigSetting> settings,
            FCMBResponse nameEnquiryFCMBResponse)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == liquidationServiceRequest.TargetId).FirstOrDefault();
                if(goal != null)
                {
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string interestDebitAccount = "";
                    
                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                    }
                    else
                    {
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }
                    NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                    string fullName = nameEnquiryResponse.FullName;
                    //Accounting Entries for interest
                    string interestTransactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{liquidationServiceRequest.Id}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string interestNarration1 = $"TSA FULL INTEREST LIQUIDATION FOR {goal.GoalName}";
                    string interestNarration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    string interestInstrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                    Domain.Models.Common.Transaction InterestTransaction = new Domain.Models.Common.Transaction
                    {
                        Amount = Decimal.Round(liquidationServiceRequest.Amount, 2),
                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        ApprovedDate = DateTime.Now,
                        
                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        PostedDate = DateTime.Now,
                        TargetGoalsId = goal.TargetGoalsId,
                        TransactionDate = DateTime.Now,
                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                    };

                    if (goal.LiquidationAccountBankCode == FCMB_BANK_CODE)
                    {
                        ActionReturn interestPaymentResult = ProcessFCMBToFCMBPayments(interestTransactionId, interestNarration1, interestNarration2, interestDebitAccount, goal.LiquidationAccount, liquidationServiceRequest.Amount,
                                fullName, interestInstrumentNo, goal.LiquidationAccountBankCode, settings);
                        if (interestPaymentResult.Flag)
                        {
                            FCMBResponse interestResponse = (FCMBResponse)interestPaymentResult.ReturnedObject;
                            //success
                            //Log to Transaction Table
                            InterestTransaction.Narration = $"TSA FULL INTEREST ONLY LIQUIDATION:({interestResponse.StatusMessage})";
                            InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                            this.AddTransaction(InterestTransaction);

                            DateTime transactionDate = DateTime.Now;
                            liquidationServiceRequest.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(interestPaymentResult)}";
                            liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                            liquidationServiceRequest.TreatedDate = transactionDate.Date;
                            liquidationServiceRequest.TreatedDateTime = transactionDate;
                            liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                            liquidationServiceRequest.Description = $"SUCCESS";

                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            //failed, SEND EMAIL IF EXCEEDS TRIAL LIMIT

                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(interestPaymentResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(interestPaymentResult)}";
                            result.Message = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                        }
                        
                    }
                    else
                    {
                        //to other banks
                        ActionReturn interestPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(interestDebitAccount, FCMB_BANK_CODE, settings,
                            nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, interestNarration1, interestTransactionId, liquidationServiceRequest.Amount);

                        if (interestPaymentOtherBankResult.Flag)
                        {
                            FCMBResponse interestResponse = (FCMBResponse)interestPaymentOtherBankResult.ReturnedObject;
                            //success
                            //Log to Transaction Table
                            InterestTransaction.Narration = $"TSA FULL INTEREST ONLY LIQUIDATION:({interestResponse.StatusMessage})";
                            InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                            this.AddTransaction(InterestTransaction);

                            DateTime transactionDate = DateTime.Now;
                            liquidationServiceRequest.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                            liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                            liquidationServiceRequest.TreatedDate = transactionDate.Date;
                            liquidationServiceRequest.TreatedDateTime = transactionDate;
                            liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                            liquidationServiceRequest.Description = $"SUCCESS";

                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            //failed, SEND EMAIL IF EXCEEDS TRIAL LIMIT

                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                            result.Message = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                        }
                    }

                }
                else
                {
                    //could not load goal
                    DateTime errorDate = DateTime.Now;
                    liquidationServiceRequest.TreatedDate = errorDate.Date;
                    liquidationServiceRequest.TreatedDateTime = errorDate;
                    liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                    liquidationServiceRequest.ProcessingResponse = $"An error occurred while fetching investment details for this Goal";
                    liquidationServiceRequest.Description = $"Account was not debited, could not get a reference to the investment";
                }
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "Process_FULL_COMPLETED_INTEREST";
            }
            return result;

        }

        private ActionReturn Process_PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST(LiquidationServiceRequest liquidationServiceRequest, List<AppConfigSetting> settings,
            FCMBResponse nameEnquiryFCMBResponse)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == liquidationServiceRequest.TargetId).FirstOrDefault();
                if (goal != null)
                {
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string principalDebitAccount = "";
                    string interestDebitAccount = "";

                    decimal interestAmount = 0;
                    decimal principalAmount = 0;
                    decimal penalCharge = 0;

                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        principalDebitAccount = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                        string TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT = settings.Where(o => o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").Select(o => o.Value).FirstOrDefault();

                        decimal interestRate = Convert.ToDecimal(TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT) / 100;
                        decimal penalChargeAmount = goal.GoalnterestEarned * interestRate;
                        decimal penalChargeAmountBalance = goal.GoalnterestEarned - penalChargeAmount;

                        
                        if(liquidationServiceRequest.Amount > penalChargeAmountBalance)
                        {
                            principalAmount = liquidationServiceRequest.Amount - penalChargeAmountBalance;
                            interestAmount = penalChargeAmountBalance;
                        }
                        else
                        {
                            principalAmount = 0;
                            interestAmount = liquidationServiceRequest.Amount;
                        }
                        //include penal charge in the transaction entry
                        penalCharge = penalChargeAmount;
                        Domain.Models.Common.Transaction penalChargeTransaction = new Domain.Models.Common.Transaction
                        {
                            Amount = Decimal.Round(penalCharge, 2),
                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            ApprovedDate = DateTime.Now,
                            Narration = $"TSA FIXED LIQUIDATION CHARGE FOR NOT COMPLETED GOAL",
                            PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            PostedDate = DateTime.Now,
                            TargetGoalsId = goal.TargetGoalsId,
                            TransactionDate = DateTime.Now,
                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                        };
                        penalChargeTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, penalChargeTransaction);
                        this.AddTransaction(penalChargeTransaction);

                        //migrate goal to flexible
                        goal.LiquidationMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;
                    }
                    else
                    {
                        principalDebitAccount = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                        if (liquidationServiceRequest.Amount > goal.GoalnterestEarned)
                        {
                            principalAmount = liquidationServiceRequest.Amount - goal.GoalnterestEarned;
                            interestAmount = goal.GoalnterestEarned;
                        }
                        else
                        {
                            principalAmount = 0;
                            interestAmount = liquidationServiceRequest.Amount;
                        }

                    }
                    NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                    string fullName = nameEnquiryResponse.FullName;
                    //Accounting Entries for principal
                    string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{liquidationServiceRequest.Id}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string Narration1 = $"TSA PARTIAL PRINCIPAL LIQUIDATION FOR {goal.GoalName}";
                    string Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    string instrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                    //Accounting Entries for interest
                    string interestTransactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{liquidationServiceRequest.Id}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string interestNarration1 = $"TSA PARTIAL INTEREST LIQUIDATION FOR {goal.GoalName}";
                    string interestNarration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    string interestInstrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                    {
                        Amount = Decimal.Round(principalAmount, 2),
                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        ApprovedDate = DateTime.Now,
                        
                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        PostedDate = DateTime.Now,
                        TargetGoalsId = goal.TargetGoalsId,
                        TransactionDate = DateTime.Now,
                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                    };
                    Domain.Models.Common.Transaction InterestTransaction = new Domain.Models.Common.Transaction
                    {
                        Amount = Decimal.Round(interestAmount, 2),
                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        ApprovedDate = DateTime.Now,
                        
                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        PostedDate = DateTime.Now,
                        TargetGoalsId = goal.TargetGoalsId,
                        TransactionDate = DateTime.Now,
                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                    };

                    if (goal.LiquidationAccountBankCode == FCMB_BANK_CODE)
                    {
                        ActionReturn principalPaymentResult = ProcessFCMBToFCMBPayments(transactionId, Narration1, Narration2, principalDebitAccount, goal.LiquidationAccount, principalAmount,
                            fullName, instrumentNo, goal.LiquidationAccountBankCode, settings);
                        if (principalPaymentResult.Flag)
                        {
                            FCMBResponse principalResponse = (FCMBResponse)principalPaymentResult.ReturnedObject;
                            //success
                            //Log to Transaction Table 
                            transaction.Narration = $"TSA PARTIAL PRINCIPAL LIQUIDATION:({principalResponse.StatusMessage})";
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);


                            ActionReturn interestPaymentResult = ProcessFCMBToFCMBPayments(interestTransactionId, interestNarration1, interestNarration2, interestDebitAccount, goal.LiquidationAccount, interestAmount,
                                fullName, interestInstrumentNo, goal.LiquidationAccountBankCode, settings);
                            if (interestPaymentResult.Flag)
                            {
                                FCMBResponse interestResponse = (FCMBResponse)interestPaymentResult.ReturnedObject;
                                //success
                                //Log to Transaction Table
                                InterestTransaction.Narration = $"TSA PARTIAL INTEREST LIQUIDATION:({interestResponse.StatusMessage})";
                                InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                                this.AddTransaction(InterestTransaction);

                                DateTime transactionDate = DateTime.Now;
                                liquidationServiceRequest.ProcessingResponse = $"SUCCESS:PRINCIPAL:{JsonConvert.SerializeObject(principalPaymentResult)} :INTEREST:{JsonConvert.SerializeObject(interestPaymentResult)}";
                                liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                                liquidationServiceRequest.TreatedDate = transactionDate.Date;
                                liquidationServiceRequest.TreatedDateTime = transactionDate;
                                liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                                liquidationServiceRequest.Description = $"SUCCESS";

                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                            else
                            {
                                //failed, log for retry
                                //revisit this.
                                //failed, log for retry
                                LiquidationServiceRequest newLiquidationServiceRequest = new LiquidationServiceRequest
                                {
                                    Amount = interestAmount,
                                    Description = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.",
                                    ProcessingResponse = Enumerators.STATUS.NEW.ToString(),
                                    ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                    RequestDate = DateTime.Now.Date,
                                    RequestDateTime = DateTime.Now,
                                    RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FAILED_INTEREST_PAYMENT.ToString(),
                                    Status = Enumerators.STATUS.NEW.ToString(),
                                    TargetId = goal.TargetGoalsId,
                                    Trial = 0,

                                };
                                this.liquidationServiceRequestRepository.Add(newLiquidationServiceRequest);
                                //liquidationServiceRequest.RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_INTEREST.ToString();
                                liquidationServiceRequest.Description = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                                //liquidationServiceRequest.Trial = 0;

                                result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(interestPaymentResult)}";
                                result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                            }

                        }
                        else
                        {
                            //failed, send email if trial exceeds limit

                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(principalPaymentResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(principalPaymentResult)}";
                            result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE PRINCIPAL PAYMENT.";
                        }
                    }
                    else
                    {
                        //other bank
                        ActionReturn principalPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(principalDebitAccount, FCMB_BANK_CODE, settings,
                            nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, Narration1, transactionId, principalAmount);
                        if (principalPaymentOtherBankResult.Flag)
                        {
                            FCMBResponse principalResponse = (FCMBResponse)principalPaymentOtherBankResult.ReturnedObject;
                            //success
                            //Log to Transaction Table 
                            transaction.Narration = $"TSA PARTIAL PRINCIPAL LIQUIDATION:({principalResponse.StatusMessage})";
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);

                            ActionReturn interestPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(interestDebitAccount, FCMB_BANK_CODE, settings,
                            nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, Narration1, interestTransactionId, interestAmount);
                            if (interestPaymentOtherBankResult.Flag)
                            {
                                FCMBResponse interestResponse = (FCMBResponse)interestPaymentOtherBankResult.ReturnedObject;
                                //success
                                //Log to Transaction Table
                                InterestTransaction.Narration = $"TSA PARTIAL INTEREST LIQUIDATION:({interestResponse.StatusMessage})";
                                InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                                this.AddTransaction(InterestTransaction);

                                DateTime transactionDate = DateTime.Now;
                                liquidationServiceRequest.ProcessingResponse = $"SUCCESS:PRINCIPAL:{JsonConvert.SerializeObject(principalPaymentOtherBankResult)} :INTEREST:{JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                                liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                                liquidationServiceRequest.TreatedDate = transactionDate.Date;
                                liquidationServiceRequest.TreatedDateTime = transactionDate;
                                liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                                liquidationServiceRequest.Description = $"SUCCESS";

                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                            else
                            {
                                //failed, log for retry
                                //revisit this.
                                //failed, log for retry
                                LiquidationServiceRequest newLiquidationServiceRequest = new LiquidationServiceRequest
                                {
                                    Amount = interestAmount,
                                    Description = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.",
                                    ProcessingResponse = Enumerators.STATUS.NEW.ToString(),
                                    ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                    RequestDate = DateTime.Now.Date,
                                    RequestDateTime = DateTime.Now,
                                    RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FAILED_INTEREST_PAYMENT.ToString(),
                                    Status = Enumerators.STATUS.NEW.ToString(),
                                    TargetId = goal.TargetGoalsId,
                                    Trial = 0,

                                };
                                this.liquidationServiceRequestRepository.Add(newLiquidationServiceRequest);
                                //liquidationServiceRequest.RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_INTEREST.ToString();
                                liquidationServiceRequest.Description = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                                //liquidationServiceRequest.Trial = 0;

                                result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                                result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                            }

                        }
                        else
                        {
                            //failed, send email if trial exceeds limit

                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(principalPaymentOtherBankResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(principalPaymentOtherBankResult)}";
                            result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE PRINCIPAL PAYMENT.";
                        }
                    }

                    
                }
                else
                {
                    //could not load goal
                    DateTime errorDate = DateTime.Now;
                    liquidationServiceRequest.TreatedDate = errorDate.Date;
                    liquidationServiceRequest.TreatedDateTime = errorDate;
                    liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                    liquidationServiceRequest.ProcessingResponse = $"An error occurred while fetching investment details for this Goal";
                    liquidationServiceRequest.Description = $"Account was not debited, could not get a reference to the investment";
                }
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "Process_PARTIAL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST";
            }
            return result;

        }

        private ActionReturn Process_FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST(LiquidationServiceRequest liquidationServiceRequest, List<AppConfigSetting> settings,
            FCMBResponse nameEnquiryFCMBResponse)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == liquidationServiceRequest.TargetId).FirstOrDefault();
                if (goal != null)
                {
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string principalDebitAccount = "";
                    string interestDebitAccount = "";
                    
                    decimal interestAmount = 0;
                    decimal principalAmount = 0; 
                    decimal penalCharge = 0;
                    
                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        principalDebitAccount = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                        string TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT= settings.Where(o => o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").Select(o => o.Value).FirstOrDefault();

                        decimal interestRate = Convert.ToDecimal(TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT) / 100;
                        decimal penalChargeAmount = goal.GoalnterestEarned * interestRate;
                        decimal penalChargeAmountBalance = goal.GoalnterestEarned - penalChargeAmount;

                        principalAmount = goal.TargetAmount - goal.GoalBalance;
                        interestAmount = penalChargeAmountBalance;
                        penalCharge = penalChargeAmount;

                        Domain.Models.Common.Transaction penalChargeTransaction = new Domain.Models.Common.Transaction
                        {
                            Amount = Decimal.Round(penalCharge, 2),
                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            ApprovedDate = DateTime.Now,
                            Narration = $"TSA FIXED LIQUIDATION CHARGE FOR NOT COMPLETED GOAL",
                            PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                            PostedDate = DateTime.Now,
                            TargetGoalsId = goal.TargetGoalsId,
                            TransactionDate = DateTime.Now,
                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                        };
                        penalChargeTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, penalChargeTransaction);
                        this.AddTransaction(penalChargeTransaction);
                    }
                    else
                    {
                        principalDebitAccount = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                        principalAmount = goal.TargetAmount - goal.GoalBalance;
                        interestAmount = goal.GoalnterestEarned;

                    }
                    NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                    string fullName = nameEnquiryResponse.FullName;
                    //Accounting Entries for principal
                    string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{liquidationServiceRequest.Id}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string Narration1 = $"TSA FULL PRINCIPAL LIQUIDATION FOR {goal.GoalName}";
                    string Narration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    string instrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                    //Accounting Entries for interest
                    string interestTransactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{liquidationServiceRequest.Id}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string interestNarration1 = $"TSA FULL INTEREST LIQUIDATION FOR {goal.GoalName}";
                    string interestNarration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    string interestInstrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                    {
                        Amount = Decimal.Round(principalAmount, 2),
                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        ApprovedDate = DateTime.Now,
                        
                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        PostedDate = DateTime.Now,
                        TargetGoalsId = goal.TargetGoalsId,
                        TransactionDate = DateTime.Now,
                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                    };
                    Domain.Models.Common.Transaction InterestTransaction = new Domain.Models.Common.Transaction
                    {
                        Amount = Decimal.Round(interestAmount, 2),
                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        ApprovedDate = DateTime.Now,
                        
                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        PostedDate = DateTime.Now,
                        TargetGoalsId = goal.TargetGoalsId,
                        TransactionDate = DateTime.Now,
                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                    };

                    //mark isLiquidated as True
                    if (goal.LiquidationAccountBankCode == FCMB_BANK_CODE)
                    {
                        //FCMB to FCMB

                        ActionReturn principalPaymentResult = ProcessFCMBToFCMBPayments(transactionId, Narration1, Narration2, principalDebitAccount, goal.LiquidationAccount, principalAmount,
                            fullName, instrumentNo, goal.LiquidationAccountBankCode, settings);
                        if (principalPaymentResult.Flag)
                        {
                            FCMBResponse principalResponse = (FCMBResponse)principalPaymentResult.ReturnedObject;
                            //success
                            //Log to Transaction Table 
                            transaction.Narration = $"TSA FULL PRINCIPAL LIQUIDATION:({principalResponse.StatusMessage})";
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);

                            //mark goal isLiquidated as true.
                            goal.IsLiquidated = true;



                            ActionReturn interestPaymentResult = ProcessFCMBToFCMBPayments(interestTransactionId, interestNarration1, interestNarration2, interestDebitAccount, goal.LiquidationAccount, interestAmount,
                                fullName, interestInstrumentNo, goal.LiquidationAccountBankCode, settings);
                            if (interestPaymentResult.Flag)
                            {
                                FCMBResponse interestResponse = (FCMBResponse)interestPaymentResult.ReturnedObject;
                                //success
                                //Log to Transaction Table
                                InterestTransaction.Narration = $"TSA FULL INTEREST LIQUIDATION:({interestResponse.StatusMessage})";
                                InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                                this.AddTransaction(InterestTransaction);

                                DateTime transactionDate = DateTime.Now;
                                liquidationServiceRequest.ProcessingResponse = $"SUCCESS:PRINCIPAL:{JsonConvert.SerializeObject(principalPaymentResult)} :INTEREST:{JsonConvert.SerializeObject(interestPaymentResult)}";
                                liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                                liquidationServiceRequest.TreatedDate = transactionDate.Date;
                                liquidationServiceRequest.TreatedDateTime = transactionDate;
                                liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                                liquidationServiceRequest.Description = $"SUCCESS";

                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                            else
                            {
                                //revisit this.
                                //failed, log for retry
                                LiquidationServiceRequest newLiquidationServiceRequest = new LiquidationServiceRequest
                                {
                                    Amount = interestAmount,
                                    Description = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.",
                                    ProcessingResponse = Enumerators.STATUS.NEW.ToString(),
                                    ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                    RequestDate = DateTime.Now.Date,
                                    RequestDateTime = DateTime.Now,
                                    RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FAILED_INTEREST_PAYMENT.ToString(),
                                    Status = Enumerators.STATUS.NEW.ToString(),
                                    TargetId = goal.TargetGoalsId,
                                    Trial = 0,

                                };
                                this.liquidationServiceRequestRepository.Add(newLiquidationServiceRequest);
                                //liquidationServiceRequest.RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_INTEREST.ToString();
                                liquidationServiceRequest.Description = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                                //liquidationServiceRequest.Trial = 0;

                                result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(interestPaymentResult)}";
                                result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                            }
                            
                        }
                        else
                        {
                            //failed, send email if trial exceeds limit

                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(principalPaymentResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(principalPaymentResult)}";
                            result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE PRINCIPAL PAYMENT.";
                        }
                    }
                    else
                    {
                        //to other banks
                        ActionReturn principalPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(principalDebitAccount, FCMB_BANK_CODE, settings,
                            nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, Narration1, transactionId, principalAmount);
                        if (principalPaymentOtherBankResult.Flag)
                        {
                            FCMBResponse principalResponse = (FCMBResponse)principalPaymentOtherBankResult.ReturnedObject;
                            //success
                            //Log to Transaction Table 
                            transaction.Narration = $"TSA FULL PRINCIPAL LIQUIDATION:({principalResponse.StatusMessage})";
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);

                            //mark goal isLiquidated as true.
                            goal.IsLiquidated = true;

                            ActionReturn interestPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(interestDebitAccount, FCMB_BANK_CODE, settings,
                            nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, interestNarration1, interestTransactionId, interestAmount);
                            if (interestPaymentOtherBankResult.Flag)
                            {
                                FCMBResponse interestResponse = (FCMBResponse)interestPaymentOtherBankResult.ReturnedObject;
                                //success
                                //Log to Transaction Table
                                InterestTransaction.Narration = $"TSA FULL INTEREST LIQUIDATION:({interestResponse.StatusMessage})";
                                InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                                this.AddTransaction(InterestTransaction);

                                DateTime transactionDate = DateTime.Now;
                                liquidationServiceRequest.ProcessingResponse = $"SUCCESS:PRINCIPAL:{JsonConvert.SerializeObject(principalPaymentOtherBankResult)} :INTEREST:{JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                                liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                                liquidationServiceRequest.TreatedDate = transactionDate.Date;
                                liquidationServiceRequest.TreatedDateTime = transactionDate;
                                liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                                liquidationServiceRequest.Description = $"SUCCESS";

                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                            else
                            {
                                //revisit this.
                                //failed, log for retry
                                LiquidationServiceRequest newLiquidationServiceRequest = new LiquidationServiceRequest
                                {
                                    Amount = interestAmount,
                                    Description = $"FULL COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.",
                                    ProcessingResponse = Enumerators.STATUS.NEW.ToString(),
                                    ProcessingStatus = Enumerators.STATUS.NEW.ToString(),
                                    RequestDate = DateTime.Now.Date,
                                    RequestDateTime = DateTime.Now,
                                    RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FAILED_INTEREST_PAYMENT.ToString(),
                                    Status = Enumerators.STATUS.NEW.ToString(),
                                    TargetId = goal.TargetGoalsId,
                                    Trial = 0,

                                };
                                this.liquidationServiceRequestRepository.Add(newLiquidationServiceRequest);
                                //liquidationServiceRequest.RequestType = Enumerators.LIQUIDATION_REQUEST_TYPE.FULL_COMPLETED_INTEREST.ToString();
                                liquidationServiceRequest.Description = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                                //liquidationServiceRequest.Trial = 0;

                                result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                                result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                            }

                        }
                        else
                        {
                            // failed, send email if trial exceeds limit

                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(principalPaymentOtherBankResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(principalPaymentOtherBankResult)}";
                            result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE PRINCIPAL PAYMENT.";
                        }
                    }
                }
                else
                {
                    //could not load goal
                    DateTime errorDate = DateTime.Now;
                    liquidationServiceRequest.TreatedDate = errorDate.Date;
                    liquidationServiceRequest.TreatedDateTime = errorDate;
                    liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                    liquidationServiceRequest.ProcessingResponse = $"An error occurred while fetching investment details for this Goal";
                    liquidationServiceRequest.Description = $"Account was not debited, could not get a reference to the investment";
                }
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "Process_FULL_NOT_COMPLETED_PRINCIPAL_AND_INTEREST";
            }
            return result;

        }

        private ActionReturn Process_FAILED_INTEREST(LiquidationServiceRequest liquidationServiceRequest, List<AppConfigSetting> settings,
            FCMBResponse nameEnquiryFCMBResponse)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == liquidationServiceRequest.TargetId).FirstOrDefault();
                if (goal != null)
                {
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                    string interestDebitAccount = "";

                    if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                    {
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                    }
                    else
                    {
                        interestDebitAccount = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    }
                    NameEnquiryResponse nameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(nameEnquiryFCMBResponse.ResponseData));
                    string fullName = nameEnquiryResponse.FullName;
                    //Accounting Entries for interest
                    string interestTransactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{liquidationServiceRequest.Id}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string interestNarration1 = $"TSA INTEREST LIQUIDATION PAYMENT FOR {goal.GoalName}";
                    string interestNarration2 = $"{goal.TargetGoalsId}|{goal.CustomerProfileId}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}";
                    string interestInstrumentNo = (Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1))).ToString();

                    Domain.Models.Common.Transaction InterestTransaction = new Domain.Models.Common.Transaction
                    {
                        Amount = Decimal.Round(liquidationServiceRequest.Amount, 2),
                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        ApprovedDate = DateTime.Now,
                        
                        PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                        PostedDate = DateTime.Now,
                        TargetGoalsId = goal.TargetGoalsId,
                        TransactionDate = DateTime.Now,
                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                    };

                    if (goal.LiquidationAccountBankCode == FCMB_BANK_CODE)
                    {
                        ActionReturn interestPaymentResult = ProcessFCMBToFCMBPayments(interestTransactionId, interestNarration1, interestNarration2, interestDebitAccount, goal.LiquidationAccount, liquidationServiceRequest.Amount,
                                fullName, interestInstrumentNo, goal.LiquidationAccountBankCode, settings);
                        if (interestPaymentResult.Flag)
                        {
                            FCMBResponse interestResponse = (FCMBResponse)interestPaymentResult.ReturnedObject;
                            //success
                            //Log to Transaction Table
                            InterestTransaction.Narration = $"TSA INTEREST LIQUIDATION PAYMENT:({interestResponse.StatusMessage})";
                            InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                            this.AddTransaction(InterestTransaction);

                            DateTime transactionDate = DateTime.Now;
                            liquidationServiceRequest.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(interestPaymentResult)}";
                            liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                            liquidationServiceRequest.TreatedDate = transactionDate.Date;
                            liquidationServiceRequest.TreatedDateTime = transactionDate;
                            liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                            liquidationServiceRequest.Description = $"SUCCESS";

                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            //failed, SEND EMAIL IF EXCEEDS TRIAL LIMIT

                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(interestPaymentResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(interestPaymentResult)}";
                            result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";

                        }

                    }
                    else
                    {
                        //to other banks
                        ActionReturn interestPaymentOtherBankResult = ProcessFCMBToOtherBankPayments(interestDebitAccount, FCMB_BANK_CODE, settings,
                            nameEnquiryFCMBResponse, liquidationServiceRequest, goal.LiquidationAccount, goal.LiquidationAccountBankCode, interestNarration1, interestTransactionId, liquidationServiceRequest.Amount);
                        if (interestPaymentOtherBankResult.Flag)
                        {
                            FCMBResponse interestResponse = (FCMBResponse)interestPaymentOtherBankResult.ReturnedObject;
                            //success
                            //Log to Transaction Table
                            InterestTransaction.Narration = $"TSA INTEREST LIQUIDATION PAYMENT:({interestResponse.StatusMessage})";
                            InterestTransaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, InterestTransaction);
                            this.AddTransaction(InterestTransaction);

                            DateTime transactionDate = DateTime.Now;
                            liquidationServiceRequest.ProcessingResponse = $"SUCCESS:{JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                            liquidationServiceRequest.ProcessingStatus = $"SUCCESS";
                            liquidationServiceRequest.TreatedDate = transactionDate.Date;
                            liquidationServiceRequest.TreatedDateTime = transactionDate;
                            liquidationServiceRequest.Status = Enumerators.STATUS.PROCESSED.ToString();
                            liquidationServiceRequest.Description = $"SUCCESS";

                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            //failed, SEND EMAIL IF EXCEEDS TRIAL LIMIT

                            DateTime errorDate = DateTime.Now;
                            liquidationServiceRequest.TreatedDate = errorDate.Date;
                            liquidationServiceRequest.TreatedDateTime = errorDate;
                            liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationServiceRequest.ProcessingResponse = $"DEBIT ERROR:{JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                            liquidationServiceRequest.Description = $"Account was not debited.";

                            result.ReturnedObject = $"INTEREST: { JsonConvert.SerializeObject(interestPaymentOtherBankResult)}";
                            result.Message = $"FULL NON COMPLETED LIQUIDATION FAILD ON THE INTEREST PAYMENT.";
                        }
                    }

                }
                else
                {
                    //could not load goal
                    DateTime errorDate = DateTime.Now;
                    liquidationServiceRequest.TreatedDate = errorDate.Date;
                    liquidationServiceRequest.TreatedDateTime = errorDate;
                    liquidationServiceRequest.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                    liquidationServiceRequest.ProcessingResponse = $"An error occurred while fetching investment details for this Goal";
                    liquidationServiceRequest.Description = $"Account was not debited, could not get a reference to the investment";
                }
                using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                {
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "Process_FAILED_INTEREST";
            }
            return result;

        }

       

        public ActionReturn ChargeCustomerCard_old(DateTime dateTime)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                string CHARGE_URL = settings.Where(o => o.Key == "Paystack_CHARGE_URL").Select(o => o.Value).FirstOrDefault();
                string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();

                ActionReturn pendingGoalsResult = this.GetTargetGoalForTransactionProcessing(dateTime);
                if (pendingGoalsResult.Flag)
                {
                    List<TargetGoal> goals = (List<TargetGoal>)pendingGoalsResult.ReturnedObject;
                    foreach (TargetGoal goal in goals)
                    {
                        string amountString = (Decimal.Round(goal.TargetFrequencyAmount, 2)).ToString();
                        long amount = Convert.ToInt64(amountString) * 100;
                        ChargeCardRequest chargeCardRequest = new ChargeCardRequest
                        {
                            amount = amount,
                            authorization_code = goal.GoalCardTokenDetails,
                            email = goal.CustomerProfile.EmailAddress
                        };
                        string jsonRequest = JsonConvert.SerializeObject(chargeCardRequest);
                        WebHeaderCollection header = new WebHeaderCollection();
                        header.Add("Authorization", $"Bearer {SECRET_KEY}");
                        ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, CHARGE_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                        if (serverResult.Flag)
                        {
                            ChargeCardResponse jsonResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverResult.ReturnedObject));
                            if (jsonResponse.status && jsonResponse.data.status == "success")
                            {
                                //verify transaction
                                VERIFY_URL = $"{VERIFY_URL}/{jsonResponse.data.reference}";
                                ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                                if (serverResult.Flag)
                                {
                                    ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                    if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                    {
                                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                        {
                                            Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                            ApprovedDate = DateTime.Now,
                                            Narration = $"{jsonResponse.data.reference}",
                                            PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                            PostedDate = DateTime.Now,
                                            TargetGoalsId = goal.TargetGoalsId,
                                            TransactionDate = DateTime.Now,
                                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                        };
                                        this.AddTransaction(transaction);
                                        goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                    }

                                }

                            }
                        }
                    }

                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        scope.Complete();
                    }
                }


                result.UpdateReturn = UpdateReturn.ItemUpdated;
                result.Message = "Done";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }
        public ActionReturn PostTransactionNotification(StandingOrderNotification standingOrderNotification)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                
                TargetDebitAccountProfile targetDebitAccountProfile = targetDebitAccontProfileRepository.GetAll()
                    .Where(o => o.MandateId == standingOrderNotification.lineItems[0].mandateId).FirstOrDefault();
                if(targetDebitAccountProfile != null)
                {
                    //verify debit status
                    ActionReturn configs = this.GetAppConfigSettings();
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    ActionReturn verifyResult = Util.ValidateDebitInstruction(standingOrderNotification.lineItems[0].mandateId, standingOrderNotification.lineItems[0].requestId, settings);
                    if (verifyResult.Flag)
                    {
                        ActionReturn targetGoalResult = this.GetTargetGoalByIdSync(targetDebitAccountProfile.TargetGoalId);
                        if (targetGoalResult.Flag)
                        {
                            TargetGoal goal = (TargetGoal)targetGoalResult.ReturnedObject;
                            Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                            {
                                Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.REMITA.ToString(),
                                ApprovedDate = DateTime.Now,
                                Narration = $"{standingOrderNotification.lineItems[0].requestId}",
                                PostedBy = Enumerators.PAYMENT_GATEWAY.REMITA.ToString(),
                                PostedDate = DateTime.Now,
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = DateTime.Now,
                                TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                            };
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            this.AddTransaction(transaction);


                            string status = Enumerators.STATUS.NEW.ToString();
                            TransactionSchedule transactionSchedule = transactionScheduleRepository.GetAll().Where(o => o.TargetGoalId == goal.TargetGoalsId && o.NextRunDate == goal.NextRunDate && o.Status == status && o.DebitType == CONSTANT.BANK_DEBIT_TYPE).FirstOrDefault();
                            if (transactionSchedule != null)
                            {
                                DateTime goalTransactionDate = DateTime.Now;
                                transactionSchedule.DateProcessed = goalTransactionDate.Date;
                                transactionSchedule.DatetimeProcessed = goalTransactionDate;
                                transactionSchedule.ProcessingStatus = Enumerators.STATUS.SUCCESS.ToString();
                                transactionSchedule.ProcessingResponse = $"SUCCESS:{standingOrderNotification}";
                                transactionSchedule.Status = Enumerators.STATUS.PROCESSED.ToString();
                                transactionSchedule.Description = $"SUCCESS";
                            }
                            goal.GoalBalance = goal.GoalBalance - goal.TargetFrequencyAmount;
                            goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                            {
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                scope.Complete();

                                result.UpdateReturn = UpdateReturn.ItemUpdated;
                                result.Message = "Done";
                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                        }
                        else
                        {
                            
                            result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                            result.Message = "Could not find a reference of this mandate.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }
                    }
                    else
                    {
                        result.ReturnedObject = verifyResult.ReturnedObject;
                        result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                        result.Message = "Error verifying debit status.";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                    
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                    result.Message = "Could not find a reference of this mandate Id.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "PostTransactionNotification(StandingOrderNotification standingOrderNotification)";
            }
            return result;
        }

        public ActionReturn PostTransactionNotification_old(StandingOrderNotification standingOrderNotification)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetDebitAccountProfile targetDebitAccountProfile = targetDebitAccontProfileRepository.GetAll()
                    .Where(o => o.MandateId == standingOrderNotification.lineItems[0].mandateId).FirstOrDefault();
                if (targetDebitAccountProfile != null)
                {
                    ActionReturn targetGoalResult = this.GetTargetGoalByIdSync(targetDebitAccountProfile.TargetGoalId);
                    if (targetGoalResult.Flag)
                    {
                        TargetGoal goal = (TargetGoal)targetGoalResult.ReturnedObject;
                        Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                        {
                            Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                            ApprovedBy = Enumerators.PAYMENT_GATEWAY.REMITA.ToString(),
                            ApprovedDate = DateTime.Now,
                            Narration = $"{standingOrderNotification.lineItems[0].requestId}",
                            PostedBy = Enumerators.PAYMENT_GATEWAY.REMITA.ToString(),
                            PostedDate = DateTime.Now,
                            TargetGoalsId = goal.TargetGoalsId,
                            TransactionDate = DateTime.Now,
                            TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                        };
                        this.AddTransaction(transaction);
                        goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                        using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                        {
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            scope.Complete();

                            result.UpdateReturn = UpdateReturn.ItemUpdated;
                            result.Message = "Done";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                        result.Message = "Could not find a reference of this mandate.";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                    result.Message = "Could not find a reference of this mandate Id.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public ActionReturn ProcessDailyTargetSavingsInterest(DateTime today)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    string TSA_FLEXIBLE_MODE_INTEREST = settings.Where(o => o.Key == "TSA_FLEXIBLE_MODE_INTEREST_IN_PERCENT").Select(o => o.Value).FirstOrDefault();
                    string TSA_FIXED_MODE_INTEREST = settings.Where(o => o.Key == "TSA_FIXED_MODE_INTEREST_IN_PERCENT").Select(o => o.Value).FirstOrDefault();
                    string TSA_FIXED_MODE_PENAL_CHARGE = settings.Where(o => o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").Select(o => o.Value).FirstOrDefault();

                    string status = Enumerators.STATUS.PROCESSED.ToString();
                    string completed = Enumerators.STATUS.COMPLETED.ToString();

                    List<TargetGoal> goals = targetGoalRepository.All.Where(o =>!o.IsLiquidated && (o.GoalStatus == status || o.GoalStatus==completed) ).Include(o=>o.Transactions).ToList();
                    foreach(TargetGoal goal in goals)
                    {
                        string creditType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString();
                        string debitType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString();
                        decimal totalCreditAmount = goal.Transactions.Where(o => o.TransactionType == creditType).Sum(o => o.Amount);
                        decimal totalDebitAmount = goal.Transactions.Where(o => o.TransactionType == debitType).Sum(o => o.Amount);
                        decimal transactionBalance = totalCreditAmount - totalDebitAmount;
                        //DateTime today = DateTime.Now;
                        //check if insert already
                        TransactionInterest existingTransactionInterest = transactionInterestRepository.Find(o => o.TargetGoalsId == goal.TargetGoalsId & o.TransactionDate == today.Date).FirstOrDefault();
                        if(existingTransactionInterest == null)
                        {
                            TransactionInterest transactionInterest = new TransactionInterest
                            {
                                Amount = 0,
                                Narration = $"DAILY INTEREST ON BALANCE",
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = today.Date,
                                GoalBalanceAmount = Decimal.Round(transactionBalance, 2),
                                PostedDate = today,
                                Status = Enumerators.STATUS.PROCESSED.ToString()
                            };

                            if (transactionBalance > 0)
                            {
                                // calculate ineterest based on mode
                                decimal interestAmount = 0;
                                decimal interest = 0;
                                if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                                {
                                    interest = Convert.ToDecimal(TSA_FIXED_MODE_INTEREST) / 100;

                                }
                                else if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE)
                                {
                                    interest = Convert.ToDecimal(TSA_FLEXIBLE_MODE_INTEREST) / 100;
                                }

                                interestAmount = (transactionBalance / 365) * interest;
                                transactionInterest.Amount = Decimal.Round(interestAmount, 2);

                            }
                            else
                            {
                                if (goal.GoalStatus == completed)
                                {
                                    continue;
                                }
                            }
                            transactionInterest.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION_INTEREST, transactionInterest);
                            transactionInterestRepository.Add(transactionInterest);

                            Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                            {
                                Amount = Decimal.Round(transactionInterest.Amount, 2),
                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                ApprovedDate = DateTime.Now,
                                Narration = $"DAILY INTEREST EARNED ON BALANCE ({today.Date.ToString("dd-MMM-yyyy")})",
                                PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                PostedDate = DateTime.Now,
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = today,
                                TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                            };
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            transactionRepository.Add(transaction);
                            TargetGoal thisGoal = targetGoalRepository.Find(o => o.TargetGoalsId == goal.TargetGoalsId).FirstOrDefault();
                            if (thisGoal != null)
                            {
                                thisGoal.GoalnterestEarned = thisGoal.GoalnterestEarned + transactionInterest.Amount;
                            }
                            
                            using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                            {
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                scope.Complete();
                            }
                        }

                    }                    

                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Error getting configuration data";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessDailyTargetSavingsInterest()";
            }
            return result;
        }

        public ActionReturn ProcessDailyTargetSavingsInterest_old()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    string TSA_FLEXIBLE_MODE_INTEREST = settings.Where(o => o.Key == "TSA_FLEXIBLE_MODE_INTEREST_IN_PERCENT").Select(o => o.Value).FirstOrDefault();
                    string TSA_FIXED_MODE_INTEREST = settings.Where(o => o.Key == "TSA_FIXED_MODE_INTEREST_IN_PERCENT").Select(o => o.Value).FirstOrDefault();
                    string TSA_FIXED_MODE_PENAL_CHARGE = settings.Where(o => o.Key == "TSA_FIXED_MODE_PENAL_CHARGE_IN_PERCENT").Select(o => o.Value).FirstOrDefault();

                    string status = Enumerators.STATUS.PROCESSED.ToString();

                    List<TargetGoal> goals = targetGoalRepository.All.Where(o => o.GoalStatus == status).Include(o => o.Transactions).ToList();
                    foreach (TargetGoal goal in goals)
                    {
                        string creditType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString();
                        string debitType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString();
                        decimal totalCreditAmount = goal.Transactions.Where(o => o.TransactionType == creditType).Sum(o => o.Amount);
                        decimal totalDebitAmount = goal.Transactions.Where(o => o.TransactionType == debitType).Sum(o => o.Amount);
                        decimal transactionBalance = totalCreditAmount - totalDebitAmount;
                        DateTime today = DateTime.Now;
                        //check if insert already
                        TransactionInterest existingTransactionInterest = transactionInterestRepository.Find(o => o.TargetGoalsId == goal.TargetGoalsId & o.TransactionDate == today.Date).FirstOrDefault();
                        if (existingTransactionInterest == null)
                        {
                            TransactionInterest transactionInterest = new TransactionInterest
                            {
                                Amount = 0,
                                Narration = $"DAILY INTEREST ON BALANCE",
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = today.Date,
                                GoalBalanceAmount = Decimal.Round(transactionBalance, 2),
                                PostedDate = today,
                                Status = Enumerators.STATUS.NEW.ToString()
                            };

                            if (transactionBalance > 0)
                            {
                                // calculate ineterest based on mode
                                decimal interestAmount = 0;
                                decimal interest = 0;
                                if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FIXED)
                                {
                                    interest = Convert.ToDecimal(TSA_FIXED_MODE_INTEREST) / 100;

                                }
                                else if (goal.LiquidationMode == (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE)
                                {
                                    interest = Convert.ToDecimal(TSA_FLEXIBLE_MODE_INTEREST) / 100;
                                }

                                interestAmount = (transactionBalance / 365) * interest;
                                transactionInterest.Amount = Decimal.Round(interestAmount, 2);

                            }
                            transactionInterest.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION_INTEREST, transactionInterest);
                            transactionInterestRepository.Add(transactionInterest);
                        }




                    }
                    using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                    {
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                        scope.Complete();
                    }

                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Error getting configuration data";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }


            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessDailyTargetSavingsInterest()";
            }
            return result;
        }

        public ActionReturn ProcessMonthlyTargetSavingsInterest(DateTime runDate)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    string TSA_FLEXIBLE_MODE_INTEREST = settings.Where(o => o.Key == "TSA_FLEXIBLE_MODE_INTEREST_IN_PERCENT").Select(o => o.Value).FirstOrDefault();
                    string TSA_FIXED_MODE_INTEREST = settings.Where(o => o.Key == "TSA_FIXED_MODE_INTEREST_IN_PERCENT").Select(o => o.Value).FirstOrDefault();
                    

                    string goalStatus = Enumerators.STATUS.PROCESSED.ToString();
                    string status = Enumerators.STATUS.NEW.ToString();
                    //compute last month
                    //get first day of  the month for runDate
                    DateTime firstDayOfRunDate = new DateTime(runDate.Year, runDate.Month, 1);
                    //get any date for last month
                    DateTime anyDateForLastMonth = firstDayOfRunDate.AddDays(-10);
                    //get first and Last date of last month
                    DateTime firstDayOfLastMonth = new DateTime(anyDateForLastMonth.Year, anyDateForLastMonth.Month, 1);
                    DateTime lastDayOfLastMonth = firstDayOfLastMonth.AddMonths(1).AddDays(-1);
                    //------------------

                    List<TransactionInterest> transactionInterests = transactionInterestRepository.All.Where(o => o.Status == status & o.TransactionDate >= firstDayOfLastMonth & o.TransactionDate <= lastDayOfLastMonth).ToList();

                    List<TransactionInterestSummary> transactionInterestSummary = (from i in transactionInterests
                                                                                   group i by i.TargetGoalsId into summary
                                                                                   select new TransactionInterestSummary
                                                                                   {
                                                                                       Amount=summary.Sum(o=>o.Amount),
                                                                                       TargetGoalsId=summary.Key
                                                                                   }
                                                                                  ).ToList();

                    foreach (TransactionInterestSummary summary in transactionInterestSummary)
                    {
                        TargetGoal goal = targetGoalRepository.Get(summary.TargetGoalsId);
                        if(goal != null)
                        {
                            Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                            {
                                Amount = Decimal.Round(summary.Amount, 2),
                                ApprovedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                ApprovedDate = DateTime.Now,
                                Narration = $"MONTHLY INTEREST EARNED ON DAILY BALANCE ({firstDayOfLastMonth.ToString("MMM, yyyy")})",
                                PostedBy = Enumerators.PAYMENT_GATEWAY.FCMB.ToString(),
                                PostedDate = DateTime.Now,
                                TargetGoalsId = goal.TargetGoalsId,
                                TransactionDate = DateTime.Now,
                                TransactionType = Enumerators.TRANSACTION_TYPE.CREDIT.ToString()
                            };
                            transaction.HashValue = Util.GetEncryptedHashValue(CONSTANT.TRANSACTION, transaction);
                            transactionRepository.Add(transaction);
                            goal.GoalnterestEarned = goal.GoalnterestEarned + summary.Amount;
                            List<TransactionInterest> thisTransactionInterests = transactionInterests.Where(o =>o.TargetGoalsId==summary.TargetGoalsId & o.Status == status & o.TransactionDate >= firstDayOfLastMonth & o.TransactionDate <= lastDayOfLastMonth).ToList();
                            foreach(TransactionInterest interest in thisTransactionInterests)
                            {
                                interest.Status = Enumerators.STATUS.PROCESSED.ToString();
                            }
                            using (System.Transactions.TransactionScope scope=new System.Transactions.TransactionScope())
                            {
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                                scope.Complete();
                            }
                        }

                    }

                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Error getting configuration data";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessMonthlyTargetSavingsInterest()";
            }
            return result;
        }

        public ActionReturn ProcessCardPaymentSweep_old(DateTime runDate)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                bool isFlexibleSuccessful = false;
                bool isFixedSuccessful = false;
                string flexibleDesccription = "";
                string fixedDescription = "";
                string otherDescription = "";

                PaymentSweepItem cardPaymentSweepItem = new PaymentSweepItem();
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_FIXED_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();

                    decimal fixedSavingsTotalDailyAmount = 0;
                    decimal flexibleSavingsTotalDailyAmount = 0;
                    //get transaction details
                    string debitType = CONSTANT.CARD_DEBIT_TYPE;
                    string status = Enumerators.STATUS.PROCESSED.ToString();
                    string description = "SUCCESS";
                    int flexibleMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;
                    int fixedMode = (int)Enumerators.LIQUIDATION_MODE.FIXED;
                    string credit = Enumerators.TRANSACTION_TYPE.CREDIT.ToString();
                    List<Transaction> transactions = transactionRepository.Find(o => o.ApprovedDate == runDate && o.TransactionType == credit).ToList();


                    List<TargetGoal> targetGoals = targetGoalRepository.Find(o => o.GoalDebitType == debitType).ToList();
                    List<TransactionSchedule> transactionSchedules = transactionScheduleRepository.Find(o => o.DateProcessed == runDate && o.DebitType == debitType && o.Status == status && o.Description == description).ToList();

                    //get account details
                    string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME = "";
                    string FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME = "";
                    ActionReturn flexibleNameEnquiryResult = FCMBIntegrator.NameEnquiry(FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT, FCMB_BANK_CODE, settings);
                    if (flexibleNameEnquiryResult.Flag)
                    {
                        FCMBResponse flexibleNameEnquiryFCMBResponse = (FCMBResponse)flexibleNameEnquiryResult.ReturnedObject;
                        //NameEnquiryResponse flexibleNameEnquiryResponse = (NameEnquiryResponse)flexibleNameEnquiryFCMBResponse.ResponseData;
                        NameEnquiryResponse flexibleNameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(flexibleNameEnquiryFCMBResponse.ResponseData));
                        FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME = flexibleNameEnquiryResponse.FullName;

                        //get sum for each mode

                        List<TargetGoal> flexibleTargetGoals = targetGoals.Where(o => o.LiquidationMode == flexibleMode).ToList();
                        var flexibleSavingsTotalDailyAmounts = (from sch in transactionSchedules
                                                                join goal in flexibleTargetGoals on sch.TargetGoalId equals goal.TargetGoalsId into scheduleGoals
                                                                from scheduleGoal in scheduleGoals
                                                                join tran in transactions on scheduleGoal.TargetGoalsId equals tran.TargetGoalsId
                                                                into transactionGoals
                                                                select new
                                                                {
                                                                    amount = transactionGoals.Select(o => o.Amount)
                                                                }).ToList();


                        string transactionId = $"{debitType}{flexibleMode}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                        string Narration1 = $"TSA PAYSTACK TRANSACTION SWEEP FOR FLEXIBLE MODE ({runDate.ToString("dd-MMM-yyyy")})";
                        string Narration2 = $"TSA DAILY TRANSACTION SWEEP";
                        long milliseconds = DateTime.Now.Ticks;
                        string requestId = milliseconds.ToString() + transactionId;
                        
                        foreach (var amount in flexibleSavingsTotalDailyAmounts)
                        {
                            flexibleSavingsTotalDailyAmount = flexibleSavingsTotalDailyAmount + Convert.ToDecimal(amount);
                        }

                        ActionReturn directCreditFlexibleResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT, decimal.Round(flexibleSavingsTotalDailyAmount, 2),
                            FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME, $"{debitType}B{flexibleMode}B{milliseconds}", requestId, FCMB_BANK_CODE, Narration1, Narration2, settings);
                        if (directCreditFlexibleResult.Flag)
                        {
                            FCMBResponse response = (FCMBResponse)directCreditFlexibleResult.ReturnedObject;
                            DirectCreditResponse flexibleDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                            isFlexibleSuccessful = true;
                            flexibleDesccription = $"FLEXIBLE DESCRIPTION:(TransactionId:{flexibleDataResponse.TransactionId}|FinacleTranID:{flexibleDataResponse.FinacleTranID}|Stan:{flexibleDataResponse.Stan})";

                        }
                        else
                        {
                            //failed sweep
                            flexibleDesccription = $"FLEXIBLE DESCRIPTION: {JsonConvert.SerializeObject(directCreditFlexibleResult)}";
                        }
                    }
                    else
                    {
                        //error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT
                        flexibleDesccription = $"FLEXIBLE DESCRIPTION:Error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT : {JsonConvert.SerializeObject(flexibleNameEnquiryResult)}";

                    }
                    ActionReturn fixedNameEnquiryResult = FCMBIntegrator.NameEnquiry(FCMB_FIXED_LIQUIDATION_ACCOUNT, FCMB_BANK_CODE, settings);
                    if (fixedNameEnquiryResult.Flag)
                    {
                        FCMBResponse fixedNameEnquiryFCMBResponse = (FCMBResponse)fixedNameEnquiryResult.ReturnedObject;
                        //NameEnquiryResponse fixedNameEnquiryResponse = (NameEnquiryResponse)fixedNameEnquiryFCMBResponse.ResponseData;
                        NameEnquiryResponse fixedNameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(fixedNameEnquiryFCMBResponse.ResponseData));
                        FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME = fixedNameEnquiryResponse.FullName;

                        //get sum for each mode

                        List<TargetGoal> fixedTargetGoals = targetGoals.Where(o => o.LiquidationMode == fixedMode).ToList();
                        var fixedSavingsTotalDailyAmounts = (from sch in transactionSchedules
                                                                join goal in fixedTargetGoals on sch.TargetGoalId equals goal.TargetGoalsId into scheduleGoals
                                                                from scheduleGoal in scheduleGoals
                                                                join tran in transactions on scheduleGoal.TargetGoalsId equals tran.TargetGoalsId
                                                                into transactionGoals
                                                                select new
                                                                {
                                                                    amount = transactionGoals.Select(o => o.Amount)
                                                                }).ToList();


                        string transactionId = $"{debitType}{fixedMode}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                        string Narration1 = $"TSA PAYSTACK TRANSACTION SWEEP FOR FIXED MODE ({runDate.ToString("dd-MMM-yyyy")})";
                        string Narration2 = $"TSA DAILY TRANSACTION SWEEP";
                        long milliseconds = DateTime.Now.Ticks;
                        string requestId = milliseconds.ToString() + transactionId;
                        
                        foreach (var amount in fixedSavingsTotalDailyAmounts)
                        {
                            fixedSavingsTotalDailyAmount = fixedSavingsTotalDailyAmount + Convert.ToDecimal(amount);
                        }

                        ActionReturn directCreditFixedResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, FCMB_FIXED_LIQUIDATION_ACCOUNT, decimal.Round(fixedSavingsTotalDailyAmount, 2),
                            FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME, $"{debitType}B{flexibleMode}B{milliseconds}", requestId, FCMB_BANK_CODE, Narration1, Narration2, settings);
                        if (directCreditFixedResult.Flag)
                        {
                            FCMBResponse response = (FCMBResponse)directCreditFixedResult.ReturnedObject;
                            DirectCreditResponse fixedDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                            //send email
                            isFixedSuccessful = true;
                            fixedDescription = $"FIXED DESCRIPTION:(TransactionId:{fixedDataResponse.TransactionId}|FinacleTranID:{fixedDataResponse.FinacleTranID}|Stan:{fixedDataResponse.Stan})";
                        }
                        else
                        {
                            //failed sweep
                            fixedDescription = $"FIXED DESCRIPTION: {JsonConvert.SerializeObject(directCreditFixedResult)}";
                        }
                    }
                    else
                    {
                        //error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT
                        fixedDescription = $"FIXED DESCRIPTION:Error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT : {JsonConvert.SerializeObject(fixedNameEnquiryResult)}";

                    }
                    cardPaymentSweepItem.DateSwept = DateTime.Now.Date;
                    cardPaymentSweepItem.DateTimeSwept = DateTime.Now;
                    cardPaymentSweepItem.FixedAccount = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                    cardPaymentSweepItem.FixedAmount = fixedSavingsTotalDailyAmount;
                    cardPaymentSweepItem.FlexibleAccount = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                    cardPaymentSweepItem.FlexibleAmount = flexibleSavingsTotalDailyAmount;
                    cardPaymentSweepItem.SweepAccount = FCMB_SAVINGS_ACCOUNT;
                    cardPaymentSweepItem.SweepDate = runDate;
                    cardPaymentSweepItem.TotalAmount = fixedSavingsTotalDailyAmount + flexibleSavingsTotalDailyAmount;

                    //email
                    Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                    Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                    //send email;
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailType = "SWEEP_STATUS",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };

                    dictionaryEmailLog.Add(0, emailLog);

                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                    emailValues.Add("FLEXIBLE_AMOUNT", cardPaymentSweepItem.FlexibleAmount.ToString("N"));
                    emailValues.Add("FIXED_AMOUNT", cardPaymentSweepItem.FixedAmount.ToString("N"));
                    emailValues.Add("RUN_DATE", cardPaymentSweepItem.SweepDate.ToString("MMMM dd, yyyy"));
                    emailValues.Add("FIXED_ACCOUNT", cardPaymentSweepItem.FixedAccount);
                    emailValues.Add("FLEXIBLE_ACCOUNT", cardPaymentSweepItem.FlexibleAccount);
                    emailValues.Add("SWEEP_ACCOUNT", cardPaymentSweepItem.SweepAccount);
                    emailValues.Add("STATUS", cardPaymentSweepItem.Status);

                    dictionaryEmailValues.Add(0, emailValues);

                    result.HasEmail = true;
                    result.EmailValues = dictionaryEmailValues;
                    result.EmailLog = dictionaryEmailLog;
                }
                else
                {
                    otherDescription = $"CONFIG DESCRIPTION: {JsonConvert.SerializeObject(configs)}";
                }
                cardPaymentSweepItem.Description = $"{flexibleDesccription} {fixedDescription} {otherDescription}";

                if(isFixedSuccessful && isFlexibleSuccessful)
                {
                    cardPaymentSweepItem.Status = Enumerators.SWEEP_STATUS.FULL_SWEEP.ToString();
                }
                else if(isFixedSuccessful && !isFlexibleSuccessful)
                {
                    cardPaymentSweepItem.Status = Enumerators.SWEEP_STATUS.PARTIAL_SWEEP_FLEXIBLE_FAILED.ToString();
                }
                else if (!isFixedSuccessful && isFlexibleSuccessful)
                {
                    cardPaymentSweepItem.Status = Enumerators.SWEEP_STATUS.PARTIAL_SWEEP_FIXED_FAILED.ToString();
                }
                else
                {
                    cardPaymentSweepItem.Status = Enumerators.SWEEP_STATUS.SWEEP_FAILED.ToString();
                }

                this.paymentSweepItemRepository.Add(cardPaymentSweepItem);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();



                result.UpdateReturn = UpdateReturn.ItemUpdated;
                result.Message = "Done";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessCardPaymentSweep(DateTime runDate)";
            }
            return result;

        }

        public ActionReturn ProcessCardPaymentSweep_old2(DateTime runDate)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                bool isFlexibleSuccessful = false;
                bool isFixedSuccessful = false;
                string flexibleDesccription = "";
                string fixedDescription = "";
                string otherDescription = "";

                PaymentSweepItem cardPaymentSweepItem = new PaymentSweepItem();
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_FIXED_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();

                    decimal fixedSavingsTotalDailyAmount = 0;
                    decimal flexibleSavingsTotalDailyAmount = 0;
                    //get transaction details
                    string debitType = CONSTANT.CARD_DEBIT_TYPE;
                    string status = Enumerators.STATUS.PROCESSED.ToString();
                    string description = "SUCCESS";
                    int flexibleMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;
                    int fixedMode = (int)Enumerators.LIQUIDATION_MODE.FIXED;
                    string credit = Enumerators.TRANSACTION_TYPE.CREDIT.ToString();
                    List<Transaction> transactions = transactionRepository.Find(o => o.ApprovedDate == runDate && o.TransactionType == credit).ToList();


                    List<TargetGoal> targetGoals = targetGoalRepository.Find(o => o.GoalDebitType == debitType).ToList();
                    List<TransactionSchedule> transactionSchedules = transactionScheduleRepository.Find(o => o.DateProcessed == runDate && o.DebitType == debitType && o.Status == status && o.Description == description).ToList();

                    //get account details
                    string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME = "";
                    string FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME = "";
                    ActionReturn flexibleNameEnquiryResult = FCMBIntegrator.NameEnquiry(FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT, FCMB_BANK_CODE, settings);
                    if (flexibleNameEnquiryResult.Flag)
                    {
                        FCMBResponse flexibleNameEnquiryFCMBResponse = (FCMBResponse)flexibleNameEnquiryResult.ReturnedObject;
                        //NameEnquiryResponse flexibleNameEnquiryResponse = (NameEnquiryResponse)flexibleNameEnquiryFCMBResponse.ResponseData;
                        NameEnquiryResponse flexibleNameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(flexibleNameEnquiryFCMBResponse.ResponseData));
                        FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME = flexibleNameEnquiryResponse.FullName;

                        //get sum for each mode

                        List<TargetGoal> flexibleTargetGoals = targetGoals.Where(o => o.LiquidationMode == flexibleMode).ToList();
                        var flexibleSavingsTotalDailyAmounts = (from sch in transactionSchedules
                                                                join goal in flexibleTargetGoals on sch.TargetGoalId equals goal.TargetGoalsId into scheduleGoals
                                                                from scheduleGoal in scheduleGoals
                                                                join tran in transactions on scheduleGoal.TargetGoalsId equals tran.TargetGoalsId
                                                                into transactionGoals
                                                                select new
                                                                {
                                                                    amount = transactionGoals.Select(o => o.Amount)
                                                                }).ToList();


                        string transactionId = $"{debitType}{flexibleMode}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                        string Narration1 = $"TSA PAYSTACK TRANSACTION SWEEP FOR FLEXIBLE MODE ({runDate.ToString("dd-MMM-yyyy")})";
                        string Narration2 = $"TSA DAILY TRANSACTION SWEEP";
                        long milliseconds = DateTime.Now.Ticks;
                        string requestId = milliseconds.ToString() + transactionId;

                        foreach (var amount in flexibleSavingsTotalDailyAmounts)
                        {
                            flexibleSavingsTotalDailyAmount = flexibleSavingsTotalDailyAmount + Convert.ToDecimal(amount);
                        }

                        ActionReturn directCreditFlexibleResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT, decimal.Round(flexibleSavingsTotalDailyAmount, 2),
                            FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME, $"{debitType}B{flexibleMode}B{milliseconds}", requestId, FCMB_BANK_CODE, Narration1, Narration2, settings);
                        if (directCreditFlexibleResult.Flag)
                        {
                            FCMBResponse response = (FCMBResponse)directCreditFlexibleResult.ReturnedObject;
                            DirectCreditResponse flexibleDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                            isFlexibleSuccessful = true;
                            flexibleDesccription = $"FLEXIBLE DESCRIPTION:(TransactionId:{flexibleDataResponse.TransactionId}|FinacleTranID:{flexibleDataResponse.FinacleTranID}|Stan:{flexibleDataResponse.Stan})";

                        }
                        else
                        {
                            //failed sweep
                            flexibleDesccription = $"FLEXIBLE DESCRIPTION: {JsonConvert.SerializeObject(directCreditFlexibleResult)}";
                        }
                    }
                    else
                    {
                        //error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT
                        flexibleDesccription = $"FLEXIBLE DESCRIPTION:Error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT : {JsonConvert.SerializeObject(flexibleNameEnquiryResult)}";

                    }
                    ActionReturn fixedNameEnquiryResult = FCMBIntegrator.NameEnquiry(FCMB_FIXED_LIQUIDATION_ACCOUNT, FCMB_BANK_CODE, settings);
                    if (fixedNameEnquiryResult.Flag)
                    {
                        FCMBResponse fixedNameEnquiryFCMBResponse = (FCMBResponse)fixedNameEnquiryResult.ReturnedObject;
                        //NameEnquiryResponse fixedNameEnquiryResponse = (NameEnquiryResponse)fixedNameEnquiryFCMBResponse.ResponseData;
                        NameEnquiryResponse fixedNameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(fixedNameEnquiryFCMBResponse.ResponseData));
                        FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME = fixedNameEnquiryResponse.FullName;

                        //get sum for each mode

                        List<TargetGoal> fixedTargetGoals = targetGoals.Where(o => o.LiquidationMode == fixedMode).ToList();
                        var fixedSavingsTotalDailyAmounts = (from sch in transactionSchedules
                                                             join goal in fixedTargetGoals on sch.TargetGoalId equals goal.TargetGoalsId into scheduleGoals
                                                             from scheduleGoal in scheduleGoals
                                                             join tran in transactions on scheduleGoal.TargetGoalsId equals tran.TargetGoalsId
                                                             into transactionGoals
                                                             select new
                                                             {
                                                                 amount = transactionGoals.Select(o => o.Amount)
                                                             }).ToList();


                        string transactionId = $"{debitType}{fixedMode}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                        string Narration1 = $"TSA PAYSTACK TRANSACTION SWEEP FOR FIXED MODE ({runDate.ToString("dd-MMM-yyyy")})";
                        string Narration2 = $"TSA DAILY TRANSACTION SWEEP";
                        long milliseconds = DateTime.Now.Ticks;
                        string requestId = milliseconds.ToString() + transactionId;

                        foreach (var amount in fixedSavingsTotalDailyAmounts)
                        {
                            fixedSavingsTotalDailyAmount = fixedSavingsTotalDailyAmount + Convert.ToDecimal(amount);
                        }

                        ActionReturn directCreditFixedResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, FCMB_FIXED_LIQUIDATION_ACCOUNT, decimal.Round(fixedSavingsTotalDailyAmount, 2),
                            FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME, $"{debitType}B{flexibleMode}B{milliseconds}", requestId, FCMB_BANK_CODE, Narration1, Narration2, settings);
                        if (directCreditFixedResult.Flag)
                        {
                            FCMBResponse response = (FCMBResponse)directCreditFixedResult.ReturnedObject;
                            DirectCreditResponse fixedDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                            //send email
                            isFixedSuccessful = true;
                            fixedDescription = $"FIXED DESCRIPTION:(TransactionId:{fixedDataResponse.TransactionId}|FinacleTranID:{fixedDataResponse.FinacleTranID}|Stan:{fixedDataResponse.Stan})";
                        }
                        else
                        {
                            //failed sweep
                            fixedDescription = $"FIXED DESCRIPTION: {JsonConvert.SerializeObject(directCreditFixedResult)}";
                        }
                    }
                    else
                    {
                        //error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT
                        fixedDescription = $"FIXED DESCRIPTION:Error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT : {JsonConvert.SerializeObject(fixedNameEnquiryResult)}";

                    }
                    cardPaymentSweepItem.DateSwept = DateTime.Now.Date;
                    cardPaymentSweepItem.DateTimeSwept = DateTime.Now;
                    cardPaymentSweepItem.FixedAccount = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                    cardPaymentSweepItem.FixedAmount = fixedSavingsTotalDailyAmount;
                    cardPaymentSweepItem.FlexibleAccount = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                    cardPaymentSweepItem.FlexibleAmount = flexibleSavingsTotalDailyAmount;
                    cardPaymentSweepItem.SweepAccount = FCMB_SAVINGS_ACCOUNT;
                    cardPaymentSweepItem.SweepDate = runDate;
                    cardPaymentSweepItem.TotalAmount = fixedSavingsTotalDailyAmount + flexibleSavingsTotalDailyAmount;

                    //email
                    Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                    Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                    //send email;
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailType = "SWEEP_STATUS",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };

                    dictionaryEmailLog.Add(0, emailLog);

                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                    emailValues.Add("FLEXIBLE_AMOUNT", cardPaymentSweepItem.FlexibleAmount.ToString("N"));
                    emailValues.Add("FIXED_AMOUNT", cardPaymentSweepItem.FixedAmount.ToString("N"));
                    emailValues.Add("RUN_DATE", cardPaymentSweepItem.SweepDate.ToString("MMMM dd, yyyy"));
                    emailValues.Add("FIXED_ACCOUNT", cardPaymentSweepItem.FixedAccount);
                    emailValues.Add("FLEXIBLE_ACCOUNT", cardPaymentSweepItem.FlexibleAccount);
                    emailValues.Add("SWEEP_ACCOUNT", cardPaymentSweepItem.SweepAccount);
                    emailValues.Add("STATUS", cardPaymentSweepItem.Status);

                    dictionaryEmailValues.Add(0, emailValues);

                    result.HasEmail = true;
                    result.EmailValues = dictionaryEmailValues;
                    result.EmailLog = dictionaryEmailLog;
                }
                else
                {
                    otherDescription = $"CONFIG DESCRIPTION: {JsonConvert.SerializeObject(configs)}";
                }
                cardPaymentSweepItem.Description = $"{flexibleDesccription} {fixedDescription} {otherDescription}";

                if (isFixedSuccessful && isFlexibleSuccessful)
                {
                    cardPaymentSweepItem.Status = Enumerators.SWEEP_STATUS.FULL_SWEEP.ToString();
                }
                else if (isFixedSuccessful && !isFlexibleSuccessful)
                {
                    cardPaymentSweepItem.Status = Enumerators.SWEEP_STATUS.PARTIAL_SWEEP_FLEXIBLE_FAILED.ToString();
                }
                else if (!isFixedSuccessful && isFlexibleSuccessful)
                {
                    cardPaymentSweepItem.Status = Enumerators.SWEEP_STATUS.PARTIAL_SWEEP_FIXED_FAILED.ToString();
                }
                else
                {
                    cardPaymentSweepItem.Status = Enumerators.SWEEP_STATUS.SWEEP_FAILED.ToString();
                }

                this.paymentSweepItemRepository.Add(cardPaymentSweepItem);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();



                result.UpdateReturn = UpdateReturn.ItemUpdated;
                result.Message = "Done";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessCardPaymentSweep(DateTime runDate)";
            }
            return result;

        }

        public ActionReturn ProcessPaymentSweep_old(DateTime runDate, string debitType)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                bool isFlexibleSuccessful = false;
                bool isFixedSuccessful = false;
                string flexibleDesccription = "";
                string fixedDescription = "";
                string otherDescription = "";

                PaymentSweepItem paymentSweepItem = new PaymentSweepItem();
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_FIXED_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();

                    decimal fixedSavingsTotalDailyAmount = 0;
                    decimal flexibleSavingsTotalDailyAmount = 0;
                    //get transaction details
                    //string debitType = CONSTANT.BANK_DEBIT_TYPE;
                    string debitTypeDescription = debitType == CONSTANT.BANK_DEBIT_TYPE ? "BANK" : "CARD";
                    string status = Enumerators.STATUS.PROCESSED.ToString();
                    string description = "SUCCESS";
                    int flexibleMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;
                    int fixedMode = (int)Enumerators.LIQUIDATION_MODE.FIXED;
                    string credit = Enumerators.TRANSACTION_TYPE.CREDIT.ToString();
                    List<Transaction> transactions = transactionRepository.Find(o => o.ApprovedDate == runDate && o.TransactionType == credit).ToList();


                    List<TargetGoal> targetGoals = targetGoalRepository.Find(o => o.GoalDebitType == debitType).ToList();
                    List<TransactionSchedule> transactionSchedules = transactionScheduleRepository.Find(o => o.DateProcessed == runDate && o.DebitType == debitType && o.Status == status && o.Description == description).ToList();

                    //get account details
                    string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME = "";
                    string FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME = "";
                    ActionReturn flexibleNameEnquiryResult = FCMBIntegrator.NameEnquiry(FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT, FCMB_BANK_CODE, settings);
                    if (flexibleNameEnquiryResult.Flag)
                    {
                        FCMBResponse flexibleNameEnquiryFCMBResponse = (FCMBResponse)flexibleNameEnquiryResult.ReturnedObject;
                        //NameEnquiryResponse flexibleNameEnquiryResponse = (NameEnquiryResponse)flexibleNameEnquiryFCMBResponse.ResponseData;
                        NameEnquiryResponse flexibleNameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(flexibleNameEnquiryFCMBResponse.ResponseData));
                        FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME = flexibleNameEnquiryResponse.FullName;

                        //get sum for each mode

                        List<TargetGoal> flexibleTargetGoals = targetGoals.Where(o => o.LiquidationMode == flexibleMode).ToList();
                        var flexibleSavingsTotalDailyAmounts = (from sch in transactionSchedules
                                                                join goal in flexibleTargetGoals on sch.TargetGoalId equals goal.TargetGoalsId into scheduleGoals
                                                                from scheduleGoal in scheduleGoals
                                                                join tran in transactions on scheduleGoal.TargetGoalsId equals tran.TargetGoalsId
                                                                into transactionGoals
                                                                select new
                                                                {
                                                                    amount = transactionGoals.Select(o => o.Amount)
                                                                }).ToList();


                        string transactionId = $"{debitType}{flexibleMode}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                        string Narration1 = $"TSA {debitTypeDescription} TRANSACTION SWEEP FOR FLEXIBLE MODE ({runDate.ToString("dd-MMM-yyyy")})";
                        string Narration2 = $"TSA DAILY TRANSACTION SWEEP";
                        long milliseconds = DateTime.Now.Ticks;
                        string requestId = milliseconds.ToString() + transactionId;

                        foreach (var amount in flexibleSavingsTotalDailyAmounts)
                        {
                            flexibleSavingsTotalDailyAmount = flexibleSavingsTotalDailyAmount + Convert.ToDecimal(amount);
                        }

                        ActionReturn directCreditFlexibleResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT, decimal.Round(flexibleSavingsTotalDailyAmount, 2),
                            FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME, $"{debitType}B{flexibleMode}B{milliseconds}", requestId, FCMB_BANK_CODE, Narration1, Narration2, settings);
                        if (directCreditFlexibleResult.Flag)
                        {
                            FCMBResponse response = (FCMBResponse)directCreditFlexibleResult.ReturnedObject;
                            DirectCreditResponse flexibleDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                            isFlexibleSuccessful = true;
                            flexibleDesccription = $"FLEXIBLE DESCRIPTION:(TransactionId:{flexibleDataResponse.TransactionId}|FinacleTranID:{flexibleDataResponse.FinacleTranID}|Stan:{flexibleDataResponse.Stan})";

                        }
                        else
                        {
                            //failed sweep
                            flexibleDesccription = $"FLEXIBLE DESCRIPTION: {JsonConvert.SerializeObject(directCreditFlexibleResult)}";
                        }
                    }
                    else
                    {
                        //error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT
                        flexibleDesccription = $"FLEXIBLE DESCRIPTION:Error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT : {JsonConvert.SerializeObject(flexibleNameEnquiryResult)}";

                    }
                    ActionReturn fixedNameEnquiryResult = FCMBIntegrator.NameEnquiry(FCMB_FIXED_LIQUIDATION_ACCOUNT, FCMB_BANK_CODE, settings);
                    if (fixedNameEnquiryResult.Flag)
                    {
                        FCMBResponse fixedNameEnquiryFCMBResponse = (FCMBResponse)fixedNameEnquiryResult.ReturnedObject;
                        //NameEnquiryResponse fixedNameEnquiryResponse = (NameEnquiryResponse)fixedNameEnquiryFCMBResponse.ResponseData;
                        NameEnquiryResponse fixedNameEnquiryResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(fixedNameEnquiryFCMBResponse.ResponseData));
                        FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME = fixedNameEnquiryResponse.FullName;

                        //get sum for each mode

                        List<TargetGoal> fixedTargetGoals = targetGoals.Where(o => o.LiquidationMode == fixedMode).ToList();
                        var fixedSavingsTotalDailyAmounts = (from sch in transactionSchedules
                                                             join goal in fixedTargetGoals on sch.TargetGoalId equals goal.TargetGoalsId into scheduleGoals
                                                             from scheduleGoal in scheduleGoals
                                                             join tran in transactions on scheduleGoal.TargetGoalsId equals tran.TargetGoalsId
                                                             into transactionGoals
                                                             select new
                                                             {
                                                                 amount = transactionGoals.Select(o => o.Amount)
                                                             }).ToList();


                        string transactionId = $"{debitType}{fixedMode}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                        string Narration1 = $"TSA {debitTypeDescription} TRANSACTION SWEEP FOR FIXED MODE ({runDate.ToString("dd-MMM-yyyy")})";
                        string Narration2 = $"TSA DAILY TRANSACTION SWEEP";
                        long milliseconds = DateTime.Now.Ticks;
                        string requestId = milliseconds.ToString() + transactionId;

                        foreach (var amount in fixedSavingsTotalDailyAmounts)
                        {
                            fixedSavingsTotalDailyAmount = fixedSavingsTotalDailyAmount + Convert.ToDecimal(amount);
                        }

                        ActionReturn directCreditFixedResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, FCMB_FIXED_LIQUIDATION_ACCOUNT, decimal.Round(fixedSavingsTotalDailyAmount, 2),
                            FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME, $"{debitType}B{flexibleMode}B{milliseconds}", requestId, FCMB_BANK_CODE, Narration1, Narration2, settings);
                        if (directCreditFixedResult.Flag)
                        {
                            FCMBResponse response = (FCMBResponse)directCreditFixedResult.ReturnedObject;
                            DirectCreditResponse fixedDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                            //send email
                            isFixedSuccessful = true;
                            fixedDescription = $"FIXED DESCRIPTION:(TransactionId:{fixedDataResponse.TransactionId}|FinacleTranID:{fixedDataResponse.FinacleTranID}|Stan:{fixedDataResponse.Stan})";
                        }
                        else
                        {
                            //failed sweep
                            fixedDescription = $"FIXED DESCRIPTION: {JsonConvert.SerializeObject(directCreditFixedResult)}";
                        }
                    }
                    else
                    {
                        //error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT
                        fixedDescription = $"FIXED DESCRIPTION:Error with name enquiry for FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT : {JsonConvert.SerializeObject(fixedNameEnquiryResult)}";

                    }
                    paymentSweepItem.DateSwept = DateTime.Now.Date;
                    paymentSweepItem.DateTimeSwept = DateTime.Now;
                    paymentSweepItem.FixedAccount = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                    paymentSweepItem.FixedAmount = fixedSavingsTotalDailyAmount;
                    paymentSweepItem.FlexibleAccount = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                    paymentSweepItem.FlexibleAmount = flexibleSavingsTotalDailyAmount;
                    paymentSweepItem.SweepAccount = FCMB_SAVINGS_ACCOUNT;
                    paymentSweepItem.SweepDate = runDate;
                    paymentSweepItem.TotalAmount = fixedSavingsTotalDailyAmount + flexibleSavingsTotalDailyAmount;
                    paymentSweepItem.SweepType = debitTypeDescription;

                    //email
                    Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                    Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                    //send email;
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailType = "SWEEP_STATUS",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };

                    dictionaryEmailLog.Add(0, emailLog);

                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                    emailValues.Add("FLEXIBLE_AMOUNT", paymentSweepItem.FlexibleAmount.ToString("N"));
                    emailValues.Add("FIXED_AMOUNT", paymentSweepItem.FixedAmount.ToString("N"));
                    emailValues.Add("RUN_DATE", paymentSweepItem.SweepDate.ToString("MMMM dd, yyyy"));
                    emailValues.Add("FIXED_ACCOUNT", paymentSweepItem.FixedAccount);
                    emailValues.Add("FLEXIBLE_ACCOUNT", paymentSweepItem.FlexibleAccount);
                    emailValues.Add("SWEEP_ACCOUNT", paymentSweepItem.SweepAccount);
                    emailValues.Add("STATUS", paymentSweepItem.Status);

                    dictionaryEmailValues.Add(0, emailValues);

                    result.HasEmail = true;
                    result.EmailValues = dictionaryEmailValues;
                    result.EmailLog = dictionaryEmailLog;
                }
                else
                {
                    otherDescription = $"CONFIG DESCRIPTION: {JsonConvert.SerializeObject(configs)}";
                }
                paymentSweepItem.Description = $"{flexibleDesccription} {fixedDescription} {otherDescription}";

                if (isFixedSuccessful && isFlexibleSuccessful)
                {
                    paymentSweepItem.Status = Enumerators.SWEEP_STATUS.FULL_SWEEP.ToString();
                }
                else if (isFixedSuccessful && !isFlexibleSuccessful)
                {
                    paymentSweepItem.Status = Enumerators.SWEEP_STATUS.PARTIAL_SWEEP_FLEXIBLE_FAILED.ToString();
                }
                else if (!isFixedSuccessful && isFlexibleSuccessful)
                {
                    paymentSweepItem.Status = Enumerators.SWEEP_STATUS.PARTIAL_SWEEP_FIXED_FAILED.ToString();
                }
                else
                {
                    paymentSweepItem.Status = Enumerators.SWEEP_STATUS.SWEEP_FAILED.ToString();
                }

                this.paymentSweepItemRepository.Add(paymentSweepItem);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();



                result.UpdateReturn = UpdateReturn.ItemUpdated;
                result.Message = "Done";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessPaymentSweep(DateTime runDate)";
            }
            return result;

        }

        private ActionReturn ProcessPaymentSweep(DateTime runDate)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                Task processCardPaymentSweepTask = Task.Factory.StartNew(() => ProcessCardPaymentSweep(runDate, CONSTANT.CARD_DEBIT_TYPE));
                Task processSweepForCardFeesTask = Task.Factory.StartNew(() => ProcessSweepForCardFees(runDate));
                Task.WaitAll(processCardPaymentSweepTask, processSweepForCardFeesTask);

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessPaymentSweep(DateTime runDate)";
            }
            return result;
        }
        public ActionReturn ProcessCardPaymentSweep(DateTime runDate, string debitType)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                bool isFlexibleSuccessful = false;
                bool isFixedSuccessful = false;
                string flexibleDesccription = "";
                string fixedDescription = "";
                string otherDescription = "";

                PaymentSweepItem paymentSweepItem = new PaymentSweepItem();
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_FIXED_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();

                    decimal fixedSavingsTotalDailyAmount = 0;
                    decimal flexibleSavingsTotalDailyAmount = 0;
                    //get transaction details
                    //string debitType = CONSTANT.BANK_DEBIT_TYPE;
                    string debitTypeDescription = debitType == CONSTANT.BANK_DEBIT_TYPE ? "BANK" : "CARD";
                    string status = Enumerators.STATUS.PROCESSED.ToString();
                    string description = "SUCCESS";
                    int flexibleMode = (int)Enumerators.LIQUIDATION_MODE.FLEXIBLE;
                    int fixedMode = (int)Enumerators.LIQUIDATION_MODE.FIXED;
                    string credit = Enumerators.TRANSACTION_TYPE.CREDIT.ToString();
                    List<Transaction> transactions = transactionRepository.Find(o => o.ApprovedDate == runDate && o.TransactionType == credit).ToList();


                    List<TargetGoal> targetGoals = targetGoalRepository.Find(o => o.GoalDebitType == debitType).ToList();
                    List<TransactionSchedule> transactionSchedules = transactionScheduleRepository.Find(o => o.DateProcessed == runDate && o.DebitType == debitType && o.Status == status && o.Description == description).ToList();

                    //get account details
                    string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME = "";
                    string FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME = "";
                    

                    //get sum for each mode

                    List<TargetGoal> flexibleTargetGoals = targetGoals.Where(o => o.LiquidationMode == flexibleMode).ToList();
                    var flexibleSavingsTotalDailyAmounts = (from sch in transactionSchedules
                                                            join goal in flexibleTargetGoals on sch.TargetGoalId equals goal.TargetGoalsId into scheduleGoals
                                                            from scheduleGoal in scheduleGoals
                                                            join tran in transactions on scheduleGoal.TargetGoalsId equals tran.TargetGoalsId
                                                            into transactionGoals
                                                            select new
                                                            {
                                                                amount = transactionGoals.Select(o => o.Amount)
                                                            }).ToList();


                    string transactionId = $"{debitType}{flexibleMode}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string Narration1 = $"TSA {debitTypeDescription} TRANSACTION SWEEP FOR FLEXIBLE MODE ({runDate.ToString("dd-MMM-yyyy")})";
                    string Narration2 = $"TSA DAILY TRANSACTION SWEEP";
                    long milliseconds = DateTime.Now.Ticks;
                    string requestId = milliseconds.ToString() + transactionId;


                    foreach (var amount in flexibleSavingsTotalDailyAmounts)
                    {
                        flexibleSavingsTotalDailyAmount = flexibleSavingsTotalDailyAmount + Convert.ToDecimal(amount);
                    }

                    ActionReturn directCreditFlexibleResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT, decimal.Round(flexibleSavingsTotalDailyAmount, 2),
                        FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT_NAME, $"{debitType}B{flexibleMode}B{milliseconds}", requestId, FCMB_BANK_CODE, Narration1, Narration2, settings);
                    if (directCreditFlexibleResult.Flag)
                    {
                        FCMBResponse response = (FCMBResponse)directCreditFlexibleResult.ReturnedObject;
                        DirectCreditResponse flexibleDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                        isFlexibleSuccessful = true;
                        flexibleDesccription = $"FLEXIBLE DESCRIPTION:(TransactionId:{flexibleDataResponse.TransactionId}|FinacleTranID:{flexibleDataResponse.FinacleTranID}|Stan:{flexibleDataResponse.Stan})";

                    }
                    else
                    {
                        //failed sweep
                        flexibleDesccription = $"FLEXIBLE DESCRIPTION: {JsonConvert.SerializeObject(directCreditFlexibleResult)}";
                    }



                    //get sum for each mode

                    List<TargetGoal> fixedTargetGoals = targetGoals.Where(o => o.LiquidationMode == fixedMode).ToList();
                    var fixedSavingsTotalDailyAmounts = (from sch in transactionSchedules
                                                         join goal in fixedTargetGoals on sch.TargetGoalId equals goal.TargetGoalsId into scheduleGoals
                                                         from scheduleGoal in scheduleGoals
                                                         join tran in transactions on scheduleGoal.TargetGoalsId equals tran.TargetGoalsId
                                                         into transactionGoals
                                                         select new
                                                         {
                                                             amount = transactionGoals.Select(o => o.Amount)
                                                         }).ToList();


                    string fixedTransactionId = $"{debitType}{fixedMode}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string fixedNarration1 = $"TSA {debitTypeDescription} TRANSACTION SWEEP FOR FIXED MODE ({runDate.ToString("dd-MMM-yyyy")})";
                    string fixedNarration2 = $"TSA DAILY TRANSACTION SWEEP";
                    long fixedMilliseconds = DateTime.Now.Ticks;
                    string fixedRequestId = fixedMilliseconds.ToString() + fixedTransactionId;

                    foreach (var amount in fixedSavingsTotalDailyAmounts)
                    {
                        fixedSavingsTotalDailyAmount = fixedSavingsTotalDailyAmount + Convert.ToDecimal(amount);
                    }

                    ActionReturn directCreditFixedResult = FCMBIntegrator.DirectCredit(FCMB_SAVINGS_ACCOUNT, FCMB_FIXED_LIQUIDATION_ACCOUNT, decimal.Round(fixedSavingsTotalDailyAmount, 2),
                        FCMB_FIXED_LIQUIDATION_ACCOUNT_NAME, $"{debitType}B{flexibleMode}B{fixedMilliseconds}", fixedRequestId, FCMB_BANK_CODE, fixedNarration1, fixedNarration2, settings);
                    if (directCreditFixedResult.Flag)
                    {
                        FCMBResponse response = (FCMBResponse)directCreditFixedResult.ReturnedObject;
                        DirectCreditResponse fixedDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                        //send email
                        isFixedSuccessful = true;
                        fixedDescription = $"FIXED DESCRIPTION:(TransactionId:{fixedDataResponse.TransactionId}|FinacleTranID:{fixedDataResponse.FinacleTranID}|Stan:{fixedDataResponse.Stan})";
                    }
                    else
                    {
                        //failed sweep
                        fixedDescription = $"FIXED DESCRIPTION: {JsonConvert.SerializeObject(directCreditFixedResult)}";
                    }


                    paymentSweepItem.DateSwept = DateTime.Now.Date;
                    paymentSweepItem.DateTimeSwept = DateTime.Now;
                    paymentSweepItem.FixedAccount = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                    paymentSweepItem.FixedAmount = fixedSavingsTotalDailyAmount;
                    paymentSweepItem.FlexibleAccount = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                    paymentSweepItem.FlexibleAmount = flexibleSavingsTotalDailyAmount;
                    paymentSweepItem.SweepAccount = FCMB_SAVINGS_ACCOUNT;
                    paymentSweepItem.SweepDate = runDate;
                    paymentSweepItem.TotalAmount = fixedSavingsTotalDailyAmount + flexibleSavingsTotalDailyAmount;
                    paymentSweepItem.SweepType = debitTypeDescription;

                    //email
                    Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                    Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                    //send email;
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailType = "SWEEP_STATUS",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };

                    dictionaryEmailLog.Add(0, emailLog);

                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                    emailValues.Add("FLEXIBLE_AMOUNT", paymentSweepItem.FlexibleAmount.ToString("N"));
                    emailValues.Add("FIXED_AMOUNT", paymentSweepItem.FixedAmount.ToString("N"));
                    emailValues.Add("RUN_DATE", paymentSweepItem.SweepDate.ToString("MMMM dd, yyyy"));
                    emailValues.Add("FIXED_ACCOUNT", paymentSweepItem.FixedAccount);
                    emailValues.Add("FLEXIBLE_ACCOUNT", paymentSweepItem.FlexibleAccount);
                    emailValues.Add("SWEEP_ACCOUNT", paymentSweepItem.SweepAccount);
                    emailValues.Add("STATUS", paymentSweepItem.Status);

                    dictionaryEmailValues.Add(0, emailValues);

                    result.HasEmail = true;
                    result.EmailValues = dictionaryEmailValues;
                    result.EmailLog = dictionaryEmailLog;
                }
                else
                {
                    otherDescription = $"CONFIG DESCRIPTION: {JsonConvert.SerializeObject(configs)}";
                }
                paymentSweepItem.Description = $"{flexibleDesccription} {fixedDescription} {otherDescription}";

                if (isFixedSuccessful && isFlexibleSuccessful)
                {
                    paymentSweepItem.Status = Enumerators.SWEEP_STATUS.FULL_SWEEP.ToString();
                }
                else if (isFixedSuccessful && !isFlexibleSuccessful)
                {
                    paymentSweepItem.Status = Enumerators.SWEEP_STATUS.PARTIAL_SWEEP_FLEXIBLE_FAILED.ToString();
                }
                else if (!isFixedSuccessful && isFlexibleSuccessful)
                {
                    paymentSweepItem.Status = Enumerators.SWEEP_STATUS.PARTIAL_SWEEP_FIXED_FAILED.ToString();
                }
                else
                {
                    paymentSweepItem.Status = Enumerators.SWEEP_STATUS.SWEEP_FAILED.ToString();
                }

                this.paymentSweepItemRepository.Add(paymentSweepItem);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();



                result.UpdateReturn = UpdateReturn.ItemUpdated;
                result.Message = "Done";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessCardPaymentSweep(DateTime runDate)";
            }
            return result;

        }

        /*
         1. Move sum of card fee difference from expense account to collection account--done on transaction schedule payment
         2. Move savings through account from collection account to flexible and fixed
         3. Move savings through card from collection account to flexible and fixed

            NB: 
            1. Add ChargedFee column to transaction table
            2. Add Percentage card fee to config table
            3. Add Card fee Limit amount to config table
            4. Add expense account to config table.
            5. Add expense sweep status table
            
         4. Deployment
            1. API
            2. Transaction Process
            3. Sweep Process

         */

        public ActionReturn ProcessSweepForCardFees(DateTime runDate)
        {
            ActionReturn result = new ActionReturn();
          
            try
            {
                string credit = Enumerators.TRANSACTION_TYPE.CREDIT.ToString();
                string postedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString();
                string debitType = CONSTANT.CARD_DEBIT_TYPE;

                List<Transaction> transactions = transactionRepository.Find(o => o.ApprovedDate == runDate && o.TransactionType == credit && o.PostedBy==postedBy).ToList();
                //get total charged fee
                decimal totalChargedFee = transactions.Sum(o => o.ChargedFee);
                //post charged fee from expense account to collection account
                
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    string FCMB_EXPENSE_ACCOUNT = settings.Where(o => o.Key == "FCMB_EXPENSE_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                    string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();

                    string transactionId = Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                    string Narration1 = $"TSA CHARGED FEE TRANSACTION SWEEP FOR CARD PAYMENTS ({runDate.ToString("dd-MMM-yyyy")})";
                    string Narration2 = $"TSA DAILY CHARGED FEE TRANSACTION SWEEP";
                    long milliseconds = DateTime.Now.Ticks;
                    string requestId = milliseconds.ToString() + transactionId;

                    ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(FCMB_EXPENSE_ACCOUNT, FCMB_SAVINGS_ACCOUNT, decimal.Round(totalChargedFee, 2),
                            CONSTANT.EMPTY, $"{debitType}B{milliseconds}", requestId, FCMB_BANK_CODE, Narration1, Narration2, settings);
                    if (directCreditResult.Flag)
                    {
                        FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                        DirectCreditResponse sweepDataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                        CardPaymentChargedFeeSweepItem cardPaymentChargedFeeSweepItem = new CardPaymentChargedFeeSweepItem
                        {
                            CollectionAccount= FCMB_SAVINGS_ACCOUNT,
                            DateSwept=DateTime.Now.Date,
                            DateTimeSwept=DateTime.Now,
                            Description= $"EXPENSE SWEEP DESCRIPTION:(TransactionId:{sweepDataResponse.TransactionId}|FinacleTranID:{sweepDataResponse.FinacleTranID}|Stan:{sweepDataResponse.Stan})",
                            ExpenseAccount= FCMB_EXPENSE_ACCOUNT,
                            Status= Enumerators.SWEEP_STATUS.FULL_SWEEP.ToString(),
                            SweepDate=runDate,
                            TotalAmount= totalChargedFee                            
                        };

                        this.cardPaymentChargedFeeSweepItemRepository.Add(cardPaymentChargedFeeSweepItem);
                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                        result.UpdateReturn = UpdateReturn.ItemUpdated;
                        result.Message = "Direct credit was successful for expense to collection sweep";
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = directCreditResult;

                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemUpdated;
                        result.Message = "Direct credit failed for expense to collection sweep";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = directCreditResult;

                       
                    }

                    //email
                    Dictionary<int, object> dictionaryEmailLog = new Dictionary<int, object>();
                    Dictionary<int, object> dictionaryEmailValues = new Dictionary<int, object>();
                    //send email;
                    EmailLog emailLog = new EmailLog
                    {
                        EmailTo = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailCopy = settings.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault(),
                        EmailType = "SWEEP_STATUS",
                        Status = Enumerators.STATUS.NEW.ToString(),
                    };

                    dictionaryEmailLog.Add(0, emailLog);

                    Dictionary<string, string> emailValues = new Dictionary<string, string>();
                    emailValues.Add("AMOUNT", totalChargedFee.ToString("N"));
                    emailValues.Add("EXPENSE_ACCOUNT", FCMB_EXPENSE_ACCOUNT);
                    emailValues.Add("SAVINGS_ACCOUNT", FCMB_SAVINGS_ACCOUNT);
                    emailValues.Add("RUN_DATE", runDate.ToString("MMMM dd, yyyy"));                    
                    emailValues.Add("STATUS", result.Message);

                    dictionaryEmailValues.Add(0, emailValues);

                    result.HasEmail = true;
                    result.EmailValues = dictionaryEmailValues;
                    result.EmailLog = dictionaryEmailLog;

                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "could not fetch config for expense to collection sweep";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = configs;

                }

                

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessSweepForCardFees(DateTime runDate)";

            }
            return result;
        }

        public ActionReturn ProcessLiquidationInterestSweep()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                string TRANSACTION_TRIAL_LIMIT = settings.Where(o => o.Key == "Paystack_TRANSACTION_TRIAL_LIMIT").Select(o => o.Value).FirstOrDefault();


                string FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string FCMB_FIXED_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string FCMB_SAVINGS_ACCOUNT = settings.Where(o => o.Key == "FCMB_SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                string FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();
                string FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT = settings.Where(o => o.Key == "FCMB_FLEXIBLE_INTEREST_LIQUIDATION_ACCOUNT").Select(o => o.Value).FirstOrDefault();

                bool limitFlag = int.TryParse(TRANSACTION_TRIAL_LIMIT, out int TRANSACTION_TRIAL_LIMIT_VALUE);
                if (!limitFlag)
                {
                    TRANSACTION_TRIAL_LIMIT_VALUE = 3;
                }
                string liquidationStatus = Enumerators.STATUS.NEW.ToString();
                List<LiquidationTransaction> liquidationTransactions = liquidationTransactionRepository.Find(o => o.Status == liquidationStatus && o.Trial < TRANSACTION_TRIAL_LIMIT_VALUE).ToList();
                if (liquidationTransactions.Count > 0)
                {
                    foreach (LiquidationTransaction liquidationTransaction in liquidationTransactions)
                    {
                        TargetSavingsRequest request = targetSavingsRequestRepository.Find(o => o.RequestId == liquidationTransaction.RequestId).FirstOrDefault();
                        if(request != null)
                        {
                            TargetGoal goal = this.targetGoalRepository.Find(o => o.TargetGoalsId == request.TargetGoalId).FirstOrDefault();
                            if(goal != null)
                            {
                                string debitAccount = "";
                                string creditAccount = "";
                                string creditBankCode = "";
                                
                                if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE.ToString())
                                {
                                    //debit fixed interest income and credit income
                                    debitAccount = FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT;
                                    creditAccount = FCMB_SAVINGS_ACCOUNT;
                                    creditBankCode = FCMB_BANK_CODE;
                                    //transaction.Narration = "TSA LIQUIDATION PENAL CHARGE";
                                    //update goal interest earned
                                    //goal.GoalnterestEarned = 0;
                                }
                                else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.PENAL_CHARGE_BALANCE.ToString())
                                {
                                    //debit fixed interest income and credit flexible income
                                    debitAccount = FCMB_FIXED_INTEREST_LIQUIDATION_ACCOUNT;
                                    creditAccount = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                                    creditBankCode = FCMB_BANK_CODE;
                                    //update goal interest earned
                                    //goal.GoalnterestEarned = 0;
                                    //transaction = null;
                                }
                                else if (liquidationTransaction.TransactionType == Enumerators.LIQUIDATION_TRANSACTION_MODE.FIXED_GOAL_BALANCE.ToString())
                                {
                                    //debit fixed income and credit flexible income
                                    debitAccount = FCMB_FIXED_LIQUIDATION_ACCOUNT;
                                    creditAccount = FCMB_FLEXIBLE_LIQUIDATION_ACCOUNT;
                                    creditBankCode = FCMB_BANK_CODE;
                                    //transaction = null;
                                }

                                liquidationTransaction.Trial++;

                                string fullName = "";
                                

                                //perform direct credit
                                string transactionId = $"{goal.TargetGoalsId}{goal.CustomerProfileId}{request.RequestId}" + Convert.ToInt64(Math.Floor((new Random().NextDouble() * 1000000000) + 1)).ToString();
                                string Narration1 = liquidationTransaction.Narration1;
                                string Narration2 = liquidationTransaction.Narration2;
                                long milliseconds = DateTime.Now.Ticks;
                                string requestId = milliseconds.ToString() + transactionId;

                                ActionReturn directCreditResult = FCMBIntegrator.DirectCredit(debitAccount, creditAccount, decimal.Round(liquidationTransaction.Amount, 2), fullName,
                                    $"{goal.TargetGoalsId}B{goal.CustomerProfileId}B{request.RequestId}", requestId, creditBankCode, Narration1, Narration2, settings);
                                if (directCreditResult.Flag)
                                {
                                    FCMBResponse response = (FCMBResponse)directCreditResult.ReturnedObject;
                                    //DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(response.ResponseData));

                                    

                                    liquidationTransaction.Status = Enumerators.STATUS.PROCESSED.ToString();
                                    liquidationTransaction.StatusDescription = "SUCCESS";
                                    liquidationTransaction.ProcessingStatus = Enumerators.STATUS.SUCCESS.ToString();
                                    liquidationTransaction.ProcessingStatusDescription = $"SUCCESS : Credit Account: { creditAccount} Credit bank Code: {creditBankCode} Debit Account:{debitAccount} :({response.StatusMessage})";
                                    liquidationTransaction.LastUpdatedDate = DateTime.Now.Date;
                                    liquidationTransaction.LastUpdatedDateTime = DateTime.Now;
                                    
                                }
                                else
                                {
                                    //direct credit issue
                                    ActionReturn verifyDirectCreditResult = FCMBIntegrator.GetTransactionStatus(requestId, settings);
                                    if (verifyDirectCreditResult.Flag)
                                    {
                                        FCMBResponse verifyDCResponse = (FCMBResponse)verifyDirectCreditResult.ReturnedObject;
                                        //GetTransactionStatusResponse verifyDataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(verifyDCResponse.ResponseData));
                                        

                                        liquidationTransaction.Status = Enumerators.STATUS.PROCESSED.ToString();
                                        liquidationTransaction.StatusDescription = "SUCCESS";
                                        liquidationTransaction.ProcessingStatus = Enumerators.STATUS.SUCCESS.ToString();
                                        liquidationTransaction.ProcessingStatusDescription = $"SUCCESS : Credit Account: { creditAccount} Credit bank Code: {creditBankCode} Debit Account:{debitAccount} :({verifyDCResponse.StatusMessage})";
                                        liquidationTransaction.LastUpdatedDate = DateTime.Now.Date;
                                        liquidationTransaction.LastUpdatedDateTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        liquidationTransaction.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                        liquidationTransaction.ProcessingStatusDescription = $"verification issue : Credit Account: { creditAccount} Credit bank Code: {creditBankCode} Debit Account:{debitAccount} :{JsonConvert.SerializeObject(verifyDirectCreditResult)}, Initial Debit: {JsonConvert.SerializeObject(directCreditResult)})";
                                        liquidationTransaction.LastUpdatedDate = DateTime.Now.Date;
                                        liquidationTransaction.LastUpdatedDateTime = DateTime.Now;
                                    }
                                }
                            }
                            else
                            {
                                //error fetchinng gaol
                                liquidationTransaction.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                                liquidationTransaction.ProcessingStatusDescription = $"Error fetching target goal";
                                liquidationTransaction.LastUpdatedDate = DateTime.Now.Date;
                                liquidationTransaction.LastUpdatedDateTime = DateTime.Now;
                            }
                        }
                        else
                        {
                            //error fetching liquidation request
                            liquidationTransaction.ProcessingStatus = Enumerators.STATUS.FAILED.ToString();
                            liquidationTransaction.ProcessingStatusDescription = $"Error fetching target request";
                            liquidationTransaction.LastUpdatedDate = DateTime.Now.Date;
                            liquidationTransaction.LastUpdatedDateTime = DateTime.Now;
                        }

                        using (System.Transactions.TransactionScope scope = new System.Transactions.TransactionScope())
                        {
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                            scope.Complete();
                        }
                    }                    
                }
                else
                {
                    result.Message = "No liquidation sweep item";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "TransactionService";
                result.MethodName = "ProcessLiquidationInterestSweep()";

            }
            return result;
        }
    }
}
