﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.EntityFramework.Extensions.Repository;
using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using Infinity.FCMB.TargetSavings.Domain.Models.DTO;
using Infinity.FCMB.TargetSavings.Domain.Models.FCMB;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.TargetSavings.Domain.Models.Paystack;
using Infinity.FCMB.TargetSavings.Domain.Models.Remita;
using Infinity.FCMB.TargetSavings.Services.Helper;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Infinity.FCMB.TargetSavings.Services.Implementation
{
    public class UtilityService : TargetSavingsServiceBase,IUtilityService
    {
        private IRepository<Category> categoryRepository { get; set; }
        private IRepository<TargetResultNote> targetResultNoteRepository { get; set; }
        private IRepository<TargetSavingsFrequency> targetSavingsFrequencyRepository { get; set; }
        private IRepository<TargetSavingsRequestType> targetSavingsRequestTypeRepository { get; set; }
        private IRepository<TargetWebPaymentProfile> targetWebPaymentProfileRepository { get; set; }
        private IRepository<AppConfigSetting> appConfigSettingRepository { get; set; }
        private IRepository<TargetDebitAccountProfile> targetDebitAccontProfileRepository { get; set; }
        private IRepository<TargetGoal> targetGoalRepository { get; set; }
        private IRepository<Bank> bankRepository { get; set; }
        private IRepository<AccountValidationLog> accountValidationLogRepository { get; set; }

        //private ITransactionService transactionService { get; set; }

        private IUnitOfWork unitOfWork { get; set; }
        public UtilityService(IUnitOfWork unitOfWork) : base(null)
        {
            unitOfWork.Database.CommandTimeout = new int?(20000);
            this.TargetSavingsUnitOfWorkUnitOfWork = unitOfWork;
            this.categoryRepository = new TargetSavingsRepositoryBase<Category>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetResultNoteRepository = new TargetSavingsRepositoryBase<TargetResultNote>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetSavingsFrequencyRepository = new TargetSavingsRepositoryBase<TargetSavingsFrequency>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetSavingsRequestTypeRepository = new TargetSavingsRepositoryBase<TargetSavingsRequestType>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetWebPaymentProfileRepository = new TargetSavingsRepositoryBase<TargetWebPaymentProfile>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.appConfigSettingRepository = new TargetSavingsRepositoryBase<AppConfigSetting>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetDebitAccontProfileRepository = new TargetSavingsRepositoryBase<TargetDebitAccountProfile>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.targetGoalRepository = new TargetSavingsRepositoryBase<TargetGoal>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.bankRepository = new TargetSavingsRepositoryBase<Bank>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.accountValidationLogRepository = new TargetSavingsRepositoryBase<AccountValidationLog>(this.TargetSavingsUnitOfWorkUnitOfWork);

            //this.transactionService = transactionService;
        }

        #region Dispose
        protected override void Dispose(bool disposing)
        {
            categoryRepository?.Dispose();
            base.Dispose(disposing);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public ActionReturn AddCategory(Category category)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                category.IsActive = true;
                
                this.categoryRepository.Add(category);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Category was created successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Category was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "AddCategory(Category category)";
            }
            return result;
        }

        public ActionReturn UpdateCategory(Category category)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                Category thisCategory = categoryRepository.Find(o => o.Id == category.Id).FirstOrDefault();
                if (thisCategory != null)
                {
                    thisCategory.Image = category.Image;                    
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Category was updated";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Category could not be updated";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Category was not updated";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "UpdateCategory(Category category)";
            }
            return result;
        }

        public ActionReturn RemoveCategory(int id)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                Category thisCategory = categoryRepository.Find(o => o.Id == id).FirstOrDefault();
                if (thisCategory != null)
                {
                    thisCategory.IsActive=false;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemDeleted;
                    result.Message = "Category was deleted successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotDeleted;
                    result.Message = "Category could not be deleted";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Category was not deleted";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "RemoveCategory(int id)";
            }
            return result;
        }
        public async Task<ActionReturn> GetCategory()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                List<Category> categories= categoryRepository.GetAll().Where(o => o.IsActive).ToList();
                List<CategoryDTO> categoryDTO = new List<CategoryDTO>();
                foreach(Category c in categories)
                {
                    categoryDTO.Add(new CategoryDTO
                    {
                        Id=c.Id,
                        Image=Encoding.UTF8.GetString(c.Image),
                        IsActive=c.IsActive,
                        Name=c.Name
                    });
                }
                result.ReturnedObject = categoryDTO;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Category were retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Category list could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetCategory()";
            }
            return await Task.FromResult(result);
        }
        
        public ActionReturn AddTargetResultNote(TargetResultNote targetResultNote)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                targetResultNote.IsActive = true;
                this.targetResultNoteRepository.Add(targetResultNote);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Note was created";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Note was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "AddTargetResultNote(TargetResultNote targetResultNote)";
            }
            return result;
        }

        public async Task<ActionReturn> GetTargetResultNote()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = targetResultNoteRepository.GetAll().Where(o => o.IsActive).ToList();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Note retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Note could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetTargetResultNote()";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn RemoveTargetResultNote(int TargetResultNoteId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetResultNote thisTargetResultNote = targetResultNoteRepository.Find(o => o.Id == TargetResultNoteId).FirstOrDefault();
                if (thisTargetResultNote != null)
                {
                    thisTargetResultNote.IsActive = false;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemDeleted;
                    result.Message = "Note deleted";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotDeleted;
                    result.Message = "Note was not deleted";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Note was not deleted";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "RemoveTargetResultNote(int TargetResultNoteId)";
            }
            return result;
        }

        public ActionReturn UpdateTargetResultNote(TargetResultNote targetResultNote)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetResultNote thisTargetResultNote = targetResultNoteRepository.Find(o => o.Id == targetResultNote.Id).FirstOrDefault();
                if (thisTargetResultNote != null)
                {
                    thisTargetResultNote.IsActive = false;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Note updated successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                    result.Message = "Note was not updated";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Note was not updated";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public ActionReturn AddTargetSavingsFrequency(TargetSavingsFrequency targetSavingsFrequency)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                targetSavingsFrequency.IsActive = true;
                this.targetSavingsFrequencyRepository.Add(targetSavingsFrequency);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Frequency was created successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Frequency could not be created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "AddTargetSavingsFrequency(TargetSavingsFrequency targetSavingsFrequency)";
            }
            return result;
        }

        public async Task<ActionReturn> GetTargetSavingsFrequency()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = targetSavingsFrequencyRepository.GetAll().Where(o => o.IsActive).ToList();
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Frequency retreived successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Frequency was not retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetTargetSavingsFrequency()";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn GetTargetSavingsFrequencySync()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = targetSavingsFrequencyRepository.GetAll().Where(o => o.IsActive).ToList();
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Frequency retreived successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Frequency was not retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetTargetSavingsFrequencySync()";
            }
            return result;
        }

        public ActionReturn RemoveTargetSavingsFrequency(int TargetSavingsFrequencyId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetSavingsFrequency thisTargetSavingsFrequency = targetSavingsFrequencyRepository.Find(o => o.Id == TargetSavingsFrequencyId).FirstOrDefault();
                if (thisTargetSavingsFrequency != null)
                {
                    thisTargetSavingsFrequency.IsActive = false;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemDeleted;
                    result.Message = "Frequency deleted successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotDeleted;
                    result.Message = "Frequency was not deleted";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Frequency was not deleted";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "RemoveTargetSavingsFrequency(int TargetSavingsFrequencyId)";
            }
            return result;
        }

        public ActionReturn UpdateTargetSavingsFrequency(TargetSavingsFrequency targetSavingsFrequency)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetSavingsFrequency thisTargetSavingsFrequency = targetSavingsFrequencyRepository.Find(o => o.Id == targetSavingsFrequency.Id).FirstOrDefault();
                if (thisTargetSavingsFrequency != null)
                {
                    thisTargetSavingsFrequency.Frequency = targetSavingsFrequency.Frequency;
                    thisTargetSavingsFrequency.ValueInDays = targetSavingsFrequency.ValueInDays;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Frequency updated successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                    result.Message = "Frequency was not updated";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Frequency was not updated";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "UpdateTargetSavingsFrequency(TargetSavingsFrequency targetSavingsFrequency)";
            }
            return result;
        }

        public ActionReturn AddTargetSavingsRequestType(TargetSavingsRequestType targetSavingsRequestType)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                targetSavingsRequestType.IsActive = true;
                this.targetSavingsRequestTypeRepository.Add(targetSavingsRequestType);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Request type created successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Request type was not created";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "AddTargetSavingsRequestType(TargetSavingsRequestType targetSavingsRequestType)";
            }
            return result;
        }

        public async Task<ActionReturn> GetTargetSavingsRequestType()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = targetSavingsRequestTypeRepository.GetAll().Where(o => o.IsActive).ToList();
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Request type retreived successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Request type could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetTargetSavingsRequestType()";
            }
            return await Task.FromResult(result);
        }

        public ActionReturn RemoveTargetSavingsRequestType(int TargetSavingsRequestTypeId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetSavingsRequestType thisTargetSavingsRequestType = targetSavingsRequestTypeRepository.Find(o => o.RequestTypeId == TargetSavingsRequestTypeId).FirstOrDefault();
                if (thisTargetSavingsRequestType != null)
                {
                    thisTargetSavingsRequestType.IsActive = false;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemDeleted;
                    result.Message = "Request type deleted successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotDeleted;
                    result.Message = "Request type was not deleted";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Request type was not deleted";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "RemoveTargetSavingsRequestType(int TargetSavingsRequestTypeId)";
            }
            return result;
        }

        public ActionReturn UpdateTargetSavingsRequestType(TargetSavingsRequestType targetSavingsRequestType)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetSavingsRequestType thisTargetSavingsRequestType = targetSavingsRequestTypeRepository.Find(o => o.RequestTypeId == targetSavingsRequestType.RequestTypeId).FirstOrDefault();
                if (thisTargetSavingsRequestType != null)
                {
                    thisTargetSavingsRequestType.RequestType = targetSavingsRequestType.RequestType;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Request type was updated successfully";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                    result.Message = "Request type could not be updated";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Request type was not updated";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "UpdateTargetSavingsRequestType(TargetSavingsRequestType targetSavingsRequestType)";
            }
            return result;
        }

        public async Task<ActionReturn> GetWebPaymentKey()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                List<AppConfigSetting> appConfigSetting = appConfigSettingRepository.GetAll().Where(o => o.IsActive == true).ToList();
                var keys = new
                {
                    key = appConfigSetting.Where(o => o.Key == "Paystack_PUBLIC_KEY").Select(o=>o.Value).FirstOrDefault(),
                    email = appConfigSetting.Where(o => o.Key == "Paystack_EMAIL").Select(o => o.Value).FirstOrDefault()
                };
                result.ReturnedObject =  keys;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Payment Key retreived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Payment key could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetWebPaymentKey()";
            }
            return await Task.FromResult(result);
        }
        public async Task<ActionReturn> GetInternalWebPaymentKey()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = targetWebPaymentProfileRepository.GetAll().FirstOrDefault();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Payment key retreived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Payment key could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetInternalWebPaymentKey()";
            }
            return await Task.FromResult(result);
        }


        public ActionReturn UpdateWebPaymentKey(TargetWebPaymentProfile targetWebPaymentProfile)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TargetWebPaymentProfile thisTargetWebPaymentProfile = targetWebPaymentProfileRepository.Find(o => o.Id == targetWebPaymentProfile.Id).FirstOrDefault();
                if (thisTargetWebPaymentProfile != null)
                {
                    thisTargetWebPaymentProfile.InternalValue = targetWebPaymentProfile.InternalValue;
                    thisTargetWebPaymentProfile.ExternalValue = targetWebPaymentProfile.ExternalValue;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemUpdated;
                    result.Message = "Payment key updated";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = null;
                    result.UpdateReturn = UpdateReturn.ItemWasNotUpdated;
                    result.Message = "Payment key was not updated";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception:Payment key was not updated";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public async Task<ActionReturn> GetBanks()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = bankRepository.GetAll().Where(o=>o.IsActive).ToList();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Banks retreived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Banks could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetBanks()";
            }
            return await Task.FromResult(result);
        }

        public async Task<ActionReturn> GetBanksFromFCMB()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                if (configs.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                    ActionReturn bankResult = FCMBIntegrator.GetBankList(settings);
                    if (bankResult.Flag)
                    {
                        FCMBResponse response = (FCMBResponse)bankResult.ReturnedObject;
                        BankListResponse dataResponse = JsonConvert.DeserializeObject<BankListResponse>(Convert.ToString(response.ResponseData));
                        result.ReturnedObject = dataResponse;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = bankResult.Message;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        result.ReturnedObject = bankResult.ReturnedObject;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = bankResult.Message;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    result.ReturnedObject = configs.ReturnedObject;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = configs.Message;
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Error getting bank information";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetBanksFromFCMB()";
            }
            return await Task.FromResult(result);

        }

        public ActionReturn SetupCustomerPendingMandates()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string status = Enumerators.STATUS.NEW.ToString();
                List<TargetDebitAccountProfile> targetDebitAccountProfiles = targetDebitAccontProfileRepository.GetAll().Where(o => o.Status == status).ToList();
                foreach (TargetDebitAccountProfile profile in targetDebitAccountProfiles)
                {
                    TargetGoal goal = targetGoalRepository.All.Include(o => o.CustomerProfile).Where(o => o.TargetGoalsId == profile.TargetGoalId).FirstOrDefault();
                    Mandate mandate = new Mandate
                    {
                        amount= (Decimal.Round(goal.TargetFrequencyAmount,2)).ToString(),
                        endDate=(Convert.ToDateTime(goal.GoalEndDate)).ToString("dd/MM/yyyy"),
                        frequency="1",
                        hash="",
                        mandateType="",
                        maxNoOfDebits="",
                        merchantId="",
                        payerAccount=goal.GoalDebitAccountNo,
                        payerBankCode=goal.GoalDebitBank,
                        payerEmail=goal.CustomerProfile.EmailAddress,
                        payerName= $"{goal.CustomerProfile.FirstName} {goal.CustomerProfile.Surname}",
                        payerPhone= goal.CustomerProfile.PhoneNumber,
                        requestId="",
                        serviceTypeId="",
                        startDate= (Convert.ToDateTime(goal.GoalStartDate)).ToString("dd/MM/yyyy")
                    };
                    ActionReturn configs = this.GetAppConfigSettings();
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;

                    ActionReturn setupResult = this.SetupMandate(mandate, settings);
                    if (setupResult.Flag)
                    {
                        MandateResponse response = (MandateResponse)setupResult.ReturnedObject;
                        profile.MandateId = response.mandateId;
                        profile.RequestId = response.requestId;
                        profile.Status = Enumerators.STATUS.PENDING.ToString();
                        profile.LastModifiedDate = DateTime.Now;
                    }
                }
                using (TransactionScope scope = new TransactionScope())
                {
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    scope.Complete();
                }
                result.UpdateReturn = UpdateReturn.ItemUpdated;
                result.Message = "Done";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.ItemWasNotCreated;
                result.Message = "Error setting up mandates";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "SetupCustomerPendingMandates()";
            }
            return result;
        }

        public ActionReturn RequestMandateValidationOTP(long targetGoalId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                //string goalStatus = Enumerators.STATUS.PENDING.ToString();
                TargetGoal goal = targetGoalRepository.All.Include(o => o.CustomerProfile).Where(o => o.TargetGoalsId == targetGoalId).FirstOrDefault();
                if(goal != null)
                {
                    string status = Enumerators.STATUS.NEW.ToString();
                    ActionReturn configs = this.GetAppConfigSettings();
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;

                    string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                    string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                    string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                    string validateMandateUrl = settings.Where(o => o.Key == "ValidateMandateUrl").Select(o => o.Value).FirstOrDefault();
                    string API_TOKEN = settings.Where(o => o.Key == "API_TOKEN").Select(o => o.Value).FirstOrDefault();

                    TargetDebitAccountProfile profile = targetDebitAccontProfileRepository.GetAll().Where(o => o.TargetGoalId==goal.TargetGoalsId).FirstOrDefault();
                    if(profile != null)
                    {
                        if (goal.GoalStatus == status)
                        {
                            Mandate mandate = new Mandate
                            {
                                amount = (Decimal.Round(goal.TargetFrequencyAmount, 2)).ToString(),
                                endDate = (Convert.ToDateTime(goal.GoalEndDate)).ToString("dd/MM/yyyy"),
                                frequency = "1",
                                hash = "",
                                mandateType = "",
                                maxNoOfDebits = "",
                                merchantId = "",
                                payerAccount = goal.GoalDebitAccountNo,
                                payerBankCode = goal.GoalDebitBank,
                                payerEmail = goal.CustomerProfile.EmailAddress,
                                payerName = $"{goal.CustomerProfile.FirstName} {goal.CustomerProfile.Surname}",
                                payerPhone = goal.CustomerProfile.PhoneNumber,
                                requestId = "",
                                serviceTypeId = "",
                                startDate = (Convert.ToDateTime(goal.GoalStartDate)).ToString("dd/MM/yyyy")
                            };
                            ActionReturn setupMandateResult = this.SetupMandateAndRequestValidation(mandate, settings);
                            if (setupMandateResult.Flag)
                            {
                                ValidateMandateResponse validateMandateResponse = (ValidateMandateResponse)setupMandateResult.ReturnedObject;
                                goal.GoalStatus = Enumerators.STATUS.PENDING.ToString();
                                profile.Status = Enumerators.STATUS.PENDING.ToString();
                                profile.RequestId = validateMandateResponse.requestId;
                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                
                                result.ReturnedObject = validateMandateResponse.remitaTransRef;
                                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                result.Message = "Mandate otp request was successful";
                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                            else
                            {
                                result.ReturnedObject = setupMandateResult.ReturnedObject;
                                result.UpdateReturn = UpdateReturn.NoChange;
                                result.Message = $"Mandate validation failed {setupMandateResult.Message}";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }
                        }
                        else if (goal.GoalStatus == Enumerators.STATUS.PENDING.ToString())
                        {
                            if (profile != null)
                            {

                                long milliseconds = DateTime.Now.Ticks;
                                //string validateMandateRequestId = milliseconds.ToString();
                                ValidateMandateRequest validateMandateRequest = new ValidateMandateRequest
                                {
                                    mandateId = profile.MandateId,
                                    requestId = profile.RequestId
                                };
                                string API_DETAILS_HASH = this.GetHash($"{apiKey}{profile.RequestId}{API_TOKEN}");

                                DateTime today = DateTime.UtcNow;
                                string REQUEST_TS = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss+000000");
                                WebHeaderCollection header = new WebHeaderCollection();
                                header.Add("API_DETAILS_HASH", API_DETAILS_HASH);
                                header.Add("API_KEY", apiKey);
                                header.Add("MERCHANT_ID", merchantId);
                                header.Add("REQUEST_ID", profile.RequestId);
                                header.Add("REQUEST_TS", REQUEST_TS);
                                string jsonRequestValidateMandateRequest = JsonConvert.SerializeObject(validateMandateRequest);
                                ActionReturn validateMandateRequestServerResult = Util.MakeRequestAndGetResponse(jsonRequestValidateMandateRequest, validateMandateUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                                if (validateMandateRequestServerResult.Flag)
                                {
                                    ValidateMandateResponse jsonValidateMandateResponse = JsonConvert.DeserializeObject<ValidateMandateResponse>(Convert.ToString(validateMandateRequestServerResult.ReturnedObject));
                                    if (jsonValidateMandateResponse.statuscode == "00")
                                    {
                                        //UPDATE
                                        profile.Status = Enumerators.STATUS.PENDING.ToString();
                                        
                                        this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                        result.ReturnedObject = jsonValidateMandateResponse.remitaTransRef;
                                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                        result.Message = "Mandate otp request was successful";
                                        result.Flag = true;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                    }
                                    else
                                    {
                                        result.ReturnedObject = jsonValidateMandateResponse.status;
                                        result.UpdateReturn = UpdateReturn.NoChange;
                                        result.Message = "Mandate validation failed";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    }

                                }
                                else
                                {
                                    result.ReturnedObject = "";
                                    result.UpdateReturn = UpdateReturn.NoChange;
                                    result.Message = "Mandate validation failed on communication with remote server.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                }
                            }
                            else
                            {
                                result.ReturnedObject = "";
                                result.UpdateReturn = UpdateReturn.NoChange;
                                result.Message = "Mandate validation failed, could not retreive goal profile.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        Mandate mandate = new Mandate
                        {
                            amount = (Decimal.Round(goal.TargetFrequencyAmount, 2)).ToString(),
                            endDate = (Convert.ToDateTime(goal.GoalEndDate)).ToString("dd/MM/yyyy"),
                            frequency = "1",
                            hash = "",
                            mandateType = "",
                            maxNoOfDebits = "",
                            merchantId = "",
                            payerAccount = goal.GoalDebitAccountNo,
                            payerBankCode = goal.GoalDebitBank,
                            payerEmail = goal.CustomerProfile.EmailAddress,
                            payerName = $"{goal.CustomerProfile.FirstName} {goal.CustomerProfile.Surname}",
                            payerPhone = goal.CustomerProfile.PhoneNumber,
                            requestId = "",
                            serviceTypeId = "",
                            startDate = (Convert.ToDateTime(goal.GoalStartDate)).ToString("dd/MM/yyyy")
                        };
                        ActionReturn setupMandateResult = this.SetupMandateAndRequestValidation(mandate, settings);
                        if (setupMandateResult.Flag)
                        {
                            ValidateMandateResponse validateMandateResponse = (ValidateMandateResponse)setupMandateResult.ReturnedObject;
                            TargetDebitAccountProfile tartgetDebitAccontProfile = new TargetDebitAccountProfile
                            {
                                DateCreated = DateTime.Now,
                                HashValue = $"{goal.CustomerProfileId}|{goal.GoalDebitAccountNo}|{goal.GoalDebitBank}|{goal.GoalEndDate}|{goal.GoalStartDate}|{goal.TargetFrequency}|{goal.TargetFrequencyAmount}",
                                IsActive = true,
                                LastModifiedDate = DateTime.Now,
                                Status = Enumerators.STATUS.NEW.ToString(),
                                TargetGoalId = goal.TargetGoalsId,
                                MandateId = validateMandateResponse.mandateId,
                                RequestId=validateMandateResponse.requestId                                
                            };

                            goal.GoalStatus = Enumerators.STATUS.PENDING.ToString();
                            profile.Status = Enumerators.STATUS.PENDING.ToString();
                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                            result.ReturnedObject = validateMandateResponse.remitaTransRef;
                            result.UpdateReturn = UpdateReturn.ItemRetrieved;
                            result.Message = "Mandate otp request was successful";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            result.ReturnedObject = setupMandateResult.ReturnedObject;
                            result.UpdateReturn = UpdateReturn.NoChange;
                            result.Message = $"Mandate validation failed {setupMandateResult.Message}";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }
                    }
                    

                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Mandate validation failed, could not retreive goal.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Mandate validation failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "RequestMandateValidationOTP(long targetGoalId)";
            }
            return result;

            

        }

        public ActionReturn ValidateMandateOTP(long targetGoalId,string remitaTransRef, string otp, string cardLast4Digit)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PENDING.ToString();
                TargetGoal goal = targetGoalRepository.All.Include(o => o.CustomerProfile).Where(o => o.TargetGoalsId == targetGoalId && o.GoalStatus== goalStatus).FirstOrDefault();
                if (goal != null)
                {
                    ActionReturn configs = this.GetAppConfigSettings();
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;

                    string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                    string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                    string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                    string validateMandateUrl = settings.Where(o => o.Key == "ValidateMandateUrl").Select(o => o.Value).FirstOrDefault();
                    string API_TOKEN = settings.Where(o => o.Key == "API_TOKEN").Select(o => o.Value).FirstOrDefault();
                    string FCMBBankCode = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();

                    if (goal.GoalDebitBank == FCMBBankCode)
                    {
                        //get AccountValidationLog
                        AccountValidationLog accountValidationLog = this.accountValidationLogRepository.Find(o => o.CustomerId == goal.CustomerProfileId & o.ReferenceId == remitaTransRef & o.TargetGoalId == targetGoalId).FirstOrDefault();
                        if (accountValidationLog != null)
                        {
                            string hashValue = Util.Decrypt(accountValidationLog.HashValue);
                            ValidateMandateResponse validateMandateResponse = JsonConvert.DeserializeObject<ValidateMandateResponse>(hashValue);
                            if (validateMandateResponse != null)
                            {
                                string[] statusCode = validateMandateResponse.statuscode.Split('|');
                                if (statusCode.Length > 7)
                                {
                                    DateTime now = DateTime.Now;
                                    DateTime created = new DateTime(Convert.ToInt32(statusCode[2]),
                                        Convert.ToInt32(statusCode[3]), Convert.ToInt32(statusCode[4]), Convert.ToInt32(statusCode[5]),
                                        Convert.ToInt32(statusCode[6]), Convert.ToInt32(statusCode[7]));

                                    if (statusCode[0] == Enumerators.STATUS.NEW.ToString() & now <= created.AddSeconds(Convert.ToInt32(statusCode[1]))
                                        & validateMandateResponse.remitaTransRef == remitaTransRef & validateMandateResponse.TargetGoalId == targetGoalId
                                        & validateMandateResponse.requestId == goal.CustomerProfileId.ToString())
                                    {
                                        ActionReturn validateotpResult = Util.ValidateOtp(otp);
                                        if (validateotpResult.Flag)
                                        {
                                            accountValidationLog.LastModifiedDate = DateTime.Now;
                                            validateMandateResponse.statuscode= $"{Enumerators.STATUS.PROCESSED.ToString()}|{statusCode[1]}|{statusCode[2]}|{statusCode[3]}|{statusCode[4]}|{statusCode[5]}|{statusCode[6]}|{statusCode[7]}";
                                            accountValidationLog.HashValue = Util.Encrypt(JsonConvert.SerializeObject(validateMandateResponse));
                                            goal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                            this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                            result.ReturnedObject = null;
                                            result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                            result.Message = "otp validation was successful";
                                            result.Flag = true;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                        }
                                        else
                                        {
                                            result.ReturnedObject = null;
                                            result.UpdateReturn = UpdateReturn.NoChange;
                                            result.Message = "otp validation failed";
                                            result.Flag = false;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                        }
                                    }
                                    else
                                    {
                                        //Could not validate otp, it may have been used, expired or does not exist.
                                        result.ReturnedObject = null;
                                        result.UpdateReturn = UpdateReturn.NoChange;
                                        result.Message = "Could not validate otp, it may have been used, expired or does not exist.";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    }
                                }
                                else
                                {
                                    //could not validate code
                                    result.ReturnedObject = null;
                                    result.UpdateReturn = UpdateReturn.NoChange;
                                    result.Message = "Invalid otp, it may have been used, expired or does not exist.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                }
                            }
                            else
                            {
                                //no valid reference Id
                                result.ReturnedObject = null;
                                result.UpdateReturn = UpdateReturn.NoChange;
                                result.Message = "Invalid otp reference, it may have been used, expired or does not exist.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }
                        }
                        else
                        {
                            //otp reference does not exist
                            result.ReturnedObject = null;
                            result.UpdateReturn = UpdateReturn.NoChange;
                            result.Message = "Invalid otp reference, it has either expired or does not exist.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }

                    }
                    else
                    {
                        string status = Enumerators.STATUS.PENDING.ToString();
                        TargetDebitAccountProfile profile = targetDebitAccontProfileRepository.GetAll().Where(o => o.TargetGoalId == goal.TargetGoalsId).FirstOrDefault();
                        if (profile != null)
                        {
                            long milliseconds = DateTime.Now.Ticks;
                            string validateMandateOTPRequestId = milliseconds.ToString(); //profile.RequestId;

                            string API_DETAILS_HASH = this.GetHash($"{apiKey}{validateMandateOTPRequestId}{API_TOKEN}");

                            DateTime today = DateTime.UtcNow;
                            string REQUEST_TS = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss+000000");
                            WebHeaderCollection header = new WebHeaderCollection();
                            header.Add("API_DETAILS_HASH", API_DETAILS_HASH);
                            header.Add("API_KEY", apiKey);
                            header.Add("MERCHANT_ID", merchantId);
                            header.Add("REQUEST_ID", validateMandateOTPRequestId);
                            header.Add("REQUEST_TS", REQUEST_TS);

                            string validateMandateRequestOTPUrl = settings.Where(o => o.Key == "ValidateMandateRequestOTP").Select(o => o.Value).FirstOrDefault();

                            List<object> authParameters = new List<object>();
                            authParameters.Add(new ValidateMandateRequestOTPParam1 { param1 = "OTP", value = otp });
                            authParameters.Add(new ValidateMandateRequestOTPParam2 { param2 = "CARD", value = cardLast4Digit });
                            ValidateMandateRequestOTP validateMandateRequestOTP = new ValidateMandateRequestOTP
                            {
                                remitaTransRef = remitaTransRef,
                                authParams = authParameters
                            };
                            string jsonRequestValidateMandateOTP = JsonConvert.SerializeObject(validateMandateRequestOTP);

                            ActionReturn validateMandateRequestOTPServerResult = Util.MakeRequestAndGetResponse(jsonRequestValidateMandateOTP, validateMandateRequestOTPUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                            if (validateMandateRequestOTPServerResult.Flag)
                            {
                                dynamic validateMandateRequestOTPServerResponse = JsonConvert.DeserializeObject(Convert.ToString(validateMandateRequestOTPServerResult.ReturnedObject));
                                if (validateMandateRequestOTPServerResponse.statuscode == "00")
                                {
                                    profile.Status = Enumerators.STATUS.PROCESSED.ToString();
                                    goal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                    result.ReturnedObject = null;
                                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                    result.Message = "Mandate otp validation was successful";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                }
                                else
                                {

                                    result.ReturnedObject = validateMandateRequestOTPServerResponse.status;
                                    result.UpdateReturn = UpdateReturn.NoChange;
                                    result.Message = "Mandate otp validation failed";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                }

                            }
                            else
                            {
                                result.ReturnedObject = null;
                                result.UpdateReturn = UpdateReturn.NoChange;
                                result.Message = "Mandate otp validation failed";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }

                        }
                        else
                        {
                            result.ReturnedObject = "";
                            result.UpdateReturn = UpdateReturn.NoChange;
                            result.Message = "Mandate otp validation failed, could not retreive goal profile.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }

                        
                    }

                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Mandate otp validation failed, could not retreive goal.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Mandate otp validation failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "ValidateMandateOTP(long targetGoalId,string remitaTransRef, string otp, string cardLast4Digit)";
            }
            return result;

        }

        public ActionReturn ValidateMandateOTP_Old(long targetGoalId, string remitaTransRef, string otp, string cardLast4Digit)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string goalStatus = Enumerators.STATUS.PENDING.ToString();
                TargetGoal goal = targetGoalRepository.All.Include(o => o.CustomerProfile).Where(o => o.TargetGoalsId == targetGoalId && o.GoalStatus == goalStatus).FirstOrDefault();
                if (goal != null)
                {
                    string status = Enumerators.STATUS.PENDING.ToString();
                    TargetDebitAccountProfile profile = targetDebitAccontProfileRepository.GetAll().Where(o => o.TargetGoalId == goal.TargetGoalsId).FirstOrDefault();
                    if (profile != null)
                    {
                        ActionReturn configs = this.GetAppConfigSettings();
                        List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;

                        string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                        string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                        string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                        string validateMandateUrl = settings.Where(o => o.Key == "ValidateMandateUrl").Select(o => o.Value).FirstOrDefault();
                        string API_TOKEN = settings.Where(o => o.Key == "API_TOKEN").Select(o => o.Value).FirstOrDefault();
                        string FCMBBankCode = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();

                        if (goal.GoalDebitBank == FCMBBankCode)
                        {
                            //get AccountValidationLog
                            AccountValidationLog accountValidationLog = this.accountValidationLogRepository.Find(o => o.CustomerId == goal.CustomerProfileId & o.ReferenceId == remitaTransRef & o.TargetGoalId == targetGoalId).FirstOrDefault();
                            if (accountValidationLog != null)
                            {
                                string hashValue = Util.Decrypt(accountValidationLog.HashValue);
                                ValidateMandateResponse validateMandateResponse = JsonConvert.DeserializeObject<ValidateMandateResponse>(hashValue);
                                if (validateMandateResponse != null)
                                {
                                    string[] statusCode = validateMandateResponse.statuscode.Split('|');
                                    if (statusCode.Length > 7)
                                    {
                                        DateTime now = DateTime.Now;
                                        DateTime created = new DateTime(Convert.ToInt32(statusCode[2]),
                                            Convert.ToInt32(statusCode[3]), Convert.ToInt32(statusCode[4]), Convert.ToInt32(statusCode[5]),
                                            Convert.ToInt32(statusCode[6]), Convert.ToInt32(statusCode[7]));

                                        if (statusCode[0] == Enumerators.STATUS.NEW.ToString() & now <= created.AddSeconds(Convert.ToInt32(statusCode[1]))
                                            & validateMandateResponse.remitaTransRef == remitaTransRef & validateMandateResponse.TargetGoalId == targetGoalId
                                            & validateMandateResponse.requestId == goal.CustomerProfileId.ToString())
                                        {
                                            ActionReturn validateotpResult = Util.ValidateOtp(otp);
                                            if (validateotpResult.Flag)
                                            {
                                                profile.Status = Enumerators.STATUS.PROCESSED.ToString();
                                                goal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                                result.ReturnedObject = null;
                                                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                                result.Message = "otp validation was successful";
                                                result.Flag = true;
                                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                            }
                                            else
                                            {
                                                result.ReturnedObject = null;
                                                result.UpdateReturn = UpdateReturn.NoChange;
                                                result.Message = "otp validation failed";
                                                result.Flag = false;
                                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                            }
                                        }
                                        else
                                        {
                                            //Could not validate otp, it may have been used, expired or does not exist.
                                            result.ReturnedObject = null;
                                            result.UpdateReturn = UpdateReturn.NoChange;
                                            result.Message = "Could not validate otp, it may have been used, expired or does not exist.";
                                            result.Flag = false;
                                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                        }
                                    }
                                    else
                                    {
                                        //could not validate code
                                        result.ReturnedObject = null;
                                        result.UpdateReturn = UpdateReturn.NoChange;
                                        result.Message = "Invalid otp, it may have been used, expired or does not exist.";
                                        result.Flag = false;
                                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                    }
                                }
                                else
                                {
                                    //no valid reference Id
                                    result.ReturnedObject = null;
                                    result.UpdateReturn = UpdateReturn.NoChange;
                                    result.Message = "Invalid otp reference, it may have been used, expired or does not exist.";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                }
                            }
                            else
                            {
                                //otp reference does not exist
                                result.ReturnedObject = null;
                                result.UpdateReturn = UpdateReturn.NoChange;
                                result.Message = "Invalid otp reference, it has either expired or does not exist.";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }

                        }
                        else
                        {
                            long milliseconds = DateTime.Now.Ticks;
                            string validateMandateOTPRequestId = milliseconds.ToString(); //profile.RequestId;

                            string API_DETAILS_HASH = this.GetHash($"{apiKey}{validateMandateOTPRequestId}{API_TOKEN}");

                            DateTime today = DateTime.UtcNow;
                            string REQUEST_TS = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss+000000");
                            WebHeaderCollection header = new WebHeaderCollection();
                            header.Add("API_DETAILS_HASH", API_DETAILS_HASH);
                            header.Add("API_KEY", apiKey);
                            header.Add("MERCHANT_ID", merchantId);
                            header.Add("REQUEST_ID", validateMandateOTPRequestId);
                            header.Add("REQUEST_TS", REQUEST_TS);

                            string validateMandateRequestOTPUrl = settings.Where(o => o.Key == "ValidateMandateRequestOTP").Select(o => o.Value).FirstOrDefault();

                            List<object> authParameters = new List<object>();
                            authParameters.Add(new ValidateMandateRequestOTPParam1 { param1 = "OTP", value = otp });
                            authParameters.Add(new ValidateMandateRequestOTPParam2 { param2 = "CARD", value = cardLast4Digit });
                            ValidateMandateRequestOTP validateMandateRequestOTP = new ValidateMandateRequestOTP
                            {
                                remitaTransRef = remitaTransRef,
                                authParams = authParameters
                            };
                            string jsonRequestValidateMandateOTP = JsonConvert.SerializeObject(validateMandateRequestOTP);

                            ActionReturn validateMandateRequestOTPServerResult = Util.MakeRequestAndGetResponse(jsonRequestValidateMandateOTP, validateMandateRequestOTPUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                            if (validateMandateRequestOTPServerResult.Flag)
                            {
                                dynamic validateMandateRequestOTPServerResponse = JsonConvert.DeserializeObject(Convert.ToString(validateMandateRequestOTPServerResult.ReturnedObject));
                                if (validateMandateRequestOTPServerResponse.statuscode == "00")
                                {
                                    profile.Status = Enumerators.STATUS.PROCESSED.ToString();
                                    goal.GoalStatus = Enumerators.STATUS.PROCESSED.ToString();
                                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();

                                    result.ReturnedObject = null;
                                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                    result.Message = "Mandate otp validation was successful";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                }
                                else
                                {

                                    result.ReturnedObject = validateMandateRequestOTPServerResponse.status;
                                    result.UpdateReturn = UpdateReturn.NoChange;
                                    result.Message = "Mandate otp validation failed";
                                    result.Flag = false;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                                }

                            }
                            else
                            {
                                result.ReturnedObject = null;
                                result.UpdateReturn = UpdateReturn.NoChange;
                                result.Message = "Mandate otp validation failed";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }
                        }



                    }
                    else
                    {
                        result.ReturnedObject = "";
                        result.UpdateReturn = UpdateReturn.NoChange;
                        result.Message = "Mandate otp validation failed, could not retreive goal profile.";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }

                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Mandate otp validation failed, could not retreive goal.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Mandate otp validation failed with an exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "ValidateMandateOTP(long targetGoalId,string remitaTransRef, string otp, string cardLast4Digit)";
            }
            return result;

        }

        public ActionReturn GetAppConfigSettings()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                result.ReturnedObject = appConfigSettingRepository.GetAll().Where(o=>o.IsActive==true).ToList();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Config retreived";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Config could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "GetAppConfigSettings()";
            }
            return result;
        }

        public string GetHash(string hashString)
        {
            System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
            Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hashString));
            sha512.Clear();
            string hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();
            return hashed;
        }

        private ActionReturn SetupMandateAndRequestValidation(Mandate mandate, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                string mandateUrl = settings.Where(o => o.Key == "MandateUrl").Select(o => o.Value).FirstOrDefault();

                long milliseconds = DateTime.Now.Ticks;
                string requestId = milliseconds.ToString();
                string hashPlain = merchantId + serviceTypeId + requestId + mandate.amount + apiKey;
                mandate.serviceTypeId = serviceTypeId;
                mandate.merchantId = merchantId;
                mandate.requestId = requestId;
                mandate.hash = this.GetHash(hashPlain);
                mandate.mandateType = "SO";
                mandate.frequency = "Day";

                string jsonRequest = JsonConvert.SerializeObject(mandate);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, mandateUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.NO_HEADER, null);
                if (serverResult.Flag)
                {
                    MandateResponse jsonResponse = JsonConvert.DeserializeObject<MandateResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.statuscode == "040")
                    {
                        //request otp
                        string validateMandateUrl = settings.Where(o => o.Key == "ValidateMandateUrl").Select(o => o.Value).FirstOrDefault();
                        string API_TOKEN = settings.Where(o => o.Key == "API_TOKEN").Select(o => o.Value).FirstOrDefault();
                        string validateMandateRequestId = milliseconds.ToString();
                        ValidateMandateRequest validateMandateRequest = new ValidateMandateRequest
                        {
                            mandateId = jsonResponse.mandateId,
                            requestId = requestId
                        };
                        string API_DETAILS_HASH = this.GetHash($"{apiKey}{validateMandateRequestId}{API_TOKEN}");
                        DateTime today = DateTime.UtcNow;
                        string REQUEST_TS = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss+000000");
                        WebHeaderCollection header = new WebHeaderCollection();
                        header.Add("API_DETAILS_HASH", API_DETAILS_HASH);
                        header.Add("API_KEY", apiKey);
                        header.Add("MERCHANT_ID", merchantId);
                        header.Add("REQUEST_ID", requestId);
                        header.Add("REQUEST_TS", REQUEST_TS);
                        string jsonRequestValidateMandateRequest = JsonConvert.SerializeObject(validateMandateRequest);
                        ActionReturn validateMandateRequestServerResult = Util.MakeRequestAndGetResponse(jsonRequestValidateMandateRequest, validateMandateUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                        if (validateMandateRequestServerResult.Flag)
                        {
                            ValidateMandateResponse jsonValidateMandateResponse = JsonConvert.DeserializeObject<ValidateMandateResponse>(Convert.ToString(validateMandateRequestServerResult.ReturnedObject));
                            if (jsonValidateMandateResponse.statuscode == "00")
                            {
                                result.ReturnedObject = jsonValidateMandateResponse;
                                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                result.Message = "Mandate otp request was successful";
                                result.Flag = true;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                            }
                            else
                            {
                                result.ReturnedObject = jsonValidateMandateResponse.status;
                                result.UpdateReturn = UpdateReturn.NoChange;
                                result.Message = "Mandate validation failed";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }

                        }
                        else
                        {
                            result.ReturnedObject = "";
                            result.UpdateReturn = UpdateReturn.NoChange;
                            result.Message = "Mandate validation failed on communication with remote server.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }


                    }
                    else
                    {
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.NoChange;
                        result.Message = "Failed";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }

                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Failed";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "SetupMandateAndRequestValidation(Mandate mandate, List<AppConfigSetting> settings)";
            }
            return result;
        }

        private ActionReturn SetupMandate_old(Mandate mandate, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {

                
                string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                string mandateUrl = settings.Where(o => o.Key == "MandateUrl").Select(o => o.Value).FirstOrDefault();

                long milliseconds = DateTime.Now.Ticks;
                string requestId = milliseconds.ToString();
                string hashPlain = merchantId + serviceTypeId + requestId + mandate.amount + apiKey;
                mandate.serviceTypeId = serviceTypeId;
                mandate.merchantId = merchantId;
                mandate.requestId = requestId;
                mandate.hash = this.GetHash(hashPlain);
                mandate.mandateType = "SO";
                mandate.frequency = "Day";

                string jsonRequest = JsonConvert.SerializeObject(mandate);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, mandateUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.NO_HEADER, null);
                if (serverResult.Flag)
                {
                    MandateResponse jsonResponse = JsonConvert.DeserializeObject<MandateResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.statuscode == "040")
                    {
                        //request otp
                        string validateMandateUrl= settings.Where(o => o.Key == "ValidateMandateUrl").Select(o => o.Value).FirstOrDefault();
                        string API_TOKEN = settings.Where(o => o.Key == "API_TOKEN").Select(o => o.Value).FirstOrDefault();
                        //string API_KEY = settings.Where(o => o.Key == "API_KEY").Select(o => o.Value).FirstOrDefault();
                        string validateMandateRequestId = milliseconds.ToString();
                        ValidateMandateRequest validateMandateRequest = new ValidateMandateRequest
                        {
                            mandateId = jsonResponse.mandateId,
                            requestId = validateMandateRequestId
                        };
                        string API_DETAILS_HASH = this.GetHash($"{apiKey}{validateMandateRequestId}{API_TOKEN}");
                        //2018-06-20T16:23:16+000000
                        DateTime today = DateTime.UtcNow;
                        string REQUEST_TS = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss+000000");
                        //var timeStamp = yyyy+'-'+mm+'-'+dd+'T'+hours+':'+minutes+':'+seconds+'+000000';
                        //string REQUEST_TS = $"{today.Year}-{today.Month}-{today.Day}T{today.Hour}:{today.Minute}:{today.Second}+000000";
                        WebHeaderCollection header= new WebHeaderCollection();
                        header.Add("API_DETAILS_HASH", API_DETAILS_HASH);
                        header.Add("API_KEY", apiKey);
                        header.Add("MERCHANT_ID", merchantId);
                        header.Add("REQUEST_ID", requestId);
                        header.Add("REQUEST_TS", REQUEST_TS);
                        string jsonRequestValidateMandateRequest = JsonConvert.SerializeObject(validateMandateRequest);
                        ActionReturn validateMandateRequestServerResult = Util.MakeRequestAndGetResponse(jsonRequestValidateMandateRequest, validateMandateUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER,header);
                        if (validateMandateRequestServerResult.Flag)
                        {
                            //validate otp
                            ValidateMandateResponse jsonValidateMandateResponse = JsonConvert.DeserializeObject<ValidateMandateResponse>(Convert.ToString(validateMandateRequestServerResult.ReturnedObject));
                            if (jsonValidateMandateResponse.statuscode == "00")
                            {
                                string validateMandateRequestOTPUrl = settings.Where(o => o.Key == "ValidateMandateRequestOTP").Select(o => o.Value).FirstOrDefault();
                                string validateMandateOTPRequestId = milliseconds.ToString();
                                List<object> authParameters = new List<object>();
                                authParameters.Add(new ValidateMandateRequestOTPParam1 { param1 = "OTP", value = "1234" });
                                authParameters.Add(new ValidateMandateRequestOTPParam2 { param2 = "CARD", value = "0441234567890" });
                                ValidateMandateRequestOTP validateMandateRequestOTP = new ValidateMandateRequestOTP
                                {
                                    remitaTransRef = jsonValidateMandateResponse.remitaTransRef,
                                    authParams = authParameters
                                };
                                string jsonRequestValidateMandateOTP = JsonConvert.SerializeObject(validateMandateRequestOTP);
                                ActionReturn validateMandateRequestOTPServerResult = Util.MakeRequestAndGetResponse(jsonRequestValidateMandateOTP, validateMandateRequestOTPUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                                if (validateMandateRequestServerResult.Flag)
                                {


                                    result.ReturnedObject = jsonResponse;
                                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                                    result.Message = "Success";
                                    result.Flag = true;
                                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                                }
                                else
                                {

                                }


                                
                            }
                            else
                            {
                                result.ReturnedObject = "";
                                result.UpdateReturn = UpdateReturn.NoChange;
                                result.Message = "Failed";
                                result.Flag = false;
                                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            }
                            
                        }
                        else
                        {
                            result.ReturnedObject = "";
                            result.UpdateReturn = UpdateReturn.NoChange;
                            result.Message = "Failed";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }

                        
                    }
                    else
                    {
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.NoChange;
                        result.Message = "Failed";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                    
                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Failed";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        private ActionReturn SetupMandate(Mandate mandate, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {


                string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                string mandateUrl = settings.Where(o => o.Key == "MandateUrl").Select(o => o.Value).FirstOrDefault();

                long milliseconds = DateTime.Now.Ticks;
                string requestId = milliseconds.ToString();
                string hashPlain = merchantId + serviceTypeId + requestId + mandate.amount + apiKey;
                mandate.serviceTypeId = serviceTypeId;
                mandate.merchantId = merchantId;
                mandate.requestId = requestId;
                mandate.hash = this.GetHash(hashPlain);
                mandate.mandateType = "SO";
                mandate.frequency = "Day";

                string jsonRequest = JsonConvert.SerializeObject(mandate);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, mandateUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.NO_HEADER, null);
                if (serverResult.Flag)
                {
                    MandateResponse jsonResponse = JsonConvert.DeserializeObject<MandateResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.statuscode == "040")
                    {

                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Mandate setup was successful";
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();


                    }
                    else
                    {
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.NoChange;
                        result.Message = "Mandate setup failed with an exception";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }

                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Failed";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "SetupMandate(Mandate mandate, List<AppConfigSetting> settings)";
            }
            return result;
        }

        public ActionReturn ValidateAdminUser1(string username,string password)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configResult = GetAppConfigSettings();
                if (configResult.Flag)
                {
                    List<AppConfigSetting> settings=(List<AppConfigSetting>)configResult.ReturnedObject;
                    
                    ActionReturn validationResult = FCMBIntegrator.ValidateActiveDirectoryUser(username, password, settings);
                    if (validationResult.Flag)
                    {
                        bool flag = true;// ActiveDirectoryUtility.isGroupMember(username, adminGroup, domainPath);
                        if (flag)
                        {

                            result.ReturnedObject = (AdminUser)validationResult.ReturnedObject;
                            result.UpdateReturn = UpdateReturn.ItemRetrieved;
                            result.Message = "User authenticated";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemRetrieved;
                            result.Message = "Invalid user profile.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = validationResult.ReturnedObject;
                        }
                        
                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Invalid user";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = validationResult.ReturnedObject;
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Error validating user, kindly contact your support team.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Invalid user";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                //Test mode
                //result.ReturnedObject = new AdminUser { FirstName = "Babatunde", LastName = "Olofin", UserId = username };
                //result.UpdateReturn = UpdateReturn.ItemRetrieved;
                //result.Message = "User authenticated";
                //result.Flag = true;
                //result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                result.ClassName = "UtilityService";
                result.MethodName = "ValidateAdminUser(string username,string password)";
            }
            return result;
        }

        public ActionReturn ValidateAdminUser(string username, string password)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configResult = GetAppConfigSettings();
                if (configResult.Flag)
                {
                    List<AppConfigSetting> settings = (List<AppConfigSetting>)configResult.ReturnedObject;

                    string domainPath = settings.Where(o => o.Key == "AD_DOMAIN_PATH").Select(o => o.Value).FirstOrDefault();
                    string domain = settings.Where(o => o.Key == "AD_DOMAIN").Select(o => o.Value).FirstOrDefault();
                    string adminGroup = settings.Where(o => o.Key == "ADMIN_AD_GROUP").Select(o => o.Value).FirstOrDefault();
                    bool flag = ActiveDirectoryUtility.Authenticate(username, password, domainPath);
                    if (flag)
                    {
                        flag = true;// ActiveDirectoryUtility.isGroupMember(username, adminGroup, domainPath);
                        if (flag)
                        {
                            string firstName = ActiveDirectoryUtility.GetADFirstName(username, domain);
                            string lastName = ActiveDirectoryUtility.GetADLastName(username, domain);

                            result.ReturnedObject = new AdminUser { FirstName = firstName, LastName = lastName, UserId = username };
                            result.UpdateReturn = UpdateReturn.ItemRetrieved;
                            result.Message = "User authenticated";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemRetrieved;
                            result.Message = "Invalid user profile.";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        }

                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Invalid user";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Error validating user, kindly contact your support team.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }


            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Invalid user";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                //Test mode
                result.ReturnedObject = new AdminUser { FirstName = "Babatunde", LastName = "Olofin", UserId = username };
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "User authenticated";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                result.ClassName = "UtilityService";
                result.MethodName = "ValidateAdminUser(string username,string password)";
            }
            return result;
        }

        public ActionReturn ChargeCustomerCard(DateTime dateTime)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn configs = this.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string SECRET_KEY = settings.Where(o => o.Key == "Paystack_SECRET_KEY").Select(o => o.Value).FirstOrDefault();
                string CHARGE_URL = settings.Where(o => o.Key == "Paystack_CHARGE_URL").Select(o => o.Value).FirstOrDefault();
                string VERIFY_URL = settings.Where(o => o.Key == "Paystack_VERIFY_URL").Select(o => o.Value).FirstOrDefault();
                string status = Enumerators.STATUS.NEW.ToString();

                var allGoals = targetGoalRepository.All.Include(o => o.CustomerProfile).Include(o=>o.TargetSavingsFrequency).AsNoTracking();
                List<TargetGoal> goals = allGoals.Where(o => o.NextRunDate == dateTime && o.GoalStatus==status).ToList();
                foreach(TargetGoal goal in goals)
                {
                    string amountString = (Decimal.Round(goal.TargetFrequencyAmount,2)).ToString();
                    long amount = Convert.ToInt64(amountString)*100;
                    ChargeCardRequest chargeCardRequest = new ChargeCardRequest
                    {
                        amount= amount,
                        authorization_code=goal.GoalCardTokenDetails,
                        email=goal.CustomerProfile.EmailAddress
                    };
                    string jsonRequest = JsonConvert.SerializeObject(chargeCardRequest);
                    WebHeaderCollection header = new WebHeaderCollection();
                    header.Add("Authorization", $"Bearer {SECRET_KEY}");
                    ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, CHARGE_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                    if (serverResult.Flag)
                    {
                        ChargeCardResponse jsonResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverResult.ReturnedObject));
                        if (jsonResponse.status && jsonResponse.data.status== "success")
                        {
                            //verify transaction
                            VERIFY_URL = $"{VERIFY_URL}/{jsonResponse.data.reference}";
                            ActionReturn serverVerifyResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, VERIFY_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                            if (serverVerifyResult.Flag)
                            {
                                ChargeCardResponse jsonVerifyResponse = JsonConvert.DeserializeObject<ChargeCardResponse>(Convert.ToString(serverVerifyResult.ReturnedObject));
                                if (jsonVerifyResponse.status && jsonVerifyResponse.data.status == "success")
                                {
                                    Domain.Models.Common.Transaction transaction = new Domain.Models.Common.Transaction
                                    {
                                        Amount = Decimal.Round(goal.TargetFrequencyAmount, 2),
                                        ApprovedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                        ApprovedDate = DateTime.Now,
                                        Narration = $"{jsonResponse.data.reference}",
                                        PostedBy = Enumerators.PAYMENT_GATEWAY.PAYSTACK.ToString(),
                                        PostedDate = DateTime.Now,
                                        TargetGoalsId = goal.TargetGoalsId,
                                        TransactionDate = DateTime.Now,
                                        TransactionType = Enumerators.TRANSACTION_TYPE.DEBIT.ToString()
                                    };
                                    //transactionService.AddTransaction(transaction);
                                    goal.NextRunDate = Util.GetNextRunDate(Convert.ToDateTime(goal.NextRunDate), goal.TargetSavingsFrequency.ValueInDays);

                                }

                            }
                            
                        }
                    }
                }

                using (TransactionScope scope = new TransactionScope())
                {
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    scope.Complete();
                }
                result.UpdateReturn = UpdateReturn.ItemUpdated;
                result.Message = "Done";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Invalid user";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "UtilityService";
                result.MethodName = "ChargeCustomerCard(DateTime dateTime)";
            }
            return result;
        }

        public async Task<ActionReturn> GetLiquidationAccountsByCustomerAsync(long customerId)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                List<Bank> banks = bankRepository.GetAll().Where(o => o.IsActive).ToList();

                //IEnumerable<TargetGoal> targetGoal = targetGoalRepository.GetAll().Where(o => o.GoalStatus == goalStatus).OrderByDescending(o => o.TargetGoalsId);
                List<LiquidationDetailsDTO> details = (from i in targetGoalRepository.All
                                                       where i.CustomerProfileId == customerId
                                                       select new LiquidationDetailsDTO
                                                       {
                                                           AccountNumber = i.LiquidationAccount,                                                           
                                                           BankCode = i.LiquidationAccountBankCode,
                                                           TargetGoalId = i.TargetGoalsId
                                                       }).ToList();

                foreach(LiquidationDetailsDTO detail in details)
                {
                    detail.Bank = banks.Where(o => o.Code == detail.BankCode).Select(o => o.BankName).FirstOrDefault();
                }
                result.ReturnedObject = details;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Liquidation details were retrieved successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Liquidation details could not be retrieved";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {
                result.ClassName = "CustomerService";
                result.MethodName = "GetTargetGoalsByCustomerAsync(long customerId)";
            }
            return await Task.FromResult(result);
        }
    }
}
