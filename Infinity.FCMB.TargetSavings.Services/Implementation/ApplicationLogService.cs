﻿using Infinity.FCMB.TargetSavings.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infinity.FCMB.TargetSavings.Domain.Models.Log;
using Infinity.FCMB.EntityFramework.Extensions.UnitOfWork;
using Infinity.FCMB.EntityFramework.Extensions.Repository;
using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using System.ComponentModel;
using System.Net.Mail;
using System.Net;
using System.Collections.Concurrent;
using System.IO;
using Newtonsoft.Json;

namespace Infinity.FCMB.TargetSavings.Services.Implementation
{
    public class ApplicationLogService : TargetSavingsServiceBase, IApplicationLogService
    {
        private static object mutex;
        private IUnitOfWork unitOfWork { get; set; }
        private IRepository<ApplicationLog> applicationLogRepository { get; set; }
        private IRepository<EmailLog> emailLogRepository { get; set; }
        private IRepository<EmailTemplate> emailTemplateRepository { get; set; }
        private IRepository<AppConfigSetting> appConfigSettingRepository { get; set; }
        private IRepository<ServiceRequestLog> serviceRequestLogRepository { get; set; }

        public ApplicationLogService(IUnitOfWork unitOfWork)
        {
            mutex = new object();
            unitOfWork.Database.CommandTimeout = new int?(20000);
            this.TargetSavingsUnitOfWorkUnitOfWork = unitOfWork;
            this.applicationLogRepository = new TargetSavingsRepositoryBase<ApplicationLog>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.emailLogRepository = new TargetSavingsRepositoryBase<EmailLog>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.emailTemplateRepository = new TargetSavingsRepositoryBase<EmailTemplate>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.appConfigSettingRepository = new TargetSavingsRepositoryBase<AppConfigSetting>(this.TargetSavingsUnitOfWorkUnitOfWork);
            this.serviceRequestLogRepository = new TargetSavingsRepositoryBase<ServiceRequestLog>(this.TargetSavingsUnitOfWorkUnitOfWork);
        }
        #region Dispose
        protected override void Dispose(bool disposing)
        {
            applicationLogRepository?.Dispose();
            emailLogRepository?.Dispose();
            serviceRequestLogRepository?.Dispose();
            appConfigSettingRepository?.Dispose();
            emailTemplateRepository?.Dispose();
            base.Dispose(disposing);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        public bool Add(ApplicationLog applicationlog)
        {
            bool flag = false;
            try
            {
                applicationlog.DatecCeated = DateTime.Now.Date;
                applicationlog.DateTimeCreated = DateTime.Now;
                this.applicationLogRepository.Add(applicationlog);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
            }
            catch(Exception ex)
            {

            }
            return flag;
        }

        public bool Add(ActionReturn result)
        {
            bool flag = false;
            try
            {
                ApplicationLog applicationLog = new ApplicationLog
                {
                    ApplicationName= "Infinity.FCMB.TargetSavings",
                    ClassName =result.ClassName,
                    MethodName=result.MethodName,
                    Flag = result.Flag.ToString(),
                    FlagDescription = result.FlagDescription,
                    Message = result.Message,
                    DatecCeated = DateTime.Now.Date,
                    DateTimeCreated = DateTime.Now,
                    ReturnedObject = result.FlagDescription == Enumerators.STATUS.EXCEPTION.ToString() ? TrimString((string)result.ReturnedObject) : ""
                };
                this.applicationLogRepository.Add(applicationLog);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return flag;
        }

        public bool AddEmail(EmailLog emailLog)
        {
            bool flag = false;
            try
            {
                DateTime today = DateTime.Today;
                emailLog.DateCreated = today.Date;
                emailLog.DateTimeCreated = today;
                emailLog.LastModifiedDate = today.Date;
                emailLog.LastModifiedDateTime = today;
                this.emailLogRepository.Add(emailLog);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return flag;
        }
        public bool Add(string className, string methodName,Exception exception)
        {
            bool flag = false;
            try
            {

                ApplicationLog applicationLog = new ApplicationLog
                {
                    ApplicationName = "Infinity.FCMB.TargetSavings",
                    ClassName =className,
                    MethodName=methodName,
                    Flag = "false",
                    FlagDescription = Enumerators.STATUS.EXCEPTION.ToString(),
                    Message = "An exception was thrown, service was not completed",
                    DatecCeated = DateTime.Now.Date,
                    DateTimeCreated = DateTime.Now,
                    ReturnedObject = this.TrimString(exception)
                };
                this.applicationLogRepository.Add(applicationLog);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {

            }
            return flag;
        }
        public void AddToFile(string message, string log_file, string logDirectoryPath)
        {
            try
            {
                
                log_file = DateTime.Today.ToString("'" + log_file + "'_yyyyMMdd'.txt'");
                if (!Directory.Exists(logDirectoryPath))
                {
                    Directory.CreateDirectory(logDirectoryPath);
                }
                
                lock (mutex)
                {
                    File.AppendAllText(logDirectoryPath + "\\" + log_file,
                            string.Format("{0:HH':'mm':'ss}|{1}\r\n", DateTime.Now, message));
                }
            }
            catch (Exception ex)
            {
            }
        }
        public void LogToFile(object log, string logDirectory, string fileName)
        {
            try
            {
                string logFile = $"{fileName}_{DateTime.Today.ToString("ddMMMyyyy")}.txt";
                logDirectory = logDirectory + "\\" + DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MMM") + "\\" + DateTime.Now.ToString("ddMMMyyy");
                if (!Directory.Exists(logDirectory))
                {
                    Directory.CreateDirectory(logDirectory);
                }
                lock (mutex)
                {

                    File.AppendAllText(logDirectory + "\\" + logFile, $"{DateTime.Now.ToString("HH:mm:ss")}|{JsonConvert.SerializeObject(log)}\r\n");
                }

            }
            catch (Exception ex) { }
        }

        public bool AddEmailToLog(EmailLog emailLog,Dictionary<string,string> values)
        {
            bool flag = false;
            try
            {
                List<AppConfigSetting> configSetting = appConfigSettingRepository.GetAll().ToList();
                string supportTeamEmail = configSetting.Where(o => o.Key == "SUPPORT_TEAM_EMAIL").Select(o => o.Value).FirstOrDefault();
                EmailTemplate emailTemplate = new EmailTemplate();
                string emailBody = string.Empty;
                switch (emailLog.EmailType)
                {
                    case "FIRST_EMAIL":
                        emailTemplate = emailTemplateRepository.GetAll().Where(o => o.IsActive == true && o.EmailType == "FIRST_EMAIL").FirstOrDefault();
                        if(emailTemplate != null)
                        {
                            emailBody = emailTemplate.Template
                                .Replace("#NAME#", values.Where(o =>  o.Key== "NAME").Select(o => o.Value).FirstOrDefault())
                                .Replace("#AMOUNT#", values.Where(o => o.Key == "AMOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#STARTDATE#", values.Where(o => o.Key == "STARTDATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#ENDDATE#", values.Where(o => o.Key == "ENDDATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#FREQUENCY#", values.Where(o => o.Key == "FREQUENCY").Select(o => o.Value).FirstOrDefault())
                                .Replace("#PLAN#", values.Where(o => o.Key == "PLAN").Select(o => o.Value).FirstOrDefault())
                                ;
                        }

                        break;
                    case "THREE_DAYS_DUE":
                        emailTemplate = emailTemplateRepository.GetAll().Where(o => o.IsActive == true && o.EmailType == "THREE_DAYS_DUE").FirstOrDefault();
                        if (emailTemplate != null)
                        {
                            emailBody = emailTemplate.Template
                                .Replace("#NAME#", values.Where(o => o.Key == "NAME").Select(o => o.Value).FirstOrDefault())
                                .Replace("#AMOUNT#", values.Where(o => o.Key == "AMOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#STARTDATE#", values.Where(o => o.Key == "STARTDATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#ENDDATE#", values.Where(o => o.Key == "ENDDATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#FREQUENCY#", values.Where(o => o.Key == "FREQUENCY").Select(o => o.Value).FirstOrDefault())
                                .Replace("#DUEDATE#", values.Where(o => o.Key == "DUEDATE").Select(o => o.Value).FirstOrDefault())
                                //.Replace("#PLAN#", values.Where(o => o.Key == "PLAN").Select(o => o.Value).FirstOrDefault())
                                ;
                        }

                        break;
                    case "FAILED_DEBIT_CUSTOMER":
                        emailTemplate = emailTemplateRepository.GetAll().Where(o => o.IsActive == true && o.EmailType == "FAILED_DEBIT_CUSTOMER").FirstOrDefault();
                        if (emailTemplate != null)
                        {
                            emailBody = emailTemplate.Template
                                .Replace("#NAME#", values.Where(o => o.Key == "NAME").Select(o => o.Value).FirstOrDefault())
                                .Replace("#AMOUNT#", values.Where(o => o.Key == "AMOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#STARTDATE#", values.Where(o => o.Key == "STARTDATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#ENDDATE#", values.Where(o => o.Key == "ENDDATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#FREQUENCY#", values.Where(o => o.Key == "FREQUENCY").Select(o => o.Value).FirstOrDefault())
                                .Replace("#PLANNAME#", values.Where(o => o.Key == "PLANNAME").Select(o => o.Value).FirstOrDefault())
                                ;
                        }

                        break;
                    case "COMPLETED_CUSTOMER":
                        
                        emailTemplate = emailTemplateRepository.GetAll().Where(o => o.IsActive == true && o.EmailType == "COMPLETED_CUSTOMER").FirstOrDefault();
                        if (emailTemplate != null)
                        {
                            emailBody = emailTemplate.Template
                                .Replace("#NAME#", values.Where(o => o.Key == "NAME").Select(o => o.Value).FirstOrDefault())
                                .Replace("#AMOUNT#", values.Where(o => o.Key == "AMOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#STARTDATE#", values.Where(o => o.Key == "STARTDATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#ENDDATE#", values.Where(o => o.Key == "ENDDATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#FREQUENCY#", values.Where(o => o.Key == "FREQUENCY").Select(o => o.Value).FirstOrDefault())
                                .Replace("#PLANNAME#", values.Where(o => o.Key == "PLANNAME").Select(o => o.Value).FirstOrDefault())
                                ;
                        }
                        break;
                    case "FAILED_SCHEDULE":
                        emailTemplate = emailTemplateRepository.GetAll().Where(o => o.IsActive == true && o.EmailType == "FAILED_SCHEDULE").FirstOrDefault();
                        emailLog.EmailTo = supportTeamEmail;
                        break;
                    case "NO_CUSTOMER_TO_DEBIT":
                        emailTemplate = emailTemplateRepository.GetAll().Where(o => o.IsActive == true && o.EmailType == "NO_CUSTOMER_TO_DEBIT").FirstOrDefault();
                        emailLog.EmailTo = supportTeamEmail;
                        break;
                    case "SWEEP_STATUS":

                        emailTemplate = emailTemplateRepository.GetAll().Where(o => o.IsActive == true && o.EmailType == "SWEEP_STATUS").FirstOrDefault();
                        if (emailTemplate != null)
                        {
                            emailBody = emailTemplate.Template
                                .Replace("#FLEXIBLE_AMOUNT#", values.Where(o => o.Key == "FLEXIBLE_AMOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#FIXED_AMOUNT#", values.Where(o => o.Key == "FIXED_AMOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#RUN_DATE#", values.Where(o => o.Key == "RUN_DATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#FIXED_ACCOUNT#", values.Where(o => o.Key == "FIXED_ACCOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#FLEXIBLE_ACCOUNT#", values.Where(o => o.Key == "FLEXIBLE_ACCOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#SWEEP_ACCOUNT#", values.Where(o => o.Key == "SWEEP_ACCOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#STATUS#", values.Where(o => o.Key == "STATUS").Select(o => o.Value).FirstOrDefault())                                
                                ;
                        }
                        break;
                    case "CARD_CHARGE_SWEEP":

                        emailTemplate = emailTemplateRepository.GetAll().Where(o => o.IsActive == true && o.EmailType == "CARD_CHARGE_SWEEP").FirstOrDefault();
                        if (emailTemplate != null)
                        {
                            emailBody = emailTemplate.Template
                                .Replace("#AMOUNT#", values.Where(o => o.Key == "AMOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#EXPENSE_ACCOUNT#", values.Where(o => o.Key == "EXPENSE_ACCOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#RUN_DATE#", values.Where(o => o.Key == "RUN_DATE").Select(o => o.Value).FirstOrDefault())
                                .Replace("#SAVINGS_ACCOUNT#", values.Where(o => o.Key == "SAVINGS_ACCOUNT").Select(o => o.Value).FirstOrDefault())
                                .Replace("#STATUS#", values.Where(o => o.Key == "STATUS").Select(o => o.Value).FirstOrDefault())
                                ;
                        }
                        break;
                    case "RESET_PASSWORD":

                        emailTemplate = emailTemplateRepository.GetAll().Where(o => o.IsActive == true && o.EmailType == "RESET_PASSWORD").FirstOrDefault();
                        if (emailTemplate != null)
                        {
                            emailBody = emailTemplate.Template
                                .Replace("#NAME#", values.Where(o => o.Key == "NAME").Select(o => o.Value).FirstOrDefault())
                                .Replace("#PASSWORD#", values.Where(o => o.Key == "PASSWORD").Select(o => o.Value).FirstOrDefault())
                                ;
                        }
                        break;
                }

                emailLog.EmailBody= emailBody
                    .Replace("#SENDERNAME#", configSetting.Where(o => o.Key == "SENDERNAME").Select(o => o.Value).FirstOrDefault())
                    .Replace("#SENDERTITLE#", configSetting.Where(o => o.Key == "SENDERTITLE").Select(o => o.Value).FirstOrDefault())
                    ;
                DateTime now = DateTime.Now;
                emailLog.Subject = emailTemplate.Subject;
                emailLog.DateCreated = now.Date;
                emailLog.DateTimeCreated = now;
                emailLog.LastModifiedDate = now.Date;
                emailLog.LastModifiedDateTime = now;

                this.emailLogRepository.Add(emailLog);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
            }
            catch (Exception ex)
            {
                Add("ApplicationLogService", "AddEmailToLog(EmailLog emailLog,Dictionary<string,string> values)", ex);
            }
            return flag;
        }

        public bool UpdateEmailLog(EmailLog emailLog)
        {
            bool flag = false;
            try
            {
                EmailLog thisEmailLog = emailLogRepository.Get(emailLog.Id);
                if(thisEmailLog != null)
                {
                    thisEmailLog.LastModifiedDate = emailLog.LastModifiedDate;
                    thisEmailLog.LastModifiedDateTime = emailLog.LastModifiedDateTime;
                    thisEmailLog.Response = emailLog.Response;
                    thisEmailLog.Status = emailLog.Status;
                    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                Add("ApplicationLogService", "UpdateEmailLog(EmailLog emailLog)", ex);
            }
            return flag;
        }

        public ActionReturn AddServiceLog(ServiceRequestLog serviceLog)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                serviceLog.RequestDate = DateTime.Now.Date;
                serviceLog.RequestDateTime = DateTime.Now;
                this.serviceRequestLogRepository.Add(serviceLog);
                this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                result.Flag = true;
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "An exception occurred";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
                Add(result);
            }
            finally
            {
                if (!result.Flag)
                {
                    this.AddServiceLog(serviceLog, Convert.ToString(result.ReturnedObject));
                }
            }
            return result;
        }
        public bool AddServiceLog(ServiceRequestLog serviceLog, string exception)
        {
            bool flag = false;
            try
            {
                // log to file path
                flag = true;
            }
            catch (Exception ex)
            {

            }
            return flag;
        }

        public bool SendPendingEmails()
        {
            bool flag = false;
            try
            {
                List<AppConfigSetting> configSetting = appConfigSettingRepository.GetAll().ToList();
                int emailTrialLimit = Convert.ToInt16(configSetting.Where(o => o.Key == "EMAIL_TRIAL_LIMIT").Select(o => o.Value).FirstOrDefault());
                string status = Enumerators.STATUS.NEW.ToString();
                List<EmailLog> pendingEmails = emailLogRepository.GetAll().Where(o => o.Status == status && o.Trial <= emailTrialLimit).ToList();
                

                //var partitionedEmails = Partitioner.Create<EmailLog>(pendingEmails, true);
                //flag=Parallel.ForEach(partitionedEmails, c => {
                //    Send(c, configSetting);
                //}).IsCompleted;

                foreach(EmailLog email in pendingEmails)
                {
                    Send(email, configSetting);
                }
                

            }
            catch (Exception ex)
            {
                Add("ApplicationLogService", "SendPendingEmails()", ex);
            }
            finally
            {
                emailLogRepository.Dispose();
                flag = true;
            }
            return flag;
        }

        private string TrimString(string message)
        {
            if (message.Length > 900)
            {
                message = message.Substring(0, 2001);
            }
            return message;
        }
        private string TrimString(Exception exception)
        {
            string message = string.Empty;
            try
            {
                int msg_length = 0;
                if (exception.InnerException == null)
                {
                    message = exception.Message;
                }
                else
                {
                    message = exception.InnerException.Message;
                }
                msg_length = message.Length;
                if (msg_length > 900)
                {
                    message = message.Substring(0, 900);
                }
            }
            catch { }
            return message;
        }

        private void Send(EmailLog message, List<AppConfigSetting> configSetting)
        {
            MailMessage mail_message = new MailMessage();
            
            try
            {
                string SMTP_SENDER = configSetting.Where(o => o.Key == "SMTP_SENDER").Select(o => o.Value).FirstOrDefault();
                string SMTP_HOST = configSetting.Where(o => o.Key == "SMTP_HOST").Select(o => o.Value).FirstOrDefault();
                string SMTP_PORT = configSetting.Where(o => o.Key == "SMTP_PORT").Select(o => o.Value).FirstOrDefault();
                string SMTP_REQUIRES_AUTH = configSetting.Where(o => o.Key == "SMTP_REQUIRES_AUTH").Select(o => o.Value).FirstOrDefault();
                string SMTP_AUTH_USRER = configSetting.Where(o => o.Key == "SMTP_AUTH_USRER").Select(o => o.Value).FirstOrDefault();
                string SMTP_AUTH_PASS = configSetting.Where(o => o.Key == "SMTP_AUTH_PASS").Select(o => o.Value).FirstOrDefault();
                string SMTP_AUTH_DOMAIN = configSetting.Where(o => o.Key == "SMTP_AUTH_DOMAIN").Select(o => o.Value).FirstOrDefault();
                string SMTP_OFFLINE_DELIVERY = configSetting.Where(o => o.Key == "SMTP_OFFLINE_DELIVERY").Select(o => o.Value).FirstOrDefault();
                string SMTP_OFFLINE_DELIVERY_PATH = configSetting.Where(o => o.Key == "SMTP_OFFLINE_DELIVERY_PATH").Select(o => o.Value).FirstOrDefault();
                String SMTP_TIMEOUT_IN_SECONDS = configSetting.Where(o => o.Key == "SMTP_TIMEOUT_IN_SECONDS").Select(o => o.Value).FirstOrDefault();

                int SMTP_TIMEOUT = 0;
                if (!int.TryParse(SMTP_TIMEOUT_IN_SECONDS, out SMTP_TIMEOUT)) SMTP_TIMEOUT = 10000;
                SMTP_TIMEOUT *= 1000;

                mail_message.From = new MailAddress(SMTP_SENDER);
                mail_message.To.Add(message.EmailTo);
                if(message.EmailCopy != null)
                {
                    mail_message.CC.Add(message.EmailCopy);
                }

                if(message.EmailBlindCopy != null)
                {
                    mail_message.Bcc.Add(message.EmailBlindCopy);
                }
                

                if (mail_message.To.Count > 0)
                {
                    SmtpClient client = new SmtpClient(SMTP_HOST, Convert.ToInt16(SMTP_PORT));
                    mail_message.Subject = message.Subject;
                    string msg_body = message.EmailBody;
                    
                    mail_message.Body = msg_body;
                    mail_message.IsBodyHtml = true;

                    if (SMTP_REQUIRES_AUTH == "Y")
                    {

                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(SMTP_AUTH_USRER, SMTP_AUTH_PASS, SMTP_AUTH_DOMAIN);
                    }
                    else
                    {
                        client.UseDefaultCredentials = true;
                    }
                    message.LastModifiedDate = DateTime.Now.Date;
                    message.LastModifiedDateTime = DateTime.Now;

                    //'------------------Remove Before Go Live----------------------------------
                    if (SMTP_OFFLINE_DELIVERY == "Y")
                    {
                        if (!Directory.Exists(SMTP_OFFLINE_DELIVERY_PATH))
                        {
                            Directory.CreateDirectory(SMTP_OFFLINE_DELIVERY_PATH);
                        }
                        client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                        client.PickupDirectoryLocation = SMTP_OFFLINE_DELIVERY_PATH;
                    }

                    //'------------------Remove Before Go Live ---------------------------------

                    client.Timeout = SMTP_TIMEOUT;
                    client.Send(mail_message);

                    message.LastModifiedDate = DateTime.Now.Date;
                    message.LastModifiedDateTime = DateTime.Now;
                    message.Response = "Message Sent Ok";
                    message.Status = Enumerators.STATUS.COMPLETED.ToString();
                    message.Trial = message.Trial + 1;
                    //EmailLog thisEmailLog = emailLogRepository.Get(message.Id);
                    //if (thisEmailLog != null)
                    //{
                    //    thisEmailLog.LastModifiedDate = message.LastModifiedDate;
                    //    thisEmailLog.LastModifiedDateTime = message.LastModifiedDateTime;
                    //    thisEmailLog.Response = message.Response;
                    //    thisEmailLog.Status = message.Status;
                    //    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                    //}
                    UpdateEmailLog(message);

                    //client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                    //client.SendAsync(mail_message, message);

                }
            }
            catch (Exception ex)
            {
                message.LastModifiedDate = DateTime.Now.Date;
                message.LastModifiedDateTime = DateTime.Now;
                message.Response = ex.ToString();//.Substring(0, 1000);
                message.Trial = message.Trial + 1;
                //hasError = true;
                //EmailLog thisEmailLog = emailLogRepository.Get(message.Id);
                //if (thisEmailLog != null)
                //{
                //    thisEmailLog.LastModifiedDate = message.LastModifiedDate;
                //    thisEmailLog.LastModifiedDateTime = message.LastModifiedDateTime;
                //    thisEmailLog.Response = message.Response;
                //    thisEmailLog.Status = message.Status;
                //    this.TargetSavingsUnitOfWorkUnitOfWork.SaveChanges();
                //}
                UpdateEmailLog(message);
            }
            finally
            {
                //if (hasError)
                //{
                //    UpdateEmailLog(message);
                //}
                
                mail_message.Dispose();
            }
        }
        
        private void client_SendCompleted(object sender, AsyncCompletedEventArgs e)
        {

            EmailLog message = (EmailLog)e.UserState;
            try
            {
                message.Trial = message.Trial + 1;
                if (e.Cancelled)
                {
                    message.Response = e.Cancelled.ToString();
                }
                if (e.Error != null)
                {
                    message.Response = e.Error.Message;
                }
                else
                {
                    message.Response = "Message Sent Ok";
                    message.Status = Enumerators.STATUS.COMPLETED.ToString();
                }
            }
            catch (Exception ex)
            {
                message.Response = ex.ToString();
            }
            finally
            {
                UpdateEmailLog(message);
            }
        }

    }
}
