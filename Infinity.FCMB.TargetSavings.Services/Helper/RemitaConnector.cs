﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Remita;
using Infinity.FCMB.TargetSavings.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;

namespace Infinity.FCMB.TargetSavings.Services.Helper
{
    public class RemitaConnector
    {
        private IUtilityService utilityService { get; set; }

        public RemitaConnector(IUtilityService utilityService)
        {
            this.utilityService = utilityService;
        }

        public ActionReturn SetupMandate(Mandate mandate)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                
                ActionReturn configs = this.utilityService.GetAppConfigSettings();
                List<AppConfigSetting> settings = (List<AppConfigSetting>)configs.ReturnedObject;
                string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                string mandateUrl = settings.Where(o => o.Key == "MandateUrl").Select(o => o.Value).FirstOrDefault();

                long milliseconds = DateTime.Now.Ticks;
                string requestId = milliseconds.ToString();
                string hashPlain = merchantId + serviceTypeId + requestId + mandate.amount + apiKey;
                mandate.hash = this.utilityService.GetHash(hashPlain);
                mandate.mandateType = "SOACT";
                mandate.frequency = "1";

                string jsonRequest = JsonConvert.SerializeObject(mandate);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, mandateUrl, "", "POST",false,null);
                if (serverResult.Flag)
                {
                    MandateResponse jsonResponse = JsonConvert.DeserializeObject<MandateResponse>(Convert.ToString(serverResult.ReturnedObject));
                    result.ReturnedObject = jsonResponse;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Success";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Failed";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
                
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }
    }
}
