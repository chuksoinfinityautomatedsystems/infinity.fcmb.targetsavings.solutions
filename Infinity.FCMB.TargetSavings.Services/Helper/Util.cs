﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.Customer;
using Infinity.FCMB.TargetSavings.Domain.Models.Remita;
using Newtonsoft.Json;
using OtpNet;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Services.Helper
{
    public static class Util
    {
        public static ActionReturn MakeRequestAndGetResponse(string datastring, string URL, string certificatePath, string Method,bool hasHeader, WebHeaderCollection headerCollection, string contentType=null, string logPath="")
        {
            string serverResponse = string.Empty;
            HttpWebRequest request = null;
            HttpWebResponse response = null;
            Stream dataStream = null;
            ActionReturn result = new ActionReturn();
            try
            {
                request = (HttpWebRequest)WebRequest.Create(URL);
                request.ContentType =string.IsNullOrEmpty(contentType)? "application/json":contentType;
                request.Method = Method;
                
                if (hasHeader)
                {
                    //request.Headers = headerCollection;
                    request.Headers.Add(headerCollection);
                }


                if (!string.IsNullOrEmpty(certificatePath))
                {
                    X509Certificate2 cert = new X509Certificate2(certificatePath);
                    request.ClientCertificates.Add(cert);
                }

                if (URL.Contains("https://"))
                {
                    //supress unsigned certificate
                    ServicePointManager.ServerCertificateValidationCallback =
                              new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);
                }

                if (Method == CONSTANT.GET)
                {
                    using (response = (HttpWebResponse)request.GetResponse())
                    {
                        using (dataStream = response.GetResponseStream())
                        {
                            using (StreamReader streamReader = new StreamReader(dataStream))
                            {
                                string responseString = streamReader.ReadToEnd();
                                if(request.ContentType== "application/json")
                                {
                                    serverResponse = Regex.Replace(responseString, @"^.+?\(|\)$", "");
                                }
                                
                            }
                        }
                    }
                }
                else
                {
                    using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                    {

                        writer.WriteLine(datastring);
                        writer.Close();

                        // Send the data to the webserver
                        using (response = (HttpWebResponse)request.GetResponse())
                        {
                            using (dataStream = response.GetResponseStream())
                            {
                                using (StreamReader streamReader = new StreamReader(dataStream))
                                {
                                    string responseString = streamReader.ReadToEnd();
                                    if (request.ContentType == "application/json")
                                    {
                                        serverResponse = Regex.Replace(responseString, @"^.+?\(|\)$", "");
                                    }
                                        
                                }
                            }
                        }
                    }
                }
                
                if (!string.IsNullOrEmpty(serverResponse))
                {
                    result.ReturnedObject = serverResponse;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Server Response";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                else
                {
                    result.ReturnedObject = serverResponse;
                    result.UpdateReturn = UpdateReturn.ItemRetrieved;
                    result.Message = "Server Response";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
            }
            //catch (WebException webEx)
            //{
                
            //    result.ReturnedObject = webEx.ToString()+ ":Web Response:"+ JsonConvert.SerializeObject(webEx.Response);
            //    result.UpdateReturn = UpdateReturn.ItemRetrieved;
            //    result.Message = "Server Response";
            //    result.Flag = false;
            //    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
            //}
            catch (WebException webEx)
            {

                string responseText = "No Error here";
                HttpWebResponse webResp = (HttpWebResponse)webEx.Response;

                if (webResp != null)
                {
                    var encoding = ASCIIEncoding.ASCII;
                    using (var reader = new System.IO.StreamReader(webResp.GetResponseStream(), encoding))
                    {
                        responseText = webResp.StatusCode+" at Requery: " + reader.ReadToEnd();



                    }

                    //switch (webResp.StatusCode)
                    //{

                    //    case HttpStatusCode.BadRequest:

                    //        var encoding = ASCIIEncoding.ASCII;
                    //        using (var reader = new System.IO.StreamReader(webResp.GetResponseStream(), encoding))
                    //        {
                    //            responseText = "BAD REQUEST (400) at Requery: " + reader.ReadToEnd();

                                

                    //        }
                    //        break;

                    //    case HttpStatusCode.NotFound: // 404
                    //        responseText ="Error has occued at Webclient => 404\n";

                    //        break;
                    //    case HttpStatusCode.InternalServerError: // 500
                    //        responseText = "Error has occued at Webclient => 500\n";

                    //        break;
                    //    case HttpStatusCode.NotModified: // 304
                    //        responseText ="Error has occued at Webclient => 304\n";

                    //        break;
                    //    default:

                    //        encoding = ASCIIEncoding.ASCII;
                    //        using (var reader = new System.IO.StreamReader(webResp.GetResponseStream(), encoding))
                    //        {
                    //            responseText = "BAD REQUEST (400) at Requery: " + reader.ReadToEnd();

                                

                    //        }

                    //        break;
                    //}
                }

                result.ReturnedObject = webEx.ToString() + "\n:Web Response:" + JsonConvert.SerializeObject(webEx.Response) + "\n:PARSED RESPONSE:" + responseText;
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Server Response";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
            }
            catch (Exception ex)
            {
                
                result.ReturnedObject = ex.ToString();
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "Server Response";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
            }
            finally
            {
                //dataStream.Close();
                //response.Close();
                List<string> headerList = new List<string>();
                headerList.Add($"[Key:URL, Value:{URL}]");
                foreach (var headerKey in headerCollection.Keys)
                {
                    var value = headerCollection.GetValues(headerKey.ToString());
                    headerList.Add($"[Key:{headerKey}, Value:{value[0]}]");
                }
                result.ServiceRequest = $"REQUEST HEARDER: {JsonConvert.SerializeObject(headerList)} REQUEST BODY: {datastring}";
                result.ServiceResponse = serverResponse;
                new FileLogger().LogToFile(result,logPath,"IntegrationServiceLogs");
            }
            return result;
        }

        private static bool IgnoreCertificateErrorHandler(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public static DateTime GetNextRunDate(DateTime dateTime ,int number)
        {
            return dateTime.AddDays(number);
        }
        public static string GetHash(string hashString)
        {
            System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
            Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hashString));
            sha512.Clear();
            string hashed = BitConverter.ToString(EncryptedSHA512).Replace("-", "").ToLower();
            return hashed;
        }

        public static byte[] GetHashInByte(string hashString)
        {
            System.Security.Cryptography.SHA512Managed sha512 = new System.Security.Cryptography.SHA512Managed();
            Byte[] EncryptedSHA512 = sha512.ComputeHash(System.Text.Encoding.UTF8.GetBytes(hashString));
            sha512.Clear();
            
            return EncryptedSHA512;
        }

        public static string ConvertObjectToJson(object objectToConvert)
        {
            return JsonConvert.SerializeObject(objectToConvert);
        }
        public static string Encrypt(string data)
        {
            string IV = "BLninKnUIcpCReM9";
            string Key = "4TfvrlH+JyITLMQS";
            return AesEncryptionAndDecryption.Encrypt(data, Key, IV); 
            //return data;
        }
        public static string Decrypt(string data)
        {
            string IV = "BLninKnUIcpCReM9";
            string Key = "4TfvrlH+JyITLMQS";
            return AesEncryptionAndDecryption.Decrypt(data, Key, IV);
            //return data;
        }

        public static string GetEncryptedHashValue2(string hashObjectName, object hashObject)
        {
            string hashed = string.Empty;
            switch (hashObjectName)
            {
                case CONSTANT.CUSTOMER_PROFILE:
                    CustomerProfile profile = (CustomerProfile)hashObject;

                    hashed = Encrypt($"{profile.BiometricVerificationNo}|{profile.EmailAddress}|{profile.PhoneNumber}" +
                        $"|{profile.ProfileStatus}|{profile.NextOfKinEmail}|{profile.NextOfKinFullName}|{profile.NextOfKinPhone}");
                    break;
                case CONSTANT.DEBIT_ACCOUNT_PROFILE:
                    TargetDebitAccountProfile DebitProfile = (TargetDebitAccountProfile)hashObject;

                    hashed = Encrypt($"{DebitProfile.TargetGoalId}|{DebitProfile.MandateId}|{DebitProfile.RequestId}" +
                        $"|{DebitProfile.DateCreated.ToString("ddMMMyyyy:HH:mm:ss")}|{DebitProfile.IsActive}|{DebitProfile.Status}");
                    break;
                case CONSTANT.TARGET_GOAL:
                    TargetGoal  goal = (TargetGoal)hashObject;

                    hashed = Encrypt($"{goal.CustomerProfileId}|{goal.SetUpDate.ToString("ddMMMyyyy:HH:mm:ss")}|{goal.TargetAmount}" +
                        $"|{goal.TargetFrequency}|{goal.TargetFrequencyAmount}|{goal.GoalBalance}|{Convert.ToDateTime(goal.GoalStartDate).ToString("ddMMMyyyy:HH:mm:ss")}" +
                        $"|{Convert.ToDateTime(goal.GoalEndDate).ToString("ddMMMyyyy:HH:mm:ss")}|{goal.GoalStartAmount}|{goal.GoalDebitType}|{goal.GoalDebitAccountNo}|{goal.GoalDebitBank}" +
                        $"|{goal.GoalStatus}|{goal.GoalCardTokenDetails}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}|{goal.LiquidationMode}");
                    break;
                case CONSTANT.REQUEST:
                    TargetSavingsRequest request = (TargetSavingsRequest)hashObject;

                    hashed = Encrypt($"{request.ApprovalStatus}|{Convert.ToDateTime(request.DateTreated).ToString("ddMMMyyyy:HH:mm:ss")}|{request.LiquidationAmount}" +
                        $"|{request.RequestDate.ToString("ddMMMyyyy:HH:mm:ss")}|{request.RequestDescription}|{request.RequestTypeId}|{request.Status}|{request.TargetGoalId}|{request.TreatedBy}");
                    break;
                case CONSTANT.TRANSACTION:
                    Transaction transaction = (Transaction)hashObject;

                    hashed = Encrypt($"{transaction.Amount}|{transaction.ApprovedBy}|{Convert.ToDateTime(transaction.ApprovedDate).ToString("ddMMMyyyy:HH:mm:ss")}" +
                        $"|{transaction.PostedBy}|{Convert.ToDateTime(transaction.PostedDate).ToString("ddMMMyyyy:HH:mm:ss")}|{transaction.TargetGoalsId}|{Convert.ToDateTime(transaction.TransactionDate).ToString("ddMMMyyyy:HH:mm:ss")}|{transaction.TransactionType}");
                    break;

            }
            return hashed;
        }

        public static string GetEncryptedHashValue(string hashObjectName, object hashObject)
        {
            string hashed = string.Empty;
            switch (hashObjectName)
            {
                case CONSTANT.CUSTOMER_PROFILE:
                    CustomerProfile profile = (CustomerProfile)hashObject;

                    hashed = Encrypt($"{profile.BiometricVerificationNo}|{profile.EmailAddress}|{profile.PhoneNumber}" +
                        $"|{profile.ProfileStatus}|{profile.NextOfKinEmail}|{profile.NextOfKinFullName}|{profile.NextOfKinPhone}");
                    break;
                case CONSTANT.DEBIT_ACCOUNT_PROFILE:
                    TargetDebitAccountProfile DebitProfile = (TargetDebitAccountProfile)hashObject;

                    hashed = Encrypt($"{DebitProfile.TargetGoalId}|{DebitProfile.MandateId}|{DebitProfile.RequestId}" +
                        $"|{DebitProfile.DateCreated.ToString("ddMMMyyyy:HH:mm:ss")}|{DebitProfile.IsActive}|{DebitProfile.Status}");
                    break;
                case CONSTANT.TARGET_GOAL:
                    TargetGoal goal = (TargetGoal)hashObject;

                    hashed = Encrypt($"{goal.CustomerProfileId}|{goal.SetUpDate.ToString("ddMMMyyyy:HH:mm:ss")}|{goal.TargetAmount}" +
                        $"|{goal.TargetFrequency}|{goal.TargetFrequencyAmount}|{goal.GoalBalance}|{Convert.ToDateTime(goal.GoalStartDate).ToString("ddMMMyyyy:HH:mm:ss")}" +
                        $"|{Convert.ToDateTime(goal.GoalEndDate).ToString("ddMMMyyyy:HH:mm:ss")}|{goal.GoalStartAmount}|{goal.GoalDebitType}|{goal.GoalDebitAccountNo}|{goal.GoalDebitBank}" +
                        $"|{goal.GoalStatus}|{goal.GoalCardTokenDetails}|{goal.LiquidationAccount}|{goal.LiquidationAccountBankCode}|{goal.LiquidationMode}");
                    break;
                case CONSTANT.REQUEST:
                    TargetSavingsRequest request = (TargetSavingsRequest)hashObject;

                    hashed = Encrypt($"{request.ApprovalStatus}|{Convert.ToDateTime(request.DateTreated).ToString("ddMMMyyyy:HH:mm:ss")}|{request.LiquidationAmount}" +
                        $"|{request.RequestDate.ToString("ddMMMyyyy:HH:mm:ss")}|{request.RequestDescription}|{request.RequestTypeId}|{request.Status}|{request.TargetGoalId}|{request.TreatedBy}");
                    break;
                case CONSTANT.TRANSACTION:
                    Transaction transaction = (Transaction)hashObject;

                    hashed = Encrypt($"{transaction.Amount}|{transaction.ApprovedBy}|{Convert.ToDateTime(transaction.ApprovedDate).ToString("ddMMMyyyy:HH:mm:ss")}" +
                        $"|{transaction.PostedBy}|{Convert.ToDateTime(transaction.PostedDate).ToString("ddMMMyyyy:HH:mm:ss")}|{transaction.TargetGoalsId}|{Convert.ToDateTime(transaction.TransactionDate).ToString("ddMMMyyyy:HH:mm:ss")}|{transaction.TransactionType}");
                    break;
                case CONSTANT.TRANSACTION_INTEREST:
                    TransactionInterest transactionInterest = (TransactionInterest)hashObject;

                    hashed = Encrypt($"{transactionInterest.Amount}|{transactionInterest.GoalBalanceAmount}|{transactionInterest.TargetGoalsId}|{Convert.ToDateTime(transactionInterest.TransactionDate).ToString("ddMMMyyyy:HH:mm:ss")}");
                    break;
                


            }
            hashed = Encrypt("NA");
            return hashed;
        }
        public static ActionReturn SetupMandate(Mandate mandate, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                string mandateUrl = settings.Where(o => o.Key == "MandateUrl").Select(o => o.Value).FirstOrDefault();

                long milliseconds = DateTime.Now.Ticks;
                string requestId = milliseconds.ToString();
                string hashPlain = merchantId + serviceTypeId + requestId + mandate.amount + apiKey;
                mandate.serviceTypeId = serviceTypeId;
                mandate.merchantId = merchantId;
                mandate.requestId = requestId;
                mandate.hash = GetHash(hashPlain);
                mandate.mandateType = "SO";
                mandate.frequency = "Day";

                string jsonRequest = JsonConvert.SerializeObject(mandate);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, mandateUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.NO_HEADER, null);
                if (serverResult.Flag)
                {
                    MandateResponse jsonResponse = JsonConvert.DeserializeObject<MandateResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.statuscode == "040")
                    {

                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Success";
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();


                    }
                    else
                    {
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.NoChange;
                        result.Message = "Failed";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }

                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Failed";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public static ActionReturn RequestMandateOTP(TargetDebitAccountProfile profile , List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                string validateMandateUrl = settings.Where(o => o.Key == "ValidateMandateUrl").Select(o => o.Value).FirstOrDefault();
                string API_TOKEN = settings.Where(o => o.Key == "API_TOKEN").Select(o => o.Value).FirstOrDefault();

                long milliseconds = DateTime.Now.Ticks;                
                ValidateMandateRequest validateMandateRequest = new ValidateMandateRequest
                {
                    mandateId = profile.MandateId,
                    requestId = profile.RequestId
                };
                string API_DETAILS_HASH = GetHash($"{apiKey}{profile.RequestId}{API_TOKEN}");

                DateTime today = DateTime.UtcNow;
                string REQUEST_TS = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss+000000");
                WebHeaderCollection header = new WebHeaderCollection();
                header.Add("API_DETAILS_HASH", API_DETAILS_HASH);
                header.Add("API_KEY", apiKey);
                header.Add("MERCHANT_ID", merchantId);
                header.Add("REQUEST_ID", profile.RequestId);
                header.Add("REQUEST_TS", REQUEST_TS);
                string jsonRequestValidateMandateRequest = JsonConvert.SerializeObject(validateMandateRequest);
                ActionReturn validateMandateRequestServerResult = Util.MakeRequestAndGetResponse(jsonRequestValidateMandateRequest, validateMandateUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                if (validateMandateRequestServerResult.Flag)
                {
                    ValidateMandateResponse jsonValidateMandateResponse = JsonConvert.DeserializeObject<ValidateMandateResponse>(Convert.ToString(validateMandateRequestServerResult.ReturnedObject));
                    if (jsonValidateMandateResponse.statuscode == "00")
                    {
                        result.ReturnedObject = jsonValidateMandateResponse;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Mandate otp request was successful";
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        result.ReturnedObject = jsonValidateMandateResponse.status;
                        result.UpdateReturn = UpdateReturn.NoChange;
                        result.Message = "Mandate validation failed";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }

                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Mandate validation failed on communication with remote server.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
            }
            catch(Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
            
        }

        public static ActionReturn ValidateDebitInstruction(string mandateId,string requestId, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string merchantId = settings.Where(o => o.Key == "MerchantId").Select(o => o.Value).FirstOrDefault();
                string serviceTypeId = settings.Where(o => o.Key == "ServiceTypeId").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "ApiKey").Select(o => o.Value).FirstOrDefault();
                string validateDebitUrl = settings.Where(o => o.Key == "REMITA_DEBIT_STATUS_URL").Select(o => o.Value).FirstOrDefault();
                
                //mandateId + merchantId + requestId + apiKey
                long milliseconds = DateTime.Now.Ticks;
                DesbitStatusRequest validateDebitRequest = new DesbitStatusRequest
                {
                    mandateId = mandateId,
                    requestId = requestId,
                    hash=GetHash($"{mandateId}{merchantId}{apiKey}"),
                    merchantId= merchantId
                };
                
                string jsonRequestValidateDebitRequest = JsonConvert.SerializeObject(validateDebitRequest);
                ActionReturn validateMandateRequestServerResult = Util.MakeRequestAndGetResponse(jsonRequestValidateDebitRequest, validateDebitUrl, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, null);
                if (validateMandateRequestServerResult.Flag)
                {
                    dynamic jsonValidateMandateResponse = JsonConvert.DeserializeObject(Convert.ToString(validateMandateRequestServerResult.ReturnedObject));
                    if (jsonValidateMandateResponse.statuscode == "00")
                    {
                        result.ReturnedObject = jsonValidateMandateResponse;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Debit was successful";
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        result.ReturnedObject = jsonValidateMandateResponse;
                        result.UpdateReturn = UpdateReturn.NoChange;
                        result.Message = "Debit validation failed";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    }

                }
                else
                {
                    result.ReturnedObject = "";
                    result.UpdateReturn = UpdateReturn.NoChange;
                    result.Message = "Debit validation failed on communication with remote server.";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;

        }

        private static byte[] GenerateKey()
        {
            //var key = KeyGeneration.GenerateRandomKey(20);
            var base32String = "4TfvrlH+JyITLMQNDJKSHDKJSJKDH783782jjdh+uhdjdghjsn42627nopskdS";
            var base32Bytes = GetHashInByte(base32String);
            return base32Bytes;
        }
        public static ActionReturn GenerateOtp()
        {
            ActionReturn result = new ActionReturn();
            try
            {
                
                var base32Bytes = GenerateKey();
                var hotp = new Hotp(base32Bytes,OtpHashMode.Sha512,6);
                //var otp = hotp.ComputeHOTP(3);

                var totp = new Totp(base32Bytes, 30, OtpHashMode.Sha512, 6);
                var otp2 = totp.ComputeTotp();

                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "Success";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                //result.ReturnedObject = "123456";
                result.ReturnedObject = otp2;

                //bool flag = hotp.VerifyHotp(otp+1, 3);
                //bool flag1 = hotp.VerifyHotp(otp+1, 3);
                //bool flag2 = hotp.VerifyHotp(otp+1, 3);
                //bool flag3 = hotp.VerifyHotp(otp, 3);

                
                //long outTimeStep;
                //bool flag5 = totp.VerifyTotp(otp2, out outTimeStep);
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }


        public static ActionReturn ValidateOtp(string otp)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                var base32Bytes = GenerateKey();
                var hotp = new Hotp(base32Bytes, OtpHashMode.Sha512, 6);
                if (hotp.VerifyHotp(otp, 3))
                //if (otp == "123456")
                {
                    result.UpdateReturn = UpdateReturn.ItemCreated;
                    result.Message = "Success";
                    result.Flag = true;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    result.ReturnedObject = "";
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemCreated;
                    result.Message = "One time password provided is invalid";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = "";
                }
                

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public static decimal ComputeCardFee(decimal amount, List<AppConfigSetting> settings)
        {
            decimal chargedFee = 0;
            string PAYSTACK_CHARGE_PERCENTAGE = settings.Where(o => o.Key == "PAYSTACK_CHARGE_PERCENTAGE").Select(o => o.Value).FirstOrDefault();
            string PAYSTACK_CHARGE_LIMIT_AMOUNT = settings.Where(o => o.Key == "PAYSTACK_CHARGE_LIMIT_AMOUNT").Select(o => o.Value).FirstOrDefault();

            decimal chargePercent = 0;
            decimal chargeLimitAMount = 0;
            bool chargeFlag = decimal.TryParse(PAYSTACK_CHARGE_PERCENTAGE, out chargePercent);
            bool limitFlag = decimal.TryParse(PAYSTACK_CHARGE_LIMIT_AMOUNT, out chargeLimitAMount);

            chargedFee = (chargePercent / 100) * amount;

            if (chargedFee > chargeLimitAMount) chargedFee = chargeLimitAMount;

            return chargedFee;
        }

    }
}
