﻿using Infinity.FCMB.EntityFramework.Extensions;
using Infinity.FCMB.TargetSavings.Domain.Models.Common;
using Infinity.FCMB.TargetSavings.Domain.Models.FCMB;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Services.Helper
{
    public static class FCMBIntegrator
    {
        public static ActionReturn NameEnquiry(string accountNumber, string bankCode, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                
                string FCMB_INTEGRATION_URL = settings.Where(o => o.Key == "FCMB_INTEGRATION_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_NAME_ENQ_URL = settings.Where(o => o.Key == "FCMB_NAME_ENQ_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_INTER_BANK_NAME_ENQ_URL = settings.Where(o => o.Key == "FCMB_INTER_BANK_NAME_ENQ_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                string FCMB_CHANNEL_CODE = settings.Where(o => o.Key == "FCMB_CHANNEL_CODE").Select(o => o.Value).FirstOrDefault();

                NameEnquiryRequest nameEnquiryRequest = new NameEnquiryRequest
                {
                    AccountNumber = accountNumber,
                    BankCode = bankCode
                };
                InterBankNameEquiryRequest interBankNameEnquiryRequest = new InterBankNameEquiryRequest
                {
                    AccountNumber = accountNumber,
                    ChannelCode = FCMB_CHANNEL_CODE,
                    DestinationInstitutionCode = bankCode
                };

                string apiKey = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppKey").Select(o => o.Value).FirstOrDefault();
                string appId = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppId").Select(o => o.Value).FirstOrDefault();
                WebHeaderCollection header = new WebHeaderCollection();
                header.Add("AppId", appId);
                header.Add("AppKey", apiKey);

                string jsonRequest = "";
                string url;
                string responseCode = "";
                if (bankCode == FCMB_BANK_CODE)
                {
                    jsonRequest = JsonConvert.SerializeObject(nameEnquiryRequest);
                    url = FCMB_INTEGRATION_URL + FCMB_NAME_ENQ_URL;
                    responseCode = "200";
                }
                else
                {
                    jsonRequest = JsonConvert.SerializeObject(interBankNameEnquiryRequest);
                    url = FCMB_INTEGRATION_URL + FCMB_INTER_BANK_NAME_ENQ_URL;
                    responseCode = "200";
                }

                
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, url, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                if (serverResult.Flag)
                {
                    FCMBResponse jsonResponse = JsonConvert.DeserializeObject<FCMBResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.StatusCode == responseCode)
                    {
                        //success
                        //NameEnquiryResponse dataResponse = JsonConvert.DeserializeObject<NameEnquiryResponse>(Convert.ToString(jsonResponse.ResponseData));
                        //jsonResponse.ResponseData = dataResponse;
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        //failed
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = jsonResponse;
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "Error communicating with server";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = serverResult;

                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public static ActionReturn GetTransactionStatus(string transactionId, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                GetTransactionStatusRequest request = new GetTransactionStatusRequest
                {
                   TransactionId=transactionId
                };
                string FCMB_INTEGRATION_URL = settings.Where(o => o.Key == "FCMB_INTEGRATION_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_TRANX_STATUS_URL = settings.Where(o => o.Key == "FCMB_TRANX_STATUS_URL").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppKey").Select(o => o.Value).FirstOrDefault();
                string appId = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppId").Select(o => o.Value).FirstOrDefault();
                WebHeaderCollection header = new WebHeaderCollection();
                header.Add("AppId", appId);
                header.Add("AppKey", apiKey);
                string jsonRequest = JsonConvert.SerializeObject(request);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, FCMB_INTEGRATION_URL+ FCMB_TRANX_STATUS_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                if (serverResult.Flag)
                {
                    FCMBResponse jsonResponse = JsonConvert.DeserializeObject<FCMBResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.StatusCode == "200")
                    {
                        //success
                        //GetTransactionStatusResponse dataResponse = JsonConvert.DeserializeObject<GetTransactionStatusResponse>(Convert.ToString(jsonResponse.ResponseData));
                        //jsonResponse.ResponseData = dataResponse;
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        //failed
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = jsonResponse;
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "Error communicating with server";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = serverResult;

                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public static ActionReturn RequestToken(string accountNumber,string tokenType, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                TokenRequest request = new TokenRequest
                {
                    AccountNumber=accountNumber,
                    TokenType=tokenType
                };
                string FCMB_INTEGRATION_URL = settings.Where(o => o.Key == "FCMB_INTEGRATION_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_TOKEN_REQ_URL = settings.Where(o => o.Key == "FCMB_TOKEN_REQ_URL").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppKey").Select(o => o.Value).FirstOrDefault();
                string appId = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppId").Select(o => o.Value).FirstOrDefault();
                WebHeaderCollection header = new WebHeaderCollection();
                header.Add("AppId", appId);
                header.Add("AppKey", apiKey);
                string jsonRequest = JsonConvert.SerializeObject(request);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, FCMB_INTEGRATION_URL+ FCMB_TOKEN_REQ_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                if (serverResult.Flag)
                {
                    FCMBResponse jsonResponse = JsonConvert.DeserializeObject<FCMBResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.StatusCode == "1")
                    {
                        //success
                        TokenResponse dataResponse = JsonConvert.DeserializeObject<TokenResponse>(Convert.ToString(jsonResponse.ResponseData));
                        jsonResponse.ResponseData = dataResponse;
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        //failed
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = jsonResponse;
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "Error communicating with server";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = serverResult;

                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public static ActionReturn BvnValidation(string bvn, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                BvnValidationRequest request = new BvnValidationRequest
                {
                    Bvn = bvn,
                    ShowImageIfAvailable=false
                };
                string FCMB_INTEGRATION_URL = settings.Where(o => o.Key == "FCMB_INTEGRATION_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_BVN_VALIDATION_URL = settings.Where(o => o.Key == "FCMB_BVN_VALIDATION_URL").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppKey").Select(o => o.Value).FirstOrDefault();
                string appId = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppId").Select(o => o.Value).FirstOrDefault();
                WebHeaderCollection header = new WebHeaderCollection();
                header.Add("AppId", appId);
                header.Add("AppKey", apiKey);
                string jsonRequest = JsonConvert.SerializeObject(request);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, FCMB_INTEGRATION_URL+ FCMB_BVN_VALIDATION_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                if (serverResult.Flag)
                {
                    FCMBResponse jsonResponse = JsonConvert.DeserializeObject<FCMBResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.StatusCode == "00")
                    {
                        //success
                       
                        //jsonResponse.ResponseData = dataResponse;
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        //failed
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = jsonResponse;
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "Error communicating with server";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = serverResult;

                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public static ActionReturn GetBankList(List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string FCMB_INTEGRATION_URL = settings.Where(o => o.Key == "FCMB_INTEGRATION_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_BANK_LIST_URL = settings.Where(o => o.Key == "FCMB_BANK_LIST_URL").Select(o => o.Value).FirstOrDefault();

                string apiKey = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppKey").Select(o => o.Value).FirstOrDefault();
                string appId = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppId").Select(o => o.Value).FirstOrDefault();
                WebHeaderCollection header = new WebHeaderCollection();
                header.Add("AppId", appId);
                header.Add("AppKey", apiKey);
                
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(CONSTANT.EMPTY, FCMB_INTEGRATION_URL + FCMB_BANK_LIST_URL, CONSTANT.EMPTY, CONSTANT.GET, CONSTANT.HAS_HEADER, header);
                if (serverResult.Flag)
                {
                    FCMBResponse jsonResponse = JsonConvert.DeserializeObject<FCMBResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.StatusCode == "1")
                    {
                        //success
                        //TokenResponse dataResponse = JsonConvert.DeserializeObject<TokenResponse>(Convert.ToString(jsonResponse.ResponseData));
                        //jsonResponse.ResponseData = dataResponse;
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        //failed
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = jsonResponse;
                    }
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "Error communicating with server";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = serverResult;

                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public static ActionReturn DirectCredit(string sourceAccount,string destinationAccount,decimal amount, string originatorName,
            string instrumentNo,string transactionId,string BankCode,string Narration1,string Narration2, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn tokenResult = new ActionReturn();
                DateTime dateTime = DateTime.Now;
                //string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                
                DirectCreditRequest request = new DirectCreditRequest
                {
                   Amount= amount.ToString("F2"),
                   BatchId="",
                   BranchCode="",
                   Currency="NGN",
                   DestinationAccountNumber=destinationAccount,
                   DestinationBank= "FCMB",
                   DestinationBankCode= BankCode,
                   InstrumentNo=instrumentNo,
                   Narration= Narration1,
                   Narration2= Narration2,
                   SourceAccountNumber=sourceAccount,
                   Token="",
                   TransactionDate= dateTime.ToString("yyyy-MM-ddTHH:mm:ss.ffff+01:00"),
                   TransactionId= transactionId
                };
                
                string FCMB_INTEGRATION_URL = settings.Where(o => o.Key == "FCMB_INTEGRATION_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_DIRECT_CREDIT_URL = settings.Where(o => o.Key == "FCMB_DIRECT_CREDIT_URL").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppKey").Select(o => o.Value).FirstOrDefault();
                string appId = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppId").Select(o => o.Value).FirstOrDefault();
                string Key = settings.Where(o => o.Key == "FCMB_INTEGRATION_DC_Key").Select(o => o.Value).FirstOrDefault(); ;
                string IV = settings.Where(o => o.Key == "FCMB_INTEGRATION_DC_IV").Select(o => o.Value).FirstOrDefault(); ;

                

                string Authorization = GetAuthorization(appId);
                string Nonce = GetNonce();
                Int32 Timestamp = GetTimestamp(dateTime);
                //string Signature = request.DestinationBankCode == BankCode ? GetFCMBSignature(Nonce, Timestamp, request.Amount, request.SourceAccountNumber, IV, Key)
                //    : GetOtherBankSignature(Nonce, Timestamp, request.Amount, request.SourceAccountNumber, request.DestinationBankCode, request.DestinationAccountNumber, request.OriginatorName, request.Narration, IV, Key);

                string Signature = GetFCMBSignature(Nonce, Timestamp, request.Amount, request.SourceAccountNumber, IV, Key);
                WebHeaderCollection header = new WebHeaderCollection();
                header.Add("Authorization", Authorization);
                header.Add("Nonce", Nonce);
                header.Add("Timestamp", Timestamp.ToString());
                header.Add("Signature", Signature);
                header.Add("appId", appId);
                header.Add("appKey", apiKey);

                string jsonRequest = JsonConvert.SerializeObject(request);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, FCMB_INTEGRATION_URL+ FCMB_DIRECT_CREDIT_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                if (serverResult.Flag)
                {
                    FCMBResponse jsonResponse = JsonConvert.DeserializeObject<FCMBResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.StatusCode == "200")
                    {
                        //success
                        //DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(jsonResponse.ResponseData));
                        //jsonResponse.ResponseData = dataResponse;
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        
                    }
                    else
                    {
                        //failed
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = jsonResponse;
                    }
                    result.ServiceRequest = serverResult.ServiceRequest;
                    result.ServiceResponse = serverResult.ServiceResponse;
                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "Error communicating with server";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = serverResult;

                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public static ActionReturn DirectCreditTest(DirectCreditRequest request,List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn tokenResult = new ActionReturn();
                //DirectCreditRequest request = new DirectCreditRequest
                //{
                //    Amount = amount,
                //    BatchId = "",
                //    BranchCode = "",
                //    Currency = "NGN",
                //    DestinationAccountNumber = destinationAccount,
                //    DestinationBank = "FCMB",
                //    DestinationBankCode = "",
                //    InstrumentNo = instrumentNo,
                //    Narration = "",
                //    Narration2 = "",
                //    SourceAccountNumber = sourceAccount,
                //    Token = "",
                //    TransactionDate = "",
                //    TransactionId = ""
                //};

                string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();
                string FCMB_INTEGRATION_URL = settings.Where(o => o.Key == "FCMB_INTEGRATION_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_DIRECT_CREDIT_URL = settings.Where(o => o.Key == "FCMB_DIRECT_CREDIT_URL").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppKey").Select(o => o.Value).FirstOrDefault();
                string appId = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppId").Select(o => o.Value).FirstOrDefault();
                string Key = settings.Where(o => o.Key == "FCMB_INTEGRATION_DC_Key").Select(o => o.Value).FirstOrDefault(); ;
                string IV = settings.Where(o => o.Key == "FCMB_INTEGRATION_DC_IV").Select(o => o.Value).FirstOrDefault(); ;
                
                DateTime dateTime = DateTime.Now;

                string Authorization = GetAuthorization(appId);
                string Nonce = GetNonce();
                Int32 Timestamp = GetTimestamp(dateTime);
                //string Signature = request.DestinationBankCode == FCMB_BANK_CODE ? GetFCMBSignature(Nonce, Timestamp, request.Amount, request.SourceAccountNumber, IV, Key) 
                //    : GetOtherBankSignature(Nonce, Timestamp, request.Amount, request.SourceAccountNumber, request.DestinationBankCode, request.DestinationAccountNumber, request.OriginatorName, request.Narration, IV, Key);

                string Signature = GetFCMBSignature(Nonce, Timestamp, request.Amount, request.SourceAccountNumber, IV, Key);
                request.TransactionDate = dateTime.ToString("yyyy-MM-ddTHH:mm:ss.ffff+01:00");

                WebHeaderCollection header = new WebHeaderCollection();
                header.Add("Authorization", Authorization);
                header.Add("Nonce", Nonce);
                header.Add("Timestamp", Timestamp.ToString());
                header.Add("Signature", Signature);
                header.Add("appId", appId);
                header.Add("appKey", apiKey);
                
                string jsonRequest = JsonConvert.SerializeObject(request);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, FCMB_INTEGRATION_URL + FCMB_DIRECT_CREDIT_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                //if (serverResult.Flag)
                //{
                //    FCMBResponse jsonResponse = JsonConvert.DeserializeObject<FCMBResponse>(Convert.ToString(serverResult.ReturnedObject));
                //    if (jsonResponse.StatusCode == "00")
                //    {
                //        //success
                //        //DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(jsonResponse.ResponseData));
                //        //jsonResponse.ResponseData = dataResponse;
                //        result.ReturnedObject = jsonResponse;
                //        result.UpdateReturn = UpdateReturn.ItemCreated;
                //        result.Message = jsonResponse.StatusMessage;
                //        result.Flag = true;
                //        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                //    }
                //    else
                //    {
                //        //failed
                //        result.UpdateReturn = UpdateReturn.ItemNotExist;
                //        result.Message = jsonResponse.StatusMessage;
                //        result.Flag = false;
                //        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                //        result.ReturnedObject = jsonResponse;
                //    }


                //}
                //else
                //{
                //    result.UpdateReturn = UpdateReturn.ItemNotExist;
                //    result.Message = "Error communicating with server";
                //    result.Flag = false;
                //    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                //    result.ReturnedObject = "";

                //}

                //FCMBResponse jsonResponse = JsonConvert.DeserializeObject<FCMBResponse>(Convert.ToString(serverResult.ReturnedObject));
                result.ReturnedObject = serverResult.ReturnedObject;
                result.UpdateReturn = UpdateReturn.ItemCreated;
                result.Message = "";// jsonResponse.StatusMessage;
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

        public static ActionReturn SendSms(string accountNo, string message, string sender, string mobileNo, string sbuCode, string channel, string branchCode, string SMSConnectionString)

        {

            ActionReturn result = new ActionReturn();
            try
            {
                /*using (SqlConnection myConnection = new SqlConnection(SMSConnectionString))

                {

                    using (SqlCommand myCommand = new SqlCommand("SMSQueue_Vanso_Priority_AddNew", myConnection))

                    {

                        myCommand.CommandType = System.Data.CommandType.StoredProcedure;

                        myCommand.Parameters.AddWithValue("@MobileID", mobileNo);

                        myCommand.Parameters.AddWithValue("@Message", message);

                        myCommand.Parameters.AddWithValue("@SenderID", sender);

                        myCommand.Parameters.AddWithValue("@TryCount", 0);

                        myCommand.Parameters.AddWithValue("@Status", "P");

                        myCommand.Parameters.AddWithValue("@TicketID", " ");

                        myCommand.Parameters.AddWithValue("@Channel", channel);

                        myCommand.Parameters.AddWithValue("@BranchCode", branchCode);

                        myCommand.Parameters.AddWithValue("@ForAcid", accountNo);

                        myCommand.Parameters.AddWithValue("@PostDate", DateTime.Now);

                        myCommand.Parameters.AddWithValue("@SbuCode", sbuCode);



                        myConnection.Open();

                        int id = Convert.ToInt32(myCommand.ExecuteScalar());
                        result.ReturnedObject = id;
                        result.UpdateReturn = UpdateReturn.ItemRetrieved;
                        result.Message = "Debit was successful";
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                    }

                    myConnection.Close();

                }*/
                result.UpdateReturn = UpdateReturn.ItemRetrieved;
                result.Message = "otp was sent successfully";
                result.Flag = true;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;

        }
        public static ActionReturn GetCustomerInformationByAccountNo(string accountNo, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                //FCMB_SMS_APPID,FCMB_SMS_APPKEY
                //FCMB_CUSTOMER_INFO_URL,FCMB_CUSTOMER_INFO_APPKEY,FCMB_CUSTOMER_INFO_APPID,FCMB_CUSTOMER_INFO_SOAP_ACTION
                string FCMB_CUSTOMER_INFO_URL = settings.Where(o => o.Key == "FCMB_CUSTOMER_INFO_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_CUSTOMER_INFO_APPKEY = settings.Where(o => o.Key == "FCMB_CUSTOMER_INFO_APPKEY").Select(o => o.Value).FirstOrDefault();
                string FCMB_CUSTOMER_INFO_APPID = settings.Where(o => o.Key == "FCMB_CUSTOMER_INFO_APPID").Select(o => o.Value).FirstOrDefault();
                string FCMB_CUSTOMER_INFO_SOAP_ACTION = settings.Where(o => o.Key == "FCMB_CUSTOMER_INFO_SOAP_ACTION").Select(o => o.Value).FirstOrDefault();

                using (FCMBSoapService.ServiceSoapClient ctx = new FCMBSoapService.ServiceSoapClient())
                {
                    string response=ctx.customeraccountdetail(accountNo, FCMB_CUSTOMER_INFO_APPID, FCMB_CUSTOMER_INFO_APPKEY);
                    if (!string.IsNullOrEmpty(response))
                    {
                        string[] responseArray = response.Split('|');
                        if(responseArray.Length > 13)
                        {
                            CustomerAccountInformation customerInforamtion = new CustomerAccountInformation
                            {
                                Name = responseArray[0],
                                Balance = responseArray[5],
                                Currency = responseArray[6],
                                PhoneNumber = responseArray[9],
                                Address = responseArray[12],
                                Email = responseArray[13]
                            };
                            result.ReturnedObject = customerInforamtion;
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Customer information was retrieved successfully";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemNotExist;
                            result.Message = "Could not retrieve customer information";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = response;

                        }
                        
                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = "Error communicating with server";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = "";
                    }
                }

                //WebHeaderCollection header = new WebHeaderCollection();
                //header.Add("SOAPAction", FCMB_CUSTOMER_INFO_SOAP_ACTION);

                //string xmlRequest = FCMBXmlRequest.CustomerInfoRequest(accountNo, FCMB_CUSTOMER_INFO_APPID, FCMB_CUSTOMER_INFO_APPKEY);
                //ActionReturn xmlResponseResult = Util.MakeRequestAndGetResponse(xmlRequest, FCMB_CUSTOMER_INFO_URL, CONSTANT.EMPTY, CONSTANT.POST, true, header, CONSTANT.CONTENT_TYPE_XML);
                //if (xmlResponseResult.Flag)
                //{

                //}
                //else
                //{
                //    //failed customer request
                //}


            }
            catch(Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {

            }
            return result;
        }

        public static ActionReturn ValidateAccountAndSendOTP(string accountNo, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                //FCMB_SMS_APPID,FCMB_SMS_APPKEY
                //FCMB_CUSTOMER_INFO_URL,FCMB_CUSTOMER_INFO_APPKEY,FCMB_CUSTOMER_INFO_APPID,FCMB_CUSTOMER_INFO_SOAP_ACTION
                string FCMB_SMS_APPKEY = settings.Where(o => o.Key == "FCMB_SMS_APPKEY").Select(o => o.Value).FirstOrDefault();
                string FCMB_SMS_APPID = settings.Where(o => o.Key == "FCMB_SMS_APPID").Select(o => o.Value).FirstOrDefault();
                string FCMB_SMS_URL = settings.Where(o => o.Key == "FCMB_SMS_URL").Select(o => o.Value).FirstOrDefault();
                


                using (FCMBSoapService2.SmsInterfaceSoapClient ctx = new FCMBSoapService2.SmsInterfaceSoapClient())
                {
                    //string response = ctx.ConfirmAccountNoandSendOTP(accountNo, FCMB_SMS_APPID, FCMB_SMS_APPKEY);
                    string response = ctx.InsertSms("FCMB", "08025019931", "Test otp", "001", "OtpTest", FCMB_SMS_APPID, FCMB_SMS_APPKEY);

                    if (!string.IsNullOrEmpty(response))
                    {
                        string[] responseArray = response.Split('|');
                        if (responseArray.Length > 13)
                        {
                            CustomerAccountInformation customerInforamtion = new CustomerAccountInformation
                            {
                                Name = responseArray[0],
                                Balance = responseArray[5],
                                Currency = responseArray[6],
                                PhoneNumber = responseArray[9],
                                Address = responseArray[12],
                                Email = responseArray[13]
                            };
                            result.ReturnedObject = customerInforamtion;
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Customer information was retrieved successfully";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemNotExist;
                            result.Message = "Could not retrieve customer information";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = response;

                        }

                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = "Error communicating with server";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = "";
                    }

                    //result.ReturnedObject = response;
                    //result.UpdateReturn = UpdateReturn.ItemCreated;
                    //result.Message = "Customer information was retrieved successfully";
                    //result.Flag = true;
                    //result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }
                

            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {

            }
            return result;
        }

        public static ActionReturn SendOTP(string accountNo, string phoneNumber, string message, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                //FCMB_SMS_APPID,FCMB_SMS_APPKEY
                //FCMB_CUSTOMER_INFO_URL,FCMB_CUSTOMER_INFO_APPKEY,FCMB_CUSTOMER_INFO_APPID,FCMB_CUSTOMER_INFO_SOAP_ACTION
                string FCMB_SMS_APPKEY = settings.Where(o => o.Key == "FCMB_SMS_APPKEY").Select(o => o.Value).FirstOrDefault();
                string FCMB_SMS_APPID = settings.Where(o => o.Key == "FCMB_SMS_APPID").Select(o => o.Value).FirstOrDefault();
                string FCMB_SMS_SENDER_ID = settings.Where(o => o.Key == "FCMB_SMS_SENDER_ID").Select(o => o.Value).FirstOrDefault();
                string FCMB_SMS_BRANCH_CODE = settings.Where(o => o.Key == "FCMB_SMS_BRANCH_CODE").Select(o => o.Value).FirstOrDefault();


                using (FCMBSoapService2.SmsInterfaceSoapClient ctx = new FCMBSoapService2.SmsInterfaceSoapClient())
                {
                    //string response = ctx.ConfirmAccountNoandSendOTP(accountNo, FCMB_SMS_APPID, FCMB_SMS_APPKEY);
                    string response = ctx.InsertSms(FCMB_SMS_SENDER_ID, phoneNumber, message, FCMB_SMS_BRANCH_CODE, accountNo, FCMB_SMS_APPID, FCMB_SMS_APPKEY);

                    if (!string.IsNullOrEmpty(response))
                    {
                        string[] responseArray = response.Split('|');
                        if (responseArray.Length > 1 & responseArray[0] == CONSTANT.SUCCESS_RESPONSE)
                        {

                            //result.ReturnedObject = customerInforamtion;
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "otp sent successfully";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemNotExist;
                            result.Message = "Could not send otp";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = response;

                        }

                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = "otp sent successfully";
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();

                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = "Error communicating with server";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = "";
                    }

                    //result.ReturnedObject = response;
                    //result.UpdateReturn = UpdateReturn.ItemCreated;
                    //result.Message = "Customer information was retrieved successfully";
                    //result.Flag = true;
                    //result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                }


            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {

            }
            return result;
        }

        public static ActionReturn ValidateOTP(string accountNo, string otp,List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                //FCMB_SMS_APPID,FCMB_SMS_APPKEY
                //FCMB_CUSTOMER_INFO_URL,FCMB_CUSTOMER_INFO_APPKEY,FCMB_CUSTOMER_INFO_APPID,FCMB_CUSTOMER_INFO_SOAP_ACTION
                string FCMB_SMS_APPKEY = settings.Where(o => o.Key == "FCMB_SMS_APPKEY").Select(o => o.Value).FirstOrDefault();
                string FCMB_SMS_APPID = settings.Where(o => o.Key == "FCMB_SMS_APPID").Select(o => o.Value).FirstOrDefault();
                string FCMB_SMS_URL = settings.Where(o => o.Key == "FCMB_SMS_URL").Select(o => o.Value).FirstOrDefault();



                using (FCMBSoapService2.SmsInterfaceSoapClient ctx = new FCMBSoapService2.SmsInterfaceSoapClient())
                {
                    string response = ctx.ValidateOTP(accountNo,otp, FCMB_SMS_APPID, FCMB_SMS_APPKEY);
                    if (!string.IsNullOrEmpty(response))
                    {
                        string[] responseArray = response.Split('|');
                        if (responseArray.Length > 13)
                        {
                            CustomerAccountInformation customerInforamtion = new CustomerAccountInformation
                            {
                                Name = responseArray[0],
                                Balance = responseArray[5],
                                Currency = responseArray[6],
                                PhoneNumber = responseArray[9],
                                Address = responseArray[12],
                                Email = responseArray[13]
                            };
                            result.ReturnedObject = customerInforamtion;
                            result.UpdateReturn = UpdateReturn.ItemCreated;
                            result.Message = "Customer information was retrieved successfully";
                            result.Flag = true;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                        }
                        else
                        {
                            result.UpdateReturn = UpdateReturn.ItemNotExist;
                            result.Message = "Could not retrieve customer information";
                            result.Flag = false;
                            result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                            result.ReturnedObject = response;

                        }

                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = "Error communicating with server";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = "";
                    }
                }


            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {

            }
            return result;
        }

        public static ActionReturn ValidateActiveDirectoryUser(string username, string password, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                string FCMB_AD_AUTH_SERVICE_APPKEY = settings.Where(o => o.Key == "FCMB_AD_AUTH_SERVICE_APPKEY").Select(o => o.Value).FirstOrDefault();
                string FCMB_AD_AUTH_SERVICE_APPID = settings.Where(o => o.Key == "FCMB_AD_AUTH_SERVICE_APPID").Select(o => o.Value).FirstOrDefault();
                using (FCMBAuthSoapService.AuthenticationServiceSoapClient ctx=new FCMBAuthSoapService.AuthenticationServiceSoapClient())
                {
                    FCMBAuthSoapService.UserAdDetails userAdDetails = ctx.GetUserAdFullDetails(username, password, FCMB_AD_AUTH_SERVICE_APPID, FCMB_AD_AUTH_SERVICE_APPKEY);
                    if(userAdDetails != null)
                    {
                        result.ReturnedObject = new AdminUser { FirstName = userAdDetails.DisplayName, LastName = "", UserId = username, FullName=userAdDetails.StaffName }; 
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = "Customer information was retrieved successfully";
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = "Could not retrieve customer information";
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = userAdDetails;
                    }
                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            finally
            {

            }
            return result;
        }

        private static string GetAuthorization(string appId)
        {
            return $"FCMBAuth {Convert.ToBase64String(Encoding.UTF8.GetBytes(appId))}";
        }
        private static Int32 GetTimestamp(DateTime dateTime)
        {
            Int32 timeStamp = (Int32)(TimeZoneInfo.ConvertTimeToUtc(dateTime) - new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
            return timeStamp;
        }
        private static string GetNonce()
        {
            return System.Guid.NewGuid().ToString().Replace("-", "");
        }
        private static string GetFCMBSignature(string Nonce, Int32 timestamp, string Amount, string SourceAccountNumber, string IV, string Key)
        {
            string hashData = (Nonce + "&" + timestamp + "&" + 1 + "&" + Amount + "&" + SourceAccountNumber);
            return AesEncryptionAndDecryption.Encrypt(hashData,Key,IV);
        }
        private static string GetOtherBankSignature(string Nonce, Int32 timestamp, decimal Amount, 
            string SourceAccountNumber,string DestinationInstitutionCode,string BeneficiaryAccountNumber,string OriginatorAccountName,string PaymentReference, string IV, string Key)
        {
            string hashData = (Nonce + "&" + timestamp + "&" + 1 + "&" + Amount + "&" + DestinationInstitutionCode + "&" + BeneficiaryAccountNumber + "&" + OriginatorAccountName + "&" + PaymentReference);
            return AesEncryptionAndDecryption.Encrypt(hashData, Key, IV);
        }
        
        public static ActionReturn InterBankTransfer(InterBankTransferRequest request, List<AppConfigSetting> settings)
        {
            ActionReturn result = new ActionReturn();
            try
            {
                ActionReturn tokenResult = new ActionReturn();
                DateTime dateTime = DateTime.Now;
                //string FCMB_BANK_CODE = settings.Where(o => o.Key == "FCMB_BANK_CODE").Select(o => o.Value).FirstOrDefault();



                string FCMB_INTEGRATION_URL = settings.Where(o => o.Key == "FCMB_INTEGRATION_URL").Select(o => o.Value).FirstOrDefault();
                string FCMB_INTER_BANK_TRANSFER_URL = settings.Where(o => o.Key == "FCMB_INTER_BANK_TRANSFER_URL").Select(o => o.Value).FirstOrDefault();
                string apiKey = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppKey").Select(o => o.Value).FirstOrDefault();
                string appId = settings.Where(o => o.Key == "FCMB_INTEGRATION_AppId").Select(o => o.Value).FirstOrDefault();
                string Key = settings.Where(o => o.Key == "FCMB_INTEGRATION_DC_Key").Select(o => o.Value).FirstOrDefault(); ;
                string IV = settings.Where(o => o.Key == "FCMB_INTEGRATION_DC_IV").Select(o => o.Value).FirstOrDefault(); ;

                request.Amount = Math.Round(request.Amount);
                request.TransactionDate = dateTime.ToString();
                string Authorization = GetAuthorization(appId);
                string Nonce = GetNonce();
                Int32 Timestamp = GetTimestamp(dateTime);
                string Signature = GetOtherBankSignature(Nonce, Timestamp, request.Amount, request.OriginatorAccountNumber, request.DestinationInstitutionCode, request.BeneficiaryAccountNumber, request.OriginatorAccountName, request.PaymentReference, IV, Key);

                WebHeaderCollection header = new WebHeaderCollection();
                header.Add("Authorization", Authorization);
                header.Add("Nonce", Nonce);
                header.Add("Timestamp", Timestamp.ToString());
                header.Add("Signature", Signature);
                header.Add("appId", appId);
                header.Add("appKey", apiKey);

                string jsonRequest = JsonConvert.SerializeObject(request);
                ActionReturn serverResult = Util.MakeRequestAndGetResponse(jsonRequest, FCMB_INTEGRATION_URL + FCMB_INTER_BANK_TRANSFER_URL, CONSTANT.EMPTY, CONSTANT.POST, CONSTANT.HAS_HEADER, header);
                if (serverResult.Flag)
                {
                    FCMBResponse jsonResponse = JsonConvert.DeserializeObject<FCMBResponse>(Convert.ToString(serverResult.ReturnedObject));
                    if (jsonResponse.StatusCode == "00")
                    {
                        //success
                        //DirectCreditResponse dataResponse = JsonConvert.DeserializeObject<DirectCreditResponse>(Convert.ToString(jsonResponse.ResponseData));
                        //jsonResponse.ResponseData = dataResponse;
                        result.ReturnedObject = jsonResponse;
                        result.UpdateReturn = UpdateReturn.ItemCreated;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = true;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.SUCCESS.ToString();
                    }
                    else
                    {
                        //failed
                        result.UpdateReturn = UpdateReturn.ItemNotExist;
                        result.Message = jsonResponse.StatusMessage;
                        result.Flag = false;
                        result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                        result.ReturnedObject = jsonResponse;
                    }
                    result.ServiceRequest = serverResult.ServiceRequest;
                    result.ServiceResponse = serverResult.ServiceResponse;

                }
                else
                {
                    result.UpdateReturn = UpdateReturn.ItemNotExist;
                    result.Message = "Error communicating with server";
                    result.Flag = false;
                    result.FlagDescription = Enumerators.FLAG_DESCRIPTION.FAILED.ToString();
                    result.ReturnedObject = serverResult;

                }
            }
            catch (Exception ex)
            {
                result.UpdateReturn = UpdateReturn.Exception;
                result.Message = "Exception";
                result.Flag = false;
                result.FlagDescription = Enumerators.FLAG_DESCRIPTION.EXCEPTION.ToString();
                result.ReturnedObject = ex.ToString();
            }
            return result;
        }

    }
}
