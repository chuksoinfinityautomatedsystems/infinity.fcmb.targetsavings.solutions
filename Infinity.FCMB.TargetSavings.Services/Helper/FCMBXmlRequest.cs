﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Infinity.FCMB.TargetSavings.Services.Helper
{
    public static class FCMBXmlRequest
    {
        public static string CustomerInfoRequest(string accountNo,string username,string password)
        {
            string builtXML = string.Empty;
            try
            {

                XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
                XNamespace xsi = "http://www.w3.org/2001/XMLSchema-instance";
                XNamespace xsd = "http://www.w3.org/2001/XMLSchema";
                XNamespace xmlns = "http://tempuri.org/";

                XElement elem = new XElement(soap + "Envelope",
                    new XAttribute(XNamespace.Xmlns + "soap", soap.NamespaceName),
                    new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName),
                    new XAttribute(XNamespace.Xmlns + "xsd", xsd.NamespaceName),
                    new XElement("Body",
                        new XElement(xmlns + "customeraccountdetail",
                            new XElement("vforacid", accountNo),
                            new XElement("vusername",username),
                            new XElement("vpasswd", password)
                            )
                        )
                    );

                Encoding enc = Encoding.UTF8;
                XDocument encodedDoc8 = new XDocument(
                    new XDeclaration("1.0", "utf-8", "yes"),
                   elem
                );
                builtXML = encodedDoc8.ToString();
            }
            catch (Exception ex)
            {
            }
            return builtXML;
        }
    }
}

