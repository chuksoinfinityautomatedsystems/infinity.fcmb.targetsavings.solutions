﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infinity.FCMB.TargetSavings.Services.Helper
{
    public class FileLogger
    {
        private static object mutex;
        public FileLogger()
        {
            mutex = new object();
        }
        public void LogToFile(object log, string logDirectory, string fileName)
        {
            try
            {
                string logFile = $"{fileName}_{DateTime.Today.ToString("ddMMMyyyy")}.txt";
                logDirectory = logDirectory + "\\" + DateTime.Now.ToString("yyyy") + "\\" + DateTime.Now.ToString("MMM") + "\\" + DateTime.Now.ToString("ddMMMyyy");
                if (!Directory.Exists(logDirectory))
                {
                    Directory.CreateDirectory(logDirectory);
                }
                lock (mutex)
                {

                    File.AppendAllText(logDirectory + "\\" + logFile, $"{DateTime.Now.ToString("HH:mm:ss")}|{JsonConvert.SerializeObject(log)}\r\n");
                }

            }
            catch (Exception ex) { }
        }
    }
}
