﻿using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace Infinity.FCMB.TargetSavings.Services.Helper
{
    public static class ActiveDirectoryUtility
    {
        public static bool Authenticate(string userName, string password,string domainPath)
        {
            bool authentic = false;
            try
            {
                DirectoryEntry entry = new DirectoryEntry(domainPath, userName, password);
                object nativeObject = entry.NativeObject;
                authentic = true;
            }
            catch (DirectoryServicesCOMException) { }
            return authentic;
        }

        public static string GetADFirstName(string username, string domain) //Return FirstName
        {
            string result = string.Empty;
            try
            {
                // if you do repeated domain access, you might want to do this *once* outside this method, // and pass it in as a second parameter!
                PrincipalContext yourDomain = new PrincipalContext(ContextType.Domain);
                string path = domain + @"\" + username;
                // find the user 
                UserPrincipal user = UserPrincipal.FindByIdentity(yourDomain, path);
                // if user is found 
                if (user != null)
                {
                    // get DirectoryEntry underlying it 
                    DirectoryEntry de = (user.GetUnderlyingObject() as DirectoryEntry);
                    if (de != null)
                    {
                        if (de.Properties.Contains("givenName"))
                        {
                            result = de.Properties["givenName"].Value.ToString();
                        }
                    }
                }
            }
            catch { }
            return result;
        }

        public static string GetADLastName(string username, string domain)
        {
            string result = string.Empty;
            try
            {
                // if you do repeated domain access, you might want to do this *once* outside this method, // and pass it in as a second parameter!
                PrincipalContext yourDomain = new PrincipalContext(ContextType.Domain);
                string path = domain + @"\" + username;
                // find the user 
                UserPrincipal user = UserPrincipal.FindByIdentity(yourDomain, path);
                // if user is found 
                if (user != null)
                {
                    // get DirectoryEntry underlying it 
                    DirectoryEntry de = (user.GetUnderlyingObject() as DirectoryEntry);
                    if (de != null)
                    {
                        if (de.Properties.Contains("sn"))
                        {
                            result = de.Properties["sn"].Value.ToString();
                        }
                    }
                }
            }
            catch { }
            return result;
        }

        public static bool isGroupMember(string username, string groupName, string domainPath)
        {
            DirectoryEntry user = new DirectoryEntry(domainPath, username, "", AuthenticationTypes.Anonymous);
            bool isingroup = false;
            //get all the groups that the Active Directory entry belongs to
            DirectorySearcher search = new DirectorySearcher();
            search.Filter = String.Format("(sAMAccountName={0})", user.Username);
            search.PropertiesToLoad.Add("memberOf");
            SearchResult result = search.FindOne();

            if (result != null)
            {
                //Check the entry's groups for given group name and return the result
                foreach (object group in result.Properties["memberOf"])
                {
                    if (group.ToString().Contains(groupName))
                    {
                        isingroup = true;
                        break;
                    }
                }
            }
            return isingroup;
        }
        public static string GetFullName(string username,string domain)
        {
            return string.Format("{0} - {1} {2}", username, GetADLastName(username,domain), GetADFirstName(username, domain));
        }
    }
}
